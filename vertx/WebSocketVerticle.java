import java.util.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.AbstractVerticle;

import vertx.PK;
import vertx.GameController;
import vertx.GameTimer;

public class WebSocketVerticle extends AbstractVerticle {

    // server run
    private static final String IP_ADDRESS = "153.126.163.82"; // SAKURA

    // local PC run
    //private static final String IP_ADDRESS = "192.168.10.100"; //HOME
    //private static final String IP_ADDRESS = "10.67.173.170";     // TURRYS_DNB
    //private static final String IP_ADDRESS = "10.4.155.198";   // STARBACKS_13
    //private static final String IP_ADDRESS = "192.168.11.57";  // COMEDA
    //private static final String IP_ADDRESS = "10.4.137.174"; // OTHER

    private static final int PORT_NUM = 1993;

    private Map<Integer, JsonObject> _objects = new HashMap<>(); // _userId => other
    private Map<Integer, ServerWebSocket> _wss = new HashMap<>(); // _userId => ws

    private GameController _gameController;

    public void start() {
        _gameController = new GameController();
        vertx.createHttpServer().websocketHandler(new Handler<ServerWebSocket>() {
            public void handle(ServerWebSocket ws) {
                System.out.println("websocket action.");
                ws.handler(new Handler<Buffer>() {
                    public void handle(Buffer data) {
                        String strData = "" + data;
                        int userId = getUserId(strData);
                        System.out.println("Received: " + strData + ", from: " + userId);

                        if (userId == 0)
                        {
                            userId = createUserId();
                            System.out.println("create userId(" + userId + ")");
                        }

                        // クラ側から送られてきたjsonデータを取得
                        loadPacketData(userId, strData);
                        int pkType = _objects.get(userId).getInteger("type");

                        // WebSocketの保存
                        if (_wss.containsKey(userId))
                        {
                            System.out.println("already exist ws. reset ws of userId(" + userId + ")");
                            _wss.put(userId, ws);
                        }
                        else
                        {
                            System.out.println("set ws of userId(" + userId + ")");
                            _wss.put(userId, ws);
                        }

                        String write = _gameController.echoMessage(pkType, _objects.get(userId));
                        if (!write.equals(""))
                        {
                            if (_gameController.isSendMany(pkType))
                            {
                                sendManyUsers(_gameController.getSendUsers(pkType, userId), write);
                            }
                            else
                            {
                                _wss.get(userId).writeFinalTextFrame(write);
                            }
                        }

                        // 以下、gameController上のフラグ確認
                        if (_gameController.existReadyRoom())
                        {
                            write = _gameController.echoMessage(PK.PK_START_GAME, _objects.get(userId));
                            if (!write.equals(""))
                            {
                                sendManyUsers(_gameController.getReadyRoomUsers() ,write);
                            }
                            _gameController.resetReadyRoom();
                        }

                        if (!_gameController.existAvailableCard(userId))
                        {
                            setRestartTimer(userId, _gameController.getSendUsers(pkType, userId));
                        }

                        // WebSocketの削除
                        if (pkType == PK.PK_CLOSE_WEBSOCKET)
                        {
                            _wss.remove(userId);
                        }
                    }
                });

                //接続完了時に送る場合
                //ws.writeFinalTextFrame("Connect!");
            }
        }).listen(PORT_NUM, IP_ADDRESS);
    }

    private int getUserId(String data)
    {
        if (!data.equals(""))
        {
            return (new JsonObject(data)).getInteger("userId");
        }
        return 0;
    }

    private int createUserId()
    {
        Random rand = new Random();
        int randUserId = rand.nextInt(899999) + 100000;
        if (randUserId < 100000 || randUserId > 999999)
        {
            System.out.println("invalid range randUserId(" + randUserId + ")");
        }
        return randUserId;
    }

    private void loadPacketData(int userId, String data)
    {
        if (!data.equals(""))
        {
            JsonObject jsonObject = new JsonObject(data);
            // createUser時,dataの書き換え
            if (userId != getUserId(data))
            {
                jsonObject.remove("userId");
                jsonObject.put("userId", userId);
            }

            _objects.put(userId, jsonObject);
            /*
            Set<String> keys = _object.fieldNames();
            for (Iterator<String> i = keys.iterator(); i.hasNext();)
            {
                String key = i.next();
                if (key.equals("type"))
                {
                    _pkType = _object.getInteger(key);
                    return;
                }
            }
            */
        }
    }

    private void sendManyUsers(List<Integer> userIds, String message)
    {
        userIds.forEach(userId -> {
            _wss.get(userId).writeFinalTextFrame(message);
        });
    }

    private void setRestartTimer(int triggerUserId, List<Integer> userIds)
    {
        boolean isFirst = true;
        // タイマーセット
        for (int userId : userIds)
        {
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("userId", userId);
            jsonObject.put("isFirst", isFirst ? 1 : 0);
            GameTimer gameTimer = new GameTimer(_gameController, userId, PK.PK_UPDATE_CARD_FORCE, _wss.get(userId), jsonObject);
            Timer timer = new Timer();
            timer.schedule((TimerTask)gameTimer, 3000);
            _gameController.setUserTimer(userId, timer);
            isFirst = false;
        }
    }
}
