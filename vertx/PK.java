package vertx;

public class PK {
    public static final int PK_CREATE_USER_ID = 100;
    public static final int PK_LOGIN = 101;
    public static final int PK_START_GAME = 110;
    public static final int PK_UPDATE_CARDS = 200;
    public static final int PK_UPDATE_CARD_FORCE = 201;
    public static final int PK_CLOSE_WEBSOCKET = 9999;
}