package vertx;

import java.util.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.http.ServerWebSocket;
import vertx.PK;
import vertx.GameController;

public class GameTimer extends TimerTask
{
    private GameController _gameController;
    private int _userId;
    private int _pkType;
    private ServerWebSocket _ws;
    private JsonObject _params;

    public GameTimer(GameController gameController, int userId, int pkType, ServerWebSocket ws, JsonObject params)
    {
        _gameController = gameController;
        _userId = userId;
        _pkType = pkType;
        _ws = ws;
        _params = params;
    }

    @Override
    public void run() {
        String write = _gameController.echoMessage(_pkType, _params);
        if (!write.equals(""))
        {
            _ws.writeFinalTextFrame(write);
        }
    }
}