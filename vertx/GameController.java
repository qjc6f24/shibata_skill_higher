package vertx;

import java.util.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;
import vertx.PK;
import vertx.GameTimer;

public class GameController {

    private static final int MAX_ROOM_COUNT = 10;
    private static final int CARD_MAX_COUNT = 4;
    private static final int USER_MAX_COUNT = 4;
    private static final int MAX_CARD_COUNT = 6;

    private static final int CARD_TYPE_COUNT = 7;

    private static boolean _isDebug = false;
    private static Random _rand;

    private List<Integer> _readyRooms = new ArrayList<>();
    private Map<Integer, List<Integer>> _roomUsers = new HashMap<>(); // roomId => List<userId>
    private Map<Integer, Integer> _userRooms = new HashMap<>(); // userId => roomId
    private Map<Integer, Field> _fields = new HashMap<>(); // _roomId => Field
    private Map<Integer, List<Timer>> _userTimers = new HashMap<>();

    public GameController()
    {
    }

    private Map<Integer, Integer> _existRoomIds = new HashMap<>();

    public boolean existReadyRoom()
    {
        return _readyRooms.size() > 0;
    }

    public Field getFieldByUserId(int userId)
    {
        return _fields.get(_userRooms.get(userId));
    }

    public boolean existAvailableCard(int userId)
    {
        Field field = getFieldByUserId(userId);
        if (field != null)
        {
            return field.getExistAvailableCard();
        }
        return false;
    }

    public List<Integer> getReadyRoomUsers()
    {
        List<Integer> users = new ArrayList<>();
        _readyRooms.forEach(roomId -> {
            _roomUsers.get(roomId).forEach(userId -> {
                users.add(userId);
            });
        });
        return users;
    }

    public void resetReadyRoom()
    {
        _readyRooms = new ArrayList<>();
    }

    public boolean isSendMany(int pkType)
    {
        switch (pkType)
        {
            case PK.PK_UPDATE_CARDS:
                return true;
            default:
                return false;
        }
    }

    public List<Integer> getSendUsers(int pkType, int userId)
    {
        int roomId = _userRooms.get(userId);
        switch (pkType)
        {
            case PK.PK_UPDATE_CARDS:
                return _roomUsers.get(roomId);
            default:
                return new ArrayList<>();
        }
    }

    public void setUserTimer(int userId, Timer timer)
    {
        int setIndex = 0;
        List<Timer> timers = _userTimers.get(userId);
        if (timers == null)
        {
            timers = new ArrayList<>();
            timers.add(timer);
            _userTimers.put(userId, timers);
            return;
        }

        timers.add(timer);
    }



    // PkAction
    public String echoMessage(int pkType, JsonObject object)
    {
        String message = "{" + setType(pkType);

        int userId = object.getInteger("userId");
        switch (pkType)
        {
            case PK.PK_CREATE_USER_ID:
                message += getMessagePkCreateUserId(userId);
                break;
            case PK.PK_LOGIN:
                message += getMessagePkLogin(userId);
                break;
            case PK.PK_START_GAME:
                message += _fields.get(_userRooms.get(userId)).getMessageAllInfo();
                break;
            case PK.PK_CLOSE_WEBSOCKET:
                //actionPkCloseWebsocket(userId); // ログアウト時にルーム情報から削除
                break;
            case PK.PK_UPDATE_CARDS:
                Field field = _fields.get(_userRooms.get(userId));
                field.updateCards(object.getInteger("playerNum"),
                                  object.getInteger("fieldIdx"),
                                  object.getInteger("userSelectingIdx"));
                message += field.getMessageAllInfo();
                break;
            case PK.PK_UPDATE_CARD_FORCE:
                field = _fields.get(_userRooms.get(userId));
                if (object.getInteger("isFirst") == 1)
                {
                    field.forceChange();
                }
                while (!field.existAvailableCard())
                {
                    field = _fields.get(_userRooms.get(userId));
                }
                message += field.getMessageAllInfo();
                break;
        }
        message = message + "}";
        return message;
    }

    private String setType(int type)
    {
        return "\"type\": " + type + ",";
    }

    private String getMessagePkCreateUserId(int userId)
    {
        String addMessage = "";
        int roomId = 0;
        if (userId != 0)
        {
            roomId = 1;
        }
        addMessage += "\"userId\": " + userId + ", ";
        addMessage += "\"roomId\": " + roomId;
        return addMessage;
    }

    private String getMessagePkLogin(int userId) {
        String addMessage = "";
        int roomId = 0;
        int playerNum = 0;
        if (userId != 0) {
            // 昇順で待機者がいる部屋に入る
            for (int i = 1; i <= MAX_ROOM_COUNT; i++) {
                if (roomId != 0) {
                    break;
                }

                // 空室は一旦無視
                if (!_roomUsers.containsKey(i)) {
                    continue;
                }

                if (_roomUsers.get(i).size() >= USER_MAX_COUNT) {
                    continue;
                }

                roomId = i;
                playerNum = _roomUsers.get(i).size();
                _roomUsers.get(i).add(userId);
                _userRooms.put(userId, roomId);
                System.out.println("room(" + roomId + ") is waiting. enter userId(" + userId + ")");
                break;
            }
            if (roomId == 0)
            {
                // 昇順で空いている部屋に入る
                for (int i = 1; i <= MAX_ROOM_COUNT; i++) {
                    if (!_roomUsers.containsKey(i)) {
                        roomId = i;
                        playerNum = 0;
                        List<Integer> list = new ArrayList<>();
                        list.add(userId);
                        _roomUsers.put(roomId, list);
                        _userRooms.put(userId, roomId);
                        System.out.println("room(" + roomId + ") is empty. enter userId(" + userId + ")");
                        break;
                    }
                }
            }
            if (_roomUsers.get(roomId).size() == USER_MAX_COUNT)
            {
                _readyRooms.add(roomId);
                Field field = new Field(roomId, _roomUsers.get(roomId));
                _fields.put(roomId, field);
            }
        }
        addMessage += "\"roomId\": " + roomId + ", ";
        addMessage += "\"playerNum\": " + playerNum;
        return addMessage;
    }

    private void actionPkCloseWebsocket(int userId)
    {
        int roomId = _userRooms.get(userId);
        List<Integer> users = _roomUsers.get(roomId);
        for (int i = 0; i < users.size(); i++)
        {
            if (users.get(i) == userId)
            {
                users.remove(i); // 人が減ったことを通知させた方が良い？
                break;
            }
        }
        if (users.size() <= 0)
        {
            _roomUsers.remove(roomId);
        }
        _userRooms.remove(userId);
    }

    private void actionPkUpdateCards(int userId)
    {
        String addMessage = "";
        addMessage += "\"fieldCardNums\": [1,1,1,1], ";
        addMessage += "\"playerCardNums\": [[1,1,1,1]], ";
        addMessage += "\"playerUserIds\": []";
    }
    // PkActionここまで

    public class Field
    {
        int _roomId;
        List<Integer> _fieldCardNums;
        List<List<Integer>> _playerCardNums;
        List<Integer> _playerUserIds;
        List<Integer> _playerCardCounts;

        private boolean _existAvailableCard = true;

        public Field (int roomId, List<Integer> playerList)
        {
            _roomId = roomId;
            boolean initEnd = false;
            int maxLoopCount = 100;
            while (!initEnd && maxLoopCount > 0)
            {
                initCards(playerList);
                if (existAvailableCard())
                {
                    initEnd = true;
                }
                maxLoopCount--;
            }
            initCardCounts();
        }

        private void initCards(List<Integer> playerList)
        {
            _fieldCardNums = new ArrayList<>();
            _playerCardNums = new ArrayList<>();
            _playerUserIds = new ArrayList<>();
            for (int i = 0; i < CARD_MAX_COUNT; i++)
            {
                _fieldCardNums.add(createCardNum());
            }
            for  (int i = 0; i < USER_MAX_COUNT; i++)
            {
                List<Integer> cardNums = new ArrayList<>();
                _playerCardNums.add(cardNums);
                for (int j = 0; j < CARD_MAX_COUNT; j++)
                {
                    _playerCardNums.get(i).add(createCardNum());
                }
                _playerUserIds.add(playerList.get(i));
            }
        }

        private void initCardCounts()
        {
            _playerCardCounts = new ArrayList<>();
            for (int i = 0; i < USER_MAX_COUNT; i++)
            {
                _playerCardCounts.add(MAX_CARD_COUNT);
            }
        }

        public void setPlayerUserIds(int playerNum, int userId)
        {
            _playerUserIds.set(playerNum, userId);
            System.out.println("setPlayerUserids(" + playerNum + "->" + _playerUserIds.get(playerNum) + ")");
        }

        public void setPlayerCardNum(int playerNum, int index, int cardNum)
        {
            _playerCardNums.get(playerNum).set(index, cardNum);
            System.out.println("setPlayerCardNum(" + playerNum + "->" + index + "->" + _playerCardNums.get(playerNum).get(index) + ")");
        }

        public String getMessageAllInfo()
        {
            String message = "";
            JsonArray arr = new JsonArray();
            for (int i = 0; i < _fieldCardNums.size(); i++)
            {
                arr.add(_fieldCardNums.get(i));
            }
            message = addMessage(message,"fieldCardNums", arr.toString());

            arr = new JsonArray();
            JsonArray miniArr = new JsonArray();
            for (int i = 0; i < _playerCardNums.size(); i++)
            {
                for (int j = 0; j < _playerCardNums.get(i).size(); j++)
                {
                    miniArr.add(_playerCardNums.get(i).get(j));
                }
                arr.add(miniArr);
                miniArr = new JsonArray();
            }
            message = addMessage(message,"playerCardNums", arr.toString());

            arr = new JsonArray();
            for (int i = 0; i < _playerUserIds.size(); i++)
            {
                arr.add(_playerUserIds.get(i));
            }
            message = addMessage(message,"playerUserIds", arr.toString());
            message = addMessage(message,"existAvailableCard", String.valueOf(getExistAvailableCard()));

            arr = new JsonArray();
            for (int i = 0; i < _playerCardCounts.size(); i++)
            {
                arr.add(_playerCardCounts.get(i));
            }
            message = addMessageEnd(message, "playerCardCounts", arr.toString());
            return message;
        }

        private String addMessage(String beforeMessage, String key, String value)
        {
            return beforeMessage + "\"" + key + "\": " + value + ", ";
        }

        private String addMessageEnd(String beforeMessage, String key, String value)
        {
            return beforeMessage + "\"" + key + "\": " + value;
        }

        public boolean getExistAvailableCard()
        {
            return _existAvailableCard;
        }

        public void updateCards(int playerNum, int fieldIdx, int selectingIdx)
        {
            System.out.println("playerNum(" + playerNum + "), fieldIdx(" + fieldIdx + "), selectingIdx(" + selectingIdx + ")");

            int fieldCardNum = _fieldCardNums.get(fieldIdx);
            int playerCardNum = _playerCardNums.get(playerNum).get(selectingIdx);

            if (!checkChangeCard(fieldCardNum, playerCardNum))
            {
                _existAvailableCard = existAvailableCard();
                return;
            }

            if (_playerCardCounts.contains(0))
            {
                return;
            }
            int newCardNum = _playerCardCounts.get(playerNum) <= CARD_MAX_COUNT ? 0 : createCardNum();
            _fieldCardNums.set(fieldIdx, playerCardNum);
            _playerCardNums.get(playerNum).set(selectingIdx, newCardNum);
            _playerCardCounts.set(playerNum, _playerCardCounts.get(playerNum) - 1);

            _existAvailableCard = existAvailableCard();
        }

        private boolean checkChangeCard(int targetCard, int selectingCard)
        {
            if (targetCard == selectingCard + 1 || targetCard == selectingCard - 1)
            {
                return true;
            }

            // 1とmaxの時
            if (Math.abs(targetCard - selectingCard) == CARD_TYPE_COUNT - 2)
            {
                return true;
            }

            return false;
        }

        private int createCardNum()
        {
            if (_isDebug)
            {
                return 1;
            }

            int randNum = getRandomInt(1, CARD_TYPE_COUNT - 1);
            if (randNum < 1 || randNum > CARD_TYPE_COUNT - 1)
            {
                System.out.println("invalid range randNum(" + randNum + ")");
            }
            return randNum;
        }

        private boolean existAvailableCard()
        {
            // フィールドのカードとチェンジできるカードの一覧を取得
            List<Integer> availableCards = new ArrayList<>();
            for (int num : _fieldCardNums)
            {
                for (int i = 1; i < CARD_TYPE_COUNT; i++)
                {
                    if (checkChangeCard(num, i))
                    {
                        //System.out.println("available card num(" + i + ")");
                        availableCards.add(i);
                    }
                }
            }

            for (int i = 0; i < _playerCardNums.size(); i++)
            {
                for (int num : _playerCardNums.get(i))
                {
                    if (availableCards.contains(num))
                    {
                        //System.out.println("available card match num(" + i + ")");
                        return true;
                    }
                }
            }

            return false;
        }

        public void forceChange()
        {
            randomChangeFieldCard();
            _existAvailableCard = existAvailableCard();
        }

        private void randomChangeFieldCard()
        {
            while (true)
            {
                int randFieldIndex = getRandomInt(0, CARD_MAX_COUNT - 1);
                int beforeCardNum = _fieldCardNums.get(randFieldIndex);
                int afterCardNum = getRandomInt(1, CARD_TYPE_COUNT - 1);
                _fieldCardNums.set(randFieldIndex, afterCardNum);
                System.out.println("idx(" + randFieldIndex + ")," + "before(" + beforeCardNum + ")," + "after(" + afterCardNum + ")," + "res(" + _fieldCardNums.get(randFieldIndex) + ")");
                if (existAvailableCard())
                {
                    break;
                }
                _fieldCardNums.set(randFieldIndex, beforeCardNum);
                System.out.println("No Change. idx(" + randFieldIndex + ")," + "before(" + beforeCardNum + ")," + "after(" + afterCardNum + ")," + "res(" + _fieldCardNums.get(randFieldIndex) + ")");
            }
        }
    }
    // FieldClassここまで

    public static Random getRandom()
    {
        if (_rand == null)
        {
            _rand = new Random();
        }
        return _rand;
    }

    public static int getRandomInt(int min, int max)
    {
        return getRandom().nextInt(max) + min;
    }
}