﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserControllerBase
struct  UserControllerBase_t1251906990  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 UserControllerBase::_selectingCardIndex
	int32_t ____selectingCardIndex_2;
	// System.Int32[] UserControllerBase::_userCards
	Int32U5BU5D_t3030399641* ____userCards_3;
	// System.Int32 UserControllerBase::_deckCount
	int32_t ____deckCount_4;

public:
	inline static int32_t get_offset_of__selectingCardIndex_2() { return static_cast<int32_t>(offsetof(UserControllerBase_t1251906990, ____selectingCardIndex_2)); }
	inline int32_t get__selectingCardIndex_2() const { return ____selectingCardIndex_2; }
	inline int32_t* get_address_of__selectingCardIndex_2() { return &____selectingCardIndex_2; }
	inline void set__selectingCardIndex_2(int32_t value)
	{
		____selectingCardIndex_2 = value;
	}

	inline static int32_t get_offset_of__userCards_3() { return static_cast<int32_t>(offsetof(UserControllerBase_t1251906990, ____userCards_3)); }
	inline Int32U5BU5D_t3030399641* get__userCards_3() const { return ____userCards_3; }
	inline Int32U5BU5D_t3030399641** get_address_of__userCards_3() { return &____userCards_3; }
	inline void set__userCards_3(Int32U5BU5D_t3030399641* value)
	{
		____userCards_3 = value;
		Il2CppCodeGenWriteBarrier(&____userCards_3, value);
	}

	inline static int32_t get_offset_of__deckCount_4() { return static_cast<int32_t>(offsetof(UserControllerBase_t1251906990, ____deckCount_4)); }
	inline int32_t get__deckCount_4() const { return ____deckCount_4; }
	inline int32_t* get_address_of__deckCount_4() { return &____deckCount_4; }
	inline void set__deckCount_4(int32_t value)
	{
		____deckCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
