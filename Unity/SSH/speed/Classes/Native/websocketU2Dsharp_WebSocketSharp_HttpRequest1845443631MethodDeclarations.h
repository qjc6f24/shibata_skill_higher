﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HttpRequest
struct HttpRequest_t1845443631;
// System.String
struct String_t;
// System.Version
struct Version_t1755874712;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Uri
struct Uri_t19570940;
// WebSocketSharp.HttpResponse
struct HttpResponse_t2820540315;
// System.IO.Stream
struct Stream_t3255436806;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t4248997468;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Version1755874712.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieCollect4248997468.h"

// System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HttpRequest__ctor_m3245535211 (HttpRequest_t1845443631 * __this, String_t* ___method0, String_t* ___uri1, Version_t1755874712 * ___version2, NameValueCollection_t3047564564 * ___headers3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String)
extern "C"  void HttpRequest__ctor_m1352921265 (HttpRequest_t1845443631 * __this, String_t* ___method0, String_t* ___uri1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateConnectRequest(System.Uri)
extern "C"  HttpRequest_t1845443631 * HttpRequest_CreateConnectRequest_m1662457562 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateWebSocketRequest(System.Uri)
extern "C"  HttpRequest_t1845443631 * HttpRequest_CreateWebSocketRequest_m3478151593 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpResponse WebSocketSharp.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern "C"  HttpResponse_t2820540315 * HttpRequest_GetResponse_m12496573 (HttpRequest_t1845443631 * __this, Stream_t3255436806 * ___stream0, int32_t ___millisecondsTimeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HttpRequest::SetCookies(WebSocketSharp.Net.CookieCollection)
extern "C"  void HttpRequest_SetCookies_m2179182154 (HttpRequest_t1845443631 * __this, CookieCollection_t4248997468 * ___cookies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HttpRequest::ToString()
extern "C"  String_t* HttpRequest_ToString_m3682586646 (HttpRequest_t1845443631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
