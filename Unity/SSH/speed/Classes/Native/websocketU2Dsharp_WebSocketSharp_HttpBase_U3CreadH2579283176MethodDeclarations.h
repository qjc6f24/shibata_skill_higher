﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0
struct U3CreadHeadersU3Ec__AnonStorey0_t2579283176;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::.ctor()
extern "C"  void U3CreadHeadersU3Ec__AnonStorey0__ctor_m4138196225 (U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::<>m__0(System.Int32)
extern "C"  void U3CreadHeadersU3Ec__AnonStorey0_U3CU3Em__0_m2437845731 (U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
