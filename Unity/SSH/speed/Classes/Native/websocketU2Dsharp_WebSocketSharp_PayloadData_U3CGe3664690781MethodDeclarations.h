﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0
struct U3CGetEnumeratorU3Ec__Iterator0_t3664690781;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3705914950 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4215007638 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m1567718122 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1400723954 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3392630107 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2914319157 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
