﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BattleController
struct BattleController_t3050984398;
// EnemyController
struct EnemyController_t2146768720;
// System.Object
struct Il2CppObject;
// CardController
struct CardController_t2595996862;
// TimeController
struct TimeController_t1641888515;
// Card
struct Card_t34057406;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayerController
struct PlayerController_t4148409433;
// WebSocketClient
struct WebSocketClient_t3274474108;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// ItemMaster
struct ItemMaster_t335070819;
// MasterBase/Base
struct Base_t3850954593;
// System.String
struct String_t;
// ItemMaster/Item
struct Item_t1836005089;
// MasterBase
struct MasterBase_t2623394359;
// PanelController
struct PanelController_t24792858;
// Pk
struct Pk_t2738205061;
// JSONObject
struct JSONObject_t1971882247;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// TestController
struct TestController_t674116116;
// TimeText
struct TimeText_t2509642322;
// UnityEngine.UI.Text
struct Text_t356221433;
// UserControllerBase
struct UserControllerBase_t1251906990;
// System.EventArgs
struct EventArgs_t3289624707;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t2890051726;
// WebSocketSharp.ErrorEventArgs
struct ErrorEventArgs_t502222999;
// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t344507773;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_BattleController3050984398.h"
#include "AssemblyU2DCSharp_BattleController3050984398MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_EnemyController2146768720.h"
#include "AssemblyU2DCSharp_CardController2595996862.h"
#include "AssemblyU2DCSharp_TimeController1641888515.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharp_TimeController1641888515MethodDeclarations.h"
#include "AssemblyU2DCSharp_Card34057406.h"
#include "AssemblyU2DCSharp_Card34057406MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_PlayerController4148409433.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserControllerBase1251906990MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardController2595996862MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2427073286MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2427073286.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketClient3274474108MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketClient3274474108.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Bounds3033363703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"
#include "AssemblyU2DCSharp_EnemyController2146768720MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "AssemblyU2DCSharp_ItemMaster335070819.h"
#include "AssemblyU2DCSharp_ItemMaster335070819MethodDeclarations.h"
#include "AssemblyU2DCSharp_MasterBase2623394359MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946MethodDeclarations.h"
#include "AssemblyU2DCSharp_MasterBase2623394359.h"
#include "AssemblyU2DCSharp_MasterBase_Base3850954593.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946.h"
#include "AssemblyU2DCSharp_ItemMaster_Item1836005089.h"
#include "AssemblyU2DCSharp_ItemMaster_Item1836005089MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "AssemblyU2DCSharp_MasterBase_Base3850954593MethodDeclarations.h"
#include "AssemblyU2DCSharp_PanelController24792858.h"
#include "AssemblyU2DCSharp_PanelController24792858MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pk2738205061.h"
#include "AssemblyU2DCSharp_Pk2738205061MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_PlayerController4148409433MethodDeclarations.h"
#include "AssemblyU2DCSharp_TestController674116116.h"
#include "AssemblyU2DCSharp_TestController674116116MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "AssemblyU2DCSharp_TimeText2509642322.h"
#include "AssemblyU2DCSharp_TimeText2509642322MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserControllerBase1251906990.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket3268376029MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket3268376029.h"
#include "mscorlib_System_EventHandler277755526MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen1481358898MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen3388497467MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen3230782241MethodDeclarations.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726.h"
#include "mscorlib_System_EventHandler_1_gen1481358898.h"
#include "mscorlib_System_EventHandler_1_gen3388497467.h"
#include "websocketU2Dsharp_WebSocketSharp_ErrorEventArgs502222999.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseEventArgs344507773.h"
#include "mscorlib_System_EventHandler_1_gen3230782241.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_ErrorEventArgs502222999MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<EnemyController>()
#define GameObject_GetComponent_TisEnemyController_t2146768720_m543378449(__this, method) ((  EnemyController_t2146768720 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CardController>()
#define GameObject_GetComponent_TisCardController_t2595996862_m969971575(__this, method) ((  CardController_t2595996862 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<TimeController>()
#define Component_GetComponent_TisTimeController_t1641888515_m710209926(__this, method) ((  TimeController_t1641888515 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerController>()
#define GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(__this, method) ((  PlayerController_t4148409433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<BattleController>()
#define GameObject_GetComponent_TisBattleController_t3050984398_m2676041633(__this, method) ((  BattleController_t3050984398 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<WebSocketClient>()
#define GameObject_GetComponent_TisWebSocketClient_t3274474108_m2540791475(__this, method) ((  WebSocketClient_t3274474108 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CardController>()
#define Component_GetComponent_TisCardController_t2595996862_m71764587(__this, method) ((  CardController_t2595996862 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Card>()
#define GameObject_GetComponent_TisCard_t34057406_m3168222211(__this, method) ((  Card_t34057406 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m3451544451(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<MasterBase/Base>(System.String)
#define JsonUtility_FromJson_TisBase_t3850954593_m232411544(__this /* static, unused */, p0, method) ((  Base_t3850954593 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<ItemMaster/Item>(System.String)
#define JsonUtility_FromJson_TisItem_t1836005089_m3155834584(__this /* static, unused */, p0, method) ((  Item_t1836005089 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m327292296(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<TimeText>()
#define GameObject_GetComponentInChildren_TisTimeText_t2509642322_m415022099(__this, method) ((  TimeText_t2509642322 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BattleController::.ctor()
extern "C"  void BattleController__ctor_m1953836025 (BattleController_t3050984398 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BattleController::Start()
extern const MethodInfo* GameObject_GetComponent_TisEnemyController_t2146768720_m543378449_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCardController_t2595996862_m969971575_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTimeController_t1641888515_m710209926_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2874847238;
extern Il2CppCodeGenString* _stringLiteral3324075380;
extern const uint32_t BattleController_Start_m1127653241_MetadataUsageId;
extern "C"  void BattleController_Start_m1127653241 (BattleController_t3050984398 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BattleController_Start_m1127653241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2874847238, /*hidden argument*/NULL);
		NullCheck(L_0);
		EnemyController_t2146768720 * L_1 = GameObject_GetComponent_TisEnemyController_t2146768720_m543378449(L_0, /*hidden argument*/GameObject_GetComponent_TisEnemyController_t2146768720_m543378449_MethodInfo_var);
		__this->set__enemyController_2(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3324075380, /*hidden argument*/NULL);
		NullCheck(L_2);
		CardController_t2595996862 * L_3 = GameObject_GetComponent_TisCardController_t2595996862_m969971575(L_2, /*hidden argument*/GameObject_GetComponent_TisCardController_t2595996862_m969971575_MethodInfo_var);
		__this->set__cardController_3(L_3);
		TimeController_t1641888515 * L_4 = Component_GetComponent_TisTimeController_t1641888515_m710209926(__this, /*hidden argument*/Component_GetComponent_TisTimeController_t1641888515_m710209926_MethodInfo_var);
		__this->set__timeController_4(L_4);
		return;
	}
}
// System.Void BattleController::Update()
extern "C"  void BattleController_Update_m3719881328 (BattleController_t3050984398 * __this, const MethodInfo* method)
{
	{
		bool L_0 = BattleController_checkBattleEnded_m846999951(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = BattleController_checkBattleStarted_m639244660(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean BattleController::checkBattleStarted()
extern "C"  bool BattleController_checkBattleStarted_m639244660 (BattleController_t3050984398 * __this, const MethodInfo* method)
{
	{
		TimeController_t1641888515 * L_0 = __this->get__timeController_4();
		NullCheck(L_0);
		bool L_1 = TimeController_isBattleStarted_m1090798863(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean BattleController::checkBattleEnded()
extern "C"  bool BattleController_checkBattleEnded_m846999951 (BattleController_t3050984398 * __this, const MethodInfo* method)
{
	{
		TimeController_t1641888515 * L_0 = __this->get__timeController_4();
		NullCheck(L_0);
		bool L_1 = TimeController_isBattleEnded_m2521904612(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Card::.ctor()
extern "C"  void Card__ctor_m586349219 (Card_t34057406 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Card::initialize()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCardController_t2595996862_m969971575_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBattleController_t3050984398_m2676041633_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral581520655;
extern Il2CppCodeGenString* _stringLiteral3324075380;
extern Il2CppCodeGenString* _stringLiteral3425671801;
extern const uint32_t Card_initialize_m3158878599_MetadataUsageId;
extern "C"  void Card_initialize_m3158878599 (Card_t34057406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Card_initialize_m3158878599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_cursor_5();
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m1033555130(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_cursorObject_6(L_5);
		GameObject_t1756533147 * L_6 = __this->get_cursor_5();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_cursorObject_6();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral581520655, /*hidden argument*/NULL);
		NullCheck(L_8);
		PlayerController_t4148409433 * L_9 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_8, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_MethodInfo_var);
		__this->set__playerController_7(L_9);
		GameObject_t1756533147 * L_10 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3324075380, /*hidden argument*/NULL);
		NullCheck(L_10);
		CardController_t2595996862 * L_11 = GameObject_GetComponent_TisCardController_t2595996862_m969971575(L_10, /*hidden argument*/GameObject_GetComponent_TisCardController_t2595996862_m969971575_MethodInfo_var);
		__this->set__cardController_8(L_11);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3425671801, /*hidden argument*/NULL);
		NullCheck(L_12);
		BattleController_t3050984398 * L_13 = GameObject_GetComponent_TisBattleController_t3050984398_m2676041633(L_12, /*hidden argument*/GameObject_GetComponent_TisBattleController_t3050984398_m2676041633_MethodInfo_var);
		__this->set__battleController_9(L_13);
		return;
	}
}
// System.Void Card::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit_t87180320_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Card_Update_m4272690700_MetadataUsageId;
extern "C"  void Card_Update_m4272690700 (Card_t34057406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Card_Update_m4272690700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Ray_t2469606224  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RaycastHit_t87180320  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00da;
		}
	}
	{
		BattleController_t3050984398 * L_1 = __this->get__battleController_9();
		NullCheck(L_1);
		bool L_2 = BattleController_checkBattleStarted_m639244660(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00da;
		}
	}
	{
		BattleController_t3050984398 * L_3 = __this->get__battleController_9();
		NullCheck(L_3);
		bool L_4 = BattleController_checkBattleEnded_m846999951(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_00da;
		}
	}
	{
		V_0 = (100.0f);
		Camera_t189460977 * L_5 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_6 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Ray_t2469606224  L_7 = Camera_ScreenPointToRay_m614889538(L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Initobj (RaycastHit_t87180320_il2cpp_TypeInfo_var, (&V_2));
		Ray_t2469606224  L_8 = V_1;
		float L_9 = V_0;
		bool L_10 = Physics_Raycast_m2308457076(NULL /*static, unused*/, L_8, (&V_2), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00da;
		}
	}
	{
		Collider_t3497673348 * L_11 = RaycastHit_get_collider_m301198172((&V_2), /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00da;
		}
	}
	{
		int32_t L_15 = __this->get_ownTargetNum_2();
		if (L_15)
		{
			goto IL_0094;
		}
	}
	{
		PlayerController_t4148409433 * L_16 = __this->get__playerController_7();
		int32_t L_17 = __this->get_ownIndex_3();
		NullCheck(L_16);
		UserControllerBase_setSelectingCardIndex_m1941113010(L_16, L_17, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_0094:
	{
		int32_t L_18 = __this->get_ownTargetNum_2();
		if ((!(((uint32_t)L_18) == ((uint32_t)4))))
		{
			goto IL_00da;
		}
	}
	{
		PlayerController_t4148409433 * L_19 = __this->get__playerController_7();
		NullCheck(L_19);
		int32_t L_20 = UserControllerBase_getSelectingCardIndex_m2205353237(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_20) > ((int32_t)3)))
		{
			goto IL_00da;
		}
	}
	{
		PlayerController_t4148409433 * L_21 = __this->get__playerController_7();
		NullCheck(L_21);
		int32_t L_22 = UserControllerBase_getUserCard_m352162031(L_21, (-1), /*hidden argument*/NULL);
		V_3 = L_22;
		CardController_t2595996862 * L_23 = __this->get__cardController_8();
		int32_t L_24 = __this->get_ownIndex_3();
		PlayerController_t4148409433 * L_25 = __this->get__playerController_7();
		NullCheck(L_25);
		int32_t L_26 = UserControllerBase_getSelectingCardIndex_m2205353237(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		CardController_checkChangeCards_m2291941626(L_23, L_24, L_26, /*hidden argument*/NULL);
	}

IL_00da:
	{
		int32_t L_27 = __this->get_ownTargetNum_2();
		if (L_27)
		{
			goto IL_00eb;
		}
	}
	{
		Card_checkCursor_m1602530015(__this, /*hidden argument*/NULL);
	}

IL_00eb:
	{
		return;
	}
}
// System.Void Card::checkCursor()
extern "C"  void Card_checkCursor_m1602530015 (Card_t34057406 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		PlayerController_t4148409433 * L_0 = __this->get__playerController_7();
		NullCheck(L_0);
		int32_t L_1 = UserControllerBase_getSelectingCardIndex_m2205353237(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_ownIndex_3();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_4 = __this->get_cursorObject_6();
		NullCheck(L_4);
		bool L_5 = GameObject_get_active_m1672294863(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_6 = __this->get_cursorObject_6();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_003b:
	{
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_cursorObject_6();
		NullCheck(L_8);
		bool L_9 = GameObject_get_active_m1672294863(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		GameObject_t1756533147 * L_10 = __this->get_cursorObject_6();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// UnityEngine.GameObject Card::getCursorObject()
extern "C"  GameObject_t1756533147 * Card_getCursorObject_m4179028025 (Card_t34057406 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_cursorObject_6();
		return L_0;
	}
}
// System.Void Card::destroyCursorObject()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Card_destroyCursorObject_m2056992626_MetadataUsageId;
extern "C"  void Card_destroyCursorObject_m2056992626 (Card_t34057406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Card_destroyCursorObject_m2056992626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_cursorObject_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m2343493981(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardController::.ctor()
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2427073286_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m139868023_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2467428667_MethodInfo_var;
extern const uint32_t CardController__ctor_m1278714583_MetadataUsageId;
extern "C"  void CardController__ctor_m1278714583 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController__ctor_m1278714583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2427073286 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_CARD_TYPE_COUNT_16();
		__this->set_cards_19(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_0+(int32_t)1)))));
		List_1_t2427073286 * L_1 = (List_1_t2427073286 *)il2cpp_codegen_object_new(List_1_t2427073286_il2cpp_TypeInfo_var);
		List_1__ctor_m139868023(L_1, /*hidden argument*/List_1__ctor_m139868023_MethodInfo_var);
		V_0 = L_1;
		List_1_t2427073286 * L_2 = V_0;
		GameObjectU5BU5D_t3057952154* L_3 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get__playerObjects_21();
		NullCheck(L_2);
		List_1_Add_m2467428667(L_2, L_3, /*hidden argument*/List_1_Add_m2467428667_MethodInfo_var);
		List_1_t2427073286 * L_4 = V_0;
		GameObjectU5BU5D_t3057952154* L_5 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get__enemyObjects1_22();
		NullCheck(L_4);
		List_1_Add_m2467428667(L_4, L_5, /*hidden argument*/List_1_Add_m2467428667_MethodInfo_var);
		List_1_t2427073286 * L_6 = V_0;
		GameObjectU5BU5D_t3057952154* L_7 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get__enemyObjects2_23();
		NullCheck(L_6);
		List_1_Add_m2467428667(L_6, L_7, /*hidden argument*/List_1_Add_m2467428667_MethodInfo_var);
		List_1_t2427073286 * L_8 = V_0;
		GameObjectU5BU5D_t3057952154* L_9 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get__enemyObjects3_24();
		NullCheck(L_8);
		List_1_Add_m2467428667(L_8, L_9, /*hidden argument*/List_1_Add_m2467428667_MethodInfo_var);
		List_1_t2427073286 * L_10 = V_0;
		GameObjectU5BU5D_t3057952154* L_11 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get__fieldObjects_20();
		NullCheck(L_10);
		List_1_Add_m2467428667(L_10, L_11, /*hidden argument*/List_1_Add_m2467428667_MethodInfo_var);
		List_1_t2427073286 * L_12 = V_0;
		__this->set__objects_25(L_12);
		int32_t L_13 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_PHASE_INIT_26();
		__this->set__phase_28(L_13);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardController::Start()
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisWebSocketClient_t3274474108_m2540791475_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCardController_t2595996862_m71764587_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral581520655;
extern Il2CppCodeGenString* _stringLiteral40213808;
extern const uint32_t CardController_Start_m1936395595_MetadataUsageId;
extern "C"  void CardController_Start_m1936395595 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_Start_m1936395595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t189460977 * L_0 = __this->get_mainCamera_13();
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_2 = Camera_ScreenToWorldPoint_m929392728(L_0, L_1, /*hidden argument*/NULL);
		__this->set__topLeft_8(L_2);
		Camera_t189460977 * L_3 = __this->get_mainCamera_13();
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, (((float)((float)L_4))), (((float)((float)L_5))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_7 = Camera_ScreenToWorldPoint_m929392728(L_3, L_6, /*hidden argument*/NULL);
		__this->set__bottomRight_9(L_7);
		Vector3_t2243707580 * L_8 = __this->get_address_of__bottomRight_9();
		float L_9 = L_8->get_x_1();
		Vector3_t2243707580 * L_10 = __this->get_address_of__topLeft_8();
		float L_11 = L_10->get_x_1();
		__this->set__screenWidth_10(((float)((float)L_9-(float)L_11)));
		Vector3_t2243707580 * L_12 = __this->get_address_of__topLeft_8();
		float L_13 = L_12->get_y_2();
		Vector3_t2243707580 * L_14 = __this->get_address_of__bottomRight_9();
		float L_15 = L_14->get_y_2();
		__this->set__screenHeight_11(((float)((float)L_13-(float)L_15)));
		GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral581520655, /*hidden argument*/NULL);
		NullCheck(L_16);
		PlayerController_t4148409433 * L_17 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_16, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_MethodInfo_var);
		__this->set__playerController_14(L_17);
		GameObject_t1756533147 * L_18 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral40213808, /*hidden argument*/NULL);
		NullCheck(L_18);
		WebSocketClient_t3274474108 * L_19 = GameObject_GetComponent_TisWebSocketClient_t3274474108_m2540791475(L_18, /*hidden argument*/GameObject_GetComponent_TisWebSocketClient_t3274474108_m2540791475_MethodInfo_var);
		__this->set__webSocketClient_15(L_19);
		WebSocketClient_t3274474108 * L_20 = __this->get__webSocketClient_15();
		CardController_t2595996862 * L_21 = Component_GetComponent_TisCardController_t2595996862_m71764587(__this, /*hidden argument*/Component_GetComponent_TisCardController_t2595996862_m71764587_MethodInfo_var);
		NullCheck(L_20);
		WebSocketClient_setCardController_m1671294365(L_20, L_21, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		float L_24 = (&V_0)->get_x_1();
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = Transform_get_position_m1104419803(L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		float L_27 = (&V_1)->get_y_2();
		Vector3_t2243707580  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2638739322(&L_28, L_24, L_27, (0.0f), /*hidden argument*/NULL);
		__this->set__centerPos_12(L_28);
		float L_29 = __this->get__screenWidth_10();
		float L_30 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 4, 0, 1, ((float)((float)((-L_29))/(float)(8.0f))), ((float)((float)((-L_30))/(float)(13.0f))), (0.2f), /*hidden argument*/NULL);
		float L_31 = __this->get__screenWidth_10();
		float L_32 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 4, 1, 2, ((float)((float)L_31/(float)(8.0f))), ((float)((float)((-L_32))/(float)(13.0f))), (0.2f), /*hidden argument*/NULL);
		float L_33 = __this->get__screenWidth_10();
		float L_34 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 4, 2, 3, ((float)((float)L_33/(float)(8.0f))), ((float)((float)L_34/(float)(13.0f))), (0.2f), /*hidden argument*/NULL);
		float L_35 = __this->get__screenWidth_10();
		float L_36 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 4, 3, 4, ((float)((float)((-L_35))/(float)(8.0f))), ((float)((float)L_36/(float)(13.0f))), (0.2f), /*hidden argument*/NULL);
		float L_37 = __this->get__screenWidth_10();
		float L_38 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 0, 0, 0, ((float)((float)((float)((float)((-L_37))/(float)(8.0f)))*(float)(3.0f))), ((float)((float)L_38/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_39 = __this->get__screenWidth_10();
		float L_40 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 0, 1, 0, ((float)((float)((-L_39))/(float)(8.0f))), ((float)((float)L_40/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_41 = __this->get__screenWidth_10();
		float L_42 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 0, 2, 0, ((float)((float)L_41/(float)(8.0f))), ((float)((float)L_42/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_43 = __this->get__screenWidth_10();
		float L_44 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 0, 3, 0, ((float)((float)((float)((float)L_43/(float)(8.0f)))*(float)(3.0f))), ((float)((float)L_44/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_45 = __this->get__screenWidth_10();
		float L_46 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 1, 0, 0, ((float)((float)((float)((float)L_45/(float)(10.0f)))*(float)(4.0f))), ((float)((float)((float)((float)L_46/(float)(18.0f)))*(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_47 = __this->get__screenWidth_10();
		float L_48 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 1, 1, 0, ((float)((float)((float)((float)L_47/(float)(10.0f)))*(float)(4.0f))), ((float)((float)L_48/(float)(18.0f))), (0.2f), /*hidden argument*/NULL);
		float L_49 = __this->get__screenWidth_10();
		float L_50 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 1, 2, 0, ((float)((float)((float)((float)L_49/(float)(10.0f)))*(float)(4.0f))), ((float)((float)((-L_50))/(float)(18.0f))), (0.2f), /*hidden argument*/NULL);
		float L_51 = __this->get__screenWidth_10();
		float L_52 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 1, 3, 0, ((float)((float)((float)((float)L_51/(float)(10.0f)))*(float)(4.0f))), ((float)((float)((float)((float)((-L_52))/(float)(18.0f)))*(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_53 = __this->get__screenWidth_10();
		float L_54 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 2, 0, 0, ((float)((float)((float)((float)L_53/(float)(10.0f)))*(float)(3.0f))), ((float)((float)((-L_54))/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_55 = __this->get__screenWidth_10();
		float L_56 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 2, 1, 0, ((float)((float)L_55/(float)(10.0f))), ((float)((float)((-L_56))/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_57 = __this->get__screenWidth_10();
		float L_58 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 2, 2, 0, ((float)((float)((-L_57))/(float)(10.0f))), ((float)((float)((-L_58))/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_59 = __this->get__screenWidth_10();
		float L_60 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 2, 3, 0, ((float)((float)((float)((float)((-L_59))/(float)(10.0f)))*(float)(3.0f))), ((float)((float)((-L_60))/(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_61 = __this->get__screenWidth_10();
		float L_62 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 3, 0, 0, ((float)((float)((float)((float)((-L_61))/(float)(10.0f)))*(float)(4.0f))), ((float)((float)((float)((float)((-L_62))/(float)(18.0f)))*(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		float L_63 = __this->get__screenWidth_10();
		float L_64 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 3, 1, 0, ((float)((float)((float)((float)((-L_63))/(float)(10.0f)))*(float)(4.0f))), ((float)((float)((-L_64))/(float)(18.0f))), (0.2f), /*hidden argument*/NULL);
		float L_65 = __this->get__screenWidth_10();
		float L_66 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 3, 2, 0, ((float)((float)((float)((float)((-L_65))/(float)(10.0f)))*(float)(4.0f))), ((float)((float)L_66/(float)(18.0f))), (0.2f), /*hidden argument*/NULL);
		float L_67 = __this->get__screenWidth_10();
		float L_68 = __this->get__screenHeight_11();
		CardController_instantiateCard_m1879922353(__this, 3, 3, 0, ((float)((float)((float)((float)((-L_67))/(float)(10.0f)))*(float)(4.0f))), ((float)((float)((float)((float)L_68/(float)(18.0f)))*(float)(3.0f))), (0.2f), /*hidden argument*/NULL);
		CardController_updateAllCards_m567226642(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_69 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_PHASE_PLAYING_27();
		__this->set__phase_28(L_69);
		return;
	}
}
// System.Void CardController::instantiateCard(System.Int32,System.Int32,System.Int32,System.Single,System.Single,System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3439907018_MethodInfo_var;
extern const uint32_t CardController_instantiateCard_m1879922353_MetadataUsageId;
extern "C"  void CardController_instantiateCard_m1879922353 (CardController_t2595996862 * __this, int32_t ___target0, int32_t ___index1, int32_t ___num2, float ___x3, float ___y4, float ___rate5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_instantiateCard_m1879922353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		int32_t L_0 = ___target0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___target0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		___rate5 = (0.1f);
	}

IL_0014:
	{
		GameObjectU5BU5D_t3057952154* L_2 = __this->get_cards_19();
		int32_t L_3 = ___num2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_t1756533147 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Vector3_t2243707580  L_6 = __this->get__centerPos_12();
		float L_7 = ___x3;
		float L_8 = ___y4;
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, L_7, L_8, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t4030073918  L_12 = Transform_get_rotation_m1033555130(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_13 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_5, L_10, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_13;
		GameObject_t1756533147 * L_14 = V_0;
		float L_15 = ___rate5;
		CardController_modifyScale_m3816870080(__this, L_14, L_15, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = V_0;
		NullCheck(L_16);
		Card_t34057406 * L_17 = GameObject_GetComponent_TisCard_t34057406_m3168222211(L_16, /*hidden argument*/GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var);
		NullCheck(L_17);
		Card_initialize_m3158878599(L_17, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = V_0;
		NullCheck(L_18);
		Card_t34057406 * L_19 = GameObject_GetComponent_TisCard_t34057406_m3168222211(L_18, /*hidden argument*/GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var);
		int32_t L_20 = ___target0;
		NullCheck(L_19);
		L_19->set_ownTargetNum_2(L_20);
		GameObject_t1756533147 * L_21 = V_0;
		NullCheck(L_21);
		Card_t34057406 * L_22 = GameObject_GetComponent_TisCard_t34057406_m3168222211(L_21, /*hidden argument*/GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var);
		int32_t L_23 = ___index1;
		NullCheck(L_22);
		L_22->set_ownIndex_3(L_23);
		GameObject_t1756533147 * L_24 = V_0;
		NullCheck(L_24);
		Card_t34057406 * L_25 = GameObject_GetComponent_TisCard_t34057406_m3168222211(L_24, /*hidden argument*/GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var);
		int32_t L_26 = ___num2;
		NullCheck(L_25);
		L_25->set_ownNum_4(L_26);
		GameObject_t1756533147 * L_27 = V_0;
		NullCheck(L_27);
		Card_t34057406 * L_28 = GameObject_GetComponent_TisCard_t34057406_m3168222211(L_27, /*hidden argument*/GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var);
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = Card_getCursorObject_m4179028025(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = GameObject_get_transform_m909382139(L_29, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = V_0;
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = GameObject_get_transform_m909382139(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_localScale_m3074381503(L_32, /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_33, (1.1f), /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_localScale_m2325460848(L_30, L_34, /*hidden argument*/NULL);
		List_1_t2427073286 * L_35 = __this->get__objects_25();
		int32_t L_36 = ___target0;
		NullCheck(L_35);
		GameObjectU5BU5D_t3057952154* L_37 = List_1_get_Item_m3439907018(L_35, L_36, /*hidden argument*/List_1_get_Item_m3439907018_MethodInfo_var);
		int32_t L_38 = ___index1;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		GameObject_t1756533147 * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		bool L_41 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_40, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00ec;
		}
	}
	{
		List_1_t2427073286 * L_42 = __this->get__objects_25();
		int32_t L_43 = ___target0;
		NullCheck(L_42);
		GameObjectU5BU5D_t3057952154* L_44 = List_1_get_Item_m3439907018(L_42, L_43, /*hidden argument*/List_1_get_Item_m3439907018_MethodInfo_var);
		int32_t L_45 = ___index1;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t1756533147 * L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Card_t34057406 * L_48 = GameObject_GetComponent_TisCard_t34057406_m3168222211(L_47, /*hidden argument*/GameObject_GetComponent_TisCard_t34057406_m3168222211_MethodInfo_var);
		NullCheck(L_48);
		Card_destroyCursorObject_m2056992626(L_48, /*hidden argument*/NULL);
		List_1_t2427073286 * L_49 = __this->get__objects_25();
		int32_t L_50 = ___target0;
		NullCheck(L_49);
		GameObjectU5BU5D_t3057952154* L_51 = List_1_get_Item_m3439907018(L_49, L_50, /*hidden argument*/List_1_get_Item_m3439907018_MethodInfo_var);
		int32_t L_52 = ___index1;
		NullCheck(L_51);
		int32_t L_53 = L_52;
		GameObject_t1756533147 * L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m2343493981(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
	}

IL_00ec:
	{
		List_1_t2427073286 * L_55 = __this->get__objects_25();
		int32_t L_56 = ___target0;
		NullCheck(L_55);
		GameObjectU5BU5D_t3057952154* L_57 = List_1_get_Item_m3439907018(L_55, L_56, /*hidden argument*/List_1_get_Item_m3439907018_MethodInfo_var);
		int32_t L_58 = ___index1;
		GameObject_t1756533147 * L_59 = V_0;
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (GameObject_t1756533147 *)L_59);
		int32_t L_60 = ___target0;
		if (L_60)
		{
			goto IL_010e;
		}
	}
	{
		PlayerController_t4148409433 * L_61 = __this->get__playerController_14();
		int32_t L_62 = ___index1;
		int32_t L_63 = ___num2;
		NullCheck(L_61);
		UserControllerBase_setUserCard_m1357951436(L_61, L_62, L_63, /*hidden argument*/NULL);
	}

IL_010e:
	{
		return;
	}
}
// System.Void CardController::modifyScale(UnityEngine.GameObject,System.Single)
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t CardController_modifyScale_m3816870080_MetadataUsageId;
extern "C"  void CardController_modifyScale_m3816870080 (CardController_t2595996862 * __this, GameObject_t1756533147 * ___gameObject0, float ___rate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_modifyScale_m3816870080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		NullCheck(L_1);
		Bounds_t3033363703  L_2 = Renderer_get_bounds_m3832626589(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t2243707580  L_3 = Bounds_get_size_m1728027642((&V_1), /*hidden argument*/NULL);
		V_2 = L_3;
		float L_4 = (&V_2)->get_x_1();
		V_0 = L_4;
		float L_5 = __this->get__screenWidth_10();
		float L_6 = ___rate1;
		float L_7 = V_0;
		V_3 = ((float)((float)((float)((float)L_5*(float)L_6))/(float)L_7));
		GameObject_t1756533147 * L_8 = ___gameObject0;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		float L_10 = V_3;
		float L_11 = V_3;
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2638739322(&L_12, L_10, L_11, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localScale_m2325460848(L_9, L_12, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 CardController::getCenterPos()
extern "C"  Vector3_t2243707580  CardController_getCenterPos_m1702691502 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get__centerPos_12();
		return L_0;
	}
}
// System.Void CardController::Update()
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern const uint32_t CardController_Update_m3525294472_MetadataUsageId;
extern "C"  void CardController_Update_m3525294472 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_Update_m3525294472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__phase_28();
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_1 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_PHASE_PLAYING_27();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0032;
		}
	}
	{
		WebSocketClient_t3274474108 * L_2 = __this->get__webSocketClient_15();
		NullCheck(L_2);
		bool L_3 = L_2->get_isChangedField_23();
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		CardController_updateAllCards_m567226642(__this, /*hidden argument*/NULL);
		WebSocketClient_t3274474108 * L_4 = __this->get__webSocketClient_15();
		NullCheck(L_4);
		L_4->set_isChangedField_23((bool)0);
	}

IL_0032:
	{
		return;
	}
}
// System.Void CardController::updateAllCards()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2160738083;
extern const uint32_t CardController_updateAllCards_m567226642_MetadataUsageId;
extern "C"  void CardController_updateAllCards_m567226642 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_updateAllCards_m567226642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2160738083, /*hidden argument*/NULL);
		CardController_updateFieldCards_m602347471(__this, /*hidden argument*/NULL);
		CardController_updateUsersCards_m2654522943(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardController::updateFieldCards()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1970855216;
extern const uint32_t CardController_updateFieldCards_m602347471_MetadataUsageId;
extern "C"  void CardController_updateFieldCards_m602347471 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_updateFieldCards_m602347471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1970855216, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0029;
	}

IL_0011:
	{
		int32_t L_0 = V_0;
		WebSocketClient_t3274474108 * L_1 = __this->get__webSocketClient_15();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = WebSocketClient_getFieldCardNum_m3900970582(L_1, L_2, /*hidden argument*/NULL);
		CardController_changeCard_m1158561212(__this, 4, L_0, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_6 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_FIELD_CARD_COUNT_18();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// System.Void CardController::updateUsersCards()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2889302432;
extern const uint32_t CardController_updateUsersCards_m2654522943_MetadataUsageId;
extern "C"  void CardController_updateUsersCards_m2654522943 (CardController_t2595996862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_updateUsersCards_m2654522943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2889302432, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0034;
	}

IL_0011:
	{
		int32_t L_0 = V_0;
		WebSocketClient_t3274474108 * L_1 = __this->get__webSocketClient_15();
		WebSocketClient_t3274474108 * L_2 = __this->get__webSocketClient_15();
		NullCheck(L_2);
		int32_t L_3 = WebSocketClient_getPlayerNum_m4203793634(L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		NullCheck(L_1);
		int32_t L_5 = WebSocketClient_getPlayerCardNum_m2837115496(L_1, L_3, L_4, /*hidden argument*/NULL);
		CardController_changeCard_m1158561212(__this, 0, L_0, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_8 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_USERS_CARD_COUNT_17();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0011;
		}
	}
	{
		V_1 = 1;
		WebSocketClient_t3274474108 * L_9 = __this->get__webSocketClient_15();
		WebSocketClient_t3274474108 * L_10 = __this->get__webSocketClient_15();
		NullCheck(L_10);
		int32_t L_11 = WebSocketClient_getPlayerNum_m4203793634(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_12 = WebSocketClient_getNextPlayerNum_m818655264(L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0099;
	}

IL_005d:
	{
		V_3 = 0;
		goto IL_007d;
	}

IL_0064:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = V_3;
		WebSocketClient_t3274474108 * L_15 = __this->get__webSocketClient_15();
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		NullCheck(L_15);
		int32_t L_18 = WebSocketClient_getPlayerCardNum_m2837115496(L_15, L_16, L_17, /*hidden argument*/NULL);
		CardController_changeCard_m1158561212(__this, L_13, L_14, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_21 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_USERS_CARD_COUNT_17();
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
		WebSocketClient_t3274474108 * L_23 = __this->get__webSocketClient_15();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		int32_t L_25 = WebSocketClient_getNextPlayerNum_m818655264(L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
	}

IL_0099:
	{
		int32_t L_26 = V_2;
		WebSocketClient_t3274474108 * L_27 = __this->get__webSocketClient_15();
		NullCheck(L_27);
		int32_t L_28 = WebSocketClient_getPlayerNum_m4203793634(L_27, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)L_28))))
		{
			goto IL_005d;
		}
	}
	{
		return;
	}
}
// System.Void CardController::changeCard(System.Int32,System.Int32,System.Int32)
extern const MethodInfo* List_1_get_Item_m3439907018_MethodInfo_var;
extern const uint32_t CardController_changeCard_m1158561212_MetadataUsageId;
extern "C"  void CardController_changeCard_m1158561212 (CardController_t2595996862 * __this, int32_t ___target0, int32_t ___index1, int32_t ___num2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_changeCard_m1158561212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		List_1_t2427073286 * L_0 = __this->get__objects_25();
		int32_t L_1 = ___target0;
		NullCheck(L_0);
		GameObjectU5BU5D_t3057952154* L_2 = List_1_get_Item_m3439907018(L_0, L_1, /*hidden argument*/List_1_get_Item_m3439907018_MethodInfo_var);
		int32_t L_3 = ___index1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_t1756533147 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = ___target0;
		int32_t L_8 = ___index1;
		int32_t L_9 = ___num2;
		Transform_t3275118058 * L_10 = V_0;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_x_1();
		Transform_t3275118058 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = (&V_2)->get_y_2();
		CardController_instantiateCard_m1879922353(__this, L_7, L_8, L_9, L_12, L_16, (0.2f), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CardController::checkChangeCard(System.Int32,System.Int32)
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern const uint32_t CardController_checkChangeCard_m2549392343_MetadataUsageId;
extern "C"  bool CardController_checkChangeCard_m2549392343 (Il2CppObject * __this /* static, unused */, int32_t ___selectingCard0, int32_t ___targetCard1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_checkChangeCard_m2549392343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___selectingCard0;
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = ___selectingCard0;
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_2 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_CARD_TYPE_COUNT_16();
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = ___targetCard1;
		int32_t L_4 = ___selectingCard0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)((int32_t)L_4+(int32_t)1)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_5 = ___targetCard1;
		int32_t L_6 = ___selectingCard0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_6-(int32_t)1))))))
		{
			goto IL_0026;
		}
	}

IL_0024:
	{
		return (bool)1;
	}

IL_0026:
	{
		return (bool)0;
	}

IL_0028:
	{
		int32_t L_7 = ___selectingCard0;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_8 = ___targetCard1;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_9 = ___targetCard1;
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_10 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_CARD_TYPE_COUNT_16();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0043;
		}
	}

IL_0041:
	{
		return (bool)1;
	}

IL_0043:
	{
		return (bool)0;
	}

IL_0045:
	{
		int32_t L_11 = ___targetCard1;
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_12 = ___targetCard1;
		IL2CPP_RUNTIME_CLASS_INIT(CardController_t2595996862_il2cpp_TypeInfo_var);
		int32_t L_13 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_CARD_TYPE_COUNT_16();
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)((int32_t)L_13-(int32_t)1))))))
		{
			goto IL_005b;
		}
	}

IL_0059:
	{
		return (bool)1;
	}

IL_005b:
	{
		return (bool)0;
	}
}
// System.Void CardController::checkChangeCards(System.Int32,System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1270222245;
extern Il2CppCodeGenString* _stringLiteral954867163;
extern Il2CppCodeGenString* _stringLiteral3662813838;
extern const uint32_t CardController_checkChangeCards_m2291941626_MetadataUsageId;
extern "C"  void CardController_checkChangeCards_m2291941626 (CardController_t2595996862 * __this, int32_t ___fieldIdx0, int32_t ___userSelectingIdx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController_checkChangeCards_m2291941626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		JSONObject_t1971882247 * L_0 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2218833806(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		WebSocketClient_t3274474108 * L_2 = __this->get__webSocketClient_15();
		NullCheck(L_2);
		int32_t L_3 = WebSocketClient_getPlayerNum_m4203793634(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_AddField_m1847913364(L_1, _stringLiteral1270222245, L_3, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_4 = V_0;
		int32_t L_5 = ___fieldIdx0;
		NullCheck(L_4);
		JSONObject_AddField_m1847913364(L_4, _stringLiteral954867163, L_5, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_6 = V_0;
		int32_t L_7 = ___userSelectingIdx1;
		NullCheck(L_6);
		JSONObject_AddField_m1847913364(L_6, _stringLiteral3662813838, L_7, /*hidden argument*/NULL);
		WebSocketClient_t3274474108 * L_8 = __this->get__webSocketClient_15();
		JSONObject_t1971882247 * L_9 = V_0;
		NullCheck(L_8);
		WebSocketClient_checkChangeCard_m1740291254(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardController::.cctor()
extern Il2CppClass* CardController_t2595996862_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern const uint32_t CardController__cctor_m506378808_MetadataUsageId;
extern "C"  void CardController__cctor_m506378808 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CardController__cctor_m506378808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set_CARD_TYPE_COUNT_16(6);
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set_USERS_CARD_COUNT_17(4);
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set_FIELD_CARD_COUNT_18(4);
		int32_t L_0 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_FIELD_CARD_COUNT_18();
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set__fieldObjects_20(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_USERS_CARD_COUNT_17();
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set__playerObjects_21(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		int32_t L_2 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_USERS_CARD_COUNT_17();
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set__enemyObjects1_22(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		int32_t L_3 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_USERS_CARD_COUNT_17();
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set__enemyObjects2_23(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = ((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->get_USERS_CARD_COUNT_17();
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set__enemyObjects3_24(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set_PHASE_INIT_26(0);
		((CardController_t2595996862_StaticFields*)CardController_t2595996862_il2cpp_TypeInfo_var->static_fields)->set_PHASE_PLAYING_27(1);
		return;
	}
}
// System.Void EnemyController::.ctor()
extern "C"  void EnemyController__ctor_m1153179309 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	{
		UserControllerBase__ctor_m345467335(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyController::Start()
extern "C"  void EnemyController_Start_m2470974037 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	{
		__this->set__timer_5((1.0f));
		return;
	}
}
// System.Void EnemyController::moveTimer()
extern "C"  void EnemyController_moveTimer_m3204164943 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__timer_5();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__timer_5(((float)((float)L_0-(float)L_1)));
		return;
	}
}
// System.Single EnemyController::getTimer()
extern "C"  float EnemyController_getTimer_m3600434872 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__timer_5();
		return L_0;
	}
}
// System.Void EnemyController::resetTimer(System.Single)
extern "C"  void EnemyController_resetTimer_m3737201724 (EnemyController_t2146768720 * __this, float ___resetTime0, const MethodInfo* method)
{
	{
		float L_0 = ___resetTime0;
		__this->set__timer_5(L_0);
		return;
	}
}
// System.Void ItemMaster::.ctor()
extern "C"  void ItemMaster__ctor_m2788594876 (ItemMaster_t335070819 * __this, const MethodInfo* method)
{
	{
		MasterBase__ctor_m2050107174(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ItemMaster::Start()
extern const MethodInfo* JsonUtility_FromJson_TisBase_t3850954593_m232411544_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisItem_t1836005089_m3155834584_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t ItemMaster_Start_m3069863920_MetadataUsageId;
extern "C"  void ItemMaster_Start_m3069863920 (ItemMaster_t335070819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemMaster_Start_m3069863920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MasterBase_loadJson_m1766485096(__this, _stringLiteral3168545717, /*hidden argument*/NULL);
		String_t* L_0 = ((MasterBase_t2623394359 *)__this)->get_json_2();
		Base_t3850954593 * L_1 = JsonUtility_FromJson_TisBase_t3850954593_m232411544(NULL /*static, unused*/, L_0, /*hidden argument*/JsonUtility_FromJson_TisBase_t3850954593_m232411544_MethodInfo_var);
		((MasterBase_t2623394359 *)__this)->set_baseMaster_3(L_1);
		Base_t3850954593 * L_2 = ((MasterBase_t2623394359 *)__this)->get_baseMaster_3();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_value_1();
		Item_t1836005089 * L_4 = JsonUtility_FromJson_TisItem_t1836005089_m3155834584(NULL /*static, unused*/, L_3, /*hidden argument*/JsonUtility_FromJson_TisItem_t1836005089_m3155834584_MethodInfo_var);
		__this->set_itemMaster_4(L_4);
		return;
	}
}
// System.Void ItemMaster/Item::.ctor()
extern "C"  void Item__ctor_m866464304 (Item_t1836005089 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MasterBase::.ctor()
extern "C"  void MasterBase__ctor_m2050107174 (MasterBase_t2623394359 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MasterBase::loadJson(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TextAsset_t3973159845_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2892505447;
extern const uint32_t MasterBase_loadJson_m1766485096_MetadataUsageId;
extern "C"  void MasterBase_loadJson_m1766485096 (MasterBase_t2623394359 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MasterBase_loadJson_m1766485096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2892505447, L_0, /*hidden argument*/NULL);
		Object_t1021602117 * L_2 = Resources_Load_m2041782325(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(((TextAsset_t3973159845 *)IsInstClass(L_2, TextAsset_t3973159845_il2cpp_TypeInfo_var)));
		String_t* L_3 = TextAsset_get_text_m2589865997(((TextAsset_t3973159845 *)IsInstClass(L_2, TextAsset_t3973159845_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_json_2(L_3);
		return;
	}
}
// System.Void MasterBase/Base::.ctor()
extern "C"  void Base__ctor_m399445744 (Base_t3850954593 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PanelController::.ctor()
extern "C"  void PanelController__ctor_m1185452505 (PanelController_t24792858 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PanelController::Start()
extern "C"  void PanelController_Start_m1747718305 (PanelController_t24792858 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PanelController::Update()
extern "C"  void PanelController_Update_m2340368764 (PanelController_t24792858 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Pk::.ctor()
extern "C"  void Pk__ctor_m1304747000 (Pk_t2738205061 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Pk::getInt(System.String,JSONObject)
extern "C"  int32_t Pk_getInt_m530972138 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_2 = JSONObject_GetField_m2890738298(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int64_t L_3 = L_2->get_i_11();
		return (((int32_t)((int32_t)L_3)));
	}
}
// System.String Pk::getStr(System.String,JSONObject)
extern "C"  String_t* Pk_getStr_m3051024009 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_2 = JSONObject_GetField_m2890738298(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_str_8();
		return L_3;
	}
}
// System.Int64 Pk::getLong(System.String,JSONObject)
extern "C"  int64_t Pk_getLong_m3967212028 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_2 = JSONObject_GetField_m2890738298(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int64_t L_3 = L_2->get_i_11();
		return L_3;
	}
}
// System.Single Pk::getFloat(System.String,JSONObject)
extern "C"  float Pk_getFloat_m3540153931 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_2 = JSONObject_GetField_m2890738298(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_3 = JSONObject_get_f_m2513971409(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Pk::getBool(System.String,JSONObject)
extern "C"  bool Pk_getBool_m3675005787 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_2 = JSONObject_GetField_m2890738298(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = L_2->get_b_12();
		return L_3;
	}
}
// System.Int32[] Pk::getIntArr(System.String,JSONObject)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t Pk_getIntArr_m37989831_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* Pk_getIntArr_m37989831 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pk_getIntArr_m37989831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_2 = JSONObject_GetField_m2890738298(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_4);
		String_t* L_6 = String_Replace_m1941156251(L_4, _stringLiteral372029431, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_7);
		String_t* L_9 = String_Replace_m1941156251(L_7, _stringLiteral372029425, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		CharU5BU5D_t1328083999* L_11 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_10);
		StringU5BU5D_t1642385972* L_12 = String_Split_m3326265864(L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		StringU5BU5D_t1642385972* L_13 = V_1;
		NullCheck(L_13);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		int32_t L_14 = V_2;
		V_3 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_14));
		V_4 = 0;
		goto IL_0077;
	}

IL_0054:
	{
		StringU5BU5D_t1642385972* L_15 = V_1;
		int32_t L_16 = V_4;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		String_t* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		bool L_19 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_18, (&V_5), /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0071;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_20 = V_3;
		int32_t L_21 = V_4;
		StringU5BU5D_t1642385972* L_22 = V_1;
		int32_t L_23 = V_4;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		String_t* L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		int32_t L_26 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_21), (int32_t)L_26);
	}

IL_0071:
	{
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_28 = V_4;
		int32_t L_29 = V_2;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0054;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_30 = V_3;
		return L_30;
	}
}
// System.Int32[][] Pk::getIntArrArr(System.String,JSONObject)
extern Il2CppClass* Int32U5BU5DU5BU5D_t3750818532_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern const uint32_t Pk_getIntArrArr_m3681112100_MetadataUsageId;
extern "C"  Int32U5BU5DU5BU5D_t3750818532* Pk_getIntArrArr_m3681112100 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pk_getIntArrArr_m3681112100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Int32U5BU5DU5BU5D_t3750818532* V_4 = NULL;
	int32_t V_5 = 0;
	StringU5BU5D_t1642385972* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject1;
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = JSONObject_HasField_m4286787708(L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return ((Int32U5BU5DU5BU5D_t3750818532*)SZArrayNew(Int32U5BU5DU5BU5D_t3750818532_il2cpp_TypeInfo_var, (uint32_t)1));
	}

IL_0013:
	{
		JSONObject_t1971882247 * L_3 = ___jsonObject1;
		String_t* L_4 = ___key0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_5 = JSONObject_GetField_m2890738298(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		CharU5BU5D_t1328083999* L_8 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)93));
		NullCheck(L_7);
		StringU5BU5D_t1642385972* L_9 = String_Split_m3326265864(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		V_2 = 0;
		V_3 = 0;
		goto IL_0056;
	}

IL_003b:
	{
		StringU5BU5D_t1642385972* L_10 = V_1;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1606060069(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) > ((int32_t)1)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0052;
	}

IL_004e:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_17 = V_3;
		StringU5BU5D_t1642385972* L_18 = V_1;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length))))-(int32_t)1)))))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_19 = V_2;
		V_4 = ((Int32U5BU5DU5BU5D_t3750818532*)SZArrayNew(Int32U5BU5DU5BU5D_t3750818532_il2cpp_TypeInfo_var, (uint32_t)L_19));
		V_5 = 0;
		goto IL_0160;
	}

IL_0071:
	{
		StringU5BU5D_t1642385972* L_20 = V_1;
		int32_t L_21 = V_5;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		String_t* L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m1606060069(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_24) > ((int32_t)1)))
		{
			goto IL_0085;
		}
	}
	{
		goto IL_015a;
	}

IL_0085:
	{
		StringU5BU5D_t1642385972* L_25 = V_1;
		int32_t L_26 = V_5;
		StringU5BU5D_t1642385972* L_27 = V_1;
		int32_t L_28 = V_5;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		String_t* L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_30);
		String_t* L_32 = String_Replace_m1941156251(L_30, _stringLiteral372029431, L_31, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_32);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (String_t*)L_32);
		StringU5BU5D_t1642385972* L_33 = V_1;
		int32_t L_34 = V_5;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		CharU5BU5D_t1328083999* L_37 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_36);
		StringU5BU5D_t1642385972* L_38 = String_Split_m3326265864(L_36, L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		StringU5BU5D_t1642385972* L_39 = V_6;
		NullCheck(L_39);
		V_7 = (((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length))));
		V_8 = 0;
		goto IL_00e5;
	}

IL_00c0:
	{
		StringU5BU5D_t1642385972* L_40 = V_6;
		int32_t L_41 = V_8;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		String_t* L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_45 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_00df;
		}
	}
	{
		int32_t L_46 = V_7;
		V_7 = ((int32_t)((int32_t)L_46-(int32_t)1));
		goto IL_00df;
	}

IL_00df:
	{
		int32_t L_47 = V_8;
		V_8 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_00e5:
	{
		int32_t L_48 = V_8;
		StringU5BU5D_t1642385972* L_49 = V_6;
		NullCheck(L_49);
		if ((((int32_t)L_48) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length)))))))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_50 = V_7;
		V_9 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_50));
		V_10 = 0;
		V_11 = 0;
		goto IL_0148;
	}

IL_0104:
	{
		StringU5BU5D_t1642385972* L_51 = V_6;
		int32_t L_52 = V_11;
		NullCheck(L_51);
		int32_t L_53 = L_52;
		String_t* L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_56 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_011d;
		}
	}
	{
		goto IL_0142;
	}

IL_011d:
	{
		StringU5BU5D_t1642385972* L_57 = V_6;
		int32_t L_58 = V_11;
		NullCheck(L_57);
		int32_t L_59 = L_58;
		String_t* L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		bool L_61 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_60, (&V_12), /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0142;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_62 = V_9;
		int32_t L_63 = V_10;
		int32_t L_64 = L_63;
		V_10 = ((int32_t)((int32_t)L_64+(int32_t)1));
		StringU5BU5D_t1642385972* L_65 = V_6;
		int32_t L_66 = V_11;
		NullCheck(L_65);
		int32_t L_67 = L_66;
		String_t* L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		int32_t L_69 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		NullCheck(L_62);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(L_64), (int32_t)L_69);
	}

IL_0142:
	{
		int32_t L_70 = V_11;
		V_11 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_0148:
	{
		int32_t L_71 = V_11;
		StringU5BU5D_t1642385972* L_72 = V_6;
		NullCheck(L_72);
		if ((((int32_t)L_71) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_72)->max_length)))))))
		{
			goto IL_0104;
		}
	}
	{
		Int32U5BU5DU5BU5D_t3750818532* L_73 = V_4;
		int32_t L_74 = V_5;
		Int32U5BU5D_t3030399641* L_75 = V_9;
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_75);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (Int32U5BU5D_t3030399641*)L_75);
	}

IL_015a:
	{
		int32_t L_76 = V_5;
		V_5 = ((int32_t)((int32_t)L_76+(int32_t)1));
	}

IL_0160:
	{
		int32_t L_77 = V_5;
		int32_t L_78 = V_2;
		if ((((int32_t)L_77) < ((int32_t)L_78)))
		{
			goto IL_0071;
		}
	}
	{
		Int32U5BU5DU5BU5D_t3750818532* L_79 = V_4;
		return L_79;
	}
}
// System.Void Pk::debugLog(System.Int32[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2403048721;
extern Il2CppCodeGenString* _stringLiteral2980329064;
extern Il2CppCodeGenString* _stringLiteral2428201918;
extern const uint32_t Pk_debugLog_m1569006240_MetadataUsageId;
extern "C"  void Pk_debugLog_m1569006240 (Pk_t2738205061 * __this, Int32U5BU5D_t3030399641* ___arr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pk_debugLog_m1569006240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2403048721, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0049;
	}

IL_0011:
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2980329064);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2980329064);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = V_0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral2428201918);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2428201918);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		Int32U5BU5D_t3030399641* L_7 = ___arr0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_12);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_15 = V_0;
		Int32U5BU5D_t3030399641* L_16 = ___arr0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// System.Void Pk::debugLogAA(System.Int32[][])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2191541535;
extern Il2CppCodeGenString* _stringLiteral2980329064;
extern Il2CppCodeGenString* _stringLiteral1328901422;
extern Il2CppCodeGenString* _stringLiteral2428201918;
extern const uint32_t Pk_debugLogAA_m2923645430_MetadataUsageId;
extern "C"  void Pk_debugLogAA_m2923645430 (Pk_t2738205061 * __this, Int32U5BU5DU5BU5D_t3750818532* ___arr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pk_debugLogAA_m2923645430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2191541535, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0072;
	}

IL_0011:
	{
		V_1 = 0;
		goto IL_0063;
	}

IL_0018:
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2980329064);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2980329064);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = V_0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1328901422);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1328901422);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral2428201918);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2428201918);
		ObjectU5BU5D_t3614634134* L_11 = L_10;
		Int32U5BU5DU5BU5D_t3750818532* L_12 = ___arr0;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Int32U5BU5D_t3030399641* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_20);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3881798623(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0063:
	{
		int32_t L_23 = V_1;
		Int32U5BU5DU5BU5D_t3750818532* L_24 = ___arr0;
		int32_t L_25 = V_0;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Int32U5BU5D_t3030399641* L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_28 = V_0;
		V_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_29 = V_0;
		Int32U5BU5DU5BU5D_t3750818532* L_30 = ___arr0;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m3280132936 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	{
		UserControllerBase__ctor_m345467335(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestController::.ctor()
extern "C"  void TestController__ctor_m2120569257 (TestController_t674116116 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestController::Start()
extern "C"  void TestController_Start_m1731473377 (TestController_t674116116 * __this, const MethodInfo* method)
{
	{
		__this->set__timer_6((5.0f));
		__this->set__playerHp_7(((int32_t)10));
		__this->set__bossHp_8(((int32_t)10));
		return;
	}
}
// System.Void TestController::Update()
extern Il2CppCodeGenString* _stringLiteral2927964997;
extern const uint32_t TestController_Update_m4000770690_MetadataUsageId;
extern "C"  void TestController_Update_m4000770690 (TestController_t674116116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestController_Update_m4000770690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = __this->get__timer_6();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__timer_6(((float)((float)L_0-(float)L_1)));
		int32_t L_2 = TestController_judge_m4085086222(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_0034;
		}
		if (L_3 == 1)
		{
			goto IL_0039;
		}
		if (L_3 == 2)
		{
			goto IL_003e;
		}
		if (L_3 == 3)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0052;
	}

IL_0034:
	{
		goto IL_0057;
	}

IL_0039:
	{
		goto IL_0057;
	}

IL_003e:
	{
		goto IL_0057;
	}

IL_0043:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2927964997, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0052:
	{
		goto IL_0057;
	}

IL_0057:
	{
		return;
	}
}
// System.Int32 TestController::judge()
extern "C"  int32_t TestController_judge_m4085086222 (TestController_t674116116 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__bossHp_8();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		return 1;
	}

IL_000e:
	{
		int32_t L_1 = __this->get__playerHp_7();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_001c;
		}
	}
	{
		return 2;
	}

IL_001c:
	{
		float L_2 = __this->get__timer_6();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_002e;
		}
	}
	{
		return 3;
	}

IL_002e:
	{
		return 0;
	}
}
// System.Void TimeController::.ctor()
extern "C"  void TimeController__ctor_m1965318492 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimeController::Start()
extern Il2CppClass* TimeController_t1641888515_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisTimeText_t2509642322_m415022099_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern const uint32_t TimeController_Start_m159638416_MetadataUsageId;
extern "C"  void TimeController_Start_m159638416 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimeController_Start_m159638416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeController_t1641888515_il2cpp_TypeInfo_var);
		float L_0 = ((TimeController_t1641888515_StaticFields*)TimeController_t1641888515_il2cpp_TypeInfo_var->static_fields)->get_BATTLE_START_INTERVAL_TIME_2();
		__this->set__battleStartIntervalTimer_4(L_0);
		float L_1 = ((TimeController_t1641888515_StaticFields*)TimeController_t1641888515_il2cpp_TypeInfo_var->static_fields)->get_BATTLE_TIME_3();
		__this->set__battleTimer_5(L_1);
		__this->set__isBattleStart_6((bool)0);
		__this->set__isBattleEnd_7((bool)0);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		NullCheck(L_2);
		TimeText_t2509642322 * L_3 = GameObject_GetComponentInChildren_TisTimeText_t2509642322_m415022099(L_2, /*hidden argument*/GameObject_GetComponentInChildren_TisTimeText_t2509642322_m415022099_MethodInfo_var);
		__this->set__timeText_8(L_3);
		return;
	}
}
// System.Void TimeController::moveBattleStartIntervalTimer()
extern "C"  void TimeController_moveBattleStartIntervalTimer_m2480696497 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__battleStartIntervalTimer_4();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__battleStartIntervalTimer_4(((float)((float)L_0-(float)L_1)));
		return;
	}
}
// System.Void TimeController::checkBattleStartIntervalTimer()
extern "C"  void TimeController_checkBattleStartIntervalTimer_m2515658548 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__battleStartIntervalTimer_4();
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		__this->set__isBattleStart_6((bool)1);
	}

IL_0017:
	{
		return;
	}
}
// System.Void TimeController::moveBattleTimer()
extern "C"  void TimeController_moveBattleTimer_m3304460796 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__battleTimer_5();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__battleTimer_5(((float)((float)L_0-(float)L_1)));
		return;
	}
}
// System.Void TimeController::checkBattleTimer()
extern "C"  void TimeController_checkBattleTimer_m3648717641 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__battleTimer_5();
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		__this->set__isBattleEnd_7((bool)1);
	}

IL_0017:
	{
		return;
	}
}
// System.Void TimeController::Update()
extern Il2CppClass* TimeController_t1641888515_il2cpp_TypeInfo_var;
extern const uint32_t TimeController_Update_m3259163491_MetadataUsageId;
extern "C"  void TimeController_Update_m3259163491 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimeController_Update_m3259163491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isBattleStart_6();
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		TimeController_moveBattleStartIntervalTimer_m2480696497(__this, /*hidden argument*/NULL);
		TimeText_t2509642322 * L_1 = __this->get__timeText_8();
		float L_2 = __this->get__battleStartIntervalTimer_4();
		IL2CPP_RUNTIME_CLASS_INIT(TimeController_t1641888515_il2cpp_TypeInfo_var);
		float L_3 = ((TimeController_t1641888515_StaticFields*)TimeController_t1641888515_il2cpp_TypeInfo_var->static_fields)->get_BATTLE_TIME_3();
		NullCheck(L_1);
		TimeText_setTime_m3820777775(L_1, L_2, (bool)0, L_3, /*hidden argument*/NULL);
		TimeController_checkBattleStartIntervalTimer_m2515658548(__this, /*hidden argument*/NULL);
		return;
	}

IL_002f:
	{
		bool L_4 = __this->get__isBattleEnd_7();
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		TimeController_moveBattleTimer_m3304460796(__this, /*hidden argument*/NULL);
		TimeText_t2509642322 * L_5 = __this->get__timeText_8();
		float L_6 = __this->get__battleTimer_5();
		IL2CPP_RUNTIME_CLASS_INIT(TimeController_t1641888515_il2cpp_TypeInfo_var);
		float L_7 = ((TimeController_t1641888515_StaticFields*)TimeController_t1641888515_il2cpp_TypeInfo_var->static_fields)->get_BATTLE_TIME_3();
		NullCheck(L_5);
		TimeText_setTime_m3820777775(L_5, L_6, (bool)1, L_7, /*hidden argument*/NULL);
		TimeController_checkBattleTimer_m3648717641(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TimeController::isBattleStarted()
extern "C"  bool TimeController_isBattleStarted_m1090798863 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__isBattleStart_6();
		return L_0;
	}
}
// System.Boolean TimeController::isBattleEnded()
extern "C"  bool TimeController_isBattleEnded_m2521904612 (TimeController_t1641888515 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__isBattleEnd_7();
		return L_0;
	}
}
// System.Void TimeController::.cctor()
extern Il2CppClass* TimeController_t1641888515_il2cpp_TypeInfo_var;
extern const uint32_t TimeController__cctor_m2823837213_MetadataUsageId;
extern "C"  void TimeController__cctor_m2823837213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimeController__cctor_m2823837213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((TimeController_t1641888515_StaticFields*)TimeController_t1641888515_il2cpp_TypeInfo_var->static_fields)->set_BATTLE_START_INTERVAL_TIME_2((3.0f));
		((TimeController_t1641888515_StaticFields*)TimeController_t1641888515_il2cpp_TypeInfo_var->static_fields)->set_BATTLE_TIME_3((10.0f));
		return;
	}
}
// System.Void TimeText::.ctor()
extern "C"  void TimeText__ctor_m1704414703 (TimeText_t2509642322 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimeText::setTime(System.Single,System.Boolean,System.Single)
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2858725202;
extern Il2CppCodeGenString* _stringLiteral3068682171;
extern const uint32_t TimeText_setTime_m3820777775_MetadataUsageId;
extern "C"  void TimeText_setTime_m3820777775 (TimeText_t2509642322 * __this, float ___time0, bool ___isStarted1, float ___battleTime2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimeText_setTime_m3820777775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		V_0 = L_0;
		float L_1 = ___time0;
		___time0 = ((float)((float)L_1+(float)(1.0f)));
		bool L_2 = ___isStarted1;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		float L_3 = ___battleTime2;
		float L_4 = ___time0;
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		Text_t356221433 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral2858725202);
		return;
	}

IL_0029:
	{
		bool L_6 = ___isStarted1;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		float L_7 = ___time0;
		if ((!(((float)L_7) < ((float)(1.0f)))))
		{
			goto IL_0046;
		}
	}
	{
		Text_t356221433 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteral3068682171);
		return;
	}

IL_0046:
	{
		Text_t356221433 * L_9 = V_0;
		float L_10 = ___time0;
		V_1 = (((int32_t)((int32_t)L_10)));
		String_t* L_11 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_11);
		return;
	}
}
// System.Void UserControllerBase::.ctor()
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t UserControllerBase__ctor_m345467335_MetadataUsageId;
extern "C"  void UserControllerBase__ctor_m345467335 (UserControllerBase_t1251906990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserControllerBase__ctor_m345467335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__selectingCardIndex_2(((int32_t)99));
		__this->set__userCards_3(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set__deckCount_4(((int32_t)10));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UserControllerBase::getSelectingCardIndex()
extern "C"  int32_t UserControllerBase_getSelectingCardIndex_m2205353237 (UserControllerBase_t1251906990 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__selectingCardIndex_2();
		return L_0;
	}
}
// System.Void UserControllerBase::setSelectingCardIndex(System.Int32)
extern "C"  void UserControllerBase_setSelectingCardIndex_m1941113010 (UserControllerBase_t1251906990 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		__this->set__selectingCardIndex_2(L_0);
		return;
	}
}
// System.Int32 UserControllerBase::getUserCard(System.Int32)
extern "C"  int32_t UserControllerBase_getUserCard_m352162031 (UserControllerBase_t1251906990 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_1 = __this->get__userCards_3();
		int32_t L_2 = UserControllerBase_getSelectingCardIndex_m2205353237(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		return L_4;
	}

IL_0015:
	{
		Int32U5BU5D_t3030399641* L_5 = __this->get__userCards_3();
		int32_t L_6 = ___index0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		return L_8;
	}
}
// System.Void UserControllerBase::setUserCard(System.Int32,System.Int32)
extern "C"  void UserControllerBase_setUserCard_m1357951436 (UserControllerBase_t1251906990 * __this, int32_t ___index0, int32_t ___num1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get__userCards_3();
		int32_t L_1 = ___index0;
		int32_t L_2 = ___num1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int32_t)L_2);
		return;
	}
}
// System.Int32 UserControllerBase::getUserDeckCount()
extern "C"  int32_t UserControllerBase_getUserDeckCount_m2706233554 (UserControllerBase_t1251906990 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__deckCount_4();
		return L_0;
	}
}
// System.Void UserControllerBase::decreaseUserDeckCount()
extern "C"  void UserControllerBase_decreaseUserDeckCount_m1860642640 (UserControllerBase_t1251906990 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__deckCount_4();
		__this->set__deckCount_4(((int32_t)((int32_t)L_0-(int32_t)1)));
		return;
	}
}
// System.Void WebSocketClient::.ctor()
extern "C"  void WebSocketClient__ctor_m3624786787 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	{
		Pk__ctor_m1304747000(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::Start()
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2844854044;
extern const uint32_t WebSocketClient_Start_m173570775_MetadataUsageId;
extern "C"  void WebSocketClient_Start_m173570775 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_Start_m173570775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_0 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_START_9();
		WebSocketClient_changePhase_m148092363(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_1 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral2844854044, 0, /*hidden argument*/NULL);
		__this->set__userId_19(L_1);
		WebSocketClient_ConnectWebSocket_m1699444666(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral2897286793;
extern Il2CppCodeGenString* _stringLiteral2194724248;
extern const uint32_t WebSocketClient_Update_m2959837662_MetadataUsageId;
extern "C"  void WebSocketClient_Update_m2959837662 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_Update_m2959837662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isConnecting_22();
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUp_m1952023453(NULL /*static, unused*/, _stringLiteral372029392, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2897286793, /*hidden argument*/NULL);
		WebSocketClient_ConnectWebSocket_m1699444666(__this, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		int32_t L_2 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_3 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_START_9();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_003c;
		}
	}
	{
		return;
	}

IL_003c:
	{
		int32_t L_4 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_5 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_CREATE_USER_10();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0065;
		}
	}
	{
		JSONObject_t1971882247 * L_6 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2218833806(L_6, /*hidden argument*/NULL);
		WebSocketClient_sendMessage_m2434924180(__this, ((int32_t)100), L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_7 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_WAIT_CREATE_USER_11();
		WebSocketClient_changePhase_m148092363(__this, L_7, /*hidden argument*/NULL);
		return;
	}

IL_0065:
	{
		int32_t L_8 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_9 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_WAIT_CREATE_USER_11();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0076;
		}
	}
	{
		return;
	}

IL_0076:
	{
		int32_t L_10 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_11 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_LOGIN_12();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_00bc;
		}
	}
	{
		int32_t L_12 = __this->get__userId_19();
		if (L_12)
		{
			goto IL_009d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_13 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_CREATE_USER_10();
		WebSocketClient_changePhase_m148092363(__this, L_13, /*hidden argument*/NULL);
		return;
	}

IL_009d:
	{
		WebSocketClient_saveUserId_m4285407080(__this, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_14 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2218833806(L_14, /*hidden argument*/NULL);
		WebSocketClient_sendMessage_m2434924180(__this, ((int32_t)101), L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_15 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_WAIT_ROOM_FILL_13();
		WebSocketClient_changePhase_m148092363(__this, L_15, /*hidden argument*/NULL);
		return;
	}

IL_00bc:
	{
		int32_t L_16 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_17 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_WAIT_ROOM_FILL_13();
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_00cd;
		}
	}
	{
		return;
	}

IL_00cd:
	{
		int32_t L_18 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_19 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_CHANGE_SCENE_14();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_19))))
		{
			goto IL_00f4;
		}
	}
	{
		SceneManager_LoadScene_m1386820036(NULL /*static, unused*/, _stringLiteral2194724248, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_20 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_GAME_INIT_15();
		WebSocketClient_changePhase_m148092363(__this, L_20, /*hidden argument*/NULL);
		return;
	}

IL_00f4:
	{
		int32_t L_21 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_22 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_GAME_INIT_15();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0110;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_23 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_GAME_PLAYING_16();
		WebSocketClient_changePhase_m148092363(__this, L_23, /*hidden argument*/NULL);
		return;
	}

IL_0110:
	{
		int32_t L_24 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_25 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_GAME_PLAYING_16();
		if ((!(((uint32_t)L_24) == ((uint32_t)L_25))))
		{
			goto IL_0121;
		}
	}
	{
		return;
	}

IL_0121:
	{
		return;
	}
}
// System.Void WebSocketClient::OnDestroy()
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketClient_OnDestroy_m2402546446_MetadataUsageId;
extern "C"  void WebSocketClient_OnDestroy_m2402546446 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_OnDestroy_m2402546446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebSocketClient_sendMessage_m2434924180(__this, ((int32_t)9999), (JSONObject_t1971882247 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		WebSocket_t3268376029 * L_0 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		NullCheck(L_0);
		WebSocket_Close_m790379411(L_0, /*hidden argument*/NULL);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set__ws_27((WebSocket_t3268376029 *)NULL);
		return;
	}
}
// System.Void WebSocketClient::ConnectWebSocket()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_1_t1481358898_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_1_t3388497467_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_1_t3230782241_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocketClient_U3CConnectWebSocketU3Em__0_m1860180633_MethodInfo_var;
extern const MethodInfo* WebSocketClient_U3CConnectWebSocketU3Em__1_m4218706127_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m508816278_MethodInfo_var;
extern const MethodInfo* WebSocketClient_U3CConnectWebSocketU3Em__2_m2020985979_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m770480887_MethodInfo_var;
extern const MethodInfo* WebSocketClient_U3CConnectWebSocketU3Em__3_m3695248688_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m21715905_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3013435652;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern const uint32_t WebSocketClient_ConnectWebSocket_m1699444666_MetadataUsageId;
extern "C"  void WebSocketClient_ConnectWebSocket_m1699444666 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_ConnectWebSocket_m1699444666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebSocket_t3268376029 * G_B2_0 = NULL;
	WebSocket_t3268376029 * G_B1_0 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3013435652);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3013435652);
		StringU5BU5D_t1642385972* L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		String_t* L_2 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_IP_ADDRESS_8();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral372029336);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029336);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PORT_NUM_7();
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral372029315);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029315);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m626692867(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_8 = (WebSocket_t3268376029 *)il2cpp_codegen_object_new(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		WebSocket__ctor_m666923799(L_8, L_7, ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set__ws_27(L_8);
		WebSocket_t3268376029 * L_9 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)WebSocketClient_U3CConnectWebSocketU3Em__0_m1860180633_MethodInfo_var);
		EventHandler_t277755526 * L_11 = (EventHandler_t277755526 *)il2cpp_codegen_object_new(EventHandler_t277755526_il2cpp_TypeInfo_var);
		EventHandler__ctor_m3447735595(L_11, __this, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		WebSocket_add_OnOpen_m924055045(L_9, L_11, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_12 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)WebSocketClient_U3CConnectWebSocketU3Em__1_m4218706127_MethodInfo_var);
		EventHandler_1_t1481358898 * L_14 = (EventHandler_1_t1481358898 *)il2cpp_codegen_object_new(EventHandler_1_t1481358898_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m508816278(L_14, __this, L_13, /*hidden argument*/EventHandler_1__ctor_m508816278_MethodInfo_var);
		NullCheck(L_12);
		WebSocket_add_OnMessage_m2021047116(L_12, L_14, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_15 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		EventHandler_1_t3388497467 * L_16 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_29();
		G_B1_0 = L_15;
		if (L_16)
		{
			G_B2_0 = L_15;
			goto IL_008c;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)WebSocketClient_U3CConnectWebSocketU3Em__2_m2020985979_MethodInfo_var);
		EventHandler_1_t3388497467 * L_18 = (EventHandler_1_t3388497467 *)il2cpp_codegen_object_new(EventHandler_1_t3388497467_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m770480887(L_18, NULL, L_17, /*hidden argument*/EventHandler_1__ctor_m770480887_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_29(L_18);
		G_B2_0 = G_B1_0;
	}

IL_008c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		EventHandler_1_t3388497467 * L_19 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_29();
		NullCheck(G_B2_0);
		WebSocket_add_OnError_m3284929358(G_B2_0, L_19, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_20 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)WebSocketClient_U3CConnectWebSocketU3Em__3_m3695248688_MethodInfo_var);
		EventHandler_1_t3230782241 * L_22 = (EventHandler_1_t3230782241 *)il2cpp_codegen_object_new(EventHandler_1_t3230782241_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m21715905(L_22, __this, L_21, /*hidden argument*/EventHandler_1__ctor_m21715905_MethodInfo_var);
		NullCheck(L_20);
		WebSocket_add_OnClose_m963191942(L_20, L_22, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_23 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		NullCheck(L_23);
		WebSocket_Connect_m1908822439(L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::sendMessage(System.Int32,JSONObject)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m804483696_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m870713862_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4175023932_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1216418429;
extern Il2CppCodeGenString* _stringLiteral1612434460;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral816986884;
extern Il2CppCodeGenString* _stringLiteral2880463618;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern const uint32_t WebSocketClient_sendMessage_m2434924180_MetadataUsageId;
extern "C"  void WebSocketClient_sendMessage_m2434924180 (WebSocketClient_t3274474108 * __this, int32_t ___pkType0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_sendMessage_m2434924180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Enumerator_t933071039  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1216418429);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1216418429);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = __this->get__userId_19();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1612434460);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1612434460);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___pkType0;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral372029310);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029310);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		JSONObject_t1971882247 * L_12 = ___jsonObject1;
		if (!L_12)
		{
			goto IL_00bd;
		}
	}
	{
		JSONObject_t1971882247 * L_13 = ___jsonObject1;
		NullCheck(L_13);
		int32_t L_14 = JSONObject_get_Count_m3265098784(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___jsonObject1;
		NullCheck(L_15);
		List_1_t1398341365 * L_16 = L_15->get_keys_7();
		NullCheck(L_16);
		Enumerator_t933071039  L_17 = List_1_GetEnumerator_m804483696(L_16, /*hidden argument*/List_1_GetEnumerator_m804483696_MethodInfo_var);
		V_2 = L_17;
	}

IL_0059:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009e;
		}

IL_005e:
		{
			String_t* L_18 = Enumerator_get_Current_m870713862((&V_2), /*hidden argument*/Enumerator_get_Current_m870713862_MethodInfo_var);
			V_1 = L_18;
			String_t* L_19 = V_0;
			V_3 = L_19;
			ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
			String_t* L_21 = V_3;
			NullCheck(L_20);
			ArrayElementTypeCheck (L_20, L_21);
			(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_21);
			ObjectU5BU5D_t3614634134* L_22 = L_20;
			NullCheck(L_22);
			ArrayElementTypeCheck (L_22, _stringLiteral816986884);
			(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral816986884);
			ObjectU5BU5D_t3614634134* L_23 = L_22;
			String_t* L_24 = V_1;
			NullCheck(L_23);
			ArrayElementTypeCheck (L_23, L_24);
			(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_24);
			ObjectU5BU5D_t3614634134* L_25 = L_23;
			NullCheck(L_25);
			ArrayElementTypeCheck (L_25, _stringLiteral2880463618);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2880463618);
			ObjectU5BU5D_t3614634134* L_26 = L_25;
			JSONObject_t1971882247 * L_27 = ___jsonObject1;
			String_t* L_28 = V_1;
			NullCheck(L_27);
			JSONObject_t1971882247 * L_29 = JSONObject_GetField_m2890738298(L_27, L_28, /*hidden argument*/NULL);
			NullCheck(L_26);
			ArrayElementTypeCheck (L_26, L_29);
			(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_29);
			ObjectU5BU5D_t3614634134* L_30 = L_26;
			NullCheck(L_30);
			ArrayElementTypeCheck (L_30, _stringLiteral372029310);
			(L_30)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral372029310);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_31 = String_Concat_m3881798623(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			V_0 = L_31;
		}

IL_009e:
		{
			bool L_32 = Enumerator_MoveNext_m4175023932((&V_2), /*hidden argument*/Enumerator_MoveNext_m4175023932_MethodInfo_var);
			if (L_32)
			{
				goto IL_005e;
			}
		}

IL_00aa:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00af);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00af;
	}

FINALLY_00af:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_2), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(175)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(175)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00bd:
	{
		String_t* L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, L_33, _stringLiteral372029393, /*hidden argument*/NULL);
		V_0 = L_34;
		String_t* L_35 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		WebSocket_t3268376029 * L_36 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get__ws_27();
		String_t* L_37 = V_0;
		NullCheck(L_36);
		WebSocket_Send_m4053134035(L_36, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::parseMessage(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4288074643;
extern Il2CppCodeGenString* _stringLiteral1421151742;
extern const uint32_t WebSocketClient_parseMessage_m1799235779_MetadataUsageId;
extern "C"  void WebSocketClient_parseMessage_m1799235779 (WebSocketClient_t3274474108 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_parseMessage_m1799235779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4288074643, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___data0;
		JSONObject_t1971882247 * L_3 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2243138423(L_3, L_2, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_3;
		JSONObject_t1971882247 * L_4 = V_0;
		NullCheck(L_4);
		JSONObject_t1971882247 * L_5 = JSONObject_GetField_m2890738298(L_4, _stringLiteral1421151742, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		JSONObject_t1971882247 * L_6 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_GetField_m2890738298(L_6, _stringLiteral1421151742, /*hidden argument*/NULL);
		NullCheck(L_7);
		int64_t L_8 = L_7->get_i_11();
		V_1 = (((int32_t)((int32_t)L_8)));
		int32_t L_9 = V_1;
		JSONObject_t1971882247 * L_10 = V_0;
		WebSocketClient_actionWithPk_m1892780742(__this, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void WebSocketClient::changePhase(System.Int32)
extern "C"  void WebSocketClient_changePhase_m148092363 (WebSocketClient_t3274474108 * __this, int32_t ___phase0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___phase0;
		__this->set__phase_18(L_0);
		return;
	}
}
// System.Void WebSocketClient::saveUserId()
extern Il2CppCodeGenString* _stringLiteral2844854044;
extern const uint32_t WebSocketClient_saveUserId_m4285407080_MetadataUsageId;
extern "C"  void WebSocketClient_saveUserId_m4285407080 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_saveUserId_m4285407080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__userId_19();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2844854044, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::pushStart()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2897286793;
extern const uint32_t WebSocketClient_pushStart_m1598994261_MetadataUsageId;
extern "C"  void WebSocketClient_pushStart_m1598994261 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_pushStart_m1598994261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isConnecting_22();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2897286793, /*hidden argument*/NULL);
		WebSocketClient_ConnectWebSocket_m1699444666(__this, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		int32_t L_1 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_2 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_START_9();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_3 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_LOGIN_12();
		WebSocketClient_changePhase_m148092363(__this, L_3, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void WebSocketClient::pushNewPlayer()
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketClient_pushNewPlayer_m507443108_MetadataUsageId;
extern "C"  void WebSocketClient_pushNewPlayer_m507443108 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_pushNewPlayer_m507443108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__phase_18();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_1 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_START_9();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set__userId_19(0);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_2 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_CREATE_USER_10();
		WebSocketClient_changePhase_m148092363(__this, L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void WebSocketClient::actionWithPk(System.Int32,JSONObject)
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2844854044;
extern Il2CppCodeGenString* _stringLiteral2046682720;
extern Il2CppCodeGenString* _stringLiteral1270222245;
extern Il2CppCodeGenString* _stringLiteral3054569979;
extern Il2CppCodeGenString* _stringLiteral3340344136;
extern Il2CppCodeGenString* _stringLiteral1712312766;
extern const uint32_t WebSocketClient_actionWithPk_m1892780742_MetadataUsageId;
extern "C"  void WebSocketClient_actionWithPk_m1892780742 (WebSocketClient_t3274474108 * __this, int32_t ___pkType0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_actionWithPk_m1892780742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___pkType0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)100))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = ___pkType0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)101))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_2 = ___pkType0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)110))))
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_3 = ___pkType0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)200))))
		{
			goto IL_00d5;
		}
	}
	{
		goto IL_012c;
	}

IL_0028:
	{
		JSONObject_t1971882247 * L_4 = ___jsonObject1;
		int32_t L_5 = Pk_getInt_m530972138(__this, _stringLiteral2844854044, L_4, /*hidden argument*/NULL);
		__this->set__userId_19(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_6 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_LOGIN_12();
		WebSocketClient_changePhase_m148092363(__this, L_6, /*hidden argument*/NULL);
		goto IL_0131;
	}

IL_004a:
	{
		JSONObject_t1971882247 * L_7 = ___jsonObject1;
		int32_t L_8 = Pk_getInt_m530972138(__this, _stringLiteral2046682720, L_7, /*hidden argument*/NULL);
		__this->set__roomId_20(L_8);
		JSONObject_t1971882247 * L_9 = ___jsonObject1;
		int32_t L_10 = Pk_getInt_m530972138(__this, _stringLiteral1270222245, L_9, /*hidden argument*/NULL);
		__this->set__playerNum_21(L_10);
		goto IL_0131;
	}

IL_0073:
	{
		__this->set__fieldCardNums_24((Int32U5BU5D_t3030399641*)NULL);
		JSONObject_t1971882247 * L_11 = ___jsonObject1;
		Int32U5BU5D_t3030399641* L_12 = Pk_getIntArr_m37989831(__this, _stringLiteral3054569979, L_11, /*hidden argument*/NULL);
		__this->set__fieldCardNums_24(L_12);
		__this->set__playerCardNums_25((Int32U5BU5DU5BU5D_t3750818532*)NULL);
		JSONObject_t1971882247 * L_13 = ___jsonObject1;
		Int32U5BU5DU5BU5D_t3750818532* L_14 = Pk_getIntArrArr_m3681112100(__this, _stringLiteral3340344136, L_13, /*hidden argument*/NULL);
		__this->set__playerCardNums_25(L_14);
		__this->set__playerUserIds_26((Int32U5BU5D_t3030399641*)NULL);
		JSONObject_t1971882247 * L_15 = ___jsonObject1;
		Int32U5BU5D_t3030399641* L_16 = Pk_getIntArr_m37989831(__this, _stringLiteral1712312766, L_15, /*hidden argument*/NULL);
		__this->set__playerUserIds_26(L_16);
		__this->set_isChangedField_23((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketClient_t3274474108_il2cpp_TypeInfo_var);
		int32_t L_17 = ((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->get_PHASE_CHANGE_SCENE_14();
		WebSocketClient_changePhase_m148092363(__this, L_17, /*hidden argument*/NULL);
		goto IL_0131;
	}

IL_00d5:
	{
		__this->set__fieldCardNums_24((Int32U5BU5D_t3030399641*)NULL);
		JSONObject_t1971882247 * L_18 = ___jsonObject1;
		Int32U5BU5D_t3030399641* L_19 = Pk_getIntArr_m37989831(__this, _stringLiteral3054569979, L_18, /*hidden argument*/NULL);
		__this->set__fieldCardNums_24(L_19);
		__this->set__playerCardNums_25((Int32U5BU5DU5BU5D_t3750818532*)NULL);
		JSONObject_t1971882247 * L_20 = ___jsonObject1;
		Int32U5BU5DU5BU5D_t3750818532* L_21 = Pk_getIntArrArr_m3681112100(__this, _stringLiteral3340344136, L_20, /*hidden argument*/NULL);
		__this->set__playerCardNums_25(L_21);
		__this->set__playerUserIds_26((Int32U5BU5D_t3030399641*)NULL);
		JSONObject_t1971882247 * L_22 = ___jsonObject1;
		Int32U5BU5D_t3030399641* L_23 = Pk_getIntArr_m37989831(__this, _stringLiteral1712312766, L_22, /*hidden argument*/NULL);
		__this->set__playerUserIds_26(L_23);
		__this->set_isChangedField_23((bool)1);
		goto IL_0131;
	}

IL_012c:
	{
		goto IL_0131;
	}

IL_0131:
	{
		return;
	}
}
// System.Void WebSocketClient::setCardController(CardController)
extern "C"  void WebSocketClient_setCardController_m1671294365 (WebSocketClient_t3274474108 * __this, CardController_t2595996862 * ___cardController0, const MethodInfo* method)
{
	{
		CardController_t2595996862 * L_0 = ___cardController0;
		__this->set__cardController_28(L_0);
		return;
	}
}
// System.Int32 WebSocketClient::getPlayerNum()
extern "C"  int32_t WebSocketClient_getPlayerNum_m4203793634 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__playerNum_21();
		return L_0;
	}
}
// System.Int32 WebSocketClient::getNextPlayerNum(System.Int32)
extern "C"  int32_t WebSocketClient_getNextPlayerNum_m818655264 (WebSocketClient_t3274474108 * __this, int32_t ___basePlayerNum0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___basePlayerNum0;
		Int32U5BU5D_t3030399641* L_1 = __this->get__playerUserIds_26();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))-(int32_t)1)))))
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		int32_t L_2 = ___basePlayerNum0;
		return ((int32_t)((int32_t)L_2+(int32_t)1));
	}
}
// System.Int32[] WebSocketClient::getFieldCardNums()
extern "C"  Int32U5BU5D_t3030399641* WebSocketClient_getFieldCardNums_m2030196866 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get__fieldCardNums_24();
		return L_0;
	}
}
// System.Int32 WebSocketClient::getFieldCardNum(System.Int32)
extern "C"  int32_t WebSocketClient_getFieldCardNum_m3900970582 (WebSocketClient_t3274474108 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get__fieldCardNums_24();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Int32[][] WebSocketClient::getPlayerCardNums()
extern "C"  Int32U5BU5DU5BU5D_t3750818532* WebSocketClient_getPlayerCardNums_m1908209225 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5DU5BU5D_t3750818532* L_0 = __this->get__playerCardNums_25();
		return L_0;
	}
}
// System.Int32[] WebSocketClient::getPlayerCardNums(System.Int32)
extern "C"  Int32U5BU5D_t3030399641* WebSocketClient_getPlayerCardNums_m3514879090 (WebSocketClient_t3274474108 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Int32U5BU5DU5BU5D_t3750818532* L_0 = __this->get__playerCardNums_25();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Int32U5BU5D_t3030399641* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Int32 WebSocketClient::getPlayerCardNum(System.Int32,System.Int32)
extern "C"  int32_t WebSocketClient_getPlayerCardNum_m2837115496 (WebSocketClient_t3274474108 * __this, int32_t ___userIndex0, int32_t ___cardIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5DU5BU5D_t3750818532* L_0 = __this->get__playerCardNums_25();
		int32_t L_1 = ___userIndex0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Int32U5BU5D_t3030399641* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		int32_t L_4 = ___cardIndex1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Int32[] WebSocketClient::getPlayerUserIds()
extern "C"  Int32U5BU5D_t3030399641* WebSocketClient_getPlayerUserIds_m4162158257 (WebSocketClient_t3274474108 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get__playerUserIds_26();
		return L_0;
	}
}
// System.Int32 WebSocketClient::getPlayerUserId(System.Int32)
extern "C"  int32_t WebSocketClient_getPlayerUserId_m3572018765 (WebSocketClient_t3274474108 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get__playerUserIds_26();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void WebSocketClient::checkChangeCard(JSONObject)
extern "C"  void WebSocketClient_checkChangeCard_m1740291254 (WebSocketClient_t3274474108 * __this, JSONObject_t1971882247 * ___jsonObject0, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___jsonObject0;
		WebSocketClient_sendMessage_m2434924180(__this, ((int32_t)200), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::.cctor()
extern Il2CppClass* WebSocketClient_t3274474108_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3940838584;
extern Il2CppCodeGenString* _stringLiteral3763104578;
extern const uint32_t WebSocketClient__cctor_m4262126418_MetadataUsageId;
extern "C"  void WebSocketClient__cctor_m4262126418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient__cctor_m4262126418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PORT_NUM_7(_stringLiteral3940838584);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_IP_ADDRESS_8(_stringLiteral3763104578);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_CREATE_USER_10(1);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_WAIT_CREATE_USER_11(2);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_LOGIN_12(3);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_WAIT_ROOM_FILL_13(4);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_CHANGE_SCENE_14(5);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_GAME_INIT_15(6);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_GAME_PLAYING_16(7);
		((WebSocketClient_t3274474108_StaticFields*)WebSocketClient_t3274474108_il2cpp_TypeInfo_var->static_fields)->set_PHASE_END_17(8);
		return;
	}
}
// System.Void WebSocketClient::<ConnectWebSocket>m__0(System.Object,System.EventArgs)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1751609921;
extern const uint32_t WebSocketClient_U3CConnectWebSocketU3Em__0_m1860180633_MetadataUsageId;
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__0_m1860180633 (WebSocketClient_t3274474108 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_U3CConnectWebSocketU3Em__0_m1860180633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__isConnecting_22((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1751609921, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::<ConnectWebSocket>m__1(System.Object,WebSocketSharp.MessageEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketClient_U3CConnectWebSocketU3Em__1_m4218706127_MetadataUsageId;
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__1_m4218706127 (WebSocketClient_t3274474108 * __this, Il2CppObject * ___sender0, MessageEventArgs_t2890051726 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_U3CConnectWebSocketU3Em__1_m4218706127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MessageEventArgs_t2890051726 * L_0 = ___e1;
		NullCheck(L_0);
		String_t* L_1 = MessageEventArgs_get_Data_m136184186(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		MessageEventArgs_t2890051726 * L_2 = ___e1;
		NullCheck(L_2);
		String_t* L_3 = MessageEventArgs_get_Data_m136184186(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		MessageEventArgs_t2890051726 * L_6 = ___e1;
		NullCheck(L_6);
		String_t* L_7 = MessageEventArgs_get_Data_m136184186(L_6, /*hidden argument*/NULL);
		WebSocketClient_parseMessage_m1799235779(__this, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void WebSocketClient::<ConnectWebSocket>m__2(System.Object,WebSocketSharp.ErrorEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3986716526;
extern const uint32_t WebSocketClient_U3CConnectWebSocketU3Em__2_m2020985979_MetadataUsageId;
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__2_m2020985979 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, ErrorEventArgs_t502222999 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_U3CConnectWebSocketU3Em__2_m2020985979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ErrorEventArgs_t502222999 * L_0 = ___e1;
		NullCheck(L_0);
		String_t* L_1 = ErrorEventArgs_get_Message_m4135312202(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3986716526, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketClient::<ConnectWebSocket>m__3(System.Object,WebSocketSharp.CloseEventArgs)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1734544187;
extern const uint32_t WebSocketClient_U3CConnectWebSocketU3Em__3_m3695248688_MetadataUsageId;
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__3_m3695248688 (WebSocketClient_t3274474108 * __this, Il2CppObject * ___sender0, CloseEventArgs_t344507773 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketClient_U3CConnectWebSocketU3Em__3_m3695248688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__isConnecting_22((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1734544187, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
