﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UserControllerBase1251906990.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyController
struct  EnemyController_t2146768720  : public UserControllerBase_t1251906990
{
public:
	// System.Single EnemyController::_timer
	float ____timer_5;

public:
	inline static int32_t get_offset_of__timer_5() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ____timer_5)); }
	inline float get__timer_5() const { return ____timer_5; }
	inline float* get_address_of__timer_5() { return &____timer_5; }
	inline void set__timer_5(float value)
	{
		____timer_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
