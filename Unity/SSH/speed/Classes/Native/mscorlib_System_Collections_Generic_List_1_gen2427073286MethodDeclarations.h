﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::.ctor()
#define List_1__ctor_m139868023(__this, method) ((  void (*) (List_1_t2427073286 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3571918367(__this, ___collection0, method) ((  void (*) (List_1_t2427073286 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::.ctor(System.Int32)
#define List_1__ctor_m2816604065(__this, ___capacity0, method) ((  void (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::.cctor()
#define List_1__cctor_m775710351(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3467287362(__this, method) ((  Il2CppObject* (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m4055737088(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2427073286 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3183842085(__this, method) ((  Il2CppObject * (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1423269244(__this, ___item0, method) ((  int32_t (*) (List_1_t2427073286 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2523748870(__this, ___item0, method) ((  bool (*) (List_1_t2427073286 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2036046378(__this, ___item0, method) ((  int32_t (*) (List_1_t2427073286 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3978589033(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2427073286 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1550973769(__this, ___item0, method) ((  void (*) (List_1_t2427073286 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m744802797(__this, method) ((  bool (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1767076136(__this, method) ((  bool (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2586030262(__this, method) ((  Il2CppObject * (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2214051701(__this, method) ((  bool (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2892020236(__this, method) ((  bool (*) (List_1_t2427073286 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1462118649(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1410203174(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2427073286 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Add(T)
#define List_1_Add_m2467428667(__this, ___item0, method) ((  void (*) (List_1_t2427073286 *, GameObjectU5BU5D_t3057952154*, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1289379698(__this, ___newCount0, method) ((  void (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m4183196890(__this, ___collection0, method) ((  void (*) (List_1_t2427073286 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2190495018(__this, ___enumerable0, method) ((  void (*) (List_1_t2427073286 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1263389185(__this, ___collection0, method) ((  void (*) (List_1_t2427073286 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.GameObject[]>::AsReadOnly()
#define List_1_AsReadOnly_m82509900(__this, method) ((  ReadOnlyCollection_1_t3243737846 * (*) (List_1_t2427073286 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Clear()
#define List_1_Clear_m820087777(__this, method) ((  void (*) (List_1_t2427073286 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Contains(T)
#define List_1_Contains_m4016840631(__this, ___item0, method) ((  bool (*) (List_1_t2427073286 *, GameObjectU5BU5D_t3057952154*, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1515179189(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2427073286 *, GameObjectU5BU5DU5BU5D_t3991477951*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Find(System.Predicate`1<T>)
#define List_1_Find_m3229057575(__this, ___match0, method) ((  GameObjectU5BU5D_t3057952154* (*) (List_1_t2427073286 *, Predicate_1_t1500922269 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m325441792(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1500922269 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1021293951(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2427073286 *, int32_t, int32_t, Predicate_1_t1500922269 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.GameObject[]>::GetEnumerator()
#define List_1_GetEnumerator_m2697303212(__this, method) ((  Enumerator_t1961802960  (*) (List_1_t2427073286 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::IndexOf(T)
#define List_1_IndexOf_m277218133(__this, ___item0, method) ((  int32_t (*) (List_1_t2427073286 *, GameObjectU5BU5D_t3057952154*, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m916366876(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2427073286 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m4069730353(__this, ___index0, method) ((  void (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Insert(System.Int32,T)
#define List_1_Insert_m513258718(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2427073286 *, int32_t, GameObjectU5BU5D_t3057952154*, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3217311443(__this, ___collection0, method) ((  void (*) (List_1_t2427073286 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Remove(T)
#define List_1_Remove_m3302213880(__this, ___item0, method) ((  bool (*) (List_1_t2427073286 *, GameObjectU5BU5D_t3057952154*, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2489411140(__this, ___match0, method) ((  int32_t (*) (List_1_t2427073286 *, Predicate_1_t1500922269 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m652001450(__this, ___index0, method) ((  void (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Reverse()
#define List_1_Reverse_m1212607350(__this, method) ((  void (*) (List_1_t2427073286 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Sort()
#define List_1_Sort_m3277979034(__this, method) ((  void (*) (List_1_t2427073286 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m895469703(__this, ___comparison0, method) ((  void (*) (List_1_t2427073286 *, Comparison_1_t24723709 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.GameObject[]>::ToArray()
#define List_1_ToArray_m84093739(__this, method) ((  GameObjectU5BU5DU5BU5D_t3991477951* (*) (List_1_t2427073286 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::TrimExcess()
#define List_1_TrimExcess_m786512997(__this, method) ((  void (*) (List_1_t2427073286 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::get_Capacity()
#define List_1_get_Capacity_m3614648191(__this, method) ((  int32_t (*) (List_1_t2427073286 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3547108250(__this, ___value0, method) ((  void (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject[]>::get_Count()
#define List_1_get_Count_m2674320780(__this, method) ((  int32_t (*) (List_1_t2427073286 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.GameObject[]>::get_Item(System.Int32)
#define List_1_get_Item_m3439907018(__this, ___index0, method) ((  GameObjectU5BU5D_t3057952154* (*) (List_1_t2427073286 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject[]>::set_Item(System.Int32,T)
#define List_1_set_Item_m376610005(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2427073286 *, int32_t, GameObjectU5BU5D_t3057952154*, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
