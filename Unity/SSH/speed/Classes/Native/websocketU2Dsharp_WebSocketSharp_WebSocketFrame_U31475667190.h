﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct U3CdumpU3Ec__AnonStorey1_t3897361314;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2
struct  U3CdumpU3Ec__AnonStorey2_t1475667190  : public Il2CppObject
{
public:
	// System.Int64 WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::lineCnt
	int64_t ___lineCnt_0;
	// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1 WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::<>f__ref$1
	U3CdumpU3Ec__AnonStorey1_t3897361314 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_lineCnt_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey2_t1475667190, ___lineCnt_0)); }
	inline int64_t get_lineCnt_0() const { return ___lineCnt_0; }
	inline int64_t* get_address_of_lineCnt_0() { return &___lineCnt_0; }
	inline void set_lineCnt_0(int64_t value)
	{
		___lineCnt_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey2_t1475667190, ___U3CU3Ef__refU241_1)); }
	inline U3CdumpU3Ec__AnonStorey1_t3897361314 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CdumpU3Ec__AnonStorey1_t3897361314 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CdumpU3Ec__AnonStorey1_t3897361314 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
