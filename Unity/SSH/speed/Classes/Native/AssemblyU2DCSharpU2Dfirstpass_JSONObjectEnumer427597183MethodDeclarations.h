﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObjectEnumer
struct JSONObjectEnumer_t427597183;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"

// System.Void JSONObjectEnumer::.ctor(JSONObject)
extern "C"  void JSONObjectEnumer__ctor_m34994965 (JSONObjectEnumer_t427597183 * __this, JSONObject_t1971882247 * ___jsonObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObjectEnumer::MoveNext()
extern "C"  bool JSONObjectEnumer_MoveNext_m3554948016 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObjectEnumer::Reset()
extern "C"  void JSONObjectEnumer_Reset_m2137692453 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObjectEnumer::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * JSONObjectEnumer_System_Collections_IEnumerator_get_Current_m2823805388 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObjectEnumer::get_Current()
extern "C"  JSONObject_t1971882247 * JSONObjectEnumer_get_Current_m78793822 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
