﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3019169210MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3541777374(__this, ___q0, method) ((  void (*) (Enumerator_t3219771641 *, Queue_1_t2709708561 *, const MethodInfo*))Enumerator__ctor_m677001007_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2392847021(__this, method) ((  void (*) (Enumerator_t3219771641 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m373072478_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2084949481(__this, method) ((  Il2CppObject * (*) (Enumerator_t3219771641 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2167685344_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::Dispose()
#define Enumerator_Dispose_m537647622(__this, method) ((  void (*) (Enumerator_t3219771641 *, const MethodInfo*))Enumerator_Dispose_m575349149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::MoveNext()
#define Enumerator_MoveNext_m4244991849(__this, method) ((  bool (*) (Enumerator_t3219771641 *, const MethodInfo*))Enumerator_MoveNext_m742418190_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::get_Current()
#define Enumerator_get_Current_m2669820510(__this, method) ((  MessageEventArgs_t2890051726 * (*) (Enumerator_t3219771641 *, const MethodInfo*))Enumerator_get_Current_m1613610405_gshared)(__this, method)
