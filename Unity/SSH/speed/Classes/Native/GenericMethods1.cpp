﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t106372939;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1186599945;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext870230697.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Converter_2_gen106372939.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Converter_2_gen106372939MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1186599945.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"

// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponents_TisIl2CppObject_m3618562997_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m3618562997(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m3618562997_gshared)(__this, method)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m851581932_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m851581932(__this, ___includeInactive0, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m851581932_gshared)(__this, ___includeInactive0, method)
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method);
#define GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873(__this, ___includeInactive0, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, bool, const MethodInfo*))GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_gshared)(__this, ___includeInactive0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588(__this, ___channel0, method) ((  ObjectU5BU5D_t3614634134* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222(__this, ___channel0, ___format1, ___dim2, method) ((  ObjectU5BU5D_t3614634134* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t30278651* Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417(__this, ___channel0, ___format1, ___dim2, method) ((  Color32U5BU5D_t30278651* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716(__this, ___channel0, method) ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426(__this, ___channel0, ___format1, ___dim2, method) ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537(__this, ___channel0, method) ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415(__this, ___channel0, ___format1, ___dim2, method) ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442(__this, ___channel0, method) ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692(__this, ___channel0, ___format1, ___dim2, method) ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisIl2CppObject_m1343658011_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m1343658011(__this /* static, unused */, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1343658011_gshared)(__this /* static, unused */, method)
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t3614634134* Resources_ConvertObjects_TisIl2CppObject_m2571720668_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t4217747464* ___rawObjects0, const MethodInfo* method);
#define Resources_ConvertObjects_TisIl2CppObject_m2571720668(__this /* static, unused */, ___rawObjects0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t4217747464*, const MethodInfo*))Resources_ConvertObjects_TisIl2CppObject_m2571720668_gshared)(__this /* static, unused */, ___rawObjects0, method)
// T[] WebSocketSharp.Ext::Reverse<System.Byte>(T[])
extern "C"  ByteU5BU5D_t3397334013* Ext_Reverse_TisByte_t3683104436_m1170405293_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, const MethodInfo* method);
#define Ext_Reverse_TisByte_t3683104436_m1170405293(__this /* static, unused */, ___array0, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, const MethodInfo*))Ext_Reverse_TisByte_t3683104436_m1170405293_gshared)(__this /* static, unused */, ___array0, method)
// T[] WebSocketSharp.Ext::Reverse<System.Object>(T[])
extern "C"  ObjectU5BU5D_t3614634134* Ext_Reverse_TisIl2CppObject_m3008685552_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method);
#define Ext_Reverse_TisIl2CppObject_m3008685552(__this /* static, unused */, ___array0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, const MethodInfo*))Ext_Reverse_TisIl2CppObject_m3008685552_gshared)(__this /* static, unused */, ___array0, method)
// T[] WebSocketSharp.Ext::SubArray<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* Ext_SubArray_TisByte_t3683104436_m2352342620_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method);
#define Ext_SubArray_TisByte_t3683104436_m2352342620(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, int32_t, const MethodInfo*))Ext_SubArray_TisByte_t3683104436_m2352342620_gshared)(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method)
// T[] WebSocketSharp.Ext::SubArray<System.Byte>(T[],System.Int64,System.Int64)
extern "C"  ByteU5BU5D_t3397334013* Ext_SubArray_TisByte_t3683104436_m2761078838_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, int64_t ___startIndex1, int64_t ___length2, const MethodInfo* method);
#define Ext_SubArray_TisByte_t3683104436_m2761078838(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int64_t, int64_t, const MethodInfo*))Ext_SubArray_TisByte_t3683104436_m2761078838_gshared)(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method)
// T[] WebSocketSharp.Ext::SubArray<System.Object>(T[],System.Int32,System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* Ext_SubArray_TisIl2CppObject_m3752645683_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method);
#define Ext_SubArray_TisIl2CppObject_m3752645683(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, int32_t, const MethodInfo*))Ext_SubArray_TisIl2CppObject_m3752645683_gshared)(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method)
// T[] WebSocketSharp.Ext::SubArray<System.Object>(T[],System.Int64,System.Int64)
extern "C"  ObjectU5BU5D_t3614634134* Ext_SubArray_TisIl2CppObject_m841950999_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, int64_t ___startIndex1, int64_t ___length2, const MethodInfo* method);
#define Ext_SubArray_TisIl2CppObject_m841950999(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int64_t, int64_t, const MethodInfo*))Ext_SubArray_TisIl2CppObject_m841950999_gshared)(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t3614634134* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Converter_2_t106372939 * ___converter1, const MethodInfo* method);
#define Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546(__this /* static, unused */, ___array0, ___converter1, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Converter_2_t106372939 *, const MethodInfo*))Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_gshared)(__this /* static, unused */, ___array0, ___converter1, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t1756533147 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, BaseEventData_t2681005625 * ___eventData1, EventFunction_1_t1186599945 * ___callbackFunction2, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared)(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  bool ExecuteEvents_Execute_TisIl2CppObject_m4168308247_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, BaseEventData_t2681005625 * ___eventData1, EventFunction_1_t1186599945 * ___functor2, const MethodInfo* method);
#define ExecuteEvents_Execute_TisIl2CppObject_m4168308247(__this /* static, unused */, ___target0, ___eventData1, ___functor2, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))ExecuteEvents_Execute_TisIl2CppObject_m4168308247_gshared)(__this /* static, unused */, ___target0, ___eventData1, ___functor2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, const MethodInfo* method);
#define ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576(__this /* static, unused */, ___root0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_gshared)(__this /* static, unused */, ___root0, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::CanHandleEvent<System.Object>(UnityEngine.GameObject)
extern "C"  bool ExecuteEvents_CanHandleEvent_TisIl2CppObject_m1201779629_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define ExecuteEvents_CanHandleEvent_TisIl2CppObject_m1201779629(__this /* static, unused */, ___go0, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))ExecuteEvents_CanHandleEvent_TisIl2CppObject_m1201779629_gshared)(__this /* static, unused */, ___go0, method)
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m3618562997_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponents_TisIl2CppObject_m3618562997_gshared (GameObject_t1756533147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m3618562997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m3486524399((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m851581932_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m851581932_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m851581932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m3486524399((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_gshared (GameObject_t1756533147 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m3479568873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1756533147 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m3486524399((GameObject_t1756533147 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m4188594588_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		ObjectU5BU5D_t3614634134* L_3 = ((  ObjectU5BU5D_t3614634134* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t3614634134*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1450958222_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		ObjectU5BU5D_t3614634134* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t30278651* Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m2030100417_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Color32U5BU5D_t30278651* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Color32U5BU5D_t30278651*)((Color32U5BU5D_t30278651*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Color32U5BU5D_t30278651*)((Color32U5BU5D_t30278651*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Color32U5BU5D_t30278651* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3651973716_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector2U5BU5D_t686124026* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		Vector2U5BU5D_t686124026* L_3 = ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector2U5BU5D_t686124026*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2U5BU5D_t686124026* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m2487531426_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector2U5BU5D_t686124026* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector2U5BU5D_t686124026*)((Vector2U5BU5D_t686124026*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector2U5BU5D_t686124026*)((Vector2U5BU5D_t686124026*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector2U5BU5D_t686124026* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2367580537_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		Vector3U5BU5D_t1172311765* L_3 = ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector3U5BU5D_t1172311765*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector3U5BU5D_t1172311765* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m2101409415_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector3U5BU5D_t1172311765*)((Vector3U5BU5D_t1172311765*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector3U5BU5D_t1172311765*)((Vector3U5BU5D_t1172311765*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector3U5BU5D_t1172311765* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m295947442_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector4U5BU5D_t1658499504* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m153181993(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t1356156583 *)__this);
		Vector4U5BU5D_t1658499504* L_3 = ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t1356156583 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector4U5BU5D_t1658499504*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector4U5BU5D_t1658499504* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m189379692_gshared (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector4U5BU5D_t1658499504* V_0 = NULL;
	{
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_0 = Mesh_get_canAccess_m2763498171((Mesh_t1356156583 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		bool L_2 = Mesh_HasChannel_m3616583481((Mesh_t1356156583 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t1356156583 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1663415136((Mesh_t1356156583 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector4U5BU5D_t1658499504*)((Vector4U5BU5D_t1658499504*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t1356156583 *)__this);
		Mesh_PrintErrorCantAccessMesh_m2827771108((Mesh_t1356156583 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector4U5BU5D_t1658499504*)((Vector4U5BU5D_t1658499504*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector4U5BU5D_t1658499504* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m1343658011_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisIl2CppObject_m1343658011_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m1343658011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_2 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t4217747464*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t4217747464*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (ObjectU5BU5D_t3614634134*)L_2;
		goto IL_001b;
	}

IL_001b:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t3614634134* Resources_ConvertObjects_TisIl2CppObject_m2571720668_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t4217747464* ___rawObjects0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t4217747464* L_0 = ___rawObjects0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = (ObjectU5BU5D_t3614634134*)NULL;
		goto IL_0041;
	}

IL_000e:
	{
		ObjectU5BU5D_t4217747464* L_1 = ___rawObjects0;
		NullCheck(L_1);
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_2 = (int32_t)0;
		goto IL_0031;
	}

IL_001e:
	{
		ObjectU5BU5D_t3614634134* L_2 = V_1;
		int32_t L_3 = V_2;
		ObjectU5BU5D_t4217747464* L_4 = ___rawObjects0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Object_t1021602117 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_8 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_2;
		ObjectU5BU5D_t3614634134* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_11 = V_1;
		V_0 = (ObjectU5BU5D_t3614634134*)L_11;
		goto IL_0041;
	}

IL_0041:
	{
		ObjectU5BU5D_t3614634134* L_12 = V_0;
		return L_12;
	}
}
// T[] WebSocketSharp.Ext::Reverse<System.Byte>(T[])
extern "C"  ByteU5BU5D_t3397334013* Ext_Reverse_TisByte_t3683104436_m1170405293_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___array0;
		NullCheck(L_0);
		V_0 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		V_1 = (ByteU5BU5D_t3397334013*)((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_1));
		int32_t L_2 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_2-(int32_t)1));
		V_3 = (int32_t)0;
		goto IL_002a;
	}

IL_0016:
	{
		ByteU5BU5D_t3397334013* L_3 = V_1;
		int32_t L_4 = V_3;
		ByteU5BU5D_t3397334013* L_5 = ___array0;
		int32_t L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)L_6-(int32_t)L_7));
		uint8_t L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint8_t)L_9);
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_2;
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			goto IL_0016;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_13 = V_1;
		return L_13;
	}
}
// T[] WebSocketSharp.Ext::Reverse<System.Object>(T[])
extern "C"  ObjectU5BU5D_t3614634134* Ext_Reverse_TisIl2CppObject_m3008685552_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		NullCheck(L_0);
		V_0 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_1));
		int32_t L_2 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_2-(int32_t)1));
		V_3 = (int32_t)0;
		goto IL_002a;
	}

IL_0016:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_1;
		int32_t L_4 = V_3;
		ObjectU5BU5D_t3614634134* L_5 = ___array0;
		int32_t L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)L_6-(int32_t)L_7));
		Il2CppObject * L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Il2CppObject *)L_9);
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_2;
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_13 = V_1;
		return L_13;
	}
}
// T[] WebSocketSharp.Ext::SubArray<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* Ext_SubArray_TisByte_t3683104436_m2352342620_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___array0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))));
		V_0 = (int32_t)L_2;
		if (L_2)
		{
			goto IL_0017;
		}
	}

IL_0010:
	{
		return ((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_0017:
	{
		int32_t L_3 = ___startIndex1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = ___length2;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_5 = ___startIndex1;
		int32_t L_6 = ___length2;
		int32_t L_7 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)L_7)))
		{
			goto IL_0035;
		}
	}

IL_002e:
	{
		return ((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_0035:
	{
		int32_t L_8 = ___startIndex1;
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = ___length2;
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0044;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = ___array0;
		return L_11;
	}

IL_0044:
	{
		int32_t L_12 = ___length2;
		V_1 = (ByteU5BU5D_t3397334013*)((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_12));
		ByteU5BU5D_t3397334013* L_13 = ___array0;
		int32_t L_14 = ___startIndex1;
		ByteU5BU5D_t3397334013* L_15 = V_1;
		int32_t L_16 = ___length2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, (int32_t)L_14, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)0, (int32_t)L_16, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_17 = V_1;
		return L_17;
	}
}
// T[] WebSocketSharp.Ext::SubArray<System.Byte>(T[],System.Int64,System.Int64)
extern "C"  ByteU5BU5D_t3397334013* Ext_SubArray_TisByte_t3683104436_m2761078838_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, int64_t ___startIndex1, int64_t ___length2, const MethodInfo* method)
{
	int64_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___array0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		int64_t L_2 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		int64_t L_3 = (int64_t)L_2;
		V_0 = (int64_t)L_3;
		if ((!(((uint64_t)L_3) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001c;
		}
	}

IL_0015:
	{
		return ((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_001c:
	{
		int64_t L_4 = ___startIndex1;
		if ((((int64_t)L_4) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0035;
		}
	}
	{
		int64_t L_5 = ___length2;
		if ((((int64_t)L_5) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0035;
		}
	}
	{
		int64_t L_6 = ___startIndex1;
		int64_t L_7 = ___length2;
		int64_t L_8 = V_0;
		if ((((int64_t)((int64_t)((int64_t)L_6+(int64_t)L_7))) <= ((int64_t)L_8)))
		{
			goto IL_003c;
		}
	}

IL_0035:
	{
		return ((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_003c:
	{
		int64_t L_9 = ___startIndex1;
		if ((!(((uint64_t)L_9) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_004d;
		}
	}
	{
		int64_t L_10 = ___length2;
		int64_t L_11 = V_0;
		if ((!(((uint64_t)L_10) == ((uint64_t)L_11))))
		{
			goto IL_004d;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_12 = ___array0;
		return L_12;
	}

IL_004d:
	{
		int64_t L_13 = ___length2;
		if ((int64_t)(L_13) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_1 = (ByteU5BU5D_t3397334013*)((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((intptr_t)L_13))));
		ByteU5BU5D_t3397334013* L_14 = ___array0;
		int64_t L_15 = ___startIndex1;
		ByteU5BU5D_t3397334013* L_16 = V_1;
		int64_t L_17 = ___length2;
		Array_Copy_m1969461849(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_14, (int64_t)L_15, (Il2CppArray *)(Il2CppArray *)L_16, (int64_t)(((int64_t)((int64_t)0))), (int64_t)L_17, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_18 = V_1;
		return L_18;
	}
}
// T[] WebSocketSharp.Ext::SubArray<System.Object>(T[],System.Int32,System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* Ext_SubArray_TisIl2CppObject_m3752645683_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))));
		V_0 = (int32_t)L_2;
		if (L_2)
		{
			goto IL_0017;
		}
	}

IL_0010:
	{
		return ((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_0017:
	{
		int32_t L_3 = ___startIndex1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = ___length2;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_5 = ___startIndex1;
		int32_t L_6 = ___length2;
		int32_t L_7 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)L_7)))
		{
			goto IL_0035;
		}
	}

IL_002e:
	{
		return ((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_0035:
	{
		int32_t L_8 = ___startIndex1;
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = ___length2;
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0044;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_11 = ___array0;
		return L_11;
	}

IL_0044:
	{
		int32_t L_12 = ___length2;
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_12));
		ObjectU5BU5D_t3614634134* L_13 = ___array0;
		int32_t L_14 = ___startIndex1;
		ObjectU5BU5D_t3614634134* L_15 = V_1;
		int32_t L_16 = ___length2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, (int32_t)L_14, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)0, (int32_t)L_16, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_17 = V_1;
		return L_17;
	}
}
// T[] WebSocketSharp.Ext::SubArray<System.Object>(T[],System.Int64,System.Int64)
extern "C"  ObjectU5BU5D_t3614634134* Ext_SubArray_TisIl2CppObject_m841950999_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, int64_t ___startIndex1, int64_t ___length2, const MethodInfo* method)
{
	int64_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		int64_t L_2 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		int64_t L_3 = (int64_t)L_2;
		V_0 = (int64_t)L_3;
		if ((!(((uint64_t)L_3) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001c;
		}
	}

IL_0015:
	{
		return ((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_001c:
	{
		int64_t L_4 = ___startIndex1;
		if ((((int64_t)L_4) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0035;
		}
	}
	{
		int64_t L_5 = ___length2;
		if ((((int64_t)L_5) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0035;
		}
	}
	{
		int64_t L_6 = ___startIndex1;
		int64_t L_7 = ___length2;
		int64_t L_8 = V_0;
		if ((((int64_t)((int64_t)((int64_t)L_6+(int64_t)L_7))) <= ((int64_t)L_8)))
		{
			goto IL_003c;
		}
	}

IL_0035:
	{
		return ((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)0));
	}

IL_003c:
	{
		int64_t L_9 = ___startIndex1;
		if ((!(((uint64_t)L_9) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_004d;
		}
	}
	{
		int64_t L_10 = ___length2;
		int64_t L_11 = V_0;
		if ((!(((uint64_t)L_10) == ((uint64_t)L_11))))
		{
			goto IL_004d;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_12 = ___array0;
		return L_12;
	}

IL_004d:
	{
		int64_t L_13 = ___length2;
		if ((int64_t)(L_13) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_1 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((intptr_t)L_13))));
		ObjectU5BU5D_t3614634134* L_14 = ___array0;
		int64_t L_15 = ___startIndex1;
		ObjectU5BU5D_t3614634134* L_16 = V_1;
		int64_t L_17 = ___length2;
		Array_Copy_m1969461849(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_14, (int64_t)L_15, (Il2CppArray *)(Il2CppArray *)L_16, (int64_t)(((int64_t)((int64_t)0))), (int64_t)L_17, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_18 = V_1;
		return L_18;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral3706307074;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_MetadataUsageId;
extern "C"  ObjectU5BU5D_t3614634134* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, Converter_2_t106372939 * ___converter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2423585546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t106372939 * L_2 = ___converter1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3706307074, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t106372939 * L_7 = ___converter1;
		ObjectU5BU5D_t3614634134* L_8 = ___array0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck((Converter_2_t106372939 *)L_7);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (Converter_2_t106372939 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Converter_2_t106372939 *)L_7, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_1;
		ObjectU5BU5D_t3614634134* L_15 = ___array0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_16 = V_0;
		return L_16;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m3519500779_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2072476413_MethodInfo_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, BaseEventData_t2681005625 * ___eventData1, EventFunction_1_t1186599945 * ___callbackFunction2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2541874163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3275118058 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		List_1_t2644239190 * L_1 = ((ExecuteEvents_t1693084770_StaticFields*)ExecuteEvents_t1693084770_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m713190182(NULL /*static, unused*/, (GameObject_t1756533147 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_0043;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		List_1_t2644239190 * L_2 = ((ExecuteEvents_t1693084770_StaticFields*)ExecuteEvents_t1693084770_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t2644239190 *)L_2);
		Transform_t3275118058 * L_4 = List_1_get_Item_m3519500779((List_1_t2644239190 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_m3519500779_MethodInfo_var);
		V_1 = (Transform_t3275118058 *)L_4;
		Transform_t3275118058 * L_5 = V_1;
		NullCheck((Component_t3819376471 *)L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t2681005625 * L_7 = ___eventData1;
		EventFunction_1_t1186599945 * L_8 = ___callbackFunction2;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, BaseEventData_t2681005625 *, EventFunction_1_t1186599945 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_6, (BaseEventData_t2681005625 *)L_7, (EventFunction_1_t1186599945 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		Transform_t3275118058 * L_10 = V_1;
		NullCheck((Component_t3819376471 *)L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_10, /*hidden argument*/NULL);
		V_2 = (GameObject_t1756533147 *)L_11;
		goto IL_005a;
	}

IL_003e:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		List_1_t2644239190 * L_14 = ((ExecuteEvents_t1693084770_StaticFields*)ExecuteEvents_t1693084770_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		NullCheck((List_1_t2644239190 *)L_14);
		int32_t L_15 = List_1_get_Count_m2072476413((List_1_t2644239190 *)L_14, /*hidden argument*/List_1_get_Count_m2072476413_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		V_2 = (GameObject_t1756533147 *)NULL;
		goto IL_005a;
	}

IL_005a:
	{
		GameObject_t1756533147 * L_16 = V_2;
		return L_16;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteEvents_t1693084770_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisIl2CppObject_m3333041576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		goto IL_0058;
	}

IL_0014:
	{
		GameObject_t1756533147 * L_2 = ___root0;
		NullCheck((GameObject_t1756533147 *)L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_2, /*hidden argument*/NULL);
		V_1 = (Transform_t3275118058 *)L_3;
		goto IL_0045;
	}

IL_0020:
	{
		Transform_t3275118058 * L_4 = V_1;
		NullCheck((Component_t3819376471 *)L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t1693084770_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1756533147 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3275118058 * L_7 = V_1;
		NullCheck((Component_t3819376471 *)L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_7, /*hidden argument*/NULL);
		V_0 = (GameObject_t1756533147 *)L_8;
		goto IL_0058;
	}

IL_003d:
	{
		Transform_t3275118058 * L_9 = V_1;
		NullCheck((Transform_t3275118058 *)L_9);
		Transform_t3275118058 * L_10 = Transform_get_parent_m147407266((Transform_t3275118058 *)L_9, /*hidden argument*/NULL);
		V_1 = (Transform_t3275118058 *)L_10;
	}

IL_0045:
	{
		Transform_t3275118058 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		goto IL_0058;
	}

IL_0058:
	{
		GameObject_t1756533147 * L_13 = V_0;
		return L_13;
	}
}
