﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Func`2<System.String[],WebSocketSharp.HttpResponse>
struct Func_2_t1532221928;

#include "websocketU2Dsharp_WebSocketSharp_HttpBase4283398485.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpRequest
struct  HttpRequest_t1845443631  : public HttpBase_t4283398485
{
public:
	// System.String WebSocketSharp.HttpRequest::_method
	String_t* ____method_3;
	// System.String WebSocketSharp.HttpRequest::_uri
	String_t* ____uri_4;

public:
	inline static int32_t get_offset_of__method_3() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631, ____method_3)); }
	inline String_t* get__method_3() const { return ____method_3; }
	inline String_t** get_address_of__method_3() { return &____method_3; }
	inline void set__method_3(String_t* value)
	{
		____method_3 = value;
		Il2CppCodeGenWriteBarrier(&____method_3, value);
	}

	inline static int32_t get_offset_of__uri_4() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631, ____uri_4)); }
	inline String_t* get__uri_4() const { return ____uri_4; }
	inline String_t** get_address_of__uri_4() { return &____uri_4; }
	inline void set__uri_4(String_t* value)
	{
		____uri_4 = value;
		Il2CppCodeGenWriteBarrier(&____uri_4, value);
	}
};

struct HttpRequest_t1845443631_StaticFields
{
public:
	// System.Func`2<System.String[],WebSocketSharp.HttpResponse> WebSocketSharp.HttpRequest::<>f__mg$cache0
	Func_2_t1532221928 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline Func_2_t1532221928 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline Func_2_t1532221928 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(Func_2_t1532221928 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
