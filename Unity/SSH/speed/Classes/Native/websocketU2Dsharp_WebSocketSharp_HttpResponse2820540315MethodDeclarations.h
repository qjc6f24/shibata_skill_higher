﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HttpResponse
struct HttpResponse_t2820540315;
// System.String
struct String_t;
// System.Version
struct Version_t1755874712;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t4248997468;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Version1755874712.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"

// System.Void WebSocketSharp.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HttpResponse__ctor_m948363293 (HttpResponse_t2820540315 * __this, String_t* ___code0, String_t* ___reason1, Version_t1755874712 * ___version2, NameValueCollection_t3047564564 * ___headers3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.CookieCollection WebSocketSharp.HttpResponse::get_Cookies()
extern "C"  CookieCollection_t4248997468 * HttpResponse_get_Cookies_m1833540882 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HttpResponse::get_HasConnectionClose()
extern "C"  bool HttpResponse_get_HasConnectionClose_m987163280 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HttpResponse::get_IsProxyAuthenticationRequired()
extern "C"  bool HttpResponse_get_IsProxyAuthenticationRequired_m1945167221 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HttpResponse::get_IsRedirect()
extern "C"  bool HttpResponse_get_IsRedirect_m3194810376 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HttpResponse::get_IsUnauthorized()
extern "C"  bool HttpResponse_get_IsUnauthorized_m1649884238 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HttpResponse::get_IsWebSocketResponse()
extern "C"  bool HttpResponse_get_IsWebSocketResponse_m3251240054 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HttpResponse::get_StatusCode()
extern "C"  String_t* HttpResponse_get_StatusCode_m1116886410 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpResponse WebSocketSharp.HttpResponse::Parse(System.String[])
extern "C"  HttpResponse_t2820540315 * HttpResponse_Parse_m1679504445 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___headerParts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HttpResponse::ToString()
extern "C"  String_t* HttpResponse_ToString_m3489423176 (HttpResponse_t2820540315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
