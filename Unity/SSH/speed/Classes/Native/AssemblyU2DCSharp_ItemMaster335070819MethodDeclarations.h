﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemMaster
struct ItemMaster_t335070819;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemMaster::.ctor()
extern "C"  void ItemMaster__ctor_m2788594876 (ItemMaster_t335070819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemMaster::Start()
extern "C"  void ItemMaster_Start_m3069863920 (ItemMaster_t335070819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
