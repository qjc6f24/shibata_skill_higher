﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;
// PlayerController
struct PlayerController_t4148409433;
// WebSocketClient
struct WebSocketClient_t3274474108;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<UnityEngine.GameObject[]>
struct List_1_t2427073286;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardController
struct  CardController_t2595996862  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 CardController::_topLeft
	Vector3_t2243707580  ____topLeft_8;
	// UnityEngine.Vector3 CardController::_bottomRight
	Vector3_t2243707580  ____bottomRight_9;
	// System.Single CardController::_screenWidth
	float ____screenWidth_10;
	// System.Single CardController::_screenHeight
	float ____screenHeight_11;
	// UnityEngine.Vector3 CardController::_centerPos
	Vector3_t2243707580  ____centerPos_12;
	// UnityEngine.Camera CardController::mainCamera
	Camera_t189460977 * ___mainCamera_13;
	// PlayerController CardController::_playerController
	PlayerController_t4148409433 * ____playerController_14;
	// WebSocketClient CardController::_webSocketClient
	WebSocketClient_t3274474108 * ____webSocketClient_15;
	// UnityEngine.GameObject[] CardController::cards
	GameObjectU5BU5D_t3057952154* ___cards_19;
	// System.Collections.Generic.List`1<UnityEngine.GameObject[]> CardController::_objects
	List_1_t2427073286 * ____objects_25;
	// System.Int32 CardController::_phase
	int32_t ____phase_28;

public:
	inline static int32_t get_offset_of__topLeft_8() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____topLeft_8)); }
	inline Vector3_t2243707580  get__topLeft_8() const { return ____topLeft_8; }
	inline Vector3_t2243707580 * get_address_of__topLeft_8() { return &____topLeft_8; }
	inline void set__topLeft_8(Vector3_t2243707580  value)
	{
		____topLeft_8 = value;
	}

	inline static int32_t get_offset_of__bottomRight_9() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____bottomRight_9)); }
	inline Vector3_t2243707580  get__bottomRight_9() const { return ____bottomRight_9; }
	inline Vector3_t2243707580 * get_address_of__bottomRight_9() { return &____bottomRight_9; }
	inline void set__bottomRight_9(Vector3_t2243707580  value)
	{
		____bottomRight_9 = value;
	}

	inline static int32_t get_offset_of__screenWidth_10() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____screenWidth_10)); }
	inline float get__screenWidth_10() const { return ____screenWidth_10; }
	inline float* get_address_of__screenWidth_10() { return &____screenWidth_10; }
	inline void set__screenWidth_10(float value)
	{
		____screenWidth_10 = value;
	}

	inline static int32_t get_offset_of__screenHeight_11() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____screenHeight_11)); }
	inline float get__screenHeight_11() const { return ____screenHeight_11; }
	inline float* get_address_of__screenHeight_11() { return &____screenHeight_11; }
	inline void set__screenHeight_11(float value)
	{
		____screenHeight_11 = value;
	}

	inline static int32_t get_offset_of__centerPos_12() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____centerPos_12)); }
	inline Vector3_t2243707580  get__centerPos_12() const { return ____centerPos_12; }
	inline Vector3_t2243707580 * get_address_of__centerPos_12() { return &____centerPos_12; }
	inline void set__centerPos_12(Vector3_t2243707580  value)
	{
		____centerPos_12 = value;
	}

	inline static int32_t get_offset_of_mainCamera_13() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ___mainCamera_13)); }
	inline Camera_t189460977 * get_mainCamera_13() const { return ___mainCamera_13; }
	inline Camera_t189460977 ** get_address_of_mainCamera_13() { return &___mainCamera_13; }
	inline void set_mainCamera_13(Camera_t189460977 * value)
	{
		___mainCamera_13 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_13, value);
	}

	inline static int32_t get_offset_of__playerController_14() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____playerController_14)); }
	inline PlayerController_t4148409433 * get__playerController_14() const { return ____playerController_14; }
	inline PlayerController_t4148409433 ** get_address_of__playerController_14() { return &____playerController_14; }
	inline void set__playerController_14(PlayerController_t4148409433 * value)
	{
		____playerController_14 = value;
		Il2CppCodeGenWriteBarrier(&____playerController_14, value);
	}

	inline static int32_t get_offset_of__webSocketClient_15() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____webSocketClient_15)); }
	inline WebSocketClient_t3274474108 * get__webSocketClient_15() const { return ____webSocketClient_15; }
	inline WebSocketClient_t3274474108 ** get_address_of__webSocketClient_15() { return &____webSocketClient_15; }
	inline void set__webSocketClient_15(WebSocketClient_t3274474108 * value)
	{
		____webSocketClient_15 = value;
		Il2CppCodeGenWriteBarrier(&____webSocketClient_15, value);
	}

	inline static int32_t get_offset_of_cards_19() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ___cards_19)); }
	inline GameObjectU5BU5D_t3057952154* get_cards_19() const { return ___cards_19; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_cards_19() { return &___cards_19; }
	inline void set_cards_19(GameObjectU5BU5D_t3057952154* value)
	{
		___cards_19 = value;
		Il2CppCodeGenWriteBarrier(&___cards_19, value);
	}

	inline static int32_t get_offset_of__objects_25() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____objects_25)); }
	inline List_1_t2427073286 * get__objects_25() const { return ____objects_25; }
	inline List_1_t2427073286 ** get_address_of__objects_25() { return &____objects_25; }
	inline void set__objects_25(List_1_t2427073286 * value)
	{
		____objects_25 = value;
		Il2CppCodeGenWriteBarrier(&____objects_25, value);
	}

	inline static int32_t get_offset_of__phase_28() { return static_cast<int32_t>(offsetof(CardController_t2595996862, ____phase_28)); }
	inline int32_t get__phase_28() const { return ____phase_28; }
	inline int32_t* get_address_of__phase_28() { return &____phase_28; }
	inline void set__phase_28(int32_t value)
	{
		____phase_28 = value;
	}
};

struct CardController_t2595996862_StaticFields
{
public:
	// System.Int32 CardController::CARD_TYPE_COUNT
	int32_t ___CARD_TYPE_COUNT_16;
	// System.Int32 CardController::USERS_CARD_COUNT
	int32_t ___USERS_CARD_COUNT_17;
	// System.Int32 CardController::FIELD_CARD_COUNT
	int32_t ___FIELD_CARD_COUNT_18;
	// UnityEngine.GameObject[] CardController::_fieldObjects
	GameObjectU5BU5D_t3057952154* ____fieldObjects_20;
	// UnityEngine.GameObject[] CardController::_playerObjects
	GameObjectU5BU5D_t3057952154* ____playerObjects_21;
	// UnityEngine.GameObject[] CardController::_enemyObjects1
	GameObjectU5BU5D_t3057952154* ____enemyObjects1_22;
	// UnityEngine.GameObject[] CardController::_enemyObjects2
	GameObjectU5BU5D_t3057952154* ____enemyObjects2_23;
	// UnityEngine.GameObject[] CardController::_enemyObjects3
	GameObjectU5BU5D_t3057952154* ____enemyObjects3_24;
	// System.Int32 CardController::PHASE_INIT
	int32_t ___PHASE_INIT_26;
	// System.Int32 CardController::PHASE_PLAYING
	int32_t ___PHASE_PLAYING_27;

public:
	inline static int32_t get_offset_of_CARD_TYPE_COUNT_16() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ___CARD_TYPE_COUNT_16)); }
	inline int32_t get_CARD_TYPE_COUNT_16() const { return ___CARD_TYPE_COUNT_16; }
	inline int32_t* get_address_of_CARD_TYPE_COUNT_16() { return &___CARD_TYPE_COUNT_16; }
	inline void set_CARD_TYPE_COUNT_16(int32_t value)
	{
		___CARD_TYPE_COUNT_16 = value;
	}

	inline static int32_t get_offset_of_USERS_CARD_COUNT_17() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ___USERS_CARD_COUNT_17)); }
	inline int32_t get_USERS_CARD_COUNT_17() const { return ___USERS_CARD_COUNT_17; }
	inline int32_t* get_address_of_USERS_CARD_COUNT_17() { return &___USERS_CARD_COUNT_17; }
	inline void set_USERS_CARD_COUNT_17(int32_t value)
	{
		___USERS_CARD_COUNT_17 = value;
	}

	inline static int32_t get_offset_of_FIELD_CARD_COUNT_18() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ___FIELD_CARD_COUNT_18)); }
	inline int32_t get_FIELD_CARD_COUNT_18() const { return ___FIELD_CARD_COUNT_18; }
	inline int32_t* get_address_of_FIELD_CARD_COUNT_18() { return &___FIELD_CARD_COUNT_18; }
	inline void set_FIELD_CARD_COUNT_18(int32_t value)
	{
		___FIELD_CARD_COUNT_18 = value;
	}

	inline static int32_t get_offset_of__fieldObjects_20() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ____fieldObjects_20)); }
	inline GameObjectU5BU5D_t3057952154* get__fieldObjects_20() const { return ____fieldObjects_20; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__fieldObjects_20() { return &____fieldObjects_20; }
	inline void set__fieldObjects_20(GameObjectU5BU5D_t3057952154* value)
	{
		____fieldObjects_20 = value;
		Il2CppCodeGenWriteBarrier(&____fieldObjects_20, value);
	}

	inline static int32_t get_offset_of__playerObjects_21() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ____playerObjects_21)); }
	inline GameObjectU5BU5D_t3057952154* get__playerObjects_21() const { return ____playerObjects_21; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__playerObjects_21() { return &____playerObjects_21; }
	inline void set__playerObjects_21(GameObjectU5BU5D_t3057952154* value)
	{
		____playerObjects_21 = value;
		Il2CppCodeGenWriteBarrier(&____playerObjects_21, value);
	}

	inline static int32_t get_offset_of__enemyObjects1_22() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ____enemyObjects1_22)); }
	inline GameObjectU5BU5D_t3057952154* get__enemyObjects1_22() const { return ____enemyObjects1_22; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__enemyObjects1_22() { return &____enemyObjects1_22; }
	inline void set__enemyObjects1_22(GameObjectU5BU5D_t3057952154* value)
	{
		____enemyObjects1_22 = value;
		Il2CppCodeGenWriteBarrier(&____enemyObjects1_22, value);
	}

	inline static int32_t get_offset_of__enemyObjects2_23() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ____enemyObjects2_23)); }
	inline GameObjectU5BU5D_t3057952154* get__enemyObjects2_23() const { return ____enemyObjects2_23; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__enemyObjects2_23() { return &____enemyObjects2_23; }
	inline void set__enemyObjects2_23(GameObjectU5BU5D_t3057952154* value)
	{
		____enemyObjects2_23 = value;
		Il2CppCodeGenWriteBarrier(&____enemyObjects2_23, value);
	}

	inline static int32_t get_offset_of__enemyObjects3_24() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ____enemyObjects3_24)); }
	inline GameObjectU5BU5D_t3057952154* get__enemyObjects3_24() const { return ____enemyObjects3_24; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__enemyObjects3_24() { return &____enemyObjects3_24; }
	inline void set__enemyObjects3_24(GameObjectU5BU5D_t3057952154* value)
	{
		____enemyObjects3_24 = value;
		Il2CppCodeGenWriteBarrier(&____enemyObjects3_24, value);
	}

	inline static int32_t get_offset_of_PHASE_INIT_26() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ___PHASE_INIT_26)); }
	inline int32_t get_PHASE_INIT_26() const { return ___PHASE_INIT_26; }
	inline int32_t* get_address_of_PHASE_INIT_26() { return &___PHASE_INIT_26; }
	inline void set_PHASE_INIT_26(int32_t value)
	{
		___PHASE_INIT_26 = value;
	}

	inline static int32_t get_offset_of_PHASE_PLAYING_27() { return static_cast<int32_t>(offsetof(CardController_t2595996862_StaticFields, ___PHASE_PLAYING_27)); }
	inline int32_t get_PHASE_PLAYING_27() const { return ___PHASE_PLAYING_27; }
	inline int32_t* get_address_of_PHASE_PLAYING_27() { return &___PHASE_PLAYING_27; }
	inline void set_PHASE_PLAYING_27(int32_t value)
	{
		___PHASE_PLAYING_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
