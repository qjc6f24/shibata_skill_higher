﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t1932982249;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2
struct  U3CToStringU3Ec__AnonStorey2_t1254512383  : public Il2CppObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::buff
	StringBuilder_t1221177846 * ___buff_0;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::$this
	WebHeaderCollection_t1932982249 * ___U24this_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey2_t1254512383, ___buff_0)); }
	inline StringBuilder_t1221177846 * get_buff_0() const { return ___buff_0; }
	inline StringBuilder_t1221177846 ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(StringBuilder_t1221177846 * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier(&___buff_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey2_t1254512383, ___U24this_1)); }
	inline WebHeaderCollection_t1932982249 * get_U24this_1() const { return ___U24this_1; }
	inline WebHeaderCollection_t1932982249 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebHeaderCollection_t1932982249 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
