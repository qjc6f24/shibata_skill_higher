﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::.ctor()
#define Queue_1__ctor_m3431931081(__this, method) ((  void (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m1726927384(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2709708561 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m2747900732(__this, method) ((  bool (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2623263182(__this, method) ((  Il2CppObject * (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3744018426(__this, method) ((  Il2CppObject* (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m1907947721(__this, method) ((  Il2CppObject * (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Clear()
#define Queue_1_Clear_m3135450124(__this, method) ((  void (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_Clear_m139654726_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m2831488845(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2709708561 *, MessageEventArgsU5BU5D_t2205345147*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Dequeue()
#define Queue_1_Dequeue_m1137069007(__this, method) ((  MessageEventArgs_t2890051726 * (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Peek()
#define Queue_1_Peek_m634826526(__this, method) ((  MessageEventArgs_t2890051726 * (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Enqueue(T)
#define Queue_1_Enqueue_m1741874628(__this, ___item0, method) ((  void (*) (Queue_1_t2709708561 *, MessageEventArgs_t2890051726 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3627378687(__this, ___new_size0, method) ((  void (*) (Queue_1_t2709708561 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::get_Count()
#define Queue_1_get_Count_m4241224761(__this, method) ((  int32_t (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::GetEnumerator()
#define Queue_1_GetEnumerator_m439811951(__this, method) ((  Enumerator_t3219771641  (*) (Queue_1_t2709708561 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
