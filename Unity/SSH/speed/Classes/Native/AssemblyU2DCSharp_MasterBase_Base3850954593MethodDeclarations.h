﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MasterBase/Base
struct Base_t3850954593;

#include "codegen/il2cpp-codegen.h"

// System.Void MasterBase/Base::.ctor()
extern "C"  void Base__ctor_m399445744 (Base_t3850954593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
