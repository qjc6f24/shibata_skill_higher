﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6
struct U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6::.ctor()
extern "C"  void U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6__ctor_m2462649083 (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6::<>m__0(System.String)
extern "C"  bool U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_U3CU3Em__0_m1780215862 (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * __this, String_t* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
