﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3968042187MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3852207814(__this, ___host0, method) ((  void (*) (Enumerator_t1402664291 *, Dictionary_2_t4011098823 *, const MethodInfo*))Enumerator__ctor_m3819430617_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1992789977(__this, method) ((  Il2CppObject * (*) (Enumerator_t1402664291 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m795438929(__this, method) ((  void (*) (Enumerator_t1402664291 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::Dispose()
#define Enumerator_Dispose_m1695366630(__this, method) ((  void (*) (Enumerator_t1402664291 *, const MethodInfo*))Enumerator_Dispose_m4238653081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::MoveNext()
#define Enumerator_MoveNext_m1984837944(__this, method) ((  bool (*) (Enumerator_t1402664291 *, const MethodInfo*))Enumerator_MoveNext_m335649778_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::get_Current()
#define Enumerator_get_Current_m3202707919(__this, method) ((  HttpHeaderInfo_t2096319561 * (*) (Enumerator_t1402664291 *, const MethodInfo*))Enumerator_get_Current_m4025002300_gshared)(__this, method)
