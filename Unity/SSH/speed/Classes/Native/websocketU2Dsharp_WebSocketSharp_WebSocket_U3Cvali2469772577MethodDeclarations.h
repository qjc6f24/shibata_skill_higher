﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5
struct U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5::.ctor()
extern "C"  void U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5__ctor_m2540079390 (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5::<>m__0(System.String)
extern "C"  bool U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_U3CU3Em__0_m3120152001 (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * __this, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
