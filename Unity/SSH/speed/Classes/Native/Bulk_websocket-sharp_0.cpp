﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t344507773;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;
// WebSocketSharp.ErrorEventArgs
struct ErrorEventArgs_t502222999;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Uri
struct Uri_t19570940;
// System.Action`1<System.Byte[]>
struct Action_1_t3199133395;
// System.Action`1<System.Exception>
struct Action_1_t1729240069;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.EventHandler
struct EventHandler_t277755526;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t4248997468;
// WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1
struct U3CContainsTwiceU3Ec__AnonStorey1_t2127424492;
// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3
struct U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943;
// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5
struct U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565;
// WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0
struct U3CSplitHeaderValueU3Ec__Iterator0_t2473694690;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// WebSocketSharp.HttpBase
struct HttpBase_t4283398485;
// System.Version
struct Version_t1755874712;
// WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0
struct U3CreadHeadersU3Ec__AnonStorey0_t2579283176;
// WebSocketSharp.HttpRequest
struct HttpRequest_t1845443631;
// WebSocketSharp.HttpResponse
struct HttpResponse_t2820540315;
// System.Func`2<System.String[],WebSocketSharp.HttpResponse>
struct Func_2_t1532221928;
// System.Func`2<System.String[],System.Object>
struct Func_2_t1401130908;
// WebSocketSharp.LogData
struct LogData_t4095822710;
// System.Diagnostics.StackFrame
struct StackFrame_t2050294881;
// WebSocketSharp.Logger
struct Logger_t2598199114;
// System.Action`2<WebSocketSharp.LogData,System.String>
struct Action_2_t502883108;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t2890051726;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t764750278;
// WebSocketSharp.Net.AuthenticationBase
struct AuthenticationBase_t909684845;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1146723439;
// WebSocketSharp.Net.AuthenticationResponse
struct AuthenticationResponse_t1212723231;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t3911206805;
// WebSocketSharp.Net.ClientSslConfiguration
struct ClientSslConfiguration_t1159130081;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t3696771181;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;
// WebSocketSharp.Net.Cookie
struct Cookie_t1826188460;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie>
struct IEnumerable_1_t2118315505;
// System.Array
struct Il2CppArray;
// WebSocketSharp.Net.CookieException
struct CookieException_t780982235;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// WebSocketSharp.Net.HttpHeaderInfo
struct HttpHeaderInfo_t2096319561;
// System.Collections.IList
struct IList_t3321498491;
// System.Text.Encoding
struct Encoding_t663144255;
// WebSocketSharp.Net.SslConfiguration
struct SslConfiguration_t760772650;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t1932982249;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t633582367;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1
struct U3CGetObjectDataU3Ec__AnonStorey1_t2338822889;
// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2
struct U3CToStringU3Ec__AnonStorey2_t1254512383;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;
// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0
struct U3CGetEnumeratorU3Ec__Iterator0_t3664690781;
// WebSocketSharp.WebSocket
struct WebSocket_t3268376029;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t3230782241;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t3388497467;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t1481358898;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1280756467;
// WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4
struct U3CstartReceivingU3Ec__AnonStorey4_t506990866;
// WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5
struct U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577;
// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6
struct U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450;
// WebSocketSharp.WebSocketException
struct WebSocketException_t1348391352;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t566549660;
// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct U3CdumpU3Ec__AnonStorey1_t3897361314;
// System.Action`4<System.String,System.String,System.String,System.String>
struct Action_4_t3853297115;
// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2
struct U3CdumpU3Ec__AnonStorey2_t1475667190;
// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0
struct U3CGetEnumeratorU3Ec__Iterator0_t908706979;
// WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3
struct U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925;
// WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7
struct U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881;
// WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4
struct U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074;
// WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5
struct U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521;
// WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6
struct U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "websocketU2Dsharp_U3CModuleU3E3783534214.h"
#include "websocketU2Dsharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU3894236545MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_ByteOrder469806806.h"
#include "websocketU2Dsharp_WebSocketSharp_ByteOrder469806806MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseEventArgs344507773.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseEventArgs344507773MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData3839327312.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseStatusCode2945181741.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseStatusCode2945181741MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_CompressionMethod4066553457.h"
#include "websocketU2Dsharp_WebSocketSharp_CompressionMethod4066553457MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_ErrorEventArgs502222999.h"
#include "websocketU2Dsharp_WebSocketSharp_ErrorEventArgs502222999MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext870230697.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext870230697MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_IO_MemoryStream743994179MethodDeclarations.h"
#include "System_System_IO_Compression_DeflateStream3198596725MethodDeclarations.h"
#include "System_System_IO_Compression_DeflateStream3198596725.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "System_System_IO_Compression_CompressionMode1471062003.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "System_Core_System_Func_2_gen1989381442MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1989381442.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CContainsTw2127424492MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3377395111MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CContainsTw2127424492.h"
#include "System_Core_System_Func_2_gen3377395111.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Action_1_gen1873676830.h"
#include "mscorlib_System_Action_1_gen1873676830MethodDeclarations.h"
#include "System_System_Uri19570940.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "mscorlib_System_BitConverter3195628829MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "websocketU2Dsharp_WebSocketSharp_Opcode2313788840.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Action_1_gen3199133395.h"
#include "mscorlib_System_Action_1_gen1729240069.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA1697116946MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1729240069MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA1697116946.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA1697116943MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen710877419MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA1697116943.h"
#include "mscorlib_System_Action_1_gen710877419.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CSplitHeade2473694690MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CSplitHeade2473694690.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_EventHandler277755526MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieCollect4248997468.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieCollect4248997468MethodDeclarations.h"
#include "mscorlib_System_BitConverter3195628829.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "System_System_UriKind1128731744.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpUtility3363705102MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3199133395MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA4262448565MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA4262448565.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Fin2752139063.h"
#include "websocketU2Dsharp_WebSocketSharp_Fin2752139063MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpBase4283398485.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpBase4283398485MethodDeclarations.h"
#include "mscorlib_System_Version1755874712.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpBase_U3CreadH2579283176MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketExceptio1348391352MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpBase_U3CreadH2579283176.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketExceptio1348391352.h"
#include "mscorlib_System_StringSplitOptions2996162939.h"
#include "mscorlib_System_IO_EndOfStreamException1711658693MethodDeclarations.h"
#include "mscorlib_System_IO_EndOfStreamException1711658693.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpRequest1845443631.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpRequest1845443631MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpVersion4270509666.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpVersion4270509666MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpResponse2820540315.h"
#include "System_Core_System_Func_2_gen1532221928MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1532221928.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpResponse2820540315MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Cookie1826188460MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Cookie1826188460.h"
#include "mscorlib_System_Version1755874712MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl1932982249MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl1932982249.h"
#include "websocketU2Dsharp_WebSocketSharp_LogData4095822710.h"
#include "websocketU2Dsharp_WebSocketSharp_LogData4095822710MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_LogLevel2748531832.h"
#include "mscorlib_System_Diagnostics_StackFrame2050294881.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Diagnostics_StackFrame2050294881MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "websocketU2Dsharp_WebSocketSharp_Logger2598199114.h"
#include "websocketU2Dsharp_WebSocketSharp_Logger2598199114MethodDeclarations.h"
#include "System_Core_System_Action_2_gen502883108.h"
#include "System_Core_System_Action_2_gen502883108MethodDeclarations.h"
#include "mscorlib_System_Console2311202731MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_IO_StreamWriter3858580635MethodDeclarations.h"
#include "mscorlib_System_IO_TextWriter4027217640MethodDeclarations.h"
#include "mscorlib_System_IO_StreamWriter3858580635.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "websocketU2Dsharp_WebSocketSharp_LogLevel2748531832MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Mask1111889066.h"
#include "websocketU2Dsharp_WebSocketSharp_Mask1111889066MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData3839327312MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authentication909684845.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authentication909684845MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_AuthenticationS29593226.h"
#include "mscorlib_System_Random1044426839MethodDeclarations.h"
#include "mscorlib_System_Random1044426839.h"
#include "mscorlib_System_Byte3683104436MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1146723439.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1146723439MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1212723231.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1212723231MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_NetworkCreden3911206805.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_NetworkCreden3911206805MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_MD51507972490MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_MD51507972490.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_AuthenticationS29593226MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_ClientSslConf1159130081.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_ClientSslConf1159130081MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "System_System_Security_Authentication_SslProtocols894678499.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_SslConfigurati760772650MethodDeclarations.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieExceptio780982235MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieExceptio780982235.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1195309592MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1195309592.h"
#include "mscorlib_System_Comparison_1_gen3087927311MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen3087927311.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Globalization_DateTimeStyles370343085.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_InvalidCastException3625212209MethodDeclarations.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat730039266.h"
#include "mscorlib_System_FormatException2948921286MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpHeaderInf2096319561.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpHeaderInf2096319561MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpHeaderTyp1518115223.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpHeaderTyp1518115223MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpUtility3363705102.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_SslConfigurati760772650.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4011098823MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4011098823.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization753258759.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_Core_System_Action_2_gen4234541925MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4234541925.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2714158666MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1402664291.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2714158666.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1402664291MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl2338822889MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl2338822889.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl1254512383MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl1254512383.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebSockets_We3488732344.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebSockets_We3488732344MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_Opcode2313788840MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket3268376029.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket3268376029MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData_U3CGe3664690781MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData_U3CGe3664690781.h"
#include "websocketU2Dsharp_WebSocketSharp_Rsv1058189029.h"
#include "websocketU2Dsharp_WebSocketSharp_Rsv1058189029MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RNGCryptoSer2688843926MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RNGCryptoSer2688843926.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber2510243513.h"
#include "mscorlib_System_Action_1_gen2691851108MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2691851108.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "System_System_Collections_Generic_Queue_1_gen2709708561MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2709708561.h"
#include "mscorlib_System_EventHandler_1_gen3230782241.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "mscorlib_System_EventHandler_1_gen3388497467.h"
#include "mscorlib_System_EventHandler_1_gen1481358898.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketState2935910988.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Threading_WaitHandle677569169MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "System_System_Net_Sockets_TcpClient408947970MethodDeclarations.h"
#include "System_System_Net_Sockets_TcpClient408947970.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "System_Core_System_Action3226471752.h"
#include "System_System_Net_Sockets_NetworkStream581172200.h"
#include "System_System_Net_Security_SslStream1853163792MethodDeclarations.h"
#include "System_System_Net_Security_SslStream1853163792.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cstart506990866MethodDeclarations.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cstart506990866.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cvali2469772577MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cvali2469772577.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cvali1655596450MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cvali1655596450.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber2510243513MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3913997830MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA13336793149.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3913997830.h"
#include "mscorlib_System_Action_1_gen566549660MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen566549660.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U33897361314MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1512722501MethodDeclarations.h"
#include "System_Core_System_Action_4_gen3853297115MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U33897361314.h"
#include "System_Core_System_Func_1_gen1512722501.h"
#include "System_Core_System_Action_4_gen3853297115.h"
#include "mscorlib_System_UInt642909196914MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31521887925MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31521887925.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31882858074MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31882858074.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U33451788521MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U33451788521.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3C983838767MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3C983838767.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3CRe8184881MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3CRe8184881.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3C908706979MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3C908706979.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31475667190MethodDeclarations.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31475667190.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketState2935910988MethodDeclarations.h"

// System.Boolean WebSocketSharp.Ext::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  bool Ext_Contains_TisIl2CppObject_m4213886283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3961629604 * ___condition1, const MethodInfo* method);
#define Ext_Contains_TisIl2CppObject_m4213886283(__this /* static, unused */, ___source0, ___condition1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Ext_Contains_TisIl2CppObject_m4213886283_gshared)(__this /* static, unused */, ___source0, ___condition1, method)
// System.Boolean WebSocketSharp.Ext::Contains<System.String>(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
#define Ext_Contains_TisString_t_m856096519(__this /* static, unused */, ___source0, ___condition1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1989381442 *, const MethodInfo*))Ext_Contains_TisIl2CppObject_m4213886283_gshared)(__this /* static, unused */, ___source0, ___condition1, method)
// T[] WebSocketSharp.Ext::SubArray<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* Ext_SubArray_TisByte_t3683104436_m2352342620_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method);
#define Ext_SubArray_TisByte_t3683104436_m2352342620(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, int32_t, const MethodInfo*))Ext_SubArray_TisByte_t3683104436_m2352342620_gshared)(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method)
// System.String WebSocketSharp.Ext::ToString<System.Object>(T[],System.String)
extern "C"  String_t* Ext_ToString_TisIl2CppObject_m2388582784_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___array0, String_t* ___separator1, const MethodInfo* method);
#define Ext_ToString_TisIl2CppObject_m2388582784(__this /* static, unused */, ___array0, ___separator1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, String_t*, const MethodInfo*))Ext_ToString_TisIl2CppObject_m2388582784_gshared)(__this /* static, unused */, ___array0, ___separator1, method)
// System.String WebSocketSharp.Ext::ToString<System.String>(T[],System.String)
#define Ext_ToString_TisString_t_m1979115242(__this /* static, unused */, ___array0, ___separator1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, StringU5BU5D_t1642385972*, String_t*, const MethodInfo*))Ext_ToString_TisIl2CppObject_m2388582784_gshared)(__this /* static, unused */, ___array0, ___separator1, method)
// T[] WebSocketSharp.Ext::Reverse<System.Byte>(T[])
extern "C"  ByteU5BU5D_t3397334013* Ext_Reverse_TisByte_t3683104436_m1170405293_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, const MethodInfo* method);
#define Ext_Reverse_TisByte_t3683104436_m1170405293(__this /* static, unused */, ___array0, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, const MethodInfo*))Ext_Reverse_TisByte_t3683104436_m1170405293_gshared)(__this /* static, unused */, ___array0, method)
// T WebSocketSharp.HttpBase::Read<System.Object>(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
extern "C"  Il2CppObject * HttpBase_Read_TisIl2CppObject_m2567159859_gshared (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, Func_2_t1401130908 * ___parser1, int32_t ___millisecondsTimeout2, const MethodInfo* method);
#define HttpBase_Read_TisIl2CppObject_m2567159859(__this /* static, unused */, ___stream0, ___parser1, ___millisecondsTimeout2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Stream_t3255436806 *, Func_2_t1401130908 *, int32_t, const MethodInfo*))HttpBase_Read_TisIl2CppObject_m2567159859_gshared)(__this /* static, unused */, ___stream0, ___parser1, ___millisecondsTimeout2, method)
// T WebSocketSharp.HttpBase::Read<WebSocketSharp.HttpResponse>(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
#define HttpBase_Read_TisHttpResponse_t2820540315_m1423434825(__this /* static, unused */, ___stream0, ___parser1, ___millisecondsTimeout2, method) ((  HttpResponse_t2820540315 * (*) (Il2CppObject * /* static, unused */, Stream_t3255436806 *, Func_2_t1532221928 *, int32_t, const MethodInfo*))HttpBase_Read_TisIl2CppObject_m2567159859_gshared)(__this /* static, unused */, ___stream0, ___parser1, ___millisecondsTimeout2, method)
// T[] WebSocketSharp.Ext::SubArray<System.Byte>(T[],System.Int64,System.Int64)
extern "C"  ByteU5BU5D_t3397334013* Ext_SubArray_TisByte_t3683104436_m2761078838_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, int64_t ___startIndex1, int64_t ___length2, const MethodInfo* method);
#define Ext_SubArray_TisByte_t3683104436_m2761078838(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int64_t, int64_t, const MethodInfo*))Ext_SubArray_TisByte_t3683104436_m2761078838_gshared)(__this /* static, unused */, ___array0, ___startIndex1, ___length2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler`1<WebSocketSharp.CloseEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_1_t3230782241_m1092629375(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_1_t3230782241 * (*) (Il2CppObject * /* static, unused */, EventHandler_1_t3230782241 **, EventHandler_1_t3230782241 *, EventHandler_1_t3230782241 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler`1<WebSocketSharp.ErrorEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_1_t3388497467_m2787463461(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_1_t3388497467 * (*) (Il2CppObject * /* static, unused */, EventHandler_1_t3388497467 **, EventHandler_1_t3388497467 *, EventHandler_1_t3388497467 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler`1<WebSocketSharp.MessageEventArgs>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_1_t1481358898_m971601286(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_1_t1481358898 * (*) (Il2CppObject * /* static, unused */, EventHandler_1_t1481358898 **, EventHandler_1_t1481358898 *, EventHandler_1_t1481358898 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_t277755526_m1365132796(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_t277755526 * (*) (Il2CppObject * /* static, unused */, EventHandler_t277755526 **, EventHandler_t277755526 *, EventHandler_t277755526 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void WebSocketSharp.Ext::Emit<System.Object>(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
extern "C"  void Ext_Emit_TisIl2CppObject_m173328602_gshared (Il2CppObject * __this /* static, unused */, EventHandler_1_t1280756467 * ___eventHandler0, Il2CppObject * ___sender1, Il2CppObject * ___e2, const MethodInfo* method);
#define Ext_Emit_TisIl2CppObject_m173328602(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t1280756467 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m173328602_gshared)(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method)
// System.Void WebSocketSharp.Ext::Emit<WebSocketSharp.CloseEventArgs>(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
#define Ext_Emit_TisCloseEventArgs_t344507773_m464613464(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t3230782241 *, Il2CppObject *, CloseEventArgs_t344507773 *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m173328602_gshared)(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method)
// System.Void WebSocketSharp.Ext::Emit<WebSocketSharp.ErrorEventArgs>(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
#define Ext_Emit_TisErrorEventArgs_t502222999_m1113509766(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t3388497467 *, Il2CppObject *, ErrorEventArgs_t502222999 *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m173328602_gshared)(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method)
// System.Void WebSocketSharp.Ext::Emit<WebSocketSharp.MessageEventArgs>(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
#define Ext_Emit_TisMessageEventArgs_t2890051726_m3679319059(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t1481358898 *, Il2CppObject *, MessageEventArgs_t2890051726 *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m173328602_gshared)(__this /* static, unused */, ___eventHandler0, ___sender1, ___e2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebSocketSharp.CloseEventArgs::.ctor(WebSocketSharp.PayloadData)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t CloseEventArgs__ctor_m271874174_MetadataUsageId;
extern "C"  void CloseEventArgs__ctor_m271874174 (CloseEventArgs_t344507773 * __this, PayloadData_t3839327312 * ___payloadData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CloseEventArgs__ctor_m271874174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		PayloadData_t3839327312 * L_0 = ___payloadData0;
		__this->set__payloadData_2(L_0);
		return;
	}
}
// System.Void WebSocketSharp.CloseEventArgs::set_WasClean(System.Boolean)
extern "C"  void CloseEventArgs_set_WasClean_m319124275 (CloseEventArgs_t344507773 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__clean_1(L_0);
		return;
	}
}
// System.Void WebSocketSharp.ErrorEventArgs::.ctor(System.String,System.Exception)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t ErrorEventArgs__ctor_m3573905349_MetadataUsageId;
extern "C"  void ErrorEventArgs__ctor_m3573905349 (ErrorEventArgs_t502222999 * __this, String_t* ___message0, Exception_t1927440687 * ___exception1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorEventArgs__ctor_m3573905349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set__message_2(L_0);
		Exception_t1927440687 * L_1 = ___exception1;
		__this->set__exception_1(L_1);
		return;
	}
}
// System.String WebSocketSharp.ErrorEventArgs::get_Message()
extern "C"  String_t* ErrorEventArgs_get_Message_m4135312202 (ErrorEventArgs_t502222999 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__message_2();
		return L_0;
	}
}
// System.IO.MemoryStream WebSocketSharp.Ext::compress(System.IO.Stream)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflateStream_t3198596725_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_compress_m1172852697_MetadataUsageId;
extern "C"  MemoryStream_t743994179 * Ext_compress_m1172852697 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_compress_m1172852697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	DeflateStream_t3198596725 * V_1 = NULL;
	MemoryStream_t743994179 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Stream_t3255436806 * L_1 = ___stream0;
		NullCheck(L_1);
		int64_t L_2 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_1);
		if ((!(((uint64_t)L_2) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0015;
		}
	}
	{
		MemoryStream_t743994179 * L_3 = V_0;
		return L_3;
	}

IL_0015:
	{
		Stream_t3255436806 * L_4 = ___stream0;
		NullCheck(L_4);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_4, (((int64_t)((int64_t)0))));
		MemoryStream_t743994179 * L_5 = V_0;
		DeflateStream_t3198596725 * L_6 = (DeflateStream_t3198596725 *)il2cpp_codegen_object_new(DeflateStream_t3198596725_il2cpp_TypeInfo_var);
		DeflateStream__ctor_m1924605200(L_6, L_5, 1, (bool)1, /*hidden argument*/NULL);
		V_1 = L_6;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_7 = ___stream0;
		DeflateStream_t3198596725 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_CopyTo_m2580875046(NULL /*static, unused*/, L_7, L_8, ((int32_t)1024), /*hidden argument*/NULL);
		DeflateStream_t3198596725 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_9);
		MemoryStream_t743994179 * L_10 = V_0;
		ByteU5BU5D_t3397334013* L_11 = ((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->get__last_0();
		NullCheck(L_10);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, 0, 1);
		MemoryStream_t743994179 * L_12 = V_0;
		NullCheck(L_12);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_12, (((int64_t)((int64_t)0))));
		MemoryStream_t743994179 * L_13 = V_0;
		V_2 = L_13;
		IL2CPP_LEAVE(0x61, FINALLY_0054);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		{
			DeflateStream_t3198596725 * L_14 = V_1;
			if (!L_14)
			{
				goto IL_0060;
			}
		}

IL_005a:
		{
			DeflateStream_t3198596725 * L_15 = V_1;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_15);
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(84)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0061:
	{
		MemoryStream_t743994179 * L_16 = V_2;
		return L_16;
	}
}
// System.Byte[] WebSocketSharp.Ext::decompress(System.Byte[])
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_decompress_m1818905461_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_decompress_m1818905461 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_decompress_m1818905461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_1) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_000f;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_2 = ___data0;
		return L_2;
	}

IL_000f:
	{
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		MemoryStream_t743994179 * L_4 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		MemoryStream_t743994179 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_6 = Ext_decompressToArray_m1334432499(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		IL2CPP_LEAVE(0x2F, FINALLY_0022);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0022;
	}

FINALLY_0022:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_7 = V_0;
			if (!L_7)
			{
				goto IL_002e;
			}
		}

IL_0028:
		{
			MemoryStream_t743994179 * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_8);
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(34)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(34)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002f:
	{
		ByteU5BU5D_t3397334013* L_9 = V_1;
		return L_9;
	}
}
// System.IO.MemoryStream WebSocketSharp.Ext::decompress(System.IO.Stream)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflateStream_t3198596725_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_decompress_m818945008_MetadataUsageId;
extern "C"  MemoryStream_t743994179 * Ext_decompress_m818945008 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_decompress_m818945008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	DeflateStream_t3198596725 * V_1 = NULL;
	MemoryStream_t743994179 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Stream_t3255436806 * L_1 = ___stream0;
		NullCheck(L_1);
		int64_t L_2 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_1);
		if ((!(((uint64_t)L_2) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0015;
		}
	}
	{
		MemoryStream_t743994179 * L_3 = V_0;
		return L_3;
	}

IL_0015:
	{
		Stream_t3255436806 * L_4 = ___stream0;
		NullCheck(L_4);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_4, (((int64_t)((int64_t)0))));
		Stream_t3255436806 * L_5 = ___stream0;
		DeflateStream_t3198596725 * L_6 = (DeflateStream_t3198596725 *)il2cpp_codegen_object_new(DeflateStream_t3198596725_il2cpp_TypeInfo_var);
		DeflateStream__ctor_m1924605200(L_6, L_5, 0, (bool)1, /*hidden argument*/NULL);
		V_1 = L_6;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		DeflateStream_t3198596725 * L_7 = V_1;
		MemoryStream_t743994179 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_CopyTo_m2580875046(NULL /*static, unused*/, L_7, L_8, ((int32_t)1024), /*hidden argument*/NULL);
		MemoryStream_t743994179 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_9, (((int64_t)((int64_t)0))));
		MemoryStream_t743994179 * L_10 = V_0;
		V_2 = L_10;
		IL2CPP_LEAVE(0x4E, FINALLY_0041);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			DeflateStream_t3198596725 * L_11 = V_1;
			if (!L_11)
			{
				goto IL_004d;
			}
		}

IL_0047:
		{
			DeflateStream_t3198596725 * L_12 = V_1;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_12);
		}

IL_004d:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004e:
	{
		MemoryStream_t743994179 * L_13 = V_2;
		return L_13;
	}
}
// System.Byte[] WebSocketSharp.Ext::decompressToArray(System.IO.Stream)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_decompressToArray_m1334432499_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_decompressToArray_m1334432499 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_decompressToArray_m1334432499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_t3255436806 * L_0 = ___stream0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		MemoryStream_t743994179 * L_1 = Ext_decompress_m818945008(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		MemoryStream_t743994179 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_2);
		MemoryStream_t743994179 * L_3 = V_0;
		NullCheck(L_3);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_3);
		V_1 = L_4;
		IL2CPP_LEAVE(0x26, FINALLY_0019);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0025;
			}
		}

IL_001f:
		{
			MemoryStream_t743994179 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_6);
		}

IL_0025:
		{
			IL2CPP_END_FINALLY(25)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		ByteU5BU5D_t3397334013* L_7 = V_1;
		return L_7;
	}
}
// System.Byte[] WebSocketSharp.Ext::Append(System.UInt16,System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3052225568_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2230618066_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1476232820_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m279205908_MethodInfo_var;
extern const uint32_t Ext_Append_m1795399003_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_Append_m1795399003 (Il2CppObject * __this /* static, unused */, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_Append_m1795399003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	List_1_t3052225568 * V_1 = NULL;
	{
		uint16_t L_0 = ___code0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = Ext_InternalToByteArray_m1846763899(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___reason1;
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		String_t* L_3 = ___reason1;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_5 = V_0;
		List_1_t3052225568 * L_6 = (List_1_t3052225568 *)il2cpp_codegen_object_new(List_1_t3052225568_il2cpp_TypeInfo_var);
		List_1__ctor_m2230618066(L_6, (Il2CppObject*)(Il2CppObject*)L_5, /*hidden argument*/List_1__ctor_m2230618066_MethodInfo_var);
		V_1 = L_6;
		List_1_t3052225568 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_8 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = ___reason1;
		NullCheck(L_8);
		ByteU5BU5D_t3397334013* L_10 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_8, L_9);
		NullCheck(L_7);
		List_1_AddRange_m1476232820(L_7, (Il2CppObject*)(Il2CppObject*)L_10, /*hidden argument*/List_1_AddRange_m1476232820_MethodInfo_var);
		List_1_t3052225568 * L_11 = V_1;
		NullCheck(L_11);
		ByteU5BU5D_t3397334013* L_12 = List_1_ToArray_m279205908(L_11, /*hidden argument*/List_1_ToArray_m279205908_MethodInfo_var);
		V_0 = L_12;
	}

IL_0039:
	{
		ByteU5BU5D_t3397334013* L_13 = V_0;
		return L_13;
	}
}
// System.String WebSocketSharp.Ext::CheckIfValidProtocols(System.String[])
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1989381442_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_U3CCheckIfValidProtocolsU3Em__0_m3636190936_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3583450723_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m856096519_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral802816478;
extern Il2CppCodeGenString* _stringLiteral4260608659;
extern const uint32_t Ext_CheckIfValidProtocols_m4274658712_MetadataUsageId;
extern "C"  String_t* Ext_CheckIfValidProtocols_m4274658712 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___protocols0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_CheckIfValidProtocols_m4274658712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* G_B2_0 = NULL;
	StringU5BU5D_t1642385972* G_B1_0 = NULL;
	String_t* G_B7_0 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ___protocols0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Func_2_t1989381442 * L_1 = ((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Ext_U3CCheckIfValidProtocolsU3Em__0_m3636190936_MethodInfo_var);
		Func_2_t1989381442 * L_3 = (Func_2_t1989381442 *)il2cpp_codegen_object_new(Func_2_t1989381442_il2cpp_TypeInfo_var);
		Func_2__ctor_m3583450723(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3583450723_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Func_2_t1989381442 * L_4 = ((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		bool L_5 = Ext_Contains_TisString_t_m856096519(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_4, /*hidden argument*/Ext_Contains_TisString_t_m856096519_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0032;
		}
	}
	{
		G_B7_0 = _stringLiteral802816478;
		goto IL_0048;
	}

IL_0032:
	{
		StringU5BU5D_t1642385972* L_6 = ___protocols0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_7 = Ext_ContainsTwice_m1833305318(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		G_B7_0 = _stringLiteral4260608659;
		goto IL_0048;
	}

IL_0047:
	{
		G_B7_0 = ((String_t*)(NULL));
	}

IL_0048:
	{
		return G_B7_0;
	}
}
// System.IO.Stream WebSocketSharp.Ext::Compress(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_Compress_m1131940372_MetadataUsageId;
extern "C"  Stream_t3255436806 * Ext_Compress_m1131940372 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, uint8_t ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_Compress_m1131940372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * G_B3_0 = NULL;
	{
		uint8_t L_0 = ___method1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		Stream_t3255436806 * L_1 = ___stream0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		MemoryStream_t743994179 * L_2 = Ext_compress_m1172852697(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0013;
	}

IL_0012:
	{
		Stream_t3255436806 * L_3 = ___stream0;
		G_B3_0 = ((MemoryStream_t743994179 *)(L_3));
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Boolean WebSocketSharp.Ext::ContainsTwice(System.String[])
extern Il2CppClass* U3CContainsTwiceU3Ec__AnonStorey1_t2127424492_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3377395111_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CContainsTwiceU3Ec__AnonStorey1_U3CU3Em__0_m1497604835_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2057901708_MethodInfo_var;
extern const MethodInfo* Func_2_Invoke_m436419272_MethodInfo_var;
extern const uint32_t Ext_ContainsTwice_m1833305318_MetadataUsageId;
extern "C"  bool Ext_ContainsTwice_m1833305318 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ContainsTwice_m1833305318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * V_0 = NULL;
	{
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_0 = (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 *)il2cpp_codegen_object_new(U3CContainsTwiceU3Ec__AnonStorey1_t2127424492_il2cpp_TypeInfo_var);
		U3CContainsTwiceU3Ec__AnonStorey1__ctor_m3941534061(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_1 = V_0;
		StringU5BU5D_t1642385972* L_2 = ___values0;
		NullCheck(L_1);
		L_1->set_values_1(L_2);
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_3 = V_0;
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_4 = V_0;
		NullCheck(L_4);
		StringU5BU5D_t1642385972* L_5 = L_4->get_values_1();
		NullCheck(L_5);
		NullCheck(L_3);
		L_3->set_len_0((((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_6 = V_0;
		NullCheck(L_6);
		L_6->set_contains_2((Func_2_t3377395111 *)NULL);
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_7 = V_0;
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CContainsTwiceU3Ec__AnonStorey1_U3CU3Em__0_m1497604835_MethodInfo_var);
		Func_2_t3377395111 * L_10 = (Func_2_t3377395111 *)il2cpp_codegen_object_new(Func_2_t3377395111_il2cpp_TypeInfo_var);
		Func_2__ctor_m2057901708(L_10, L_8, L_9, /*hidden argument*/Func_2__ctor_m2057901708_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_contains_2(L_10);
		U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * L_11 = V_0;
		NullCheck(L_11);
		Func_2_t3377395111 * L_12 = L_11->get_contains_2();
		NullCheck(L_12);
		bool L_13 = Func_2_Invoke_m436419272(L_12, 0, /*hidden argument*/Func_2_Invoke_m436419272_MethodInfo_var);
		return L_13;
	}
}
// System.Void WebSocketSharp.Ext::CopyTo(System.IO.Stream,System.IO.Stream,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t Ext_CopyTo_m2580875046_MetadataUsageId;
extern "C"  void Ext_CopyTo_m2580875046 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___source0, Stream_t3255436806 * ___destination1, int32_t ___bufferLength2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_CopyTo_m2580875046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___bufferLength2;
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = 0;
		goto IL_0017;
	}

IL_000e:
	{
		Stream_t3255436806 * L_1 = ___destination1;
		ByteU5BU5D_t3397334013* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_1);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, 0, L_3);
	}

IL_0017:
	{
		Stream_t3255436806 * L_4 = ___source0;
		ByteU5BU5D_t3397334013* L_5 = V_0;
		int32_t L_6 = ___bufferLength2;
		NullCheck(L_4);
		int32_t L_7 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, 0, L_6);
		int32_t L_8 = L_7;
		V_1 = L_8;
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Byte[] WebSocketSharp.Ext::Decompress(System.Byte[],WebSocketSharp.CompressionMethod)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_Decompress_m2853080347_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_Decompress_m2853080347 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, uint8_t ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_Decompress_m2853080347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* G_B3_0 = NULL;
	{
		uint8_t L_0 = ___method1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Ext_decompress_m1818905461(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0013;
	}

IL_0012:
	{
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		G_B3_0 = L_3;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Byte[] WebSocketSharp.Ext::DecompressToArray(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_DecompressToArray_m3180897949_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_DecompressToArray_m3180897949 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, uint8_t ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_DecompressToArray_m3180897949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* G_B3_0 = NULL;
	{
		uint8_t L_0 = ___method1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		Stream_t3255436806 * L_1 = ___stream0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Ext_decompressToArray_m1334432499(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0018;
	}

IL_0012:
	{
		Stream_t3255436806 * L_3 = ___stream0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_4 = Ext_ToByteArray_m3607175438(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Boolean WebSocketSharp.Ext::EqualsWith(System.Int32,System.Char,System.Action`1<System.Int32>)
extern const MethodInfo* Action_1_Invoke_m1712580485_MethodInfo_var;
extern const uint32_t Ext_EqualsWith_m1232563349_MetadataUsageId;
extern "C"  bool Ext_EqualsWith_m1232563349 (Il2CppObject * __this /* static, unused */, int32_t ___value0, Il2CppChar ___c1, Action_1_t1873676830 * ___action2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_EqualsWith_m1232563349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1873676830 * L_0 = ___action2;
		int32_t L_1 = ___value0;
		NullCheck(L_0);
		Action_1_Invoke_m1712580485(L_0, L_1, /*hidden argument*/Action_1_Invoke_m1712580485_MethodInfo_var);
		int32_t L_2 = ___value0;
		Il2CppChar L_3 = ___c1;
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
// System.String WebSocketSharp.Ext::GetAbsolutePath(System.Uri)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const uint32_t Ext_GetAbsolutePath_m430442765_MetadataUsageId;
extern "C"  String_t* Ext_GetAbsolutePath_m430442765 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_GetAbsolutePath_m430442765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B7_0 = NULL;
	{
		Uri_t19570940 * L_0 = ___uri0;
		NullCheck(L_0);
		bool L_1 = Uri_get_IsAbsoluteUri_m4123650233(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Uri_t19570940 * L_2 = ___uri0;
		NullCheck(L_2);
		String_t* L_3 = Uri_get_AbsolutePath_m802771013(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Uri_t19570940 * L_4 = ___uri0;
		NullCheck(L_4);
		String_t* L_5 = Uri_get_OriginalString_m2475338851(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		NullCheck(L_6);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_6, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)47))))
		{
			goto IL_0029;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0029:
	{
		String_t* L_8 = V_0;
		CharU5BU5D_t1328083999* L_9 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)63));
		CharU5BU5D_t1328083999* L_10 = L_9;
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)35));
		NullCheck(L_8);
		int32_t L_11 = String_IndexOfAny_m2016554902(L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		String_t* L_15 = String_Substring_m12482732(L_13, 0, L_14, /*hidden argument*/NULL);
		G_B7_0 = L_15;
		goto IL_0055;
	}

IL_0054:
	{
		String_t* L_16 = V_0;
		G_B7_0 = L_16;
	}

IL_0055:
	{
		return G_B7_0;
	}
}
// System.String WebSocketSharp.Ext::GetMessage(WebSocketSharp.CloseStatusCode)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3865390677;
extern Il2CppCodeGenString* _stringLiteral2184070584;
extern Il2CppCodeGenString* _stringLiteral2698209287;
extern Il2CppCodeGenString* _stringLiteral4012644892;
extern Il2CppCodeGenString* _stringLiteral2309523547;
extern Il2CppCodeGenString* _stringLiteral2710806373;
extern Il2CppCodeGenString* _stringLiteral406489070;
extern Il2CppCodeGenString* _stringLiteral3699246282;
extern Il2CppCodeGenString* _stringLiteral626330158;
extern const uint32_t Ext_GetMessage_m4136468021_MetadataUsageId;
extern "C"  String_t* Ext_GetMessage_m4136468021 (Il2CppObject * __this /* static, unused */, uint16_t ___code0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_GetMessage_m4136468021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B19_0 = NULL;
	{
		uint16_t L_0 = ___code0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)1002)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B19_0 = _stringLiteral3865390677;
		goto IL_00c2;
	}

IL_0015:
	{
		uint16_t L_1 = ___code0;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)1003)))))
		{
			goto IL_002a;
		}
	}
	{
		G_B19_0 = _stringLiteral2184070584;
		goto IL_00c2;
	}

IL_002a:
	{
		uint16_t L_2 = ___code0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)1006)))))
		{
			goto IL_003f;
		}
	}
	{
		G_B19_0 = _stringLiteral2698209287;
		goto IL_00c2;
	}

IL_003f:
	{
		uint16_t L_3 = ___code0;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)1007)))))
		{
			goto IL_0054;
		}
	}
	{
		G_B19_0 = _stringLiteral4012644892;
		goto IL_00c2;
	}

IL_0054:
	{
		uint16_t L_4 = ___code0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)1008)))))
		{
			goto IL_0069;
		}
	}
	{
		G_B19_0 = _stringLiteral2309523547;
		goto IL_00c2;
	}

IL_0069:
	{
		uint16_t L_5 = ___code0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)1009)))))
		{
			goto IL_007e;
		}
	}
	{
		G_B19_0 = _stringLiteral2710806373;
		goto IL_00c2;
	}

IL_007e:
	{
		uint16_t L_6 = ___code0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)1010)))))
		{
			goto IL_0093;
		}
	}
	{
		G_B19_0 = _stringLiteral406489070;
		goto IL_00c2;
	}

IL_0093:
	{
		uint16_t L_7 = ___code0;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)1011)))))
		{
			goto IL_00a8;
		}
	}
	{
		G_B19_0 = _stringLiteral3699246282;
		goto IL_00c2;
	}

IL_00a8:
	{
		uint16_t L_8 = ___code0;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)1015)))))
		{
			goto IL_00bd;
		}
	}
	{
		G_B19_0 = _stringLiteral626330158;
		goto IL_00c2;
	}

IL_00bd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B19_0 = L_9;
	}

IL_00c2:
	{
		return G_B19_0;
	}
}
// System.String WebSocketSharp.Ext::GetValue(System.String,System.Char)
extern "C"  String_t* Ext_GetValue_m936838958 (Il2CppObject * __this /* static, unused */, String_t* ___nameAndValue0, Il2CppChar ___separator1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* G_B4_0 = NULL;
	{
		String_t* L_0 = ___nameAndValue0;
		Il2CppChar L_1 = ___separator1;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m2358239236(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_4 = V_0;
		String_t* L_5 = ___nameAndValue0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)((int32_t)((int32_t)L_6-(int32_t)1)))))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_7 = ___nameAndValue0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m2032624251(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = String_Trim_m2668767713(L_9, /*hidden argument*/NULL);
		G_B4_0 = L_10;
		goto IL_0031;
	}

IL_0030:
	{
		G_B4_0 = ((String_t*)(NULL));
	}

IL_0031:
	{
		return G_B4_0;
	}
}
// System.String WebSocketSharp.Ext::GetValue(System.String,System.Char,System.Boolean)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_GetValue_m1342481707_MetadataUsageId;
extern "C"  String_t* Ext_GetValue_m1342481707 (Il2CppObject * __this /* static, unused */, String_t* ___nameAndValue0, Il2CppChar ___separator1, bool ___unquote2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_GetValue_m1342481707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	String_t* G_B6_0 = NULL;
	{
		String_t* L_0 = ___nameAndValue0;
		Il2CppChar L_1 = ___separator1;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m2358239236(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = V_0;
		String_t* L_5 = ___nameAndValue0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)((int32_t)L_6-(int32_t)1))))))
		{
			goto IL_001f;
		}
	}

IL_001d:
	{
		return (String_t*)NULL;
	}

IL_001f:
	{
		String_t* L_7 = ___nameAndValue0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m2032624251(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = String_Trim_m2668767713(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		bool L_11 = ___unquote2;
		if (!L_11)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_13 = Ext_Unquote_m3419740799(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
		goto IL_0040;
	}

IL_003f:
	{
		String_t* L_14 = V_1;
		G_B6_0 = L_14;
	}

IL_0040:
	{
		return G_B6_0;
	}
}
// System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt16,WebSocketSharp.ByteOrder)
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_InternalToByteArray_m1846763899_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_InternalToByteArray_m1846763899 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, int32_t ___order1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_InternalToByteArray_m1846763899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		uint16_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = BitConverter_GetBytes_m1050477013(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___order1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_3 = Ext_IsHostOrder_m3724135050(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = V_0;
		Array_Reverse_m3883292526(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		ByteU5BU5D_t3397334013* L_5 = V_0;
		return L_5;
	}
}
// System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt64,WebSocketSharp.ByteOrder)
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_InternalToByteArray_m3991209682_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_InternalToByteArray_m3991209682 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, int32_t ___order1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_InternalToByteArray_m3991209682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		uint64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = BitConverter_GetBytes_m2213276262(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___order1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_3 = Ext_IsHostOrder_m3724135050(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = V_0;
		Array_Reverse_m3883292526(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		ByteU5BU5D_t3397334013* L_5 = V_0;
		return L_5;
	}
}
// System.Boolean WebSocketSharp.Ext::IsCompressionExtension(System.String,WebSocketSharp.CompressionMethod)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_IsCompressionExtension_m2185878088_MetadataUsageId;
extern "C"  bool Ext_IsCompressionExtension_m2185878088 (Il2CppObject * __this /* static, unused */, String_t* ___value0, uint8_t ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_IsCompressionExtension_m2185878088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		uint8_t L_1 = ___method1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_2 = Ext_ToExtensionString_m2837890601(NULL /*static, unused*/, L_1, ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = String_StartsWith_m1841920685(L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean WebSocketSharp.Ext::IsControl(System.Byte)
extern "C"  bool Ext_IsControl_m1453602925 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = ___opcode0;
		if ((((int32_t)L_0) <= ((int32_t)7)))
		{
			goto IL_000e;
		}
	}
	{
		uint8_t L_1 = ___opcode0;
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)((int32_t)16)))? 1 : 0);
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsData(System.Byte)
extern "C"  bool Ext_IsData_m1914395218 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = ___opcode0;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000d;
		}
	}
	{
		uint8_t L_1 = ___opcode0;
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
	}

IL_000e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsData(WebSocketSharp.Opcode)
extern "C"  bool Ext_IsData_m2145172138 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = ___opcode0;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000d;
		}
	}
	{
		uint8_t L_1 = ___opcode0;
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
	}

IL_000e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsReserved(System.UInt16)
extern "C"  bool Ext_IsReserved_m2404909809 (Il2CppObject * __this /* static, unused */, uint16_t ___code0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		uint16_t L_0 = ___code0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)1004))))
		{
			goto IL_002b;
		}
	}
	{
		uint16_t L_1 = ___code0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)1005))))
		{
			goto IL_002b;
		}
	}
	{
		uint16_t L_2 = ___code0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)1006))))
		{
			goto IL_002b;
		}
	}
	{
		uint16_t L_3 = ___code0;
		G_B5_0 = ((((int32_t)L_3) == ((int32_t)((int32_t)1015)))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B5_0 = 1;
	}

IL_002c:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsSupported(System.Byte)
extern const Il2CppType* Opcode_t2313788840_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern const uint32_t Ext_IsSupported_m2133247636_MetadataUsageId;
extern "C"  bool Ext_IsSupported_m2133247636 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_IsSupported_m2133247636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Opcode_t2313788840_0_0_0_var), /*hidden argument*/NULL);
		uint8_t L_1 = ___opcode0;
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		bool L_4 = Enum_IsDefined_m92789062(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean WebSocketSharp.Ext::IsText(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2168004012;
extern Il2CppCodeGenString* _stringLiteral1759037025;
extern const uint32_t Ext_IsText_m4225803182_MetadataUsageId;
extern "C"  bool Ext_IsText_m4225803182 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_IsText_m4225803182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		String_t* L_0 = ___value0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		goto IL_007d;
	}

IL_000e:
	{
		String_t* L_2 = ___value0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Il2CppChar L_4 = String_get_Chars_m4230566705(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		Il2CppChar L_5 = V_2;
		if ((((int32_t)L_5) >= ((int32_t)((int32_t)32))))
		{
			goto IL_0039;
		}
	}
	{
		CharU5BU5D_t1328083999* L_6 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppChar L_7 = V_2;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_8 = Ext_Contains_m600248741(NULL /*static, unused*/, _stringLiteral2168004012, L_6, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0039:
	{
		Il2CppChar L_9 = V_2;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)127)))))
		{
			goto IL_0043;
		}
	}
	{
		return (bool)0;
	}

IL_0043:
	{
		Il2CppChar L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ((int32_t)((int32_t)L_11+(int32_t)1));
		V_1 = L_12;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_14 = ___value0;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		Il2CppChar L_16 = String_get_Chars_m4230566705(L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		CharU5BU5D_t1328083999* L_17 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppChar L_18 = V_2;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_19 = Ext_Contains_m600248741(NULL /*static, unused*/, _stringLiteral1759037025, L_17, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0079;
		}
	}
	{
		return (bool)0;
	}

IL_0079:
	{
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.Ext::IsToken(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral749251466;
extern const uint32_t Ext_IsToken_m1069302108_MetadataUsageId;
extern "C"  bool Ext_IsToken_m1069302108 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_IsToken_m1069302108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___value0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0040;
	}

IL_0009:
	{
		String_t* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m4230566705(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppChar L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)32))))
		{
			goto IL_003a;
		}
	}
	{
		Il2CppChar L_5 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)((int32_t)127))))
		{
			goto IL_003a;
		}
	}
	{
		CharU5BU5D_t1328083999* L_6 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppChar L_7 = V_0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_8 = Ext_Contains_m600248741(NULL /*static, unused*/, _stringLiteral749251466, L_6, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003c;
		}
	}

IL_003a:
	{
		return (bool)0;
	}

IL_003c:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_2;
		String_t* L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m1606060069(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}
}
// System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_SubArray_TisByte_t3683104436_m2352342620_MethodInfo_var;
extern const uint32_t Ext_ReadBytes_m2197848324_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_ReadBytes_m2197848324 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, int32_t ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ReadBytes_m2197848324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___length1;
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = 0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			V_2 = 0;
			goto IL_002e;
		}

IL_0010:
		{
			Stream_t3255436806 * L_1 = ___stream0;
			ByteU5BU5D_t3397334013* L_2 = V_0;
			int32_t L_3 = V_1;
			int32_t L_4 = ___length1;
			NullCheck(L_1);
			int32_t L_5 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, L_3, L_4);
			V_2 = L_5;
			int32_t L_6 = V_2;
			if (L_6)
			{
				goto IL_0025;
			}
		}

IL_0020:
		{
			goto IL_0035;
		}

IL_0025:
		{
			int32_t L_7 = V_1;
			int32_t L_8 = V_2;
			V_1 = ((int32_t)((int32_t)L_7+(int32_t)L_8));
			int32_t L_9 = ___length1;
			int32_t L_10 = V_2;
			___length1 = ((int32_t)((int32_t)L_9-(int32_t)L_10));
		}

IL_002e:
		{
			int32_t L_11 = ___length1;
			if ((((int32_t)L_11) > ((int32_t)0)))
			{
				goto IL_0010;
			}
		}

IL_0035:
		{
			goto IL_0040;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003a;
		throw e;
	}

CATCH_003a:
	{ // begin catch(System.Object)
		goto IL_0040;
	} // end catch (depth: 1)

IL_0040:
	{
		ByteU5BU5D_t3397334013* L_12 = V_0;
		int32_t L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_14 = Ext_SubArray_TisByte_t3683104436_m2352342620(NULL /*static, unused*/, L_12, 0, L_13, /*hidden argument*/Ext_SubArray_TisByte_t3683104436_m2352342620_MethodInfo_var);
		return L_14;
	}
}
// System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int64,System.Int32)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_ReadBytes_m3342660388_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_ReadBytes_m3342660388 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, int64_t ___length1, int32_t ___bufferLength2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ReadBytes_m3342660388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t V_2 = 0;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				int32_t L_1 = ___bufferLength2;
				V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_1));
				V_2 = 0;
				goto IL_0044;
			}

IL_0014:
			{
				int64_t L_2 = ___length1;
				int32_t L_3 = ___bufferLength2;
				if ((((int64_t)L_2) >= ((int64_t)(((int64_t)((int64_t)L_3))))))
				{
					goto IL_0020;
				}
			}

IL_001c:
			{
				int64_t L_4 = ___length1;
				___bufferLength2 = (((int32_t)((int32_t)L_4)));
			}

IL_0020:
			{
				Stream_t3255436806 * L_5 = ___stream0;
				ByteU5BU5D_t3397334013* L_6 = V_1;
				int32_t L_7 = ___bufferLength2;
				NullCheck(L_5);
				int32_t L_8 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_5, L_6, 0, L_7);
				V_2 = L_8;
				int32_t L_9 = V_2;
				if (L_9)
				{
					goto IL_0035;
				}
			}

IL_0030:
			{
				goto IL_004c;
			}

IL_0035:
			{
				MemoryStream_t743994179 * L_10 = V_0;
				ByteU5BU5D_t3397334013* L_11 = V_1;
				int32_t L_12 = V_2;
				NullCheck(L_10);
				VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, 0, L_12);
				int64_t L_13 = ___length1;
				int32_t L_14 = V_2;
				___length1 = ((int64_t)((int64_t)L_13-(int64_t)(((int64_t)((int64_t)L_14)))));
			}

IL_0044:
			{
				int64_t L_15 = ___length1;
				if ((((int64_t)L_15) > ((int64_t)(((int64_t)((int64_t)0))))))
				{
					goto IL_0014;
				}
			}

IL_004c:
			{
				goto IL_0057;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0051;
			throw e;
		}

CATCH_0051:
		{ // begin catch(System.Object)
			goto IL_0057;
		} // end catch (depth: 2)

IL_0057:
		{
			MemoryStream_t743994179 * L_16 = V_0;
			NullCheck(L_16);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_16);
			MemoryStream_t743994179 * L_17 = V_0;
			NullCheck(L_17);
			ByteU5BU5D_t3397334013* L_18 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_17);
			V_3 = L_18;
			IL2CPP_LEAVE(0x76, FINALLY_0069);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0069;
	}

FINALLY_0069:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_19 = V_0;
			if (!L_19)
			{
				goto IL_0075;
			}
		}

IL_006f:
		{
			MemoryStream_t743994179 * L_20 = V_0;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_20);
		}

IL_0075:
		{
			IL2CPP_END_FINALLY(105)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(105)
	{
		IL2CPP_JUMP_TBL(0x76, IL_0076)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0076:
	{
		ByteU5BU5D_t3397334013* L_21 = V_3;
		return L_21;
	}
}
// System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t163412349_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadBytesAsyncU3Ec__AnonStorey3_U3CU3Em__0_m436706603_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m308472790_MethodInfo_var;
extern const uint32_t Ext_ReadBytesAsync_m2123500857_MetadataUsageId;
extern "C"  void Ext_ReadBytesAsync_m2123500857 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, int32_t ___length1, Action_1_t3199133395 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ReadBytesAsync_m2123500857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * V_0 = NULL;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_0 = (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 *)il2cpp_codegen_object_new(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946_il2cpp_TypeInfo_var);
		U3CReadBytesAsyncU3Ec__AnonStorey3__ctor_m1339069249(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_1 = V_0;
		Stream_t3255436806 * L_2 = ___stream0;
		NullCheck(L_1);
		L_1->set_stream_0(L_2);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_3 = V_0;
		int32_t L_4 = ___length1;
		NullCheck(L_3);
		L_3->set_length_4(L_4);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_5 = V_0;
		Action_1_t3199133395 * L_6 = ___completed2;
		NullCheck(L_5);
		L_5->set_completed_6(L_6);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_7 = V_0;
		Action_1_t1729240069 * L_8 = ___error3;
		NullCheck(L_7);
		L_7->set_error_7(L_8);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_9 = V_0;
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_length_4();
		NullCheck(L_9);
		L_9->set_buff_2(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_12 = V_0;
		NullCheck(L_12);
		L_12->set_offset_3(0);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_13 = V_0;
		NullCheck(L_13);
		L_13->set_retry_1(0);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_14 = V_0;
		NullCheck(L_14);
		L_14->set_callback_5((AsyncCallback_t163412349 *)NULL);
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_15 = V_0;
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)U3CReadBytesAsyncU3Ec__AnonStorey3_U3CU3Em__0_m436706603_MethodInfo_var);
		AsyncCallback_t163412349 * L_18 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m3071689932(L_18, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_callback_5(L_18);
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_19 = V_0;
		NullCheck(L_19);
		Stream_t3255436806 * L_20 = L_19->get_stream_0();
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_21 = V_0;
		NullCheck(L_21);
		ByteU5BU5D_t3397334013* L_22 = L_21->get_buff_2();
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_offset_3();
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_length_4();
		U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_27 = V_0;
		NullCheck(L_27);
		AsyncCallback_t163412349 * L_28 = L_27->get_callback_5();
		NullCheck(L_20);
		VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t3397334013*, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject * >::Invoke(20 /* System.IAsyncResult System.IO.Stream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_20, L_22, L_24, L_26, L_28, NULL);
		goto IL_00a1;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0084;
		throw e;
	}

CATCH_0084:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1927440687 *)__exception_local);
			U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_29 = V_0;
			NullCheck(L_29);
			Action_1_t1729240069 * L_30 = L_29->get_error_7();
			if (!L_30)
			{
				goto IL_009c;
			}
		}

IL_0090:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * L_31 = V_0;
			NullCheck(L_31);
			Action_1_t1729240069 * L_32 = L_31->get_error_7();
			Exception_t1927440687 * L_33 = V_1;
			NullCheck(L_32);
			Action_1_Invoke_m308472790(L_32, L_33, /*hidden argument*/Action_1_Invoke_m308472790_MethodInfo_var);
		}

IL_009c:
		{
			goto IL_00a1;
		}
	} // end catch (depth: 1)

IL_00a1:
	{
		return;
	}
}
// System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int64,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t710877419_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadBytesAsyncU3Ec__AnonStorey4_U3CU3Em__0_m3429307487_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2816864791_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1838049932_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m308472790_MethodInfo_var;
extern const uint32_t Ext_ReadBytesAsync_m2468226979_MetadataUsageId;
extern "C"  void Ext_ReadBytesAsync_m2468226979 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, int64_t ___length1, int32_t ___bufferLength2, Action_1_t3199133395 * ___completed3, Action_1_t1729240069 * ___error4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ReadBytesAsync_m2468226979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * V_0 = NULL;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_0 = (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 *)il2cpp_codegen_object_new(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943_il2cpp_TypeInfo_var);
		U3CReadBytesAsyncU3Ec__AnonStorey4__ctor_m1345062016(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_1 = V_0;
		int32_t L_2 = ___bufferLength2;
		NullCheck(L_1);
		L_1->set_bufferLength_0(L_2);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_3 = V_0;
		Stream_t3255436806 * L_4 = ___stream0;
		NullCheck(L_3);
		L_3->set_stream_1(L_4);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_5 = V_0;
		Action_1_t3199133395 * L_6 = ___completed3;
		NullCheck(L_5);
		L_5->set_completed_6(L_6);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_7 = V_0;
		Action_1_t1729240069 * L_8 = ___error4;
		NullCheck(L_7);
		L_7->set_error_7(L_8);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_9 = V_0;
		MemoryStream_t743994179 * L_10 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_dest_3(L_10);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_11 = V_0;
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_bufferLength_0();
		NullCheck(L_11);
		L_11->set_buff_2(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_13)));
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_14 = V_0;
		NullCheck(L_14);
		L_14->set_retry_4(0);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_15 = V_0;
		NullCheck(L_15);
		L_15->set_read_5((Action_1_t710877419 *)NULL);
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_16 = V_0;
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)U3CReadBytesAsyncU3Ec__AnonStorey4_U3CU3Em__0_m3429307487_MethodInfo_var);
		Action_1_t710877419 * L_19 = (Action_1_t710877419 *)il2cpp_codegen_object_new(Action_1_t710877419_il2cpp_TypeInfo_var);
		Action_1__ctor_m2816864791(L_19, L_17, L_18, /*hidden argument*/Action_1__ctor_m2816864791_MethodInfo_var);
		NullCheck(L_16);
		L_16->set_read_5(L_19);
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_20 = V_0;
		NullCheck(L_20);
		Action_1_t710877419 * L_21 = L_20->get_read_5();
		int64_t L_22 = ___length1;
		NullCheck(L_21);
		Action_1_Invoke_m1838049932(L_21, L_22, /*hidden argument*/Action_1_Invoke_m1838049932_MethodInfo_var);
		goto IL_0098;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0070;
		throw e;
	}

CATCH_0070:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1927440687 *)__exception_local);
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_23 = V_0;
			NullCheck(L_23);
			MemoryStream_t743994179 * L_24 = L_23->get_dest_3();
			NullCheck(L_24);
			Stream_Dispose_m2417657914(L_24, /*hidden argument*/NULL);
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_25 = V_0;
			NullCheck(L_25);
			Action_1_t1729240069 * L_26 = L_25->get_error_7();
			if (!L_26)
			{
				goto IL_0093;
			}
		}

IL_0087:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_27 = V_0;
			NullCheck(L_27);
			Action_1_t1729240069 * L_28 = L_27->get_error_7();
			Exception_t1927440687 * L_29 = V_1;
			NullCheck(L_28);
			Action_1_Invoke_m308472790(L_28, L_29, /*hidden argument*/Action_1_Invoke_m308472790_MethodInfo_var);
		}

IL_0093:
		{
			goto IL_0098;
		}
	} // end catch (depth: 1)

IL_0098:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharp.Ext::SplitHeaderValue(System.String,System.Char[])
extern Il2CppClass* U3CSplitHeaderValueU3Ec__Iterator0_t2473694690_il2cpp_TypeInfo_var;
extern const uint32_t Ext_SplitHeaderValue_m3588285122_MetadataUsageId;
extern "C"  Il2CppObject* Ext_SplitHeaderValue_m3588285122 (Il2CppObject * __this /* static, unused */, String_t* ___value0, CharU5BU5D_t1328083999* ___separators1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_SplitHeaderValue_m3588285122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * V_0 = NULL;
	{
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_0 = (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 *)il2cpp_codegen_object_new(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690_il2cpp_TypeInfo_var);
		U3CSplitHeaderValueU3Ec__Iterator0__ctor_m1143298119(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_1 = V_0;
		String_t* L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_3 = V_0;
		CharU5BU5D_t1328083999* L_4 = ___separators1;
		NullCheck(L_3);
		L_3->set_separators_2(L_4);
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_5 = V_0;
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_6 = L_5;
		NullCheck(L_6);
		L_6->set_U24PC_11(((int32_t)-2));
		return L_6;
	}
}
// System.Byte[] WebSocketSharp.Ext::ToByteArray(System.IO.Stream)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_ToByteArray_m3607175438_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_ToByteArray_m3607175438 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ToByteArray_m3607175438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_1 = ___stream0;
		NullCheck(L_1);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_1, (((int64_t)((int64_t)0))));
		Stream_t3255436806 * L_2 = ___stream0;
		MemoryStream_t743994179 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_CopyTo_m2580875046(NULL /*static, unused*/, L_2, L_3, ((int32_t)1024), /*hidden argument*/NULL);
		MemoryStream_t743994179 * L_4 = V_0;
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_4);
		MemoryStream_t743994179 * L_5 = V_0;
		NullCheck(L_5);
		ByteU5BU5D_t3397334013* L_6 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_5);
		V_1 = L_6;
		IL2CPP_LEAVE(0x39, FINALLY_002c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_7 = V_0;
			if (!L_7)
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			MemoryStream_t743994179 * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_8);
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(44)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0039:
	{
		ByteU5BU5D_t3397334013* L_9 = V_1;
		return L_9;
	}
}
// System.String WebSocketSharp.Ext::ToExtensionString(WebSocketSharp.CompressionMethod,System.String[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CompressionMethod_t4066553457_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_ToString_TisString_t_m1979115242_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3995565367;
extern Il2CppCodeGenString* _stringLiteral2268160698;
extern Il2CppCodeGenString* _stringLiteral811305495;
extern const uint32_t Ext_ToExtensionString_m2837890601_MetadataUsageId;
extern "C"  String_t* Ext_ToExtensionString_m2837890601 (Il2CppObject * __this /* static, unused */, uint8_t ___method0, StringU5BU5D_t1642385972* ___parameters1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ToExtensionString_m2837890601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		uint8_t L_0 = ___method0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_000c:
	{
		Il2CppObject * L_2 = Box(CompressionMethod_t4066553457_il2cpp_TypeInfo_var, (&___method0));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2994460523(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3995565367, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		StringU5BU5D_t1642385972* L_6 = ___parameters1;
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		StringU5BU5D_t1642385972* L_7 = ___parameters1;
		NullCheck(L_7);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))
		{
			goto IL_0039;
		}
	}

IL_0037:
	{
		String_t* L_8 = V_0;
		return L_8;
	}

IL_0039:
	{
		String_t* L_9 = V_0;
		StringU5BU5D_t1642385972* L_10 = ___parameters1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_11 = Ext_ToString_TisString_t_m1979115242(NULL /*static, unused*/, L_10, _stringLiteral811305495, /*hidden argument*/Ext_ToString_TisString_t_m1979115242_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2268160698, L_9, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.UInt16 WebSocketSharp.Ext::ToUInt16(System.Byte[],WebSocketSharp.ByteOrder)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t Ext_ToUInt16_m2022554856_MetadataUsageId;
extern "C"  uint16_t Ext_ToUInt16_m2022554856 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___source0, int32_t ___sourceOrder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ToUInt16_m2022554856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___source0;
		int32_t L_1 = ___sourceOrder1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Ext_ToHostOrder_m3539884168(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		uint16_t L_3 = BitConverter_ToUInt16_m2715119381(NULL /*static, unused*/, L_2, 0, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UInt64 WebSocketSharp.Ext::ToUInt64(System.Byte[],WebSocketSharp.ByteOrder)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t Ext_ToUInt64_m2029374714_MetadataUsageId;
extern "C"  uint64_t Ext_ToUInt64_m2029374714 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___source0, int32_t ___sourceOrder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ToUInt64_m2029374714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___source0;
		int32_t L_1 = ___sourceOrder1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Ext_ToHostOrder_m3539884168(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		uint64_t L_3 = BitConverter_ToUInt64_m2584075445(NULL /*static, unused*/, L_2, 0, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean WebSocketSharp.Ext::TryCreateWebSocketUri(System.String,System.Uri&,System.String&)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3133802767;
extern Il2CppCodeGenString* _stringLiteral702528953;
extern Il2CppCodeGenString* _stringLiteral3110053184;
extern Il2CppCodeGenString* _stringLiteral3115737039;
extern Il2CppCodeGenString* _stringLiteral1186751924;
extern Il2CppCodeGenString* _stringLiteral2878643109;
extern Il2CppCodeGenString* _stringLiteral3385185776;
extern Il2CppCodeGenString* _stringLiteral3766009434;
extern const uint32_t Ext_TryCreateWebSocketUri_m2978564150_MetadataUsageId;
extern "C"  bool Ext_TryCreateWebSocketUri_m2978564150 (Il2CppObject * __this /* static, unused */, String_t* ___uriString0, Uri_t19570940 ** ___result1, String_t** ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_TryCreateWebSocketUri_m2978564150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t19570940 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	Uri_t19570940 ** G_B13_0 = NULL;
	Uri_t19570940 ** G_B12_0 = NULL;
	Uri_t19570940 * G_B17_0 = NULL;
	Uri_t19570940 ** G_B17_1 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t3614634134* G_B15_1 = NULL;
	ObjectU5BU5D_t3614634134* G_B15_2 = NULL;
	String_t* G_B15_3 = NULL;
	Uri_t19570940 ** G_B15_4 = NULL;
	int32_t G_B14_0 = 0;
	ObjectU5BU5D_t3614634134* G_B14_1 = NULL;
	ObjectU5BU5D_t3614634134* G_B14_2 = NULL;
	String_t* G_B14_3 = NULL;
	Uri_t19570940 ** G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	int32_t G_B16_1 = 0;
	ObjectU5BU5D_t3614634134* G_B16_2 = NULL;
	ObjectU5BU5D_t3614634134* G_B16_3 = NULL;
	String_t* G_B16_4 = NULL;
	Uri_t19570940 ** G_B16_5 = NULL;
	{
		Uri_t19570940 ** L_0 = ___result1;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		String_t** L_1 = ___message2;
		*((Il2CppObject **)(L_1)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_1), (Il2CppObject *)NULL);
		String_t* L_2 = ___uriString0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Uri_t19570940 * L_3 = Ext_ToUri_m1195258854(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Uri_t19570940 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_5 = Uri_op_Equality_m110355127(NULL /*static, unused*/, L_4, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		String_t** L_6 = ___message2;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)_stringLiteral3133802767;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)_stringLiteral3133802767);
		return (bool)0;
	}

IL_0022:
	{
		Uri_t19570940 * L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = Uri_get_IsAbsoluteUri_m4123650233(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0036;
		}
	}
	{
		String_t** L_9 = ___message2;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)_stringLiteral702528953;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)_stringLiteral702528953);
		return (bool)0;
	}

IL_0036:
	{
		Uri_t19570940 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = Uri_get_Scheme_m55908894(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral3110053184, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0066;
		}
	}
	{
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral3115737039, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0066;
		}
	}
	{
		String_t** L_16 = ___message2;
		*((Il2CppObject **)(L_16)) = (Il2CppObject *)_stringLiteral1186751924;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_16), (Il2CppObject *)_stringLiteral1186751924);
		return (bool)0;
	}

IL_0066:
	{
		Uri_t19570940 * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = Uri_get_Port_m834512465(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		int32_t L_19 = V_2;
		if (L_19)
		{
			goto IL_007c;
		}
	}
	{
		String_t** L_20 = ___message2;
		*((Il2CppObject **)(L_20)) = (Il2CppObject *)_stringLiteral2878643109;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_20), (Il2CppObject *)_stringLiteral2878643109);
		return (bool)0;
	}

IL_007c:
	{
		Uri_t19570940 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = Uri_get_Fragment_m769951107(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1606060069(L_22, /*hidden argument*/NULL);
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_0096;
		}
	}
	{
		String_t** L_24 = ___message2;
		*((Il2CppObject **)(L_24)) = (Il2CppObject *)_stringLiteral3385185776;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_24), (Il2CppObject *)_stringLiteral3385185776);
		return (bool)0;
	}

IL_0096:
	{
		Uri_t19570940 ** L_25 = ___result1;
		int32_t L_26 = V_2;
		G_B12_0 = L_25;
		if ((((int32_t)L_26) == ((int32_t)(-1))))
		{
			G_B13_0 = L_25;
			goto IL_00a4;
		}
	}
	{
		Uri_t19570940 * L_27 = V_0;
		G_B17_0 = L_27;
		G_B17_1 = G_B12_0;
		goto IL_00f3;
	}

IL_00a4:
	{
		ObjectU5BU5D_t3614634134* L_28 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_29 = V_1;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_29);
		ObjectU5BU5D_t3614634134* L_30 = L_28;
		Uri_t19570940 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = Uri_get_Host_m395387191(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_30;
		String_t* L_34 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_34, _stringLiteral3110053184, /*hidden argument*/NULL);
		G_B14_0 = 2;
		G_B14_1 = L_33;
		G_B14_2 = L_33;
		G_B14_3 = _stringLiteral3766009434;
		G_B14_4 = G_B13_0;
		if (!L_35)
		{
			G_B15_0 = 2;
			G_B15_1 = L_33;
			G_B15_2 = L_33;
			G_B15_3 = _stringLiteral3766009434;
			G_B15_4 = G_B13_0;
			goto IL_00d5;
		}
	}
	{
		G_B16_0 = ((int32_t)80);
		G_B16_1 = G_B14_0;
		G_B16_2 = G_B14_1;
		G_B16_3 = G_B14_2;
		G_B16_4 = G_B14_3;
		G_B16_5 = G_B14_4;
		goto IL_00da;
	}

IL_00d5:
	{
		G_B16_0 = ((int32_t)443);
		G_B16_1 = G_B15_0;
		G_B16_2 = G_B15_1;
		G_B16_3 = G_B15_2;
		G_B16_4 = G_B15_3;
		G_B16_5 = G_B15_4;
	}

IL_00da:
	{
		int32_t L_36 = G_B16_0;
		Il2CppObject * L_37 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_36);
		NullCheck(G_B16_2);
		ArrayElementTypeCheck (G_B16_2, L_37);
		(G_B16_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B16_1), (Il2CppObject *)L_37);
		ObjectU5BU5D_t3614634134* L_38 = G_B16_3;
		Uri_t19570940 * L_39 = V_0;
		NullCheck(L_39);
		String_t* L_40 = Uri_get_PathAndQuery_m2303438487(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_40);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_40);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Format_m1263743648(NULL /*static, unused*/, G_B16_4, L_38, /*hidden argument*/NULL);
		Uri_t19570940 * L_42 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_42, L_41, /*hidden argument*/NULL);
		G_B17_0 = L_42;
		G_B17_1 = G_B16_5;
	}

IL_00f3:
	{
		*((Il2CppObject **)(G_B17_1)) = (Il2CppObject *)G_B17_0;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(G_B17_1), (Il2CppObject *)G_B17_0);
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.Ext::TryGetUTF8EncodedBytes(System.String,System.Byte[]&)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Ext_TryGetUTF8EncodedBytes_m1396876955_MetadataUsageId;
extern "C"  bool Ext_TryGetUTF8EncodedBytes_m1396876955 (Il2CppObject * __this /* static, unused */, String_t* ___s0, ByteU5BU5D_t3397334013** ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_TryGetUTF8EncodedBytes_m1396876955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t3397334013** L_0 = ___bytes1;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
	}

IL_0003:
	try
	{ // begin try (depth: 1)
		ByteU5BU5D_t3397334013** L_1 = ___bytes1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_2 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ___s0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, L_3);
		*((Il2CppObject **)(L_1)) = (Il2CppObject *)L_4;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_1), (Il2CppObject *)L_4);
		goto IL_001d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_001f;
	} // end catch (depth: 1)

IL_001d:
	{
		return (bool)1;
	}

IL_001f:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.String WebSocketSharp.Ext::Unquote(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3943473468;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern const uint32_t Ext_Unquote_m3419740799_MetadataUsageId;
extern "C"  String_t* Ext_Unquote_m3419740799 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_Unquote_m3419740799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* G_B7_0 = NULL;
	{
		String_t* L_0 = ___value0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m2358239236(L_0, ((int32_t)34), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_3 = ___value0;
		return L_3;
	}

IL_0012:
	{
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		int32_t L_5 = String_LastIndexOf_m3555875680(L_4, ((int32_t)34), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)L_7))-(int32_t)1));
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_9 = ___value0;
		G_B7_0 = L_9;
		goto IL_0057;
	}

IL_002e:
	{
		int32_t L_10 = V_2;
		if (L_10)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B7_0 = L_11;
		goto IL_0057;
	}

IL_003e:
	{
		String_t* L_12 = ___value0;
		int32_t L_13 = V_0;
		int32_t L_14 = V_2;
		NullCheck(L_12);
		String_t* L_15 = String_Substring_m12482732(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = String_Replace_m1941156251(L_15, _stringLiteral3943473468, _stringLiteral372029312, /*hidden argument*/NULL);
		G_B7_0 = L_16;
	}

IL_0057:
	{
		return G_B7_0;
	}
}
// System.String WebSocketSharp.Ext::UTF8Decode(System.Byte[])
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Ext_UTF8Decode_m63039028_MetadataUsageId;
extern "C"  String_t* Ext_UTF8Decode_m63039028 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_UTF8Decode_m63039028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_1 = ___bytes0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_0, L_1);
		V_0 = L_2;
		goto IL_0019;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.Object)
		V_0 = (String_t*)NULL;
		goto IL_0019;
	} // end catch (depth: 1)

IL_0019:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Byte[] WebSocketSharp.Ext::UTF8Encode(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t Ext_UTF8Encode_m2432627974_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_UTF8Encode_m2432627974 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_UTF8Encode_m2432627974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___s0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Void WebSocketSharp.Ext::WriteBytes(System.IO.Stream,System.Byte[],System.Int32)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Ext_WriteBytes_m3545246308_MetadataUsageId;
extern "C"  void Ext_WriteBytes_m3545246308 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___bufferLength2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_WriteBytes_m3545246308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes1;
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		MemoryStream_t743994179 * L_2 = V_0;
		Stream_t3255436806 * L_3 = ___stream0;
		int32_t L_4 = ___bufferLength2;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_CopyTo_m2580875046(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x21, FINALLY_0014);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0014;
	}

FINALLY_0014:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0020;
			}
		}

IL_001a:
		{
			MemoryStream_t743994179 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_6);
		}

IL_0020:
		{
			IL2CPP_END_FINALLY(20)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(20)
	{
		IL2CPP_JUMP_TBL(0x21, IL_0021)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0021:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.Ext::Contains(System.String,System.Char[])
extern "C"  bool Ext_Contains_m600248741 (Il2CppObject * __this /* static, unused */, String_t* ___value0, CharU5BU5D_t1328083999* ___chars1, const MethodInfo* method)
{
	int32_t G_B7_0 = 0;
	{
		CharU5BU5D_t1328083999* L_0 = ___chars1;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		CharU5BU5D_t1328083999* L_1 = ___chars1;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B7_0 = 1;
		goto IL_0035;
	}

IL_0014:
	{
		String_t* L_2 = ___value0;
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002b;
		}
	}

IL_0025:
	{
		G_B7_0 = 0;
		goto IL_0035;
	}

IL_002b:
	{
		String_t* L_5 = ___value0;
		CharU5BU5D_t1328083999* L_6 = ___chars1;
		NullCheck(L_5);
		int32_t L_7 = String_IndexOfAny_m2016554902(L_5, L_6, /*hidden argument*/NULL);
		G_B7_0 = ((((int32_t)L_7) > ((int32_t)(-1)))? 1 : 0);
	}

IL_0035:
	{
		return (bool)G_B7_0;
	}
}
// System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String)
extern "C"  bool Ext_Contains_m704373083 (Il2CppObject * __this /* static, unused */, NameValueCollection_t3047564564 * ___collection0, String_t* ___name1, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		NameValueCollection_t3047564564 * L_0 = ___collection0;
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		NameValueCollection_t3047564564 * L_1 = ___collection0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, L_1);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		NameValueCollection_t3047564564 * L_3 = ___collection0;
		String_t* L_4 = ___name1;
		NullCheck(L_3);
		String_t* L_5 = NameValueCollection_get_Item_m2776418562(L_3, L_4, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((Il2CppObject*)(String_t*)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B4_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String,System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const uint32_t Ext_Contains_m183431397_MetadataUsageId;
extern "C"  bool Ext_Contains_m183431397 (Il2CppObject * __this /* static, unused */, NameValueCollection_t3047564564 * ___collection0, String_t* ___name1, String_t* ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_Contains_m183431397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	{
		NameValueCollection_t3047564564 * L_0 = ___collection0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		NameValueCollection_t3047564564 * L_1 = ___collection0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, L_1);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		NameValueCollection_t3047564564 * L_3 = ___collection0;
		String_t* L_4 = ___name1;
		NullCheck(L_3);
		String_t* L_5 = NameValueCollection_get_Item_m2776418562(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		if (L_6)
		{
			goto IL_0023;
		}
	}
	{
		return (bool)0;
	}

IL_0023:
	{
		String_t* L_7 = V_0;
		CharU5BU5D_t1328083999* L_8 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_7);
		StringU5BU5D_t1642385972* L_9 = String_Split_m3326265864(L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		V_3 = 0;
		goto IL_0058;
	}

IL_003c:
	{
		StringU5BU5D_t1642385972* L_10 = V_2;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
		String_t* L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = String_Trim_m2668767713(L_14, /*hidden argument*/NULL);
		String_t* L_16 = ___value2;
		NullCheck(L_15);
		bool L_17 = String_Equals_m1185277978(L_15, L_16, 5, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0054;
		}
	}
	{
		return (bool)1;
	}

IL_0054:
	{
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_19 = V_3;
		StringU5BU5D_t1642385972* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_003c;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void WebSocketSharp.Ext::Emit(System.EventHandler,System.Object,System.EventArgs)
extern "C"  void Ext_Emit_m3411749349 (Il2CppObject * __this /* static, unused */, EventHandler_t277755526 * ___eventHandler0, Il2CppObject * ___sender1, EventArgs_t3289624707 * ___e2, const MethodInfo* method)
{
	{
		EventHandler_t277755526 * L_0 = ___eventHandler0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		EventHandler_t277755526 * L_1 = ___eventHandler0;
		Il2CppObject * L_2 = ___sender1;
		EventArgs_t3289624707 * L_3 = ___e2;
		NullCheck(L_1);
		EventHandler_Invoke_m1137722757(L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_000e:
	{
		return;
	}
}
// WebSocketSharp.Net.CookieCollection WebSocketSharp.Ext::GetCookies(System.Collections.Specialized.NameValueCollection,System.Boolean)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieCollection_t4248997468_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral598520931;
extern Il2CppCodeGenString* _stringLiteral2167658612;
extern const uint32_t Ext_GetCookies_m3383446715_MetadataUsageId;
extern "C"  CookieCollection_t4248997468 * Ext_GetCookies_m3383446715 (Il2CppObject * __this /* static, unused */, NameValueCollection_t3047564564 * ___headers0, bool ___response1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_GetCookies_m3383446715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	CookieCollection_t4248997468 * G_B7_0 = NULL;
	{
		bool L_0 = ___response1;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral598520931;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral2167658612;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		NameValueCollection_t3047564564 * L_1 = ___headers0;
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		NameValueCollection_t3047564564 * L_2 = ___headers0;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_4 = Ext_Contains_m704373083(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		NameValueCollection_t3047564564 * L_5 = ___headers0;
		String_t* L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = NameValueCollection_get_Item_m2776418562(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = ___response1;
		CookieCollection_t4248997468 * L_9 = CookieCollection_Parse_m2687394286(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		G_B7_0 = L_9;
		goto IL_003f;
	}

IL_003a:
	{
		CookieCollection_t4248997468 * L_10 = (CookieCollection_t4248997468 *)il2cpp_codegen_object_new(CookieCollection_t4248997468_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m1652849323(L_10, /*hidden argument*/NULL);
		G_B7_0 = L_10;
	}

IL_003f:
	{
		return G_B7_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsEnclosedIn(System.String,System.Char)
extern "C"  bool Ext_IsEnclosedIn_m1922991632 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___c1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m4230566705(L_3, 0, /*hidden argument*/NULL);
		Il2CppChar L_5 = ___c1;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_6 = ___value0;
		String_t* L_7 = ___value0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1606060069(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_6, ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_10 = ___c1;
		G_B5_0 = ((((int32_t)L_9) == ((int32_t)L_10))? 1 : 0);
		goto IL_0033;
	}

IL_0032:
	{
		G_B5_0 = 0;
	}

IL_0033:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsHostOrder(WebSocketSharp.ByteOrder)
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t Ext_IsHostOrder_m3724135050_MetadataUsageId;
extern "C"  bool Ext_IsHostOrder_m3724135050 (Il2CppObject * __this /* static, unused */, int32_t ___order0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_IsHostOrder_m3724135050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t3195628829_StaticFields*)BitConverter_t3195628829_il2cpp_TypeInfo_var->static_fields)->get_IsLittleEndian_1();
		int32_t L_1 = ___order0;
		return (bool)((((int32_t)((int32_t)((int32_t)L_0^(int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0)))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.Ext::IsNullOrEmpty(System.String)
extern "C"  bool Ext_IsNullOrEmpty_m2240019316 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.Ext::IsPredefinedScheme(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3833952254;
extern Il2CppCodeGenString* _stringLiteral4021508911;
extern Il2CppCodeGenString* _stringLiteral3110053184;
extern Il2CppCodeGenString* _stringLiteral3115737039;
extern Il2CppCodeGenString* _stringLiteral3457518716;
extern Il2CppCodeGenString* _stringLiteral3875251400;
extern Il2CppCodeGenString* _stringLiteral910714343;
extern Il2CppCodeGenString* _stringLiteral1813908152;
extern Il2CppCodeGenString* _stringLiteral1857677307;
extern Il2CppCodeGenString* _stringLiteral2910133225;
extern Il2CppCodeGenString* _stringLiteral4110900156;
extern Il2CppCodeGenString* _stringLiteral157337414;
extern const uint32_t Ext_IsPredefinedScheme_m2280627044_MetadataUsageId;
extern "C"  bool Ext_IsPredefinedScheme_m2280627044 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_IsPredefinedScheme_m2280627044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t G_B7_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B17_0 = 0;
	int32_t G_B28_0 = 0;
	int32_t G_B30_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)2)))
		{
			goto IL_0014;
		}
	}

IL_0012:
	{
		return (bool)0;
	}

IL_0014:
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m4230566705(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		Il2CppChar L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)104)))))
		{
			goto IL_0043;
		}
	}
	{
		String_t* L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral3833952254, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_8 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral4021508911, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_9));
		goto IL_0042;
	}

IL_0041:
	{
		G_B7_0 = 1;
	}

IL_0042:
	{
		return (bool)G_B7_0;
	}

IL_0043:
	{
		Il2CppChar L_10 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)119)))))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_11 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral3110053184, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0068;
		}
	}
	{
		String_t* L_13 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral3115737039, /*hidden argument*/NULL);
		G_B12_0 = ((int32_t)(L_14));
		goto IL_0069;
	}

IL_0068:
	{
		G_B12_0 = 1;
	}

IL_0069:
	{
		return (bool)G_B12_0;
	}

IL_006a:
	{
		Il2CppChar L_15 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0091;
		}
	}
	{
		String_t* L_16 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral3457518716, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_18 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral3875251400, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_19));
		goto IL_0090;
	}

IL_008f:
	{
		G_B17_0 = 1;
	}

IL_0090:
	{
		return (bool)G_B17_0;
	}

IL_0091:
	{
		Il2CppChar L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)103)))))
		{
			goto IL_00a5;
		}
	}
	{
		String_t* L_21 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_21, _stringLiteral910714343, /*hidden argument*/NULL);
		return L_22;
	}

IL_00a5:
	{
		Il2CppChar L_23 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)109)))))
		{
			goto IL_00b9;
		}
	}
	{
		String_t* L_24 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral1813908152, /*hidden argument*/NULL);
		return L_25;
	}

IL_00b9:
	{
		Il2CppChar L_26 = V_0;
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0110;
		}
	}
	{
		String_t* L_27 = ___value0;
		NullCheck(L_27);
		Il2CppChar L_28 = String_get_Chars_m4230566705(L_27, 1, /*hidden argument*/NULL);
		V_0 = L_28;
		Il2CppChar L_29 = V_0;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0104;
		}
	}
	{
		String_t* L_30 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_30, _stringLiteral1857677307, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_00fe;
		}
	}
	{
		String_t* L_32 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_32, _stringLiteral2910133225, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00fe;
		}
	}
	{
		String_t* L_34 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_34, _stringLiteral4110900156, /*hidden argument*/NULL);
		G_B28_0 = ((int32_t)(L_35));
		goto IL_00ff;
	}

IL_00fe:
	{
		G_B28_0 = 1;
	}

IL_00ff:
	{
		G_B30_0 = G_B28_0;
		goto IL_010f;
	}

IL_0104:
	{
		String_t* L_36 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_36, _stringLiteral157337414, /*hidden argument*/NULL);
		G_B30_0 = ((int32_t)(L_37));
	}

IL_010f:
	{
		return (bool)G_B30_0;
	}

IL_0110:
	{
		return (bool)0;
	}
}
// System.Boolean WebSocketSharp.Ext::MaybeUri(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_MaybeUri_m1741664919_MetadataUsageId;
extern "C"  bool Ext_MaybeUri_m1741664919 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_MaybeUri_m1741664919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m2358239236(L_3, ((int32_t)58), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}
	{
		return (bool)0;
	}

IL_0025:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)10))))
		{
			goto IL_002f;
		}
	}
	{
		return (bool)0;
	}

IL_002f:
	{
		String_t* L_7 = ___value0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m12482732(L_7, 0, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_11 = Ext_IsPredefinedScheme_m2280627044(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void WebSocketSharp.Ext::Times(System.Int32,System.Action`1<System.Int32>)
extern const MethodInfo* Action_1_Invoke_m1712580485_MethodInfo_var;
extern const uint32_t Ext_Times_m279352861_MetadataUsageId;
extern "C"  void Ext_Times_m279352861 (Il2CppObject * __this /* static, unused */, int32_t ___n0, Action_1_t1873676830 * ___action1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_Times_m279352861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___n0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		Action_1_t1873676830 * L_1 = ___action1;
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0014:
	{
		Action_1_t1873676830 * L_2 = ___action1;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Action_1_Invoke_m1712580485(L_2, L_3, /*hidden argument*/Action_1_Invoke_m1712580485_MethodInfo_var);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = ___n0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0014;
		}
	}

IL_0026:
	{
		return;
	}
}
// System.Byte[] WebSocketSharp.Ext::ToHostOrder(System.Byte[],WebSocketSharp.ByteOrder)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Reverse_TisByte_t3683104436_m1170405293_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4211174801;
extern const uint32_t Ext_ToHostOrder_m3539884168_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Ext_ToHostOrder_m3539884168 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___source0, int32_t ___sourceOrder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ToHostOrder_m3539884168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* G_B6_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___source0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral4211174801, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ByteU5BU5D_t3397334013* L_2 = ___source0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = ___sourceOrder1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_4 = Ext_IsHostOrder_m3724135050(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0030;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_5 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_6 = Ext_Reverse_TisByte_t3683104436_m1170405293(NULL /*static, unused*/, L_5, /*hidden argument*/Ext_Reverse_TisByte_t3683104436_m1170405293_MethodInfo_var);
		G_B6_0 = L_6;
		goto IL_0031;
	}

IL_0030:
	{
		ByteU5BU5D_t3397334013* L_7 = ___source0;
		G_B6_0 = L_7;
	}

IL_0031:
	{
		return G_B6_0;
	}
}
// System.Uri WebSocketSharp.Ext::ToUri(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern const uint32_t Ext_ToUri_m1195258854_MetadataUsageId;
extern "C"  Uri_t19570940 * Ext_ToUri_m1195258854 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_ToUri_m1195258854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t19570940 * V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	{
		String_t* L_0 = ___value0;
		String_t* L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_2 = Ext_MaybeUri_m1741664919(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0012;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 2;
		G_B3_1 = G_B2_0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri_TryCreate_m16561530(NULL /*static, unused*/, G_B3_1, G_B3_0, (&V_0), /*hidden argument*/NULL);
		Uri_t19570940 * L_3 = V_0;
		return L_3;
	}
}
// System.String WebSocketSharp.Ext::UrlDecode(System.String)
extern Il2CppClass* HttpUtility_t3363705102_il2cpp_TypeInfo_var;
extern const uint32_t Ext_UrlDecode_m482702685_MetadataUsageId;
extern "C"  String_t* Ext_UrlDecode_m482702685 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_UrlDecode_m482702685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B4_0 = NULL;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		String_t* L_4 = HttpUtility_UrlDecode_m2084203637(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_001e;
	}

IL_001d:
	{
		String_t* L_5 = ___value0;
		G_B4_0 = L_5;
	}

IL_001e:
	{
		return G_B4_0;
	}
}
// System.Void WebSocketSharp.Ext::.cctor()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext__cctor_m1406134044_MetadataUsageId;
extern "C"  void Ext__cctor_m1406134044 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext__cctor_m1406134044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->set__last_0(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)1)));
		((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->set__retry_1(5);
		return;
	}
}
// System.Boolean WebSocketSharp.Ext::<CheckIfValidProtocols>m__0(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t Ext_U3CCheckIfValidProtocolsU3Em__0_m3636190936_MetadataUsageId;
extern "C"  bool Ext_U3CCheckIfValidProtocolsU3Em__0_m3636190936 (Il2CppObject * __this /* static, unused */, String_t* ___protocol0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ext_U3CCheckIfValidProtocolsU3Em__0_m3636190936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___protocol0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___protocol0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_3 = ___protocol0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_4 = Ext_IsToken_m1069302108(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = 1;
	}

IL_001d:
	{
		return (bool)G_B4_0;
	}
}
// System.Void WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::.ctor()
extern "C"  void U3CContainsTwiceU3Ec__AnonStorey1__ctor_m3941534061 (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::<>m__0(System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2_Invoke_m436419272_MethodInfo_var;
extern const uint32_t U3CContainsTwiceU3Ec__AnonStorey1_U3CU3Em__0_m1497604835_MetadataUsageId;
extern "C"  bool U3CContainsTwiceU3Ec__AnonStorey1_U3CU3Em__0_m1497604835 (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * __this, int32_t ___idx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContainsTwiceU3Ec__AnonStorey1_U3CU3Em__0_m1497604835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___idx0;
		int32_t L_1 = __this->get_len_0();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)((int32_t)L_1-(int32_t)1)))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_2 = ___idx0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
		goto IL_0037;
	}

IL_0017:
	{
		StringU5BU5D_t1642385972* L_3 = __this->get_values_1();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		StringU5BU5D_t1642385972* L_7 = __this->get_values_1();
		int32_t L_8 = ___idx0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0033;
		}
	}
	{
		return (bool)1;
	}

IL_0033:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = __this->get_len_0();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0017;
		}
	}
	{
		Func_2_t3377395111 * L_15 = __this->get_contains_2();
		int32_t L_16 = ___idx0;
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		___idx0 = L_17;
		NullCheck(L_15);
		bool L_18 = Func_2_Invoke_m436419272(L_15, L_17, /*hidden argument*/Func_2_Invoke_m436419272_MethodInfo_var);
		return L_18;
	}

IL_0055:
	{
		return (bool)0;
	}
}
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey3__ctor_m1339069249 (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::<>m__0(System.IAsyncResult)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_SubArray_TisByte_t3683104436_m2352342620_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1475203383_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m308472790_MethodInfo_var;
extern const uint32_t U3CReadBytesAsyncU3Ec__AnonStorey3_U3CU3Em__0_m436706603_MetadataUsageId;
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey3_U3CU3Em__0_m436706603 (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadBytesAsyncU3Ec__AnonStorey3_U3CU3Em__0_m436706603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Stream_t3255436806 * L_0 = __this->get_stream_0();
			Il2CppObject * L_1 = ___ar0;
			NullCheck(L_0);
			int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(22 /* System.Int32 System.IO.Stream::EndRead(System.IAsyncResult) */, L_0, L_1);
			V_0 = L_2;
			int32_t L_3 = V_0;
			if (L_3)
			{
				goto IL_005b;
			}
		}

IL_0013:
		{
			int32_t L_4 = __this->get_retry_1();
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			int32_t L_5 = ((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->get__retry_1();
			if ((((int32_t)L_4) >= ((int32_t)L_5)))
			{
				goto IL_005b;
			}
		}

IL_0023:
		{
			int32_t L_6 = __this->get_retry_1();
			__this->set_retry_1(((int32_t)((int32_t)L_6+(int32_t)1)));
			Stream_t3255436806 * L_7 = __this->get_stream_0();
			ByteU5BU5D_t3397334013* L_8 = __this->get_buff_2();
			int32_t L_9 = __this->get_offset_3();
			int32_t L_10 = __this->get_length_4();
			AsyncCallback_t163412349 * L_11 = __this->get_callback_5();
			NullCheck(L_7);
			VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t3397334013*, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject * >::Invoke(20 /* System.IAsyncResult System.IO.Stream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_7, L_8, L_9, L_10, L_11, NULL);
			goto IL_0106;
		}

IL_005b:
		{
			int32_t L_12 = V_0;
			if (!L_12)
			{
				goto IL_006d;
			}
		}

IL_0061:
		{
			int32_t L_13 = V_0;
			int32_t L_14 = __this->get_length_4();
			if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
			{
				goto IL_009c;
			}
		}

IL_006d:
		{
			Action_1_t3199133395 * L_15 = __this->get_completed_6();
			if (!L_15)
			{
				goto IL_0097;
			}
		}

IL_0078:
		{
			Action_1_t3199133395 * L_16 = __this->get_completed_6();
			ByteU5BU5D_t3397334013* L_17 = __this->get_buff_2();
			int32_t L_18 = __this->get_offset_3();
			int32_t L_19 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_20 = Ext_SubArray_TisByte_t3683104436_m2352342620(NULL /*static, unused*/, L_17, 0, ((int32_t)((int32_t)L_18+(int32_t)L_19)), /*hidden argument*/Ext_SubArray_TisByte_t3683104436_m2352342620_MethodInfo_var);
			NullCheck(L_16);
			Action_1_Invoke_m1475203383(L_16, L_20, /*hidden argument*/Action_1_Invoke_m1475203383_MethodInfo_var);
		}

IL_0097:
		{
			goto IL_0106;
		}

IL_009c:
		{
			__this->set_retry_1(0);
			int32_t L_21 = __this->get_offset_3();
			int32_t L_22 = V_0;
			__this->set_offset_3(((int32_t)((int32_t)L_21+(int32_t)L_22)));
			int32_t L_23 = __this->get_length_4();
			int32_t L_24 = V_0;
			__this->set_length_4(((int32_t)((int32_t)L_23-(int32_t)L_24)));
			Stream_t3255436806 * L_25 = __this->get_stream_0();
			ByteU5BU5D_t3397334013* L_26 = __this->get_buff_2();
			int32_t L_27 = __this->get_offset_3();
			int32_t L_28 = __this->get_length_4();
			AsyncCallback_t163412349 * L_29 = __this->get_callback_5();
			NullCheck(L_25);
			VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t3397334013*, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject * >::Invoke(20 /* System.IAsyncResult System.IO.Stream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_25, L_26, L_27, L_28, L_29, NULL);
			goto IL_0106;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00e9;
		throw e;
	}

CATCH_00e9:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1927440687 *)__exception_local);
			Action_1_t1729240069 * L_30 = __this->get_error_7();
			if (!L_30)
			{
				goto IL_0101;
			}
		}

IL_00f5:
		{
			Action_1_t1729240069 * L_31 = __this->get_error_7();
			Exception_t1927440687 * L_32 = V_1;
			NullCheck(L_31);
			Action_1_Invoke_m308472790(L_31, L_32, /*hidden argument*/Action_1_Invoke_m308472790_MethodInfo_var);
		}

IL_0101:
		{
			goto IL_0106;
		}
	} // end catch (depth: 1)

IL_0106:
	{
		return;
	}
}
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey4__ctor_m1345062016 (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::<>m__0(System.Int64)
extern Il2CppClass* U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t163412349_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadBytesAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3118594234_MethodInfo_var;
extern const uint32_t U3CReadBytesAsyncU3Ec__AnonStorey4_U3CU3Em__0_m3429307487_MetadataUsageId;
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey4_U3CU3Em__0_m3429307487 (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * __this, int64_t ___len0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadBytesAsyncU3Ec__AnonStorey4_U3CU3Em__0_m3429307487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * V_0 = NULL;
	{
		U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * L_0 = (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 *)il2cpp_codegen_object_new(U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565_il2cpp_TypeInfo_var);
		U3CReadBytesAsyncU3Ec__AnonStorey5__ctor_m3354575350(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU244_1(__this);
		U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * L_2 = V_0;
		int64_t L_3 = ___len0;
		NullCheck(L_2);
		L_2->set_len_0(L_3);
		U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * L_4 = V_0;
		NullCheck(L_4);
		int64_t L_5 = L_4->get_len_0();
		int32_t L_6 = __this->get_bufferLength_0();
		if ((((int64_t)L_5) >= ((int64_t)(((int64_t)((int64_t)L_6))))))
		{
			goto IL_0033;
		}
	}
	{
		U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * L_7 = V_0;
		NullCheck(L_7);
		int64_t L_8 = L_7->get_len_0();
		__this->set_bufferLength_0((((int32_t)((int32_t)L_8))));
	}

IL_0033:
	{
		Stream_t3255436806 * L_9 = __this->get_stream_1();
		ByteU5BU5D_t3397334013* L_10 = __this->get_buff_2();
		int32_t L_11 = __this->get_bufferLength_0();
		U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * L_12 = V_0;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CReadBytesAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3118594234_MethodInfo_var);
		AsyncCallback_t163412349 * L_14 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m3071689932(L_14, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t3397334013*, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject * >::Invoke(20 /* System.IAsyncResult System.IO.Stream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_9, L_10, 0, L_11, L_14, NULL);
		return;
	}
}
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey5__ctor_m3354575350 (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::<>m__0(System.IAsyncResult)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1838049932_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1475203383_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m308472790_MethodInfo_var;
extern const uint32_t U3CReadBytesAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3118594234_MetadataUsageId;
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3118594234 (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadBytesAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3118594234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_0 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_0);
			Stream_t3255436806 * L_1 = L_0->get_stream_1();
			Il2CppObject * L_2 = ___ar0;
			NullCheck(L_1);
			int32_t L_3 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(22 /* System.Int32 System.IO.Stream::EndRead(System.IAsyncResult) */, L_1, L_2);
			V_0 = L_3;
			int32_t L_4 = V_0;
			if ((((int32_t)L_4) <= ((int32_t)0)))
			{
				goto IL_0036;
			}
		}

IL_0019:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_5 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_5);
			MemoryStream_t743994179 * L_6 = L_5->get_dest_3();
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_7 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_7);
			ByteU5BU5D_t3397334013* L_8 = L_7->get_buff_2();
			int32_t L_9 = V_0;
			NullCheck(L_6);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_6, L_8, 0, L_9);
		}

IL_0036:
		{
			int32_t L_10 = V_0;
			if (L_10)
			{
				goto IL_0084;
			}
		}

IL_003c:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_11 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_11);
			int32_t L_12 = L_11->get_retry_4();
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			int32_t L_13 = ((Ext_t870230697_StaticFields*)Ext_t870230697_il2cpp_TypeInfo_var->static_fields)->get__retry_1();
			if ((((int32_t)L_12) >= ((int32_t)L_13)))
			{
				goto IL_0084;
			}
		}

IL_0051:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_14 = __this->get_U3CU3Ef__refU244_1();
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_15 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_15);
			int32_t L_16 = L_15->get_retry_4();
			NullCheck(L_14);
			L_14->set_retry_4(((int32_t)((int32_t)L_16+(int32_t)1)));
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_17 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_17);
			Action_1_t710877419 * L_18 = L_17->get_read_5();
			int64_t L_19 = __this->get_len_0();
			NullCheck(L_18);
			Action_1_Invoke_m1838049932(L_18, L_19, /*hidden argument*/Action_1_Invoke_m1838049932_MethodInfo_var);
			goto IL_014d;
		}

IL_0084:
		{
			int32_t L_20 = V_0;
			if (!L_20)
			{
				goto IL_0097;
			}
		}

IL_008a:
		{
			int32_t L_21 = V_0;
			int64_t L_22 = __this->get_len_0();
			if ((!(((uint64_t)(((int64_t)((int64_t)L_21)))) == ((uint64_t)L_22))))
			{
				goto IL_00ec;
			}
		}

IL_0097:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_23 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_23);
			Action_1_t3199133395 * L_24 = L_23->get_completed_6();
			if (!L_24)
			{
				goto IL_00d7;
			}
		}

IL_00a7:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_25 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_25);
			MemoryStream_t743994179 * L_26 = L_25->get_dest_3();
			NullCheck(L_26);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_26);
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_27 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_27);
			Action_1_t3199133395 * L_28 = L_27->get_completed_6();
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_29 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_29);
			MemoryStream_t743994179 * L_30 = L_29->get_dest_3();
			NullCheck(L_30);
			ByteU5BU5D_t3397334013* L_31 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_30);
			NullCheck(L_28);
			Action_1_Invoke_m1475203383(L_28, L_31, /*hidden argument*/Action_1_Invoke_m1475203383_MethodInfo_var);
		}

IL_00d7:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_32 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_32);
			MemoryStream_t743994179 * L_33 = L_32->get_dest_3();
			NullCheck(L_33);
			Stream_Dispose_m2417657914(L_33, /*hidden argument*/NULL);
			goto IL_014d;
		}

IL_00ec:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_34 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_34);
			L_34->set_retry_4(0);
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_35 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_35);
			Action_1_t710877419 * L_36 = L_35->get_read_5();
			int64_t L_37 = __this->get_len_0();
			int32_t L_38 = V_0;
			NullCheck(L_36);
			Action_1_Invoke_m1838049932(L_36, ((int64_t)((int64_t)L_37-(int64_t)(((int64_t)((int64_t)L_38))))), /*hidden argument*/Action_1_Invoke_m1838049932_MethodInfo_var);
			goto IL_014d;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0116;
		throw e;
	}

CATCH_0116:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1927440687 *)__exception_local);
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_39 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_39);
			MemoryStream_t743994179 * L_40 = L_39->get_dest_3();
			NullCheck(L_40);
			Stream_Dispose_m2417657914(L_40, /*hidden argument*/NULL);
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_41 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_41);
			Action_1_t1729240069 * L_42 = L_41->get_error_7();
			if (!L_42)
			{
				goto IL_0148;
			}
		}

IL_0137:
		{
			U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * L_43 = __this->get_U3CU3Ef__refU244_1();
			NullCheck(L_43);
			Action_1_t1729240069 * L_44 = L_43->get_error_7();
			Exception_t1927440687 * L_45 = V_1;
			NullCheck(L_44);
			Action_1_Invoke_m308472790(L_44, L_45, /*hidden argument*/Action_1_Invoke_m308472790_MethodInfo_var);
		}

IL_0148:
		{
			goto IL_014d;
		}
	} // end catch (depth: 1)

IL_014d:
	{
		return;
	}
}
// System.Void WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::.ctor()
extern "C"  void U3CSplitHeaderValueU3Ec__Iterator0__ctor_m1143298119 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::MoveNext()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t U3CSplitHeaderValueU3Ec__Iterator0_MoveNext_m4267442781_MetadataUsageId;
extern "C"  bool U3CSplitHeaderValueU3Ec__Iterator0_MoveNext_m4267442781 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSplitHeaderValueU3Ec__Iterator0_MoveNext_m4267442781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_11();
		V_0 = L_0;
		__this->set_U24PC_11((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_015a;
		}
		if (L_1 == 2)
		{
			goto IL_01d7;
		}
	}
	{
		goto IL_01de;
	}

IL_0025:
	{
		String_t* L_2 = __this->get_value_0();
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		__this->set_U3ClenU3E__0_1(L_3);
		CharU5BU5D_t1328083999* L_4 = __this->get_separators_2();
		String_t* L_5 = String_CreateString_m3818307083(NULL, L_4, /*hidden argument*/NULL);
		__this->set_U3CsepsU3E__0_3(L_5);
		StringBuilder_t1221177846 * L_6 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_6, ((int32_t)32), /*hidden argument*/NULL);
		__this->set_U3CbuffU3E__0_4(L_6);
		__this->set_U3CescapedU3E__0_5((bool)0);
		__this->set_U3CquotedU3E__0_6((bool)0);
		__this->set_U3CiU3E__1_7(0);
		goto IL_0190;
	}

IL_006e:
	{
		String_t* L_7 = __this->get_value_0();
		int32_t L_8 = __this->get_U3CiU3E__1_7();
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_7, L_8, /*hidden argument*/NULL);
		__this->set_U3CcU3E__2_8(L_9);
		Il2CppChar L_10 = __this->get_U3CcU3E__2_8();
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00c5;
		}
	}
	{
		bool L_11 = __this->get_U3CescapedU3E__0_5();
		if (!L_11)
		{
			goto IL_00b1;
		}
	}
	{
		bool L_12 = __this->get_U3CescapedU3E__0_5();
		__this->set_U3CescapedU3E__0_5((bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0));
		goto IL_00c0;
	}

IL_00b1:
	{
		bool L_13 = __this->get_U3CquotedU3E__0_6();
		__this->set_U3CquotedU3E__0_6((bool)((((int32_t)L_13) == ((int32_t)0))? 1 : 0));
	}

IL_00c0:
	{
		goto IL_0170;
	}

IL_00c5:
	{
		Il2CppChar L_14 = __this->get_U3CcU3E__2_8();
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_010b;
		}
	}
	{
		int32_t L_15 = __this->get_U3CiU3E__1_7();
		int32_t L_16 = __this->get_U3ClenU3E__0_1();
		if ((((int32_t)L_15) >= ((int32_t)((int32_t)((int32_t)L_16-(int32_t)1)))))
		{
			goto IL_0106;
		}
	}
	{
		String_t* L_17 = __this->get_value_0();
		int32_t L_18 = __this->get_U3CiU3E__1_7();
		NullCheck(L_17);
		Il2CppChar L_19 = String_get_Chars_m4230566705(L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0106;
		}
	}
	{
		__this->set_U3CescapedU3E__0_5((bool)1);
	}

IL_0106:
	{
		goto IL_0170;
	}

IL_010b:
	{
		String_t* L_20 = __this->get_U3CsepsU3E__0_3();
		CharU5BU5D_t1328083999* L_21 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppChar L_22 = __this->get_U3CcU3E__2_8();
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_22);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_23 = Ext_Contains_m600248741(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0170;
		}
	}
	{
		bool L_24 = __this->get_U3CquotedU3E__0_6();
		if (L_24)
		{
			goto IL_016b;
		}
	}
	{
		StringBuilder_t1221177846 * L_25 = __this->get_U3CbuffU3E__0_4();
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		__this->set_U24current_9(L_26);
		bool L_27 = __this->get_U24disposing_10();
		if (L_27)
		{
			goto IL_0155;
		}
	}
	{
		__this->set_U24PC_11(1);
	}

IL_0155:
	{
		goto IL_01e0;
	}

IL_015a:
	{
		StringBuilder_t1221177846 * L_28 = __this->get_U3CbuffU3E__0_4();
		NullCheck(L_28);
		StringBuilder_set_Length_m3039225444(L_28, 0, /*hidden argument*/NULL);
		goto IL_0182;
	}

IL_016b:
	{
		goto IL_0170;
	}

IL_0170:
	{
		StringBuilder_t1221177846 * L_29 = __this->get_U3CbuffU3E__0_4();
		Il2CppChar L_30 = __this->get_U3CcU3E__2_8();
		NullCheck(L_29);
		StringBuilder_Append_m3618697540(L_29, L_30, /*hidden argument*/NULL);
	}

IL_0182:
	{
		int32_t L_31 = __this->get_U3CiU3E__1_7();
		__this->set_U3CiU3E__1_7(((int32_t)((int32_t)L_31+(int32_t)1)));
	}

IL_0190:
	{
		int32_t L_32 = __this->get_U3CiU3E__1_7();
		int32_t L_33 = __this->get_U3ClenU3E__0_1();
		if ((((int32_t)L_32) < ((int32_t)L_33)))
		{
			goto IL_006e;
		}
	}
	{
		StringBuilder_t1221177846 * L_34 = __this->get_U3CbuffU3E__0_4();
		NullCheck(L_34);
		int32_t L_35 = StringBuilder_get_Length_m1608241323(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_01d7;
		}
	}
	{
		StringBuilder_t1221177846 * L_36 = __this->get_U3CbuffU3E__0_4();
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_36);
		__this->set_U24current_9(L_37);
		bool L_38 = __this->get_U24disposing_10();
		if (L_38)
		{
			goto IL_01d2;
		}
	}
	{
		__this->set_U24PC_11(2);
	}

IL_01d2:
	{
		goto IL_01e0;
	}

IL_01d7:
	{
		__this->set_U24PC_11((-1));
	}

IL_01de:
	{
		return (bool)0;
	}

IL_01e0:
	{
		return (bool)1;
	}
}
// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m800420733 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Object WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2010760885 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Void WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::Dispose()
extern "C"  void U3CSplitHeaderValueU3Ec__Iterator0_Dispose_m1591902190 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_10((bool)1);
		__this->set_U24PC_11((-1));
		return;
	}
}
// System.Void WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSplitHeaderValueU3Ec__Iterator0_Reset_m1608940_MetadataUsageId;
extern "C"  void U3CSplitHeaderValueU3Ec__Iterator0_Reset_m1608940 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSplitHeaderValueU3Ec__Iterator0_Reset_m1608940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m2109920722 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3498999098(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern Il2CppClass* U3CSplitHeaderValueU3Ec__Iterator0_t2473694690_il2cpp_TypeInfo_var;
extern const uint32_t U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3498999098_MetadataUsageId;
extern "C"  Il2CppObject* U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3498999098 (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSplitHeaderValueU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3498999098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_11();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_2 = (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 *)il2cpp_codegen_object_new(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690_il2cpp_TypeInfo_var);
		U3CSplitHeaderValueU3Ec__Iterator0__ctor_m1143298119(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_3 = V_0;
		String_t* L_4 = __this->get_value_0();
		NullCheck(L_3);
		L_3->set_value_0(L_4);
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_5 = V_0;
		CharU5BU5D_t1328083999* L_6 = __this->get_separators_2();
		NullCheck(L_5);
		L_5->set_separators_2(L_6);
		U3CSplitHeaderValueU3Ec__Iterator0_t2473694690 * L_7 = V_0;
		return L_7;
	}
}
// System.Void WebSocketSharp.HttpBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HttpBase__ctor_m3246364927 (HttpBase_t4283398485 * __this, Version_t1755874712 * ___version0, NameValueCollection_t3047564564 * ___headers1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Version_t1755874712 * L_0 = ___version0;
		__this->set__version_1(L_0);
		NameValueCollection_t3047564564 * L_1 = ___headers1;
		__this->set__headers_0(L_1);
		return;
	}
}
// System.String WebSocketSharp.HttpBase::get_EntityBody()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpUtility_t3363705102_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern const uint32_t HttpBase_get_EntityBody_m1135519468_MetadataUsageId;
extern "C"  String_t* HttpBase_get_EntityBody_m1135519468 (HttpBase_t4283398485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpBase_get_EntityBody_m1135519468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Encoding_t663144255 * V_0 = NULL;
	String_t* V_1 = NULL;
	Encoding_t663144255 * G_B8_0 = NULL;
	Encoding_t663144255 * G_B7_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_EntityBodyData_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = __this->get_EntityBodyData_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		int64_t L_2 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_2) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0023;
		}
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}

IL_0023:
	{
		V_0 = (Encoding_t663144255 *)NULL;
		NameValueCollection_t3047564564 * L_4 = __this->get__headers_0();
		NullCheck(L_4);
		String_t* L_5 = NameValueCollection_get_Item_m2776418562(L_4, _stringLiteral1048821954, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = V_1;
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		String_t* L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1606060069(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_10 = HttpUtility_GetEncoding_m792195308(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_004f:
	{
		Encoding_t663144255 * L_11 = V_0;
		Encoding_t663144255 * L_12 = L_11;
		G_B7_0 = L_12;
		if (L_12)
		{
			G_B8_0 = L_12;
			goto IL_005c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_13 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_13;
	}

IL_005c:
	{
		ByteU5BU5D_t3397334013* L_14 = __this->get_EntityBodyData_2();
		NullCheck(G_B8_0);
		String_t* L_15 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, G_B8_0, L_14);
		return L_15;
	}
}
// System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::get_Headers()
extern "C"  NameValueCollection_t3047564564 * HttpBase_get_Headers_m1195641886 (HttpBase_t4283398485 * __this, const MethodInfo* method)
{
	{
		NameValueCollection_t3047564564 * L_0 = __this->get__headers_0();
		return L_0;
	}
}
// System.Version WebSocketSharp.HttpBase::get_ProtocolVersion()
extern "C"  Version_t1755874712 * HttpBase_get_ProtocolVersion_m412752516 (HttpBase_t4283398485 * __this, const MethodInfo* method)
{
	{
		Version_t1755874712 * L_0 = __this->get__version_1();
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.HttpBase::readEntityBody(System.IO.Stream,System.String)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3721597373;
extern Il2CppCodeGenString* _stringLiteral3438791774;
extern Il2CppCodeGenString* _stringLiteral1126105942;
extern const uint32_t HttpBase_readEntityBody_m2865581765_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* HttpBase_readEntityBody_m2865581765 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, String_t* ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpBase_readEntityBody_m2865581765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	ByteU5BU5D_t3397334013* G_B9_0 = NULL;
	{
		String_t* L_0 = ___length1;
		bool L_1 = Int64_TryParse_m948922810(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_2, _stringLiteral3721597373, _stringLiteral3438791774, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		int64_t L_3 = V_0;
		if ((((int64_t)L_3) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0035;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_4 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_4, _stringLiteral3438791774, _stringLiteral1126105942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0035:
	{
		int64_t L_5 = V_0;
		if ((((int64_t)L_5) <= ((int64_t)(((int64_t)((int64_t)((int32_t)1024)))))))
		{
			goto IL_0052;
		}
	}
	{
		Stream_t3255436806 * L_6 = ___stream0;
		int64_t L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_8 = Ext_ReadBytes_m3342660388(NULL /*static, unused*/, L_6, L_7, ((int32_t)1024), /*hidden argument*/NULL);
		G_B9_0 = L_8;
		goto IL_0068;
	}

IL_0052:
	{
		int64_t L_9 = V_0;
		if ((((int64_t)L_9) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0067;
		}
	}
	{
		Stream_t3255436806 * L_10 = ___stream0;
		int64_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_12 = Ext_ReadBytes_m2197848324(NULL /*static, unused*/, L_10, (((int32_t)((int32_t)L_11))), /*hidden argument*/NULL);
		G_B9_0 = L_12;
		goto IL_0068;
	}

IL_0067:
	{
		G_B9_0 = ((ByteU5BU5D_t3397334013*)(NULL));
	}

IL_0068:
	{
		return G_B9_0;
	}
}
// System.String[] WebSocketSharp.HttpBase::readHeaders(System.IO.Stream,System.Int32)
extern Il2CppClass* U3CreadHeadersU3Ec__AnonStorey0_t2579283176_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3052225568_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1873676830_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3502187290_MethodInfo_var;
extern const MethodInfo* U3CreadHeadersU3Ec__AnonStorey0_U3CU3Em__0_m2437845731_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2068252604_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m279205908_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1018079312;
extern Il2CppCodeGenString* _stringLiteral2168003987;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral2168004012;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern const uint32_t HttpBase_readHeaders_m3569468332_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* HttpBase_readHeaders_m3569468332 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, int32_t ___maxLength1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpBase_readHeaders_m3569468332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * V_0 = NULL;
	Action_1_t1873676830 * V_1 = NULL;
	bool V_2 = false;
	{
		U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * L_0 = (U3CreadHeadersU3Ec__AnonStorey0_t2579283176 *)il2cpp_codegen_object_new(U3CreadHeadersU3Ec__AnonStorey0_t2579283176_il2cpp_TypeInfo_var);
		U3CreadHeadersU3Ec__AnonStorey0__ctor_m4138196225(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * L_1 = V_0;
		List_1_t3052225568 * L_2 = (List_1_t3052225568 *)il2cpp_codegen_object_new(List_1_t3052225568_il2cpp_TypeInfo_var);
		List_1__ctor_m3502187290(L_2, /*hidden argument*/List_1__ctor_m3502187290_MethodInfo_var);
		NullCheck(L_1);
		L_1->set_buff_0(L_2);
		U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_cnt_1(0);
		U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CreadHeadersU3Ec__AnonStorey0_U3CU3Em__0_m2437845731_MethodInfo_var);
		Action_1_t1873676830 * L_6 = (Action_1_t1873676830 *)il2cpp_codegen_object_new(Action_1_t1873676830_il2cpp_TypeInfo_var);
		Action_1__ctor_m2068252604(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m2068252604_MethodInfo_var);
		V_1 = L_6;
		V_2 = (bool)0;
		goto IL_007f;
	}

IL_002c:
	{
		Stream_t3255436806 * L_7 = ___stream0;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_7);
		Action_1_t1873676830 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_10 = Ext_EqualsWith_m1232563349(NULL /*static, unused*/, L_8, ((int32_t)13), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007f;
		}
	}
	{
		Stream_t3255436806 * L_11 = ___stream0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_11);
		Action_1_t1873676830 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_14 = Ext_EqualsWith_m1232563349(NULL /*static, unused*/, L_12, ((int32_t)10), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007f;
		}
	}
	{
		Stream_t3255436806 * L_15 = ___stream0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_15);
		Action_1_t1873676830 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_18 = Ext_EqualsWith_m1232563349(NULL /*static, unused*/, L_16, ((int32_t)13), L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_007f;
		}
	}
	{
		Stream_t3255436806 * L_19 = ___stream0;
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_19);
		Action_1_t1873676830 * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_22 = Ext_EqualsWith_m1232563349(NULL /*static, unused*/, L_20, ((int32_t)10), L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_007f;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_008b;
	}

IL_007f:
	{
		U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_cnt_1();
		int32_t L_25 = ___maxLength1;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_002c;
		}
	}

IL_008b:
	{
		bool L_26 = V_2;
		if (L_26)
		{
			goto IL_009c;
		}
	}
	{
		WebSocketException_t1348391352 * L_27 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_27, _stringLiteral1018079312, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_28 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * L_29 = V_0;
		NullCheck(L_29);
		List_1_t3052225568 * L_30 = L_29->get_buff_0();
		NullCheck(L_30);
		ByteU5BU5D_t3397334013* L_31 = List_1_ToArray_m279205908(L_30, /*hidden argument*/List_1_ToArray_m279205908_MethodInfo_var);
		NullCheck(L_28);
		String_t* L_32 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_28, L_31);
		NullCheck(L_32);
		String_t* L_33 = String_Replace_m1941156251(L_32, _stringLiteral2168003987, _stringLiteral372029310, /*hidden argument*/NULL);
		NullCheck(L_33);
		String_t* L_34 = String_Replace_m1941156251(L_33, _stringLiteral2168004012, _stringLiteral372029310, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_35 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral2162321587);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2162321587);
		NullCheck(L_34);
		StringU5BU5D_t1642385972* L_36 = String_Split_m3927740091(L_34, L_35, 1, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.Byte[] WebSocketSharp.HttpBase::ToByteArray()
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t HttpBase_ToByteArray_m230888017_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* HttpBase_ToByteArray_m230888017 (HttpBase_t4283398485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpBase_ToByteArray_m230888017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Void WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::.ctor()
extern "C"  void U3CreadHeadersU3Ec__AnonStorey0__ctor_m4138196225 (U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::<>m__0(System.Int32)
extern Il2CppClass* EndOfStreamException_t1711658693_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1851245878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral363182886;
extern const uint32_t U3CreadHeadersU3Ec__AnonStorey0_U3CU3Em__0_m2437845731_MetadataUsageId;
extern "C"  void U3CreadHeadersU3Ec__AnonStorey0_U3CU3Em__0_m2437845731 (U3CreadHeadersU3Ec__AnonStorey0_t2579283176 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHeadersU3Ec__AnonStorey0_U3CU3Em__0_m2437845731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___i0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0012;
		}
	}
	{
		EndOfStreamException_t1711658693 * L_1 = (EndOfStreamException_t1711658693 *)il2cpp_codegen_object_new(EndOfStreamException_t1711658693_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m463559158(L_1, _stringLiteral363182886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		List_1_t3052225568 * L_2 = __this->get_buff_0();
		int32_t L_3 = ___i0;
		NullCheck(L_2);
		List_1_Add_m1851245878(L_2, (((int32_t)((uint8_t)L_3))), /*hidden argument*/List_1_Add_m1851245878_MethodInfo_var);
		int32_t L_4 = __this->get_cnt_1();
		__this->set_cnt_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		return;
	}
}
// System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HttpRequest__ctor_m3245535211 (HttpRequest_t1845443631 * __this, String_t* ___method0, String_t* ___uri1, Version_t1755874712 * ___version2, NameValueCollection_t3047564564 * ___headers3, const MethodInfo* method)
{
	{
		Version_t1755874712 * L_0 = ___version2;
		NameValueCollection_t3047564564 * L_1 = ___headers3;
		HttpBase__ctor_m3246364927(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___method0;
		__this->set__method_3(L_2);
		String_t* L_3 = ___uri1;
		__this->set__uri_4(L_3);
		return;
	}
}
// System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String)
extern Il2CppClass* HttpVersion_t4270509666_il2cpp_TypeInfo_var;
extern Il2CppClass* NameValueCollection_t3047564564_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral874979797;
extern Il2CppCodeGenString* _stringLiteral2976477906;
extern const uint32_t HttpRequest__ctor_m1352921265_MetadataUsageId;
extern "C"  void HttpRequest__ctor_m1352921265 (HttpRequest_t1845443631 * __this, String_t* ___method0, String_t* ___uri1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpRequest__ctor_m1352921265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___method0;
		String_t* L_1 = ___uri1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpVersion_t4270509666_il2cpp_TypeInfo_var);
		Version_t1755874712 * L_2 = ((HttpVersion_t4270509666_StaticFields*)HttpVersion_t4270509666_il2cpp_TypeInfo_var->static_fields)->get_Version11_1();
		NameValueCollection_t3047564564 * L_3 = (NameValueCollection_t3047564564 *)il2cpp_codegen_object_new(NameValueCollection_t3047564564_il2cpp_TypeInfo_var);
		NameValueCollection__ctor_m1767369537(L_3, /*hidden argument*/NULL);
		HttpRequest__ctor_m3245535211(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_4 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		NameValueCollection_set_Item_m3775607929(L_4, _stringLiteral874979797, _stringLiteral2976477906, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateConnectRequest(System.Uri)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpRequest_t1845443631_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral240174043;
extern Il2CppCodeGenString* _stringLiteral1542762236;
extern Il2CppCodeGenString* _stringLiteral3430668980;
extern const uint32_t HttpRequest_CreateConnectRequest_m1662457562_MetadataUsageId;
extern "C"  HttpRequest_t1845443631 * HttpRequest_CreateConnectRequest_m1662457562 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpRequest_CreateConnectRequest_m1662457562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	HttpRequest_t1845443631 * V_3 = NULL;
	String_t* G_B2_0 = NULL;
	NameValueCollection_t3047564564 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	NameValueCollection_t3047564564 * G_B1_1 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	NameValueCollection_t3047564564 * G_B3_2 = NULL;
	{
		Uri_t19570940 * L_0 = ___uri0;
		NullCheck(L_0);
		String_t* L_1 = Uri_get_DnsSafeHost_m795496231(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t19570940 * L_2 = ___uri0;
		NullCheck(L_2);
		int32_t L_3 = Uri_get_Port_m834512465(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral240174043, L_4, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = V_2;
		HttpRequest_t1845443631 * L_10 = (HttpRequest_t1845443631 *)il2cpp_codegen_object_new(HttpRequest_t1845443631_il2cpp_TypeInfo_var);
		HttpRequest__ctor_m1352921265(L_10, _stringLiteral1542762236, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		HttpRequest_t1845443631 * L_11 = V_3;
		NullCheck(L_11);
		NameValueCollection_t3047564564 * L_12 = HttpBase_get_Headers_m1195641886(L_11, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		G_B1_0 = _stringLiteral3430668980;
		G_B1_1 = L_12;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)80)))))
		{
			G_B2_0 = _stringLiteral3430668980;
			G_B2_1 = L_12;
			goto IL_0045;
		}
	}
	{
		String_t* L_14 = V_0;
		G_B3_0 = L_14;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0046;
	}

IL_0045:
	{
		String_t* L_15 = V_2;
		G_B3_0 = L_15;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		NameValueCollection_set_Item_m3775607929(G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		HttpRequest_t1845443631 * L_16 = V_3;
		return L_16;
	}
}
// WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateWebSocketRequest(System.Uri)
extern Il2CppClass* HttpRequest_t1845443631_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern Il2CppCodeGenString* _stringLiteral3430668980;
extern Il2CppCodeGenString* _stringLiteral3110053184;
extern Il2CppCodeGenString* _stringLiteral3115737039;
extern Il2CppCodeGenString* _stringLiteral3608358242;
extern Il2CppCodeGenString* _stringLiteral1942363561;
extern Il2CppCodeGenString* _stringLiteral208267350;
extern const uint32_t HttpRequest_CreateWebSocketRequest_m3478151593_MetadataUsageId;
extern "C"  HttpRequest_t1845443631 * HttpRequest_CreateWebSocketRequest_m3478151593 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpRequest_CreateWebSocketRequest_m3478151593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpRequest_t1845443631 * V_0 = NULL;
	NameValueCollection_t3047564564 * V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* G_B2_0 = NULL;
	NameValueCollection_t3047564564 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	NameValueCollection_t3047564564 * G_B1_1 = NULL;
	String_t* G_B4_0 = NULL;
	NameValueCollection_t3047564564 * G_B4_1 = NULL;
	String_t* G_B5_0 = NULL;
	NameValueCollection_t3047564564 * G_B5_1 = NULL;
	String_t* G_B3_0 = NULL;
	NameValueCollection_t3047564564 * G_B3_1 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	NameValueCollection_t3047564564 * G_B6_2 = NULL;
	{
		Uri_t19570940 * L_0 = ___uri0;
		NullCheck(L_0);
		String_t* L_1 = Uri_get_PathAndQuery_m2303438487(L_0, /*hidden argument*/NULL);
		HttpRequest_t1845443631 * L_2 = (HttpRequest_t1845443631 *)il2cpp_codegen_object_new(HttpRequest_t1845443631_il2cpp_TypeInfo_var);
		HttpRequest__ctor_m1352921265(L_2, _stringLiteral1596707798, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		HttpRequest_t1845443631 * L_3 = V_0;
		NullCheck(L_3);
		NameValueCollection_t3047564564 * L_4 = HttpBase_get_Headers_m1195641886(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Uri_t19570940 * L_5 = ___uri0;
		NullCheck(L_5);
		int32_t L_6 = Uri_get_Port_m834512465(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Uri_t19570940 * L_7 = ___uri0;
		NullCheck(L_7);
		String_t* L_8 = Uri_get_Scheme_m55908894(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		NameValueCollection_t3047564564 * L_9 = V_1;
		int32_t L_10 = V_2;
		G_B1_0 = _stringLiteral3430668980;
		G_B1_1 = L_9;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)80)))))
		{
			G_B2_0 = _stringLiteral3430668980;
			G_B2_1 = L_9;
			goto IL_0044;
		}
	}
	{
		String_t* L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral3110053184, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		if (L_12)
		{
			G_B4_0 = G_B1_0;
			G_B4_1 = G_B1_1;
			goto IL_005f;
		}
	}

IL_0044:
	{
		int32_t L_13 = V_2;
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)443)))))
		{
			G_B5_0 = G_B2_0;
			G_B5_1 = G_B2_1;
			goto IL_006a;
		}
	}
	{
		String_t* L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral3115737039, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		if (!L_15)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			goto IL_006a;
		}
	}

IL_005f:
	{
		Uri_t19570940 * L_16 = ___uri0;
		NullCheck(L_16);
		String_t* L_17 = Uri_get_DnsSafeHost_m795496231(L_16, /*hidden argument*/NULL);
		G_B6_0 = L_17;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0070;
	}

IL_006a:
	{
		Uri_t19570940 * L_18 = ___uri0;
		NullCheck(L_18);
		String_t* L_19 = Uri_get_Authority_m936382664(L_18, /*hidden argument*/NULL);
		G_B6_0 = L_19;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0070:
	{
		NullCheck(G_B6_2);
		NameValueCollection_set_Item_m3775607929(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_20 = V_1;
		NullCheck(L_20);
		NameValueCollection_set_Item_m3775607929(L_20, _stringLiteral3608358242, _stringLiteral1942363561, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_21 = V_1;
		NullCheck(L_21);
		NameValueCollection_set_Item_m3775607929(L_21, _stringLiteral208267350, _stringLiteral3608358242, /*hidden argument*/NULL);
		HttpRequest_t1845443631 * L_22 = V_0;
		return L_22;
	}
}
// WebSocketSharp.HttpResponse WebSocketSharp.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern Il2CppClass* HttpRequest_t1845443631_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1532221928_il2cpp_TypeInfo_var;
extern const MethodInfo* HttpResponse_Parse_m1679504445_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m375740568_MethodInfo_var;
extern const MethodInfo* HttpBase_Read_TisHttpResponse_t2820540315_m1423434825_MethodInfo_var;
extern const uint32_t HttpRequest_GetResponse_m12496573_MetadataUsageId;
extern "C"  HttpResponse_t2820540315 * HttpRequest_GetResponse_m12496573 (HttpRequest_t1845443631 * __this, Stream_t3255436806 * ___stream0, int32_t ___millisecondsTimeout1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpRequest_GetResponse_m12496573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	Stream_t3255436806 * G_B2_0 = NULL;
	Stream_t3255436806 * G_B1_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = HttpBase_ToByteArray_m230888017(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Stream_t3255436806 * L_1 = ___stream0;
		ByteU5BU5D_t3397334013* L_2 = V_0;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		NullCheck(L_3);
		NullCheck(L_1);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))));
		Stream_t3255436806 * L_4 = ___stream0;
		Func_2_t1532221928 * L_5 = ((HttpRequest_t1845443631_StaticFields*)HttpRequest_t1845443631_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_5();
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_002b;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)HttpResponse_Parse_m1679504445_MethodInfo_var);
		Func_2_t1532221928 * L_7 = (Func_2_t1532221928 *)il2cpp_codegen_object_new(Func_2_t1532221928_il2cpp_TypeInfo_var);
		Func_2__ctor_m375740568(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m375740568_MethodInfo_var);
		((HttpRequest_t1845443631_StaticFields*)HttpRequest_t1845443631_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_5(L_7);
		G_B2_0 = G_B1_0;
	}

IL_002b:
	{
		Func_2_t1532221928 * L_8 = ((HttpRequest_t1845443631_StaticFields*)HttpRequest_t1845443631_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_5();
		int32_t L_9 = ___millisecondsTimeout1;
		HttpResponse_t2820540315 * L_10 = HttpBase_Read_TisHttpResponse_t2820540315_m1423434825(NULL /*static, unused*/, G_B2_0, L_8, L_9, /*hidden argument*/HttpBase_Read_TisHttpResponse_t2820540315_m1423434825_MethodInfo_var);
		return L_10;
	}
}
// System.Void WebSocketSharp.HttpRequest::SetCookies(WebSocketSharp.Net.CookieCollection)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2118315505_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3596679583_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2501727721;
extern Il2CppCodeGenString* _stringLiteral2167658612;
extern const uint32_t HttpRequest_SetCookies_m2179182154_MetadataUsageId;
extern "C"  void HttpRequest_SetCookies_m2179182154 (HttpRequest_t1845443631 * __this, CookieCollection_t4248997468 * ___cookies0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpRequest_SetCookies_m2179182154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	Cookie_t1826188460 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	int32_t V_3 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CookieCollection_t4248997468 * L_0 = ___cookies0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		CookieCollection_t4248997468 * L_1 = ___cookies0;
		NullCheck(L_1);
		int32_t L_2 = CookieCollection_get_Count_m3590414075(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0012;
		}
	}

IL_0011:
	{
		return;
	}

IL_0012:
	{
		StringBuilder_t1221177846 * L_3 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_3, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_3;
		CookieCollection_t4248997468 * L_4 = ___cookies0;
		NullCheck(L_4);
		Il2CppObject* L_5 = CookieCollection_get_Sorted_m1762804415(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie>::GetEnumerator() */, IEnumerable_1_t2118315505_il2cpp_TypeInfo_var, L_5);
		V_2 = L_6;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_002b:
		{
			Il2CppObject* L_7 = V_2;
			NullCheck(L_7);
			Cookie_t1826188460 * L_8 = InterfaceFuncInvoker0< Cookie_t1826188460 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie>::get_Current() */, IEnumerator_1_t3596679583_il2cpp_TypeInfo_var, L_7);
			V_1 = L_8;
			Cookie_t1826188460 * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = Cookie_get_Expired_m1144720675(L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_004f;
			}
		}

IL_003d:
		{
			StringBuilder_t1221177846 * L_11 = V_0;
			Cookie_t1826188460 * L_12 = V_1;
			NullCheck(L_12);
			String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
			NullCheck(L_11);
			StringBuilder_AppendFormat_m3265503696(L_11, _stringLiteral2501727721, L_13, /*hidden argument*/NULL);
		}

IL_004f:
		{
			Il2CppObject* L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_002b;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x6C, FINALLY_005f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005f;
	}

FINALLY_005f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_16 = V_2;
			if (!L_16)
			{
				goto IL_006b;
			}
		}

IL_0065:
		{
			Il2CppObject* L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_17);
		}

IL_006b:
		{
			IL2CPP_END_FINALLY(95)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(95)
	{
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006c:
	{
		StringBuilder_t1221177846 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = StringBuilder_get_Length_m1608241323(L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		int32_t L_20 = V_3;
		if ((((int32_t)L_20) <= ((int32_t)2)))
		{
			goto IL_0099;
		}
	}
	{
		StringBuilder_t1221177846 * L_21 = V_0;
		int32_t L_22 = V_3;
		NullCheck(L_21);
		StringBuilder_set_Length_m3039225444(L_21, ((int32_t)((int32_t)L_22-(int32_t)2)), /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_23 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		NullCheck(L_23);
		NameValueCollection_set_Item_m3775607929(L_23, _stringLiteral2167658612, L_25, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.String WebSocketSharp.HttpRequest::ToString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1896309355;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral2840330715;
extern const uint32_t HttpRequest_ToString_m3682586646_MetadataUsageId;
extern "C"  String_t* HttpRequest_ToString_m3682586646 (HttpRequest_t1845443631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpRequest_ToString_m3682586646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	NameValueCollection_t3047564564 * V_1 = NULL;
	String_t* V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_3 = __this->get__method_3();
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_2;
		String_t* L_5 = __this->get__uri_4();
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_4;
		Version_t1755874712 * L_7 = HttpBase_get_ProtocolVersion_m412752516(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_6;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral2162321587);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2162321587);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1879616656(L_1, _stringLiteral1896309355, L_8, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_9 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		NameValueCollection_t3047564564 * L_10 = V_1;
		NullCheck(L_10);
		StringU5BU5D_t1642385972* L_11 = VirtFuncInvoker0< StringU5BU5D_t1642385972* >::Invoke(16 /* System.String[] System.Collections.Specialized.NameValueCollection::get_AllKeys() */, L_10);
		V_3 = L_11;
		V_4 = 0;
		goto IL_0077;
	}

IL_0053:
	{
		StringU5BU5D_t1642385972* L_12 = V_3;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_2 = L_15;
		StringBuilder_t1221177846 * L_16 = V_0;
		String_t* L_17 = V_2;
		NameValueCollection_t3047564564 * L_18 = V_1;
		String_t* L_19 = V_2;
		NullCheck(L_18);
		String_t* L_20 = NameValueCollection_get_Item_m2776418562(L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_AppendFormat_m1666670800(L_16, _stringLiteral2840330715, L_17, L_20, _stringLiteral2162321587, /*hidden argument*/NULL);
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_22 = V_4;
		StringU5BU5D_t1642385972* L_23 = V_3;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_Append_m3636508479(L_24, _stringLiteral2162321587, /*hidden argument*/NULL);
		String_t* L_25 = HttpBase_get_EntityBody_m1135519468(__this, /*hidden argument*/NULL);
		V_5 = L_25;
		String_t* L_26 = V_5;
		NullCheck(L_26);
		int32_t L_27 = String_get_Length_m1606060069(L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_27) <= ((int32_t)0)))
		{
			goto IL_00ab;
		}
	}
	{
		StringBuilder_t1221177846 * L_28 = V_0;
		String_t* L_29 = V_5;
		NullCheck(L_28);
		StringBuilder_Append_m3636508479(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		StringBuilder_t1221177846 * L_30 = V_0;
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		return L_31;
	}
}
// System.Void WebSocketSharp.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HttpResponse__ctor_m948363293 (HttpResponse_t2820540315 * __this, String_t* ___code0, String_t* ___reason1, Version_t1755874712 * ___version2, NameValueCollection_t3047564564 * ___headers3, const MethodInfo* method)
{
	{
		Version_t1755874712 * L_0 = ___version2;
		NameValueCollection_t3047564564 * L_1 = ___headers3;
		HttpBase__ctor_m3246364927(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___code0;
		__this->set__code_3(L_2);
		String_t* L_3 = ___reason1;
		__this->set__reason_4(L_3);
		return;
	}
}
// WebSocketSharp.Net.CookieCollection WebSocketSharp.HttpResponse::get_Cookies()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t HttpResponse_get_Cookies_m1833540882_MetadataUsageId;
extern "C"  CookieCollection_t4248997468 * HttpResponse_get_Cookies_m1833540882 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_get_Cookies_m1833540882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameValueCollection_t3047564564 * L_0 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		CookieCollection_t4248997468 * L_1 = Ext_GetCookies_m3383446715(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean WebSocketSharp.HttpResponse::get_HasConnectionClose()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral208267350;
extern Il2CppCodeGenString* _stringLiteral3033369902;
extern const uint32_t HttpResponse_get_HasConnectionClose_m987163280_MetadataUsageId;
extern "C"  bool HttpResponse_get_HasConnectionClose_m987163280 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_get_HasConnectionClose_m987163280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameValueCollection_t3047564564 * L_0 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_1 = Ext_Contains_m183431397(NULL /*static, unused*/, L_0, _stringLiteral208267350, _stringLiteral3033369902, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean WebSocketSharp.HttpResponse::get_IsProxyAuthenticationRequired()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104526415;
extern const uint32_t HttpResponse_get_IsProxyAuthenticationRequired_m1945167221_MetadataUsageId;
extern "C"  bool HttpResponse_get_IsProxyAuthenticationRequired_m1945167221 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_get_IsProxyAuthenticationRequired_m1945167221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__code_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral104526415, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean WebSocketSharp.HttpResponse::get_IsRedirect()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104526640;
extern Il2CppCodeGenString* _stringLiteral104526637;
extern const uint32_t HttpResponse_get_IsRedirect_m3194810376_MetadataUsageId;
extern "C"  bool HttpResponse_get_IsRedirect_m3194810376 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_get_IsRedirect_m3194810376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = __this->get__code_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral104526640, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = __this->get__code_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral104526637, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 1;
	}

IL_0028:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.HttpResponse::get_IsUnauthorized()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104526409;
extern const uint32_t HttpResponse_get_IsUnauthorized_m1649884238_MetadataUsageId;
extern "C"  bool HttpResponse_get_IsUnauthorized_m1649884238 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_get_IsUnauthorized_m1649884238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__code_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral104526409, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean WebSocketSharp.HttpResponse::get_IsWebSocketResponse()
extern Il2CppClass* HttpVersion_t4270509666_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104526574;
extern Il2CppCodeGenString* _stringLiteral3608358242;
extern Il2CppCodeGenString* _stringLiteral1942363561;
extern Il2CppCodeGenString* _stringLiteral208267350;
extern const uint32_t HttpResponse_get_IsWebSocketResponse_m3251240054_MetadataUsageId;
extern "C"  bool HttpResponse_get_IsWebSocketResponse_m3251240054 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_get_IsWebSocketResponse_m3251240054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NameValueCollection_t3047564564 * V_0 = NULL;
	int32_t G_B5_0 = 0;
	{
		NameValueCollection_t3047564564 * L_0 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Version_t1755874712 * L_1 = HttpBase_get_ProtocolVersion_m412752516(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HttpVersion_t4270509666_il2cpp_TypeInfo_var);
		Version_t1755874712 * L_2 = ((HttpVersion_t4270509666_StaticFields*)HttpVersion_t4270509666_il2cpp_TypeInfo_var->static_fields)->get_Version10_0();
		bool L_3 = Version_op_GreaterThan_m1157947872(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0058;
		}
	}
	{
		String_t* L_4 = __this->get__code_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral104526574, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0058;
		}
	}
	{
		NameValueCollection_t3047564564 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_7 = Ext_Contains_m183431397(NULL /*static, unused*/, L_6, _stringLiteral3608358242, _stringLiteral1942363561, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		NameValueCollection_t3047564564 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_9 = Ext_Contains_m183431397(NULL /*static, unused*/, L_8, _stringLiteral208267350, _stringLiteral3608358242, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_0059;
	}

IL_0058:
	{
		G_B5_0 = 0;
	}

IL_0059:
	{
		return (bool)G_B5_0;
	}
}
// System.String WebSocketSharp.HttpResponse::get_StatusCode()
extern "C"  String_t* HttpResponse_get_StatusCode_m1116886410 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__code_3();
		return L_0;
	}
}
// WebSocketSharp.HttpResponse WebSocketSharp.HttpResponse::Parse(System.String[])
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t1755874712_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpResponse_t2820540315_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1490481569;
extern const uint32_t HttpResponse_Parse_m1679504445_MetadataUsageId;
extern "C"  HttpResponse_t2820540315 * HttpResponse_Parse_m1679504445 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___headerParts0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_Parse_m1679504445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	WebHeaderCollection_t1932982249 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringU5BU5D_t1642385972* L_0 = ___headerParts0;
		NullCheck(L_0);
		int32_t L_1 = 0;
		String_t* L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		CharU5BU5D_t1328083999* L_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_2);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3350696563(L_2, L_3, 3, /*hidden argument*/NULL);
		V_0 = L_4;
		StringU5BU5D_t1642385972* L_5 = V_0;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0031;
		}
	}
	{
		StringU5BU5D_t1642385972* L_6 = ___headerParts0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		String_t* L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1490481569, L_8, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_10 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0031:
	{
		WebHeaderCollection_t1932982249 * L_11 = (WebHeaderCollection_t1932982249 *)il2cpp_codegen_object_new(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		WebHeaderCollection__ctor_m648964198(L_11, /*hidden argument*/NULL);
		V_1 = L_11;
		V_2 = 1;
		goto IL_004c;
	}

IL_003e:
	{
		WebHeaderCollection_t1932982249 * L_12 = V_1;
		StringU5BU5D_t1642385972* L_13 = ___headerParts0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_12);
		WebHeaderCollection_InternalSet_m4257553668(L_12, L_16, (bool)1, /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_18 = V_2;
		StringU5BU5D_t1642385972* L_19 = ___headerParts0;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		StringU5BU5D_t1642385972* L_20 = V_0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		String_t* L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		StringU5BU5D_t1642385972* L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = 2;
		String_t* L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		StringU5BU5D_t1642385972* L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = 0;
		String_t* L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		String_t* L_29 = String_Substring_m2032624251(L_28, 5, /*hidden argument*/NULL);
		Version_t1755874712 * L_30 = (Version_t1755874712 *)il2cpp_codegen_object_new(Version_t1755874712_il2cpp_TypeInfo_var);
		Version__ctor_m2972638031(L_30, L_29, /*hidden argument*/NULL);
		WebHeaderCollection_t1932982249 * L_31 = V_1;
		HttpResponse_t2820540315 * L_32 = (HttpResponse_t2820540315 *)il2cpp_codegen_object_new(HttpResponse_t2820540315_il2cpp_TypeInfo_var);
		HttpResponse__ctor_m948363293(L_32, L_22, L_25, L_30, L_31, /*hidden argument*/NULL);
		return L_32;
	}
}
// System.String WebSocketSharp.HttpResponse::ToString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3443848903;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral2840330715;
extern const uint32_t HttpResponse_ToString_m3489423176_MetadataUsageId;
extern "C"  String_t* HttpResponse_ToString_m3489423176 (HttpResponse_t2820540315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpResponse_ToString_m3489423176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	NameValueCollection_t3047564564 * V_1 = NULL;
	String_t* V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		Version_t1755874712 * L_3 = HttpBase_get_ProtocolVersion_m412752516(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_2;
		String_t* L_5 = __this->get__code_3();
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_4;
		String_t* L_7 = __this->get__reason_4();
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_6;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral2162321587);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2162321587);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1879616656(L_1, _stringLiteral3443848903, L_8, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_9 = HttpBase_get_Headers_m1195641886(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		NameValueCollection_t3047564564 * L_10 = V_1;
		NullCheck(L_10);
		StringU5BU5D_t1642385972* L_11 = VirtFuncInvoker0< StringU5BU5D_t1642385972* >::Invoke(16 /* System.String[] System.Collections.Specialized.NameValueCollection::get_AllKeys() */, L_10);
		V_3 = L_11;
		V_4 = 0;
		goto IL_0077;
	}

IL_0053:
	{
		StringU5BU5D_t1642385972* L_12 = V_3;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_2 = L_15;
		StringBuilder_t1221177846 * L_16 = V_0;
		String_t* L_17 = V_2;
		NameValueCollection_t3047564564 * L_18 = V_1;
		String_t* L_19 = V_2;
		NullCheck(L_18);
		String_t* L_20 = NameValueCollection_get_Item_m2776418562(L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_AppendFormat_m1666670800(L_16, _stringLiteral2840330715, L_17, L_20, _stringLiteral2162321587, /*hidden argument*/NULL);
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_22 = V_4;
		StringU5BU5D_t1642385972* L_23 = V_3;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_Append_m3636508479(L_24, _stringLiteral2162321587, /*hidden argument*/NULL);
		String_t* L_25 = HttpBase_get_EntityBody_m1135519468(__this, /*hidden argument*/NULL);
		V_5 = L_25;
		String_t* L_26 = V_5;
		NullCheck(L_26);
		int32_t L_27 = String_get_Length_m1606060069(L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_27) <= ((int32_t)0)))
		{
			goto IL_00ab;
		}
	}
	{
		StringBuilder_t1221177846 * L_28 = V_0;
		String_t* L_29 = V_5;
		NullCheck(L_28);
		StringBuilder_Append_m3636508479(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		StringBuilder_t1221177846 * L_30 = V_0;
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		return L_31;
	}
}
// System.Void WebSocketSharp.LogData::.ctor(WebSocketSharp.LogLevel,System.Diagnostics.StackFrame,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t LogData__ctor_m3812241173_MetadataUsageId;
extern "C"  void LogData__ctor_m3812241173 (LogData_t4095822710 * __this, int32_t ___level0, StackFrame_t2050294881 * ___caller1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogData__ctor_m3812241173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	LogData_t4095822710 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	LogData_t4095822710 * G_B1_1 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___level0;
		__this->set__level_2(L_0);
		StackFrame_t2050294881 * L_1 = ___caller1;
		__this->set__caller_0(L_1);
		String_t* L_2 = ___message2;
		String_t* L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = __this;
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = __this;
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_4;
		G_B2_1 = G_B1_1;
	}

IL_0022:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__message_3(G_B2_0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_5 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__date_1(L_5);
		return;
	}
}
// System.String WebSocketSharp.LogData::ToString()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* LogLevel_t2748531832_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral244949957;
extern Il2CppCodeGenString* _stringLiteral3348240795;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral3057600209;
extern Il2CppCodeGenString* _stringLiteral657790231;
extern Il2CppCodeGenString* _stringLiteral68307041;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t LogData_ToString_m1930073031_MetadataUsageId;
extern "C"  String_t* LogData_ToString_m1930073031 (LogData_t4095822710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogData_ToString_m1930073031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	MethodBase_t904190842 * V_1 = NULL;
	Type_t * V_2 = NULL;
	String_t* V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	StringBuilder_t1221177846 * V_5 = NULL;
	String_t* V_6 = NULL;
	int32_t V_7 = 0;
	{
		DateTime_t693205669  L_0 = __this->get__date_1();
		DateTime_t693205669  L_1 = L_0;
		Il2CppObject * L_2 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = __this->get__level_2();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(LogLevel_t2748531832_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral244949957, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		StackFrame_t2050294881 * L_7 = __this->get__caller_0();
		NullCheck(L_7);
		MethodBase_t904190842 * L_8 = VirtFuncInvoker0< MethodBase_t904190842 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_7);
		V_1 = L_8;
		MethodBase_t904190842 * L_9 = V_1;
		NullCheck(L_9);
		Type_t * L_10 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_9);
		V_2 = L_10;
		String_t* L_11 = V_0;
		Type_t * L_12 = V_2;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_12);
		MethodBase_t904190842 * L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_14);
		String_t* L_16 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral3348240795, L_11, L_13, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = __this->get__message_3();
		NullCheck(L_17);
		String_t* L_18 = String_Replace_m1941156251(L_17, _stringLiteral2162321587, _stringLiteral372029352, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_19 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_18);
		String_t* L_20 = String_TrimEnd_m3153143011(L_18, L_19, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_21 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_20);
		StringU5BU5D_t1642385972* L_22 = String_Split_m3326265864(L_20, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		StringU5BU5D_t1642385972* L_23 = V_4;
		NullCheck(L_23);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))))) > ((int32_t)1)))
		{
			goto IL_009f;
		}
	}
	{
		String_t* L_24 = V_3;
		String_t* L_25 = __this->get__message_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3057600209, L_24, L_25, /*hidden argument*/NULL);
		return L_26;
	}

IL_009f:
	{
		String_t* L_27 = V_3;
		StringU5BU5D_t1642385972* L_28 = V_4;
		NullCheck(L_28);
		int32_t L_29 = 0;
		String_t* L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral657790231, L_27, L_30, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_32 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1456828835(L_32, L_31, ((int32_t)64), /*hidden argument*/NULL);
		V_5 = L_32;
		String_t* L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m1606060069(L_33, /*hidden argument*/NULL);
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_35);
		String_t* L_37 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral68307041, L_36, /*hidden argument*/NULL);
		V_6 = L_37;
		V_7 = 1;
		goto IL_00f0;
	}

IL_00d6:
	{
		StringBuilder_t1221177846 * L_38 = V_5;
		String_t* L_39 = V_6;
		StringU5BU5D_t1642385972* L_40 = V_4;
		int32_t L_41 = V_7;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		String_t* L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_38);
		StringBuilder_AppendFormat_m759296786(L_38, L_39, _stringLiteral371857150, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_7;
		V_7 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00f0:
	{
		int32_t L_45 = V_7;
		StringU5BU5D_t1642385972* L_46 = V_4;
		NullCheck(L_46);
		if ((((int32_t)L_45) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))))
		{
			goto IL_00d6;
		}
	}
	{
		StringBuilder_t1221177846 * L_47 = V_5;
		StringBuilder_t1221177846 * L_48 = L_47;
		NullCheck(L_48);
		int32_t L_49 = StringBuilder_get_Length_m1608241323(L_48, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_set_Length_m3039225444(L_48, ((int32_t)((int32_t)L_49-(int32_t)1)), /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_50 = V_5;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		return L_51;
	}
}
// System.Void WebSocketSharp.Logger::.ctor()
extern "C"  void Logger__ctor_m3564207842 (Logger_t2598199114 * __this, const MethodInfo* method)
{
	{
		Logger__ctor_m4098367179(__this, 4, (String_t*)NULL, (Action_2_t502883108 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Logger::.ctor(WebSocketSharp.LogLevel,System.String,System.Action`2<WebSocketSharp.LogData,System.String>)
extern Il2CppClass* Logger_t2598199114_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t502883108_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* Logger_defaultOutput_m34181901_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m3114532927_MethodInfo_var;
extern const uint32_t Logger__ctor_m4098367179_MetadataUsageId;
extern "C"  void Logger__ctor_m4098367179 (Logger_t2598199114 * __this, int32_t ___level0, String_t* ___file1, Action_2_t502883108 * ___output2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger__ctor_m4098367179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t502883108 * G_B4_0 = NULL;
	Logger_t2598199114 * G_B4_1 = NULL;
	Action_2_t502883108 * G_B1_0 = NULL;
	Logger_t2598199114 * G_B1_1 = NULL;
	Logger_t2598199114 * G_B3_0 = NULL;
	Logger_t2598199114 * G_B2_0 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___level0;
		il2cpp_codegen_memory_barrier();
		__this->set__level_1(L_0);
		String_t* L_1 = ___file1;
		il2cpp_codegen_memory_barrier();
		__this->set__file_0(L_1);
		Action_2_t502883108 * L_2 = ___output2;
		Action_2_t502883108 * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = __this;
		if (L_3)
		{
			G_B4_0 = L_3;
			G_B4_1 = __this;
			goto IL_003e;
		}
	}
	{
		Action_2_t502883108 * L_4 = ((Logger_t2598199114_StaticFields*)Logger_t2598199114_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		G_B2_0 = G_B1_1;
		if (L_4)
		{
			G_B3_0 = G_B1_1;
			goto IL_0039;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)Logger_defaultOutput_m34181901_MethodInfo_var);
		Action_2_t502883108 * L_6 = (Action_2_t502883108 *)il2cpp_codegen_object_new(Action_2_t502883108_il2cpp_TypeInfo_var);
		Action_2__ctor_m3114532927(L_6, NULL, L_5, /*hidden argument*/Action_2__ctor_m3114532927_MethodInfo_var);
		((Logger_t2598199114_StaticFields*)Logger_t2598199114_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_4(L_6);
		G_B3_0 = G_B2_0;
	}

IL_0039:
	{
		Action_2_t502883108 * L_7 = ((Logger_t2598199114_StaticFields*)Logger_t2598199114_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_003e:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__output_2(G_B4_0);
		Il2CppObject * L_8 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_8, /*hidden argument*/NULL);
		__this->set__sync_3(L_8);
		return;
	}
}
// System.Void WebSocketSharp.Logger::defaultOutput(WebSocketSharp.LogData,System.String)
extern Il2CppClass* Console_t2311202731_il2cpp_TypeInfo_var;
extern const uint32_t Logger_defaultOutput_m34181901_MetadataUsageId;
extern "C"  void Logger_defaultOutput_m34181901 (Il2CppObject * __this /* static, unused */, LogData_t4095822710 * ___data0, String_t* ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_defaultOutput_m34181901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		LogData_t4095822710 * L_0 = ___data0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
		Console_WriteLine_m3271989373(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___path1;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_4 = ___path1;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_6 = V_0;
		String_t* L_7 = ___path1;
		Logger_writeToFile_m1073706148(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void WebSocketSharp.Logger::output(System.String,WebSocketSharp.LogLevel)
extern Il2CppClass* StackFrame_t2050294881_il2cpp_TypeInfo_var;
extern Il2CppClass* LogData_t4095822710_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t2311202731_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m1875803990_MethodInfo_var;
extern const uint32_t Logger_output_m1063266994_MetadataUsageId;
extern "C"  void Logger_output_m1063266994 (Logger_t2598199114 * __this, String_t* ___message0, int32_t ___level1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_output_m1063266994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	LogData_t4095822710 * V_1 = NULL;
	Exception_t1927440687 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__sync_3();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = __this->get__level_1();
			il2cpp_codegen_memory_barrier();
			int32_t L_3 = ___level1;
			if ((((int32_t)L_2) <= ((int32_t)L_3)))
			{
				goto IL_0020;
			}
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x7B, FINALLY_0074);
		}

IL_0020:
		{
			V_1 = (LogData_t4095822710 *)NULL;
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			int32_t L_4 = ___level1;
			StackFrame_t2050294881 * L_5 = (StackFrame_t2050294881 *)il2cpp_codegen_object_new(StackFrame_t2050294881_il2cpp_TypeInfo_var);
			StackFrame__ctor_m743151936(L_5, 2, (bool)1, /*hidden argument*/NULL);
			String_t* L_6 = ___message0;
			LogData_t4095822710 * L_7 = (LogData_t4095822710 *)il2cpp_codegen_object_new(LogData_t4095822710_il2cpp_TypeInfo_var);
			LogData__ctor_m3812241173(L_7, L_4, L_5, L_6, /*hidden argument*/NULL);
			V_1 = L_7;
			Action_2_t502883108 * L_8 = __this->get__output_2();
			LogData_t4095822710 * L_9 = V_1;
			String_t* L_10 = __this->get__file_0();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			Action_2_Invoke_m1875803990(L_8, L_9, L_10, /*hidden argument*/Action_2_Invoke_m1875803990_MethodInfo_var);
			goto IL_006f;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_004a;
			throw e;
		}

CATCH_004a:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t1927440687 *)__exception_local);
			StackFrame_t2050294881 * L_11 = (StackFrame_t2050294881 *)il2cpp_codegen_object_new(StackFrame_t2050294881_il2cpp_TypeInfo_var);
			StackFrame__ctor_m743151936(L_11, 0, (bool)1, /*hidden argument*/NULL);
			Exception_t1927440687 * L_12 = V_2;
			NullCheck(L_12);
			String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_12);
			LogData_t4095822710 * L_14 = (LogData_t4095822710 *)il2cpp_codegen_object_new(LogData_t4095822710_il2cpp_TypeInfo_var);
			LogData__ctor_m3812241173(L_14, 5, L_11, L_13, /*hidden argument*/NULL);
			V_1 = L_14;
			LogData_t4095822710 * L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
			goto IL_006f;
		} // end catch (depth: 2)

IL_006f:
		{
			IL2CPP_LEAVE(0x7B, FINALLY_0074);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		Il2CppObject * L_17 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void WebSocketSharp.Logger::writeToFile(System.String,System.String)
extern Il2CppClass* StreamWriter_t3858580635_il2cpp_TypeInfo_var;
extern Il2CppClass* TextWriter_t4027217640_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Logger_writeToFile_m1073706148_MetadataUsageId;
extern "C"  void Logger_writeToFile_m1073706148 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_writeToFile_m1073706148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StreamWriter_t3858580635 * V_0 = NULL;
	TextWriter_t4027217640 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___path1;
		StreamWriter_t3858580635 * L_1 = (StreamWriter_t3858580635 *)il2cpp_codegen_object_new(StreamWriter_t3858580635_il2cpp_TypeInfo_var);
		StreamWriter__ctor_m2615016597(L_1, L_0, (bool)1, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			StreamWriter_t3858580635 * L_2 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(TextWriter_t4027217640_il2cpp_TypeInfo_var);
			TextWriter_t4027217640 * L_3 = TextWriter_Synchronized_m724223080(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			V_1 = L_3;
		}

IL_000f:
		try
		{ // begin try (depth: 2)
			TextWriter_t4027217640 * L_4 = V_1;
			String_t* L_5 = ___value0;
			NullCheck(L_4);
			VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_4, L_5);
			IL2CPP_LEAVE(0x28, FINALLY_001b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_001b;
		}

FINALLY_001b:
		{ // begin finally (depth: 2)
			{
				TextWriter_t4027217640 * L_6 = V_1;
				if (!L_6)
				{
					goto IL_0027;
				}
			}

IL_0021:
			{
				TextWriter_t4027217640 * L_7 = V_1;
				NullCheck(L_7);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_7);
			}

IL_0027:
			{
				IL2CPP_END_FINALLY(27)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(27)
		{
			IL2CPP_JUMP_TBL(0x28, IL_0028)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0028:
		{
			IL2CPP_LEAVE(0x3A, FINALLY_002d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		{
			StreamWriter_t3858580635 * L_8 = V_0;
			if (!L_8)
			{
				goto IL_0039;
			}
		}

IL_0033:
		{
			StreamWriter_t3858580635 * L_9 = V_0;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(45)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003a:
	{
		return;
	}
}
// System.Void WebSocketSharp.Logger::Debug(System.String)
extern "C"  void Logger_Debug_m3721126547 (Logger_t2598199114 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__level_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		String_t* L_1 = ___message0;
		Logger_output_m1063266994(__this, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Logger::Error(System.String)
extern "C"  void Logger_Error_m3326994786 (Logger_t2598199114 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__level_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) <= ((int32_t)4)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		String_t* L_1 = ___message0;
		Logger_output_m1063266994(__this, L_1, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Logger::Fatal(System.String)
extern "C"  void Logger_Fatal_m3177508194 (Logger_t2598199114 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Logger_output_m1063266994(__this, L_0, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Logger::Info(System.String)
extern "C"  void Logger_Info_m2694160494 (Logger_t2598199114 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__level_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) <= ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		String_t* L_1 = ___message0;
		Logger_output_m1063266994(__this, L_1, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Logger::Trace(System.String)
extern "C"  void Logger_Trace_m763429895 (Logger_t2598199114 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__level_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		String_t* L_1 = ___message0;
		Logger_output_m1063266994(__this, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Logger::Warn(System.String)
extern "C"  void Logger_Warn_m2119571238 (Logger_t2598199114 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__level_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) <= ((int32_t)3)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		String_t* L_1 = ___message0;
		Logger_output_m1063266994(__this, L_1, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t MessageEventArgs__ctor_m2878951651_MetadataUsageId;
extern "C"  void MessageEventArgs__ctor_m2878951651 (MessageEventArgs_t2890051726 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEventArgs__ctor_m2878951651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		WebSocketFrame_t764750278 * L_0 = ___frame0;
		NullCheck(L_0);
		uint8_t L_1 = WebSocketFrame_get_Opcode_m1205139909(L_0, /*hidden argument*/NULL);
		__this->set__opcode_3(L_1);
		WebSocketFrame_t764750278 * L_2 = ___frame0;
		NullCheck(L_2);
		PayloadData_t3839327312 * L_3 = WebSocketFrame_get_PayloadData_m4003687813(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		ByteU5BU5D_t3397334013* L_4 = PayloadData_get_ApplicationData_m3068266889(L_3, /*hidden argument*/NULL);
		__this->set__rawData_4(L_4);
		return;
	}
}
// System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.Opcode,System.Byte[])
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern const uint32_t MessageEventArgs__ctor_m362763596_MetadataUsageId;
extern "C"  void MessageEventArgs__ctor_m362763596 (MessageEventArgs_t2890051726 * __this, uint8_t ___opcode0, ByteU5BU5D_t3397334013* ___rawData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEventArgs__ctor_m362763596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___rawData1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		uint64_t L_2 = ((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->get_MaxLength_8();
		if ((!(((uint64_t)L_1) > ((uint64_t)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		WebSocketException_t1348391352 * L_3 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2973187130(L_3, ((int32_t)1009), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		uint8_t L_4 = ___opcode0;
		__this->set__opcode_3(L_4);
		ByteU5BU5D_t3397334013* L_5 = ___rawData1;
		__this->set__rawData_4(L_5);
		return;
	}
}
// System.String WebSocketSharp.MessageEventArgs::get_Data()
extern "C"  String_t* MessageEventArgs_get_Data_m136184186 (MessageEventArgs_t2890051726 * __this, const MethodInfo* method)
{
	{
		MessageEventArgs_setData_m906492130(__this, /*hidden argument*/NULL);
		String_t* L_0 = __this->get__data_1();
		return L_0;
	}
}
// System.Void WebSocketSharp.MessageEventArgs::setData()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t MessageEventArgs_setData_m906492130_MetadataUsageId;
extern "C"  void MessageEventArgs_setData_m906492130 (MessageEventArgs_t2890051726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEventArgs_setData_m906492130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__dataSet_2();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		uint8_t L_1 = __this->get__opcode_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0020;
		}
	}
	{
		__this->set__dataSet_2((bool)1);
		return;
	}

IL_0020:
	{
		ByteU5BU5D_t3397334013* L_2 = __this->get__rawData_4();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_3 = Ext_UTF8Decode_m63039028(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set__data_1(L_3);
		__this->set__dataSet_2((bool)1);
		return;
	}
}
// System.Void WebSocketSharp.Net.AuthenticationBase::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern "C"  void AuthenticationBase__ctor_m401083951 (AuthenticationBase_t909684845 * __this, int32_t ___scheme0, NameValueCollection_t3047564564 * ___parameters1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___scheme0;
		__this->set__scheme_0(L_0);
		NameValueCollection_t3047564564 * L_1 = ___parameters1;
		__this->set_Parameters_1(L_1);
		return;
	}
}
// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.AuthenticationBase::get_Scheme()
extern "C"  int32_t AuthenticationBase_get_Scheme_m1504673019 (AuthenticationBase_t909684845 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__scheme_0();
		return L_0;
	}
}
// System.String WebSocketSharp.Net.AuthenticationBase::CreateNonceValue()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Random_t1044426839_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3231012720;
extern const uint32_t AuthenticationBase_CreateNonceValue_m1493611651_MetadataUsageId;
extern "C"  String_t* AuthenticationBase_CreateNonceValue_m1493611651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationBase_CreateNonceValue_m1493611651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	Random_t1044426839 * V_1 = NULL;
	StringBuilder_t1221177846 * V_2 = NULL;
	uint8_t V_3 = 0x0;
	ByteU5BU5D_t3397334013* V_4 = NULL;
	int32_t V_5 = 0;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		Random_t1044426839 * L_0 = (Random_t1044426839 *)il2cpp_codegen_object_new(Random_t1044426839_il2cpp_TypeInfo_var);
		Random__ctor_m1561335652(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		Random_t1044426839 * L_1 = V_1;
		ByteU5BU5D_t3397334013* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(5 /* System.Void System.Random::NextBytes(System.Byte[]) */, L_1, L_2);
		StringBuilder_t1221177846 * L_3 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_3, ((int32_t)32), /*hidden argument*/NULL);
		V_2 = L_3;
		ByteU5BU5D_t3397334013* L_4 = V_0;
		V_4 = L_4;
		V_5 = 0;
		goto IL_0047;
	}

IL_0028:
	{
		ByteU5BU5D_t3397334013* L_5 = V_4;
		int32_t L_6 = V_5;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_3 = L_8;
		StringBuilder_t1221177846 * L_9 = V_2;
		String_t* L_10 = Byte_ToString_m1309661918((&V_3), _stringLiteral3231012720, /*hidden argument*/NULL);
		NullCheck(L_9);
		StringBuilder_Append_m3636508479(L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_5;
		V_5 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_12 = V_5;
		ByteU5BU5D_t3397334013* L_13 = V_4;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		StringBuilder_t1221177846 * L_14 = V_2;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		return L_15;
	}
}
// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.AuthenticationBase::ParseParameters(System.String)
extern Il2CppClass* NameValueCollection_t3047564564_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2321347278_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3799711356_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t AuthenticationBase_ParseParameters_m2772080257_MetadataUsageId;
extern "C"  NameValueCollection_t3047564564 * AuthenticationBase_ParseParameters_m2772080257 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationBase_ParseParameters_m2772080257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NameValueCollection_t3047564564 * V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B5_0 = NULL;
	String_t* G_B10_0 = NULL;
	{
		NameValueCollection_t3047564564 * L_0 = (NameValueCollection_t3047564564 *)il2cpp_codegen_object_new(NameValueCollection_t3047564564_il2cpp_TypeInfo_var);
		NameValueCollection__ctor_m1767369537(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___value0;
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Ext_SplitHeaderValue_m3588285122(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t2321347278_il2cpp_TypeInfo_var, L_3);
		V_2 = L_4;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b2;
		}

IL_0022:
		{
			Il2CppObject* L_5 = V_2;
			NullCheck(L_5);
			String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t3799711356_il2cpp_TypeInfo_var, L_5);
			V_1 = L_6;
			String_t* L_7 = V_1;
			NullCheck(L_7);
			int32_t L_8 = String_IndexOf_m2358239236(L_7, ((int32_t)61), /*hidden argument*/NULL);
			V_3 = L_8;
			int32_t L_9 = V_3;
			if ((((int32_t)L_9) <= ((int32_t)0)))
			{
				goto IL_004b;
			}
		}

IL_0039:
		{
			String_t* L_10 = V_1;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			String_t* L_12 = String_Substring_m12482732(L_10, 0, L_11, /*hidden argument*/NULL);
			NullCheck(L_12);
			String_t* L_13 = String_Trim_m2668767713(L_12, /*hidden argument*/NULL);
			G_B5_0 = L_13;
			goto IL_004c;
		}

IL_004b:
		{
			G_B5_0 = ((String_t*)(NULL));
		}

IL_004c:
		{
			V_4 = G_B5_0;
			int32_t L_14 = V_3;
			if ((((int32_t)L_14) >= ((int32_t)0)))
			{
				goto IL_0070;
			}
		}

IL_0055:
		{
			String_t* L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = String_Trim_m2668767713(L_15, /*hidden argument*/NULL);
			CharU5BU5D_t1328083999* L_17 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_17);
			(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)34));
			NullCheck(L_16);
			String_t* L_18 = String_Trim_m3982520224(L_16, L_17, /*hidden argument*/NULL);
			G_B10_0 = L_18;
			goto IL_00a6;
		}

IL_0070:
		{
			int32_t L_19 = V_3;
			String_t* L_20 = V_1;
			NullCheck(L_20);
			int32_t L_21 = String_get_Length_m1606060069(L_20, /*hidden argument*/NULL);
			if ((((int32_t)L_19) >= ((int32_t)((int32_t)((int32_t)L_21-(int32_t)1)))))
			{
				goto IL_00a1;
			}
		}

IL_007e:
		{
			String_t* L_22 = V_1;
			int32_t L_23 = V_3;
			NullCheck(L_22);
			String_t* L_24 = String_Substring_m2032624251(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
			NullCheck(L_24);
			String_t* L_25 = String_Trim_m2668767713(L_24, /*hidden argument*/NULL);
			CharU5BU5D_t1328083999* L_26 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_26);
			(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)34));
			NullCheck(L_25);
			String_t* L_27 = String_Trim_m3982520224(L_25, L_26, /*hidden argument*/NULL);
			G_B10_0 = L_27;
			goto IL_00a6;
		}

IL_00a1:
		{
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_28 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			G_B10_0 = L_28;
		}

IL_00a6:
		{
			V_5 = G_B10_0;
			NameValueCollection_t3047564564 * L_29 = V_0;
			String_t* L_30 = V_4;
			String_t* L_31 = V_5;
			NullCheck(L_29);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(17 /* System.Void System.Collections.Specialized.NameValueCollection::Add(System.String,System.String) */, L_29, L_30, L_31);
		}

IL_00b2:
		{
			Il2CppObject* L_32 = V_2;
			NullCheck(L_32);
			bool L_33 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_32);
			if (L_33)
			{
				goto IL_0022;
			}
		}

IL_00bd:
		{
			IL2CPP_LEAVE(0xCF, FINALLY_00c2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c2;
	}

FINALLY_00c2:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_34 = V_2;
			if (!L_34)
			{
				goto IL_00ce;
			}
		}

IL_00c8:
		{
			Il2CppObject* L_35 = V_2;
			NullCheck(L_35);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_35);
		}

IL_00ce:
		{
			IL2CPP_END_FINALLY(194)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(194)
	{
		IL2CPP_JUMP_TBL(0xCF, IL_00cf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00cf:
	{
		NameValueCollection_t3047564564 * L_36 = V_0;
		return L_36;
	}
}
// System.String WebSocketSharp.Net.AuthenticationBase::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AuthenticationBase_ToString_m1178263339_MetadataUsageId;
extern "C"  String_t* AuthenticationBase_ToString_m1178263339 (AuthenticationBase_t909684845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationBase_ToString_m1178263339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B5_0 = NULL;
	{
		int32_t L_0 = __this->get__scheme_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String WebSocketSharp.Net.AuthenticationBase::ToBasicString() */, __this);
		G_B5_0 = L_1;
		goto IL_0033;
	}

IL_0017:
	{
		int32_t L_2 = __this->get__scheme_0();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String WebSocketSharp.Net.AuthenticationBase::ToDigestString() */, __this);
		G_B5_0 = L_3;
		goto IL_0033;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B5_0 = L_4;
	}

IL_0033:
	{
		return G_B5_0;
	}
}
// System.Void WebSocketSharp.Net.AuthenticationChallenge::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern "C"  void AuthenticationChallenge__ctor_m3414566521 (AuthenticationChallenge_t1146723439 * __this, int32_t ___scheme0, NameValueCollection_t3047564564 * ___parameters1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___scheme0;
		NameValueCollection_t3047564564 * L_1 = ___parameters1;
		AuthenticationBase__ctor_m401083951(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.Net.AuthenticationChallenge::Parse(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationChallenge_t1146723439_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128911352;
extern Il2CppCodeGenString* _stringLiteral815823174;
extern const uint32_t AuthenticationChallenge_Parse_m2486528551_MetadataUsageId;
extern "C"  AuthenticationChallenge_t1146723439 * AuthenticationChallenge_Parse_m2486528551 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationChallenge_Parse_m2486528551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	String_t* V_1 = NULL;
	AuthenticationChallenge_t1146723439 * G_B7_0 = NULL;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_0);
		StringU5BU5D_t1642385972* L_2 = String_Split_m3350696563(L_0, L_1, 2, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1642385972* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)2)))
		{
			goto IL_001e;
		}
	}
	{
		return (AuthenticationChallenge_t1146723439 *)NULL;
	}

IL_001e:
	{
		StringU5BU5D_t1642385972* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		String_t* L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		String_t* L_7 = String_ToLower_m2994460523(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral4128911352, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 1;
		String_t* L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NameValueCollection_t3047564564 * L_13 = AuthenticationBase_ParseParameters_m2772080257(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		AuthenticationChallenge_t1146723439 * L_14 = (AuthenticationChallenge_t1146723439 *)il2cpp_codegen_object_new(AuthenticationChallenge_t1146723439_il2cpp_TypeInfo_var);
		AuthenticationChallenge__ctor_m3414566521(L_14, 8, L_13, /*hidden argument*/NULL);
		G_B7_0 = L_14;
		goto IL_006e;
	}

IL_004a:
	{
		String_t* L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, _stringLiteral815823174, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_006d;
		}
	}
	{
		StringU5BU5D_t1642385972* L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		String_t* L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NameValueCollection_t3047564564 * L_20 = AuthenticationBase_ParseParameters_m2772080257(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		AuthenticationChallenge_t1146723439 * L_21 = (AuthenticationChallenge_t1146723439 *)il2cpp_codegen_object_new(AuthenticationChallenge_t1146723439_il2cpp_TypeInfo_var);
		AuthenticationChallenge__ctor_m3414566521(L_21, 1, L_20, /*hidden argument*/NULL);
		G_B7_0 = L_21;
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = ((AuthenticationChallenge_t1146723439 *)(NULL));
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// System.String WebSocketSharp.Net.AuthenticationChallenge::ToBasicString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1585498868;
extern Il2CppCodeGenString* _stringLiteral3967562967;
extern const uint32_t AuthenticationChallenge_ToBasicString_m4168534939_MetadataUsageId;
extern "C"  String_t* AuthenticationChallenge_ToBasicString_m4168534939 (AuthenticationChallenge_t1146723439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationChallenge_ToBasicString_m4168534939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameValueCollection_t3047564564 * L_0 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_0);
		String_t* L_1 = NameValueCollection_get_Item_m2776418562(L_0, _stringLiteral3967562967, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1585498868, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String WebSocketSharp.Net.AuthenticationChallenge::ToDigestString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3774104446;
extern Il2CppCodeGenString* _stringLiteral1013487924;
extern Il2CppCodeGenString* _stringLiteral3967562967;
extern Il2CppCodeGenString* _stringLiteral795234029;
extern Il2CppCodeGenString* _stringLiteral3597635731;
extern Il2CppCodeGenString* _stringLiteral2895712387;
extern Il2CppCodeGenString* _stringLiteral1160589310;
extern Il2CppCodeGenString* _stringLiteral627234723;
extern Il2CppCodeGenString* _stringLiteral2847215176;
extern Il2CppCodeGenString* _stringLiteral3249564939;
extern Il2CppCodeGenString* _stringLiteral386526964;
extern Il2CppCodeGenString* _stringLiteral1502598870;
extern Il2CppCodeGenString* _stringLiteral625614029;
extern const uint32_t AuthenticationChallenge_ToDigestString_m3565166461_MetadataUsageId;
extern "C"  String_t* AuthenticationChallenge_ToDigestString_m3565166461 (AuthenticationChallenge_t1146723439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationChallenge_ToDigestString_m3565166461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)128), /*hidden argument*/NULL);
		V_0 = L_0;
		NameValueCollection_t3047564564 * L_1 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_1);
		String_t* L_2 = NameValueCollection_get_Item_m2776418562(L_1, _stringLiteral3774104446, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = V_1;
		if (!L_3)
		{
			goto IL_0054;
		}
	}
	{
		StringBuilder_t1221177846 * L_4 = V_0;
		NameValueCollection_t3047564564 * L_5 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_5);
		String_t* L_6 = NameValueCollection_get_Item_m2776418562(L_5, _stringLiteral3967562967, /*hidden argument*/NULL);
		String_t* L_7 = V_1;
		NameValueCollection_t3047564564 * L_8 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_8);
		String_t* L_9 = NameValueCollection_get_Item_m2776418562(L_8, _stringLiteral795234029, /*hidden argument*/NULL);
		NullCheck(L_4);
		StringBuilder_AppendFormat_m1666670800(L_4, _stringLiteral1013487924, L_6, L_7, L_9, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0054:
	{
		StringBuilder_t1221177846 * L_10 = V_0;
		NameValueCollection_t3047564564 * L_11 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_11);
		String_t* L_12 = NameValueCollection_get_Item_m2776418562(L_11, _stringLiteral3967562967, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_13 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_13);
		String_t* L_14 = NameValueCollection_get_Item_m2776418562(L_13, _stringLiteral795234029, /*hidden argument*/NULL);
		NullCheck(L_10);
		StringBuilder_AppendFormat_m759296786(L_10, _stringLiteral3597635731, L_12, L_14, /*hidden argument*/NULL);
	}

IL_0080:
	{
		NameValueCollection_t3047564564 * L_15 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_15);
		String_t* L_16 = NameValueCollection_get_Item_m2776418562(L_15, _stringLiteral2895712387, /*hidden argument*/NULL);
		V_2 = L_16;
		String_t* L_17 = V_2;
		if (!L_17)
		{
			goto IL_00a4;
		}
	}
	{
		StringBuilder_t1221177846 * L_18 = V_0;
		String_t* L_19 = V_2;
		NullCheck(L_18);
		StringBuilder_AppendFormat_m3265503696(L_18, _stringLiteral1160589310, L_19, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		NameValueCollection_t3047564564 * L_20 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_20);
		String_t* L_21 = NameValueCollection_get_Item_m2776418562(L_20, _stringLiteral627234723, /*hidden argument*/NULL);
		V_3 = L_21;
		String_t* L_22 = V_3;
		if (!L_22)
		{
			goto IL_00c8;
		}
	}
	{
		StringBuilder_t1221177846 * L_23 = V_0;
		String_t* L_24 = V_3;
		NullCheck(L_23);
		StringBuilder_AppendFormat_m3265503696(L_23, _stringLiteral2847215176, L_24, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		NameValueCollection_t3047564564 * L_25 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_25);
		String_t* L_26 = NameValueCollection_get_Item_m2776418562(L_25, _stringLiteral3249564939, /*hidden argument*/NULL);
		V_4 = L_26;
		String_t* L_27 = V_4;
		if (!L_27)
		{
			goto IL_00ef;
		}
	}
	{
		StringBuilder_t1221177846 * L_28 = V_0;
		String_t* L_29 = V_4;
		NullCheck(L_28);
		StringBuilder_AppendFormat_m3265503696(L_28, _stringLiteral386526964, L_29, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		NameValueCollection_t3047564564 * L_30 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_30);
		String_t* L_31 = NameValueCollection_get_Item_m2776418562(L_30, _stringLiteral1502598870, /*hidden argument*/NULL);
		V_5 = L_31;
		String_t* L_32 = V_5;
		if (!L_32)
		{
			goto IL_0116;
		}
	}
	{
		StringBuilder_t1221177846 * L_33 = V_0;
		String_t* L_34 = V_5;
		NullCheck(L_33);
		StringBuilder_AppendFormat_m3265503696(L_33, _stringLiteral625614029, L_34, /*hidden argument*/NULL);
	}

IL_0116:
	{
		StringBuilder_t1221177846 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		return L_36;
	}
}
// System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.NetworkCredential)
extern Il2CppClass* NameValueCollection_t3047564564_il2cpp_TypeInfo_var;
extern const uint32_t AuthenticationResponse__ctor_m1022749245_MetadataUsageId;
extern "C"  void AuthenticationResponse__ctor_m1022749245 (AuthenticationResponse_t1212723231 * __this, NetworkCredential_t3911206805 * ___credentials0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse__ctor_m1022749245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameValueCollection_t3047564564 * L_0 = (NameValueCollection_t3047564564 *)il2cpp_codegen_object_new(NameValueCollection_t3047564564_il2cpp_TypeInfo_var);
		NameValueCollection__ctor_m1767369537(L_0, /*hidden argument*/NULL);
		NetworkCredential_t3911206805 * L_1 = ___credentials0;
		AuthenticationResponse__ctor_m705882896(__this, 8, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationChallenge,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern "C"  void AuthenticationResponse__ctor_m3805970484 (AuthenticationResponse_t1212723231 * __this, AuthenticationChallenge_t1146723439 * ___challenge0, NetworkCredential_t3911206805 * ___credentials1, uint32_t ___nonceCount2, const MethodInfo* method)
{
	{
		AuthenticationChallenge_t1146723439 * L_0 = ___challenge0;
		NullCheck(L_0);
		int32_t L_1 = AuthenticationBase_get_Scheme_m1504673019(L_0, /*hidden argument*/NULL);
		AuthenticationChallenge_t1146723439 * L_2 = ___challenge0;
		NullCheck(L_2);
		NameValueCollection_t3047564564 * L_3 = ((AuthenticationBase_t909684845 *)L_2)->get_Parameters_1();
		NetworkCredential_t3911206805 * L_4 = ___credentials1;
		uint32_t L_5 = ___nonceCount2;
		AuthenticationResponse__ctor_m705882896(__this, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern Il2CppCodeGenString* _stringLiteral3726277302;
extern Il2CppCodeGenString* _stringLiteral3561732527;
extern Il2CppCodeGenString* _stringLiteral386853516;
extern const uint32_t AuthenticationResponse__ctor_m705882896_MetadataUsageId;
extern "C"  void AuthenticationResponse__ctor_m705882896 (AuthenticationResponse_t1212723231 * __this, int32_t ___scheme0, NameValueCollection_t3047564564 * ___parameters1, NetworkCredential_t3911206805 * ___credentials2, uint32_t ___nonceCount3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse__ctor_m705882896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___scheme0;
		NameValueCollection_t3047564564 * L_1 = ___parameters1;
		AuthenticationBase__ctor_m401083951(__this, L_0, L_1, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_2 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NetworkCredential_t3911206805 * L_3 = ___credentials2;
		NullCheck(L_3);
		String_t* L_4 = NetworkCredential_get_UserName_m2976578456(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		NameValueCollection_set_Item_m3775607929(L_2, _stringLiteral3726277302, L_4, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_5 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NetworkCredential_t3911206805 * L_6 = ___credentials2;
		NullCheck(L_6);
		String_t* L_7 = NetworkCredential_get_Password_m659916903(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		NameValueCollection_set_Item_m3775607929(L_5, _stringLiteral3561732527, L_7, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_8 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NetworkCredential_t3911206805 * L_9 = ___credentials2;
		NullCheck(L_9);
		String_t* L_10 = NetworkCredential_get_Domain_m3112363792(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		NameValueCollection_set_Item_m3775607929(L_8, _stringLiteral386853516, L_10, /*hidden argument*/NULL);
		uint32_t L_11 = ___nonceCount3;
		__this->set__nonceCount_2(L_11);
		int32_t L_12 = ___scheme0;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_005f;
		}
	}
	{
		AuthenticationResponse_initAsDigest_m686452706(__this, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.UInt32 WebSocketSharp.Net.AuthenticationResponse::get_NonceCount()
extern "C"  uint32_t AuthenticationResponse_get_NonceCount_m2749455448 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method)
{
	uint32_t G_B3_0 = 0;
	{
		uint32_t L_0 = __this->get__nonceCount_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)(-1)))))
		{
			goto IL_0017;
		}
	}
	{
		uint32_t L_1 = __this->get__nonceCount_2();
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = ((uint32_t)(0));
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4248128977;
extern const uint32_t AuthenticationResponse_createA1_m953246629_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_createA1_m953246629 (Il2CppObject * __this /* static, unused */, String_t* ___username0, String_t* ___password1, String_t* ___realm2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_createA1_m953246629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___username0;
		String_t* L_1 = ___realm2;
		String_t* L_2 = ___password1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4248128977, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4248128977;
extern const uint32_t AuthenticationResponse_createA1_m811929829_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_createA1_m811929829 (Il2CppObject * __this /* static, unused */, String_t* ___username0, String_t* ___password1, String_t* ___realm2, String_t* ___nonce3, String_t* ___cnonce4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_createA1_m811929829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___username0;
		String_t* L_1 = ___password1;
		String_t* L_2 = ___realm2;
		String_t* L_3 = AuthenticationResponse_createA1_m953246629(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		String_t* L_4 = AuthenticationResponse_hash_m2875148591(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___nonce3;
		String_t* L_6 = ___cnonce4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4248128977, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral240174043;
extern const uint32_t AuthenticationResponse_createA2_m3596400622_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_createA2_m3596400622 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___uri1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_createA2_m3596400622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___method0;
		String_t* L_1 = ___uri1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral240174043, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4248128977;
extern const uint32_t AuthenticationResponse_createA2_m3539481730_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_createA2_m3539481730 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___uri1, String_t* ___entity2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_createA2_m3539481730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___method0;
		String_t* L_1 = ___uri1;
		String_t* L_2 = ___entity2;
		String_t* L_3 = AuthenticationResponse_hash_m2875148591(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4248128977, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::hash(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3231012720;
extern const uint32_t AuthenticationResponse_hash_m2875148591_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_hash_m2875148591 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_hash_m2875148591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	MD5_t1507972490 * V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	StringBuilder_t1221177846 * V_3 = NULL;
	uint8_t V_4 = 0x0;
	ByteU5BU5D_t3397334013* V_5 = NULL;
	int32_t V_6 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_0 = L_2;
		MD5_t1507972490 * L_3 = MD5_Create_m1572997499(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		MD5_t1507972490 * L_4 = V_1;
		ByteU5BU5D_t3397334013* L_5 = V_0;
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_6 = HashAlgorithm_ComputeHash_m3637856778(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		StringBuilder_t1221177846 * L_7 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_7, ((int32_t)64), /*hidden argument*/NULL);
		V_3 = L_7;
		ByteU5BU5D_t3397334013* L_8 = V_2;
		V_5 = L_8;
		V_6 = 0;
		goto IL_004d;
	}

IL_002d:
	{
		ByteU5BU5D_t3397334013* L_9 = V_5;
		int32_t L_10 = V_6;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		uint8_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_4 = L_12;
		StringBuilder_t1221177846 * L_13 = V_3;
		String_t* L_14 = Byte_ToString_m1309661918((&V_4), _stringLiteral3231012720, /*hidden argument*/NULL);
		NullCheck(L_13);
		StringBuilder_Append_m3636508479(L_13, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_6;
		V_6 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_16 = V_6;
		ByteU5BU5D_t3397334013* L_17 = V_5;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		StringBuilder_t1221177846 * L_18 = V_3;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		return L_19;
	}
}
// System.Void WebSocketSharp.Net.AuthenticationResponse::initAsDigest()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1989381442_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* AuthenticationResponse_U3CinitAsDigestU3Em__0_m1328643795_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3583450723_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m856096519_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1502598870;
extern Il2CppCodeGenString* _stringLiteral4069222856;
extern Il2CppCodeGenString* _stringLiteral2825867754;
extern Il2CppCodeGenString* _stringLiteral3822513897;
extern Il2CppCodeGenString* _stringLiteral4038829036;
extern Il2CppCodeGenString* _stringLiteral165463247;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern Il2CppCodeGenString* _stringLiteral806622719;
extern const uint32_t AuthenticationResponse_initAsDigest_m686452706_MetadataUsageId;
extern "C"  void AuthenticationResponse_initAsDigest_m686452706 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_initAsDigest_m686452706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	uint32_t V_1 = 0;
	StringU5BU5D_t1642385972* G_B3_0 = NULL;
	StringU5BU5D_t1642385972* G_B2_0 = NULL;
	{
		NameValueCollection_t3047564564 * L_0 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_0);
		String_t* L_1 = NameValueCollection_get_Item_m2776418562(L_0, _stringLiteral1502598870, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_00bf;
		}
	}
	{
		String_t* L_3 = V_0;
		CharU5BU5D_t1328083999* L_4 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_3);
		StringU5BU5D_t1642385972* L_5 = String_Split_m3326265864(L_3, L_4, /*hidden argument*/NULL);
		Func_2_t1989381442 * L_6 = ((AuthenticationResponse_t1212723231_StaticFields*)AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_3();
		G_B2_0 = L_5;
		if (L_6)
		{
			G_B3_0 = L_5;
			goto IL_0040;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)AuthenticationResponse_U3CinitAsDigestU3Em__0_m1328643795_MethodInfo_var);
		Func_2_t1989381442 * L_8 = (Func_2_t1989381442 *)il2cpp_codegen_object_new(Func_2_t1989381442_il2cpp_TypeInfo_var);
		Func_2__ctor_m3583450723(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m3583450723_MethodInfo_var);
		((AuthenticationResponse_t1212723231_StaticFields*)AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_3(L_8);
		G_B3_0 = G_B2_0;
	}

IL_0040:
	{
		Func_2_t1989381442 * L_9 = ((AuthenticationResponse_t1212723231_StaticFields*)AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_3();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_10 = Ext_Contains_TisString_t_m856096519(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B3_0, L_9, /*hidden argument*/Ext_Contains_TisString_t_m856096519_MethodInfo_var);
		if (!L_10)
		{
			goto IL_00ae;
		}
	}
	{
		NameValueCollection_t3047564564 * L_11 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_11);
		NameValueCollection_set_Item_m3775607929(L_11, _stringLiteral1502598870, _stringLiteral4069222856, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_12 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		String_t* L_13 = AuthenticationBase_CreateNonceValue_m1493611651(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		NameValueCollection_set_Item_m3775607929(L_12, _stringLiteral2825867754, L_13, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_14 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		uint32_t L_15 = __this->get__nonceCount_2();
		int32_t L_16 = ((int32_t)((int32_t)L_15+(int32_t)1));
		V_1 = L_16;
		__this->set__nonceCount_2(L_16);
		uint32_t L_17 = V_1;
		uint32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4038829036, L_19, /*hidden argument*/NULL);
		NullCheck(L_14);
		NameValueCollection_set_Item_m3775607929(L_14, _stringLiteral3822513897, L_20, /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_00ae:
	{
		NameValueCollection_t3047564564 * L_21 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_21);
		NameValueCollection_set_Item_m3775607929(L_21, _stringLiteral1502598870, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		NameValueCollection_t3047564564 * L_22 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_22);
		NameValueCollection_set_Item_m3775607929(L_22, _stringLiteral165463247, _stringLiteral1596707798, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_23 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NameValueCollection_t3047564564 * L_24 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		String_t* L_25 = AuthenticationResponse_CreateRequestDigest_m1063439045(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		NameValueCollection_set_Item_m3775607929(L_23, _stringLiteral806622719, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3726277302;
extern Il2CppCodeGenString* _stringLiteral3561732527;
extern Il2CppCodeGenString* _stringLiteral3967562967;
extern Il2CppCodeGenString* _stringLiteral795234029;
extern Il2CppCodeGenString* _stringLiteral386853516;
extern Il2CppCodeGenString* _stringLiteral3249564939;
extern Il2CppCodeGenString* _stringLiteral1502598870;
extern Il2CppCodeGenString* _stringLiteral2825867754;
extern Il2CppCodeGenString* _stringLiteral3822513897;
extern Il2CppCodeGenString* _stringLiteral165463247;
extern Il2CppCodeGenString* _stringLiteral2208660927;
extern Il2CppCodeGenString* _stringLiteral873161548;
extern Il2CppCodeGenString* _stringLiteral2180653895;
extern Il2CppCodeGenString* _stringLiteral818381720;
extern Il2CppCodeGenString* _stringLiteral240174043;
extern const uint32_t AuthenticationResponse_CreateRequestDigest_m1063439045_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_CreateRequestDigest_m1063439045 (Il2CppObject * __this /* static, unused */, NameValueCollection_t3047564564 * ___parameters0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_CreateRequestDigest_m1063439045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B11_0 = NULL;
	{
		NameValueCollection_t3047564564 * L_0 = ___parameters0;
		NullCheck(L_0);
		String_t* L_1 = NameValueCollection_get_Item_m2776418562(L_0, _stringLiteral3726277302, /*hidden argument*/NULL);
		V_0 = L_1;
		NameValueCollection_t3047564564 * L_2 = ___parameters0;
		NullCheck(L_2);
		String_t* L_3 = NameValueCollection_get_Item_m2776418562(L_2, _stringLiteral3561732527, /*hidden argument*/NULL);
		V_1 = L_3;
		NameValueCollection_t3047564564 * L_4 = ___parameters0;
		NullCheck(L_4);
		String_t* L_5 = NameValueCollection_get_Item_m2776418562(L_4, _stringLiteral3967562967, /*hidden argument*/NULL);
		V_2 = L_5;
		NameValueCollection_t3047564564 * L_6 = ___parameters0;
		NullCheck(L_6);
		String_t* L_7 = NameValueCollection_get_Item_m2776418562(L_6, _stringLiteral795234029, /*hidden argument*/NULL);
		V_3 = L_7;
		NameValueCollection_t3047564564 * L_8 = ___parameters0;
		NullCheck(L_8);
		String_t* L_9 = NameValueCollection_get_Item_m2776418562(L_8, _stringLiteral386853516, /*hidden argument*/NULL);
		V_4 = L_9;
		NameValueCollection_t3047564564 * L_10 = ___parameters0;
		NullCheck(L_10);
		String_t* L_11 = NameValueCollection_get_Item_m2776418562(L_10, _stringLiteral3249564939, /*hidden argument*/NULL);
		V_5 = L_11;
		NameValueCollection_t3047564564 * L_12 = ___parameters0;
		NullCheck(L_12);
		String_t* L_13 = NameValueCollection_get_Item_m2776418562(L_12, _stringLiteral1502598870, /*hidden argument*/NULL);
		V_6 = L_13;
		NameValueCollection_t3047564564 * L_14 = ___parameters0;
		NullCheck(L_14);
		String_t* L_15 = NameValueCollection_get_Item_m2776418562(L_14, _stringLiteral2825867754, /*hidden argument*/NULL);
		V_7 = L_15;
		NameValueCollection_t3047564564 * L_16 = ___parameters0;
		NullCheck(L_16);
		String_t* L_17 = NameValueCollection_get_Item_m2776418562(L_16, _stringLiteral3822513897, /*hidden argument*/NULL);
		V_8 = L_17;
		NameValueCollection_t3047564564 * L_18 = ___parameters0;
		NullCheck(L_18);
		String_t* L_19 = NameValueCollection_get_Item_m2776418562(L_18, _stringLiteral165463247, /*hidden argument*/NULL);
		V_9 = L_19;
		String_t* L_20 = V_5;
		if (!L_20)
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_21 = V_5;
		NullCheck(L_21);
		String_t* L_22 = String_ToLower_m2994460523(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_22, _stringLiteral2208660927, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_24 = V_0;
		String_t* L_25 = V_1;
		String_t* L_26 = V_2;
		String_t* L_27 = V_3;
		String_t* L_28 = V_7;
		String_t* L_29 = AuthenticationResponse_createA1_m811929829(NULL /*static, unused*/, L_24, L_25, L_26, L_27, L_28, /*hidden argument*/NULL);
		G_B4_0 = L_29;
		goto IL_00b3;
	}

IL_00ab:
	{
		String_t* L_30 = V_0;
		String_t* L_31 = V_1;
		String_t* L_32 = V_2;
		String_t* L_33 = AuthenticationResponse_createA1_m953246629(NULL /*static, unused*/, L_30, L_31, L_32, /*hidden argument*/NULL);
		G_B4_0 = L_33;
	}

IL_00b3:
	{
		V_10 = G_B4_0;
		String_t* L_34 = V_6;
		if (!L_34)
		{
			goto IL_00eb;
		}
	}
	{
		String_t* L_35 = V_6;
		NullCheck(L_35);
		String_t* L_36 = String_ToLower_m2994460523(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_36, _stringLiteral873161548, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00eb;
		}
	}
	{
		String_t* L_38 = V_9;
		String_t* L_39 = V_4;
		NameValueCollection_t3047564564 * L_40 = ___parameters0;
		NullCheck(L_40);
		String_t* L_41 = NameValueCollection_get_Item_m2776418562(L_40, _stringLiteral2180653895, /*hidden argument*/NULL);
		String_t* L_42 = AuthenticationResponse_createA2_m3539481730(NULL /*static, unused*/, L_38, L_39, L_41, /*hidden argument*/NULL);
		G_B8_0 = L_42;
		goto IL_00f4;
	}

IL_00eb:
	{
		String_t* L_43 = V_9;
		String_t* L_44 = V_4;
		String_t* L_45 = AuthenticationResponse_createA2_m3596400622(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		G_B8_0 = L_45;
	}

IL_00f4:
	{
		V_11 = G_B8_0;
		String_t* L_46 = V_10;
		String_t* L_47 = AuthenticationResponse_hash_m2875148591(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		V_12 = L_47;
		String_t* L_48 = V_6;
		if (!L_48)
		{
			goto IL_0138;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_49 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_50 = V_3;
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_50);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_50);
		ObjectU5BU5D_t3614634134* L_51 = L_49;
		String_t* L_52 = V_8;
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_52);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_52);
		ObjectU5BU5D_t3614634134* L_53 = L_51;
		String_t* L_54 = V_7;
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, L_54);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_54);
		ObjectU5BU5D_t3614634134* L_55 = L_53;
		String_t* L_56 = V_6;
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_56);
		ObjectU5BU5D_t3614634134* L_57 = L_55;
		String_t* L_58 = V_11;
		String_t* L_59 = AuthenticationResponse_hash_m2875148591(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral818381720, L_57, /*hidden argument*/NULL);
		G_B11_0 = L_60;
		goto IL_014a;
	}

IL_0138:
	{
		String_t* L_61 = V_3;
		String_t* L_62 = V_11;
		String_t* L_63 = AuthenticationResponse_hash_m2875148591(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral240174043, L_61, L_63, /*hidden argument*/NULL);
		G_B11_0 = L_64;
	}

IL_014a:
	{
		V_13 = G_B11_0;
		String_t* L_65 = V_12;
		String_t* L_66 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral240174043, L_65, L_66, /*hidden argument*/NULL);
		String_t* L_68 = AuthenticationResponse_hash_m2875148591(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		return L_68;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::ToBasicString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral240174043;
extern Il2CppCodeGenString* _stringLiteral3726277302;
extern Il2CppCodeGenString* _stringLiteral3561732527;
extern Il2CppCodeGenString* _stringLiteral4085839064;
extern const uint32_t AuthenticationResponse_ToBasicString_m679739463_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_ToBasicString_m679739463 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_ToBasicString_m679739463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		NameValueCollection_t3047564564 * L_0 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_0);
		String_t* L_1 = NameValueCollection_get_Item_m2776418562(L_0, _stringLiteral3726277302, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_2 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_2);
		String_t* L_3 = NameValueCollection_get_Item_m2776418562(L_2, _stringLiteral3561732527, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral240174043, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_5 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		NullCheck(L_5);
		ByteU5BU5D_t3397334013* L_7 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_8 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = V_1;
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4085839064, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.String WebSocketSharp.Net.AuthenticationResponse::ToDigestString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1255049426;
extern Il2CppCodeGenString* _stringLiteral3726277302;
extern Il2CppCodeGenString* _stringLiteral3967562967;
extern Il2CppCodeGenString* _stringLiteral795234029;
extern Il2CppCodeGenString* _stringLiteral386853516;
extern Il2CppCodeGenString* _stringLiteral806622719;
extern Il2CppCodeGenString* _stringLiteral2895712387;
extern Il2CppCodeGenString* _stringLiteral1160589310;
extern Il2CppCodeGenString* _stringLiteral3249564939;
extern Il2CppCodeGenString* _stringLiteral386526964;
extern Il2CppCodeGenString* _stringLiteral1502598870;
extern Il2CppCodeGenString* _stringLiteral1057904473;
extern Il2CppCodeGenString* _stringLiteral2825867754;
extern Il2CppCodeGenString* _stringLiteral3822513897;
extern const uint32_t AuthenticationResponse_ToDigestString_m2983562877_MetadataUsageId;
extern "C"  String_t* AuthenticationResponse_ToDigestString_m2983562877 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_ToDigestString_m2983562877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)256), /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NameValueCollection_t3047564564 * L_3 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_3);
		String_t* L_4 = NameValueCollection_get_Item_m2776418562(L_3, _stringLiteral3726277302, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_2;
		NameValueCollection_t3047564564 * L_6 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_6);
		String_t* L_7 = NameValueCollection_get_Item_m2776418562(L_6, _stringLiteral3967562967, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_5;
		NameValueCollection_t3047564564 * L_9 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_9);
		String_t* L_10 = NameValueCollection_get_Item_m2776418562(L_9, _stringLiteral795234029, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = L_8;
		NameValueCollection_t3047564564 * L_12 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_12);
		String_t* L_13 = NameValueCollection_get_Item_m2776418562(L_12, _stringLiteral386853516, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		ObjectU5BU5D_t3614634134* L_14 = L_11;
		NameValueCollection_t3047564564 * L_15 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_15);
		String_t* L_16 = NameValueCollection_get_Item_m2776418562(L_15, _stringLiteral806622719, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_16);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1879616656(L_1, _stringLiteral1255049426, L_14, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_17 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_17);
		String_t* L_18 = NameValueCollection_get_Item_m2776418562(L_17, _stringLiteral2895712387, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = V_1;
		if (!L_19)
		{
			goto IL_00a0;
		}
	}
	{
		StringBuilder_t1221177846 * L_20 = V_0;
		String_t* L_21 = V_1;
		NullCheck(L_20);
		StringBuilder_AppendFormat_m3265503696(L_20, _stringLiteral1160589310, L_21, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		NameValueCollection_t3047564564 * L_22 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_22);
		String_t* L_23 = NameValueCollection_get_Item_m2776418562(L_22, _stringLiteral3249564939, /*hidden argument*/NULL);
		V_2 = L_23;
		String_t* L_24 = V_2;
		if (!L_24)
		{
			goto IL_00c4;
		}
	}
	{
		StringBuilder_t1221177846 * L_25 = V_0;
		String_t* L_26 = V_2;
		NullCheck(L_25);
		StringBuilder_AppendFormat_m3265503696(L_25, _stringLiteral386526964, L_26, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		NameValueCollection_t3047564564 * L_27 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_27);
		String_t* L_28 = NameValueCollection_get_Item_m2776418562(L_27, _stringLiteral1502598870, /*hidden argument*/NULL);
		V_3 = L_28;
		String_t* L_29 = V_3;
		if (!L_29)
		{
			goto IL_0108;
		}
	}
	{
		StringBuilder_t1221177846 * L_30 = V_0;
		String_t* L_31 = V_3;
		NameValueCollection_t3047564564 * L_32 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_32);
		String_t* L_33 = NameValueCollection_get_Item_m2776418562(L_32, _stringLiteral2825867754, /*hidden argument*/NULL);
		NameValueCollection_t3047564564 * L_34 = ((AuthenticationBase_t909684845 *)__this)->get_Parameters_1();
		NullCheck(L_34);
		String_t* L_35 = NameValueCollection_get_Item_m2776418562(L_34, _stringLiteral3822513897, /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_AppendFormat_m1666670800(L_30, _stringLiteral1057904473, L_31, L_33, L_35, /*hidden argument*/NULL);
	}

IL_0108:
	{
		StringBuilder_t1221177846 * L_36 = V_0;
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_36);
		return L_37;
	}
}
// System.Boolean WebSocketSharp.Net.AuthenticationResponse::<initAsDigest>m__0(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4069222856;
extern const uint32_t AuthenticationResponse_U3CinitAsDigestU3Em__0_m1328643795_MetadataUsageId;
extern "C"  bool AuthenticationResponse_U3CinitAsDigestU3Em__0_m1328643795 (Il2CppObject * __this /* static, unused */, String_t* ___qop0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthenticationResponse_U3CinitAsDigestU3Em__0_m1328643795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___qop0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m2668767713(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = String_ToLower_m2994460523(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral4069222856, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String)
extern "C"  void ClientSslConfiguration__ctor_m596200420 (ClientSslConfiguration_t1159130081 * __this, String_t* ___targetHost0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___targetHost0;
		ClientSslConfiguration__ctor_m573646549(__this, L_0, (X509CertificateCollection_t1197680765 *)NULL, ((int32_t)240), (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean)
extern "C"  void ClientSslConfiguration__ctor_m573646549 (ClientSslConfiguration_t1159130081 * __this, String_t* ___targetHost0, X509CertificateCollection_t1197680765 * ___clientCertificates1, int32_t ___enabledSslProtocols2, bool ___checkCertificateRevocation3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___enabledSslProtocols2;
		bool L_1 = ___checkCertificateRevocation3;
		SslConfiguration__ctor_m3803613030(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___targetHost0;
		__this->set__host_7(L_2);
		X509CertificateCollection_t1197680765 * L_3 = ___clientCertificates1;
		__this->set__certs_6(L_3);
		return;
	}
}
// System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificates()
extern "C"  X509CertificateCollection_t1197680765 * ClientSslConfiguration_get_ClientCertificates_m3726515733 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method)
{
	{
		X509CertificateCollection_t1197680765 * L_0 = __this->get__certs_6();
		return L_0;
	}
}
// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern "C"  LocalCertificateSelectionCallback_t3696771181 * ClientSslConfiguration_get_ClientCertificateSelectionCallback_m3153947016 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method)
{
	{
		LocalCertificateSelectionCallback_t3696771181 * L_0 = SslConfiguration_get_CertificateSelectionCallback_m3447833320(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern "C"  RemoteCertificateValidationCallback_t2756269959 * ClientSslConfiguration_get_ServerCertificateValidationCallback_m3599237829 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method)
{
	{
		RemoteCertificateValidationCallback_t2756269959 * L_0 = SslConfiguration_get_CertificateValidationCallback_m3145107081(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String WebSocketSharp.Net.ClientSslConfiguration::get_TargetHost()
extern "C"  String_t* ClientSslConfiguration_get_TargetHost_m740170825 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__host_7();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::.cctor()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0_FieldInfo_var;
extern const uint32_t Cookie__cctor_m2912269528_MetadataUsageId;
extern "C"  void Cookie__cctor_m2912269528 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie__cctor_m2912269528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)7));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0_FieldInfo_var), /*hidden argument*/NULL);
		((Cookie_t1826188460_StaticFields*)Cookie_t1826188460_il2cpp_TypeInfo_var->static_fields)->set__reservedCharsForName_10(L_0);
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)59));
		CharU5BU5D_t1328083999* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)44));
		((Cookie_t1826188460_StaticFields*)Cookie_t1826188460_il2cpp_TypeInfo_var->static_fields)->set__reservedCharsForValue_11(L_2);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t Cookie__ctor_m2639561341_MetadataUsageId;
extern "C"  void Cookie__ctor_m2639561341 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie__ctor_m2639561341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__comment_0(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__domain_3(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_2 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		__this->set__expires_4(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__name_6(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__path_7(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__port_8(L_5);
		__this->set__ports_9(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0)));
		DateTime_t693205669  L_6 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__timestamp_13(L_6);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__value_14(L_7);
		__this->set__version_15(0);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::.ctor(System.String,System.String)
extern "C"  void Cookie__ctor_m1121807237 (Cookie_t1826188460 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	{
		Cookie__ctor_m2639561341(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		Cookie_set_Name_m1639698133(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___value1;
		Cookie_set_Value_m1591147809(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_ExactDomain(System.Boolean)
extern "C"  void Cookie_set_ExactDomain_m2132388388 (Cookie_t1826188460 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CExactDomainU3Ek__BackingField_16(L_0);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Comment(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Comment_m2223812233_MetadataUsageId;
extern "C"  void Cookie_set_Comment_m2223812233 (Cookie_t1826188460 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Comment_m2223812233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	Cookie_t1826188460 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	Cookie_t1826188460 * G_B1_1 = NULL;
	{
		String_t* L_0 = ___value0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
		G_B2_1 = G_B1_1;
	}

IL_000e:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__comment_0(G_B2_0);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_CommentUri(System.Uri)
extern "C"  void Cookie_set_CommentUri_m522455678 (Cookie_t1826188460 * __this, Uri_t19570940 * ___value0, const MethodInfo* method)
{
	{
		Uri_t19570940 * L_0 = ___value0;
		__this->set__commentUri_1(L_0);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Discard(System.Boolean)
extern "C"  void Cookie_set_Discard_m367320081 (Cookie_t1826188460 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__discard_2(L_0);
		return;
	}
}
// System.String WebSocketSharp.Net.Cookie::get_Domain()
extern "C"  String_t* Cookie_get_Domain_m104047669 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__domain_3();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Domain(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Domain_m4209062518_MetadataUsageId;
extern "C"  void Cookie_set_Domain_m4209062518 (Cookie_t1826188460 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Domain_m4209062518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_1 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__domain_3(L_2);
		Cookie_set_ExactDomain_m2132388388(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0022:
	{
		String_t* L_3 = ___value0;
		__this->set__domain_3(L_3);
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_4, 0, /*hidden argument*/NULL);
		Cookie_set_ExactDomain_m2132388388(__this, (bool)((((int32_t)((((int32_t)L_5) == ((int32_t)((int32_t)46)))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.Net.Cookie::get_Expired()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_get_Expired_m1144720675_MetadataUsageId;
extern "C"  bool Cookie_get_Expired_m1144720675 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_get_Expired_m1144720675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		DateTime_t693205669  L_0 = __this->get__expires_4();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		bool L_2 = DateTime_op_Inequality_m1607380213(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		DateTime_t693205669  L_3 = __this->get__expires_4();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_4 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = DateTime_op_LessThanOrEqual_m2191641069(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 0;
	}

IL_0028:
	{
		return (bool)G_B3_0;
	}
}
// System.DateTime WebSocketSharp.Net.Cookie::get_Expires()
extern "C"  DateTime_t693205669  Cookie_get_Expires_m2850971449 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = __this->get__expires_4();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Expires(System.DateTime)
extern "C"  void Cookie_set_Expires_m50100848 (Cookie_t1826188460 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set__expires_4(L_0);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_HttpOnly(System.Boolean)
extern "C"  void Cookie_set_HttpOnly_m2334289127 (Cookie_t1826188460 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__httpOnly_5(L_0);
		return;
	}
}
// System.String WebSocketSharp.Net.Cookie::get_Name()
extern "C"  String_t* Cookie_get_Name_m298234216 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__name_6();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Name(System.String)
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieException_t780982235_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Name_m1639698133_MetadataUsageId;
extern "C"  void Cookie_set_Name_m1639698133 (Cookie_t1826188460 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Name_m1639698133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t1826188460_il2cpp_TypeInfo_var);
		bool L_1 = Cookie_canSetName_m3790830748(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = V_0;
		CookieException_t780982235 * L_3 = (CookieException_t780982235 *)il2cpp_codegen_object_new(CookieException_t780982235_il2cpp_TypeInfo_var);
		CookieException__ctor_m2644350728(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0014:
	{
		String_t* L_4 = ___value0;
		__this->set__name_6(L_4);
		return;
	}
}
// System.String WebSocketSharp.Net.Cookie::get_Path()
extern "C"  String_t* Cookie_get_Path_m3567104664 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__path_7();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Path(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_set_Path_m3813951051_MetadataUsageId;
extern "C"  void Cookie_set_Path_m3813951051 (Cookie_t1826188460 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Path_m3813951051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	Cookie_t1826188460 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	Cookie_t1826188460 * G_B1_1 = NULL;
	{
		String_t* L_0 = ___value0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
		G_B2_1 = G_B1_1;
	}

IL_000e:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__path_7(G_B2_0);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Port(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieException_t780982235_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3682658782;
extern Il2CppCodeGenString* _stringLiteral589905953;
extern const uint32_t Cookie_set_Port_m2069304487_MetadataUsageId;
extern "C"  void Cookie_set_Port_m2069304487 (Cookie_t1826188460 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Port_m2069304487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_1 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__port_8(L_2);
		__this->set__ports_9(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}

IL_0023:
	{
		String_t* L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_4 = Ext_IsEnclosedIn_m1922991632(NULL /*static, unused*/, L_3, ((int32_t)34), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003b;
		}
	}
	{
		CookieException_t780982235 * L_5 = (CookieException_t780982235 *)il2cpp_codegen_object_new(CookieException_t780982235_il2cpp_TypeInfo_var);
		CookieException__ctor_m2644350728(L_5, _stringLiteral3682658782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003b:
	{
		String_t* L_6 = ___value0;
		Int32U5BU5D_t3030399641** L_7 = __this->get_address_of__ports_9();
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t1826188460_il2cpp_TypeInfo_var);
		bool L_8 = Cookie_tryCreatePorts_m371608671(NULL /*static, unused*/, L_6, L_7, (&V_0), /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral589905953, L_9, /*hidden argument*/NULL);
		CookieException_t780982235 * L_11 = (CookieException_t780982235 *)il2cpp_codegen_object_new(CookieException_t780982235_il2cpp_TypeInfo_var);
		CookieException__ctor_m2644350728(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_005f:
	{
		String_t* L_12 = ___value0;
		__this->set__port_8(L_12);
		return;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Secure(System.Boolean)
extern "C"  void Cookie_set_Secure_m2990972016 (Cookie_t1826188460 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__secure_12(L_0);
		return;
	}
}
// System.String WebSocketSharp.Net.Cookie::get_Value()
extern "C"  String_t* Cookie_get_Value_m2532036198 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_14();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Value(System.String)
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieException_t780982235_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3943473354;
extern const uint32_t Cookie_set_Value_m1591147809_MetadataUsageId;
extern "C"  void Cookie_set_Value_m1591147809 (Cookie_t1826188460 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Value_m1591147809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Cookie_t1826188460 * G_B4_0 = NULL;
	Cookie_t1826188460 * G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	Cookie_t1826188460 * G_B5_1 = NULL;
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t1826188460_il2cpp_TypeInfo_var);
		bool L_1 = Cookie_canSetValue_m3205416658(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = V_0;
		CookieException_t780982235 * L_3 = (CookieException_t780982235 *)il2cpp_codegen_object_new(CookieException_t780982235_il2cpp_TypeInfo_var);
		CookieException__ctor_m2644350728(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0014:
	{
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		G_B3_0 = __this;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			G_B4_0 = __this;
			goto IL_0027;
		}
	}
	{
		String_t* L_6 = ___value0;
		G_B5_0 = L_6;
		G_B5_1 = G_B3_0;
		goto IL_002c;
	}

IL_0027:
	{
		G_B5_0 = _stringLiteral3943473354;
		G_B5_1 = G_B4_0;
	}

IL_002c:
	{
		NullCheck(G_B5_1);
		G_B5_1->set__value_14(G_B5_0);
		return;
	}
}
// System.Int32 WebSocketSharp.Net.Cookie::get_Version()
extern "C"  int32_t Cookie_get_Version_m2361584692 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__version_15();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.Cookie::set_Version(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral252777403;
extern const uint32_t Cookie_set_Version_m707117643_MetadataUsageId;
extern "C"  void Cookie_set_Version_m707117643 (Cookie_t1826188460 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_set_Version_m707117643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___value0;
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_001e;
		}
	}

IL_000e:
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_2, _stringLiteral1803325615, _stringLiteral252777403, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001e:
	{
		int32_t L_3 = ___value0;
		__this->set__version_15(L_3);
		return;
	}
}
// System.Boolean WebSocketSharp.Net.Cookie::canSetName(System.String,System.String&)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1230471066;
extern Il2CppCodeGenString* _stringLiteral4091021519;
extern const uint32_t Cookie_canSetName_m3790830748_MetadataUsageId;
extern "C"  bool Cookie_canSetName_m3790830748 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t** ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_canSetName_m3790830748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_1 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t** L_2 = ___message1;
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)_stringLiteral1230471066;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)_stringLiteral1230471066);
		return (bool)0;
	}

IL_0014:
	{
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m4230566705(L_3, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)36))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_5 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t1826188460_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_6 = ((Cookie_t1826188460_StaticFields*)Cookie_t1826188460_il2cpp_TypeInfo_var->static_fields)->get__reservedCharsForName_10();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_7 = Ext_Contains_m600248741(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}

IL_0032:
	{
		String_t** L_8 = ___message1;
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)_stringLiteral4091021519;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)_stringLiteral4091021519);
		return (bool)0;
	}

IL_003b:
	{
		String_t** L_9 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)L_10;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)L_10);
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.Net.Cookie::canSetValue(System.String,System.String&)
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1924498924;
extern Il2CppCodeGenString* _stringLiteral486560515;
extern const uint32_t Cookie_canSetValue_m3205416658_MetadataUsageId;
extern "C"  bool Cookie_canSetValue_m3205416658 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t** ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_canSetValue_m3205416658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t** L_1 = ___message1;
		*((Il2CppObject **)(L_1)) = (Il2CppObject *)_stringLiteral1924498924;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_1), (Il2CppObject *)_stringLiteral1924498924);
		return (bool)0;
	}

IL_000f:
	{
		String_t* L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t1826188460_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_3 = ((Cookie_t1826188460_StaticFields*)Cookie_t1826188460_il2cpp_TypeInfo_var->static_fields)->get__reservedCharsForValue_11();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_4 = Ext_Contains_m600248741(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_5 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_6 = Ext_IsEnclosedIn_m1922991632(NULL /*static, unused*/, L_5, ((int32_t)34), /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		String_t** L_7 = ___message1;
		*((Il2CppObject **)(L_7)) = (Il2CppObject *)_stringLiteral486560515;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_7), (Il2CppObject *)_stringLiteral486560515);
		return (bool)0;
	}

IL_0035:
	{
		String_t** L_8 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)L_9;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)L_9);
		return (bool)1;
	}
}
// System.Int32 WebSocketSharp.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Cookie_hash_m805479866 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___j1, int32_t ___k2, int32_t ___l3, int32_t ___m4, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = ___j1;
		int32_t L_2 = ___j1;
		int32_t L_3 = ___k2;
		int32_t L_4 = ___k2;
		int32_t L_5 = ___l3;
		int32_t L_6 = ___l3;
		int32_t L_7 = ___m4;
		int32_t L_8 = ___m4;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)13)))|(int32_t)((int32_t)((int32_t)L_2>>(int32_t)((int32_t)19)))))))^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)26)))|(int32_t)((int32_t)((int32_t)L_4>>(int32_t)6))))))^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5<<(int32_t)7))|(int32_t)((int32_t)((int32_t)L_6>>(int32_t)((int32_t)25)))))))^(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)20)))|(int32_t)((int32_t)((int32_t)L_8>>(int32_t)((int32_t)12)))))));
	}
}
// System.Boolean WebSocketSharp.Net.Cookie::tryCreatePorts(System.String,System.Int32[]&,System.String&)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_tryCreatePorts_m371608671_MetadataUsageId;
extern "C"  bool Cookie_tryCreatePorts_m371608671 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Int32U5BU5D_t3030399641** ___result1, String_t** ___parseError2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_tryCreatePorts_m371608671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	int32_t V_1 = 0;
	Int32U5BU5D_t3030399641* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)34));
		NullCheck(L_0);
		String_t* L_2 = String_Trim_m3982520224(L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3326265864(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		StringU5BU5D_t1642385972* L_5 = V_0;
		NullCheck(L_5);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))));
		int32_t L_6 = V_1;
		V_2 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_6));
		V_3 = 0;
		goto IL_007c;
	}

IL_0034:
	{
		Int32U5BU5D_t3030399641* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)((int32_t)-2147483648LL));
		StringU5BU5D_t1642385972* L_9 = V_0;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		String_t* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		String_t* L_13 = String_Trim_m2668767713(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		String_t* L_14 = V_4;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0057;
		}
	}
	{
		goto IL_0078;
	}

IL_0057:
	{
		String_t* L_16 = V_4;
		Int32U5BU5D_t3030399641* L_17 = V_2;
		int32_t L_18 = V_3;
		NullCheck(L_17);
		bool L_19 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_16, ((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0078;
		}
	}
	{
		Int32U5BU5D_t3030399641** L_20 = ___result1;
		*((Il2CppObject **)(L_20)) = (Il2CppObject *)((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_20), (Il2CppObject *)((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0)));
		String_t** L_21 = ___parseError2;
		String_t* L_22 = V_4;
		*((Il2CppObject **)(L_21)) = (Il2CppObject *)L_22;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_21), (Il2CppObject *)L_22);
		return (bool)0;
	}

IL_0078:
	{
		int32_t L_23 = V_3;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_24 = V_3;
		int32_t L_25 = V_1;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0034;
		}
	}
	{
		Int32U5BU5D_t3030399641** L_26 = ___result1;
		Int32U5BU5D_t3030399641* L_27 = V_2;
		*((Il2CppObject **)(L_26)) = (Il2CppObject *)L_27;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_26), (Il2CppObject *)L_27);
		String_t** L_28 = ___parseError2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_28)) = (Il2CppObject *)L_29;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_28), (Il2CppObject *)L_29);
		return (bool)1;
	}
}
// System.String WebSocketSharp.Net.Cookie::ToRequestString(System.Uri)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3829328834;
extern Il2CppCodeGenString* _stringLiteral481016548;
extern Il2CppCodeGenString* _stringLiteral3611852145;
extern Il2CppCodeGenString* _stringLiteral3038142920;
extern Il2CppCodeGenString* _stringLiteral3376039524;
extern Il2CppCodeGenString* _stringLiteral3943473354;
extern Il2CppCodeGenString* _stringLiteral2914234788;
extern Il2CppCodeGenString* _stringLiteral367589465;
extern const uint32_t Cookie_ToRequestString_m1768598564_MetadataUsageId;
extern "C"  String_t* Cookie_ToRequestString_m1768598564 (Cookie_t1826188460 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_ToRequestString_m1768598564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B12_0 = 0;
	{
		String_t* L_0 = __this->get__name_6();
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get__version_15();
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_4 = __this->get__name_6();
		String_t* L_5 = __this->get__value_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3829328834, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0038:
	{
		StringBuilder_t1221177846 * L_7 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_7, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_7;
		StringBuilder_t1221177846 * L_8 = V_0;
		int32_t L_9 = __this->get__version_15();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		String_t* L_12 = __this->get__name_6();
		String_t* L_13 = __this->get__value_14();
		NullCheck(L_8);
		StringBuilder_AppendFormat_m1666670800(L_8, _stringLiteral481016548, L_11, L_12, L_13, /*hidden argument*/NULL);
		String_t* L_14 = __this->get__path_7();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_15 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008a;
		}
	}
	{
		StringBuilder_t1221177846 * L_16 = V_0;
		String_t* L_17 = __this->get__path_7();
		NullCheck(L_16);
		StringBuilder_AppendFormat_m3265503696(L_16, _stringLiteral3611852145, L_17, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_008a:
	{
		Uri_t19570940 * L_18 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_19 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_18, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00ad;
		}
	}
	{
		StringBuilder_t1221177846 * L_20 = V_0;
		Uri_t19570940 * L_21 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_22 = Ext_GetAbsolutePath_m430442765(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		StringBuilder_AppendFormat_m3265503696(L_20, _stringLiteral3611852145, L_22, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_00ad:
	{
		StringBuilder_t1221177846 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m3636508479(L_23, _stringLiteral3038142920, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		Uri_t19570940 * L_24 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_25 = Uri_op_Equality_m110355127(NULL /*static, unused*/, L_24, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00d8;
		}
	}
	{
		Uri_t19570940 * L_26 = ___uri0;
		NullCheck(L_26);
		String_t* L_27 = Uri_get_Host_m395387191(L_26, /*hidden argument*/NULL);
		String_t* L_28 = __this->get__domain_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		G_B12_0 = ((int32_t)(L_29));
		goto IL_00d9;
	}

IL_00d8:
	{
		G_B12_0 = 1;
	}

IL_00d9:
	{
		V_1 = (bool)G_B12_0;
		bool L_30 = V_1;
		if (!L_30)
		{
			goto IL_0102;
		}
	}
	{
		String_t* L_31 = __this->get__domain_3();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_32 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_0102;
		}
	}
	{
		StringBuilder_t1221177846 * L_33 = V_0;
		String_t* L_34 = __this->get__domain_3();
		NullCheck(L_33);
		StringBuilder_AppendFormat_m3265503696(L_33, _stringLiteral3376039524, L_34, /*hidden argument*/NULL);
	}

IL_0102:
	{
		String_t* L_35 = __this->get__port_8();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_36 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_014a;
		}
	}
	{
		String_t* L_37 = __this->get__port_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_37, _stringLiteral3943473354, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0138;
		}
	}
	{
		StringBuilder_t1221177846 * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m3636508479(L_39, _stringLiteral2914234788, /*hidden argument*/NULL);
		goto IL_014a;
	}

IL_0138:
	{
		StringBuilder_t1221177846 * L_40 = V_0;
		String_t* L_41 = __this->get__port_8();
		NullCheck(L_40);
		StringBuilder_AppendFormat_m3265503696(L_40, _stringLiteral367589465, L_41, /*hidden argument*/NULL);
	}

IL_014a:
	{
		StringBuilder_t1221177846 * L_42 = V_0;
		NullCheck(L_42);
		String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_42);
		return L_43;
	}
}
// System.Boolean WebSocketSharp.Net.Cookie::Equals(System.Object)
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_Equals_m3609067006_MetadataUsageId;
extern "C"  bool Cookie_Equals_m3609067006 (Cookie_t1826188460 * __this, Il2CppObject * ___comparand0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_Equals_m3609067006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cookie_t1826188460 * V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___comparand0;
		V_0 = ((Cookie_t1826188460 *)IsInstSealed(L_0, Cookie_t1826188460_il2cpp_TypeInfo_var));
		Cookie_t1826188460 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_2 = __this->get__name_6();
		Cookie_t1826188460 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = Cookie_get_Name_m298234216(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = String_Equals_m1185277978(L_2, L_4, 3, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_6 = __this->get__value_14();
		Cookie_t1826188460 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = Cookie_get_Value_m2532036198(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_9 = String_Equals_m1185277978(L_6, L_8, 2, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_10 = __this->get__path_7();
		Cookie_t1826188460 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = Cookie_get_Path_m3567104664(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_13 = String_Equals_m1185277978(L_10, L_12, 2, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_14 = __this->get__domain_3();
		Cookie_t1826188460 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = Cookie_get_Domain_m104047669(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_17 = String_Equals_m1185277978(L_14, L_16, 3, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_18 = __this->get__version_15();
		Cookie_t1826188460 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = Cookie_get_Version_m2361584692(L_19, /*hidden argument*/NULL);
		G_B7_0 = ((((int32_t)L_18) == ((int32_t)L_20))? 1 : 0);
		goto IL_007a;
	}

IL_0079:
	{
		G_B7_0 = 0;
	}

IL_007a:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 WebSocketSharp.Net.Cookie::GetHashCode()
extern Il2CppClass* StringComparer_t1574862926_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern const uint32_t Cookie_GetHashCode_m400185106_MetadataUsageId;
extern "C"  int32_t Cookie_GetHashCode_m400185106 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cookie_GetHashCode_m400185106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1574862926_il2cpp_TypeInfo_var);
		StringComparer_t1574862926 * L_0 = StringComparer_get_InvariantCultureIgnoreCase_m1052445386(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get__name_6();
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(12 /* System.Int32 System.StringComparer::GetHashCode(System.String) */, L_0, L_1);
		String_t* L_3 = __this->get__value_14();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		String_t* L_5 = __this->get__path_7();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_5);
		StringComparer_t1574862926 * L_7 = StringComparer_get_InvariantCultureIgnoreCase_m1052445386(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = __this->get__domain_3();
		NullCheck(L_7);
		int32_t L_9 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(12 /* System.Int32 System.StringComparer::GetHashCode(System.String) */, L_7, L_8);
		int32_t L_10 = __this->get__version_15();
		IL2CPP_RUNTIME_CLASS_INIT(Cookie_t1826188460_il2cpp_TypeInfo_var);
		int32_t L_11 = Cookie_hash_m805479866(NULL /*static, unused*/, L_2, L_4, L_6, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String WebSocketSharp.Net.Cookie::ToString()
extern "C"  String_t* Cookie_ToString_m2617540906 (Cookie_t1826188460 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Cookie_ToRequestString_m1768598564(__this, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.CookieCollection::.ctor()
extern Il2CppClass* List_1_t1195309592_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2338834983_MethodInfo_var;
extern const uint32_t CookieCollection__ctor_m1652849323_MetadataUsageId;
extern "C"  void CookieCollection__ctor_m1652849323 (CookieCollection_t4248997468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection__ctor_m1652849323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t1195309592 * L_0 = (List_1_t1195309592 *)il2cpp_codegen_object_new(List_1_t1195309592_il2cpp_TypeInfo_var);
		List_1__ctor_m2338834983(L_0, /*hidden argument*/List_1__ctor_m2338834983_MethodInfo_var);
		__this->set__list_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::get_Sorted()
extern Il2CppClass* List_1_t1195309592_il2cpp_TypeInfo_var;
extern Il2CppClass* CookieCollection_t4248997468_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t3087927311_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4246732173_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2495479295_MethodInfo_var;
extern const MethodInfo* CookieCollection_compareCookieWithinSorted_m340466066_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m2916915280_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m3729238717_MethodInfo_var;
extern const uint32_t CookieCollection_get_Sorted_m1762804415_MetadataUsageId;
extern "C"  Il2CppObject* CookieCollection_get_Sorted_m1762804415 (CookieCollection_t4248997468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_get_Sorted_m1762804415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1195309592 * V_0 = NULL;
	List_1_t1195309592 * G_B3_0 = NULL;
	List_1_t1195309592 * G_B2_0 = NULL;
	{
		List_1_t1195309592 * L_0 = __this->get__list_0();
		List_1_t1195309592 * L_1 = (List_1_t1195309592 *)il2cpp_codegen_object_new(List_1_t1195309592_il2cpp_TypeInfo_var);
		List_1__ctor_m4246732173(L_1, L_0, /*hidden argument*/List_1__ctor_m4246732173_MethodInfo_var);
		V_0 = L_1;
		List_1_t1195309592 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2495479295(L_2, /*hidden argument*/List_1_get_Count_m2495479295_MethodInfo_var);
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		List_1_t1195309592 * L_4 = V_0;
		Comparison_1_t3087927311 * L_5 = ((CookieCollection_t4248997468_StaticFields*)CookieCollection_t4248997468_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_2();
		G_B2_0 = L_4;
		if (L_5)
		{
			G_B3_0 = L_4;
			goto IL_0031;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)CookieCollection_compareCookieWithinSorted_m340466066_MethodInfo_var);
		Comparison_1_t3087927311 * L_7 = (Comparison_1_t3087927311 *)il2cpp_codegen_object_new(Comparison_1_t3087927311_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m2916915280(L_7, NULL, L_6, /*hidden argument*/Comparison_1__ctor_m2916915280_MethodInfo_var);
		((CookieCollection_t4248997468_StaticFields*)CookieCollection_t4248997468_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_2(L_7);
		G_B3_0 = G_B2_0;
	}

IL_0031:
	{
		Comparison_1_t3087927311 * L_8 = ((CookieCollection_t4248997468_StaticFields*)CookieCollection_t4248997468_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_2();
		NullCheck(G_B3_0);
		List_1_Sort_m3729238717(G_B3_0, L_8, /*hidden argument*/List_1_Sort_m3729238717_MethodInfo_var);
	}

IL_003b:
	{
		List_1_t1195309592 * L_9 = V_0;
		return L_9;
	}
}
// System.Int32 WebSocketSharp.Net.CookieCollection::get_Count()
extern const MethodInfo* List_1_get_Count_m2495479295_MethodInfo_var;
extern const uint32_t CookieCollection_get_Count_m3590414075_MetadataUsageId;
extern "C"  int32_t CookieCollection_get_Count_m3590414075 (CookieCollection_t4248997468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_get_Count_m3590414075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1195309592 * L_0 = __this->get__list_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2495479295(L_0, /*hidden argument*/List_1_get_Count_m2495479295_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean WebSocketSharp.Net.CookieCollection::get_IsSynchronized()
extern "C"  bool CookieCollection_get_IsSynchronized_m265719460 (CookieCollection_t4248997468 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object WebSocketSharp.Net.CookieCollection::get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t CookieCollection_get_SyncRoot_m3288836602_MetadataUsageId;
extern "C"  Il2CppObject * CookieCollection_get_SyncRoot_m3288836602 (CookieCollection_t4248997468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_get_SyncRoot_m3288836602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get__sync_1();
		Il2CppObject * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0021;
		}
	}
	{
		List_1_t1195309592 * L_2 = __this->get__list_0();
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_2);
		Il2CppObject * L_4 = L_3;
		V_0 = L_4;
		__this->set__sync_1(L_4);
		Il2CppObject * L_5 = V_0;
		G_B2_0 = L_5;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Int32 WebSocketSharp.Net.CookieCollection::compareCookieWithinSorted(WebSocketSharp.Net.Cookie,WebSocketSharp.Net.Cookie)
extern "C"  int32_t CookieCollection_compareCookieWithinSorted_m340466066 (Il2CppObject * __this /* static, unused */, Cookie_t1826188460 * ___x0, Cookie_t1826188460 * ___y1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		V_0 = 0;
		Cookie_t1826188460 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = Cookie_get_Version_m2361584692(L_0, /*hidden argument*/NULL);
		Cookie_t1826188460 * L_2 = ___y1;
		NullCheck(L_2);
		int32_t L_3 = Cookie_get_Version_m2361584692(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ((int32_t)((int32_t)L_1-(int32_t)L_3));
		V_0 = L_4;
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_5 = V_0;
		G_B5_0 = L_5;
		goto IL_0051;
	}

IL_001c:
	{
		Cookie_t1826188460 * L_6 = ___x0;
		NullCheck(L_6);
		String_t* L_7 = Cookie_get_Name_m298234216(L_6, /*hidden argument*/NULL);
		Cookie_t1826188460 * L_8 = ___y1;
		NullCheck(L_8);
		String_t* L_9 = Cookie_get_Name_m298234216(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_10 = String_CompareTo_m3879609894(L_7, L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		V_0 = L_11;
		if (!L_11)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_12 = V_0;
		G_B5_0 = L_12;
		goto IL_0051;
	}

IL_003a:
	{
		Cookie_t1826188460 * L_13 = ___y1;
		NullCheck(L_13);
		String_t* L_14 = Cookie_get_Path_m3567104664(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		Cookie_t1826188460 * L_16 = ___x0;
		NullCheck(L_16);
		String_t* L_17 = Cookie_get_Path_m3567104664(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m1606060069(L_17, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)((int32_t)L_15-(int32_t)L_18));
	}

IL_0051:
	{
		return G_B5_0;
	}
}
// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseRequest(System.String)
extern Il2CppClass* CookieCollection_t4248997468_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3020041398;
extern Il2CppCodeGenString* _stringLiteral2616711437;
extern Il2CppCodeGenString* _stringLiteral3797768058;
extern Il2CppCodeGenString* _stringLiteral3423281085;
extern Il2CppCodeGenString* _stringLiteral3943473354;
extern const uint32_t CookieCollection_parseRequest_m4112886156_MetadataUsageId;
extern "C"  CookieCollection_t4248997468 * CookieCollection_parseRequest_m4112886156 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_parseRequest_m4112886156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CookieCollection_t4248997468 * V_0 = NULL;
	Cookie_t1826188460 * V_1 = NULL;
	int32_t V_2 = 0;
	StringU5BU5D_t1642385972* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	int32_t V_9 = 0;
	String_t* G_B17_0 = NULL;
	{
		CookieCollection_t4248997468 * L_0 = (CookieCollection_t4248997468 *)il2cpp_codegen_object_new(CookieCollection_t4248997468_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m1652849323(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (Cookie_t1826188460 *)NULL;
		V_2 = 0;
		String_t* L_1 = ___value0;
		StringU5BU5D_t1642385972* L_2 = CookieCollection_splitCookieHeaderValue_m3823806052(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_3 = L_2;
		V_4 = 0;
		goto IL_01b7;
	}

IL_0019:
	{
		StringU5BU5D_t1642385972* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		String_t* L_7 = String_Trim_m2668767713(L_6, /*hidden argument*/NULL);
		V_5 = L_7;
		String_t* L_8 = V_5;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1606060069(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_01b1;
	}

IL_0035:
	{
		String_t* L_10 = V_5;
		NullCheck(L_10);
		bool L_11 = String_StartsWith_m46695182(L_10, _stringLiteral3020041398, 3, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_12 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_13 = Ext_GetValue_m1342481707(NULL /*static, unused*/, L_12, ((int32_t)61), (bool)1, /*hidden argument*/NULL);
		int32_t L_14 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		goto IL_01b1;
	}

IL_005c:
	{
		String_t* L_15 = V_5;
		NullCheck(L_15);
		bool L_16 = String_StartsWith_m46695182(L_15, _stringLiteral2616711437, 3, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		Cookie_t1826188460 * L_17 = V_1;
		if (!L_17)
		{
			goto IL_0083;
		}
	}
	{
		Cookie_t1826188460 * L_18 = V_1;
		String_t* L_19 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_20 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_19, ((int32_t)61), /*hidden argument*/NULL);
		NullCheck(L_18);
		Cookie_set_Path_m3813951051(L_18, L_20, /*hidden argument*/NULL);
	}

IL_0083:
	{
		goto IL_01b1;
	}

IL_0088:
	{
		String_t* L_21 = V_5;
		NullCheck(L_21);
		bool L_22 = String_StartsWith_m46695182(L_21, _stringLiteral3797768058, 3, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b4;
		}
	}
	{
		Cookie_t1826188460 * L_23 = V_1;
		if (!L_23)
		{
			goto IL_00af;
		}
	}
	{
		Cookie_t1826188460 * L_24 = V_1;
		String_t* L_25 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_26 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_25, ((int32_t)61), /*hidden argument*/NULL);
		NullCheck(L_24);
		Cookie_set_Domain_m4209062518(L_24, L_26, /*hidden argument*/NULL);
	}

IL_00af:
	{
		goto IL_01b1;
	}

IL_00b4:
	{
		String_t* L_27 = V_5;
		NullCheck(L_27);
		bool L_28 = String_StartsWith_m46695182(L_27, _stringLiteral3423281085, 3, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0100;
		}
	}
	{
		String_t* L_29 = V_5;
		NullCheck(L_29);
		bool L_30 = String_Equals_m1185277978(L_29, _stringLiteral3423281085, 3, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e2;
		}
	}
	{
		G_B17_0 = _stringLiteral3943473354;
		goto IL_00eb;
	}

IL_00e2:
	{
		String_t* L_31 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_32 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_31, ((int32_t)61), /*hidden argument*/NULL);
		G_B17_0 = L_32;
	}

IL_00eb:
	{
		V_6 = G_B17_0;
		Cookie_t1826188460 * L_33 = V_1;
		if (!L_33)
		{
			goto IL_00fb;
		}
	}
	{
		Cookie_t1826188460 * L_34 = V_1;
		String_t* L_35 = V_6;
		NullCheck(L_34);
		Cookie_set_Port_m2069304487(L_34, L_35, /*hidden argument*/NULL);
	}

IL_00fb:
	{
		goto IL_01b1;
	}

IL_0100:
	{
		Cookie_t1826188460 * L_36 = V_1;
		if (!L_36)
		{
			goto IL_010d;
		}
	}
	{
		CookieCollection_t4248997468 * L_37 = V_0;
		Cookie_t1826188460 * L_38 = V_1;
		NullCheck(L_37);
		CookieCollection_Add_m557733692(L_37, L_38, /*hidden argument*/NULL);
	}

IL_010d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_8 = L_39;
		String_t* L_40 = V_5;
		NullCheck(L_40);
		int32_t L_41 = String_IndexOf_m2358239236(L_40, ((int32_t)61), /*hidden argument*/NULL);
		V_9 = L_41;
		int32_t L_42 = V_9;
		if ((!(((uint32_t)L_42) == ((uint32_t)(-1)))))
		{
			goto IL_0130;
		}
	}
	{
		String_t* L_43 = V_5;
		V_7 = L_43;
		goto IL_019a;
	}

IL_0130:
	{
		int32_t L_44 = V_9;
		String_t* L_45 = V_5;
		NullCheck(L_45);
		int32_t L_46 = String_get_Length_m1606060069(L_45, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)((int32_t)L_46-(int32_t)1))))))
		{
			goto IL_0161;
		}
	}
	{
		String_t* L_47 = V_5;
		int32_t L_48 = V_9;
		NullCheck(L_47);
		String_t* L_49 = String_Substring_m12482732(L_47, 0, L_48, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_50 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_49);
		String_t* L_51 = String_TrimEnd_m3153143011(L_49, L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		goto IL_019a;
	}

IL_0161:
	{
		String_t* L_52 = V_5;
		int32_t L_53 = V_9;
		NullCheck(L_52);
		String_t* L_54 = String_Substring_m12482732(L_52, 0, L_53, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_55 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_54);
		String_t* L_56 = String_TrimEnd_m3153143011(L_54, L_55, /*hidden argument*/NULL);
		V_7 = L_56;
		String_t* L_57 = V_5;
		int32_t L_58 = V_9;
		NullCheck(L_57);
		String_t* L_59 = String_Substring_m2032624251(L_57, ((int32_t)((int32_t)L_58+(int32_t)1)), /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_60 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_60);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_59);
		String_t* L_61 = String_TrimStart_m710830036(L_59, L_60, /*hidden argument*/NULL);
		V_8 = L_61;
	}

IL_019a:
	{
		String_t* L_62 = V_7;
		String_t* L_63 = V_8;
		Cookie_t1826188460 * L_64 = (Cookie_t1826188460 *)il2cpp_codegen_object_new(Cookie_t1826188460_il2cpp_TypeInfo_var);
		Cookie__ctor_m1121807237(L_64, L_62, L_63, /*hidden argument*/NULL);
		V_1 = L_64;
		int32_t L_65 = V_2;
		if (!L_65)
		{
			goto IL_01b1;
		}
	}
	{
		Cookie_t1826188460 * L_66 = V_1;
		int32_t L_67 = V_2;
		NullCheck(L_66);
		Cookie_set_Version_m707117643(L_66, L_67, /*hidden argument*/NULL);
	}

IL_01b1:
	{
		int32_t L_68 = V_4;
		V_4 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01b7:
	{
		int32_t L_69 = V_4;
		StringU5BU5D_t1642385972* L_70 = V_3;
		NullCheck(L_70);
		if ((((int32_t)L_69) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_70)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		Cookie_t1826188460 * L_71 = V_1;
		if (!L_71)
		{
			goto IL_01ce;
		}
	}
	{
		CookieCollection_t4248997468 * L_72 = V_0;
		Cookie_t1826188460 * L_73 = V_1;
		NullCheck(L_72);
		CookieCollection_Add_m557733692(L_72, L_73, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		CookieCollection_t4248997468 * L_74 = V_0;
		return L_74;
	}
}
// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseResponse(System.String)
extern Il2CppClass* CookieCollection_t4248997468_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern Il2CppCodeGenString* _stringLiteral236246406;
extern Il2CppCodeGenString* _stringLiteral1903226552;
extern Il2CppCodeGenString* _stringLiteral734619943;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral3618214548;
extern Il2CppCodeGenString* _stringLiteral2250554746;
extern Il2CppCodeGenString* _stringLiteral2375273309;
extern Il2CppCodeGenString* _stringLiteral3774104446;
extern Il2CppCodeGenString* _stringLiteral3430667965;
extern Il2CppCodeGenString* _stringLiteral3943473354;
extern Il2CppCodeGenString* _stringLiteral452620487;
extern Il2CppCodeGenString* _stringLiteral1333948088;
extern Il2CppCodeGenString* _stringLiteral1345499080;
extern Il2CppCodeGenString* _stringLiteral3640583895;
extern Il2CppCodeGenString* _stringLiteral3612033256;
extern const uint32_t CookieCollection_parseResponse_m3448933298_MetadataUsageId;
extern "C"  CookieCollection_t4248997468 * CookieCollection_parseResponse_m3448933298 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_parseResponse_m3448933298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CookieCollection_t4248997468 * V_0 = NULL;
	Cookie_t1826188460 * V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	StringBuilder_t1221177846 * V_5 = NULL;
	DateTime_t693205669  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	DateTime_t693205669  V_8;
	memset(&V_8, 0, sizeof(V_8));
	DateTime_t693205669  V_9;
	memset(&V_9, 0, sizeof(V_9));
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	int32_t V_13 = 0;
	String_t* G_B32_0 = NULL;
	{
		CookieCollection_t4248997468 * L_0 = (CookieCollection_t4248997468 *)il2cpp_codegen_object_new(CookieCollection_t4248997468_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m1652849323(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (Cookie_t1826188460 *)NULL;
		String_t* L_1 = ___value0;
		StringU5BU5D_t1642385972* L_2 = CookieCollection_splitCookieHeaderValue_m3823806052(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0378;
	}

IL_0016:
	{
		StringU5BU5D_t1642385972* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		String_t* L_7 = String_Trim_m2668767713(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		String_t* L_8 = V_4;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1606060069(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_0374;
	}

IL_0031:
	{
		String_t* L_10 = V_4;
		NullCheck(L_10);
		bool L_11 = String_StartsWith_m46695182(L_10, _stringLiteral3617362, 3, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		Cookie_t1826188460 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_005e;
		}
	}
	{
		Cookie_t1826188460 * L_13 = V_1;
		String_t* L_14 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_15 = Ext_GetValue_m1342481707(NULL /*static, unused*/, L_14, ((int32_t)61), (bool)1, /*hidden argument*/NULL);
		int32_t L_16 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		Cookie_set_Version_m707117643(L_13, L_16, /*hidden argument*/NULL);
	}

IL_005e:
	{
		goto IL_0374;
	}

IL_0063:
	{
		String_t* L_17 = V_4;
		NullCheck(L_17);
		bool L_18 = String_StartsWith_m46695182(L_17, _stringLiteral236246406, 3, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0114;
		}
	}
	{
		String_t* L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_20 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_19, ((int32_t)61), /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_21 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1456828835(L_21, L_20, ((int32_t)32), /*hidden argument*/NULL);
		V_5 = L_21;
		int32_t L_22 = V_3;
		StringU5BU5D_t1642385972* L_23 = V_2;
		NullCheck(L_23);
		if ((((int32_t)L_22) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))))-(int32_t)1)))))
		{
			goto IL_00ab;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_5;
		StringU5BU5D_t1642385972* L_25 = V_2;
		int32_t L_26 = V_3;
		int32_t L_27 = ((int32_t)((int32_t)L_26+(int32_t)1));
		V_3 = L_27;
		NullCheck(L_25);
		int32_t L_28 = L_27;
		String_t* L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		String_t* L_30 = String_Trim_m2668767713(L_29, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_AppendFormat_m3265503696(L_24, _stringLiteral1903226552, L_30, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		StringBuilder_t1221177846 * L_31 = V_5;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_31);
		StringU5BU5D_t1642385972* L_33 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral734619943);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral734619943);
		StringU5BU5D_t1642385972* L_34 = L_33;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral372029392);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral372029392);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_35 = CultureInfo_CreateSpecificCulture_m3401773697(NULL /*static, unused*/, _stringLiteral3618214548, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_36 = DateTime_TryParseExact_m1935997846(NULL /*static, unused*/, L_32, L_34, L_35, ((int32_t)80), (&V_6), /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_00e7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_37 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_37;
	}

IL_00e7:
	{
		Cookie_t1826188460 * L_38 = V_1;
		if (!L_38)
		{
			goto IL_010f;
		}
	}
	{
		Cookie_t1826188460 * L_39 = V_1;
		NullCheck(L_39);
		DateTime_t693205669  L_40 = Cookie_get_Expires_m2850971449(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_41 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		bool L_42 = DateTime_op_Equality_m1867073872(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_010f;
		}
	}
	{
		Cookie_t1826188460 * L_43 = V_1;
		DateTime_t693205669  L_44 = DateTime_ToLocalTime_m1957689902((&V_6), /*hidden argument*/NULL);
		NullCheck(L_43);
		Cookie_set_Expires_m50100848(L_43, L_44, /*hidden argument*/NULL);
	}

IL_010f:
	{
		goto IL_0374;
	}

IL_0114:
	{
		String_t* L_45 = V_4;
		NullCheck(L_45);
		bool L_46 = String_StartsWith_m46695182(L_45, _stringLiteral2250554746, 3, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_015d;
		}
	}
	{
		String_t* L_47 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_48 = Ext_GetValue_m1342481707(NULL /*static, unused*/, L_47, ((int32_t)61), (bool)1, /*hidden argument*/NULL);
		int32_t L_49 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		V_7 = L_49;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_50 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_50;
		int32_t L_51 = V_7;
		DateTime_t693205669  L_52 = DateTime_AddSeconds_m722082155((&V_9), (((double)((double)L_51))), /*hidden argument*/NULL);
		V_8 = L_52;
		Cookie_t1826188460 * L_53 = V_1;
		if (!L_53)
		{
			goto IL_0158;
		}
	}
	{
		Cookie_t1826188460 * L_54 = V_1;
		DateTime_t693205669  L_55 = V_8;
		NullCheck(L_54);
		Cookie_set_Expires_m50100848(L_54, L_55, /*hidden argument*/NULL);
	}

IL_0158:
	{
		goto IL_0374;
	}

IL_015d:
	{
		String_t* L_56 = V_4;
		NullCheck(L_56);
		bool L_57 = String_StartsWith_m46695182(L_56, _stringLiteral2375273309, 3, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0189;
		}
	}
	{
		Cookie_t1826188460 * L_58 = V_1;
		if (!L_58)
		{
			goto IL_0184;
		}
	}
	{
		Cookie_t1826188460 * L_59 = V_1;
		String_t* L_60 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_61 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_60, ((int32_t)61), /*hidden argument*/NULL);
		NullCheck(L_59);
		Cookie_set_Path_m3813951051(L_59, L_61, /*hidden argument*/NULL);
	}

IL_0184:
	{
		goto IL_0374;
	}

IL_0189:
	{
		String_t* L_62 = V_4;
		NullCheck(L_62);
		bool L_63 = String_StartsWith_m46695182(L_62, _stringLiteral3774104446, 3, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_01b5;
		}
	}
	{
		Cookie_t1826188460 * L_64 = V_1;
		if (!L_64)
		{
			goto IL_01b0;
		}
	}
	{
		Cookie_t1826188460 * L_65 = V_1;
		String_t* L_66 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_67 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_66, ((int32_t)61), /*hidden argument*/NULL);
		NullCheck(L_65);
		Cookie_set_Domain_m4209062518(L_65, L_67, /*hidden argument*/NULL);
	}

IL_01b0:
	{
		goto IL_0374;
	}

IL_01b5:
	{
		String_t* L_68 = V_4;
		NullCheck(L_68);
		bool L_69 = String_StartsWith_m46695182(L_68, _stringLiteral3430667965, 3, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0201;
		}
	}
	{
		String_t* L_70 = V_4;
		NullCheck(L_70);
		bool L_71 = String_Equals_m1185277978(L_70, _stringLiteral3430667965, 3, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_01e3;
		}
	}
	{
		G_B32_0 = _stringLiteral3943473354;
		goto IL_01ec;
	}

IL_01e3:
	{
		String_t* L_72 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_73 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_72, ((int32_t)61), /*hidden argument*/NULL);
		G_B32_0 = L_73;
	}

IL_01ec:
	{
		V_10 = G_B32_0;
		Cookie_t1826188460 * L_74 = V_1;
		if (!L_74)
		{
			goto IL_01fc;
		}
	}
	{
		Cookie_t1826188460 * L_75 = V_1;
		String_t* L_76 = V_10;
		NullCheck(L_75);
		Cookie_set_Port_m2069304487(L_75, L_76, /*hidden argument*/NULL);
	}

IL_01fc:
	{
		goto IL_0374;
	}

IL_0201:
	{
		String_t* L_77 = V_4;
		NullCheck(L_77);
		bool L_78 = String_StartsWith_m46695182(L_77, _stringLiteral452620487, 3, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_0232;
		}
	}
	{
		Cookie_t1826188460 * L_79 = V_1;
		if (!L_79)
		{
			goto IL_022d;
		}
	}
	{
		Cookie_t1826188460 * L_80 = V_1;
		String_t* L_81 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_82 = Ext_GetValue_m936838958(NULL /*static, unused*/, L_81, ((int32_t)61), /*hidden argument*/NULL);
		String_t* L_83 = Ext_UrlDecode_m482702685(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		NullCheck(L_80);
		Cookie_set_Comment_m2223812233(L_80, L_83, /*hidden argument*/NULL);
	}

IL_022d:
	{
		goto IL_0374;
	}

IL_0232:
	{
		String_t* L_84 = V_4;
		NullCheck(L_84);
		bool L_85 = String_StartsWith_m46695182(L_84, _stringLiteral1333948088, 3, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_0264;
		}
	}
	{
		Cookie_t1826188460 * L_86 = V_1;
		if (!L_86)
		{
			goto IL_025f;
		}
	}
	{
		Cookie_t1826188460 * L_87 = V_1;
		String_t* L_88 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_89 = Ext_GetValue_m1342481707(NULL /*static, unused*/, L_88, ((int32_t)61), (bool)1, /*hidden argument*/NULL);
		Uri_t19570940 * L_90 = Ext_ToUri_m1195258854(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		NullCheck(L_87);
		Cookie_set_CommentUri_m522455678(L_87, L_90, /*hidden argument*/NULL);
	}

IL_025f:
	{
		goto IL_0374;
	}

IL_0264:
	{
		String_t* L_91 = V_4;
		NullCheck(L_91);
		bool L_92 = String_StartsWith_m46695182(L_91, _stringLiteral1345499080, 3, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_0288;
		}
	}
	{
		Cookie_t1826188460 * L_93 = V_1;
		if (!L_93)
		{
			goto IL_0283;
		}
	}
	{
		Cookie_t1826188460 * L_94 = V_1;
		NullCheck(L_94);
		Cookie_set_Discard_m367320081(L_94, (bool)1, /*hidden argument*/NULL);
	}

IL_0283:
	{
		goto IL_0374;
	}

IL_0288:
	{
		String_t* L_95 = V_4;
		NullCheck(L_95);
		bool L_96 = String_StartsWith_m46695182(L_95, _stringLiteral3640583895, 3, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_02ac;
		}
	}
	{
		Cookie_t1826188460 * L_97 = V_1;
		if (!L_97)
		{
			goto IL_02a7;
		}
	}
	{
		Cookie_t1826188460 * L_98 = V_1;
		NullCheck(L_98);
		Cookie_set_Secure_m2990972016(L_98, (bool)1, /*hidden argument*/NULL);
	}

IL_02a7:
	{
		goto IL_0374;
	}

IL_02ac:
	{
		String_t* L_99 = V_4;
		NullCheck(L_99);
		bool L_100 = String_StartsWith_m46695182(L_99, _stringLiteral3612033256, 3, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_02d0;
		}
	}
	{
		Cookie_t1826188460 * L_101 = V_1;
		if (!L_101)
		{
			goto IL_02cb;
		}
	}
	{
		Cookie_t1826188460 * L_102 = V_1;
		NullCheck(L_102);
		Cookie_set_HttpOnly_m2334289127(L_102, (bool)1, /*hidden argument*/NULL);
	}

IL_02cb:
	{
		goto IL_0374;
	}

IL_02d0:
	{
		Cookie_t1826188460 * L_103 = V_1;
		if (!L_103)
		{
			goto IL_02dd;
		}
	}
	{
		CookieCollection_t4248997468 * L_104 = V_0;
		Cookie_t1826188460 * L_105 = V_1;
		NullCheck(L_104);
		CookieCollection_Add_m557733692(L_104, L_105, /*hidden argument*/NULL);
	}

IL_02dd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_106 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_12 = L_106;
		String_t* L_107 = V_4;
		NullCheck(L_107);
		int32_t L_108 = String_IndexOf_m2358239236(L_107, ((int32_t)61), /*hidden argument*/NULL);
		V_13 = L_108;
		int32_t L_109 = V_13;
		if ((!(((uint32_t)L_109) == ((uint32_t)(-1)))))
		{
			goto IL_0300;
		}
	}
	{
		String_t* L_110 = V_4;
		V_11 = L_110;
		goto IL_036a;
	}

IL_0300:
	{
		int32_t L_111 = V_13;
		String_t* L_112 = V_4;
		NullCheck(L_112);
		int32_t L_113 = String_get_Length_m1606060069(L_112, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_111) == ((uint32_t)((int32_t)((int32_t)L_113-(int32_t)1))))))
		{
			goto IL_0331;
		}
	}
	{
		String_t* L_114 = V_4;
		int32_t L_115 = V_13;
		NullCheck(L_114);
		String_t* L_116 = String_Substring_m12482732(L_114, 0, L_115, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_117 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_117);
		(L_117)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_116);
		String_t* L_118 = String_TrimEnd_m3153143011(L_116, L_117, /*hidden argument*/NULL);
		V_11 = L_118;
		goto IL_036a;
	}

IL_0331:
	{
		String_t* L_119 = V_4;
		int32_t L_120 = V_13;
		NullCheck(L_119);
		String_t* L_121 = String_Substring_m12482732(L_119, 0, L_120, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_122 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_122);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_121);
		String_t* L_123 = String_TrimEnd_m3153143011(L_121, L_122, /*hidden argument*/NULL);
		V_11 = L_123;
		String_t* L_124 = V_4;
		int32_t L_125 = V_13;
		NullCheck(L_124);
		String_t* L_126 = String_Substring_m2032624251(L_124, ((int32_t)((int32_t)L_125+(int32_t)1)), /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_127 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_127);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_126);
		String_t* L_128 = String_TrimStart_m710830036(L_126, L_127, /*hidden argument*/NULL);
		V_12 = L_128;
	}

IL_036a:
	{
		String_t* L_129 = V_11;
		String_t* L_130 = V_12;
		Cookie_t1826188460 * L_131 = (Cookie_t1826188460 *)il2cpp_codegen_object_new(Cookie_t1826188460_il2cpp_TypeInfo_var);
		Cookie__ctor_m1121807237(L_131, L_129, L_130, /*hidden argument*/NULL);
		V_1 = L_131;
	}

IL_0374:
	{
		int32_t L_132 = V_3;
		V_3 = ((int32_t)((int32_t)L_132+(int32_t)1));
	}

IL_0378:
	{
		int32_t L_133 = V_3;
		StringU5BU5D_t1642385972* L_134 = V_2;
		NullCheck(L_134);
		if ((((int32_t)L_133) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_134)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		Cookie_t1826188460 * L_135 = V_1;
		if (!L_135)
		{
			goto IL_038e;
		}
	}
	{
		CookieCollection_t4248997468 * L_136 = V_0;
		Cookie_t1826188460 * L_137 = V_1;
		NullCheck(L_136);
		CookieCollection_Add_m557733692(L_136, L_137, /*hidden argument*/NULL);
	}

IL_038e:
	{
		CookieCollection_t4248997468 * L_138 = V_0;
		return L_138;
	}
}
// System.Int32 WebSocketSharp.Net.CookieCollection::searchCookie(WebSocketSharp.Net.Cookie)
extern const MethodInfo* List_1_get_Count_m2495479295_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3751875586_MethodInfo_var;
extern const uint32_t CookieCollection_searchCookie_m2522887713_MetadataUsageId;
extern "C"  int32_t CookieCollection_searchCookie_m2522887713 (CookieCollection_t4248997468 * __this, Cookie_t1826188460 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_searchCookie_m2522887713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Cookie_t1826188460 * V_5 = NULL;
	{
		Cookie_t1826188460 * L_0 = ___cookie0;
		NullCheck(L_0);
		String_t* L_1 = Cookie_get_Name_m298234216(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Cookie_t1826188460 * L_2 = ___cookie0;
		NullCheck(L_2);
		String_t* L_3 = Cookie_get_Path_m3567104664(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Cookie_t1826188460 * L_4 = ___cookie0;
		NullCheck(L_4);
		String_t* L_5 = Cookie_get_Domain_m104047669(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Cookie_t1826188460 * L_6 = ___cookie0;
		NullCheck(L_6);
		int32_t L_7 = Cookie_get_Version_m2361584692(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		List_1_t1195309592 * L_8 = __this->get__list_0();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m2495479295(L_8, /*hidden argument*/List_1_get_Count_m2495479295_MethodInfo_var);
		V_4 = ((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_008e;
	}

IL_0030:
	{
		List_1_t1195309592 * L_10 = __this->get__list_0();
		int32_t L_11 = V_4;
		NullCheck(L_10);
		Cookie_t1826188460 * L_12 = List_1_get_Item_m3751875586(L_10, L_11, /*hidden argument*/List_1_get_Item_m3751875586_MethodInfo_var);
		V_5 = L_12;
		Cookie_t1826188460 * L_13 = V_5;
		NullCheck(L_13);
		String_t* L_14 = Cookie_get_Name_m298234216(L_13, /*hidden argument*/NULL);
		String_t* L_15 = V_0;
		NullCheck(L_14);
		bool L_16 = String_Equals_m1185277978(L_14, L_15, 3, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		Cookie_t1826188460 * L_17 = V_5;
		NullCheck(L_17);
		String_t* L_18 = Cookie_get_Path_m3567104664(L_17, /*hidden argument*/NULL);
		String_t* L_19 = V_1;
		NullCheck(L_18);
		bool L_20 = String_Equals_m1185277978(L_18, L_19, 2, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0088;
		}
	}
	{
		Cookie_t1826188460 * L_21 = V_5;
		NullCheck(L_21);
		String_t* L_22 = Cookie_get_Domain_m104047669(L_21, /*hidden argument*/NULL);
		String_t* L_23 = V_2;
		NullCheck(L_22);
		bool L_24 = String_Equals_m1185277978(L_22, L_23, 3, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0088;
		}
	}
	{
		Cookie_t1826188460 * L_25 = V_5;
		NullCheck(L_25);
		int32_t L_26 = Cookie_get_Version_m2361584692(L_25, /*hidden argument*/NULL);
		int32_t L_27 = V_3;
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_28 = V_4;
		return L_28;
	}

IL_0088:
	{
		int32_t L_29 = V_4;
		V_4 = ((int32_t)((int32_t)L_29-(int32_t)1));
	}

IL_008e:
	{
		int32_t L_30 = V_4;
		if ((((int32_t)L_30) >= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		return (-1);
	}
}
// System.String[] WebSocketSharp.Net.CookieCollection::splitCookieHeaderValue(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3800815465_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m948919263_MethodInfo_var;
extern const uint32_t CookieCollection_splitCookieHeaderValue_m3823806052_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* CookieCollection_splitCookieHeaderValue_m3823806052 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_splitCookieHeaderValue_m3823806052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		CharU5BU5D_t1328083999* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)59));
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Ext_SplitHeaderValue_m3588285122(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		List_1_t1398341365 * L_4 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3800815465(L_4, L_3, /*hidden argument*/List_1__ctor_m3800815465_MethodInfo_var);
		NullCheck(L_4);
		StringU5BU5D_t1642385972* L_5 = List_1_ToArray_m948919263(L_4, /*hidden argument*/List_1_ToArray_m948919263_MethodInfo_var);
		return L_5;
	}
}
// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::Parse(System.String,System.Boolean)
extern "C"  CookieCollection_t4248997468 * CookieCollection_Parse_m2687394286 (Il2CppObject * __this /* static, unused */, String_t* ___value0, bool ___response1, const MethodInfo* method)
{
	CookieCollection_t4248997468 * G_B3_0 = NULL;
	{
		bool L_0 = ___response1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___value0;
		CookieCollection_t4248997468 * L_2 = CookieCollection_parseResponse_m3448933298(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		String_t* L_3 = ___value0;
		CookieCollection_t4248997468 * L_4 = CookieCollection_parseRequest_m4112886156(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.Cookie)
extern const MethodInfo* List_1_Add_m68273547_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m73518115_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m774823889_MethodInfo_var;
extern const uint32_t CookieCollection_SetOrRemove_m3535463430_MetadataUsageId;
extern "C"  void CookieCollection_SetOrRemove_m3535463430 (CookieCollection_t4248997468 * __this, Cookie_t1826188460 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_SetOrRemove_m3535463430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Cookie_t1826188460 * L_0 = ___cookie0;
		int32_t L_1 = CookieCollection_searchCookie_m2522887713(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0027;
		}
	}
	{
		Cookie_t1826188460 * L_3 = ___cookie0;
		NullCheck(L_3);
		bool L_4 = Cookie_get_Expired_m1144720675(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		List_1_t1195309592 * L_5 = __this->get__list_0();
		Cookie_t1826188460 * L_6 = ___cookie0;
		NullCheck(L_5);
		List_1_Add_m68273547(L_5, L_6, /*hidden argument*/List_1_Add_m68273547_MethodInfo_var);
	}

IL_0026:
	{
		return;
	}

IL_0027:
	{
		Cookie_t1826188460 * L_7 = ___cookie0;
		NullCheck(L_7);
		bool L_8 = Cookie_get_Expired_m1144720675(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0040;
		}
	}
	{
		List_1_t1195309592 * L_9 = __this->get__list_0();
		int32_t L_10 = V_0;
		Cookie_t1826188460 * L_11 = ___cookie0;
		NullCheck(L_9);
		List_1_set_Item_m73518115(L_9, L_10, L_11, /*hidden argument*/List_1_set_Item_m73518115_MethodInfo_var);
		return;
	}

IL_0040:
	{
		List_1_t1195309592 * L_12 = __this->get__list_0();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		List_1_RemoveAt_m774823889(L_12, L_13, /*hidden argument*/List_1_RemoveAt_m774823889_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.CookieCollection)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t1826188460_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t CookieCollection_SetOrRemove_m69867026_MetadataUsageId;
extern "C"  void CookieCollection_SetOrRemove_m69867026 (CookieCollection_t4248997468 * __this, CookieCollection_t4248997468 * ___cookies0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_SetOrRemove_m69867026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cookie_t1826188460 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CookieCollection_t4248997468 * L_0 = ___cookies0;
		NullCheck(L_0);
		Il2CppObject * L_1 = CookieCollection_GetEnumerator_m72020375(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001f;
		}

IL_000c:
		{
			Il2CppObject * L_2 = V_1;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Cookie_t1826188460 *)CastclassSealed(L_3, Cookie_t1826188460_il2cpp_TypeInfo_var));
			Cookie_t1826188460 * L_4 = V_0;
			CookieCollection_SetOrRemove_m3535463430(__this, L_4, /*hidden argument*/NULL);
		}

IL_001f:
		{
			Il2CppObject * L_5 = V_1;
			NullCheck(L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_002a:
		{
			IL2CPP_LEAVE(0x43, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_7 = V_1;
			Il2CppObject * L_8 = ((Il2CppObject *)IsInst(L_7, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_2 = L_8;
			if (!L_8)
			{
				goto IL_0042;
			}
		}

IL_003c:
		{
			Il2CppObject * L_9 = V_2;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(47)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieCollection::Add(WebSocketSharp.Net.Cookie)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m68273547_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m73518115_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2167626068;
extern const uint32_t CookieCollection_Add_m557733692_MetadataUsageId;
extern "C"  void CookieCollection_Add_m557733692 (CookieCollection_t4248997468 * __this, Cookie_t1826188460 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_Add_m557733692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Cookie_t1826188460 * L_0 = ___cookie0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2167626068, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Cookie_t1826188460 * L_2 = ___cookie0;
		int32_t L_3 = CookieCollection_searchCookie_m2522887713(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1195309592 * L_5 = __this->get__list_0();
		Cookie_t1826188460 * L_6 = ___cookie0;
		NullCheck(L_5);
		List_1_Add_m68273547(L_5, L_6, /*hidden argument*/List_1_Add_m68273547_MethodInfo_var);
		return;
	}

IL_002d:
	{
		List_1_t1195309592 * L_7 = __this->get__list_0();
		int32_t L_8 = V_0;
		Cookie_t1826188460 * L_9 = ___cookie0;
		NullCheck(L_7);
		List_1_set_Item_m73518115(L_7, L_8, L_9, /*hidden argument*/List_1_set_Item_m73518115_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieCollection::CopyTo(System.Array,System.Int32)
extern const Il2CppType* Cookie_t1826188460_0_0_0_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2495479295_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral1126105942;
extern Il2CppCodeGenString* _stringLiteral1927859098;
extern Il2CppCodeGenString* _stringLiteral1020733225;
extern Il2CppCodeGenString* _stringLiteral697385800;
extern const uint32_t CookieCollection_CopyTo_m1035559898_MetadataUsageId;
extern "C"  void CookieCollection_CopyTo_m1035559898 (CookieCollection_t4248997468 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_CopyTo_m1035559898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppArray * L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1460639766, _stringLiteral1126105942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		Il2CppArray * L_4 = ___array0;
		NullCheck(L_4);
		int32_t L_5 = Array_get_Rank_m3837250695(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)1)))
		{
			goto IL_0044;
		}
	}
	{
		ArgumentException_t3259014390 * L_6 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_6, _stringLiteral1927859098, _stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0044:
	{
		Il2CppArray * L_7 = ___array0;
		NullCheck(L_7);
		int32_t L_8 = Array_get_Length_m1498215565(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ___index1;
		List_1_t1195309592 * L_10 = __this->get__list_0();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m2495479295(L_10, /*hidden argument*/List_1_get_Count_m2495479295_MethodInfo_var);
		if ((((int32_t)((int32_t)((int32_t)L_8-(int32_t)L_9))) >= ((int32_t)L_11)))
		{
			goto IL_0067;
		}
	}
	{
		ArgumentException_t3259014390 * L_12 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_12, _stringLiteral1020733225, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0067:
	{
		Il2CppArray * L_13 = ___array0;
		NullCheck(L_13);
		Type_t * L_14 = Object_GetType_m191970594(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Type_t * L_15 = VirtFuncInvoker0< Type_t * >::Invoke(42 /* System.Type System.Type::GetElementType() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Cookie_t1826188460_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		bool L_17 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_15, L_16);
		if (L_17)
		{
			goto IL_0091;
		}
	}
	{
		InvalidCastException_t3625212209 * L_18 = (InvalidCastException_t3625212209 *)il2cpp_codegen_object_new(InvalidCastException_t3625212209_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m2960334316(L_18, _stringLiteral697385800, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0091:
	{
		List_1_t1195309592 * L_19 = __this->get__list_0();
		Il2CppArray * L_20 = ___array0;
		int32_t L_21 = ___index1;
		NullCheck(L_19);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, L_19, L_20, L_21);
		return;
	}
}
// System.Collections.IEnumerator WebSocketSharp.Net.CookieCollection::GetEnumerator()
extern Il2CppClass* Enumerator_t730039266_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3500890186_MethodInfo_var;
extern const uint32_t CookieCollection_GetEnumerator_m72020375_MetadataUsageId;
extern "C"  Il2CppObject * CookieCollection_GetEnumerator_m72020375 (CookieCollection_t4248997468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CookieCollection_GetEnumerator_m72020375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1195309592 * L_0 = __this->get__list_0();
		NullCheck(L_0);
		Enumerator_t730039266  L_1 = List_1_GetEnumerator_m3500890186(L_0, /*hidden argument*/List_1_GetEnumerator_m3500890186_MethodInfo_var);
		Enumerator_t730039266  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t730039266_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.Void WebSocketSharp.Net.CookieException::.ctor(System.String)
extern "C"  void CookieException__ctor_m2644350728 (CookieException_t780982235 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		FormatException__ctor_m1466217969(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CookieException__ctor_m881085793 (CookieException_t780982235 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___serializationInfo0;
		StreamingContext_t1417235061  L_1 = ___streamingContext1;
		FormatException__ctor_m3740644286(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieException::.ctor()
extern "C"  void CookieException__ctor_m1857836918 (CookieException_t780982235 * __this, const MethodInfo* method)
{
	{
		FormatException__ctor_m3521145315(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CookieException_GetObjectData_m2164715846 (CookieException_t780982235 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___serializationInfo0;
		StreamingContext_t1417235061  L_1 = ___streamingContext1;
		Exception_GetObjectData_m2653827630(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m1046840253 (CookieException_t780982235 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___serializationInfo0;
		StreamingContext_t1417235061  L_1 = ___streamingContext1;
		Exception_GetObjectData_m2653827630(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.HttpHeaderInfo::.ctor(System.String,WebSocketSharp.Net.HttpHeaderType)
extern "C"  void HttpHeaderInfo__ctor_m3133389831 (HttpHeaderInfo_t2096319561 * __this, String_t* ___name0, int32_t ___type1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set__name_0(L_0);
		int32_t L_1 = ___type1;
		__this->set__type_1(L_1);
		return;
	}
}
// System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInRequest()
extern "C"  bool HttpHeaderInfo_get_IsMultiValueInRequest_m231326667 (HttpHeaderInfo_t2096319561 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__type_1();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)((int32_t)16)))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInResponse()
extern "C"  bool HttpHeaderInfo_get_IsMultiValueInResponse_m1203443673 (HttpHeaderInfo_t2096319561 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__type_1();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)32)))) == ((int32_t)((int32_t)32)))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsRequest()
extern "C"  bool HttpHeaderInfo_get_IsRequest_m3670607568 (HttpHeaderInfo_t2096319561 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__type_1();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsResponse()
extern "C"  bool HttpHeaderInfo_get_IsResponse_m4072830454 (HttpHeaderInfo_t2096319561 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__type_1();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)2))? 1 : 0);
	}
}
// System.String WebSocketSharp.Net.HttpHeaderInfo::get_Name()
extern "C"  String_t* HttpHeaderInfo_get_Name_m1154984627 (HttpHeaderInfo_t2096319561 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__name_0();
		return L_0;
	}
}
// System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsMultiValue(System.Boolean)
extern "C"  bool HttpHeaderInfo_IsMultiValue_m1246610623 (HttpHeaderInfo_t2096319561 * __this, bool ___response0, const MethodInfo* method)
{
	bool G_B4_0 = false;
	bool G_B8_0 = false;
	{
		int32_t L_0 = __this->get__type_1();
		if ((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((uint32_t)8))))
		{
			goto IL_002a;
		}
	}
	{
		bool L_1 = ___response0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = HttpHeaderInfo_get_IsResponse_m4072830454(__this, /*hidden argument*/NULL);
		G_B4_0 = L_2;
		goto IL_0025;
	}

IL_001f:
	{
		bool L_3 = HttpHeaderInfo_get_IsRequest_m3670607568(__this, /*hidden argument*/NULL);
		G_B4_0 = L_3;
	}

IL_0025:
	{
		G_B8_0 = G_B4_0;
		goto IL_0041;
	}

IL_002a:
	{
		bool L_4 = ___response0;
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		bool L_5 = HttpHeaderInfo_get_IsMultiValueInResponse_m1203443673(__this, /*hidden argument*/NULL);
		G_B8_0 = L_5;
		goto IL_0041;
	}

IL_003b:
	{
		bool L_6 = HttpHeaderInfo_get_IsMultiValueInRequest_m231326667(__this, /*hidden argument*/NULL);
		G_B8_0 = L_6;
	}

IL_0041:
	{
		return G_B8_0;
	}
}
// System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsRestricted(System.Boolean)
extern "C"  bool HttpHeaderInfo_IsRestricted_m2648242912 (HttpHeaderInfo_t2096319561 * __this, bool ___response0, const MethodInfo* method)
{
	bool G_B4_0 = false;
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = __this->get__type_1();
		if ((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((uint32_t)4))))
		{
			goto IL_002a;
		}
	}
	{
		bool L_1 = ___response0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = HttpHeaderInfo_get_IsResponse_m4072830454(__this, /*hidden argument*/NULL);
		G_B4_0 = L_2;
		goto IL_0025;
	}

IL_001f:
	{
		bool L_3 = HttpHeaderInfo_get_IsRequest_m3670607568(__this, /*hidden argument*/NULL);
		G_B4_0 = L_3;
	}

IL_0025:
	{
		G_B6_0 = ((int32_t)(G_B4_0));
		goto IL_002b;
	}

IL_002a:
	{
		G_B6_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 WebSocketSharp.Net.HttpUtility::getChar(System.String,System.Int32,System.Int32)
extern Il2CppClass* HttpUtility_t3363705102_il2cpp_TypeInfo_var;
extern const uint32_t HttpUtility_getChar_m2917879435_MetadataUsageId;
extern "C"  int32_t HttpUtility_getChar_m2917879435 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpUtility_getChar_m2917879435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___length2;
		int32_t L_1 = ___offset1;
		V_1 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___offset1;
		V_2 = L_2;
		goto IL_003d;
	}

IL_000d:
	{
		String_t* L_3 = ___s0;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		Il2CppChar L_6 = V_3;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)127))))
		{
			goto IL_001f;
		}
	}
	{
		return (-1);
	}

IL_001f:
	{
		Il2CppChar L_7 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		int32_t L_8 = HttpUtility_getInt_m135708171(NULL /*static, unused*/, (((int32_t)((uint8_t)L_7))), /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = V_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		return (-1);
	}

IL_0032:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_4;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_10<<(int32_t)4))+(int32_t)L_11));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_15 = V_0;
		return L_15;
	}
}
// System.Int32 WebSocketSharp.Net.HttpUtility::getInt(System.Byte)
extern "C"  int32_t HttpUtility_getInt_m135708171 (Il2CppObject * __this /* static, unused */, uint8_t ___b0, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	int32_t G_B10_0 = 0;
	{
		uint8_t L_0 = ___b0;
		V_0 = (((int32_t)((uint16_t)L_0)));
		Il2CppChar L_1 = V_0;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)48))))
		{
			goto IL_001c;
		}
	}
	{
		Il2CppChar L_2 = V_0;
		if ((((int32_t)L_2) > ((int32_t)((int32_t)57))))
		{
			goto IL_001c;
		}
	}
	{
		Il2CppChar L_3 = V_0;
		G_B10_0 = ((int32_t)((int32_t)L_3-(int32_t)((int32_t)48)));
		goto IL_0055;
	}

IL_001c:
	{
		Il2CppChar L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)97))))
		{
			goto IL_0038;
		}
	}
	{
		Il2CppChar L_5 = V_0;
		if ((((int32_t)L_5) > ((int32_t)((int32_t)102))))
		{
			goto IL_0038;
		}
	}
	{
		Il2CppChar L_6 = V_0;
		G_B10_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)((int32_t)97)))+(int32_t)((int32_t)10)));
		goto IL_0055;
	}

IL_0038:
	{
		Il2CppChar L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)65))))
		{
			goto IL_0054;
		}
	}
	{
		Il2CppChar L_8 = V_0;
		if ((((int32_t)L_8) > ((int32_t)((int32_t)70))))
		{
			goto IL_0054;
		}
	}
	{
		Il2CppChar L_9 = V_0;
		G_B10_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9-(int32_t)((int32_t)65)))+(int32_t)((int32_t)10)));
		goto IL_0055;
	}

IL_0054:
	{
		G_B10_0 = (-1);
	}

IL_0055:
	{
		return G_B10_0;
	}
}
// System.Void WebSocketSharp.Net.HttpUtility::writeCharBytes(System.Char,System.Collections.IList,System.Text.Encoding)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t HttpUtility_writeCharBytes_m3800210614_MetadataUsageId;
extern "C"  void HttpUtility_writeCharBytes_m3800210614 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, Il2CppObject * ___buffer1, Encoding_t663144255 * ___encoding2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpUtility_writeCharBytes_m3800210614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppChar L_0 = ___c0;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0042;
		}
	}
	{
		Encoding_t663144255 * L_1 = ___encoding2;
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppChar L_3 = ___c0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_3);
		NullCheck(L_1);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, CharU5BU5D_t1328083999* >::Invoke(12 /* System.Byte[] System.Text.Encoding::GetBytes(System.Char[]) */, L_1, L_2);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0038;
	}

IL_0023:
	{
		ByteU5BU5D_t3397334013* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = L_8;
		Il2CppObject * L_9 = ___buffer1;
		uint8_t L_10 = V_0;
		uint8_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_9, L_12);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_14 = V_2;
		ByteU5BU5D_t3397334013* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}

IL_0042:
	{
		Il2CppObject * L_16 = ___buffer1;
		Il2CppChar L_17 = ___c0;
		uint8_t L_18 = ((uint8_t)(((int32_t)((uint8_t)L_17))));
		Il2CppObject * L_19 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_16, L_19);
		return;
	}
}
// System.Text.Encoding WebSocketSharp.Net.HttpUtility::GetEncoding(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651008258;
extern const uint32_t HttpUtility_GetEncoding_m792195308_MetadataUsageId;
extern "C"  Encoding_t663144255 * HttpUtility_GetEncoding_m792195308 (Il2CppObject * __this /* static, unused */, String_t* ___contentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpUtility_GetEncoding_m792195308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	{
		String_t* L_0 = ___contentType0;
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)59));
		NullCheck(L_0);
		StringU5BU5D_t1642385972* L_2 = String_Split_m3326265864(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1642385972* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_004d;
	}

IL_001b:
	{
		StringU5BU5D_t1642385972* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		String_t* L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = String_Trim_m2668767713(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		String_t* L_10 = V_4;
		NullCheck(L_10);
		bool L_11 = String_StartsWith_m46695182(L_10, _stringLiteral3651008258, 5, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_12 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_13 = Ext_GetValue_m1342481707(NULL /*static, unused*/, L_12, ((int32_t)61), (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_14 = Encoding_GetEncoding_m2475966878(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0049:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_16 = V_3;
		StringU5BU5D_t1642385972* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		return (Encoding_t663144255 *)NULL;
	}
}
// System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpUtility_t3363705102_il2cpp_TypeInfo_var;
extern const uint32_t HttpUtility_UrlDecode_m2084203637_MetadataUsageId;
extern "C"  String_t* HttpUtility_UrlDecode_m2084203637 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpUtility_UrlDecode_m2084203637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_1 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		String_t* L_2 = HttpUtility_UrlDecode_m834738660(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3052225568_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpUtility_t3363705102_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3502187290_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m279205908_MethodInfo_var;
extern const uint32_t HttpUtility_UrlDecode_m834738660_MetadataUsageId;
extern "C"  String_t* HttpUtility_UrlDecode_m834738660 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t663144255 * ___encoding1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpUtility_UrlDecode_m834738660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3052225568 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	int32_t V_4 = 0;
	{
		String_t* L_0 = ___s0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_3 = ___s0;
		CharU5BU5D_t1328083999* L_4 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)37));
		CharU5BU5D_t1328083999* L_5 = L_4;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)43));
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_6 = Ext_Contains_m600248741(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002e;
		}
	}

IL_002c:
	{
		String_t* L_7 = ___s0;
		return L_7;
	}

IL_002e:
	{
		Encoding_t663144255 * L_8 = ___encoding1;
		if (L_8)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_9 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		___encoding1 = L_9;
	}

IL_003b:
	{
		List_1_t3052225568 * L_10 = (List_1_t3052225568 *)il2cpp_codegen_object_new(List_1_t3052225568_il2cpp_TypeInfo_var);
		List_1__ctor_m3502187290(L_10, /*hidden argument*/List_1__ctor_m3502187290_MethodInfo_var);
		V_0 = L_10;
		String_t* L_11 = ___s0;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m1606060069(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		V_2 = 0;
		goto IL_011c;
	}

IL_004f:
	{
		String_t* L_13 = ___s0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		Il2CppChar L_15 = String_get_Chars_m4230566705(L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		Il2CppChar L_16 = V_3;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)((int32_t)((int32_t)L_17+(int32_t)2))) >= ((int32_t)L_18)))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_19 = ___s0;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m4230566705(L_19, ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)((int32_t)37))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_22 = ___s0;
		int32_t L_23 = V_2;
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m4230566705(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_00c6;
		}
	}
	{
		int32_t L_25 = V_2;
		int32_t L_26 = V_1;
		if ((((int32_t)((int32_t)((int32_t)L_25+(int32_t)5))) >= ((int32_t)L_26)))
		{
			goto IL_00c6;
		}
	}
	{
		String_t* L_27 = ___s0;
		int32_t L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		int32_t L_29 = HttpUtility_getChar_m2917879435(NULL /*static, unused*/, L_27, ((int32_t)((int32_t)L_28+(int32_t)2)), 4, /*hidden argument*/NULL);
		V_4 = L_29;
		int32_t L_30 = V_4;
		if ((((int32_t)L_30) == ((int32_t)(-1))))
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_31 = V_4;
		List_1_t3052225568 * L_32 = V_0;
		Encoding_t663144255 * L_33 = ___encoding1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		HttpUtility_writeCharBytes_m3800210614(NULL /*static, unused*/, (((int32_t)((uint16_t)L_31))), L_32, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_2;
		V_2 = ((int32_t)((int32_t)L_34+(int32_t)5));
		goto IL_00c1;
	}

IL_00b8:
	{
		List_1_t3052225568 * L_35 = V_0;
		Encoding_t663144255 * L_36 = ___encoding1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		HttpUtility_writeCharBytes_m3800210614(NULL /*static, unused*/, ((int32_t)37), L_35, L_36, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		goto IL_00f5;
	}

IL_00c6:
	{
		String_t* L_37 = ___s0;
		int32_t L_38 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		int32_t L_39 = HttpUtility_getChar_m2917879435(NULL /*static, unused*/, L_37, ((int32_t)((int32_t)L_38+(int32_t)1)), 2, /*hidden argument*/NULL);
		int32_t L_40 = L_39;
		V_4 = L_40;
		if ((((int32_t)L_40) == ((int32_t)(-1))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_41 = V_4;
		List_1_t3052225568 * L_42 = V_0;
		Encoding_t663144255 * L_43 = ___encoding1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		HttpUtility_writeCharBytes_m3800210614(NULL /*static, unused*/, (((int32_t)((uint16_t)L_41))), L_42, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_2;
		V_2 = ((int32_t)((int32_t)L_44+(int32_t)2));
		goto IL_00f5;
	}

IL_00ec:
	{
		List_1_t3052225568 * L_45 = V_0;
		Encoding_t663144255 * L_46 = ___encoding1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		HttpUtility_writeCharBytes_m3800210614(NULL /*static, unused*/, ((int32_t)37), L_45, L_46, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		goto IL_0118;
	}

IL_00fa:
	{
		Il2CppChar L_47 = V_3;
		if ((!(((uint32_t)L_47) == ((uint32_t)((int32_t)43)))))
		{
			goto IL_0110;
		}
	}
	{
		List_1_t3052225568 * L_48 = V_0;
		Encoding_t663144255 * L_49 = ___encoding1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		HttpUtility_writeCharBytes_m3800210614(NULL /*static, unused*/, ((int32_t)32), L_48, L_49, /*hidden argument*/NULL);
		goto IL_0118;
	}

IL_0110:
	{
		Il2CppChar L_50 = V_3;
		List_1_t3052225568 * L_51 = V_0;
		Encoding_t663144255 * L_52 = ___encoding1;
		IL2CPP_RUNTIME_CLASS_INIT(HttpUtility_t3363705102_il2cpp_TypeInfo_var);
		HttpUtility_writeCharBytes_m3800210614(NULL /*static, unused*/, L_50, L_51, L_52, /*hidden argument*/NULL);
	}

IL_0118:
	{
		int32_t L_53 = V_2;
		V_2 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_011c:
	{
		int32_t L_54 = V_2;
		int32_t L_55 = V_1;
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_004f;
		}
	}
	{
		Encoding_t663144255 * L_56 = ___encoding1;
		List_1_t3052225568 * L_57 = V_0;
		NullCheck(L_57);
		ByteU5BU5D_t3397334013* L_58 = List_1_ToArray_m279205908(L_57, /*hidden argument*/List_1_ToArray_m279205908_MethodInfo_var);
		NullCheck(L_56);
		String_t* L_59 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_56, L_58);
		return L_59;
	}
}
// System.Void WebSocketSharp.Net.HttpUtility::.cctor()
extern Il2CppClass* HttpUtility_t3363705102_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral85642790;
extern const uint32_t HttpUtility__cctor_m223165678_MetadataUsageId;
extern "C"  void HttpUtility__cctor_m223165678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpUtility__cctor_m223165678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck(_stringLiteral85642790);
		CharU5BU5D_t1328083999* L_0 = String_ToCharArray_m870309954(_stringLiteral85642790, /*hidden argument*/NULL);
		((HttpUtility_t3363705102_StaticFields*)HttpUtility_t3363705102_il2cpp_TypeInfo_var->static_fields)->set__hexChars_0(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		((HttpUtility_t3363705102_StaticFields*)HttpUtility_t3363705102_il2cpp_TypeInfo_var->static_fields)->set__sync_1(L_1);
		return;
	}
}
// System.Void WebSocketSharp.Net.HttpVersion::.cctor()
extern Il2CppClass* Version_t1755874712_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpVersion_t4270509666_il2cpp_TypeInfo_var;
extern const uint32_t HttpVersion__cctor_m1193883554_MetadataUsageId;
extern "C"  void HttpVersion__cctor_m1193883554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpVersion__cctor_m1193883554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Version_t1755874712 * L_0 = (Version_t1755874712 *)il2cpp_codegen_object_new(Version_t1755874712_il2cpp_TypeInfo_var);
		Version__ctor_m2035466001(L_0, 1, 0, /*hidden argument*/NULL);
		((HttpVersion_t4270509666_StaticFields*)HttpVersion_t4270509666_il2cpp_TypeInfo_var->static_fields)->set_Version10_0(L_0);
		Version_t1755874712 * L_1 = (Version_t1755874712 *)il2cpp_codegen_object_new(Version_t1755874712_il2cpp_TypeInfo_var);
		Version__ctor_m2035466001(L_1, 1, 1, /*hidden argument*/NULL);
		((HttpVersion_t4270509666_StaticFields*)HttpVersion_t4270509666_il2cpp_TypeInfo_var->static_fields)->set_Version11_1(L_1);
		return;
	}
}
// System.String WebSocketSharp.Net.NetworkCredential::get_Domain()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t NetworkCredential_get_Domain_m3112363792_MetadataUsageId;
extern "C"  String_t* NetworkCredential_get_Domain_m3112363792 (NetworkCredential_t3911206805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkCredential_get_Domain_m3112363792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get__domain_0();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_0012:
	{
		return G_B2_0;
	}
}
// System.String WebSocketSharp.Net.NetworkCredential::get_Password()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t NetworkCredential_get_Password_m659916903_MetadataUsageId;
extern "C"  String_t* NetworkCredential_get_Password_m659916903 (NetworkCredential_t3911206805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkCredential_get_Password_m659916903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get__password_1();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_0012:
	{
		return G_B2_0;
	}
}
// System.String WebSocketSharp.Net.NetworkCredential::get_UserName()
extern "C"  String_t* NetworkCredential_get_UserName_m2976578456 (NetworkCredential_t3911206805 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__userName_3();
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.SslConfiguration::.ctor(System.Security.Authentication.SslProtocols,System.Boolean)
extern "C"  void SslConfiguration__ctor_m3803613030 (SslConfiguration_t760772650 * __this, int32_t ___enabledSslProtocols0, bool ___checkCertificateRevocation1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___enabledSslProtocols0;
		__this->set__enabledProtocols_3(L_0);
		bool L_1 = ___checkCertificateRevocation1;
		__this->set__checkCertRevocation_2(L_1);
		return;
	}
}
// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::get_CertificateSelectionCallback()
extern Il2CppClass* SslConfiguration_t760772650_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalCertificateSelectionCallback_t3696771181_il2cpp_TypeInfo_var;
extern const MethodInfo* SslConfiguration_U3Cget_CertificateSelectionCallbackU3Em__0_m3029484398_MethodInfo_var;
extern const uint32_t SslConfiguration_get_CertificateSelectionCallback_m3447833320_MetadataUsageId;
extern "C"  LocalCertificateSelectionCallback_t3696771181 * SslConfiguration_get_CertificateSelectionCallback_m3447833320 (SslConfiguration_t760772650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SslConfiguration_get_CertificateSelectionCallback_m3447833320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LocalCertificateSelectionCallback_t3696771181 * V_0 = NULL;
	LocalCertificateSelectionCallback_t3696771181 * G_B4_0 = NULL;
	LocalCertificateSelectionCallback_t3696771181 * G_B1_0 = NULL;
	SslConfiguration_t760772650 * G_B3_0 = NULL;
	SslConfiguration_t760772650 * G_B2_0 = NULL;
	{
		LocalCertificateSelectionCallback_t3696771181 * L_0 = __this->get__certSelectionCallback_0();
		LocalCertificateSelectionCallback_t3696771181 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B4_0 = L_1;
			goto IL_0033;
		}
	}
	{
		LocalCertificateSelectionCallback_t3696771181 * L_2 = ((SslConfiguration_t760772650_StaticFields*)SslConfiguration_t760772650_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_4();
		G_B2_0 = __this;
		if (L_2)
		{
			G_B3_0 = __this;
			goto IL_0026;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)SslConfiguration_U3Cget_CertificateSelectionCallbackU3Em__0_m3029484398_MethodInfo_var);
		LocalCertificateSelectionCallback_t3696771181 * L_4 = (LocalCertificateSelectionCallback_t3696771181 *)il2cpp_codegen_object_new(LocalCertificateSelectionCallback_t3696771181_il2cpp_TypeInfo_var);
		LocalCertificateSelectionCallback__ctor_m3518900705(L_4, NULL, L_3, /*hidden argument*/NULL);
		((SslConfiguration_t760772650_StaticFields*)SslConfiguration_t760772650_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_4(L_4);
		G_B3_0 = G_B2_0;
	}

IL_0026:
	{
		LocalCertificateSelectionCallback_t3696771181 * L_5 = ((SslConfiguration_t760772650_StaticFields*)SslConfiguration_t760772650_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_4();
		LocalCertificateSelectionCallback_t3696771181 * L_6 = L_5;
		V_0 = L_6;
		NullCheck(G_B3_0);
		G_B3_0->set__certSelectionCallback_0(L_6);
		LocalCertificateSelectionCallback_t3696771181 * L_7 = V_0;
		G_B4_0 = L_7;
	}

IL_0033:
	{
		return G_B4_0;
	}
}
// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::get_CertificateValidationCallback()
extern Il2CppClass* SslConfiguration_t760772650_il2cpp_TypeInfo_var;
extern Il2CppClass* RemoteCertificateValidationCallback_t2756269959_il2cpp_TypeInfo_var;
extern const MethodInfo* SslConfiguration_U3Cget_CertificateValidationCallbackU3Em__1_m3473816448_MethodInfo_var;
extern const uint32_t SslConfiguration_get_CertificateValidationCallback_m3145107081_MetadataUsageId;
extern "C"  RemoteCertificateValidationCallback_t2756269959 * SslConfiguration_get_CertificateValidationCallback_m3145107081 (SslConfiguration_t760772650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SslConfiguration_get_CertificateValidationCallback_m3145107081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RemoteCertificateValidationCallback_t2756269959 * V_0 = NULL;
	RemoteCertificateValidationCallback_t2756269959 * G_B4_0 = NULL;
	RemoteCertificateValidationCallback_t2756269959 * G_B1_0 = NULL;
	SslConfiguration_t760772650 * G_B3_0 = NULL;
	SslConfiguration_t760772650 * G_B2_0 = NULL;
	{
		RemoteCertificateValidationCallback_t2756269959 * L_0 = __this->get__certValidationCallback_1();
		RemoteCertificateValidationCallback_t2756269959 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B4_0 = L_1;
			goto IL_0033;
		}
	}
	{
		RemoteCertificateValidationCallback_t2756269959 * L_2 = ((SslConfiguration_t760772650_StaticFields*)SslConfiguration_t760772650_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_5();
		G_B2_0 = __this;
		if (L_2)
		{
			G_B3_0 = __this;
			goto IL_0026;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)SslConfiguration_U3Cget_CertificateValidationCallbackU3Em__1_m3473816448_MethodInfo_var);
		RemoteCertificateValidationCallback_t2756269959 * L_4 = (RemoteCertificateValidationCallback_t2756269959 *)il2cpp_codegen_object_new(RemoteCertificateValidationCallback_t2756269959_il2cpp_TypeInfo_var);
		RemoteCertificateValidationCallback__ctor_m2946714095(L_4, NULL, L_3, /*hidden argument*/NULL);
		((SslConfiguration_t760772650_StaticFields*)SslConfiguration_t760772650_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_5(L_4);
		G_B3_0 = G_B2_0;
	}

IL_0026:
	{
		RemoteCertificateValidationCallback_t2756269959 * L_5 = ((SslConfiguration_t760772650_StaticFields*)SslConfiguration_t760772650_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_5();
		RemoteCertificateValidationCallback_t2756269959 * L_6 = L_5;
		V_0 = L_6;
		NullCheck(G_B3_0);
		G_B3_0->set__certValidationCallback_1(L_6);
		RemoteCertificateValidationCallback_t2756269959 * L_7 = V_0;
		G_B4_0 = L_7;
	}

IL_0033:
	{
		return G_B4_0;
	}
}
// System.Boolean WebSocketSharp.Net.SslConfiguration::get_CheckCertificateRevocation()
extern "C"  bool SslConfiguration_get_CheckCertificateRevocation_m1861366223 (SslConfiguration_t760772650 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__checkCertRevocation_2();
		return L_0;
	}
}
// System.Security.Authentication.SslProtocols WebSocketSharp.Net.SslConfiguration::get_EnabledSslProtocols()
extern "C"  int32_t SslConfiguration_get_EnabledSslProtocols_m4088005259 (SslConfiguration_t760772650 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__enabledProtocols_3();
		return L_0;
	}
}
// System.Security.Cryptography.X509Certificates.X509Certificate WebSocketSharp.Net.SslConfiguration::<get_CertificateSelectionCallback>m__0(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern "C"  X509Certificate_t283079845 * SslConfiguration_U3Cget_CertificateSelectionCallbackU3Em__0_m3029484398 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, String_t* ___targetHost1, X509CertificateCollection_t1197680765 * ___localCertificates2, X509Certificate_t283079845 * ___remoteCertificate3, StringU5BU5D_t1642385972* ___acceptableIssuers4, const MethodInfo* method)
{
	{
		return (X509Certificate_t283079845 *)NULL;
	}
}
// System.Boolean WebSocketSharp.Net.SslConfiguration::<get_CertificateValidationCallback>m__1(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool SslConfiguration_U3Cget_CertificateValidationCallbackU3Em__1_m3473816448 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::.cctor()
extern Il2CppClass* StringComparer_t1574862926_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t4011098823_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var;
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2540854212_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m464019225_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3819387978;
extern Il2CppCodeGenString* _stringLiteral3825230842;
extern Il2CppCodeGenString* _stringLiteral667199935;
extern Il2CppCodeGenString* _stringLiteral2542706899;
extern Il2CppCodeGenString* _stringLiteral3093553084;
extern Il2CppCodeGenString* _stringLiteral3470269872;
extern Il2CppCodeGenString* _stringLiteral1047232699;
extern Il2CppCodeGenString* _stringLiteral718400824;
extern Il2CppCodeGenString* _stringLiteral1071694037;
extern Il2CppCodeGenString* _stringLiteral1858829707;
extern Il2CppCodeGenString* _stringLiteral1776488541;
extern Il2CppCodeGenString* _stringLiteral2313763943;
extern Il2CppCodeGenString* _stringLiteral3396891663;
extern Il2CppCodeGenString* _stringLiteral70265372;
extern Il2CppCodeGenString* _stringLiteral208267350;
extern Il2CppCodeGenString* _stringLiteral2730815906;
extern Il2CppCodeGenString* _stringLiteral4152225807;
extern Il2CppCodeGenString* _stringLiteral569599163;
extern Il2CppCodeGenString* _stringLiteral336639684;
extern Il2CppCodeGenString* _stringLiteral648667121;
extern Il2CppCodeGenString* _stringLiteral677017058;
extern Il2CppCodeGenString* _stringLiteral728744484;
extern Il2CppCodeGenString* _stringLiteral1238491025;
extern Il2CppCodeGenString* _stringLiteral3142455781;
extern Il2CppCodeGenString* _stringLiteral2890767806;
extern Il2CppCodeGenString* _stringLiteral1947920300;
extern Il2CppCodeGenString* _stringLiteral4260444135;
extern Il2CppCodeGenString* _stringLiteral2821961097;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral2167658612;
extern Il2CppCodeGenString* _stringLiteral4063053990;
extern Il2CppCodeGenString* _stringLiteral2328219770;
extern Il2CppCodeGenString* _stringLiteral1992021647;
extern Il2CppCodeGenString* _stringLiteral237328742;
extern Il2CppCodeGenString* _stringLiteral2624615951;
extern Il2CppCodeGenString* _stringLiteral2967033390;
extern Il2CppCodeGenString* _stringLiteral3430668980;
extern Il2CppCodeGenString* _stringLiteral234852226;
extern Il2CppCodeGenString* _stringLiteral1610122205;
extern Il2CppCodeGenString* _stringLiteral1444095912;
extern Il2CppCodeGenString* _stringLiteral3646691448;
extern Il2CppCodeGenString* _stringLiteral629182836;
extern Il2CppCodeGenString* _stringLiteral2211929686;
extern Il2CppCodeGenString* _stringLiteral2204249166;
extern Il2CppCodeGenString* _stringLiteral940134089;
extern Il2CppCodeGenString* _stringLiteral2878179655;
extern Il2CppCodeGenString* _stringLiteral750893443;
extern Il2CppCodeGenString* _stringLiteral2341113048;
extern Il2CppCodeGenString* _stringLiteral4118028813;
extern Il2CppCodeGenString* _stringLiteral3441356471;
extern Il2CppCodeGenString* _stringLiteral3171699710;
extern Il2CppCodeGenString* _stringLiteral2325276717;
extern Il2CppCodeGenString* _stringLiteral3395443378;
extern Il2CppCodeGenString* _stringLiteral2708058001;
extern Il2CppCodeGenString* _stringLiteral707257838;
extern Il2CppCodeGenString* _stringLiteral719609705;
extern Il2CppCodeGenString* _stringLiteral3133534876;
extern Il2CppCodeGenString* _stringLiteral2191732495;
extern Il2CppCodeGenString* _stringLiteral343754898;
extern Il2CppCodeGenString* _stringLiteral765986898;
extern Il2CppCodeGenString* _stringLiteral2577051699;
extern Il2CppCodeGenString* _stringLiteral683438393;
extern Il2CppCodeGenString* _stringLiteral1353023627;
extern Il2CppCodeGenString* _stringLiteral3277927953;
extern Il2CppCodeGenString* _stringLiteral3043185442;
extern Il2CppCodeGenString* _stringLiteral3817214473;
extern Il2CppCodeGenString* _stringLiteral1628577290;
extern Il2CppCodeGenString* _stringLiteral3689292460;
extern Il2CppCodeGenString* _stringLiteral776174704;
extern Il2CppCodeGenString* _stringLiteral22895698;
extern Il2CppCodeGenString* _stringLiteral747056407;
extern Il2CppCodeGenString* _stringLiteral347639339;
extern Il2CppCodeGenString* _stringLiteral3625675062;
extern Il2CppCodeGenString* _stringLiteral566102924;
extern Il2CppCodeGenString* _stringLiteral705762738;
extern Il2CppCodeGenString* _stringLiteral2444067936;
extern Il2CppCodeGenString* _stringLiteral3452399285;
extern Il2CppCodeGenString* _stringLiteral507040284;
extern Il2CppCodeGenString* _stringLiteral598520931;
extern Il2CppCodeGenString* _stringLiteral1752778486;
extern Il2CppCodeGenString* _stringLiteral3686238577;
extern Il2CppCodeGenString* _stringLiteral3015944881;
extern Il2CppCodeGenString* _stringLiteral1591023441;
extern Il2CppCodeGenString* _stringLiteral1205284341;
extern Il2CppCodeGenString* _stringLiteral4058379636;
extern Il2CppCodeGenString* _stringLiteral1055241449;
extern Il2CppCodeGenString* _stringLiteral3736214732;
extern Il2CppCodeGenString* _stringLiteral3608358242;
extern Il2CppCodeGenString* _stringLiteral702028774;
extern Il2CppCodeGenString* _stringLiteral874979797;
extern Il2CppCodeGenString* _stringLiteral3228897586;
extern Il2CppCodeGenString* _stringLiteral339800494;
extern Il2CppCodeGenString* _stringLiteral1861340610;
extern Il2CppCodeGenString* _stringLiteral613330752;
extern Il2CppCodeGenString* _stringLiteral1978770395;
extern const uint32_t WebHeaderCollection__cctor_m4052197875_MetadataUsageId;
extern "C"  void WebHeaderCollection__cctor_m4052197875 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection__cctor_m4052197875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t4011098823 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1574862926_il2cpp_TypeInfo_var);
		StringComparer_t1574862926 * L_0 = StringComparer_get_InvariantCultureIgnoreCase_m1052445386(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t4011098823 * L_1 = (Dictionary_2_t4011098823 *)il2cpp_codegen_object_new(Dictionary_2_t4011098823_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2540854212(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m2540854212_MethodInfo_var);
		V_0 = L_1;
		Dictionary_2_t4011098823 * L_2 = V_0;
		HttpHeaderInfo_t2096319561 * L_3 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_3, _stringLiteral3819387978, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_2);
		Dictionary_2_Add_m464019225(L_2, _stringLiteral3819387978, L_3, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_4 = V_0;
		HttpHeaderInfo_t2096319561 * L_5 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_5, _stringLiteral667199935, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m464019225(L_4, _stringLiteral3825230842, L_5, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_6 = V_0;
		HttpHeaderInfo_t2096319561 * L_7 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_7, _stringLiteral3093553084, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_6);
		Dictionary_2_Add_m464019225(L_6, _stringLiteral2542706899, L_7, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_8 = V_0;
		HttpHeaderInfo_t2096319561 * L_9 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_9, _stringLiteral1047232699, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_8);
		Dictionary_2_Add_m464019225(L_8, _stringLiteral3470269872, L_9, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_10 = V_0;
		HttpHeaderInfo_t2096319561 * L_11 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_11, _stringLiteral1071694037, ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_Add_m464019225(L_10, _stringLiteral718400824, L_11, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_12 = V_0;
		HttpHeaderInfo_t2096319561 * L_13 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_13, _stringLiteral1858829707, 2, /*hidden argument*/NULL);
		NullCheck(L_12);
		Dictionary_2_Add_m464019225(L_12, _stringLiteral1858829707, L_13, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_14 = V_0;
		HttpHeaderInfo_t2096319561 * L_15 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_15, _stringLiteral1776488541, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_14);
		Dictionary_2_Add_m464019225(L_14, _stringLiteral1776488541, L_15, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_16 = V_0;
		HttpHeaderInfo_t2096319561 * L_17 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_17, _stringLiteral2313763943, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_16);
		Dictionary_2_Add_m464019225(L_16, _stringLiteral2313763943, L_17, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_18 = V_0;
		HttpHeaderInfo_t2096319561 * L_19 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_19, _stringLiteral70265372, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_18);
		Dictionary_2_Add_m464019225(L_18, _stringLiteral3396891663, L_19, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_20 = V_0;
		HttpHeaderInfo_t2096319561 * L_21 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_21, _stringLiteral208267350, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_20);
		Dictionary_2_Add_m464019225(L_20, _stringLiteral208267350, L_21, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_22 = V_0;
		HttpHeaderInfo_t2096319561 * L_23 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_23, _stringLiteral4152225807, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_22);
		Dictionary_2_Add_m464019225(L_22, _stringLiteral2730815906, L_23, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_24 = V_0;
		HttpHeaderInfo_t2096319561 * L_25 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_25, _stringLiteral336639684, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_24);
		Dictionary_2_Add_m464019225(L_24, _stringLiteral569599163, L_25, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_26 = V_0;
		HttpHeaderInfo_t2096319561 * L_27 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_27, _stringLiteral677017058, 7, /*hidden argument*/NULL);
		NullCheck(L_26);
		Dictionary_2_Add_m464019225(L_26, _stringLiteral648667121, L_27, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_28 = V_0;
		HttpHeaderInfo_t2096319561 * L_29 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_29, _stringLiteral1238491025, 3, /*hidden argument*/NULL);
		NullCheck(L_28);
		Dictionary_2_Add_m464019225(L_28, _stringLiteral728744484, L_29, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_30 = V_0;
		HttpHeaderInfo_t2096319561 * L_31 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_31, _stringLiteral2890767806, 3, /*hidden argument*/NULL);
		NullCheck(L_30);
		Dictionary_2_Add_m464019225(L_30, _stringLiteral3142455781, L_31, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_32 = V_0;
		HttpHeaderInfo_t2096319561 * L_33 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_33, _stringLiteral4260444135, 3, /*hidden argument*/NULL);
		NullCheck(L_32);
		Dictionary_2_Add_m464019225(L_32, _stringLiteral1947920300, L_33, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_34 = V_0;
		HttpHeaderInfo_t2096319561 * L_35 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_35, _stringLiteral1048821954, 7, /*hidden argument*/NULL);
		NullCheck(L_34);
		Dictionary_2_Add_m464019225(L_34, _stringLiteral2821961097, L_35, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_36 = V_0;
		HttpHeaderInfo_t2096319561 * L_37 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_37, _stringLiteral2167658612, 1, /*hidden argument*/NULL);
		NullCheck(L_36);
		Dictionary_2_Add_m464019225(L_36, _stringLiteral2167658612, L_37, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_38 = V_0;
		HttpHeaderInfo_t2096319561 * L_39 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_39, _stringLiteral4063053990, 1, /*hidden argument*/NULL);
		NullCheck(L_38);
		Dictionary_2_Add_m464019225(L_38, _stringLiteral4063053990, L_39, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_40 = V_0;
		HttpHeaderInfo_t2096319561 * L_41 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_41, _stringLiteral2328219770, 7, /*hidden argument*/NULL);
		NullCheck(L_40);
		Dictionary_2_Add_m464019225(L_40, _stringLiteral2328219770, L_41, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_42 = V_0;
		HttpHeaderInfo_t2096319561 * L_43 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_43, _stringLiteral1992021647, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_42);
		Dictionary_2_Add_m464019225(L_42, _stringLiteral1992021647, L_43, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_44 = V_0;
		HttpHeaderInfo_t2096319561 * L_45 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_45, _stringLiteral237328742, 3, /*hidden argument*/NULL);
		NullCheck(L_44);
		Dictionary_2_Add_m464019225(L_44, _stringLiteral237328742, L_45, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_46 = V_0;
		HttpHeaderInfo_t2096319561 * L_47 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_47, _stringLiteral2624615951, 2, /*hidden argument*/NULL);
		NullCheck(L_46);
		Dictionary_2_Add_m464019225(L_46, _stringLiteral2624615951, L_47, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_48 = V_0;
		HttpHeaderInfo_t2096319561 * L_49 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_49, _stringLiteral2967033390, 1, /*hidden argument*/NULL);
		NullCheck(L_48);
		Dictionary_2_Add_m464019225(L_48, _stringLiteral2967033390, L_49, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_50 = V_0;
		HttpHeaderInfo_t2096319561 * L_51 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_51, _stringLiteral3430668980, 5, /*hidden argument*/NULL);
		NullCheck(L_50);
		Dictionary_2_Add_m464019225(L_50, _stringLiteral3430668980, L_51, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_52 = V_0;
		HttpHeaderInfo_t2096319561 * L_53 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_53, _stringLiteral1610122205, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_52);
		Dictionary_2_Add_m464019225(L_52, _stringLiteral234852226, L_53, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_54 = V_0;
		HttpHeaderInfo_t2096319561 * L_55 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_55, _stringLiteral3646691448, 5, /*hidden argument*/NULL);
		NullCheck(L_54);
		Dictionary_2_Add_m464019225(L_54, _stringLiteral1444095912, L_55, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_56 = V_0;
		HttpHeaderInfo_t2096319561 * L_57 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_57, _stringLiteral2211929686, ((int32_t)9), /*hidden argument*/NULL);
		NullCheck(L_56);
		Dictionary_2_Add_m464019225(L_56, _stringLiteral629182836, L_57, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_58 = V_0;
		HttpHeaderInfo_t2096319561 * L_59 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_59, _stringLiteral940134089, 1, /*hidden argument*/NULL);
		NullCheck(L_58);
		Dictionary_2_Add_m464019225(L_58, _stringLiteral2204249166, L_59, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_60 = V_0;
		HttpHeaderInfo_t2096319561 * L_61 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_61, _stringLiteral750893443, 1, /*hidden argument*/NULL);
		NullCheck(L_60);
		Dictionary_2_Add_m464019225(L_60, _stringLiteral2878179655, L_61, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_62 = V_0;
		HttpHeaderInfo_t2096319561 * L_63 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_63, _stringLiteral4118028813, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_62);
		Dictionary_2_Add_m464019225(L_62, _stringLiteral2341113048, L_63, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_64 = V_0;
		HttpHeaderInfo_t2096319561 * L_65 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_65, _stringLiteral3171699710, 3, /*hidden argument*/NULL);
		NullCheck(L_64);
		Dictionary_2_Add_m464019225(L_64, _stringLiteral3441356471, L_65, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_66 = V_0;
		HttpHeaderInfo_t2096319561 * L_67 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_67, _stringLiteral2325276717, 2, /*hidden argument*/NULL);
		NullCheck(L_66);
		Dictionary_2_Add_m464019225(L_66, _stringLiteral2325276717, L_67, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_68 = V_0;
		HttpHeaderInfo_t2096319561 * L_69 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_69, _stringLiteral2708058001, 1, /*hidden argument*/NULL);
		NullCheck(L_68);
		Dictionary_2_Add_m464019225(L_68, _stringLiteral3395443378, L_69, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_70 = V_0;
		HttpHeaderInfo_t2096319561 * L_71 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_71, _stringLiteral707257838, 3, /*hidden argument*/NULL);
		NullCheck(L_70);
		Dictionary_2_Add_m464019225(L_70, _stringLiteral707257838, L_71, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_72 = V_0;
		HttpHeaderInfo_t2096319561 * L_73 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_73, _stringLiteral3133534876, ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_72);
		Dictionary_2_Add_m464019225(L_72, _stringLiteral719609705, L_73, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_74 = V_0;
		HttpHeaderInfo_t2096319561 * L_75 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_75, _stringLiteral343754898, 1, /*hidden argument*/NULL);
		NullCheck(L_74);
		Dictionary_2_Add_m464019225(L_74, _stringLiteral2191732495, L_75, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_76 = V_0;
		HttpHeaderInfo_t2096319561 * L_77 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_77, _stringLiteral2577051699, 7, /*hidden argument*/NULL);
		NullCheck(L_76);
		Dictionary_2_Add_m464019225(L_76, _stringLiteral765986898, L_77, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_78 = V_0;
		HttpHeaderInfo_t2096319561 * L_79 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_79, _stringLiteral683438393, ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_78);
		Dictionary_2_Add_m464019225(L_78, _stringLiteral683438393, L_79, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_80 = V_0;
		HttpHeaderInfo_t2096319561 * L_81 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_81, _stringLiteral1353023627, ((int32_t)13), /*hidden argument*/NULL);
		NullCheck(L_80);
		Dictionary_2_Add_m464019225(L_80, _stringLiteral1353023627, L_81, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_82 = V_0;
		HttpHeaderInfo_t2096319561 * L_83 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_83, _stringLiteral3277927953, 5, /*hidden argument*/NULL);
		NullCheck(L_82);
		Dictionary_2_Add_m464019225(L_82, _stringLiteral3277927953, L_83, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_84 = V_0;
		HttpHeaderInfo_t2096319561 * L_85 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_85, _stringLiteral3817214473, 2, /*hidden argument*/NULL);
		NullCheck(L_84);
		Dictionary_2_Add_m464019225(L_84, _stringLiteral3043185442, L_85, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_86 = V_0;
		HttpHeaderInfo_t2096319561 * L_87 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_87, _stringLiteral3689292460, 6, /*hidden argument*/NULL);
		NullCheck(L_86);
		Dictionary_2_Add_m464019225(L_86, _stringLiteral1628577290, L_87, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_88 = V_0;
		HttpHeaderInfo_t2096319561 * L_89 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_89, _stringLiteral22895698, ((int32_t)23), /*hidden argument*/NULL);
		NullCheck(L_88);
		Dictionary_2_Add_m464019225(L_88, _stringLiteral776174704, L_89, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_90 = V_0;
		HttpHeaderInfo_t2096319561 * L_91 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_91, _stringLiteral347639339, 5, /*hidden argument*/NULL);
		NullCheck(L_90);
		Dictionary_2_Add_m464019225(L_90, _stringLiteral747056407, L_91, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_92 = V_0;
		HttpHeaderInfo_t2096319561 * L_93 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_93, _stringLiteral566102924, ((int32_t)19), /*hidden argument*/NULL);
		NullCheck(L_92);
		Dictionary_2_Add_m464019225(L_92, _stringLiteral3625675062, L_93, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_94 = V_0;
		HttpHeaderInfo_t2096319561 * L_95 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_95, _stringLiteral2444067936, ((int32_t)39), /*hidden argument*/NULL);
		NullCheck(L_94);
		Dictionary_2_Add_m464019225(L_94, _stringLiteral705762738, L_95, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_96 = V_0;
		HttpHeaderInfo_t2096319561 * L_97 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_97, _stringLiteral3452399285, 2, /*hidden argument*/NULL);
		NullCheck(L_96);
		Dictionary_2_Add_m464019225(L_96, _stringLiteral3452399285, L_97, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_98 = V_0;
		HttpHeaderInfo_t2096319561 * L_99 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_99, _stringLiteral598520931, ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_98);
		Dictionary_2_Add_m464019225(L_98, _stringLiteral507040284, L_99, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_100 = V_0;
		HttpHeaderInfo_t2096319561 * L_101 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_101, _stringLiteral3686238577, ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_100);
		Dictionary_2_Add_m464019225(L_100, _stringLiteral1752778486, L_101, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_102 = V_0;
		HttpHeaderInfo_t2096319561 * L_103 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_103, _stringLiteral1591023441, 1, /*hidden argument*/NULL);
		NullCheck(L_102);
		Dictionary_2_Add_m464019225(L_102, _stringLiteral3015944881, L_103, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_104 = V_0;
		HttpHeaderInfo_t2096319561 * L_105 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_105, _stringLiteral1205284341, 3, /*hidden argument*/NULL);
		NullCheck(L_104);
		Dictionary_2_Add_m464019225(L_104, _stringLiteral1205284341, L_105, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_106 = V_0;
		HttpHeaderInfo_t2096319561 * L_107 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_107, _stringLiteral1055241449, ((int32_t)15), /*hidden argument*/NULL);
		NullCheck(L_106);
		Dictionary_2_Add_m464019225(L_106, _stringLiteral4058379636, L_107, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_108 = V_0;
		HttpHeaderInfo_t2096319561 * L_109 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_109, _stringLiteral3736214732, 1, /*hidden argument*/NULL);
		NullCheck(L_108);
		Dictionary_2_Add_m464019225(L_108, _stringLiteral3736214732, L_109, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_110 = V_0;
		HttpHeaderInfo_t2096319561 * L_111 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_111, _stringLiteral3608358242, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_110);
		Dictionary_2_Add_m464019225(L_110, _stringLiteral3608358242, L_111, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_112 = V_0;
		HttpHeaderInfo_t2096319561 * L_113 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_113, _stringLiteral874979797, 5, /*hidden argument*/NULL);
		NullCheck(L_112);
		Dictionary_2_Add_m464019225(L_112, _stringLiteral702028774, L_113, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_114 = V_0;
		HttpHeaderInfo_t2096319561 * L_115 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_115, _stringLiteral3228897586, ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_114);
		Dictionary_2_Add_m464019225(L_114, _stringLiteral3228897586, L_115, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_116 = V_0;
		HttpHeaderInfo_t2096319561 * L_117 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_117, _stringLiteral339800494, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_116);
		Dictionary_2_Add_m464019225(L_116, _stringLiteral339800494, L_117, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_118 = V_0;
		HttpHeaderInfo_t2096319561 * L_119 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_119, _stringLiteral1861340610, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_118);
		Dictionary_2_Add_m464019225(L_118, _stringLiteral1861340610, L_119, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_120 = V_0;
		HttpHeaderInfo_t2096319561 * L_121 = (HttpHeaderInfo_t2096319561 *)il2cpp_codegen_object_new(HttpHeaderInfo_t2096319561_il2cpp_TypeInfo_var);
		HttpHeaderInfo__ctor_m3133389831(L_121, _stringLiteral1978770395, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_120);
		Dictionary_2_Add_m464019225(L_120, _stringLiteral613330752, L_121, /*hidden argument*/Dictionary_2_Add_m464019225_MethodInfo_var);
		Dictionary_2_t4011098823 * L_122 = V_0;
		((WebHeaderCollection_t1932982249_StaticFields*)WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var->static_fields)->set__headers_12(L_122);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* SerializationException_t753258759_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral438048066;
extern Il2CppCodeGenString* _stringLiteral630702433;
extern Il2CppCodeGenString* _stringLiteral1695958603;
extern Il2CppCodeGenString* _stringLiteral1554779067;
extern const uint32_t WebHeaderCollection__ctor_m4110470603_MetadataUsageId;
extern "C"  void WebHeaderCollection__ctor_m4110470603 (WebHeaderCollection_t1932982249 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection__ctor_m4110470603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	SerializationException_t753258759 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NameValueCollection__ctor_m1767369537(__this, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_0 = ___serializationInfo0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral438048066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			SerializationInfo_t228987430 * L_2 = ___serializationInfo0;
			NullCheck(L_2);
			bool L_3 = SerializationInfo_GetBoolean_m3573708305(L_2, _stringLiteral630702433, /*hidden argument*/NULL);
			__this->set__internallyUsed_13(L_3);
			SerializationInfo_t228987430 * L_4 = ___serializationInfo0;
			NullCheck(L_4);
			int32_t L_5 = SerializationInfo_GetInt32_m4039439501(L_4, _stringLiteral1695958603, /*hidden argument*/NULL);
			__this->set__state_14(L_5);
			SerializationInfo_t228987430 * L_6 = ___serializationInfo0;
			NullCheck(L_6);
			int32_t L_7 = SerializationInfo_GetInt32_m4039439501(L_6, _stringLiteral1554779067, /*hidden argument*/NULL);
			V_0 = L_7;
			V_1 = 0;
			goto IL_0080;
		}

IL_004c:
		{
			SerializationInfo_t228987430 * L_8 = ___serializationInfo0;
			String_t* L_9 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
			NullCheck(L_8);
			String_t* L_10 = SerializationInfo_GetString_m547109409(L_8, L_9, /*hidden argument*/NULL);
			SerializationInfo_t228987430 * L_11 = ___serializationInfo0;
			int32_t L_12 = V_0;
			int32_t L_13 = V_1;
			V_2 = ((int32_t)((int32_t)L_12+(int32_t)L_13));
			String_t* L_14 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
			NullCheck(L_11);
			String_t* L_15 = SerializationInfo_GetString_m547109409(L_11, L_14, /*hidden argument*/NULL);
			NameValueCollection_Add_m263445674(__this, L_10, L_15, /*hidden argument*/NULL);
			int32_t L_16 = V_1;
			V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
		}

IL_0080:
		{
			int32_t L_17 = V_1;
			int32_t L_18 = V_0;
			if ((((int32_t)L_17) < ((int32_t)L_18)))
			{
				goto IL_004c;
			}
		}

IL_0087:
		{
			goto IL_009f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (SerializationException_t753258759_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_008c;
		throw e;
	}

CATCH_008c:
	{ // begin catch(System.Runtime.Serialization.SerializationException)
		V_3 = ((SerializationException_t753258759 *)__exception_local);
		SerializationException_t753258759 * L_19 = V_3;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_19);
		SerializationException_t753258759 * L_21 = V_3;
		ArgumentException_t3259014390 * L_22 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3312963299(L_22, L_20, _stringLiteral438048066, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	} // end catch (depth: 1)

IL_009f:
	{
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor()
extern "C"  void WebHeaderCollection__ctor_m648964198 (WebHeaderCollection_t1932982249 * __this, const MethodInfo* method)
{
	{
		NameValueCollection__ctor_m1767369537(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] WebSocketSharp.Net.WebHeaderCollection::get_AllKeys()
extern "C"  StringU5BU5D_t1642385972* WebHeaderCollection_get_AllKeys_m546917123 (WebHeaderCollection_t1932982249 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = NameValueCollection_get_AllKeys_m1515107960(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 WebSocketSharp.Net.WebHeaderCollection::get_Count()
extern "C"  int32_t WebHeaderCollection_get_Count_m297968716 (WebHeaderCollection_t1932982249 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = NameObjectCollectionBase_get_Count_m2353593692(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection WebSocketSharp.Net.WebHeaderCollection::get_Keys()
extern "C"  KeysCollection_t633582367 * WebHeaderCollection_get_Keys_m4089646907 (WebHeaderCollection_t1932982249 * __this, const MethodInfo* method)
{
	{
		KeysCollection_t633582367 * L_0 = NameObjectCollectionBase_get_Keys_m446053925(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::add(System.String,System.String,System.Boolean)
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const MethodInfo* WebHeaderCollection_addWithoutCheckingNameAndRestricted_m613474086_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m925710042_MethodInfo_var;
extern const MethodInfo* WebHeaderCollection_addWithoutCheckingName_m706405108_MethodInfo_var;
extern const uint32_t WebHeaderCollection_add_m3974908564_MetadataUsageId;
extern "C"  void WebHeaderCollection_add_m3974908564 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, bool ___ignoreRestricted2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_add_m3974908564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t4234541925 * V_0 = NULL;
	Action_2_t4234541925 * G_B3_0 = NULL;
	{
		bool L_0 = ___ignoreRestricted2;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)WebHeaderCollection_addWithoutCheckingNameAndRestricted_m613474086_MethodInfo_var);
		Action_2_t4234541925 * L_2 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m925710042(L_2, __this, L_1, /*hidden argument*/Action_2__ctor_m925710042_MethodInfo_var);
		G_B3_0 = L_2;
		goto IL_0023;
	}

IL_0017:
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)WebHeaderCollection_addWithoutCheckingName_m706405108_MethodInfo_var);
		Action_2_t4234541925 * L_4 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m925710042(L_4, __this, L_3, /*hidden argument*/Action_2__ctor_m925710042_MethodInfo_var);
		G_B3_0 = L_4;
	}

IL_0023:
	{
		V_0 = G_B3_0;
		Action_2_t4234541925 * L_5 = V_0;
		String_t* L_6 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		String_t* L_7 = WebHeaderCollection_checkName_m4185892436(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_8 = ___value1;
		WebHeaderCollection_doWithCheckingState_m3077687946(__this, L_5, L_7, L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingName(System.String,System.String)
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern const MethodInfo* NameValueCollection_Add_m263445674_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m925710042_MethodInfo_var;
extern const uint32_t WebHeaderCollection_addWithoutCheckingName_m706405108_MetadataUsageId;
extern "C"  void WebHeaderCollection_addWithoutCheckingName_m706405108 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_addWithoutCheckingName_m706405108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)NameValueCollection_Add_m263445674_MethodInfo_var);
		Action_2_t4234541925 * L_1 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m925710042(L_1, __this, L_0, /*hidden argument*/Action_2__ctor_m925710042_MethodInfo_var);
		String_t* L_2 = ___name0;
		String_t* L_3 = ___value1;
		WebHeaderCollection_doWithoutCheckingName_m1412329631(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingNameAndRestricted(System.String,System.String)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_addWithoutCheckingNameAndRestricted_m613474086_MetadataUsageId;
extern "C"  void WebHeaderCollection_addWithoutCheckingNameAndRestricted_m613474086 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_addWithoutCheckingNameAndRestricted_m613474086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		String_t* L_2 = WebHeaderCollection_checkValue_m2315833202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NameValueCollection_Add_m263445674(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocketSharp.Net.WebHeaderCollection::checkColonSeparated(System.String)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1026313278;
extern Il2CppCodeGenString* _stringLiteral51154647;
extern const uint32_t WebHeaderCollection_checkColonSeparated_m3792609556_MetadataUsageId;
extern "C"  int32_t WebHeaderCollection_checkColonSeparated_m3792609556 (Il2CppObject * __this /* static, unused */, String_t* ___header0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_checkColonSeparated_m3792609556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___header0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m2358239236(L_0, ((int32_t)58), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_3, _stringLiteral1026313278, _stringLiteral51154647, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::checkHeaderType(System.String)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_checkHeaderType_m3180900097_MetadataUsageId;
extern "C"  int32_t WebHeaderCollection_checkHeaderType_m3180900097 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_checkHeaderType_m3180900097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpHeaderInfo_t2096319561 * V_0 = NULL;
	int32_t G_B9_0 = 0;
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		HttpHeaderInfo_t2096319561 * L_1 = WebHeaderCollection_getHeaderInfo_m3913797101(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HttpHeaderInfo_t2096319561 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		G_B9_0 = 0;
		goto IL_004c;
	}

IL_0013:
	{
		HttpHeaderInfo_t2096319561 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = HttpHeaderInfo_get_IsRequest_m3670607568(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		HttpHeaderInfo_t2096319561 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = HttpHeaderInfo_get_IsResponse_m4072830454(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002f;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_004c;
	}

IL_002f:
	{
		HttpHeaderInfo_t2096319561 * L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = HttpHeaderInfo_get_IsRequest_m3670607568(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		HttpHeaderInfo_t2096319561 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = HttpHeaderInfo_get_IsResponse_m4072830454(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004b;
		}
	}
	{
		G_B9_0 = 2;
		goto IL_004c;
	}

IL_004b:
	{
		G_B9_0 = 0;
	}

IL_004c:
	{
		return (int32_t)(G_B9_0);
	}
}
// System.String WebSocketSharp.Net.WebHeaderCollection::checkName(System.String)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern Il2CppCodeGenString* _stringLiteral402758406;
extern const uint32_t WebHeaderCollection_checkName_m4185892436_MetadataUsageId;
extern "C"  String_t* WebHeaderCollection_checkName_m4185892436 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_checkName_m4185892436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___name0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}

IL_0011:
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = ___name0;
		NullCheck(L_4);
		String_t* L_5 = String_Trim_m2668767713(L_4, /*hidden argument*/NULL);
		___name0 = L_5;
		String_t* L_6 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		bool L_7 = WebHeaderCollection_IsHeaderName_m1718848626(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, _stringLiteral402758406, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003f:
	{
		String_t* L_9 = ___name0;
		return L_9;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::checkRestricted(System.String)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3027738051;
extern const uint32_t WebHeaderCollection_checkRestricted_m2386154677_MetadataUsageId;
extern "C"  void WebHeaderCollection_checkRestricted_m2386154677 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_checkRestricted_m2386154677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__internallyUsed_13();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_1 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		bool L_2 = WebHeaderCollection_isRestricted_m1778673578(NULL /*static, unused*/, L_1, (bool)1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, _stringLiteral3027738051, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::checkState(System.Boolean)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3048209269;
extern Il2CppCodeGenString* _stringLiteral48526911;
extern const uint32_t WebHeaderCollection_checkState_m4278117532_MetadataUsageId;
extern "C"  void WebHeaderCollection_checkState_m4278117532 (WebHeaderCollection_t1932982249 * __this, bool ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_checkState_m4278117532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__state_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = ___response0;
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = __this->get__state_14();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral3048209269, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		bool L_4 = ___response0;
		if (L_4)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_5 = __this->get__state_14();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0046;
		}
	}
	{
		InvalidOperationException_t721527559 * L_6 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_6, _stringLiteral48526911, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0046:
	{
		return;
	}
}
// System.String WebSocketSharp.Net.WebHeaderCollection::checkValue(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral498760489;
extern Il2CppCodeGenString* _stringLiteral402758406;
extern const uint32_t WebHeaderCollection_checkValue_m2315833202_MetadataUsageId;
extern "C"  String_t* WebHeaderCollection_checkValue_m2315833202 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_checkValue_m2315833202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}

IL_0017:
	{
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		String_t* L_5 = String_Trim_m2668767713(L_4, /*hidden argument*/NULL);
		___value0 = L_5;
		String_t* L_6 = ___value0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, _stringLiteral1803325615, _stringLiteral498760489, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003f:
	{
		String_t* L_9 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		bool L_10 = WebHeaderCollection_IsHeaderValue_m1729483932(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_005a;
		}
	}
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_11, _stringLiteral402758406, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_005a:
	{
		String_t* L_12 = ___value0;
		return L_12;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m538947191_MethodInfo_var;
extern const uint32_t WebHeaderCollection_doWithCheckingState_m3077687946_MetadataUsageId;
extern "C"  void WebHeaderCollection_doWithCheckingState_m3077687946 (WebHeaderCollection_t1932982249 * __this, Action_2_t4234541925 * ___action0, String_t* ___name1, String_t* ___value2, bool ___setState3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_doWithCheckingState_m3077687946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___name1;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		int32_t L_1 = WebHeaderCollection_checkHeaderType_m3180900097(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001f;
		}
	}
	{
		Action_2_t4234541925 * L_3 = ___action0;
		String_t* L_4 = ___name1;
		String_t* L_5 = ___value2;
		bool L_6 = ___setState3;
		WebHeaderCollection_doWithCheckingState_m2988265113(__this, L_3, L_4, L_5, (bool)0, L_6, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_001f:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_0037;
		}
	}
	{
		Action_2_t4234541925 * L_8 = ___action0;
		String_t* L_9 = ___name1;
		String_t* L_10 = ___value2;
		bool L_11 = ___setState3;
		WebHeaderCollection_doWithCheckingState_m2988265113(__this, L_8, L_9, L_10, (bool)1, L_11, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0037:
	{
		Action_2_t4234541925 * L_12 = ___action0;
		String_t* L_13 = ___name1;
		String_t* L_14 = ___value2;
		NullCheck(L_12);
		Action_2_Invoke_m538947191(L_12, L_13, L_14, /*hidden argument*/Action_2_Invoke_m538947191_MethodInfo_var);
	}

IL_003f:
	{
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean,System.Boolean)
extern const MethodInfo* Action_2_Invoke_m538947191_MethodInfo_var;
extern const uint32_t WebHeaderCollection_doWithCheckingState_m2988265113_MetadataUsageId;
extern "C"  void WebHeaderCollection_doWithCheckingState_m2988265113 (WebHeaderCollection_t1932982249 * __this, Action_2_t4234541925 * ___action0, String_t* ___name1, String_t* ___value2, bool ___response3, bool ___setState4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_doWithCheckingState_m2988265113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebHeaderCollection_t1932982249 * G_B4_0 = NULL;
	WebHeaderCollection_t1932982249 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	WebHeaderCollection_t1932982249 * G_B5_1 = NULL;
	{
		bool L_0 = ___response3;
		WebHeaderCollection_checkState_m4278117532(__this, L_0, /*hidden argument*/NULL);
		Action_2_t4234541925 * L_1 = ___action0;
		String_t* L_2 = ___name1;
		String_t* L_3 = ___value2;
		NullCheck(L_1);
		Action_2_Invoke_m538947191(L_1, L_2, L_3, /*hidden argument*/Action_2_Invoke_m538947191_MethodInfo_var);
		bool L_4 = ___setState4;
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = __this->get__state_14();
		if (L_5)
		{
			goto IL_0036;
		}
	}
	{
		bool L_6 = ___response3;
		G_B3_0 = __this;
		if (!L_6)
		{
			G_B4_0 = __this;
			goto IL_0030;
		}
	}
	{
		G_B5_0 = 2;
		G_B5_1 = G_B3_0;
		goto IL_0031;
	}

IL_0030:
	{
		G_B5_0 = 1;
		G_B5_1 = G_B4_0;
	}

IL_0031:
	{
		NullCheck(G_B5_1);
		G_B5_1->set__state_14(G_B5_0);
	}

IL_0036:
	{
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::doWithoutCheckingName(System.Action`2<System.String,System.String>,System.String,System.String)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m538947191_MethodInfo_var;
extern const uint32_t WebHeaderCollection_doWithoutCheckingName_m1412329631_MetadataUsageId;
extern "C"  void WebHeaderCollection_doWithoutCheckingName_m1412329631 (WebHeaderCollection_t1932982249 * __this, Action_2_t4234541925 * ___action0, String_t* ___name1, String_t* ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_doWithoutCheckingName_m1412329631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name1;
		WebHeaderCollection_checkRestricted_m2386154677(__this, L_0, /*hidden argument*/NULL);
		Action_2_t4234541925 * L_1 = ___action0;
		String_t* L_2 = ___name1;
		String_t* L_3 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		String_t* L_4 = WebHeaderCollection_checkValue_m2315833202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Action_2_Invoke_m538947191(L_1, L_2, L_4, /*hidden argument*/Action_2_Invoke_m538947191_MethodInfo_var);
		return;
	}
}
// WebSocketSharp.Net.HttpHeaderInfo WebSocketSharp.Net.WebHeaderCollection::getHeaderInfo(System.String)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m3187983155_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m1394871341_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3202707919_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1984837944_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1695366630_MethodInfo_var;
extern const uint32_t WebHeaderCollection_getHeaderInfo_m3913797101_MetadataUsageId;
extern "C"  HttpHeaderInfo_t2096319561 * WebHeaderCollection_getHeaderInfo_m3913797101 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_getHeaderInfo_m3913797101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpHeaderInfo_t2096319561 * V_0 = NULL;
	Enumerator_t1402664291  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HttpHeaderInfo_t2096319561 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		Dictionary_2_t4011098823 * L_0 = ((WebHeaderCollection_t1932982249_StaticFields*)WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var->static_fields)->get__headers_12();
		NullCheck(L_0);
		ValueCollection_t2714158666 * L_1 = Dictionary_2_get_Values_m3187983155(L_0, /*hidden argument*/Dictionary_2_get_Values_m3187983155_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t1402664291  L_2 = ValueCollection_GetEnumerator_m1394871341(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m1394871341_MethodInfo_var);
		V_1 = L_2;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0036;
		}

IL_0015:
		{
			HttpHeaderInfo_t2096319561 * L_3 = Enumerator_get_Current_m3202707919((&V_1), /*hidden argument*/Enumerator_get_Current_m3202707919_MethodInfo_var);
			V_0 = L_3;
			HttpHeaderInfo_t2096319561 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = HttpHeaderInfo_get_Name_m1154984627(L_4, /*hidden argument*/NULL);
			String_t* L_6 = ___name0;
			NullCheck(L_5);
			bool L_7 = String_Equals_m1185277978(L_5, L_6, 3, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0036;
			}
		}

IL_002f:
		{
			HttpHeaderInfo_t2096319561 * L_8 = V_0;
			V_2 = L_8;
			IL2CPP_LEAVE(0x57, FINALLY_0047);
		}

IL_0036:
		{
			bool L_9 = Enumerator_MoveNext_m1984837944((&V_1), /*hidden argument*/Enumerator_MoveNext_m1984837944_MethodInfo_var);
			if (L_9)
			{
				goto IL_0015;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1695366630((&V_1), /*hidden argument*/Enumerator_Dispose_m1695366630_MethodInfo_var);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0055:
	{
		return (HttpHeaderInfo_t2096319561 *)NULL;
	}

IL_0057:
	{
		HttpHeaderInfo_t2096319561 * L_10 = V_2;
		return L_10;
	}
}
// System.Boolean WebSocketSharp.Net.WebHeaderCollection::isRestricted(System.String,System.Boolean)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_isRestricted_m1778673578_MetadataUsageId;
extern "C"  bool WebHeaderCollection_isRestricted_m1778673578 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___response1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_isRestricted_m1778673578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpHeaderInfo_t2096319561 * V_0 = NULL;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		HttpHeaderInfo_t2096319561 * L_1 = WebHeaderCollection_getHeaderInfo_m3913797101(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HttpHeaderInfo_t2096319561 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		HttpHeaderInfo_t2096319561 * L_3 = V_0;
		bool L_4 = ___response1;
		NullCheck(L_3);
		bool L_5 = HttpHeaderInfo_IsRestricted_m2648242912(L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::setWithoutCheckingName(System.String,System.String)
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern const MethodInfo* NameValueCollection_Set_m2969274643_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m925710042_MethodInfo_var;
extern const uint32_t WebHeaderCollection_setWithoutCheckingName_m1068881879_MetadataUsageId;
extern "C"  void WebHeaderCollection_setWithoutCheckingName_m1068881879 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_setWithoutCheckingName_m1068881879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)NameValueCollection_Set_m2969274643_MethodInfo_var);
		Action_2_t4234541925 * L_1 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m925710042(L_1, __this, L_0, /*hidden argument*/Action_2__ctor_m925710042_MethodInfo_var);
		String_t* L_2 = ___name0;
		String_t* L_3 = ___value1;
		WebHeaderCollection_doWithoutCheckingName_m1412329631(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.Boolean)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_InternalSet_m4257553668_MetadataUsageId;
extern "C"  void WebHeaderCollection_InternalSet_m4257553668 (WebHeaderCollection_t1932982249 * __this, String_t* ___header0, bool ___response1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_InternalSet_m4257553668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___header0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		int32_t L_1 = WebHeaderCollection_checkColonSeparated_m3792609556(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___header0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		String_t* L_4 = String_Substring_m12482732(L_2, 0, L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___header0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m2032624251(L_5, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		bool L_8 = ___response1;
		WebHeaderCollection_InternalSet_m3824004272(__this, L_4, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.String,System.Boolean)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_InternalSet_m3824004272_MetadataUsageId;
extern "C"  void WebHeaderCollection_InternalSet_m3824004272 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, bool ___response2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_InternalSet_m3824004272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		String_t* L_1 = WebHeaderCollection_checkValue_m2315833202(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___value1 = L_1;
		String_t* L_2 = ___name0;
		bool L_3 = ___response2;
		bool L_4 = WebHeaderCollection_IsMultiValue_m3564922921(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_5 = ___name0;
		String_t* L_6 = ___value1;
		NameValueCollection_Add_m263445674(__this, L_5, L_6, /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_0021:
	{
		String_t* L_7 = ___name0;
		String_t* L_8 = ___value1;
		NameValueCollection_Set_m2969274643(__this, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.Net.WebHeaderCollection::IsHeaderName(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_IsHeaderName_m1718848626_MetadataUsageId;
extern "C"  bool WebHeaderCollection_IsHeaderName_m1718848626 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_IsHeaderName_m1718848626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = ___name0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_3 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_4 = Ext_IsToken_m1069302108(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_001b;
	}

IL_001a:
	{
		G_B4_0 = 0;
	}

IL_001b:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean WebSocketSharp.Net.WebHeaderCollection::IsHeaderValue(System.String)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_IsHeaderValue_m1729483932_MetadataUsageId;
extern "C"  bool WebHeaderCollection_IsHeaderValue_m1729483932 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_IsHeaderValue_m1729483932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_1 = Ext_IsText_m4225803182(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean WebSocketSharp.Net.WebHeaderCollection::IsMultiValue(System.String,System.Boolean)
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const uint32_t WebHeaderCollection_IsMultiValue_m3564922921_MetadataUsageId;
extern "C"  bool WebHeaderCollection_IsMultiValue_m3564922921 (Il2CppObject * __this /* static, unused */, String_t* ___headerName0, bool ___response1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_IsMultiValue_m3564922921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpHeaderInfo_t2096319561 * V_0 = NULL;
	int32_t G_B6_0 = 0;
	{
		String_t* L_0 = ___headerName0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___headerName0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		String_t* L_3 = ___headerName0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		HttpHeaderInfo_t2096319561 * L_4 = WebHeaderCollection_getHeaderInfo_m3913797101(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		HttpHeaderInfo_t2096319561 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		HttpHeaderInfo_t2096319561 * L_6 = V_0;
		bool L_7 = ___response1;
		NullCheck(L_6);
		bool L_8 = HttpHeaderInfo_IsMultiValue_m1246610623(L_6, L_7, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_8));
		goto IL_002a;
	}

IL_0029:
	{
		G_B6_0 = 0;
	}

IL_002a:
	{
		return (bool)G_B6_0;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::Add(System.String,System.String)
extern "C"  void WebHeaderCollection_Add_m1096468729 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___value1;
		WebHeaderCollection_add_m3974908564(__this, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.Int32)
extern "C"  String_t* WebHeaderCollection_Get_m3063241044 (WebHeaderCollection_t1932982249 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		String_t* L_1 = NameValueCollection_Get_m861415899(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.String)
extern "C"  String_t* WebHeaderCollection_Get_m720961545 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = NameValueCollection_Get_m2509739626(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator WebSocketSharp.Net.WebHeaderCollection::GetEnumerator()
extern "C"  Il2CppObject * WebHeaderCollection_GetEnumerator_m276714436 (WebHeaderCollection_t1932982249 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = NameObjectCollectionBase_GetEnumerator_m646294968(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String WebSocketSharp.Net.WebHeaderCollection::GetKey(System.Int32)
extern "C"  String_t* WebHeaderCollection_GetKey_m2630998537 (WebHeaderCollection_t1932982249 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		String_t* L_1 = NameValueCollection_GetKey_m3871624648(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* U3CGetObjectDataU3Ec__AnonStorey1_t2338822889_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1873676830_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetObjectDataU3Ec__AnonStorey1_U3CU3Em__0_m882231296_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2068252604_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral438048066;
extern Il2CppCodeGenString* _stringLiteral630702433;
extern Il2CppCodeGenString* _stringLiteral1695958603;
extern Il2CppCodeGenString* _stringLiteral1554779067;
extern const uint32_t WebHeaderCollection_GetObjectData_m3154505602_MetadataUsageId;
extern "C"  void WebHeaderCollection_GetObjectData_m3154505602 (WebHeaderCollection_t1932982249 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_GetObjectData_m3154505602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * V_0 = NULL;
	{
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_0 = (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 *)il2cpp_codegen_object_new(U3CGetObjectDataU3Ec__AnonStorey1_t2338822889_il2cpp_TypeInfo_var);
		U3CGetObjectDataU3Ec__AnonStorey1__ctor_m4120423482(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_1 = V_0;
		SerializationInfo_t228987430 * L_2 = ___serializationInfo0;
		NullCheck(L_1);
		L_1->set_serializationInfo_0(L_2);
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_2(__this);
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_4 = V_0;
		NullCheck(L_4);
		SerializationInfo_t228987430 * L_5 = L_4->get_serializationInfo_0();
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t628810857 * L_6 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_6, _stringLiteral438048066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002a:
	{
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_7 = V_0;
		NullCheck(L_7);
		SerializationInfo_t228987430 * L_8 = L_7->get_serializationInfo_0();
		bool L_9 = __this->get__internallyUsed_13();
		NullCheck(L_8);
		SerializationInfo_AddValue_m1192926088(L_8, _stringLiteral630702433, L_9, /*hidden argument*/NULL);
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_10 = V_0;
		NullCheck(L_10);
		SerializationInfo_t228987430 * L_11 = L_10->get_serializationInfo_0();
		int32_t L_12 = __this->get__state_14();
		NullCheck(L_11);
		SerializationInfo_AddValue_m902275108(L_11, _stringLiteral1695958603, L_12, /*hidden argument*/NULL);
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_13 = V_0;
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, __this);
		NullCheck(L_13);
		L_13->set_cnt_1(L_14);
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_15 = V_0;
		NullCheck(L_15);
		SerializationInfo_t228987430 * L_16 = L_15->get_serializationInfo_0();
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_cnt_1();
		NullCheck(L_16);
		SerializationInfo_AddValue_m902275108(L_16, _stringLiteral1554779067, L_18, /*hidden argument*/NULL);
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_cnt_1();
		U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * L_21 = V_0;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)U3CGetObjectDataU3Ec__AnonStorey1_U3CU3Em__0_m882231296_MethodInfo_var);
		Action_1_t1873676830 * L_23 = (Action_1_t1873676830 *)il2cpp_codegen_object_new(Action_1_t1873676830_il2cpp_TypeInfo_var);
		Action_1__ctor_m2068252604(L_23, L_21, L_22, /*hidden argument*/Action_1__ctor_m2068252604_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_Times_m279352861(NULL /*static, unused*/, L_20, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern "C"  void WebHeaderCollection_OnDeserialization_m3543128932 (WebHeaderCollection_t1932982249 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::Set(System.String,System.String)
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern Il2CppClass* WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var;
extern const MethodInfo* WebHeaderCollection_setWithoutCheckingName_m1068881879_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m925710042_MethodInfo_var;
extern const uint32_t WebHeaderCollection_Set_m3105981884_MetadataUsageId;
extern "C"  void WebHeaderCollection_Set_m3105981884 (WebHeaderCollection_t1932982249 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_Set_m3105981884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)WebHeaderCollection_setWithoutCheckingName_m1068881879_MethodInfo_var);
		Action_2_t4234541925 * L_1 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m925710042(L_1, __this, L_0, /*hidden argument*/Action_2__ctor_m925710042_MethodInfo_var);
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(WebHeaderCollection_t1932982249_il2cpp_TypeInfo_var);
		String_t* L_3 = WebHeaderCollection_checkName_m4185892436(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___value1;
		WebHeaderCollection_doWithCheckingState_m3077687946(__this, L_1, L_3, L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocketSharp.Net.WebHeaderCollection::ToString()
extern Il2CppClass* U3CToStringU3Ec__AnonStorey2_t1254512383_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1873676830_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CToStringU3Ec__AnonStorey2_U3CU3Em__0_m1532176860_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2068252604_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern const uint32_t WebHeaderCollection_ToString_m2251909035_MetadataUsageId;
extern "C"  String_t* WebHeaderCollection_ToString_m2251909035 (WebHeaderCollection_t1932982249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebHeaderCollection_ToString_m2251909035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CToStringU3Ec__AnonStorey2_t1254512383 * V_0 = NULL;
	{
		U3CToStringU3Ec__AnonStorey2_t1254512383 * L_0 = (U3CToStringU3Ec__AnonStorey2_t1254512383 *)il2cpp_codegen_object_new(U3CToStringU3Ec__AnonStorey2_t1254512383_il2cpp_TypeInfo_var);
		U3CToStringU3Ec__AnonStorey2__ctor_m606439006(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CToStringU3Ec__AnonStorey2_t1254512383 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CToStringU3Ec__AnonStorey2_t1254512383 * L_2 = V_0;
		StringBuilder_t1221177846 * L_3 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_buff_0(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count() */, __this);
		U3CToStringU3Ec__AnonStorey2_t1254512383 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CToStringU3Ec__AnonStorey2_U3CU3Em__0_m1532176860_MethodInfo_var);
		Action_1_t1873676830 * L_7 = (Action_1_t1873676830 *)il2cpp_codegen_object_new(Action_1_t1873676830_il2cpp_TypeInfo_var);
		Action_1__ctor_m2068252604(L_7, L_5, L_6, /*hidden argument*/Action_1__ctor_m2068252604_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_Times_m279352861(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		U3CToStringU3Ec__AnonStorey2_t1254512383 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_t1221177846 * L_9 = L_8->get_buff_0();
		NullCheck(L_9);
		StringBuilder_t1221177846 * L_10 = StringBuilder_Append_m3636508479(L_9, _stringLiteral2162321587, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		return L_11;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m1194935359 (WebHeaderCollection_t1932982249 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___serializationInfo0;
		StreamingContext_t1417235061  L_1 = ___streamingContext1;
		VirtActionInvoker2< SerializationInfo_t228987430 *, StreamingContext_t1417235061  >::Invoke(13 /* System.Void System.Collections.Specialized.NameObjectCollectionBase::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext) */, __this, L_0, L_1);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::.ctor()
extern "C"  void U3CGetObjectDataU3Ec__AnonStorey1__ctor_m4120423482 (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::<>m__0(System.Int32)
extern "C"  void U3CGetObjectDataU3Ec__AnonStorey1_U3CU3Em__0_m882231296 (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * __this, int32_t ___i0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SerializationInfo_t228987430 * L_0 = __this->get_serializationInfo_0();
		String_t* L_1 = Int32_ToString_m2960866144((&___i0), /*hidden argument*/NULL);
		WebHeaderCollection_t1932982249 * L_2 = __this->get_U24this_2();
		int32_t L_3 = ___i0;
		NullCheck(L_2);
		String_t* L_4 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(20 /* System.String System.Collections.Specialized.NameValueCollection::GetKey(System.Int32) */, L_2, L_3);
		NullCheck(L_0);
		SerializationInfo_AddValue_m1740888931(L_0, L_1, L_4, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_5 = __this->get_serializationInfo_0();
		int32_t L_6 = __this->get_cnt_1();
		int32_t L_7 = ___i0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)L_7));
		String_t* L_8 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		WebHeaderCollection_t1932982249 * L_9 = __this->get_U24this_2();
		int32_t L_10 = ___i0;
		NullCheck(L_9);
		String_t* L_11 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(18 /* System.String System.Collections.Specialized.NameValueCollection::Get(System.Int32) */, L_9, L_10);
		NullCheck(L_5);
		SerializationInfo_AddValue_m1740888931(L_5, L_8, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::.ctor()
extern "C"  void U3CToStringU3Ec__AnonStorey2__ctor_m606439006 (U3CToStringU3Ec__AnonStorey2_t1254512383 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::<>m__0(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3092404974;
extern const uint32_t U3CToStringU3Ec__AnonStorey2_U3CU3Em__0_m1532176860_MetadataUsageId;
extern "C"  void U3CToStringU3Ec__AnonStorey2_U3CU3Em__0_m1532176860 (U3CToStringU3Ec__AnonStorey2_t1254512383 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CToStringU3Ec__AnonStorey2_U3CU3Em__0_m1532176860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = __this->get_buff_0();
		WebHeaderCollection_t1932982249 * L_1 = __this->get_U24this_1();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(20 /* System.String System.Collections.Specialized.NameValueCollection::GetKey(System.Int32) */, L_1, L_2);
		WebHeaderCollection_t1932982249 * L_4 = __this->get_U24this_1();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(18 /* System.String System.Collections.Specialized.NameValueCollection::Get(System.Int32) */, L_4, L_5);
		NullCheck(L_0);
		StringBuilder_AppendFormat_m759296786(L_0, _stringLiteral3092404974, L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.cctor()
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__cctor_m933897493_MetadataUsageId;
extern "C"  void PayloadData__cctor_m933897493 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__cctor_m933897493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PayloadData_t3839327312 * L_0 = (PayloadData_t3839327312 *)il2cpp_codegen_object_new(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData__ctor_m3776509826(L_0, /*hidden argument*/NULL);
		((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->set_Empty_7(L_0);
		((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->set_MaxLength_8(((int64_t)std::numeric_limits<int64_t>::max()));
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__ctor_m3776509826_MetadataUsageId;
extern "C"  void PayloadData__ctor_m3776509826 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m3776509826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set__code_0(((int32_t)1005));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__reason_5(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		__this->set__data_2(L_1);
		__this->set__codeSet_1((bool)1);
		__this->set__reasonSet_6((bool)1);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[])
extern "C"  void PayloadData__ctor_m2204175409 (PayloadData_t3839327312 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		ByteU5BU5D_t3397334013* L_1 = ___data0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		int64_t L_2 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		PayloadData__ctor_m1269236835(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Int64)
extern "C"  void PayloadData__ctor_m1269236835 (PayloadData_t3839327312 * __this, ByteU5BU5D_t3397334013* ___data0, int64_t ___length1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		__this->set__data_2(L_0);
		int64_t L_1 = ___length1;
		__this->set__length_4(L_1);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.UInt16,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__ctor_m2168673930_MetadataUsageId;
extern "C"  void PayloadData__ctor_m2168673930 (PayloadData_t3839327312 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m2168673930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	PayloadData_t3839327312 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	PayloadData_t3839327312 * G_B1_1 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		uint16_t L_0 = ___code0;
		__this->set__code_0(L_0);
		String_t* L_1 = ___reason1;
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__reason_5(G_B2_0);
		uint16_t L_4 = ___code0;
		String_t* L_5 = ___reason1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_6 = Ext_Append_m1795399003(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->set__data_2(L_6);
		ByteU5BU5D_t3397334013* L_7 = __this->get__data_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		int64_t L_8 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_7, /*hidden argument*/NULL);
		__this->set__length_4(L_8);
		__this->set__codeSet_1((bool)1);
		__this->set__reasonSet_6((bool)1);
		return;
	}
}
// System.UInt16 WebSocketSharp.PayloadData::get_Code()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_SubArray_TisByte_t3683104436_m2352342620_MethodInfo_var;
extern const uint32_t PayloadData_get_Code_m330398687_MetadataUsageId;
extern "C"  uint16_t PayloadData_get_Code_m330398687 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_get_Code_m330398687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PayloadData_t3839327312 * G_B3_0 = NULL;
	PayloadData_t3839327312 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	PayloadData_t3839327312 * G_B4_1 = NULL;
	{
		bool L_0 = __this->get__codeSet_1();
		if (L_0)
		{
			goto IL_0042;
		}
	}
	{
		int64_t L_1 = __this->get__length_4();
		G_B2_0 = __this;
		if ((((int64_t)L_1) <= ((int64_t)(((int64_t)((int64_t)1))))))
		{
			G_B3_0 = __this;
			goto IL_0031;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_2 = __this->get__data_2();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_3 = Ext_SubArray_TisByte_t3683104436_m2352342620(NULL /*static, unused*/, L_2, 0, 2, /*hidden argument*/Ext_SubArray_TisByte_t3683104436_m2352342620_MethodInfo_var);
		uint16_t L_4 = Ext_ToUInt16_m2022554856(NULL /*static, unused*/, L_3, 1, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_4));
		G_B4_1 = G_B2_0;
		goto IL_0036;
	}

IL_0031:
	{
		G_B4_0 = ((int32_t)1005);
		G_B4_1 = G_B3_0;
	}

IL_0036:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__code_0(G_B4_0);
		__this->set__codeSet_1((bool)1);
	}

IL_0042:
	{
		uint16_t L_5 = __this->get__code_0();
		return L_5;
	}
}
// System.Boolean WebSocketSharp.PayloadData::get_HasReservedCode()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData_get_HasReservedCode_m933285790_MetadataUsageId;
extern "C"  bool PayloadData_get_HasReservedCode_m933285790 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_get_HasReservedCode_m933285790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int64_t L_0 = __this->get__length_4();
		if ((((int64_t)L_0) <= ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_001a;
		}
	}
	{
		uint16_t L_1 = PayloadData_get_Code_m330398687(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_2 = Ext_IsReserved_m2404909809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		return (bool)G_B3_0;
	}
}
// System.Byte[] WebSocketSharp.PayloadData::get_ApplicationData()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_SubArray_TisByte_t3683104436_m2761078838_MethodInfo_var;
extern const uint32_t PayloadData_get_ApplicationData_m3068266889_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* PayloadData_get_ApplicationData_m3068266889 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_get_ApplicationData_m3068266889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* G_B3_0 = NULL;
	{
		int64_t L_0 = __this->get__extDataLength_3();
		if ((((int64_t)L_0) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0030;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = __this->get__data_2();
		int64_t L_2 = __this->get__extDataLength_3();
		int64_t L_3 = __this->get__length_4();
		int64_t L_4 = __this->get__extDataLength_3();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_5 = Ext_SubArray_TisByte_t3683104436_m2761078838(NULL /*static, unused*/, L_1, L_2, ((int64_t)((int64_t)L_3-(int64_t)L_4)), /*hidden argument*/Ext_SubArray_TisByte_t3683104436_m2761078838_MethodInfo_var);
		G_B3_0 = L_5;
		goto IL_0036;
	}

IL_0030:
	{
		ByteU5BU5D_t3397334013* L_6 = __this->get__data_2();
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.UInt64 WebSocketSharp.PayloadData::get_Length()
extern "C"  uint64_t PayloadData_get_Length_m4122883639 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get__length_4();
		return L_0;
	}
}
// System.Void WebSocketSharp.PayloadData::Mask(System.Byte[])
extern "C"  void PayloadData_Mask_m34083521 (PayloadData_t3839327312 * __this, ByteU5BU5D_t3397334013* ___key0, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		goto IL_0028;
	}

IL_0008:
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__data_2();
		int64_t L_1 = V_0;
		if ((int64_t)(L_1) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		ByteU5BU5D_t3397334013* L_2 = __this->get__data_2();
		int64_t L_3 = V_0;
		if ((int64_t)(L_3) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_2);
		intptr_t L_4 = (((intptr_t)L_3));
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		ByteU5BU5D_t3397334013* L_6 = ___key0;
		int64_t L_7 = V_0;
		if ((int64_t)(((int64_t)((int64_t)L_7%(int64_t)(((int64_t)((int64_t)4)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_6);
		intptr_t L_8 = (((intptr_t)((int64_t)((int64_t)L_7%(int64_t)(((int64_t)((int64_t)4)))))));
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>((((intptr_t)L_1))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_5^(int32_t)L_9))))));
		int64_t L_10 = V_0;
		V_0 = ((int64_t)((int64_t)L_10+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_0028:
	{
		int64_t L_11 = V_0;
		int64_t L_12 = __this->get__length_4();
		if ((((int64_t)L_11) < ((int64_t)L_12)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.PayloadData::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator0_t3664690781_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData_GetEnumerator_m388557599_MetadataUsageId;
extern "C"  Il2CppObject* PayloadData_GetEnumerator_m388557599 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_GetEnumerator_m388557599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator0_t3664690781_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator0__ctor_m3705914950(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * L_2 = V_0;
		return L_2;
	}
}
// System.Byte[] WebSocketSharp.PayloadData::ToArray()
extern "C"  ByteU5BU5D_t3397334013* PayloadData_ToArray_m2512776348 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__data_2();
		return L_0;
	}
}
// System.String WebSocketSharp.PayloadData::ToString()
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData_ToString_m1247159645_MetadataUsageId;
extern "C"  String_t* PayloadData_ToString_m1247159645 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_ToString_m1247159645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__data_2();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		String_t* L_1 = BitConverter_ToString_m927173850(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator WebSocketSharp.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PayloadData_System_Collections_IEnumerable_GetEnumerator_m2773702999 (PayloadData_t3839327312 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = PayloadData_GetEnumerator_m388557599(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3705914950 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4215007638 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0071;
		}
	}
	{
		goto IL_0099;
	}

IL_0021:
	{
		PayloadData_t3839327312 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = L_2->get__data_2();
		__this->set_U24locvar0_0(L_3);
		__this->set_U24locvar1_1(0);
		goto IL_007f;
	}

IL_003e:
	{
		ByteU5BU5D_t3397334013* L_4 = __this->get_U24locvar0_0();
		int32_t L_5 = __this->get_U24locvar1_1();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_U3CbU3E__1_2(L_7);
		uint8_t L_8 = __this->get_U3CbU3E__1_2();
		__this->set_U24current_4(L_8);
		bool L_9 = __this->get_U24disposing_5();
		if (L_9)
		{
			goto IL_006c;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_006c:
	{
		goto IL_009b;
	}

IL_0071:
	{
		int32_t L_10 = __this->get_U24locvar1_1();
		__this->set_U24locvar1_1(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_007f:
	{
		int32_t L_11 = __this->get_U24locvar1_1();
		ByteU5BU5D_t3397334013* L_12 = __this->get_U24locvar0_0();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_0099:
	{
		return (bool)0;
	}

IL_009b:
	{
		return (bool)1;
	}
}
// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m1567718122 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1400723954_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1400723954 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1400723954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0 = __this->get_U24current_4();
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3392630107 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2914319157_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2914319157 (U3CGetEnumeratorU3Ec__Iterator0_t3664690781 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2914319157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSharp.WebSocket::.cctor()
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* RNGCryptoServiceProvider_t2688843926_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket__cctor_m1237934464_MetadataUsageId;
extern "C"  void WebSocket__cctor_m1237934464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__cctor_m1237934464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->set__maxRetryCountForConnect_25(((int32_t)10));
		((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->set_EmptyBytes_47(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0)));
		((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->set_FragmentLength_48(((int32_t)1016));
		RNGCryptoServiceProvider_t2688843926 * L_0 = (RNGCryptoServiceProvider_t2688843926 *)il2cpp_codegen_object_new(RNGCryptoServiceProvider_t2688843926_il2cpp_TypeInfo_var);
		RNGCryptoServiceProvider__ctor_m3226360387(L_0, /*hidden argument*/NULL);
		((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->set_RandomNumber_49(L_0);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t2598199114_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2691851108_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_messagec_m2913074030_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1241046126_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral386853519;
extern Il2CppCodeGenString* _stringLiteral644180869;
extern Il2CppCodeGenString* _stringLiteral2451692261;
extern Il2CppCodeGenString* _stringLiteral3115737039;
extern const uint32_t WebSocket__ctor_m666923799_MetadataUsageId;
extern "C"  void WebSocket__ctor_m666923799 (WebSocket_t3268376029 * __this, String_t* ___url0, StringU5BU5D_t1642385972* ___protocols1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m666923799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral386853519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___url0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_4, _stringLiteral644180869, _stringLiteral386853519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		String_t* L_5 = ___url0;
		Uri_t19570940 ** L_6 = __this->get_address_of__uri_44();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_7 = Ext_TryCreateWebSocketUri_m2978564150(NULL /*static, unused*/, L_5, L_6, (&V_0), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		String_t* L_8 = V_0;
		ArgumentException_t3259014390 * L_9 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_9, L_8, _stringLiteral386853519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0051:
	{
		StringU5BU5D_t1642385972* L_10 = ___protocols1;
		if (!L_10)
		{
			goto IL_0080;
		}
	}
	{
		StringU5BU5D_t1642385972* L_11 = ___protocols1;
		NullCheck(L_11);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		StringU5BU5D_t1642385972* L_12 = ___protocols1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_13 = Ext_CheckIfValidProtocols_m4274658712(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		String_t* L_14 = V_0;
		if (!L_14)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_15 = V_0;
		ArgumentException_t3259014390 * L_16 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_16, L_15, _stringLiteral2451692261, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0079:
	{
		StringU5BU5D_t1642385972* L_17 = ___protocols1;
		__this->set__protocols_33(L_17);
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		String_t* L_18 = WebSocket_CreateBase64Key_m3247627260(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__base64Key_1(L_18);
		__this->set__client_2((bool)1);
		Logger_t2598199114 * L_19 = (Logger_t2598199114 *)il2cpp_codegen_object_new(Logger_t2598199114_il2cpp_TypeInfo_var);
		Logger__ctor_m3564207842(L_19, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set__logger_24(L_19);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)WebSocket_messagec_m2913074030_MethodInfo_var);
		Action_1_t2691851108 * L_21 = (Action_1_t2691851108 *)il2cpp_codegen_object_new(Action_1_t2691851108_il2cpp_TypeInfo_var);
		Action_1__ctor_m1241046126(L_21, __this, L_20, /*hidden argument*/Action_1__ctor_m1241046126_MethodInfo_var);
		__this->set__message_26(L_21);
		Uri_t19570940 * L_22 = __this->get__uri_44();
		NullCheck(L_22);
		String_t* L_23 = Uri_get_Scheme_m55908894(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_23, _stringLiteral3115737039, /*hidden argument*/NULL);
		__this->set__secure_40(L_24);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_25 = TimeSpan_FromSeconds_m2861206200(NULL /*static, unused*/, (5.0), /*hidden argument*/NULL);
		__this->set__waitTime_46(L_25);
		WebSocket_init_m2964322191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::get_HasMessage()
extern const MethodInfo* Queue_1_get_Count_m4241224761_MethodInfo_var;
extern const uint32_t WebSocket_get_HasMessage_m2690126053_MetadataUsageId;
extern "C"  bool WebSocket_get_HasMessage_m2690126053 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_HasMessage_m2690126053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forMessageEventQueue_12();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2709708561 * L_2 = __this->get__messageEventQueue_27();
		NullCheck(L_2);
		int32_t L_3 = Queue_1_get_Count_m4241224761(L_2, /*hidden argument*/Queue_1_get_Count_m4241224761_MethodInfo_var);
		V_1 = (bool)((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0028:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::get_SslConfiguration()
extern Il2CppClass* ClientSslConfiguration_t1159130081_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_get_SslConfiguration_m3776182150_MetadataUsageId;
extern "C"  ClientSslConfiguration_t1159130081 * WebSocket_get_SslConfiguration_m3776182150 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_SslConfiguration_m3776182150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ClientSslConfiguration_t1159130081 * V_0 = NULL;
	ClientSslConfiguration_t1159130081 * G_B3_0 = NULL;
	ClientSslConfiguration_t1159130081 * G_B2_0 = NULL;
	ClientSslConfiguration_t1159130081 * G_B5_0 = NULL;
	{
		bool L_0 = __this->get__client_2();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		ClientSslConfiguration_t1159130081 * L_1 = __this->get__sslConfig_41();
		ClientSslConfiguration_t1159130081 * L_2 = L_1;
		G_B2_0 = L_2;
		if (L_2)
		{
			G_B3_0 = L_2;
			goto IL_0031;
		}
	}
	{
		Uri_t19570940 * L_3 = __this->get__uri_44();
		NullCheck(L_3);
		String_t* L_4 = Uri_get_DnsSafeHost_m795496231(L_3, /*hidden argument*/NULL);
		ClientSslConfiguration_t1159130081 * L_5 = (ClientSslConfiguration_t1159130081 *)il2cpp_codegen_object_new(ClientSslConfiguration_t1159130081_il2cpp_TypeInfo_var);
		ClientSslConfiguration__ctor_m596200420(L_5, L_4, /*hidden argument*/NULL);
		ClientSslConfiguration_t1159130081 * L_6 = L_5;
		V_0 = L_6;
		__this->set__sslConfig_41(L_6);
		ClientSslConfiguration_t1159130081 * L_7 = V_0;
		G_B3_0 = L_7;
	}

IL_0031:
	{
		G_B5_0 = G_B3_0;
		goto IL_0037;
	}

IL_0036:
	{
		G_B5_0 = ((ClientSslConfiguration_t1159130081 *)(NULL));
	}

IL_0037:
	{
		return G_B5_0;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern Il2CppClass* EventHandler_1_t3230782241_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnClose_m963191942_MetadataUsageId;
extern "C"  void WebSocket_add_OnClose_m963191942 (WebSocket_t3268376029 * __this, EventHandler_1_t3230782241 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnClose_m963191942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3230782241 * V_0 = NULL;
	EventHandler_1_t3230782241 * V_1 = NULL;
	{
		EventHandler_1_t3230782241 * L_0 = __this->get_OnClose_50();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3230782241 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3230782241 ** L_2 = __this->get_address_of_OnClose_50();
		EventHandler_1_t3230782241 * L_3 = V_1;
		EventHandler_1_t3230782241 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3230782241 * L_6 = V_0;
		EventHandler_1_t3230782241 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3230782241 *>(L_2, ((EventHandler_1_t3230782241 *)CastclassSealed(L_5, EventHandler_1_t3230782241_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3230782241 * L_8 = V_0;
		EventHandler_1_t3230782241 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3230782241 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3230782241 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern Il2CppClass* EventHandler_1_t3230782241_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnClose_m1985956969_MetadataUsageId;
extern "C"  void WebSocket_remove_OnClose_m1985956969 (WebSocket_t3268376029 * __this, EventHandler_1_t3230782241 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnClose_m1985956969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3230782241 * V_0 = NULL;
	EventHandler_1_t3230782241 * V_1 = NULL;
	{
		EventHandler_1_t3230782241 * L_0 = __this->get_OnClose_50();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3230782241 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3230782241 ** L_2 = __this->get_address_of_OnClose_50();
		EventHandler_1_t3230782241 * L_3 = V_1;
		EventHandler_1_t3230782241 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3230782241 * L_6 = V_0;
		EventHandler_1_t3230782241 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3230782241 *>(L_2, ((EventHandler_1_t3230782241 *)CastclassSealed(L_5, EventHandler_1_t3230782241_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3230782241 * L_8 = V_0;
		EventHandler_1_t3230782241 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3230782241 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3230782241 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern Il2CppClass* EventHandler_1_t3388497467_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnError_m3284929358_MetadataUsageId;
extern "C"  void WebSocket_add_OnError_m3284929358 (WebSocket_t3268376029 * __this, EventHandler_1_t3388497467 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnError_m3284929358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3388497467 * V_0 = NULL;
	EventHandler_1_t3388497467 * V_1 = NULL;
	{
		EventHandler_1_t3388497467 * L_0 = __this->get_OnError_51();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3388497467 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3388497467 ** L_2 = __this->get_address_of_OnError_51();
		EventHandler_1_t3388497467 * L_3 = V_1;
		EventHandler_1_t3388497467 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3388497467 * L_6 = V_0;
		EventHandler_1_t3388497467 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3388497467 *>(L_2, ((EventHandler_1_t3388497467 *)CastclassSealed(L_5, EventHandler_1_t3388497467_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3388497467 * L_8 = V_0;
		EventHandler_1_t3388497467 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3388497467 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3388497467 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern Il2CppClass* EventHandler_1_t3388497467_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnError_m2985090157_MetadataUsageId;
extern "C"  void WebSocket_remove_OnError_m2985090157 (WebSocket_t3268376029 * __this, EventHandler_1_t3388497467 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnError_m2985090157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3388497467 * V_0 = NULL;
	EventHandler_1_t3388497467 * V_1 = NULL;
	{
		EventHandler_1_t3388497467 * L_0 = __this->get_OnError_51();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3388497467 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3388497467 ** L_2 = __this->get_address_of_OnError_51();
		EventHandler_1_t3388497467 * L_3 = V_1;
		EventHandler_1_t3388497467 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3388497467 * L_6 = V_0;
		EventHandler_1_t3388497467 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3388497467 *>(L_2, ((EventHandler_1_t3388497467 *)CastclassSealed(L_5, EventHandler_1_t3388497467_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3388497467 * L_8 = V_0;
		EventHandler_1_t3388497467 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3388497467 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3388497467 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern Il2CppClass* EventHandler_1_t1481358898_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnMessage_m2021047116_MetadataUsageId;
extern "C"  void WebSocket_add_OnMessage_m2021047116 (WebSocket_t3268376029 * __this, EventHandler_1_t1481358898 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnMessage_m2021047116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1481358898 * V_0 = NULL;
	EventHandler_1_t1481358898 * V_1 = NULL;
	{
		EventHandler_1_t1481358898 * L_0 = __this->get_OnMessage_52();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1481358898 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1481358898 ** L_2 = __this->get_address_of_OnMessage_52();
		EventHandler_1_t1481358898 * L_3 = V_1;
		EventHandler_1_t1481358898 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1481358898 * L_6 = V_0;
		EventHandler_1_t1481358898 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1481358898 *>(L_2, ((EventHandler_1_t1481358898 *)CastclassSealed(L_5, EventHandler_1_t1481358898_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1481358898 * L_8 = V_0;
		EventHandler_1_t1481358898 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1481358898 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1481358898 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern Il2CppClass* EventHandler_1_t1481358898_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnMessage_m2270677221_MetadataUsageId;
extern "C"  void WebSocket_remove_OnMessage_m2270677221 (WebSocket_t3268376029 * __this, EventHandler_1_t1481358898 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnMessage_m2270677221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1481358898 * V_0 = NULL;
	EventHandler_1_t1481358898 * V_1 = NULL;
	{
		EventHandler_1_t1481358898 * L_0 = __this->get_OnMessage_52();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1481358898 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1481358898 ** L_2 = __this->get_address_of_OnMessage_52();
		EventHandler_1_t1481358898 * L_3 = V_1;
		EventHandler_1_t1481358898 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1481358898 * L_6 = V_0;
		EventHandler_1_t1481358898 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1481358898 *>(L_2, ((EventHandler_1_t1481358898 *)CastclassSealed(L_5, EventHandler_1_t1481358898_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1481358898 * L_8 = V_0;
		EventHandler_1_t1481358898 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1481358898 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1481358898 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnOpen(System.EventHandler)
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnOpen_m924055045_MetadataUsageId;
extern "C"  void WebSocket_add_OnOpen_m924055045 (WebSocket_t3268376029 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnOpen_m924055045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t277755526 * V_0 = NULL;
	EventHandler_t277755526 * V_1 = NULL;
	{
		EventHandler_t277755526 * L_0 = __this->get_OnOpen_53();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t277755526 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t277755526 ** L_2 = __this->get_address_of_OnOpen_53();
		EventHandler_t277755526 * L_3 = V_1;
		EventHandler_t277755526 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t277755526 * L_6 = V_0;
		EventHandler_t277755526 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t277755526 *>(L_2, ((EventHandler_t277755526 *)CastclassSealed(L_5, EventHandler_t277755526_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t277755526 * L_8 = V_0;
		EventHandler_t277755526 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t277755526 *)L_8) == ((Il2CppObject*)(EventHandler_t277755526 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnOpen(System.EventHandler)
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnOpen_m3980689398_MetadataUsageId;
extern "C"  void WebSocket_remove_OnOpen_m3980689398 (WebSocket_t3268376029 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnOpen_m3980689398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t277755526 * V_0 = NULL;
	EventHandler_t277755526 * V_1 = NULL;
	{
		EventHandler_t277755526 * L_0 = __this->get_OnOpen_53();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t277755526 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t277755526 ** L_2 = __this->get_address_of_OnOpen_53();
		EventHandler_t277755526 * L_3 = V_1;
		EventHandler_t277755526 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t277755526 * L_6 = V_0;
		EventHandler_t277755526 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t277755526 *>(L_2, ((EventHandler_t277755526 *)CastclassSealed(L_5, EventHandler_t277755526_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t277755526 * L_8 = V_0;
		EventHandler_t277755526 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t277755526 *)L_8) == ((Il2CppObject*)(EventHandler_t277755526 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::checkHandshakeResponse(WebSocketSharp.HttpResponse,System.String&)
extern Il2CppCodeGenString* _stringLiteral2409755735;
extern Il2CppCodeGenString* _stringLiteral2092397585;
extern Il2CppCodeGenString* _stringLiteral245632835;
extern Il2CppCodeGenString* _stringLiteral3689292460;
extern Il2CppCodeGenString* _stringLiteral1643304268;
extern Il2CppCodeGenString* _stringLiteral566102924;
extern Il2CppCodeGenString* _stringLiteral4045660976;
extern Il2CppCodeGenString* _stringLiteral22895698;
extern Il2CppCodeGenString* _stringLiteral1568602642;
extern Il2CppCodeGenString* _stringLiteral2444067936;
extern Il2CppCodeGenString* _stringLiteral2794455674;
extern const uint32_t WebSocket_checkHandshakeResponse_m3220347755_MetadataUsageId;
extern "C"  bool WebSocket_checkHandshakeResponse_m3220347755 (WebSocket_t3268376029 * __this, HttpResponse_t2820540315 * ___response0, String_t** ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkHandshakeResponse_m3220347755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NameValueCollection_t3047564564 * V_0 = NULL;
	{
		String_t** L_0 = ___message1;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		HttpResponse_t2820540315 * L_1 = ___response0;
		NullCheck(L_1);
		bool L_2 = HttpResponse_get_IsRedirect_m3194810376(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		String_t** L_3 = ___message1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)_stringLiteral2409755735;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)_stringLiteral2409755735);
		return (bool)0;
	}

IL_0017:
	{
		HttpResponse_t2820540315 * L_4 = ___response0;
		NullCheck(L_4);
		bool L_5 = HttpResponse_get_IsUnauthorized_m1649884238(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		String_t** L_6 = ___message1;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)_stringLiteral2092397585;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)_stringLiteral2092397585);
		return (bool)0;
	}

IL_002b:
	{
		HttpResponse_t2820540315 * L_7 = ___response0;
		NullCheck(L_7);
		bool L_8 = HttpResponse_get_IsWebSocketResponse_m3251240054(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003f;
		}
	}
	{
		String_t** L_9 = ___message1;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)_stringLiteral245632835;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)_stringLiteral245632835);
		return (bool)0;
	}

IL_003f:
	{
		HttpResponse_t2820540315 * L_10 = ___response0;
		NullCheck(L_10);
		NameValueCollection_t3047564564 * L_11 = HttpBase_get_Headers_m1195641886(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		NameValueCollection_t3047564564 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = NameValueCollection_get_Item_m2776418562(L_12, _stringLiteral3689292460, /*hidden argument*/NULL);
		bool L_14 = WebSocket_validateSecWebSocketAcceptHeader_m1256291114(__this, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0065;
		}
	}
	{
		String_t** L_15 = ___message1;
		*((Il2CppObject **)(L_15)) = (Il2CppObject *)_stringLiteral1643304268;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_15), (Il2CppObject *)_stringLiteral1643304268);
		return (bool)0;
	}

IL_0065:
	{
		NameValueCollection_t3047564564 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = NameValueCollection_get_Item_m2776418562(L_16, _stringLiteral566102924, /*hidden argument*/NULL);
		bool L_18 = WebSocket_validateSecWebSocketProtocolServerHeader_m2442699293(__this, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0084;
		}
	}
	{
		String_t** L_19 = ___message1;
		*((Il2CppObject **)(L_19)) = (Il2CppObject *)_stringLiteral4045660976;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_19), (Il2CppObject *)_stringLiteral4045660976);
		return (bool)0;
	}

IL_0084:
	{
		NameValueCollection_t3047564564 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = NameValueCollection_get_Item_m2776418562(L_20, _stringLiteral22895698, /*hidden argument*/NULL);
		bool L_22 = WebSocket_validateSecWebSocketExtensionsServerHeader_m1069285281(__this, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00a3;
		}
	}
	{
		String_t** L_23 = ___message1;
		*((Il2CppObject **)(L_23)) = (Il2CppObject *)_stringLiteral1568602642;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_23), (Il2CppObject *)_stringLiteral1568602642);
		return (bool)0;
	}

IL_00a3:
	{
		NameValueCollection_t3047564564 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = NameValueCollection_get_Item_m2776418562(L_24, _stringLiteral2444067936, /*hidden argument*/NULL);
		bool L_26 = WebSocket_validateSecWebSocketVersionServerHeader_m936281861(__this, L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00c2;
		}
	}
	{
		String_t** L_27 = ___message1;
		*((Il2CppObject **)(L_27)) = (Il2CppObject *)_stringLiteral2794455674;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_27), (Il2CppObject *)_stringLiteral2794455674);
		return (bool)0;
	}

IL_00c2:
	{
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern Il2CppCodeGenString* _stringLiteral3940188790;
extern Il2CppCodeGenString* _stringLiteral3160518568;
extern Il2CppCodeGenString* _stringLiteral2777180271;
extern Il2CppCodeGenString* _stringLiteral664913176;
extern const uint32_t WebSocket_checkIfAvailable_m4114200801_MetadataUsageId;
extern "C"  bool WebSocket_checkIfAvailable_m4114200801 (WebSocket_t3268376029 * __this, bool ___connecting0, bool ___open1, bool ___closing2, bool ___closed3, String_t** ___message4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkIfAvailable_m4114200801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t** L_0 = ___message4;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		bool L_1 = ___connecting0;
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		uint16_t L_2 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t** L_3 = ___message4;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)_stringLiteral3940188790;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)_stringLiteral3940188790);
		return (bool)0;
	}

IL_0021:
	{
		bool L_4 = ___open1;
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		uint16_t L_5 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_003f;
		}
	}
	{
		String_t** L_6 = ___message4;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)_stringLiteral3160518568;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)_stringLiteral3160518568);
		return (bool)0;
	}

IL_003f:
	{
		bool L_7 = ___closing2;
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		uint16_t L_8 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_005d;
		}
	}
	{
		String_t** L_9 = ___message4;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)_stringLiteral2777180271;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)_stringLiteral2777180271);
		return (bool)0;
	}

IL_005d:
	{
		bool L_10 = ___closed3;
		if (L_10)
		{
			goto IL_007c;
		}
	}
	{
		uint16_t L_11 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_11) == ((uint32_t)3))))
		{
			goto IL_007c;
		}
	}
	{
		String_t** L_12 = ___message4;
		*((Il2CppObject **)(L_12)) = (Il2CppObject *)_stringLiteral664913176;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_12), (Il2CppObject *)_stringLiteral664913176);
		return (bool)0;
	}

IL_007c:
	{
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern Il2CppCodeGenString* _stringLiteral3675863005;
extern Il2CppCodeGenString* _stringLiteral3779960737;
extern const uint32_t WebSocket_checkIfAvailable_m2802667577_MetadataUsageId;
extern "C"  bool WebSocket_checkIfAvailable_m2802667577 (WebSocket_t3268376029 * __this, bool ___client0, bool ___server1, bool ___connecting2, bool ___open3, bool ___closing4, bool ___closed5, String_t** ___message6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkIfAvailable_m2802667577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t** L_0 = ___message6;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		bool L_1 = ___client0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = __this->get__client_2();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		String_t** L_3 = ___message6;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)_stringLiteral3675863005;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)_stringLiteral3675863005);
		return (bool)0;
	}

IL_001f:
	{
		bool L_4 = ___server1;
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		bool L_5 = __this->get__client_2();
		if (L_5)
		{
			goto IL_003a;
		}
	}
	{
		String_t** L_6 = ___message6;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)_stringLiteral3779960737;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)_stringLiteral3779960737);
		return (bool)0;
	}

IL_003a:
	{
		bool L_7 = ___connecting2;
		bool L_8 = ___open3;
		bool L_9 = ___closing4;
		bool L_10 = ___closed5;
		String_t** L_11 = ___message6;
		bool L_12 = WebSocket_checkIfAvailable_m4114200801(__this, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean WebSocketSharp.WebSocket::checkReceivedFrame(WebSocketSharp.WebSocketFrame,System.String&)
extern Il2CppCodeGenString* _stringLiteral167751887;
extern Il2CppCodeGenString* _stringLiteral2348175174;
extern Il2CppCodeGenString* _stringLiteral2969603945;
extern Il2CppCodeGenString* _stringLiteral1243698582;
extern Il2CppCodeGenString* _stringLiteral2167795346;
extern Il2CppCodeGenString* _stringLiteral3513718295;
extern const uint32_t WebSocket_checkReceivedFrame_m3485348324_MetadataUsageId;
extern "C"  bool WebSocket_checkReceivedFrame_m3485348324 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, String_t** ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkReceivedFrame_m3485348324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		String_t** L_0 = ___message1;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		WebSocketFrame_t764750278 * L_1 = ___frame0;
		NullCheck(L_1);
		bool L_2 = WebSocketFrame_get_IsMasked_m373012606(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = __this->get__client_2();
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		String_t** L_5 = ___message1;
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)_stringLiteral167751887;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)_stringLiteral167751887);
		return (bool)0;
	}

IL_0024:
	{
		bool L_6 = __this->get__client_2();
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		String_t** L_8 = ___message1;
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)_stringLiteral2348175174;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)_stringLiteral2348175174);
		return (bool)0;
	}

IL_003e:
	{
		bool L_9 = __this->get__inContinuation_22();
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		WebSocketFrame_t764750278 * L_10 = ___frame0;
		NullCheck(L_10);
		bool L_11 = WebSocketFrame_get_IsData_m180465203(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005d;
		}
	}
	{
		String_t** L_12 = ___message1;
		*((Il2CppObject **)(L_12)) = (Il2CppObject *)_stringLiteral2969603945;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_12), (Il2CppObject *)_stringLiteral2969603945);
		return (bool)0;
	}

IL_005d:
	{
		WebSocketFrame_t764750278 * L_13 = ___frame0;
		NullCheck(L_13);
		bool L_14 = WebSocketFrame_get_IsCompressed_m2728001770(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		uint8_t L_15 = __this->get__compression_4();
		if (L_15)
		{
			goto IL_007c;
		}
	}
	{
		String_t** L_16 = ___message1;
		*((Il2CppObject **)(L_16)) = (Il2CppObject *)_stringLiteral1243698582;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_16), (Il2CppObject *)_stringLiteral1243698582);
		return (bool)0;
	}

IL_007c:
	{
		WebSocketFrame_t764750278 * L_17 = ___frame0;
		NullCheck(L_17);
		uint8_t L_18 = WebSocketFrame_get_Rsv2_m3102243471(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_0091;
		}
	}
	{
		String_t** L_19 = ___message1;
		*((Il2CppObject **)(L_19)) = (Il2CppObject *)_stringLiteral2167795346;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_19), (Il2CppObject *)_stringLiteral2167795346);
		return (bool)0;
	}

IL_0091:
	{
		WebSocketFrame_t764750278 * L_20 = ___frame0;
		NullCheck(L_20);
		uint8_t L_21 = WebSocketFrame_get_Rsv3_m353157140(L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_00a6;
		}
	}
	{
		String_t** L_22 = ___message1;
		*((Il2CppObject **)(L_22)) = (Il2CppObject *)_stringLiteral3513718295;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_22), (Il2CppObject *)_stringLiteral3513718295);
		return (bool)0;
	}

IL_00a6:
	{
		return (bool)1;
	}
}
// System.Void WebSocketSharp.WebSocket::close(System.UInt16,System.String)
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1923737138;
extern Il2CppCodeGenString* _stringLiteral3766899547;
extern const uint32_t WebSocket_close_m712187051_MetadataUsageId;
extern "C"  void WebSocket_close_m712187051 (WebSocket_t3268376029 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_close_m712187051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		uint16_t L_0 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0021;
		}
	}
	{
		Logger_t2598199114 * L_1 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_1);
		Logger_Info_m2694160494(L_1, _stringLiteral1923737138, /*hidden argument*/NULL);
		return;
	}

IL_0021:
	{
		uint16_t L_2 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_0042;
		}
	}
	{
		Logger_t2598199114 * L_3 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_3);
		Logger_Info_m2694160494(L_3, _stringLiteral3766899547, /*hidden argument*/NULL);
		return;
	}

IL_0042:
	{
		uint16_t L_4 = ___code0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)1005)))))
		{
			goto IL_005c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData_t3839327312 * L_5 = ((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->get_Empty_7();
		WebSocket_close_m3666496073(__this, L_5, (bool)1, (bool)1, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_005c:
	{
		uint16_t L_6 = ___code0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_7 = Ext_IsReserved_m2404909809(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		uint16_t L_8 = ___code0;
		String_t* L_9 = ___reason1;
		PayloadData_t3839327312 * L_10 = (PayloadData_t3839327312 *)il2cpp_codegen_object_new(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData__ctor_m2168673930(L_10, L_8, L_9, /*hidden argument*/NULL);
		bool L_11 = V_0;
		bool L_12 = V_0;
		WebSocket_close_m3666496073(__this, L_10, L_11, L_12, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern Il2CppClass* CloseEventArgs_t344507773_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Emit_TisCloseEventArgs_t344507773_m464613464_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1923737138;
extern Il2CppCodeGenString* _stringLiteral3766899547;
extern Il2CppCodeGenString* _stringLiteral3120871241;
extern Il2CppCodeGenString* _stringLiteral4096714493;
extern Il2CppCodeGenString* _stringLiteral1687154973;
extern const uint32_t WebSocket_close_m3666496073_MetadataUsageId;
extern "C"  void WebSocket_close_m3666496073 (WebSocket_t3268376029 * __this, PayloadData_t3839327312 * ___payloadData0, bool ___send1, bool ___receive2, bool ___received3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_close_m3666496073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	CloseEventArgs_t344507773 * V_2 = NULL;
	Exception_t1927440687 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B8_0 = 0;
	int32_t G_B11_0 = 0;
	{
		Il2CppObject * L_0 = __this->get__forState_15();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_2) == ((uint32_t)2))))
			{
				goto IL_0032;
			}
		}

IL_001b:
		{
			Logger_t2598199114 * L_3 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_3);
			Logger_Info_m2694160494(L_3, _stringLiteral1923737138, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x112, FINALLY_0087);
		}

IL_0032:
		{
			uint16_t L_4 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_4) == ((uint32_t)3))))
			{
				goto IL_0057;
			}
		}

IL_0040:
		{
			Logger_t2598199114 * L_5 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_5);
			Logger_Info_m2694160494(L_5, _stringLiteral3766899547, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x112, FINALLY_0087);
		}

IL_0057:
		{
			bool L_6 = ___send1;
			if (!L_6)
			{
				goto IL_006a;
			}
		}

IL_005d:
		{
			uint16_t L_7 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			G_B8_0 = ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
			goto IL_006b;
		}

IL_006a:
		{
			G_B8_0 = 0;
		}

IL_006b:
		{
			___send1 = (bool)G_B8_0;
			bool L_8 = ___send1;
			if (!L_8)
			{
				goto IL_0076;
			}
		}

IL_0073:
		{
			bool L_9 = ___receive2;
			G_B11_0 = ((int32_t)(L_9));
			goto IL_0077;
		}

IL_0076:
		{
			G_B11_0 = 0;
		}

IL_0077:
		{
			___receive2 = (bool)G_B11_0;
			il2cpp_codegen_memory_barrier();
			__this->set__readyState_37(2);
			IL2CPP_LEAVE(0x8E, FINALLY_0087);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0087;
	}

FINALLY_0087:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(135)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(135)
	{
		IL2CPP_JUMP_TBL(0x112, IL_0112)
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008e:
	{
		Logger_t2598199114 * L_11 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_11);
		Logger_Trace_m763429895(L_11, _stringLiteral3120871241, /*hidden argument*/NULL);
		PayloadData_t3839327312 * L_12 = ___payloadData0;
		bool L_13 = ___send1;
		bool L_14 = ___receive2;
		bool L_15 = ___received3;
		bool L_16 = WebSocket_closeHandshake_m3651859090(__this, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		WebSocket_releaseResources_m4163756605(__this, /*hidden argument*/NULL);
		Logger_t2598199114 * L_17 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_17);
		Logger_Trace_m763429895(L_17, _stringLiteral4096714493, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_37(3);
		PayloadData_t3839327312 * L_18 = ___payloadData0;
		CloseEventArgs_t344507773 * L_19 = (CloseEventArgs_t344507773 *)il2cpp_codegen_object_new(CloseEventArgs_t344507773_il2cpp_TypeInfo_var);
		CloseEventArgs__ctor_m271874174(L_19, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		CloseEventArgs_t344507773 * L_20 = V_2;
		bool L_21 = V_1;
		NullCheck(L_20);
		CloseEventArgs_set_WasClean_m319124275(L_20, L_21, /*hidden argument*/NULL);
	}

IL_00db:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t3230782241 * L_22 = __this->get_OnClose_50();
		CloseEventArgs_t344507773 * L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_Emit_TisCloseEventArgs_t344507773_m464613464(NULL /*static, unused*/, L_22, __this, L_23, /*hidden argument*/Ext_Emit_TisCloseEventArgs_t344507773_m464613464_MethodInfo_var);
		goto IL_0112;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ed;
		throw e;
	}

CATCH_00ed:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1927440687 *)__exception_local);
		Logger_t2598199114 * L_24 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		Exception_t1927440687 * L_25 = V_3;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		NullCheck(L_24);
		Logger_Error_m3326994786(L_24, L_26, /*hidden argument*/NULL);
		Exception_t1927440687 * L_27 = V_3;
		WebSocket_error_m3171452371(__this, _stringLiteral1687154973, L_27, /*hidden argument*/NULL);
		goto IL_0112;
	} // end catch (depth: 1)

IL_0112:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::closeHandshake(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3875350291;
extern const uint32_t WebSocket_closeHandshake_m3651859090_MetadataUsageId;
extern "C"  bool WebSocket_closeHandshake_m3651859090 (WebSocket_t3268376029 * __this, PayloadData_t3839327312 * ___payloadData0, bool ___send1, bool ___receive2, bool ___received3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_closeHandshake_m3651859090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	WebSocketFrame_t764750278 * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	{
		V_0 = (bool)0;
		bool L_0 = ___send1;
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		PayloadData_t3839327312 * L_1 = ___payloadData0;
		bool L_2 = __this->get__client_2();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_t764750278 * L_3 = WebSocketFrame_CreateCloseFrame_m2686217177(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		WebSocketFrame_t764750278 * L_4 = V_1;
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_5 = WebSocketFrame_ToArray_m4086645856(L_4, /*hidden argument*/NULL);
		bool L_6 = WebSocket_sendBytes_m1170524623(__this, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		bool L_7 = __this->get__client_2();
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		WebSocketFrame_t764750278 * L_8 = V_1;
		NullCheck(L_8);
		WebSocketFrame_Unmask_m217551411(L_8, /*hidden argument*/NULL);
	}

IL_0033:
	{
		bool L_9 = ___received3;
		if (L_9)
		{
			goto IL_0054;
		}
	}
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0054;
		}
	}
	{
		bool L_11 = ___receive2;
		if (!L_11)
		{
			goto IL_0054;
		}
	}
	{
		ManualResetEvent_t926074657 * L_12 = __this->get__receivingExited_38();
		G_B8_0 = ((((int32_t)((((Il2CppObject*)(ManualResetEvent_t926074657 *)L_12) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0054:
	{
		G_B8_0 = 0;
	}

IL_0055:
	{
		V_2 = (bool)G_B8_0;
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_006f;
		}
	}
	{
		ManualResetEvent_t926074657 * L_14 = __this->get__receivingExited_38();
		TimeSpan_t3430258949  L_15 = __this->get__waitTime_46();
		NullCheck(L_14);
		bool L_16 = VirtFuncInvoker1< bool, TimeSpan_t3430258949  >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.TimeSpan) */, L_14, L_15);
		___received3 = L_16;
	}

IL_006f:
	{
		bool L_17 = V_0;
		if (!L_17)
		{
			goto IL_0079;
		}
	}
	{
		bool L_18 = ___received3;
		G_B13_0 = ((int32_t)(L_18));
		goto IL_007a;
	}

IL_0079:
	{
		G_B13_0 = 0;
	}

IL_007a:
	{
		V_3 = (bool)G_B13_0;
		Logger_t2598199114 * L_19 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		bool L_20 = V_3;
		bool L_21 = L_20;
		Il2CppObject * L_22 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_21);
		bool L_23 = V_0;
		bool L_24 = L_23;
		Il2CppObject * L_25 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_24);
		bool L_26 = ___received3;
		bool L_27 = L_26;
		Il2CppObject * L_28 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral3875350291, L_22, L_25, L_28, /*hidden argument*/NULL);
		NullCheck(L_19);
		Logger_Debug_m3721126547(L_19, L_29, /*hidden argument*/NULL);
		bool L_30 = V_3;
		return L_30;
	}
}
// System.Boolean WebSocketSharp.WebSocket::connect()
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral485548041;
extern Il2CppCodeGenString* _stringLiteral998968157;
extern Il2CppCodeGenString* _stringLiteral2609640190;
extern const uint32_t WebSocket_connect_m3298158743_MetadataUsageId;
extern "C"  bool WebSocket_connect_m3298158743 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_connect_m3298158743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	Exception_t1927440687 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forState_15();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = WebSocket_checkIfAvailable_m4114200801(__this, (bool)1, (bool)0, (bool)0, (bool)1, (&V_1), /*hidden argument*/NULL);
			if (L_2)
			{
				goto IL_003f;
			}
		}

IL_001e:
		{
			Logger_t2598199114 * L_3 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			String_t* L_4 = V_1;
			NullCheck(L_3);
			Logger_Error_m3326994786(L_3, L_4, /*hidden argument*/NULL);
			WebSocket_error_m3171452371(__this, _stringLiteral485548041, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
			V_2 = (bool)0;
			IL2CPP_LEAVE(0xD6, FINALLY_00cf);
		}

IL_003f:
		{
			int32_t L_5 = __this->get__retryCountForConnect_39();
			IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
			int32_t L_6 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get__maxRetryCountForConnect_25();
			if ((((int32_t)L_5) <= ((int32_t)L_6)))
			{
				goto IL_006f;
			}
		}

IL_004f:
		{
			__this->set__retryCountForConnect_39(0);
			Logger_t2598199114 * L_7 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_7);
			Logger_Fatal_m3177508194(L_7, _stringLiteral998968157, /*hidden argument*/NULL);
			V_2 = (bool)0;
			IL2CPP_LEAVE(0xD6, FINALLY_00cf);
		}

IL_006f:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__readyState_37(0);
		}

IL_0078:
		try
		{ // begin try (depth: 2)
			WebSocket_doHandshake_m244091241(__this, /*hidden argument*/NULL);
			goto IL_00b8;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0083;
			throw e;
		}

CATCH_0083:
		{ // begin catch(System.Exception)
			V_3 = ((Exception_t1927440687 *)__exception_local);
			int32_t L_8 = __this->get__retryCountForConnect_39();
			__this->set__retryCountForConnect_39(((int32_t)((int32_t)L_8+(int32_t)1)));
			Logger_t2598199114 * L_9 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			Exception_t1927440687 * L_10 = V_3;
			NullCheck(L_10);
			String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
			NullCheck(L_9);
			Logger_Fatal_m3177508194(L_9, L_11, /*hidden argument*/NULL);
			Exception_t1927440687 * L_12 = V_3;
			WebSocket_fatal_m2341174381(__this, _stringLiteral2609640190, L_12, /*hidden argument*/NULL);
			V_2 = (bool)0;
			IL2CPP_LEAVE(0xD6, FINALLY_00cf);
		} // end catch (depth: 2)

IL_00b8:
		{
			__this->set__retryCountForConnect_39(1);
			il2cpp_codegen_memory_barrier();
			__this->set__readyState_37(1);
			V_2 = (bool)1;
			IL2CPP_LEAVE(0xD6, FINALLY_00cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00cf;
	}

FINALLY_00cf:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(207)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(207)
	{
		IL2CPP_JUMP_TBL(0xD6, IL_00d6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00d6:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
// System.String WebSocketSharp.WebSocket::createExtensions()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral696342149;
extern Il2CppCodeGenString* _stringLiteral1865761409;
extern Il2CppCodeGenString* _stringLiteral2548781888;
extern const uint32_t WebSocket_createExtensions_m1782492306_MetadataUsageId;
extern "C"  String_t* WebSocket_createExtensions_m1782492306 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_createExtensions_m1782492306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)80), /*hidden argument*/NULL);
		V_0 = L_0;
		uint8_t L_1 = __this->get__compression_4();
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		uint8_t L_2 = __this->get__compression_4();
		StringU5BU5D_t1642385972* L_3 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral696342149);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral696342149);
		StringU5BU5D_t1642385972* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral1865761409);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1865761409);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_5 = Ext_ToExtensionString_m2837890601(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringBuilder_t1221177846 * L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		StringBuilder_AppendFormat_m3265503696(L_6, _stringLiteral2548781888, L_7, /*hidden argument*/NULL);
	}

IL_0042:
	{
		StringBuilder_t1221177846 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = StringBuilder_get_Length_m1608241323(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) <= ((int32_t)2)))
		{
			goto IL_0060;
		}
	}
	{
		StringBuilder_t1221177846 * L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		StringBuilder_set_Length_m3039225444(L_11, ((int32_t)((int32_t)L_12-(int32_t)2)), /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		return L_14;
	}

IL_0060:
	{
		return (String_t*)NULL;
	}
}
// WebSocketSharp.HttpRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_ToString_TisString_t_m1979115242_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3886465728;
extern Il2CppCodeGenString* _stringLiteral347639339;
extern Il2CppCodeGenString* _stringLiteral566102924;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral22895698;
extern Il2CppCodeGenString* _stringLiteral2444067936;
extern Il2CppCodeGenString* _stringLiteral1664928706;
extern Il2CppCodeGenString* _stringLiteral2313763943;
extern const uint32_t WebSocket_createHandshakeRequest_m4128701898_MetadataUsageId;
extern "C"  HttpRequest_t1845443631 * WebSocket_createHandshakeRequest_m4128701898 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_createHandshakeRequest_m4128701898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpRequest_t1845443631 * V_0 = NULL;
	NameValueCollection_t3047564564 * V_1 = NULL;
	AuthenticationResponse_t1212723231 * V_2 = NULL;
	{
		Uri_t19570940 * L_0 = __this->get__uri_44();
		HttpRequest_t1845443631 * L_1 = HttpRequest_CreateWebSocketRequest_m3478151593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HttpRequest_t1845443631 * L_2 = V_0;
		NullCheck(L_2);
		NameValueCollection_t3047564564 * L_3 = HttpBase_get_Headers_m1195641886(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = __this->get__origin_29();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_5 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		NameValueCollection_t3047564564 * L_6 = V_1;
		String_t* L_7 = __this->get__origin_29();
		NullCheck(L_6);
		NameValueCollection_set_Item_m3775607929(L_6, _stringLiteral3886465728, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		NameValueCollection_t3047564564 * L_8 = V_1;
		String_t* L_9 = __this->get__base64Key_1();
		NullCheck(L_8);
		NameValueCollection_set_Item_m3775607929(L_8, _stringLiteral347639339, L_9, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_10 = __this->get__protocols_33();
		__this->set__protocolsRequested_34((bool)((((int32_t)((((Il2CppObject*)(StringU5BU5D_t1642385972*)L_10) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		bool L_11 = __this->get__protocolsRequested_34();
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		NameValueCollection_t3047564564 * L_12 = V_1;
		StringU5BU5D_t1642385972* L_13 = __this->get__protocols_33();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_14 = Ext_ToString_TisString_t_m1979115242(NULL /*static, unused*/, L_13, _stringLiteral811305474, /*hidden argument*/Ext_ToString_TisString_t_m1979115242_MethodInfo_var);
		NullCheck(L_12);
		NameValueCollection_set_Item_m3775607929(L_12, _stringLiteral566102924, L_14, /*hidden argument*/NULL);
	}

IL_007d:
	{
		uint8_t L_15 = __this->get__compression_4();
		__this->set__extensionsRequested_11((bool)((((int32_t)((((int32_t)L_15) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		bool L_16 = __this->get__extensionsRequested_11();
		if (!L_16)
		{
			goto IL_00ab;
		}
	}
	{
		NameValueCollection_t3047564564 * L_17 = V_1;
		String_t* L_18 = WebSocket_createExtensions_m1782492306(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		NameValueCollection_set_Item_m3775607929(L_17, _stringLiteral22895698, L_18, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		NameValueCollection_t3047564564 * L_19 = V_1;
		NullCheck(L_19);
		NameValueCollection_set_Item_m3775607929(L_19, _stringLiteral2444067936, _stringLiteral1664928706, /*hidden argument*/NULL);
		V_2 = (AuthenticationResponse_t1212723231 *)NULL;
		AuthenticationChallenge_t1146723439 * L_20 = __this->get__authChallenge_0();
		if (!L_20)
		{
			goto IL_00fc;
		}
	}
	{
		NetworkCredential_t3911206805 * L_21 = __this->get__credentials_7();
		if (!L_21)
		{
			goto IL_00fc;
		}
	}
	{
		AuthenticationChallenge_t1146723439 * L_22 = __this->get__authChallenge_0();
		NetworkCredential_t3911206805 * L_23 = __this->get__credentials_7();
		uint32_t L_24 = __this->get__nonceCount_28();
		AuthenticationResponse_t1212723231 * L_25 = (AuthenticationResponse_t1212723231 *)il2cpp_codegen_object_new(AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m3805970484(L_25, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		AuthenticationResponse_t1212723231 * L_26 = V_2;
		NullCheck(L_26);
		uint32_t L_27 = AuthenticationResponse_get_NonceCount_m2749455448(L_26, /*hidden argument*/NULL);
		__this->set__nonceCount_28(L_27);
		goto IL_0113;
	}

IL_00fc:
	{
		bool L_28 = __this->get__preAuth_31();
		if (!L_28)
		{
			goto IL_0113;
		}
	}
	{
		NetworkCredential_t3911206805 * L_29 = __this->get__credentials_7();
		AuthenticationResponse_t1212723231 * L_30 = (AuthenticationResponse_t1212723231 *)il2cpp_codegen_object_new(AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m1022749245(L_30, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
	}

IL_0113:
	{
		AuthenticationResponse_t1212723231 * L_31 = V_2;
		if (!L_31)
		{
			goto IL_012a;
		}
	}
	{
		NameValueCollection_t3047564564 * L_32 = V_1;
		AuthenticationResponse_t1212723231 * L_33 = V_2;
		NullCheck(L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_33);
		NullCheck(L_32);
		NameValueCollection_set_Item_m3775607929(L_32, _stringLiteral2313763943, L_34, /*hidden argument*/NULL);
	}

IL_012a:
	{
		CookieCollection_t4248997468 * L_35 = __this->get__cookies_6();
		NullCheck(L_35);
		int32_t L_36 = CookieCollection_get_Count_m3590414075(L_35, /*hidden argument*/NULL);
		if ((((int32_t)L_36) <= ((int32_t)0)))
		{
			goto IL_0147;
		}
	}
	{
		HttpRequest_t1845443631 * L_37 = V_0;
		CookieCollection_t4248997468 * L_38 = __this->get__cookies_6();
		NullCheck(L_37);
		HttpRequest_SetCookies_m2179182154(L_37, L_38, /*hidden argument*/NULL);
	}

IL_0147:
	{
		HttpRequest_t1845443631 * L_39 = V_0;
		return L_39;
	}
}
// System.Void WebSocketSharp.WebSocket::doHandshake()
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral566102924;
extern Il2CppCodeGenString* _stringLiteral22895698;
extern const uint32_t WebSocket_doHandshake_m244091241_MetadataUsageId;
extern "C"  void WebSocket_doHandshake_m244091241 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_doHandshake_m244091241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpResponse_t2820540315 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		WebSocket_setClientStream_m3923254386(__this, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_0 = WebSocket_sendHandshakeRequest_m1722434510(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		HttpResponse_t2820540315 * L_1 = V_0;
		bool L_2 = WebSocket_checkHandshakeResponse_m3220347755(__this, L_1, (&V_1), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = V_1;
		WebSocketException_t1348391352 * L_4 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3554051322(L_4, ((int32_t)1002), L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		bool L_5 = __this->get__protocolsRequested_34();
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		HttpResponse_t2820540315 * L_6 = V_0;
		NullCheck(L_6);
		NameValueCollection_t3047564564 * L_7 = HttpBase_get_Headers_m1195641886(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = NameValueCollection_get_Item_m2776418562(L_7, _stringLiteral566102924, /*hidden argument*/NULL);
		__this->set__protocol_32(L_8);
	}

IL_0048:
	{
		bool L_9 = __this->get__extensionsRequested_11();
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		HttpResponse_t2820540315 * L_10 = V_0;
		NullCheck(L_10);
		NameValueCollection_t3047564564 * L_11 = HttpBase_get_Headers_m1195641886(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = NameValueCollection_get_Item_m2776418562(L_11, _stringLiteral22895698, /*hidden argument*/NULL);
		WebSocket_processSecWebSocketExtensionsServerHeader_m1362359134(__this, L_12, /*hidden argument*/NULL);
	}

IL_0069:
	{
		HttpResponse_t2820540315 * L_13 = V_0;
		NullCheck(L_13);
		CookieCollection_t4248997468 * L_14 = HttpResponse_get_Cookies_m1833540882(L_13, /*hidden argument*/NULL);
		WebSocket_processCookies_m4266111229(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern const MethodInfo* Queue_1_Enqueue_m1741874628_MethodInfo_var;
extern const uint32_t WebSocket_enqueueToMessageEventQueue_m820858605_MetadataUsageId;
extern "C"  void WebSocket_enqueueToMessageEventQueue_m820858605 (WebSocket_t3268376029 * __this, MessageEventArgs_t2890051726 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_enqueueToMessageEventQueue_m820858605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forMessageEventQueue_12();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2709708561 * L_2 = __this->get__messageEventQueue_27();
		MessageEventArgs_t2890051726 * L_3 = ___e0;
		NullCheck(L_2);
		Queue_1_Enqueue_m1741874628(L_2, L_3, /*hidden argument*/Queue_1_Enqueue_m1741874628_MethodInfo_var);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::error(System.String,System.Exception)
extern Il2CppClass* ErrorEventArgs_t502222999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Emit_TisErrorEventArgs_t502222999_m1113509766_MethodInfo_var;
extern const uint32_t WebSocket_error_m3171452371_MetadataUsageId;
extern "C"  void WebSocket_error_m3171452371 (WebSocket_t3268376029 * __this, String_t* ___message0, Exception_t1927440687 * ___exception1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_error_m3171452371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t3388497467 * L_0 = __this->get_OnError_51();
		String_t* L_1 = ___message0;
		Exception_t1927440687 * L_2 = ___exception1;
		ErrorEventArgs_t502222999 * L_3 = (ErrorEventArgs_t502222999 *)il2cpp_codegen_object_new(ErrorEventArgs_t502222999_il2cpp_TypeInfo_var);
		ErrorEventArgs__ctor_m3573905349(L_3, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_Emit_TisErrorEventArgs_t502222999_m1113509766(NULL /*static, unused*/, L_0, __this, L_3, /*hidden argument*/Ext_Emit_TisErrorEventArgs_t502222999_m1113509766_MethodInfo_var);
		goto IL_0031;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t1927440687 *)__exception_local);
		Logger_t2598199114 * L_4 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		Exception_t1927440687 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		NullCheck(L_4);
		Logger_Error_m3326994786(L_4, L_6, /*hidden argument*/NULL);
		goto IL_0031;
	} // end catch (depth: 1)

IL_0031:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::fatal(System.String,System.Exception)
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_fatal_m2341174381_MetadataUsageId;
extern "C"  void WebSocket_fatal_m2341174381 (WebSocket_t3268376029 * __this, String_t* ___message0, Exception_t1927440687 * ___exception1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_fatal_m2341174381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint16_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		Exception_t1927440687 * L_0 = ___exception1;
		if (!((WebSocketException_t1348391352 *)IsInstClass(L_0, WebSocketException_t1348391352_il2cpp_TypeInfo_var)))
		{
			goto IL_001b;
		}
	}
	{
		Exception_t1927440687 * L_1 = ___exception1;
		NullCheck(((WebSocketException_t1348391352 *)CastclassClass(L_1, WebSocketException_t1348391352_il2cpp_TypeInfo_var)));
		uint16_t L_2 = WebSocketException_get_Code_m1814741209(((WebSocketException_t1348391352 *)CastclassClass(L_1, WebSocketException_t1348391352_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001b:
	{
		G_B3_0 = ((int32_t)1006);
	}

IL_0020:
	{
		V_0 = G_B3_0;
		String_t* L_3 = ___message0;
		uint16_t L_4 = V_0;
		WebSocket_fatal_m2362838679(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::fatal(System.String,System.UInt16)
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_fatal_m2362838679_MetadataUsageId;
extern "C"  void WebSocket_fatal_m2362838679 (WebSocket_t3268376029 * __this, String_t* ___message0, uint16_t ___code1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_fatal_m2362838679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PayloadData_t3839327312 * V_0 = NULL;
	{
		uint16_t L_0 = ___code1;
		String_t* L_1 = ___message0;
		PayloadData_t3839327312 * L_2 = (PayloadData_t3839327312 *)il2cpp_codegen_object_new(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData__ctor_m2168673930(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PayloadData_t3839327312 * L_3 = V_0;
		uint16_t L_4 = ___code1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_5 = Ext_IsReserved_m2404909809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		WebSocket_close_m3666496073(__this, L_3, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), (bool)0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::fatal(System.String,WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocket_fatal_m1525925709 (WebSocket_t3268376029 * __this, String_t* ___message0, uint16_t ___code1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		uint16_t L_1 = ___code1;
		WebSocket_fatal_m2362838679(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::init()
extern Il2CppClass* CookieCollection_t4248997468_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Queue_1_t2709708561_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m3431931081_MethodInfo_var;
extern const uint32_t WebSocket_init_m2964322191_MetadataUsageId;
extern "C"  void WebSocket_init_m2964322191 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_init_m2964322191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__compression_4(0);
		CookieCollection_t4248997468 * L_0 = (CookieCollection_t4248997468 *)il2cpp_codegen_object_new(CookieCollection_t4248997468_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m1652849323(L_0, /*hidden argument*/NULL);
		__this->set__cookies_6(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		__this->set__forPing_13(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_2, /*hidden argument*/NULL);
		__this->set__forSend_14(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_3, /*hidden argument*/NULL);
		__this->set__forState_15(L_3);
		Queue_1_t2709708561 * L_4 = (Queue_1_t2709708561 *)il2cpp_codegen_object_new(Queue_1_t2709708561_il2cpp_TypeInfo_var);
		Queue_1__ctor_m3431931081(L_4, /*hidden argument*/Queue_1__ctor_m3431931081_MethodInfo_var);
		__this->set__messageEventQueue_27(L_4);
		Queue_1_t2709708561 * L_5 = __this->get__messageEventQueue_27();
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_5);
		__this->set__forMessageEventQueue_12(L_6);
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_37(0);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::message()
extern const MethodInfo* Queue_1_get_Count_m4241224761_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1137069007_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1404390863_MethodInfo_var;
extern const uint32_t WebSocket_message_m4162534566_MetadataUsageId;
extern "C"  void WebSocket_message_m4162534566 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_message_m4162534566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MessageEventArgs_t2890051726 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (MessageEventArgs_t2890051726 *)NULL;
		Il2CppObject * L_0 = __this->get__forMessageEventQueue_12();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = __this->get__inMessage_23();
			il2cpp_codegen_memory_barrier();
			if (L_2)
			{
				goto IL_003a;
			}
		}

IL_001c:
		{
			Queue_1_t2709708561 * L_3 = __this->get__messageEventQueue_27();
			NullCheck(L_3);
			int32_t L_4 = Queue_1_get_Count_m4241224761(L_3, /*hidden argument*/Queue_1_get_Count_m4241224761_MethodInfo_var);
			if (!L_4)
			{
				goto IL_003a;
			}
		}

IL_002c:
		{
			uint16_t L_5 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_5) == ((int32_t)1)))
			{
				goto IL_003f;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x6C, FINALLY_0059);
		}

IL_003f:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__inMessage_23(1);
			Queue_1_t2709708561 * L_6 = __this->get__messageEventQueue_27();
			NullCheck(L_6);
			MessageEventArgs_t2890051726 * L_7 = Queue_1_Dequeue_m1137069007(L_6, /*hidden argument*/Queue_1_Dequeue_m1137069007_MethodInfo_var);
			V_0 = L_7;
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0060:
	{
		Action_1_t2691851108 * L_9 = __this->get__message_26();
		MessageEventArgs_t2890051726 * L_10 = V_0;
		NullCheck(L_9);
		Action_1_Invoke_m1404390863(L_9, L_10, /*hidden argument*/Action_1_Invoke_m1404390863_MethodInfo_var);
	}

IL_006c:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::messagec(WebSocketSharp.MessageEventArgs)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Emit_TisMessageEventArgs_t2890051726_m3679319059_MethodInfo_var;
extern const MethodInfo* Queue_1_get_Count_m4241224761_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1137069007_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1925949088;
extern const uint32_t WebSocket_messagec_m2913074030_MetadataUsageId;
extern "C"  void WebSocket_messagec_m2913074030 (WebSocket_t3268376029 * __this, MessageEventArgs_t2890051726 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_messagec_m2913074030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t1481358898 * L_0 = __this->get_OnMessage_52();
		MessageEventArgs_t2890051726 * L_1 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_Emit_TisMessageEventArgs_t2890051726_m3679319059(NULL /*static, unused*/, L_0, __this, L_1, /*hidden argument*/Ext_Emit_TisMessageEventArgs_t2890051726_m3679319059_MethodInfo_var);
		goto IL_0037;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0012;
		throw e;
	}

CATCH_0012:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t1927440687 *)__exception_local);
		Logger_t2598199114 * L_2 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		NullCheck(L_2);
		Logger_Error_m3326994786(L_2, L_4, /*hidden argument*/NULL);
		Exception_t1927440687 * L_5 = V_0;
		WebSocket_error_m3171452371(__this, _stringLiteral1925949088, L_5, /*hidden argument*/NULL);
		goto IL_0037;
	} // end catch (depth: 1)

IL_0037:
	{
		Il2CppObject * L_6 = __this->get__forMessageEventQueue_12();
		V_1 = L_6;
		Il2CppObject * L_7 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t2709708561 * L_8 = __this->get__messageEventQueue_27();
			NullCheck(L_8);
			int32_t L_9 = Queue_1_get_Count_m4241224761(L_8, /*hidden argument*/Queue_1_get_Count_m4241224761_MethodInfo_var);
			if (!L_9)
			{
				goto IL_0062;
			}
		}

IL_0054:
		{
			uint16_t L_10 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_10) == ((int32_t)1)))
			{
				goto IL_0070;
			}
		}

IL_0062:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__inMessage_23(0);
			IL2CPP_LEAVE(0x8E, FINALLY_0082);
		}

IL_0070:
		{
			Queue_1_t2709708561 * L_11 = __this->get__messageEventQueue_27();
			NullCheck(L_11);
			MessageEventArgs_t2890051726 * L_12 = Queue_1_Dequeue_m1137069007(L_11, /*hidden argument*/Queue_1_Dequeue_m1137069007_MethodInfo_var);
			___e0 = L_12;
			IL2CPP_LEAVE(0x89, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(130)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0089:
	{
		goto IL_0000;
	}

IL_008e:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::open()
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t163412349_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m4241224761_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1137069007_MethodInfo_var;
extern const MethodInfo* WebSocket_U3CopenU3Em__0_m4266960067_MethodInfo_var;
extern const MethodInfo* Action_1_BeginInvoke_m1694518038_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral747705343;
extern const uint32_t WebSocket_open_m476730549_MetadataUsageId;
extern "C"  void WebSocket_open_m476730549 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_open_m476730549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	MessageEventArgs_t2890051726 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		il2cpp_codegen_memory_barrier();
		__this->set__inMessage_23(1);
		WebSocket_startReceiving_m872661131(__this, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		EventHandler_t277755526 * L_0 = __this->get_OnOpen_53();
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs_t3289624707 * L_1 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_Emit_m3411749349(NULL /*static, unused*/, L_0, __this, L_1, /*hidden argument*/NULL);
		goto IL_004a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_0025:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t1927440687 *)__exception_local);
		Logger_t2598199114 * L_2 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		NullCheck(L_2);
		Logger_Error_m3326994786(L_2, L_4, /*hidden argument*/NULL);
		Exception_t1927440687 * L_5 = V_0;
		WebSocket_error_m3171452371(__this, _stringLiteral747705343, L_5, /*hidden argument*/NULL);
		goto IL_004a;
	} // end catch (depth: 1)

IL_004a:
	{
		V_1 = (MessageEventArgs_t2890051726 *)NULL;
		Il2CppObject * L_6 = __this->get__forMessageEventQueue_12();
		V_2 = L_6;
		Il2CppObject * L_7 = V_2;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0059:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t2709708561 * L_8 = __this->get__messageEventQueue_27();
			NullCheck(L_8);
			int32_t L_9 = Queue_1_get_Count_m4241224761(L_8, /*hidden argument*/Queue_1_get_Count_m4241224761_MethodInfo_var);
			if (!L_9)
			{
				goto IL_0077;
			}
		}

IL_0069:
		{
			uint16_t L_10 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_10) == ((int32_t)1)))
			{
				goto IL_0085;
			}
		}

IL_0077:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__inMessage_23(0);
			IL2CPP_LEAVE(0xB7, FINALLY_0096);
		}

IL_0085:
		{
			Queue_1_t2709708561 * L_11 = __this->get__messageEventQueue_27();
			NullCheck(L_11);
			MessageEventArgs_t2890051726 * L_12 = Queue_1_Dequeue_m1137069007(L_11, /*hidden argument*/Queue_1_Dequeue_m1137069007_MethodInfo_var);
			V_1 = L_12;
			IL2CPP_LEAVE(0x9D, FINALLY_0096);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0096;
	}

FINALLY_0096:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_2;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(150)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(150)
	{
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_JUMP_TBL(0x9D, IL_009d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009d:
	{
		Action_1_t2691851108 * L_14 = __this->get__message_26();
		MessageEventArgs_t2890051726 * L_15 = V_1;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)WebSocket_U3CopenU3Em__0_m4266960067_MethodInfo_var);
		AsyncCallback_t163412349 * L_17 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m3071689932(L_17, __this, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Action_1_BeginInvoke_m1694518038(L_14, L_15, L_17, NULL, /*hidden argument*/Action_1_BeginInvoke_m1694518038_MethodInfo_var);
	}

IL_00b7:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processCloseFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processCloseFrame_m1007050172 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	PayloadData_t3839327312 * V_0 = NULL;
	{
		WebSocketFrame_t764750278 * L_0 = ___frame0;
		NullCheck(L_0);
		PayloadData_t3839327312 * L_1 = WebSocketFrame_get_PayloadData_m4003687813(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PayloadData_t3839327312 * L_2 = V_0;
		PayloadData_t3839327312 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = PayloadData_get_HasReservedCode_m933285790(L_3, /*hidden argument*/NULL);
		WebSocket_close_m3666496073(__this, L_2, (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0), (bool)0, (bool)1, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void WebSocketSharp.WebSocket::processCookies(WebSocketSharp.Net.CookieCollection)
extern "C"  void WebSocket_processCookies_m4266111229 (WebSocket_t3268376029 * __this, CookieCollection_t4248997468 * ___cookies0, const MethodInfo* method)
{
	{
		CookieCollection_t4248997468 * L_0 = ___cookies0;
		NullCheck(L_0);
		int32_t L_1 = CookieCollection_get_Count_m3590414075(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		CookieCollection_t4248997468 * L_2 = __this->get__cookies_6();
		CookieCollection_t4248997468 * L_3 = ___cookies0;
		NullCheck(L_2);
		CookieCollection_SetOrRemove_m69867026(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processDataFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageEventArgs_t2890051726_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_processDataFrame_m1864204174_MetadataUsageId;
extern "C"  bool WebSocket_processDataFrame_m1864204174 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_processDataFrame_m1864204174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebSocket_t3268376029 * G_B2_0 = NULL;
	WebSocket_t3268376029 * G_B1_0 = NULL;
	MessageEventArgs_t2890051726 * G_B3_0 = NULL;
	WebSocket_t3268376029 * G_B3_1 = NULL;
	{
		WebSocketFrame_t764750278 * L_0 = ___frame0;
		NullCheck(L_0);
		bool L_1 = WebSocketFrame_get_IsCompressed_m2728001770(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_0032;
		}
	}
	{
		WebSocketFrame_t764750278 * L_2 = ___frame0;
		NullCheck(L_2);
		uint8_t L_3 = WebSocketFrame_get_Opcode_m1205139909(L_2, /*hidden argument*/NULL);
		WebSocketFrame_t764750278 * L_4 = ___frame0;
		NullCheck(L_4);
		PayloadData_t3839327312 * L_5 = WebSocketFrame_get_PayloadData_m4003687813(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t3397334013* L_6 = PayloadData_get_ApplicationData_m3068266889(L_5, /*hidden argument*/NULL);
		uint8_t L_7 = __this->get__compression_4();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_8 = Ext_Decompress_m2853080347(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		MessageEventArgs_t2890051726 * L_9 = (MessageEventArgs_t2890051726 *)il2cpp_codegen_object_new(MessageEventArgs_t2890051726_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m362763596(L_9, L_3, L_8, /*hidden argument*/NULL);
		G_B3_0 = L_9;
		G_B3_1 = G_B1_0;
		goto IL_0038;
	}

IL_0032:
	{
		WebSocketFrame_t764750278 * L_10 = ___frame0;
		MessageEventArgs_t2890051726 * L_11 = (MessageEventArgs_t2890051726 *)il2cpp_codegen_object_new(MessageEventArgs_t2890051726_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m2878951651(L_11, L_10, /*hidden argument*/NULL);
		G_B3_0 = L_11;
		G_B3_1 = G_B2_0;
	}

IL_0038:
	{
		NullCheck(G_B3_1);
		WebSocket_enqueueToMessageEventQueue_m820858605(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processFragmentFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageEventArgs_t2890051726_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_processFragmentFrame_m2596041414_MetadataUsageId;
extern "C"  bool WebSocket_processFragmentFrame_m2596041414 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_processFragmentFrame_m2596041414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	ByteU5BU5D_t3397334013* G_B9_0 = NULL;
	{
		bool L_0 = __this->get__inContinuation_22();
		if (L_0)
		{
			goto IL_0042;
		}
	}
	{
		WebSocketFrame_t764750278 * L_1 = ___frame0;
		NullCheck(L_1);
		bool L_2 = WebSocketFrame_get_IsContinuation_m4031195624(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_0018:
	{
		WebSocketFrame_t764750278 * L_3 = ___frame0;
		NullCheck(L_3);
		uint8_t L_4 = WebSocketFrame_get_Opcode_m1205139909(L_3, /*hidden argument*/NULL);
		__this->set__fragmentsOpcode_18(L_4);
		WebSocketFrame_t764750278 * L_5 = ___frame0;
		NullCheck(L_5);
		bool L_6 = WebSocketFrame_get_IsCompressed_m2728001770(L_5, /*hidden argument*/NULL);
		__this->set__fragmentsCompressed_17(L_6);
		MemoryStream_t743994179 * L_7 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_7, /*hidden argument*/NULL);
		__this->set__fragmentsBuffer_16(L_7);
		__this->set__inContinuation_22((bool)1);
	}

IL_0042:
	{
		MemoryStream_t743994179 * L_8 = __this->get__fragmentsBuffer_16();
		WebSocketFrame_t764750278 * L_9 = ___frame0;
		NullCheck(L_9);
		PayloadData_t3839327312 * L_10 = WebSocketFrame_get_PayloadData_m4003687813(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		ByteU5BU5D_t3397334013* L_11 = PayloadData_get_ApplicationData_m3068266889(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_WriteBytes_m3545246308(NULL /*static, unused*/, L_8, L_11, ((int32_t)1024), /*hidden argument*/NULL);
		WebSocketFrame_t764750278 * L_12 = ___frame0;
		NullCheck(L_12);
		bool L_13 = WebSocketFrame_get_IsFinal_m2923746739(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ce;
		}
	}
	{
		MemoryStream_t743994179 * L_14 = __this->get__fragmentsBuffer_16();
		V_0 = L_14;
	}

IL_006f:
	try
	{ // begin try (depth: 1)
		{
			bool L_15 = __this->get__fragmentsCompressed_17();
			if (!L_15)
			{
				goto IL_0090;
			}
		}

IL_007a:
		{
			MemoryStream_t743994179 * L_16 = __this->get__fragmentsBuffer_16();
			uint8_t L_17 = __this->get__compression_4();
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_18 = Ext_DecompressToArray_m3180897949(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
			G_B9_0 = L_18;
			goto IL_009b;
		}

IL_0090:
		{
			MemoryStream_t743994179 * L_19 = __this->get__fragmentsBuffer_16();
			NullCheck(L_19);
			ByteU5BU5D_t3397334013* L_20 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_19);
			G_B9_0 = L_20;
		}

IL_009b:
		{
			V_1 = G_B9_0;
			uint8_t L_21 = __this->get__fragmentsOpcode_18();
			ByteU5BU5D_t3397334013* L_22 = V_1;
			MessageEventArgs_t2890051726 * L_23 = (MessageEventArgs_t2890051726 *)il2cpp_codegen_object_new(MessageEventArgs_t2890051726_il2cpp_TypeInfo_var);
			MessageEventArgs__ctor_m362763596(L_23, L_21, L_22, /*hidden argument*/NULL);
			WebSocket_enqueueToMessageEventQueue_m820858605(__this, L_23, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xC0, FINALLY_00b3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00b3;
	}

FINALLY_00b3:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_24 = V_0;
			if (!L_24)
			{
				goto IL_00bf;
			}
		}

IL_00b9:
		{
			MemoryStream_t743994179 * L_25 = V_0;
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_25);
		}

IL_00bf:
		{
			IL2CPP_END_FINALLY(179)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(179)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c0:
	{
		__this->set__fragmentsBuffer_16((MemoryStream_t743994179 *)NULL);
		__this->set__inContinuation_22((bool)0);
	}

IL_00ce:
	{
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processPingFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageEventArgs_t2890051726_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2467429018;
extern Il2CppCodeGenString* _stringLiteral3329758934;
extern const uint32_t WebSocket_processPingFrame_m4267023506_MetadataUsageId;
extern "C"  bool WebSocket_processPingFrame_m4267023506 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_processPingFrame_m4267023506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebSocketFrame_t764750278 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebSocketFrame_t764750278 * L_0 = ___frame0;
		NullCheck(L_0);
		PayloadData_t3839327312 * L_1 = WebSocketFrame_get_PayloadData_m4003687813(L_0, /*hidden argument*/NULL);
		bool L_2 = __this->get__client_2();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_t764750278 * L_3 = WebSocketFrame_CreatePongFrame_m3364323429(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = __this->get__forState_15();
		V_1 = L_4;
		Il2CppObject * L_5 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_6 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_6) == ((int32_t)1)))
			{
				goto IL_0046;
			}
		}

IL_002d:
		{
			Logger_t2598199114 * L_7 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_7);
			Logger_Error_m3326994786(L_7, _stringLiteral2467429018, /*hidden argument*/NULL);
			V_2 = (bool)1;
			IL2CPP_LEAVE(0xA6, FINALLY_0063);
		}

IL_0046:
		{
			WebSocketFrame_t764750278 * L_8 = V_0;
			NullCheck(L_8);
			ByteU5BU5D_t3397334013* L_9 = WebSocketFrame_ToArray_m4086645856(L_8, /*hidden argument*/NULL);
			bool L_10 = WebSocket_sendBytes_m1170524623(__this, L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_005e;
			}
		}

IL_0057:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0xA6, FINALLY_0063);
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006a:
	{
		Logger_t2598199114 * L_12 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_12);
		Logger_Trace_m763429895(L_12, _stringLiteral3329758934, /*hidden argument*/NULL);
		bool L_13 = __this->get__emitOnPing_8();
		if (!L_13)
		{
			goto IL_00a4;
		}
	}
	{
		bool L_14 = __this->get__client_2();
		if (!L_14)
		{
			goto IL_0098;
		}
	}
	{
		WebSocketFrame_t764750278 * L_15 = V_0;
		NullCheck(L_15);
		WebSocketFrame_Unmask_m217551411(L_15, /*hidden argument*/NULL);
	}

IL_0098:
	{
		WebSocketFrame_t764750278 * L_16 = ___frame0;
		MessageEventArgs_t2890051726 * L_17 = (MessageEventArgs_t2890051726 *)il2cpp_codegen_object_new(MessageEventArgs_t2890051726_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m2878951651(L_17, L_16, /*hidden argument*/NULL);
		WebSocket_enqueueToMessageEventQueue_m820858605(__this, L_17, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		return (bool)1;
	}

IL_00a6:
	{
		bool L_18 = V_2;
		return L_18;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processPongFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1033904974;
extern const uint32_t WebSocket_processPongFrame_m3106904476_MetadataUsageId;
extern "C"  bool WebSocket_processPongFrame_m3106904476 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_processPongFrame_m3106904476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		ManualResetEvent_t926074657 * L_0 = __this->get__pongReceived_30();
		NullCheck(L_0);
		EventWaitHandle_Set_m2975776670(L_0, /*hidden argument*/NULL);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		if(il2cpp_codegen_class_is_assignable_from (ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0019;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.NullReferenceException)
		V_0 = (bool)0;
		goto IL_0035;
	} // end catch (depth: 1)

CATCH_0019:
	{ // begin catch(System.ObjectDisposedException)
		V_0 = (bool)0;
		goto IL_0035;
	} // end catch (depth: 1)

IL_0021:
	{
		Logger_t2598199114 * L_1 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_1);
		Logger_Trace_m763429895(L_1, _stringLiteral1033904974, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0035:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processReceivedFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_processReceivedFrame_m2771665755_MetadataUsageId;
extern "C"  bool WebSocket_processReceivedFrame_m2771665755 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_processReceivedFrame_m2771665755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool G_B13_0 = false;
	{
		WebSocketFrame_t764750278 * L_0 = ___frame0;
		bool L_1 = WebSocket_checkReceivedFrame_m3485348324(__this, L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_2 = V_0;
		WebSocketException_t1348391352 * L_3 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3554051322(L_3, ((int32_t)1002), L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001a:
	{
		WebSocketFrame_t764750278 * L_4 = ___frame0;
		NullCheck(L_4);
		WebSocketFrame_Unmask_m217551411(L_4, /*hidden argument*/NULL);
		WebSocketFrame_t764750278 * L_5 = ___frame0;
		NullCheck(L_5);
		bool L_6 = WebSocketFrame_get_IsFragment_m2286655259(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		WebSocketFrame_t764750278 * L_7 = ___frame0;
		bool L_8 = WebSocket_processFragmentFrame_m2596041414(__this, L_7, /*hidden argument*/NULL);
		G_B13_0 = L_8;
		goto IL_009a;
	}

IL_0037:
	{
		WebSocketFrame_t764750278 * L_9 = ___frame0;
		NullCheck(L_9);
		bool L_10 = WebSocketFrame_get_IsData_m180465203(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004e;
		}
	}
	{
		WebSocketFrame_t764750278 * L_11 = ___frame0;
		bool L_12 = WebSocket_processDataFrame_m1864204174(__this, L_11, /*hidden argument*/NULL);
		G_B13_0 = L_12;
		goto IL_009a;
	}

IL_004e:
	{
		WebSocketFrame_t764750278 * L_13 = ___frame0;
		NullCheck(L_13);
		bool L_14 = WebSocketFrame_get_IsPing_m1702797379(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		WebSocketFrame_t764750278 * L_15 = ___frame0;
		bool L_16 = WebSocket_processPingFrame_m4267023506(__this, L_15, /*hidden argument*/NULL);
		G_B13_0 = L_16;
		goto IL_009a;
	}

IL_0065:
	{
		WebSocketFrame_t764750278 * L_17 = ___frame0;
		NullCheck(L_17);
		bool L_18 = WebSocketFrame_get_IsPong_m3078149733(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_007c;
		}
	}
	{
		WebSocketFrame_t764750278 * L_19 = ___frame0;
		bool L_20 = WebSocket_processPongFrame_m3106904476(__this, L_19, /*hidden argument*/NULL);
		G_B13_0 = L_20;
		goto IL_009a;
	}

IL_007c:
	{
		WebSocketFrame_t764750278 * L_21 = ___frame0;
		NullCheck(L_21);
		bool L_22 = WebSocketFrame_get_IsClose_m4037370433(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0093;
		}
	}
	{
		WebSocketFrame_t764750278 * L_23 = ___frame0;
		bool L_24 = WebSocket_processCloseFrame_m1007050172(__this, L_23, /*hidden argument*/NULL);
		G_B13_0 = L_24;
		goto IL_009a;
	}

IL_0093:
	{
		WebSocketFrame_t764750278 * L_25 = ___frame0;
		bool L_26 = WebSocket_processUnsupportedFrame_m1830558689(__this, L_25, /*hidden argument*/NULL);
		G_B13_0 = L_26;
	}

IL_009a:
	{
		return G_B13_0;
	}
}
// System.Void WebSocketSharp.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern "C"  void WebSocket_processSecWebSocketExtensionsServerHeader_m1362359134 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		__this->set__compression_4(0);
		return;
	}

IL_000e:
	{
		String_t* L_1 = ___value0;
		__this->set__extensions_10(L_1);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::processUnsupportedFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral699811549;
extern Il2CppCodeGenString* _stringLiteral395929828;
extern const uint32_t WebSocket_processUnsupportedFrame_m1830558689_MetadataUsageId;
extern "C"  bool WebSocket_processUnsupportedFrame_m1830558689 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_processUnsupportedFrame_m1830558689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Logger_t2598199114 * L_0 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		WebSocketFrame_t764750278 * L_1 = ___frame0;
		NullCheck(L_1);
		String_t* L_2 = WebSocketFrame_PrintToString_m2233387245(L_1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral699811549, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Logger_Fatal_m3177508194(L_0, L_3, /*hidden argument*/NULL);
		WebSocket_fatal_m1525925709(__this, _stringLiteral395929828, ((int32_t)1008), /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void WebSocketSharp.WebSocket::releaseClientResources()
extern "C"  void WebSocket_releaseClientResources_m2146138518 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get__stream_42();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Stream_t3255436806 * L_1 = __this->get__stream_42();
		NullCheck(L_1);
		Stream_Dispose_m2417657914(L_1, /*hidden argument*/NULL);
		__this->set__stream_42((Stream_t3255436806 *)NULL);
	}

IL_001d:
	{
		TcpClient_t408947970 * L_2 = __this->get__tcpClient_43();
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		TcpClient_t408947970 * L_3 = __this->get__tcpClient_43();
		NullCheck(L_3);
		TcpClient_Close_m4260227760(L_3, /*hidden argument*/NULL);
		__this->set__tcpClient_43((TcpClient_t408947970 *)NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::releaseCommonResources()
extern "C"  void WebSocket_releaseCommonResources_m2256071308 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	{
		MemoryStream_t743994179 * L_0 = __this->get__fragmentsBuffer_16();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MemoryStream_t743994179 * L_1 = __this->get__fragmentsBuffer_16();
		NullCheck(L_1);
		Stream_Dispose_m2417657914(L_1, /*hidden argument*/NULL);
		__this->set__fragmentsBuffer_16((MemoryStream_t743994179 *)NULL);
		__this->set__inContinuation_22((bool)0);
	}

IL_0024:
	{
		ManualResetEvent_t926074657 * L_2 = __this->get__pongReceived_30();
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		ManualResetEvent_t926074657 * L_3 = __this->get__pongReceived_30();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_3);
		__this->set__pongReceived_30((ManualResetEvent_t926074657 *)NULL);
	}

IL_0041:
	{
		ManualResetEvent_t926074657 * L_4 = __this->get__receivingExited_38();
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		ManualResetEvent_t926074657 * L_5 = __this->get__receivingExited_38();
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_5);
		__this->set__receivingExited_38((ManualResetEvent_t926074657 *)NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::releaseResources()
extern "C"  void WebSocket_releaseResources_m4163756605 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__client_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		WebSocket_releaseClientResources_m2146138518(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		WebSocket_releaseServerResources_m239262322(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		WebSocket_releaseCommonResources_m2256071308(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::releaseServerResources()
extern "C"  void WebSocket_releaseServerResources_m239262322 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get__closeContext_3();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_t3226471752 * L_1 = __this->get__closeContext_3();
		NullCheck(L_1);
		Action_Invoke_m3801112262(L_1, /*hidden argument*/NULL);
		__this->set__closeContext_3((Action_t3226471752 *)NULL);
		__this->set__stream_42((Stream_t3255436806 *)NULL);
		__this->set__context_5((WebSocketContext_t3488732344 *)NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral622602267;
extern Il2CppCodeGenString* _stringLiteral584010916;
extern const uint32_t WebSocket_send_m161023691_MetadataUsageId;
extern "C"  bool WebSocket_send_m161023691 (WebSocket_t3268376029 * __this, uint8_t ___opcode0, Stream_t3255436806 * ___stream1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m161023691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Stream_t3255436806 * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	Exception_t1927440687 * V_4 = NULL;
	bool V_5 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forSend_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Stream_t3255436806 * L_2 = ___stream1;
			V_1 = L_2;
			V_2 = (bool)0;
			V_3 = (bool)0;
		}

IL_0013:
		try
		{ // begin try (depth: 2)
			try
			{ // begin try (depth: 3)
				{
					uint8_t L_3 = __this->get__compression_4();
					if (!L_3)
					{
						goto IL_002e;
					}
				}

IL_001e:
				{
					Stream_t3255436806 * L_4 = ___stream1;
					uint8_t L_5 = __this->get__compression_4();
					IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
					Stream_t3255436806 * L_6 = Ext_Compress_m1131940372(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
					___stream1 = L_6;
					V_2 = (bool)1;
				}

IL_002e:
				{
					uint8_t L_7 = ___opcode0;
					Stream_t3255436806 * L_8 = ___stream1;
					bool L_9 = V_2;
					bool L_10 = WebSocket_send_m3640785804(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
					V_3 = L_10;
					bool L_11 = V_3;
					if (L_11)
					{
						goto IL_004a;
					}
				}

IL_003e:
				{
					WebSocket_error_m3171452371(__this, _stringLiteral622602267, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
				}

IL_004a:
				{
					IL2CPP_LEAVE(0x8A, FINALLY_0077);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__exception_local = (Exception_t1927440687 *)e.ex;
				if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
					goto CATCH_004f;
				throw e;
			}

CATCH_004f:
			{ // begin catch(System.Exception)
				V_4 = ((Exception_t1927440687 *)__exception_local);
				Logger_t2598199114 * L_12 = __this->get__logger_24();
				il2cpp_codegen_memory_barrier();
				Exception_t1927440687 * L_13 = V_4;
				NullCheck(L_13);
				String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
				NullCheck(L_12);
				Logger_Error_m3326994786(L_12, L_14, /*hidden argument*/NULL);
				Exception_t1927440687 * L_15 = V_4;
				WebSocket_error_m3171452371(__this, _stringLiteral584010916, L_15, /*hidden argument*/NULL);
				IL2CPP_LEAVE(0x8A, FINALLY_0077);
			} // end catch (depth: 3)
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0077;
		}

FINALLY_0077:
		{ // begin finally (depth: 2)
			{
				bool L_16 = V_2;
				if (!L_16)
				{
					goto IL_0083;
				}
			}

IL_007d:
			{
				Stream_t3255436806 * L_17 = ___stream1;
				NullCheck(L_17);
				Stream_Dispose_m2417657914(L_17, /*hidden argument*/NULL);
			}

IL_0083:
			{
				Stream_t3255436806 * L_18 = V_1;
				NullCheck(L_18);
				Stream_Dispose_m2417657914(L_18, /*hidden argument*/NULL);
				IL2CPP_END_FINALLY(119)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(119)
		{
			IL2CPP_JUMP_TBL(0x8A, IL_008a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_008a:
		{
			bool L_19 = V_3;
			V_5 = L_19;
			IL2CPP_LEAVE(0x99, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		Il2CppObject * L_20 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(146)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0x99, IL_0099)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0099:
	{
		bool L_21 = V_5;
		return L_21;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream,System.Boolean)
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_send_m3640785804_MetadataUsageId;
extern "C"  bool WebSocket_send_m3640785804 (WebSocket_t3268376029 * __this, uint8_t ___opcode0, Stream_t3255436806 * ___stream1, bool ___compressed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m3640785804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	bool V_4 = false;
	int64_t V_5 = 0;
	int64_t V_6 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B16_0 = 0;
	int64_t G_B21_0 = 0;
	int32_t G_B25_0 = 0;
	int32_t G_B35_0 = 0;
	{
		Stream_t3255436806 * L_0 = ___stream1;
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_0);
		V_0 = L_1;
		int64_t L_2 = V_0;
		if ((!(((uint64_t)L_2) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001e;
		}
	}
	{
		uint8_t L_3 = ___opcode0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_4 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		bool L_5 = WebSocket_send_m4234817766(__this, 1, L_3, L_4, (bool)0, /*hidden argument*/NULL);
		return L_5;
	}

IL_001e:
	{
		int64_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		int32_t L_7 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		V_1 = ((int64_t)((int64_t)L_6/(int64_t)(((int64_t)((int64_t)L_7)))));
		int64_t L_8 = V_0;
		int32_t L_9 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		V_2 = (((int32_t)((int32_t)((int64_t)((int64_t)L_8%(int64_t)(((int64_t)((int64_t)L_9))))))));
		V_3 = (ByteU5BU5D_t3397334013*)NULL;
		int64_t L_10 = V_1;
		if ((!(((uint64_t)L_10) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_11 = V_2;
		V_3 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_11));
		Stream_t3255436806 * L_12 = ___stream1;
		ByteU5BU5D_t3397334013* L_13 = V_3;
		int32_t L_14 = V_2;
		NullCheck(L_12);
		int32_t L_15 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_12, L_13, 0, L_14);
		int32_t L_16 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_005d;
		}
	}
	{
		uint8_t L_17 = ___opcode0;
		ByteU5BU5D_t3397334013* L_18 = V_3;
		bool L_19 = ___compressed2;
		bool L_20 = WebSocket_send_m4234817766(__this, 1, L_17, L_18, L_19, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_20));
		goto IL_005e;
	}

IL_005d:
	{
		G_B6_0 = 0;
	}

IL_005e:
	{
		return (bool)G_B6_0;
	}

IL_005f:
	{
		int64_t L_21 = V_1;
		if ((!(((uint64_t)L_21) == ((uint64_t)(((int64_t)((int64_t)1)))))))
		{
			goto IL_009d;
		}
	}
	{
		int32_t L_22 = V_2;
		if (L_22)
		{
			goto IL_009d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		int32_t L_23 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		V_3 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_23));
		Stream_t3255436806 * L_24 = ___stream1;
		ByteU5BU5D_t3397334013* L_25 = V_3;
		int32_t L_26 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		NullCheck(L_24);
		int32_t L_27 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_24, L_25, 0, L_26);
		int32_t L_28 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_009b;
		}
	}
	{
		uint8_t L_29 = ___opcode0;
		ByteU5BU5D_t3397334013* L_30 = V_3;
		bool L_31 = ___compressed2;
		bool L_32 = WebSocket_send_m4234817766(__this, 1, L_29, L_30, L_31, /*hidden argument*/NULL);
		G_B12_0 = ((int32_t)(L_32));
		goto IL_009c;
	}

IL_009b:
	{
		G_B12_0 = 0;
	}

IL_009c:
	{
		return (bool)G_B12_0;
	}

IL_009d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		int32_t L_33 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		V_3 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_33));
		Stream_t3255436806 * L_34 = ___stream1;
		ByteU5BU5D_t3397334013* L_35 = V_3;
		int32_t L_36 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		NullCheck(L_34);
		int32_t L_37 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, L_36);
		int32_t L_38 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		if ((!(((uint32_t)L_37) == ((uint32_t)L_38))))
		{
			goto IL_00cb;
		}
	}
	{
		uint8_t L_39 = ___opcode0;
		ByteU5BU5D_t3397334013* L_40 = V_3;
		bool L_41 = ___compressed2;
		bool L_42 = WebSocket_send_m4234817766(__this, 0, L_39, L_40, L_41, /*hidden argument*/NULL);
		G_B16_0 = ((int32_t)(L_42));
		goto IL_00cc;
	}

IL_00cb:
	{
		G_B16_0 = 0;
	}

IL_00cc:
	{
		V_4 = (bool)G_B16_0;
		bool L_43 = V_4;
		if (L_43)
		{
			goto IL_00d7;
		}
	}
	{
		return (bool)0;
	}

IL_00d7:
	{
		int32_t L_44 = V_2;
		if (L_44)
		{
			goto IL_00e6;
		}
	}
	{
		int64_t L_45 = V_1;
		G_B21_0 = ((int64_t)((int64_t)L_45-(int64_t)(((int64_t)((int64_t)2)))));
		goto IL_00ea;
	}

IL_00e6:
	{
		int64_t L_46 = V_1;
		G_B21_0 = ((int64_t)((int64_t)L_46-(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_00ea:
	{
		V_5 = G_B21_0;
		V_6 = (((int64_t)((int64_t)0)));
		goto IL_012b;
	}

IL_00f5:
	{
		Stream_t3255436806 * L_47 = ___stream1;
		ByteU5BU5D_t3397334013* L_48 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		int32_t L_49 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		NullCheck(L_47);
		int32_t L_50 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_47, L_48, 0, L_49);
		int32_t L_51 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		if ((!(((uint32_t)L_50) == ((uint32_t)L_51))))
		{
			goto IL_0118;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_52 = V_3;
		bool L_53 = WebSocket_send_m4234817766(__this, 0, 0, L_52, (bool)0, /*hidden argument*/NULL);
		G_B25_0 = ((int32_t)(L_53));
		goto IL_0119;
	}

IL_0118:
	{
		G_B25_0 = 0;
	}

IL_0119:
	{
		V_4 = (bool)G_B25_0;
		bool L_54 = V_4;
		if (L_54)
		{
			goto IL_0124;
		}
	}
	{
		return (bool)0;
	}

IL_0124:
	{
		int64_t L_55 = V_6;
		V_6 = ((int64_t)((int64_t)L_55+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_012b:
	{
		int64_t L_56 = V_6;
		int64_t L_57 = V_5;
		if ((((int64_t)L_56) < ((int64_t)L_57)))
		{
			goto IL_00f5;
		}
	}
	{
		int32_t L_58 = V_2;
		if (L_58)
		{
			goto IL_0145;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		int32_t L_59 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_FragmentLength_48();
		V_2 = L_59;
		goto IL_014c;
	}

IL_0145:
	{
		int32_t L_60 = V_2;
		V_3 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_60));
	}

IL_014c:
	{
		Stream_t3255436806 * L_61 = ___stream1;
		ByteU5BU5D_t3397334013* L_62 = V_3;
		int32_t L_63 = V_2;
		NullCheck(L_61);
		int32_t L_64 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_61, L_62, 0, L_63);
		int32_t L_65 = V_2;
		if ((!(((uint32_t)L_64) == ((uint32_t)L_65))))
		{
			goto IL_0167;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_66 = V_3;
		bool L_67 = WebSocket_send_m4234817766(__this, 1, 0, L_66, (bool)0, /*hidden argument*/NULL);
		G_B35_0 = ((int32_t)(L_67));
		goto IL_0168;
	}

IL_0167:
	{
		G_B35_0 = 0;
	}

IL_0168:
	{
		return (bool)G_B35_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean)
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2467429018;
extern const uint32_t WebSocket_send_m4234817766_MetadataUsageId;
extern "C"  bool WebSocket_send_m4234817766 (WebSocket_t3268376029 * __this, uint8_t ___fin0, uint8_t ___opcode1, ByteU5BU5D_t3397334013* ___data2, bool ___compressed3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m4234817766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	WebSocketFrame_t764750278 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forState_15();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_37();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0034;
			}
		}

IL_001b:
		{
			Logger_t2598199114 * L_3 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_3);
			Logger_Error_m3326994786(L_3, _stringLiteral2467429018, /*hidden argument*/NULL);
			V_1 = (bool)0;
			IL2CPP_LEAVE(0x5E, FINALLY_0057);
		}

IL_0034:
		{
			uint8_t L_4 = ___fin0;
			uint8_t L_5 = ___opcode1;
			ByteU5BU5D_t3397334013* L_6 = ___data2;
			bool L_7 = ___compressed3;
			bool L_8 = __this->get__client_2();
			WebSocketFrame_t764750278 * L_9 = (WebSocketFrame_t764750278 *)il2cpp_codegen_object_new(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
			WebSocketFrame__ctor_m4039525610(L_9, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			WebSocketFrame_t764750278 * L_10 = V_2;
			NullCheck(L_10);
			ByteU5BU5D_t3397334013* L_11 = WebSocketFrame_ToArray_m4086645856(L_10, /*hidden argument*/NULL);
			bool L_12 = WebSocket_sendBytes_m1170524623(__this, L_11, /*hidden argument*/NULL);
			V_1 = L_12;
			IL2CPP_LEAVE(0x5E, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005e:
	{
		bool L_14 = V_1;
		return L_14;
	}
}
// System.Boolean WebSocketSharp.WebSocket::sendBytes(System.Byte[])
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_sendBytes_m1170524623_MetadataUsageId;
extern "C"  bool WebSocket_sendBytes_m1170524623 (WebSocket_t3268376029 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendBytes_m1170524623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_0 = __this->get__stream_42();
		ByteU5BU5D_t3397334013* L_1 = ___bytes0;
		ByteU5BU5D_t3397334013* L_2 = ___bytes0;
		NullCheck(L_2);
		NullCheck(L_0);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		V_0 = (bool)1;
		goto IL_0032;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1927440687 *)__exception_local);
		Logger_t2598199114 * L_3 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		Exception_t1927440687 * L_4 = V_1;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_3);
		Logger_Error_m3326994786(L_3, L_5, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_0032;
	} // end catch (depth: 1)

IL_0032:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1978770395;
extern Il2CppCodeGenString* _stringLiteral1059615132;
extern Il2CppCodeGenString* _stringLiteral3614653178;
extern Il2CppCodeGenString* _stringLiteral1221281231;
extern Il2CppCodeGenString* _stringLiteral2313763943;
extern Il2CppCodeGenString* _stringLiteral2325276717;
extern Il2CppCodeGenString* _stringLiteral2641437383;
extern Il2CppCodeGenString* _stringLiteral669573099;
extern Il2CppCodeGenString* _stringLiteral3121705848;
extern Il2CppCodeGenString* _stringLiteral3115737039;
extern const uint32_t WebSocket_sendHandshakeRequest_m1722434510_MetadataUsageId;
extern "C"  HttpResponse_t2820540315 * WebSocket_sendHandshakeRequest_m1722434510 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendHandshakeRequest_m1722434510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpRequest_t1845443631 * V_0 = NULL;
	HttpResponse_t2820540315 * V_1 = NULL;
	String_t* V_2 = NULL;
	AuthenticationResponse_t1212723231 * V_3 = NULL;
	String_t* V_4 = NULL;
	Uri_t19570940 * V_5 = NULL;
	String_t* V_6 = NULL;
	{
		HttpRequest_t1845443631 * L_0 = WebSocket_createHandshakeRequest_m4128701898(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		HttpRequest_t1845443631 * L_1 = V_0;
		HttpResponse_t2820540315 * L_2 = WebSocket_sendHttpRequest_m4116722520(__this, L_1, ((int32_t)90000), /*hidden argument*/NULL);
		V_1 = L_2;
		HttpResponse_t2820540315 * L_3 = V_1;
		NullCheck(L_3);
		bool L_4 = HttpResponse_get_IsUnauthorized_m1649884238(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0117;
		}
	}
	{
		HttpResponse_t2820540315 * L_5 = V_1;
		NullCheck(L_5);
		NameValueCollection_t3047564564 * L_6 = HttpBase_get_Headers_m1195641886(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = NameValueCollection_get_Item_m2776418562(L_6, _stringLiteral1978770395, /*hidden argument*/NULL);
		V_2 = L_7;
		Logger_t2598199114 * L_8 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		String_t* L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1059615132, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Logger_Warn_m2119571238(L_8, L_10, /*hidden argument*/NULL);
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_12 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0067;
		}
	}
	{
		Logger_t2598199114 * L_13 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_13);
		Logger_Error_m3326994786(L_13, _stringLiteral3614653178, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_14 = V_1;
		return L_14;
	}

IL_0067:
	{
		String_t* L_15 = V_2;
		AuthenticationChallenge_t1146723439 * L_16 = AuthenticationChallenge_Parse_m2486528551(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		__this->set__authChallenge_0(L_16);
		AuthenticationChallenge_t1146723439 * L_17 = __this->get__authChallenge_0();
		if (L_17)
		{
			goto IL_0092;
		}
	}
	{
		Logger_t2598199114 * L_18 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_18);
		Logger_Error_m3326994786(L_18, _stringLiteral1221281231, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_19 = V_1;
		return L_19;
	}

IL_0092:
	{
		NetworkCredential_t3911206805 * L_20 = __this->get__credentials_7();
		if (!L_20)
		{
			goto IL_0117;
		}
	}
	{
		bool L_21 = __this->get__preAuth_31();
		if (!L_21)
		{
			goto IL_00b9;
		}
	}
	{
		AuthenticationChallenge_t1146723439 * L_22 = __this->get__authChallenge_0();
		NullCheck(L_22);
		int32_t L_23 = AuthenticationBase_get_Scheme_m1504673019(L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_0117;
		}
	}

IL_00b9:
	{
		HttpResponse_t2820540315 * L_24 = V_1;
		NullCheck(L_24);
		bool L_25 = HttpResponse_get_HasConnectionClose_m987163280(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d0;
		}
	}
	{
		WebSocket_releaseClientResources_m2146138518(__this, /*hidden argument*/NULL);
		WebSocket_setClientStream_m3923254386(__this, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		AuthenticationChallenge_t1146723439 * L_26 = __this->get__authChallenge_0();
		NetworkCredential_t3911206805 * L_27 = __this->get__credentials_7();
		uint32_t L_28 = __this->get__nonceCount_28();
		AuthenticationResponse_t1212723231 * L_29 = (AuthenticationResponse_t1212723231 *)il2cpp_codegen_object_new(AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m3805970484(L_29, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_3 = L_29;
		AuthenticationResponse_t1212723231 * L_30 = V_3;
		NullCheck(L_30);
		uint32_t L_31 = AuthenticationResponse_get_NonceCount_m2749455448(L_30, /*hidden argument*/NULL);
		__this->set__nonceCount_28(L_31);
		HttpRequest_t1845443631 * L_32 = V_0;
		NullCheck(L_32);
		NameValueCollection_t3047564564 * L_33 = HttpBase_get_Headers_m1195641886(L_32, /*hidden argument*/NULL);
		AuthenticationResponse_t1212723231 * L_34 = V_3;
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		NullCheck(L_33);
		NameValueCollection_set_Item_m3775607929(L_33, _stringLiteral2313763943, L_35, /*hidden argument*/NULL);
		HttpRequest_t1845443631 * L_36 = V_0;
		HttpResponse_t2820540315 * L_37 = WebSocket_sendHttpRequest_m4116722520(__this, L_36, ((int32_t)15000), /*hidden argument*/NULL);
		V_1 = L_37;
	}

IL_0117:
	{
		HttpResponse_t2820540315 * L_38 = V_1;
		NullCheck(L_38);
		bool L_39 = HttpResponse_get_IsRedirect_m3194810376(L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_01d5;
		}
	}
	{
		HttpResponse_t2820540315 * L_40 = V_1;
		NullCheck(L_40);
		NameValueCollection_t3047564564 * L_41 = HttpBase_get_Headers_m1195641886(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		String_t* L_42 = NameValueCollection_get_Item_m2776418562(L_41, _stringLiteral2325276717, /*hidden argument*/NULL);
		V_4 = L_42;
		Logger_t2598199114 * L_43 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		String_t* L_44 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2641437383, L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		Logger_Warn_m2119571238(L_43, L_45, /*hidden argument*/NULL);
		bool L_46 = __this->get__enableRedirection_9();
		if (!L_46)
		{
			goto IL_01d5;
		}
	}
	{
		String_t* L_47 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_48 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0178;
		}
	}
	{
		Logger_t2598199114 * L_49 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_49);
		Logger_Error_m3326994786(L_49, _stringLiteral669573099, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_50 = V_1;
		return L_50;
	}

IL_0178:
	{
		String_t* L_51 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_52 = Ext_TryCreateWebSocketUri_m2978564150(NULL /*static, unused*/, L_51, (&V_5), (&V_6), /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_01a3;
		}
	}
	{
		Logger_t2598199114 * L_53 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		String_t* L_54 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3121705848, L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		Logger_Error_m3326994786(L_53, L_55, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_56 = V_1;
		return L_56;
	}

IL_01a3:
	{
		WebSocket_releaseClientResources_m2146138518(__this, /*hidden argument*/NULL);
		Uri_t19570940 * L_57 = V_5;
		__this->set__uri_44(L_57);
		Uri_t19570940 * L_58 = V_5;
		NullCheck(L_58);
		String_t* L_59 = Uri_get_Scheme_m55908894(L_58, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_59, _stringLiteral3115737039, /*hidden argument*/NULL);
		__this->set__secure_40(L_60);
		WebSocket_setClientStream_m3923254386(__this, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_61 = WebSocket_sendHandshakeRequest_m1722434510(__this, /*hidden argument*/NULL);
		return L_61;
	}

IL_01d5:
	{
		HttpResponse_t2820540315 * L_62 = V_1;
		return L_62;
	}
}
// WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHttpRequest(WebSocketSharp.HttpRequest,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3534263999;
extern Il2CppCodeGenString* _stringLiteral3995926990;
extern const uint32_t WebSocket_sendHttpRequest_m4116722520_MetadataUsageId;
extern "C"  HttpResponse_t2820540315 * WebSocket_sendHttpRequest_m4116722520 (WebSocket_t3268376029 * __this, HttpRequest_t1845443631 * ___request0, int32_t ___millisecondsTimeout1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendHttpRequest_m4116722520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpResponse_t2820540315 * V_0 = NULL;
	{
		Logger_t2598199114 * L_0 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		HttpRequest_t1845443631 * L_1 = ___request0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3534263999, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Logger_Debug_m3721126547(L_0, L_3, /*hidden argument*/NULL);
		HttpRequest_t1845443631 * L_4 = ___request0;
		Stream_t3255436806 * L_5 = __this->get__stream_42();
		int32_t L_6 = ___millisecondsTimeout1;
		NullCheck(L_4);
		HttpResponse_t2820540315 * L_7 = HttpRequest_GetResponse_m12496573(L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Logger_t2598199114 * L_8 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		HttpResponse_t2820540315 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3995926990, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Logger_Debug_m3721126547(L_8, L_11, /*hidden argument*/NULL);
		HttpResponse_t2820540315 * L_12 = V_0;
		return L_12;
	}
}
// System.Void WebSocketSharp.WebSocket::sendProxyConnectRequest()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppClass* TcpClient_t408947970_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3133534876;
extern Il2CppCodeGenString* _stringLiteral932190086;
extern Il2CppCodeGenString* _stringLiteral2103671714;
extern Il2CppCodeGenString* _stringLiteral1889244043;
extern Il2CppCodeGenString* _stringLiteral343754898;
extern Il2CppCodeGenString* _stringLiteral2034746328;
extern Il2CppCodeGenString* _stringLiteral3978599445;
extern const uint32_t WebSocket_sendProxyConnectRequest_m2863768904_MetadataUsageId;
extern "C"  void WebSocket_sendProxyConnectRequest_m2863768904 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendProxyConnectRequest_m2863768904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpRequest_t1845443631 * V_0 = NULL;
	HttpResponse_t2820540315 * V_1 = NULL;
	String_t* V_2 = NULL;
	AuthenticationChallenge_t1146723439 * V_3 = NULL;
	AuthenticationResponse_t1212723231 * V_4 = NULL;
	{
		Uri_t19570940 * L_0 = __this->get__uri_44();
		HttpRequest_t1845443631 * L_1 = HttpRequest_CreateConnectRequest_m1662457562(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HttpRequest_t1845443631 * L_2 = V_0;
		HttpResponse_t2820540315 * L_3 = WebSocket_sendHttpRequest_m4116722520(__this, L_2, ((int32_t)90000), /*hidden argument*/NULL);
		V_1 = L_3;
		HttpResponse_t2820540315 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = HttpResponse_get_IsProxyAuthenticationRequired_m1945167221(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0112;
		}
	}
	{
		HttpResponse_t2820540315 * L_6 = V_1;
		NullCheck(L_6);
		NameValueCollection_t3047564564 * L_7 = HttpBase_get_Headers_m1195641886(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = NameValueCollection_get_Item_m2776418562(L_7, _stringLiteral3133534876, /*hidden argument*/NULL);
		V_2 = L_8;
		Logger_t2598199114 * L_9 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		String_t* L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral932190086, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Logger_Warn_m2119571238(L_9, L_11, /*hidden argument*/NULL);
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_13 = Ext_IsNullOrEmpty_m2240019316(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0063;
		}
	}
	{
		WebSocketException_t1348391352 * L_14 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_14, _stringLiteral2103671714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0063:
	{
		String_t* L_15 = V_2;
		AuthenticationChallenge_t1146723439 * L_16 = AuthenticationChallenge_Parse_m2486528551(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		AuthenticationChallenge_t1146723439 * L_17 = V_3;
		if (L_17)
		{
			goto IL_007b;
		}
	}
	{
		WebSocketException_t1348391352 * L_18 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_18, _stringLiteral1889244043, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_007b:
	{
		NetworkCredential_t3911206805 * L_19 = __this->get__proxyCredentials_35();
		if (!L_19)
		{
			goto IL_00fc;
		}
	}
	{
		HttpResponse_t2820540315 * L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = HttpResponse_get_HasConnectionClose_m987163280(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c9;
		}
	}
	{
		WebSocket_releaseClientResources_m2146138518(__this, /*hidden argument*/NULL);
		Uri_t19570940 * L_22 = __this->get__proxyUri_36();
		NullCheck(L_22);
		String_t* L_23 = Uri_get_DnsSafeHost_m795496231(L_22, /*hidden argument*/NULL);
		Uri_t19570940 * L_24 = __this->get__proxyUri_36();
		NullCheck(L_24);
		int32_t L_25 = Uri_get_Port_m834512465(L_24, /*hidden argument*/NULL);
		TcpClient_t408947970 * L_26 = (TcpClient_t408947970 *)il2cpp_codegen_object_new(TcpClient_t408947970_il2cpp_TypeInfo_var);
		TcpClient__ctor_m4115769373(L_26, L_23, L_25, /*hidden argument*/NULL);
		__this->set__tcpClient_43(L_26);
		TcpClient_t408947970 * L_27 = __this->get__tcpClient_43();
		NullCheck(L_27);
		NetworkStream_t581172200 * L_28 = TcpClient_GetStream_m872175179(L_27, /*hidden argument*/NULL);
		__this->set__stream_42(L_28);
	}

IL_00c9:
	{
		AuthenticationChallenge_t1146723439 * L_29 = V_3;
		NetworkCredential_t3911206805 * L_30 = __this->get__proxyCredentials_35();
		AuthenticationResponse_t1212723231 * L_31 = (AuthenticationResponse_t1212723231 *)il2cpp_codegen_object_new(AuthenticationResponse_t1212723231_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m3805970484(L_31, L_29, L_30, 0, /*hidden argument*/NULL);
		V_4 = L_31;
		HttpRequest_t1845443631 * L_32 = V_0;
		NullCheck(L_32);
		NameValueCollection_t3047564564 * L_33 = HttpBase_get_Headers_m1195641886(L_32, /*hidden argument*/NULL);
		AuthenticationResponse_t1212723231 * L_34 = V_4;
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		NullCheck(L_33);
		NameValueCollection_set_Item_m3775607929(L_33, _stringLiteral343754898, L_35, /*hidden argument*/NULL);
		HttpRequest_t1845443631 * L_36 = V_0;
		HttpResponse_t2820540315 * L_37 = WebSocket_sendHttpRequest_m4116722520(__this, L_36, ((int32_t)15000), /*hidden argument*/NULL);
		V_1 = L_37;
	}

IL_00fc:
	{
		HttpResponse_t2820540315 * L_38 = V_1;
		NullCheck(L_38);
		bool L_39 = HttpResponse_get_IsProxyAuthenticationRequired_m1945167221(L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0112;
		}
	}
	{
		WebSocketException_t1348391352 * L_40 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_40, _stringLiteral2034746328, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_0112:
	{
		HttpResponse_t2820540315 * L_41 = V_1;
		NullCheck(L_41);
		String_t* L_42 = HttpResponse_get_StatusCode_m1116886410(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Il2CppChar L_43 = String_get_Chars_m4230566705(L_42, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_43) == ((int32_t)((int32_t)50))))
		{
			goto IL_0130;
		}
	}
	{
		WebSocketException_t1348391352 * L_44 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_44, _stringLiteral3978599445, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	}

IL_0130:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::setClientStream()
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* TcpClient_t408947970_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppClass* SslStream_t1853163792_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3138661567;
extern const uint32_t WebSocket_setClientStream_m3923254386_MetadataUsageId;
extern "C"  void WebSocket_setClientStream_m3923254386 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_setClientStream_m3923254386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ClientSslConfiguration_t1159130081 * V_0 = NULL;
	String_t* V_1 = NULL;
	SslStream_t1853163792 * V_2 = NULL;
	Exception_t1927440687 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Uri_t19570940 * L_0 = __this->get__proxyUri_36();
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_0, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		Uri_t19570940 * L_2 = __this->get__proxyUri_36();
		NullCheck(L_2);
		String_t* L_3 = Uri_get_DnsSafeHost_m795496231(L_2, /*hidden argument*/NULL);
		Uri_t19570940 * L_4 = __this->get__proxyUri_36();
		NullCheck(L_4);
		int32_t L_5 = Uri_get_Port_m834512465(L_4, /*hidden argument*/NULL);
		TcpClient_t408947970 * L_6 = (TcpClient_t408947970 *)il2cpp_codegen_object_new(TcpClient_t408947970_il2cpp_TypeInfo_var);
		TcpClient__ctor_m4115769373(L_6, L_3, L_5, /*hidden argument*/NULL);
		__this->set__tcpClient_43(L_6);
		TcpClient_t408947970 * L_7 = __this->get__tcpClient_43();
		NullCheck(L_7);
		NetworkStream_t581172200 * L_8 = TcpClient_GetStream_m872175179(L_7, /*hidden argument*/NULL);
		__this->set__stream_42(L_8);
		WebSocket_sendProxyConnectRequest_m2863768904(__this, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_004e:
	{
		Uri_t19570940 * L_9 = __this->get__uri_44();
		NullCheck(L_9);
		String_t* L_10 = Uri_get_DnsSafeHost_m795496231(L_9, /*hidden argument*/NULL);
		Uri_t19570940 * L_11 = __this->get__uri_44();
		NullCheck(L_11);
		int32_t L_12 = Uri_get_Port_m834512465(L_11, /*hidden argument*/NULL);
		TcpClient_t408947970 * L_13 = (TcpClient_t408947970 *)il2cpp_codegen_object_new(TcpClient_t408947970_il2cpp_TypeInfo_var);
		TcpClient__ctor_m4115769373(L_13, L_10, L_12, /*hidden argument*/NULL);
		__this->set__tcpClient_43(L_13);
		TcpClient_t408947970 * L_14 = __this->get__tcpClient_43();
		NullCheck(L_14);
		NetworkStream_t581172200 * L_15 = TcpClient_GetStream_m872175179(L_14, /*hidden argument*/NULL);
		__this->set__stream_42(L_15);
	}

IL_0080:
	{
		bool L_16 = __this->get__secure_40();
		if (!L_16)
		{
			goto IL_010a;
		}
	}
	{
		ClientSslConfiguration_t1159130081 * L_17 = WebSocket_get_SslConfiguration_m3776182150(__this, /*hidden argument*/NULL);
		V_0 = L_17;
		ClientSslConfiguration_t1159130081 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = ClientSslConfiguration_get_TargetHost_m740170825(L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		String_t* L_20 = V_1;
		Uri_t19570940 * L_21 = __this->get__uri_44();
		NullCheck(L_21);
		String_t* L_22 = Uri_get_DnsSafeHost_m795496231(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00bf;
		}
	}
	{
		WebSocketException_t1348391352 * L_24 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3554051322(L_24, ((int32_t)1015), _stringLiteral3138661567, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00bf:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_25 = __this->get__stream_42();
		ClientSslConfiguration_t1159130081 * L_26 = V_0;
		NullCheck(L_26);
		RemoteCertificateValidationCallback_t2756269959 * L_27 = ClientSslConfiguration_get_ServerCertificateValidationCallback_m3599237829(L_26, /*hidden argument*/NULL);
		ClientSslConfiguration_t1159130081 * L_28 = V_0;
		NullCheck(L_28);
		LocalCertificateSelectionCallback_t3696771181 * L_29 = ClientSslConfiguration_get_ClientCertificateSelectionCallback_m3153947016(L_28, /*hidden argument*/NULL);
		SslStream_t1853163792 * L_30 = (SslStream_t1853163792 *)il2cpp_codegen_object_new(SslStream_t1853163792_il2cpp_TypeInfo_var);
		SslStream__ctor_m2229021900(L_30, L_25, (bool)0, L_27, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
		SslStream_t1853163792 * L_31 = V_2;
		String_t* L_32 = V_1;
		ClientSslConfiguration_t1159130081 * L_33 = V_0;
		NullCheck(L_33);
		X509CertificateCollection_t1197680765 * L_34 = ClientSslConfiguration_get_ClientCertificates_m3726515733(L_33, /*hidden argument*/NULL);
		ClientSslConfiguration_t1159130081 * L_35 = V_0;
		NullCheck(L_35);
		int32_t L_36 = SslConfiguration_get_EnabledSslProtocols_m4088005259(L_35, /*hidden argument*/NULL);
		ClientSslConfiguration_t1159130081 * L_37 = V_0;
		NullCheck(L_37);
		bool L_38 = SslConfiguration_get_CheckCertificateRevocation_m1861366223(L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker4< String_t*, X509CertificateCollection_t1197680765 *, int32_t, bool >::Invoke(26 /* System.Void System.Net.Security.SslStream::AuthenticateAsClient(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean) */, L_31, L_32, L_34, L_36, L_38);
		SslStream_t1853163792 * L_39 = V_2;
		__this->set__stream_42(L_39);
		goto IL_010a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00fd;
		throw e;
	}

CATCH_00fd:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_40 = V_3;
		WebSocketException_t1348391352 * L_41 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m1123856050(L_41, ((int32_t)1015), L_40, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_41);
	} // end catch (depth: 1)

IL_010a:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::startReceiving()
extern Il2CppClass* U3CstartReceivingU3Ec__AnonStorey4_t506990866_il2cpp_TypeInfo_var;
extern Il2CppClass* ManualResetEvent_t926074657_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m4241224761_MethodInfo_var;
extern const MethodInfo* Queue_1_Clear_m3135450124_MethodInfo_var;
extern const MethodInfo* U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__0_m231510862_MethodInfo_var;
extern const uint32_t WebSocket_startReceiving_m872661131_MetadataUsageId;
extern "C"  void WebSocket_startReceiving_m872661131 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_startReceiving_m872661131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CstartReceivingU3Ec__AnonStorey4_t506990866 * V_0 = NULL;
	{
		U3CstartReceivingU3Ec__AnonStorey4_t506990866 * L_0 = (U3CstartReceivingU3Ec__AnonStorey4_t506990866 *)il2cpp_codegen_object_new(U3CstartReceivingU3Ec__AnonStorey4_t506990866_il2cpp_TypeInfo_var);
		U3CstartReceivingU3Ec__AnonStorey4__ctor_m3102331381(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CstartReceivingU3Ec__AnonStorey4_t506990866 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		Queue_1_t2709708561 * L_2 = __this->get__messageEventQueue_27();
		NullCheck(L_2);
		int32_t L_3 = Queue_1_get_Count_m4241224761(L_2, /*hidden argument*/Queue_1_get_Count_m4241224761_MethodInfo_var);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		Queue_1_t2709708561 * L_4 = __this->get__messageEventQueue_27();
		NullCheck(L_4);
		Queue_1_Clear_m3135450124(L_4, /*hidden argument*/Queue_1_Clear_m3135450124_MethodInfo_var);
	}

IL_0029:
	{
		ManualResetEvent_t926074657 * L_5 = (ManualResetEvent_t926074657 *)il2cpp_codegen_object_new(ManualResetEvent_t926074657_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m3470249043(L_5, (bool)0, /*hidden argument*/NULL);
		__this->set__pongReceived_30(L_5);
		ManualResetEvent_t926074657 * L_6 = (ManualResetEvent_t926074657 *)il2cpp_codegen_object_new(ManualResetEvent_t926074657_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m3470249043(L_6, (bool)0, /*hidden argument*/NULL);
		__this->set__receivingExited_38(L_6);
		U3CstartReceivingU3Ec__AnonStorey4_t506990866 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_receive_0((Action_t3226471752 *)NULL);
		U3CstartReceivingU3Ec__AnonStorey4_t506990866 * L_8 = V_0;
		U3CstartReceivingU3Ec__AnonStorey4_t506990866 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__0_m231510862_MethodInfo_var);
		Action_t3226471752 * L_11 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_11, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_receive_0(L_11);
		U3CstartReceivingU3Ec__AnonStorey4_t506990866 * L_12 = V_0;
		NullCheck(L_12);
		Action_t3226471752 * L_13 = L_12->get_receive_0();
		NullCheck(L_13);
		Action_Invoke_m3801112262(L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_validateSecWebSocketAcceptHeader_m1256291114_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketAcceptHeader_m1256291114 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketAcceptHeader_m1256291114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = ___value0;
		String_t* L_2 = __this->get__base64Key_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		String_t* L_3 = WebSocket_CreateResponseKey_m281547458(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2321347278_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3799711356_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1989381442_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_U3CU3Em__0_m3120152001_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3583450723_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m856096519_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral696342149;
extern Il2CppCodeGenString* _stringLiteral1080887675;
extern Il2CppCodeGenString* _stringLiteral1865761409;
extern Il2CppCodeGenString* _stringLiteral97053799;
extern const uint32_t WebSocket_validateSecWebSocketExtensionsServerHeader_m1069285281_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketExtensionsServerHeader_m1069285281 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketExtensionsServerHeader_m1069285281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	String_t* V_3 = NULL;
	U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * V_4 = NULL;
	bool V_5 = false;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)1;
	}

IL_0008:
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		bool L_3 = __this->get__extensionsRequested_11();
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		uint8_t L_4 = __this->get__compression_4();
		V_0 = (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		String_t* L_5 = ___value0;
		CharU5BU5D_t1328083999* L_6 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Il2CppObject* L_7 = Ext_SplitHeaderValue_m3588285122(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t2321347278_il2cpp_TypeInfo_var, L_7);
		V_2 = L_8;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_011c;
		}

IL_004b:
		{
			Il2CppObject* L_9 = V_2;
			NullCheck(L_9);
			String_t* L_10 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t3799711356_il2cpp_TypeInfo_var, L_9);
			V_1 = L_10;
			String_t* L_11 = V_1;
			NullCheck(L_11);
			String_t* L_12 = String_Trim_m2668767713(L_11, /*hidden argument*/NULL);
			V_3 = L_12;
			bool L_13 = V_0;
			if (!L_13)
			{
				goto IL_0114;
			}
		}

IL_005f:
		{
			String_t* L_14 = V_3;
			uint8_t L_15 = __this->get__compression_4();
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			bool L_16 = Ext_IsCompressionExtension_m2185878088(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_0114;
			}
		}

IL_0070:
		{
			U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * L_17 = (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 *)il2cpp_codegen_object_new(U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577_il2cpp_TypeInfo_var);
			U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5__ctor_m2540079390(L_17, /*hidden argument*/NULL);
			V_4 = L_17;
			String_t* L_18 = V_3;
			NullCheck(L_18);
			bool L_19 = String_Contains_m4017059963(L_18, _stringLiteral696342149, /*hidden argument*/NULL);
			if (L_19)
			{
				goto IL_00a1;
			}
		}

IL_0087:
		{
			Logger_t2598199114 * L_20 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_20);
			Logger_Error_m3326994786(L_20, _stringLiteral1080887675, /*hidden argument*/NULL);
			V_5 = (bool)0;
			IL2CPP_LEAVE(0x13B, FINALLY_012c);
		}

IL_00a1:
		{
			String_t* L_21 = V_3;
			NullCheck(L_21);
			bool L_22 = String_Contains_m4017059963(L_21, _stringLiteral1865761409, /*hidden argument*/NULL);
			if (L_22)
			{
				goto IL_00c3;
			}
		}

IL_00b1:
		{
			Logger_t2598199114 * L_23 = __this->get__logger_24();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_23);
			Logger_Warn_m2119571238(L_23, _stringLiteral97053799, /*hidden argument*/NULL);
		}

IL_00c3:
		{
			U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * L_24 = V_4;
			uint8_t L_25 = __this->get__compression_4();
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			String_t* L_26 = Ext_ToExtensionString_m2837890601(NULL /*static, unused*/, L_25, ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
			NullCheck(L_24);
			L_24->set_method_0(L_26);
			String_t* L_27 = V_3;
			CharU5BU5D_t1328083999* L_28 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_28);
			(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)59));
			Il2CppObject* L_29 = Ext_SplitHeaderValue_m3588285122(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
			U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * L_30 = V_4;
			IntPtr_t L_31;
			L_31.set_m_value_0((void*)(void*)U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_U3CU3Em__0_m3120152001_MethodInfo_var);
			Func_2_t1989381442 * L_32 = (Func_2_t1989381442 *)il2cpp_codegen_object_new(Func_2_t1989381442_il2cpp_TypeInfo_var);
			Func_2__ctor_m3583450723(L_32, L_30, L_31, /*hidden argument*/Func_2__ctor_m3583450723_MethodInfo_var);
			bool L_33 = Ext_Contains_TisString_t_m856096519(NULL /*static, unused*/, L_29, L_32, /*hidden argument*/Ext_Contains_TisString_t_m856096519_MethodInfo_var);
			V_6 = L_33;
			bool L_34 = V_6;
			if (!L_34)
			{
				goto IL_010f;
			}
		}

IL_0107:
		{
			V_5 = (bool)0;
			IL2CPP_LEAVE(0x13B, FINALLY_012c);
		}

IL_010f:
		{
			goto IL_011c;
		}

IL_0114:
		{
			V_5 = (bool)0;
			IL2CPP_LEAVE(0x13B, FINALLY_012c);
		}

IL_011c:
		{
			Il2CppObject* L_35 = V_2;
			NullCheck(L_35);
			bool L_36 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_35);
			if (L_36)
			{
				goto IL_004b;
			}
		}

IL_0127:
		{
			IL2CPP_LEAVE(0x139, FINALLY_012c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_012c;
	}

FINALLY_012c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_37 = V_2;
			if (!L_37)
			{
				goto IL_0138;
			}
		}

IL_0132:
		{
			Il2CppObject* L_38 = V_2;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_38);
		}

IL_0138:
		{
			IL2CPP_END_FINALLY(300)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(300)
	{
		IL2CPP_JUMP_TBL(0x13B, IL_013b)
		IL2CPP_JUMP_TBL(0x139, IL_0139)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0139:
	{
		return (bool)1;
	}

IL_013b:
	{
		bool L_39 = V_5;
		return L_39;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern Il2CppClass* U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1989381442_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_U3CU3Em__0_m1780215862_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3583450723_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m856096519_MethodInfo_var;
extern const uint32_t WebSocket_validateSecWebSocketProtocolServerHeader_m2442699293_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketProtocolServerHeader_m2442699293 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketProtocolServerHeader_m2442699293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * L_0 = (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 *)il2cpp_codegen_object_new(U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450_il2cpp_TypeInfo_var);
		U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6__ctor_m2462649083(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * L_1 = V_0;
		String_t* L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_value_0();
		if (L_4)
		{
			goto IL_0022;
		}
	}
	{
		bool L_5 = __this->get__protocolsRequested_34();
		return (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
	}

IL_0022:
	{
		U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_value_0();
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1606060069(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0034;
		}
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		bool L_9 = __this->get__protocolsRequested_34();
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		StringU5BU5D_t1642385972* L_10 = __this->get__protocols_33();
		U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_U3CU3Em__0_m1780215862_MethodInfo_var);
		Func_2_t1989381442 * L_13 = (Func_2_t1989381442 *)il2cpp_codegen_object_new(Func_2_t1989381442_il2cpp_TypeInfo_var);
		Func_2__ctor_m3583450723(L_13, L_11, L_12, /*hidden argument*/Func_2__ctor_m3583450723_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_14 = Ext_Contains_TisString_t_m856096519(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_10, L_13, /*hidden argument*/Ext_Contains_TisString_t_m856096519_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_14));
		goto IL_0059;
	}

IL_0058:
	{
		G_B7_0 = 0;
	}

IL_0059:
	{
		return (bool)G_B7_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1664928706;
extern const uint32_t WebSocket_validateSecWebSocketVersionServerHeader_m936281861_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketVersionServerHeader_m936281861 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketVersionServerHeader_m936281861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1664928706, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 1;
	}

IL_0014:
	{
		return (bool)G_B3_0;
	}
}
// System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_CreateBase64Key_m3247627260_MetadataUsageId;
extern "C"  String_t* WebSocket_CreateBase64Key_m3247627260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CreateBase64Key_m3247627260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		RandomNumberGenerator_t2510243513 * L_0 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_RandomNumber_49();
		ByteU5BU5D_t3397334013* L_1 = V_0;
		NullCheck(L_0);
		VirtActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_0, L_1);
		ByteU5BU5D_t3397334013* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_3 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* SHA1CryptoServiceProvider_t3913997830_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3821450161;
extern const uint32_t WebSocket_CreateResponseKey_m281547458_MetadataUsageId;
extern "C"  String_t* WebSocket_CreateResponseKey_m281547458 (Il2CppObject * __this /* static, unused */, String_t* ___base64Key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CreateResponseKey_m281547458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	SHA1_t3336793149 * V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	{
		String_t* L_0 = ___base64Key0;
		StringBuilder_t1221177846 * L_1 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1456828835(L_1, L_0, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_1;
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck(L_2);
		StringBuilder_Append_m3636508479(L_2, _stringLiteral3821450161, /*hidden argument*/NULL);
		SHA1CryptoServiceProvider_t3913997830 * L_3 = (SHA1CryptoServiceProvider_t3913997830 *)il2cpp_codegen_object_new(SHA1CryptoServiceProvider_t3913997830_il2cpp_TypeInfo_var);
		SHA1CryptoServiceProvider__ctor_m3127563759(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		SHA1_t3336793149 * L_4 = V_1;
		StringBuilder_t1221177846 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_7 = Ext_UTF8Encode_m2432627974(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_8 = HashAlgorithm_ComputeHash_m3637856778(L_4, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		ByteU5BU5D_t3397334013* L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_10 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void WebSocketSharp.WebSocket::Close()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Close_m790379411_MetadataUsageId;
extern "C"  void WebSocket_Close_m790379411 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m790379411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebSocket_close_m712187051(__this, ((int32_t)1005), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Connect()
extern Il2CppCodeGenString* _stringLiteral485548041;
extern const uint32_t WebSocket_Connect_m1908822439_MetadataUsageId;
extern "C"  void WebSocket_Connect_m1908822439 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Connect_m1908822439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = WebSocket_checkIfAvailable_m2802667577(__this, (bool)1, (bool)0, (bool)1, (bool)0, (bool)0, (bool)1, (&V_0), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		Logger_t2598199114 * L_1 = __this->get__logger_24();
		il2cpp_codegen_memory_barrier();
		String_t* L_2 = V_0;
		NullCheck(L_1);
		Logger_Error_m3326994786(L_1, L_2, /*hidden argument*/NULL);
		WebSocket_error_m3171452371(__this, _stringLiteral485548041, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		bool L_3 = WebSocket_connect_m3298158743(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		WebSocket_open_m476730549(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Send(System.String)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral947848154;
extern Il2CppCodeGenString* _stringLiteral2619694;
extern Il2CppCodeGenString* _stringLiteral2842060801;
extern const uint32_t WebSocket_Send_m4053134035_MetadataUsageId;
extern "C"  void WebSocket_Send_m4053134035 (WebSocket_t3268376029 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m4053134035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		V_0 = _stringLiteral947848154;
		String_t* L_1 = V_0;
		InvalidOperationException_t721527559 * L_2 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		String_t* L_3 = ___data0;
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral2619694, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002c:
	{
		String_t* L_5 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_6 = Ext_TryGetUTF8EncodedBytes_m1396876955(NULL /*static, unused*/, L_5, (&V_1), /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, _stringLiteral2842060801, _stringLiteral2619694, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0049:
	{
		ByteU5BU5D_t3397334013* L_8 = V_1;
		MemoryStream_t743994179 * L_9 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_9, L_8, /*hidden argument*/NULL);
		WebSocket_send_m161023691(__this, 1, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_System_IDisposable_Dispose_m3961917556_MetadataUsageId;
extern "C"  void WebSocket_System_IDisposable_Dispose_m3961917556 (WebSocket_t3268376029 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_System_IDisposable_Dispose_m3961917556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebSocket_close_m712187051(__this, ((int32_t)1001), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::<open>m__0(System.IAsyncResult)
extern const MethodInfo* Action_1_EndInvoke_m239398136_MethodInfo_var;
extern const uint32_t WebSocket_U3CopenU3Em__0_m4266960067_MetadataUsageId;
extern "C"  void WebSocket_U3CopenU3Em__0_m4266960067 (WebSocket_t3268376029 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_U3CopenU3Em__0_m4266960067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t2691851108 * L_0 = __this->get__message_26();
		Il2CppObject * L_1 = ___ar0;
		NullCheck(L_0);
		Action_1_EndInvoke_m239398136(L_0, L_1, /*hidden argument*/Action_1_EndInvoke_m239398136_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::.ctor()
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4__ctor_m3102331381 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::<>m__0()
extern Il2CppClass* Action_1_t566549660_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1729240069_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__1_m2573079210_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2625419694_MethodInfo_var;
extern const MethodInfo* U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__2_m717826270_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2259300693_MethodInfo_var;
extern const uint32_t U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__0_m231510862_MetadataUsageId;
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__0_m231510862 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__0_m231510862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebSocket_t3268376029 * L_0 = __this->get_U24this_1();
		NullCheck(L_0);
		Stream_t3255436806 * L_1 = L_0->get__stream_42();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__1_m2573079210_MethodInfo_var);
		Action_1_t566549660 * L_3 = (Action_1_t566549660 *)il2cpp_codegen_object_new(Action_1_t566549660_il2cpp_TypeInfo_var);
		Action_1__ctor_m2625419694(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2625419694_MethodInfo_var);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__2_m717826270_MethodInfo_var);
		Action_1_t1729240069 * L_5 = (Action_1_t1729240069 *)il2cpp_codegen_object_new(Action_1_t1729240069_il2cpp_TypeInfo_var);
		Action_1__ctor_m2259300693(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m2259300693_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_ReadFrameAsync_m2244208000(NULL /*static, unused*/, L_1, (bool)0, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::<>m__1(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__1_m2573079210 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	ManualResetEvent_t926074657 * V_0 = NULL;
	{
		WebSocket_t3268376029 * L_0 = __this->get_U24this_1();
		WebSocketFrame_t764750278 * L_1 = ___frame0;
		NullCheck(L_0);
		bool L_2 = WebSocket_processReceivedFrame_m2771665755(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		WebSocket_t3268376029 * L_3 = __this->get_U24this_1();
		NullCheck(L_3);
		uint16_t L_4 = L_3->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_003e;
		}
	}

IL_0024:
	{
		WebSocket_t3268376029 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		ManualResetEvent_t926074657 * L_6 = L_5->get__receivingExited_38();
		V_0 = L_6;
		ManualResetEvent_t926074657 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_003d;
		}
	}
	{
		ManualResetEvent_t926074657 * L_8 = V_0;
		NullCheck(L_8);
		EventWaitHandle_Set_m2975776670(L_8, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}

IL_003e:
	{
		Action_t3226471752 * L_9 = __this->get_receive_0();
		NullCheck(L_9);
		Action_Invoke_m3801112262(L_9, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_10 = __this->get_U24this_1();
		NullCheck(L_10);
		bool L_11 = L_10->get__inMessage_23();
		il2cpp_codegen_memory_barrier();
		if (L_11)
		{
			goto IL_007e;
		}
	}
	{
		WebSocket_t3268376029 * L_12 = __this->get_U24this_1();
		NullCheck(L_12);
		bool L_13 = WebSocket_get_HasMessage_m2690126053(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_007e;
		}
	}
	{
		WebSocket_t3268376029 * L_14 = __this->get_U24this_1();
		NullCheck(L_14);
		uint16_t L_15 = L_14->get__readyState_37();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_15) == ((int32_t)1)))
		{
			goto IL_007f;
		}
	}

IL_007e:
	{
		return;
	}

IL_007f:
	{
		WebSocket_t3268376029 * L_16 = __this->get_U24this_1();
		NullCheck(L_16);
		WebSocket_message_m4162534566(L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::<>m__2(System.Exception)
extern Il2CppCodeGenString* _stringLiteral2337153590;
extern const uint32_t U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__2_m717826270_MetadataUsageId;
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__2_m717826270 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, Exception_t1927440687 * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__2_m717826270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebSocket_t3268376029 * L_0 = __this->get_U24this_1();
		NullCheck(L_0);
		Logger_t2598199114 * L_1 = L_0->get__logger_24();
		il2cpp_codegen_memory_barrier();
		Exception_t1927440687 * L_2 = ___ex0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		Logger_Fatal_m3177508194(L_1, L_3, /*hidden argument*/NULL);
		WebSocket_t3268376029 * L_4 = __this->get_U24this_1();
		Exception_t1927440687 * L_5 = ___ex0;
		NullCheck(L_4);
		WebSocket_fatal_m2341174381(L_4, _stringLiteral2337153590, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5::.ctor()
extern "C"  void U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5__ctor_m2540079390 (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5::<>m__0(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral696342149;
extern Il2CppCodeGenString* _stringLiteral1865761409;
extern const uint32_t U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_U3CU3Em__0_m3120152001_MetadataUsageId;
extern "C"  bool U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_U3CU3Em__0_m3120152001 (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577 * __this, String_t* ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_U3CU3Em__0_m3120152001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___t0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m2668767713(L_0, /*hidden argument*/NULL);
		___t0 = L_1;
		String_t* L_2 = ___t0;
		String_t* L_3 = __this->get_method_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_5 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_5, _stringLiteral696342149, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_7 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_7, _stringLiteral1865761409, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_8));
		goto IL_0037;
	}

IL_0036:
	{
		G_B4_0 = 0;
	}

IL_0037:
	{
		return (bool)G_B4_0;
	}
}
// System.Void WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6::.ctor()
extern "C"  void U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6__ctor_m2462649083 (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6::<>m__0(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_U3CU3Em__0_m1780215862_MetadataUsageId;
extern "C"  bool U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_U3CU3Em__0_m1780215862 (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450 * __this, String_t* ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_U3CU3Em__0_m1780215862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___p0;
		String_t* L_1 = __this->get_value_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(System.String)
extern "C"  void WebSocketException__ctor_m2096770588 (WebSocketException_t1348391352 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		WebSocketException__ctor_m4157317458(__this, ((int32_t)1006), L_0, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocketException__ctor_m2973187130 (WebSocketException_t1348391352 * __this, uint16_t ___code0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		WebSocketException__ctor_m4157317458(__this, L_0, (String_t*)NULL, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(System.String,System.Exception)
extern "C"  void WebSocketException__ctor_m2458709252 (WebSocketException_t1348391352 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		WebSocketException__ctor_m4157317458(__this, ((int32_t)1006), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.Exception)
extern "C"  void WebSocketException__ctor_m1123856050 (WebSocketException_t1348391352 * __this, uint16_t ___code0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		Exception_t1927440687 * L_1 = ___innerException1;
		WebSocketException__ctor_m4157317458(__this, L_0, (String_t*)NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String)
extern "C"  void WebSocketException__ctor_m3554051322 (WebSocketException_t1348391352 * __this, uint16_t ___code0, String_t* ___message1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		String_t* L_1 = ___message1;
		WebSocketException__ctor_m4157317458(__this, L_0, L_1, (Exception_t1927440687 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String,System.Exception)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketException__ctor_m4157317458_MetadataUsageId;
extern "C"  void WebSocketException__ctor_m4157317458 (WebSocketException_t1348391352 * __this, uint16_t ___code0, String_t* ___message1, Exception_t1927440687 * ___innerException2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketException__ctor_m4157317458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	WebSocketException_t1348391352 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	WebSocketException_t1348391352 * G_B1_1 = NULL;
	{
		String_t* L_0 = ___message1;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_000f;
		}
	}
	{
		uint16_t L_2 = ___code0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_3 = Ext_GetMessage_m4136468021(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_000f:
	{
		Exception_t1927440687 * L_4 = ___innerException2;
		NullCheck(G_B2_1);
		Exception__ctor_m2453009240(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		uint16_t L_5 = ___code0;
		__this->set__code_11(L_5);
		return;
	}
}
// WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::get_Code()
extern "C"  uint16_t WebSocketException_get_Code_m1814741209 (WebSocketException_t1348391352 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = __this->get__code_11();
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame__cctor_m2029978231_MetadataUsageId;
extern "C"  void WebSocketFrame__cctor_m2029978231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame__cctor_m2029978231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebSocketFrame_t764750278 * L_0 = WebSocketFrame_CreatePingFrame_m1746908400(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_1 = WebSocketFrame_ToArray_m4086645856(L_0, /*hidden argument*/NULL);
		((WebSocketFrame_t764750278_StaticFields*)WebSocketFrame_t764750278_il2cpp_TypeInfo_var->static_fields)->set_EmptyPingBytes_10(L_1);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern "C"  void WebSocketFrame__ctor_m1988968598 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame__ctor_m4039525610_MetadataUsageId;
extern "C"  void WebSocketFrame__ctor_m4039525610 (WebSocketFrame_t764750278 * __this, uint8_t ___fin0, uint8_t ___opcode1, ByteU5BU5D_t3397334013* ___data2, bool ___compressed3, bool ___mask4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame__ctor_m4039525610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0 = ___fin0;
		uint8_t L_1 = ___opcode1;
		ByteU5BU5D_t3397334013* L_2 = ___data2;
		PayloadData_t3839327312 * L_3 = (PayloadData_t3839327312 *)il2cpp_codegen_object_new(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData__ctor_m2204175409(L_3, L_2, /*hidden argument*/NULL);
		bool L_4 = ___compressed3;
		bool L_5 = ___mask4;
		WebSocketFrame__ctor_m2307151800(__this, L_0, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame__ctor_m2307151800_MetadataUsageId;
extern "C"  void WebSocketFrame__ctor_m2307151800 (WebSocketFrame_t764750278 * __this, uint8_t ___fin0, uint8_t ___opcode1, PayloadData_t3839327312 * ___payloadData2, bool ___compressed3, bool ___mask4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame__ctor_m2307151800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	WebSocketFrame_t764750278 * G_B3_0 = NULL;
	WebSocketFrame_t764750278 * G_B1_0 = NULL;
	WebSocketFrame_t764750278 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	WebSocketFrame_t764750278 * G_B4_1 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		uint8_t L_0 = ___fin0;
		__this->set__fin_1(L_0);
		uint8_t L_1 = ___opcode1;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_2 = Ext_IsData_m2145172138(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_2)
		{
			G_B3_0 = __this;
			goto IL_0026;
		}
	}
	{
		bool L_3 = ___compressed3;
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0026;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0027:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__rsv1_7(G_B4_0);
		__this->set__rsv2_8(0);
		__this->set__rsv3_9(0);
		uint8_t L_4 = ___opcode1;
		__this->set__opcode_4(L_4);
		PayloadData_t3839327312 * L_5 = ___payloadData2;
		NullCheck(L_5);
		uint64_t L_6 = PayloadData_get_Length_m4122883639(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		uint64_t L_7 = V_0;
		if ((!(((uint64_t)L_7) < ((uint64_t)(((int64_t)((int64_t)((int32_t)126))))))))
		{
			goto IL_0069;
		}
	}
	{
		uint64_t L_8 = V_0;
		__this->set__payloadLength_6((((int32_t)((uint8_t)L_8))));
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_9 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		__this->set__extPayloadLength_0(L_9);
		goto IL_00a5;
	}

IL_0069:
	{
		uint64_t L_10 = V_0;
		if ((!(((uint64_t)L_10) < ((uint64_t)(((int64_t)((int64_t)((int32_t)65536))))))))
		{
			goto IL_0090;
		}
	}
	{
		__this->set__payloadLength_6(((int32_t)126));
		uint64_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_12 = Ext_InternalToByteArray_m1846763899(NULL /*static, unused*/, (((int32_t)((uint16_t)L_11))), 1, /*hidden argument*/NULL);
		__this->set__extPayloadLength_0(L_12);
		goto IL_00a5;
	}

IL_0090:
	{
		__this->set__payloadLength_6(((int32_t)127));
		uint64_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_14 = Ext_InternalToByteArray_m3991209682(NULL /*static, unused*/, L_13, 1, /*hidden argument*/NULL);
		__this->set__extPayloadLength_0(L_14);
	}

IL_00a5:
	{
		bool L_15 = ___mask4;
		if (!L_15)
		{
			goto IL_00cf;
		}
	}
	{
		__this->set__mask_2(1);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_16 = WebSocketFrame_createMaskingKey_m4228633051(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__maskingKey_3(L_16);
		PayloadData_t3839327312 * L_17 = ___payloadData2;
		ByteU5BU5D_t3397334013* L_18 = __this->get__maskingKey_3();
		NullCheck(L_17);
		PayloadData_Mask_m34083521(L_17, L_18, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_00cf:
	{
		__this->set__mask_2(0);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_19 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		__this->set__maskingKey_3(L_19);
	}

IL_00e1:
	{
		PayloadData_t3839327312 * L_20 = ___payloadData2;
		__this->set__payloadData_5(L_20);
		return;
	}
}
// System.Int32 WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLengthCount()
extern "C"  int32_t WebSocketFrame_get_ExtendedPayloadLengthCount_m522041249 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		uint8_t L_0 = __this->get__payloadLength_6();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)126))))
		{
			goto IL_0013;
		}
	}
	{
		G_B5_0 = 0;
		goto IL_0027;
	}

IL_0013:
	{
		uint8_t L_1 = __this->get__payloadLength_6();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_0026;
		}
	}
	{
		G_B5_0 = 2;
		goto IL_0027;
	}

IL_0026:
	{
		G_B5_0 = 8;
	}

IL_0027:
	{
		return G_B5_0;
	}
}
// System.UInt64 WebSocketSharp.WebSocketFrame::get_FullPayloadLength()
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_get_FullPayloadLength_m3591012120_MetadataUsageId;
extern "C"  uint64_t WebSocketFrame_get_FullPayloadLength_m3591012120 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_get_FullPayloadLength_m3591012120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t G_B5_0 = 0;
	{
		uint8_t L_0 = __this->get__payloadLength_6();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)126))))
		{
			goto IL_0019;
		}
	}
	{
		uint8_t L_1 = __this->get__payloadLength_6();
		G_B5_0 = ((uint64_t)((((int64_t)((uint64_t)L_1)))));
		goto IL_0044;
	}

IL_0019:
	{
		uint8_t L_2 = __this->get__payloadLength_6();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_0038;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_3 = __this->get__extPayloadLength_0();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		uint16_t L_4 = Ext_ToUInt16_m2022554856(NULL /*static, unused*/, L_3, 1, /*hidden argument*/NULL);
		G_B5_0 = ((uint64_t)((((int64_t)((uint64_t)L_4)))));
		goto IL_0044;
	}

IL_0038:
	{
		ByteU5BU5D_t3397334013* L_5 = __this->get__extPayloadLength_0();
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		uint64_t L_6 = Ext_ToUInt64_m2029374714(NULL /*static, unused*/, L_5, 1, /*hidden argument*/NULL);
		G_B5_0 = L_6;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern "C"  bool WebSocketFrame_get_IsClose_m4037370433 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)8))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern "C"  bool WebSocketFrame_get_IsCompressed_m2728001770 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv1_7();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern "C"  bool WebSocketFrame_get_IsContinuation_m4031195624 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern "C"  bool WebSocketFrame_get_IsData_m180465203 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = __this->get__opcode_4();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		uint8_t L_1 = __this->get__opcode_4();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern "C"  bool WebSocketFrame_get_IsFinal_m2923746739 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__fin_1();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragment()
extern "C"  bool WebSocketFrame_get_IsFragment_m2286655259 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = __this->get__fin_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_1 = __this->get__opcode_4();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern "C"  bool WebSocketFrame_get_IsMasked_m373012606 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__mask_2();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern "C"  bool WebSocketFrame_get_IsPing_m1702797379 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)9)))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern "C"  bool WebSocketFrame_get_IsPong_m3078149733 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)10)))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern "C"  bool WebSocketFrame_get_IsText_m3469174916 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern "C"  uint64_t WebSocketFrame_get_Length_m1540738281 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__extPayloadLength_0();
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_1 = __this->get__maskingKey_3();
		NullCheck(L_1);
		PayloadData_t3839327312 * L_2 = __this->get__payloadData_5();
		NullCheck(L_2);
		uint64_t L_3 = PayloadData_get_Length_m4122883639(L_2, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)2)))+(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))))))+(int64_t)L_3));
	}
}
// WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern "C"  uint8_t WebSocketFrame_get_Opcode_m1205139909 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return L_0;
	}
}
// WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern "C"  PayloadData_t3839327312 * WebSocketFrame_get_PayloadData_m4003687813 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		PayloadData_t3839327312 * L_0 = __this->get__payloadData_5();
		return L_0;
	}
}
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern "C"  uint8_t WebSocketFrame_get_Rsv2_m3102243471 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv2_8();
		return L_0;
	}
}
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern "C"  uint8_t WebSocketFrame_get_Rsv3_m353157140 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv3_9();
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_createMaskingKey_m4228633051_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WebSocketFrame_createMaskingKey_m4228633051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_createMaskingKey_m4228633051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		RandomNumberGenerator_t2510243513 * L_0 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_RandomNumber_49();
		ByteU5BU5D_t3397334013* L_1 = V_0;
		NullCheck(L_0);
		VirtActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_0, L_1);
		ByteU5BU5D_t3397334013* L_2 = V_0;
		return L_2;
	}
}
// System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* U3CdumpU3Ec__AnonStorey1_t3897361314_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t1512722501_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CdumpU3Ec__AnonStorey1_U3CU3Em__0_m3543383872_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m486925240_MethodInfo_var;
extern const MethodInfo* Func_1_Invoke_m1190264994_MethodInfo_var;
extern const MethodInfo* Action_4_Invoke_m4025141208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3617471220;
extern Il2CppCodeGenString* _stringLiteral3788868466;
extern Il2CppCodeGenString* _stringLiteral1900311902;
extern Il2CppCodeGenString* _stringLiteral4160209483;
extern Il2CppCodeGenString* _stringLiteral587598288;
extern Il2CppCodeGenString* _stringLiteral1643613597;
extern Il2CppCodeGenString* _stringLiteral2265132950;
extern Il2CppCodeGenString* _stringLiteral3406535737;
extern const uint32_t WebSocketFrame_dump_m4054277966_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_dump_m4054277966 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_dump_m4054277966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CdumpU3Ec__AnonStorey1_t3897361314 * V_0 = NULL;
	uint64_t V_1 = 0;
	int64_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	Func_1_t1512722501 * V_9 = NULL;
	Action_4_t3853297115 * V_10 = NULL;
	ByteU5BU5D_t3397334013* V_11 = NULL;
	int64_t V_12 = 0;
	int64_t V_13 = 0;
	String_t* G_B13_0 = NULL;
	Action_4_t3853297115 * G_B13_1 = NULL;
	String_t* G_B12_0 = NULL;
	Action_4_t3853297115 * G_B12_1 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B14_1 = NULL;
	Action_4_t3853297115 * G_B14_2 = NULL;
	String_t* G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	Action_4_t3853297115 * G_B16_2 = NULL;
	String_t* G_B15_0 = NULL;
	String_t* G_B15_1 = NULL;
	Action_4_t3853297115 * G_B15_2 = NULL;
	String_t* G_B17_0 = NULL;
	String_t* G_B17_1 = NULL;
	String_t* G_B17_2 = NULL;
	Action_4_t3853297115 * G_B17_3 = NULL;
	{
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_0 = (U3CdumpU3Ec__AnonStorey1_t3897361314 *)il2cpp_codegen_object_new(U3CdumpU3Ec__AnonStorey1_t3897361314_il2cpp_TypeInfo_var);
		U3CdumpU3Ec__AnonStorey1__ctor_m284381467(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		WebSocketFrame_t764750278 * L_1 = ___frame0;
		NullCheck(L_1);
		uint64_t L_2 = WebSocketFrame_get_Length_m1540738281(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		uint64_t L_3 = V_1;
		V_2 = ((int64_t)((uint64_t)(int64_t)L_3/(uint64_t)(int64_t)(((int64_t)((int64_t)4)))));
		uint64_t L_4 = V_1;
		V_3 = (((int32_t)((int32_t)((int64_t)((uint64_t)(int64_t)L_4%(uint64_t)(int64_t)(((int64_t)((int64_t)4))))))));
		int64_t L_5 = V_2;
		if ((((int64_t)L_5) >= ((int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))
		{
			goto IL_0033;
		}
	}
	{
		V_4 = 4;
		V_5 = _stringLiteral3617471220;
		goto IL_0077;
	}

IL_0033:
	{
		int64_t L_6 = V_2;
		if ((((int64_t)L_6) >= ((int64_t)(((int64_t)((int64_t)((int32_t)65536)))))))
		{
			goto IL_004e;
		}
	}
	{
		V_4 = 4;
		V_5 = _stringLiteral3788868466;
		goto IL_0077;
	}

IL_004e:
	{
		int64_t L_7 = V_2;
		if ((((int64_t)L_7) >= ((int64_t)((int64_t)4294967296LL))))
		{
			goto IL_006c;
		}
	}
	{
		V_4 = 8;
		V_5 = _stringLiteral1900311902;
		goto IL_0077;
	}

IL_006c:
	{
		V_4 = ((int32_t)16);
		V_5 = _stringLiteral4160209483;
	}

IL_0077:
	{
		int32_t L_8 = V_4;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral587598288, L_10, /*hidden argument*/NULL);
		V_6 = L_11;
		String_t* L_12 = V_6;
		String_t* L_13 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1643613597, L_12, /*hidden argument*/NULL);
		V_7 = L_13;
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_14 = V_0;
		String_t* L_15 = V_5;
		String_t* L_16 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2265132950, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_lineFmt_1(L_16);
		String_t* L_17 = V_6;
		String_t* L_18 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3406535737, L_17, /*hidden argument*/NULL);
		V_8 = L_18;
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_19 = V_0;
		StringBuilder_t1221177846 * L_20 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_20, ((int32_t)64), /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_output_0(L_20);
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_21 = V_0;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)U3CdumpU3Ec__AnonStorey1_U3CU3Em__0_m3543383872_MethodInfo_var);
		Func_1_t1512722501 * L_23 = (Func_1_t1512722501 *)il2cpp_codegen_object_new(Func_1_t1512722501_il2cpp_TypeInfo_var);
		Func_1__ctor_m486925240(L_23, L_21, L_22, /*hidden argument*/Func_1__ctor_m486925240_MethodInfo_var);
		V_9 = L_23;
		Func_1_t1512722501 * L_24 = V_9;
		NullCheck(L_24);
		Action_4_t3853297115 * L_25 = Func_1_Invoke_m1190264994(L_24, /*hidden argument*/Func_1_Invoke_m1190264994_MethodInfo_var);
		V_10 = L_25;
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_26 = V_0;
		NullCheck(L_26);
		StringBuilder_t1221177846 * L_27 = L_26->get_output_0();
		String_t* L_28 = V_7;
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_27);
		StringBuilder_AppendFormat_m3265503696(L_27, L_28, L_29, /*hidden argument*/NULL);
		WebSocketFrame_t764750278 * L_30 = ___frame0;
		NullCheck(L_30);
		ByteU5BU5D_t3397334013* L_31 = WebSocketFrame_ToArray_m4086645856(L_30, /*hidden argument*/NULL);
		V_11 = L_31;
		V_12 = (((int64_t)((int64_t)0)));
		goto IL_01f2;
	}

IL_0100:
	{
		int64_t L_32 = V_12;
		V_13 = ((int64_t)((int64_t)L_32*(int64_t)(((int64_t)((int64_t)4)))));
		int64_t L_33 = V_12;
		int64_t L_34 = V_2;
		if ((((int64_t)L_33) >= ((int64_t)L_34)))
		{
			goto IL_0174;
		}
	}
	{
		Action_4_t3853297115 * L_35 = V_10;
		ByteU5BU5D_t3397334013* L_36 = V_11;
		int64_t L_37 = V_13;
		if ((int64_t)(L_37) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_36);
		intptr_t L_38 = (((intptr_t)L_37));
		uint8_t L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_40 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_39, 2, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = String_PadLeft_m1726975163(L_40, 8, ((int32_t)48), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_42 = V_11;
		int64_t L_43 = V_13;
		if ((int64_t)(((int64_t)((int64_t)L_43+(int64_t)(((int64_t)((int64_t)1)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_42);
		intptr_t L_44 = (((intptr_t)((int64_t)((int64_t)L_43+(int64_t)(((int64_t)((int64_t)1)))))));
		uint8_t L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		String_t* L_46 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_45, 2, /*hidden argument*/NULL);
		NullCheck(L_46);
		String_t* L_47 = String_PadLeft_m1726975163(L_46, 8, ((int32_t)48), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_48 = V_11;
		int64_t L_49 = V_13;
		if ((int64_t)(((int64_t)((int64_t)L_49+(int64_t)(((int64_t)((int64_t)2)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_48);
		intptr_t L_50 = (((intptr_t)((int64_t)((int64_t)L_49+(int64_t)(((int64_t)((int64_t)2)))))));
		uint8_t L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		String_t* L_52 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_51, 2, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_53 = String_PadLeft_m1726975163(L_52, 8, ((int32_t)48), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_54 = V_11;
		int64_t L_55 = V_13;
		if ((int64_t)(((int64_t)((int64_t)L_55+(int64_t)(((int64_t)((int64_t)3)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_54);
		intptr_t L_56 = (((intptr_t)((int64_t)((int64_t)L_55+(int64_t)(((int64_t)((int64_t)3)))))));
		uint8_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		String_t* L_58 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_57, 2, /*hidden argument*/NULL);
		NullCheck(L_58);
		String_t* L_59 = String_PadLeft_m1726975163(L_58, 8, ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_35);
		Action_4_Invoke_m4025141208(L_35, L_41, L_47, L_53, L_59, /*hidden argument*/Action_4_Invoke_m4025141208_MethodInfo_var);
		goto IL_01eb;
	}

IL_0174:
	{
		int32_t L_60 = V_3;
		if ((((int32_t)L_60) <= ((int32_t)0)))
		{
			goto IL_01eb;
		}
	}
	{
		Action_4_t3853297115 * L_61 = V_10;
		ByteU5BU5D_t3397334013* L_62 = V_11;
		int64_t L_63 = V_13;
		if ((int64_t)(L_63) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_62);
		intptr_t L_64 = (((intptr_t)L_63));
		uint8_t L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_66 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_65, 2, /*hidden argument*/NULL);
		NullCheck(L_66);
		String_t* L_67 = String_PadLeft_m1726975163(L_66, 8, ((int32_t)48), /*hidden argument*/NULL);
		int32_t L_68 = V_3;
		G_B12_0 = L_67;
		G_B12_1 = L_61;
		if ((((int32_t)L_68) < ((int32_t)2)))
		{
			G_B13_0 = L_67;
			G_B13_1 = L_61;
			goto IL_01b4;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_69 = V_11;
		int64_t L_70 = V_13;
		if ((int64_t)(((int64_t)((int64_t)L_70+(int64_t)(((int64_t)((int64_t)1)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_69);
		intptr_t L_71 = (((intptr_t)((int64_t)((int64_t)L_70+(int64_t)(((int64_t)((int64_t)1)))))));
		uint8_t L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_73 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_72, 2, /*hidden argument*/NULL);
		NullCheck(L_73);
		String_t* L_74 = String_PadLeft_m1726975163(L_73, 8, ((int32_t)48), /*hidden argument*/NULL);
		G_B14_0 = L_74;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_01b9;
	}

IL_01b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_75 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B14_0 = L_75;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_01b9:
	{
		int32_t L_76 = V_3;
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		G_B15_2 = G_B14_2;
		if ((!(((uint32_t)L_76) == ((uint32_t)3))))
		{
			G_B16_0 = G_B14_0;
			G_B16_1 = G_B14_1;
			G_B16_2 = G_B14_2;
			goto IL_01dc;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_77 = V_11;
		int64_t L_78 = V_13;
		if ((int64_t)(((int64_t)((int64_t)L_78+(int64_t)(((int64_t)((int64_t)2)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_77);
		intptr_t L_79 = (((intptr_t)((int64_t)((int64_t)L_78+(int64_t)(((int64_t)((int64_t)2)))))));
		uint8_t L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_81 = Convert_ToString_m3884855475(NULL /*static, unused*/, L_80, 2, /*hidden argument*/NULL);
		NullCheck(L_81);
		String_t* L_82 = String_PadLeft_m1726975163(L_81, 8, ((int32_t)48), /*hidden argument*/NULL);
		G_B17_0 = L_82;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		goto IL_01e1;
	}

IL_01dc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_83 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B17_0 = L_83;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
	}

IL_01e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(G_B17_3);
		Action_4_Invoke_m4025141208(G_B17_3, G_B17_2, G_B17_1, G_B17_0, L_84, /*hidden argument*/Action_4_Invoke_m4025141208_MethodInfo_var);
	}

IL_01eb:
	{
		int64_t L_85 = V_12;
		V_12 = ((int64_t)((int64_t)L_85+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_01f2:
	{
		int64_t L_86 = V_12;
		int64_t L_87 = V_2;
		if ((((int64_t)L_86) <= ((int64_t)L_87)))
		{
			goto IL_0100;
		}
	}
	{
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_88 = V_0;
		NullCheck(L_88);
		StringBuilder_t1221177846 * L_89 = L_88->get_output_0();
		String_t* L_90 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_89);
		StringBuilder_AppendFormat_m3265503696(L_89, L_90, L_91, /*hidden argument*/NULL);
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_92 = V_0;
		NullCheck(L_92);
		StringBuilder_t1221177846 * L_93 = L_92->get_output_0();
		NullCheck(L_93);
		String_t* L_94 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_93);
		return L_94;
	}
}
// System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Fin_t2752139063_il2cpp_TypeInfo_var;
extern Il2CppClass* Rsv_t1058189029_il2cpp_TypeInfo_var;
extern Il2CppClass* Opcode_t2313788840_il2cpp_TypeInfo_var;
extern Il2CppClass* Mask_t1111889066_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1220271437;
extern Il2CppCodeGenString* _stringLiteral1513299775;
extern const uint32_t WebSocketFrame_print_m1232680307_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_print_m1232680307 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_print_m1232680307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	String_t* V_1 = NULL;
	uint64_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B13_0 = NULL;
	{
		WebSocketFrame_t764750278 * L_0 = ___frame0;
		NullCheck(L_0);
		uint8_t L_1 = L_0->get__payloadLength_6();
		V_0 = L_1;
		uint8_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)125))))
		{
			goto IL_0028;
		}
	}
	{
		WebSocketFrame_t764750278 * L_3 = ___frame0;
		NullCheck(L_3);
		uint64_t L_4 = WebSocketFrame_get_FullPayloadLength_m3591012120(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		String_t* L_5 = UInt64_ToString_m446228920((&V_2), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002d;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
	}

IL_002d:
	{
		V_1 = G_B3_0;
		WebSocketFrame_t764750278 * L_7 = ___frame0;
		NullCheck(L_7);
		ByteU5BU5D_t3397334013* L_8 = L_7->get__maskingKey_3();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		String_t* L_9 = BitConverter_ToString_m927173850(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		uint8_t L_10 = V_0;
		if (L_10)
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B13_0 = L_11;
		goto IL_00a8;
	}

IL_004a:
	{
		uint8_t L_12 = V_0;
		if ((((int32_t)L_12) <= ((int32_t)((int32_t)125))))
		{
			goto IL_005c;
		}
	}
	{
		G_B13_0 = _stringLiteral1220271437;
		goto IL_00a8;
	}

IL_005c:
	{
		WebSocketFrame_t764750278 * L_13 = ___frame0;
		NullCheck(L_13);
		bool L_14 = WebSocketFrame_get_IsText_m3469174916(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009d;
		}
	}
	{
		WebSocketFrame_t764750278 * L_15 = ___frame0;
		NullCheck(L_15);
		bool L_16 = WebSocketFrame_get_IsFragment_m2286655259(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_009d;
		}
	}
	{
		WebSocketFrame_t764750278 * L_17 = ___frame0;
		NullCheck(L_17);
		bool L_18 = WebSocketFrame_get_IsMasked_m373012606(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_009d;
		}
	}
	{
		WebSocketFrame_t764750278 * L_19 = ___frame0;
		NullCheck(L_19);
		bool L_20 = WebSocketFrame_get_IsCompressed_m2728001770(L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_009d;
		}
	}
	{
		WebSocketFrame_t764750278 * L_21 = ___frame0;
		NullCheck(L_21);
		PayloadData_t3839327312 * L_22 = L_21->get__payloadData_5();
		NullCheck(L_22);
		ByteU5BU5D_t3397334013* L_23 = PayloadData_get_ApplicationData_m3068266889(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		String_t* L_24 = Ext_UTF8Decode_m63039028(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		G_B13_0 = L_24;
		goto IL_00a8;
	}

IL_009d:
	{
		WebSocketFrame_t764750278 * L_25 = ___frame0;
		NullCheck(L_25);
		PayloadData_t3839327312 * L_26 = L_25->get__payloadData_5();
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_26);
		G_B13_0 = L_27;
	}

IL_00a8:
	{
		V_4 = G_B13_0;
		V_5 = _stringLiteral1513299775;
		String_t* L_28 = V_5;
		ObjectU5BU5D_t3614634134* L_29 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		WebSocketFrame_t764750278 * L_30 = ___frame0;
		NullCheck(L_30);
		uint8_t L_31 = L_30->get__fin_1();
		uint8_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Fin_t2752139063_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_33);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_33);
		ObjectU5BU5D_t3614634134* L_34 = L_29;
		WebSocketFrame_t764750278 * L_35 = ___frame0;
		NullCheck(L_35);
		uint8_t L_36 = L_35->get__rsv1_7();
		uint8_t L_37 = L_36;
		Il2CppObject * L_38 = Box(Rsv_t1058189029_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, L_38);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t3614634134* L_39 = L_34;
		WebSocketFrame_t764750278 * L_40 = ___frame0;
		NullCheck(L_40);
		uint8_t L_41 = L_40->get__rsv2_8();
		uint8_t L_42 = L_41;
		Il2CppObject * L_43 = Box(Rsv_t1058189029_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_43);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_43);
		ObjectU5BU5D_t3614634134* L_44 = L_39;
		WebSocketFrame_t764750278 * L_45 = ___frame0;
		NullCheck(L_45);
		uint8_t L_46 = L_45->get__rsv3_9();
		uint8_t L_47 = L_46;
		Il2CppObject * L_48 = Box(Rsv_t1058189029_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_48);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_48);
		ObjectU5BU5D_t3614634134* L_49 = L_44;
		WebSocketFrame_t764750278 * L_50 = ___frame0;
		NullCheck(L_50);
		uint8_t L_51 = L_50->get__opcode_4();
		uint8_t L_52 = L_51;
		Il2CppObject * L_53 = Box(Opcode_t2313788840_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_53);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_53);
		ObjectU5BU5D_t3614634134* L_54 = L_49;
		WebSocketFrame_t764750278 * L_55 = ___frame0;
		NullCheck(L_55);
		uint8_t L_56 = L_55->get__mask_2();
		uint8_t L_57 = L_56;
		Il2CppObject * L_58 = Box(Mask_t1111889066_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_58);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_58);
		ObjectU5BU5D_t3614634134* L_59 = L_54;
		uint8_t L_60 = V_0;
		uint8_t L_61 = L_60;
		Il2CppObject * L_62 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_61);
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, L_62);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_62);
		ObjectU5BU5D_t3614634134* L_63 = L_59;
		String_t* L_64 = V_1;
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, L_64);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_64);
		ObjectU5BU5D_t3614634134* L_65 = L_63;
		String_t* L_66 = V_3;
		NullCheck(L_65);
		ArrayElementTypeCheck (L_65, L_66);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_66);
		ObjectU5BU5D_t3614634134* L_67 = L_65;
		String_t* L_68 = V_4;
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, L_68);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_68);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = String_Format_m1263743648(NULL /*static, unused*/, L_28, L_67, /*hidden argument*/NULL);
		return L_69;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::processHeader(System.Byte[])
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2625584160;
extern Il2CppCodeGenString* _stringLiteral2655762422;
extern Il2CppCodeGenString* _stringLiteral130907672;
extern Il2CppCodeGenString* _stringLiteral227054134;
extern Il2CppCodeGenString* _stringLiteral1884446940;
extern const uint32_t WebSocketFrame_processHeader_m4003474107_MetadataUsageId;
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_processHeader_m4003474107 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___header0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_processHeader_m4003474107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0;
	uint8_t V_1 = 0;
	uint8_t V_2 = 0;
	uint8_t V_3 = 0;
	uint8_t V_4 = 0x0;
	uint8_t V_5 = 0;
	uint8_t V_6 = 0x0;
	String_t* V_7 = NULL;
	WebSocketFrame_t764750278 * V_8 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B17_0 = 0;
	String_t* G_B29_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___header0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		WebSocketException_t1348391352 * L_1 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_1, _stringLiteral2625584160, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ByteU5BU5D_t3397334013* L_2 = ___header0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		uint8_t L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((!(((uint32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)128)))) == ((uint32_t)((int32_t)128)))))
		{
			goto IL_002d;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_002e;
	}

IL_002d:
	{
		G_B5_0 = 0;
	}

IL_002e:
	{
		V_0 = G_B5_0;
		ByteU5BU5D_t3397334013* L_5 = ___header0;
		NullCheck(L_5);
		int32_t L_6 = 0;
		uint8_t L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if ((!(((uint32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)64)))) == ((uint32_t)((int32_t)64)))))
		{
			goto IL_0042;
		}
	}
	{
		G_B8_0 = 1;
		goto IL_0043;
	}

IL_0042:
	{
		G_B8_0 = 0;
	}

IL_0043:
	{
		V_1 = G_B8_0;
		ByteU5BU5D_t3397334013* L_8 = ___header0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		uint8_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		if ((!(((uint32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)32)))) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_0057;
		}
	}
	{
		G_B11_0 = 1;
		goto IL_0058;
	}

IL_0057:
	{
		G_B11_0 = 0;
	}

IL_0058:
	{
		V_2 = G_B11_0;
		ByteU5BU5D_t3397334013* L_11 = ___header0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		uint8_t L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		if ((!(((uint32_t)((int32_t)((int32_t)L_13&(int32_t)((int32_t)16)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_006c;
		}
	}
	{
		G_B14_0 = 1;
		goto IL_006d;
	}

IL_006c:
	{
		G_B14_0 = 0;
	}

IL_006d:
	{
		V_3 = G_B14_0;
		ByteU5BU5D_t3397334013* L_14 = ___header0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		uint8_t L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_4 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)15))))));
		ByteU5BU5D_t3397334013* L_17 = ___header0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		uint8_t L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		if ((!(((uint32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)128)))) == ((uint32_t)((int32_t)128)))))
		{
			goto IL_0090;
		}
	}
	{
		G_B17_0 = 1;
		goto IL_0091;
	}

IL_0090:
	{
		G_B17_0 = 0;
	}

IL_0091:
	{
		V_5 = G_B17_0;
		ByteU5BU5D_t3397334013* L_20 = ___header0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		uint8_t L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_6 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_22&(int32_t)((int32_t)127))))));
		uint8_t L_23 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_24 = Ext_IsSupported_m2133247636(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00b2;
		}
	}
	{
		G_B29_0 = _stringLiteral2655762422;
		goto IL_010b;
	}

IL_00b2:
	{
		uint8_t L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_26 = Ext_IsData_m1914395218(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00cf;
		}
	}
	{
		uint8_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)1))))
		{
			goto IL_00cf;
		}
	}
	{
		G_B29_0 = _stringLiteral130907672;
		goto IL_010b;
	}

IL_00cf:
	{
		uint8_t L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_29 = Ext_IsControl_m1453602925(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00eb;
		}
	}
	{
		uint8_t L_30 = V_0;
		if (L_30)
		{
			goto IL_00eb;
		}
	}
	{
		G_B29_0 = _stringLiteral227054134;
		goto IL_010b;
	}

IL_00eb:
	{
		uint8_t L_31 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		bool L_32 = Ext_IsControl_m1453602925(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_010a;
		}
	}
	{
		uint8_t L_33 = V_6;
		if ((((int32_t)L_33) <= ((int32_t)((int32_t)125))))
		{
			goto IL_010a;
		}
	}
	{
		G_B29_0 = _stringLiteral1884446940;
		goto IL_010b;
	}

IL_010a:
	{
		G_B29_0 = ((String_t*)(NULL));
	}

IL_010b:
	{
		V_7 = G_B29_0;
		String_t* L_34 = V_7;
		if (!L_34)
		{
			goto IL_0121;
		}
	}
	{
		String_t* L_35 = V_7;
		WebSocketException_t1348391352 * L_36 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3554051322(L_36, ((int32_t)1002), L_35, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_0121:
	{
		WebSocketFrame_t764750278 * L_37 = (WebSocketFrame_t764750278 *)il2cpp_codegen_object_new(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1988968598(L_37, /*hidden argument*/NULL);
		V_8 = L_37;
		WebSocketFrame_t764750278 * L_38 = V_8;
		uint8_t L_39 = V_0;
		NullCheck(L_38);
		L_38->set__fin_1(L_39);
		WebSocketFrame_t764750278 * L_40 = V_8;
		uint8_t L_41 = V_1;
		NullCheck(L_40);
		L_40->set__rsv1_7(L_41);
		WebSocketFrame_t764750278 * L_42 = V_8;
		uint8_t L_43 = V_2;
		NullCheck(L_42);
		L_42->set__rsv2_8(L_43);
		WebSocketFrame_t764750278 * L_44 = V_8;
		uint8_t L_45 = V_3;
		NullCheck(L_44);
		L_44->set__rsv3_9(L_45);
		WebSocketFrame_t764750278 * L_46 = V_8;
		uint8_t L_47 = V_4;
		NullCheck(L_46);
		L_46->set__opcode_4(L_47);
		WebSocketFrame_t764750278 * L_48 = V_8;
		uint8_t L_49 = V_5;
		NullCheck(L_48);
		L_48->set__mask_2(L_49);
		WebSocketFrame_t764750278 * L_50 = V_8;
		uint8_t L_51 = V_6;
		NullCheck(L_50);
		L_50->set__payloadLength_6(L_51);
		WebSocketFrame_t764750278 * L_52 = V_8;
		return L_52;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3199133395_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern const MethodInfo* U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_U3CU3Em__0_m1260491982_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1814062808_MethodInfo_var;
extern const uint32_t WebSocketFrame_readExtendedPayloadLengthAsync_m1529395252_MetadataUsageId;
extern "C"  void WebSocketFrame_readExtendedPayloadLengthAsync_m1529395252 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, WebSocketFrame_t764750278 * ___frame1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_readExtendedPayloadLengthAsync_m1529395252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * V_0 = NULL;
	{
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_0 = (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 *)il2cpp_codegen_object_new(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925_il2cpp_TypeInfo_var);
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3__ctor_m3809299408(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_1 = V_0;
		WebSocketFrame_t764750278 * L_2 = ___frame1;
		NullCheck(L_1);
		L_1->set_frame_1(L_2);
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_3 = V_0;
		Action_1_t566549660 * L_4 = ___completed2;
		NullCheck(L_3);
		L_3->set_completed_2(L_4);
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_5 = V_0;
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_6 = V_0;
		NullCheck(L_6);
		WebSocketFrame_t764750278 * L_7 = L_6->get_frame_1();
		NullCheck(L_7);
		int32_t L_8 = WebSocketFrame_get_ExtendedPayloadLengthCount_m522041249(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_len_0(L_8);
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_len_0();
		if (L_10)
		{
			goto IL_0052;
		}
	}
	{
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_11 = V_0;
		NullCheck(L_11);
		WebSocketFrame_t764750278 * L_12 = L_11->get_frame_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_13 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		NullCheck(L_12);
		L_12->set__extPayloadLength_0(L_13);
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_14 = V_0;
		NullCheck(L_14);
		Action_1_t566549660 * L_15 = L_14->get_completed_2();
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_16 = V_0;
		NullCheck(L_16);
		WebSocketFrame_t764750278 * L_17 = L_16->get_frame_1();
		NullCheck(L_15);
		Action_1_Invoke_m2464278655(L_15, L_17, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}

IL_0052:
	{
		Stream_t3255436806 * L_18 = ___stream0;
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_len_0();
		U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * L_21 = V_0;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_U3CU3Em__0_m1260491982_MethodInfo_var);
		Action_1_t3199133395 * L_23 = (Action_1_t3199133395 *)il2cpp_codegen_object_new(Action_1_t3199133395_il2cpp_TypeInfo_var);
		Action_1__ctor_m1814062808(L_23, L_21, L_22, /*hidden argument*/Action_1__ctor_m1814062808_MethodInfo_var);
		Action_1_t1729240069 * L_24 = ___error3;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_ReadBytesAsync_m2123500857(NULL /*static, unused*/, L_18, L_20, L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3199133395_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CreadHeaderAsyncU3Ec__AnonStorey4_U3CU3Em__0_m376984049_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1814062808_MethodInfo_var;
extern const uint32_t WebSocketFrame_readHeaderAsync_m4057604783_MetadataUsageId;
extern "C"  void WebSocketFrame_readHeaderAsync_m4057604783 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, Action_1_t566549660 * ___completed1, Action_1_t1729240069 * ___error2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_readHeaderAsync_m4057604783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * V_0 = NULL;
	{
		U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * L_0 = (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 *)il2cpp_codegen_object_new(U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074_il2cpp_TypeInfo_var);
		U3CreadHeaderAsyncU3Ec__AnonStorey4__ctor_m1250933485(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * L_1 = V_0;
		Action_1_t566549660 * L_2 = ___completed1;
		NullCheck(L_1);
		L_1->set_completed_0(L_2);
		Stream_t3255436806 * L_3 = ___stream0;
		U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CreadHeaderAsyncU3Ec__AnonStorey4_U3CU3Em__0_m376984049_MethodInfo_var);
		Action_1_t3199133395 * L_6 = (Action_1_t3199133395 *)il2cpp_codegen_object_new(Action_1_t3199133395_il2cpp_TypeInfo_var);
		Action_1__ctor_m1814062808(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m1814062808_MethodInfo_var);
		Action_1_t1729240069 * L_7 = ___error2;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_ReadBytesAsync_m2123500857(NULL /*static, unused*/, L_3, 2, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3199133395_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern const MethodInfo* U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3284532258_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1814062808_MethodInfo_var;
extern const uint32_t WebSocketFrame_readMaskingKeyAsync_m1840909340_MetadataUsageId;
extern "C"  void WebSocketFrame_readMaskingKeyAsync_m1840909340 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, WebSocketFrame_t764750278 * ___frame1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_readMaskingKeyAsync_m1840909340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * V_0 = NULL;
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * G_B2_0 = NULL;
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * G_B3_1 = NULL;
	{
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_0 = (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 *)il2cpp_codegen_object_new(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521_il2cpp_TypeInfo_var);
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5__ctor_m135055404(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_1 = V_0;
		WebSocketFrame_t764750278 * L_2 = ___frame1;
		NullCheck(L_1);
		L_1->set_frame_1(L_2);
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_3 = V_0;
		Action_1_t566549660 * L_4 = ___completed2;
		NullCheck(L_3);
		L_3->set_completed_2(L_4);
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_5 = V_0;
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_6 = V_0;
		NullCheck(L_6);
		WebSocketFrame_t764750278 * L_7 = L_6->get_frame_1();
		NullCheck(L_7);
		bool L_8 = WebSocketFrame_get_IsMasked_m373012606(L_7, /*hidden argument*/NULL);
		G_B1_0 = L_5;
		if (!L_8)
		{
			G_B2_0 = L_5;
			goto IL_002b;
		}
	}
	{
		G_B3_0 = 4;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_len_0(G_B3_0);
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_len_0();
		if (L_10)
		{
			goto IL_005e;
		}
	}
	{
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_11 = V_0;
		NullCheck(L_11);
		WebSocketFrame_t764750278 * L_12 = L_11->get_frame_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_13 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		NullCheck(L_12);
		L_12->set__maskingKey_3(L_13);
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_14 = V_0;
		NullCheck(L_14);
		Action_1_t566549660 * L_15 = L_14->get_completed_2();
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_16 = V_0;
		NullCheck(L_16);
		WebSocketFrame_t764750278 * L_17 = L_16->get_frame_1();
		NullCheck(L_15);
		Action_1_Invoke_m2464278655(L_15, L_17, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}

IL_005e:
	{
		Stream_t3255436806 * L_18 = ___stream0;
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_len_0();
		U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * L_21 = V_0;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3284532258_MethodInfo_var);
		Action_1_t3199133395 * L_23 = (Action_1_t3199133395 *)il2cpp_codegen_object_new(Action_1_t3199133395_il2cpp_TypeInfo_var);
		Action_1__ctor_m1814062808(L_23, L_21, L_22, /*hidden argument*/Action_1__ctor_m1814062808_MethodInfo_var);
		Action_1_t1729240069 * L_24 = ___error3;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_ReadBytesAsync_m2123500857(NULL /*static, unused*/, L_18, L_20, L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3199133395_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern const MethodInfo* U3CreadPayloadDataAsyncU3Ec__AnonStorey6_U3CU3Em__0_m928418714_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1814062808_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral37365029;
extern const uint32_t WebSocketFrame_readPayloadDataAsync_m3818797513_MetadataUsageId;
extern "C"  void WebSocketFrame_readPayloadDataAsync_m3818797513 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, WebSocketFrame_t764750278 * ___frame1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_readPayloadDataAsync_m3818797513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * V_0 = NULL;
	uint64_t V_1 = 0;
	Action_1_t3199133395 * V_2 = NULL;
	{
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_0 = (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 *)il2cpp_codegen_object_new(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767_il2cpp_TypeInfo_var);
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6__ctor_m4228040884(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_1 = V_0;
		WebSocketFrame_t764750278 * L_2 = ___frame1;
		NullCheck(L_1);
		L_1->set_frame_1(L_2);
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_3 = V_0;
		Action_1_t566549660 * L_4 = ___completed2;
		NullCheck(L_3);
		L_3->set_completed_2(L_4);
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_5 = V_0;
		NullCheck(L_5);
		WebSocketFrame_t764750278 * L_6 = L_5->get_frame_1();
		NullCheck(L_6);
		uint64_t L_7 = WebSocketFrame_get_FullPayloadLength_m3591012120(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		uint64_t L_8 = V_1;
		if ((!(((uint64_t)L_8) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_004a;
		}
	}
	{
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_9 = V_0;
		NullCheck(L_9);
		WebSocketFrame_t764750278 * L_10 = L_9->get_frame_1();
		IL2CPP_RUNTIME_CLASS_INIT(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData_t3839327312 * L_11 = ((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->get_Empty_7();
		NullCheck(L_10);
		L_10->set__payloadData_5(L_11);
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_12 = V_0;
		NullCheck(L_12);
		Action_1_t566549660 * L_13 = L_12->get_completed_2();
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_14 = V_0;
		NullCheck(L_14);
		WebSocketFrame_t764750278 * L_15 = L_14->get_frame_1();
		NullCheck(L_13);
		Action_1_Invoke_m2464278655(L_13, L_15, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}

IL_004a:
	{
		uint64_t L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		uint64_t L_17 = ((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->get_MaxLength_8();
		if ((!(((uint64_t)L_16) > ((uint64_t)L_17))))
		{
			goto IL_0065;
		}
	}
	{
		WebSocketException_t1348391352 * L_18 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3554051322(L_18, ((int32_t)1009), _stringLiteral37365029, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0065:
	{
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_19 = V_0;
		uint64_t L_20 = V_1;
		NullCheck(L_19);
		L_19->set_llen_0(L_20);
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_21 = V_0;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)U3CreadPayloadDataAsyncU3Ec__AnonStorey6_U3CU3Em__0_m928418714_MethodInfo_var);
		Action_1_t3199133395 * L_23 = (Action_1_t3199133395 *)il2cpp_codegen_object_new(Action_1_t3199133395_il2cpp_TypeInfo_var);
		Action_1__ctor_m1814062808(L_23, L_21, L_22, /*hidden argument*/Action_1__ctor_m1814062808_MethodInfo_var);
		V_2 = L_23;
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_24 = V_0;
		NullCheck(L_24);
		WebSocketFrame_t764750278 * L_25 = L_24->get_frame_1();
		NullCheck(L_25);
		uint8_t L_26 = L_25->get__payloadLength_6();
		if ((((int32_t)L_26) >= ((int32_t)((int32_t)127))))
		{
			goto IL_0096;
		}
	}
	{
		Stream_t3255436806 * L_27 = ___stream0;
		uint64_t L_28 = V_1;
		Action_1_t3199133395 * L_29 = V_2;
		Action_1_t1729240069 * L_30 = ___error3;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_ReadBytesAsync_m2123500857(NULL /*static, unused*/, L_27, (((int32_t)((int32_t)L_28))), L_29, L_30, /*hidden argument*/NULL);
		return;
	}

IL_0096:
	{
		Stream_t3255436806 * L_31 = ___stream0;
		U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * L_32 = V_0;
		NullCheck(L_32);
		int64_t L_33 = L_32->get_llen_0();
		Action_1_t3199133395 * L_34 = V_2;
		Action_1_t1729240069 * L_35 = ___error3;
		IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
		Ext_ReadBytesAsync_m2468226979(NULL /*static, unused*/, L_31, L_33, ((int32_t)1024), L_34, L_35, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.PayloadData,System.Boolean)
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreateCloseFrame_m2686217177_MetadataUsageId;
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_CreateCloseFrame_m2686217177 (Il2CppObject * __this /* static, unused */, PayloadData_t3839327312 * ___payloadData0, bool ___mask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreateCloseFrame_m2686217177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PayloadData_t3839327312 * L_0 = ___payloadData0;
		bool L_1 = ___mask1;
		WebSocketFrame_t764750278 * L_2 = (WebSocketFrame_t764750278 *)il2cpp_codegen_object_new(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m2307151800(L_2, 1, 8, L_0, (bool)0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(System.Boolean)
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreatePingFrame_m1746908400_MetadataUsageId;
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_CreatePingFrame_m1746908400 (Il2CppObject * __this /* static, unused */, bool ___mask0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreatePingFrame_m1746908400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData_t3839327312 * L_0 = ((PayloadData_t3839327312_StaticFields*)PayloadData_t3839327312_il2cpp_TypeInfo_var->static_fields)->get_Empty_7();
		bool L_1 = ___mask0;
		WebSocketFrame_t764750278 * L_2 = (WebSocketFrame_t764750278 *)il2cpp_codegen_object_new(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m2307151800(L_2, 1, ((int32_t)9), L_0, (bool)0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.PayloadData,System.Boolean)
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreatePongFrame_m3364323429_MetadataUsageId;
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_CreatePongFrame_m3364323429 (Il2CppObject * __this /* static, unused */, PayloadData_t3839327312 * ___payloadData0, bool ___mask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreatePongFrame_m3364323429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PayloadData_t3839327312 * L_0 = ___payloadData0;
		bool L_1 = ___mask1;
		WebSocketFrame_t764750278 * L_2 = (WebSocketFrame_t764750278 *)il2cpp_codegen_object_new(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m2307151800(L_2, 1, ((int32_t)10), L_0, (bool)0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t566549660_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__0_m1065408216_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2625419694_MethodInfo_var;
extern const uint32_t WebSocketFrame_ReadFrameAsync_m2244208000_MetadataUsageId;
extern "C"  void WebSocketFrame_ReadFrameAsync_m2244208000 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, bool ___unmask1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ReadFrameAsync_m2244208000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * V_0 = NULL;
	{
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_0 = (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 *)il2cpp_codegen_object_new(U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881_il2cpp_TypeInfo_var);
		U3CReadFrameAsyncU3Ec__AnonStorey7__ctor_m1437034700(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_1 = V_0;
		Stream_t3255436806 * L_2 = ___stream0;
		NullCheck(L_1);
		L_1->set_stream_0(L_2);
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_3 = V_0;
		Action_1_t1729240069 * L_4 = ___error3;
		NullCheck(L_3);
		L_3->set_error_1(L_4);
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_5 = V_0;
		bool L_6 = ___unmask1;
		NullCheck(L_5);
		L_5->set_unmask_2(L_6);
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_7 = V_0;
		Action_1_t566549660 * L_8 = ___completed2;
		NullCheck(L_7);
		L_7->set_completed_3(L_8);
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_9 = V_0;
		NullCheck(L_9);
		Stream_t3255436806 * L_10 = L_9->get_stream_0();
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__0_m1065408216_MethodInfo_var);
		Action_1_t566549660 * L_13 = (Action_1_t566549660 *)il2cpp_codegen_object_new(Action_1_t566549660_il2cpp_TypeInfo_var);
		Action_1__ctor_m2625419694(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m2625419694_MethodInfo_var);
		U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * L_14 = V_0;
		NullCheck(L_14);
		Action_1_t1729240069 * L_15 = L_14->get_error_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_readHeaderAsync_m4057604783(NULL /*static, unused*/, L_10, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::Unmask()
extern Il2CppClass* WebSocket_t3268376029_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_Unmask_m217551411_MetadataUsageId;
extern "C"  void WebSocketFrame_Unmask_m217551411 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_Unmask_m217551411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0 = __this->get__mask_2();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set__mask_2(0);
		PayloadData_t3839327312 * L_1 = __this->get__payloadData_5();
		ByteU5BU5D_t3397334013* L_2 = __this->get__maskingKey_3();
		NullCheck(L_1);
		PayloadData_Mask_m34083521(L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t3268376029_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_3 = ((WebSocket_t3268376029_StaticFields*)WebSocket_t3268376029_il2cpp_TypeInfo_var->static_fields)->get_EmptyBytes_47();
		__this->set__maskingKey_3(L_3);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator0_t908706979_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_GetEnumerator_m2330151961_MetadataUsageId;
extern "C"  Il2CppObject* WebSocketFrame_GetEnumerator_m2330151961 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_GetEnumerator_m2330151961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator0_t908706979 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t908706979 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t908706979 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator0_t908706979_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator0__ctor_m3915779002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t908706979 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t908706979 * L_2 = V_0;
		return L_2;
	}
}
// System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_PrintToString_m2233387245_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_PrintToString_m2233387245 (WebSocketFrame_t764750278 * __this, bool ___dumped0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_PrintToString_m2233387245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___dumped0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		String_t* L_1 = WebSocketFrame_dump_m4054277966(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		String_t* L_2 = WebSocketFrame_print_m1232680307(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketFrame::ToArray()
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Ext_t870230697_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_ToArray_m4086645856_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WebSocketFrame_ToArray_m4086645856 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ToArray_m4086645856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B4_0 = 0;
	ByteU5BU5D_t3397334013* G_B4_1 = NULL;
	MemoryStream_t743994179 * G_B4_2 = NULL;
	int32_t G_B3_0 = 0;
	ByteU5BU5D_t3397334013* G_B3_1 = NULL;
	MemoryStream_t743994179 * G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B5_1 = 0;
	ByteU5BU5D_t3397334013* G_B5_2 = NULL;
	MemoryStream_t743994179 * G_B5_3 = NULL;
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			uint8_t L_1 = __this->get__fin_1();
			V_1 = L_1;
			int32_t L_2 = V_1;
			uint8_t L_3 = __this->get__rsv1_7();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)1))+(int32_t)L_3));
			int32_t L_4 = V_1;
			uint8_t L_5 = __this->get__rsv2_8();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)1))+(int32_t)L_5));
			int32_t L_6 = V_1;
			uint8_t L_7 = __this->get__rsv3_9();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6<<(int32_t)1))+(int32_t)L_7));
			int32_t L_8 = V_1;
			uint8_t L_9 = __this->get__opcode_4();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_8<<(int32_t)4))+(int32_t)L_9));
			int32_t L_10 = V_1;
			uint8_t L_11 = __this->get__mask_2();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_10<<(int32_t)1))+(int32_t)L_11));
			int32_t L_12 = V_1;
			uint8_t L_13 = __this->get__payloadLength_6();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12<<(int32_t)7))+(int32_t)L_13));
			MemoryStream_t743994179 * L_14 = V_0;
			int32_t L_15 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_16 = Ext_InternalToByteArray_m1846763899(NULL /*static, unused*/, (((int32_t)((uint16_t)L_15))), 1, /*hidden argument*/NULL);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_16, 0, 2);
			uint8_t L_17 = __this->get__payloadLength_6();
			if ((((int32_t)L_17) <= ((int32_t)((int32_t)125))))
			{
				goto IL_008d;
			}
		}

IL_006c:
		{
			MemoryStream_t743994179 * L_18 = V_0;
			ByteU5BU5D_t3397334013* L_19 = __this->get__extPayloadLength_0();
			uint8_t L_20 = __this->get__payloadLength_6();
			G_B3_0 = 0;
			G_B3_1 = L_19;
			G_B3_2 = L_18;
			if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)126)))))
			{
				G_B4_0 = 0;
				G_B4_1 = L_19;
				G_B4_2 = L_18;
				goto IL_0087;
			}
		}

IL_0081:
		{
			G_B5_0 = 2;
			G_B5_1 = G_B3_0;
			G_B5_2 = G_B3_1;
			G_B5_3 = G_B3_2;
			goto IL_0088;
		}

IL_0087:
		{
			G_B5_0 = 8;
			G_B5_1 = G_B4_0;
			G_B5_2 = G_B4_1;
			G_B5_3 = G_B4_2;
		}

IL_0088:
		{
			NullCheck(G_B5_3);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, G_B5_3, G_B5_2, G_B5_1, G_B5_0);
		}

IL_008d:
		{
			uint8_t L_21 = __this->get__mask_2();
			if ((!(((uint32_t)L_21) == ((uint32_t)1))))
			{
				goto IL_00a7;
			}
		}

IL_0099:
		{
			MemoryStream_t743994179 * L_22 = V_0;
			ByteU5BU5D_t3397334013* L_23 = __this->get__maskingKey_3();
			NullCheck(L_22);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_22, L_23, 0, 4);
		}

IL_00a7:
		{
			uint8_t L_24 = __this->get__payloadLength_6();
			if ((((int32_t)L_24) <= ((int32_t)0)))
			{
				goto IL_00e8;
			}
		}

IL_00b3:
		{
			PayloadData_t3839327312 * L_25 = __this->get__payloadData_5();
			NullCheck(L_25);
			ByteU5BU5D_t3397334013* L_26 = PayloadData_ToArray_m2512776348(L_25, /*hidden argument*/NULL);
			V_2 = L_26;
			uint8_t L_27 = __this->get__payloadLength_6();
			if ((((int32_t)L_27) >= ((int32_t)((int32_t)127))))
			{
				goto IL_00dc;
			}
		}

IL_00cc:
		{
			MemoryStream_t743994179 * L_28 = V_0;
			ByteU5BU5D_t3397334013* L_29 = V_2;
			ByteU5BU5D_t3397334013* L_30 = V_2;
			NullCheck(L_30);
			NullCheck(L_28);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_28, L_29, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))));
			goto IL_00e8;
		}

IL_00dc:
		{
			MemoryStream_t743994179 * L_31 = V_0;
			ByteU5BU5D_t3397334013* L_32 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Ext_t870230697_il2cpp_TypeInfo_var);
			Ext_WriteBytes_m3545246308(NULL /*static, unused*/, L_31, L_32, ((int32_t)1024), /*hidden argument*/NULL);
		}

IL_00e8:
		{
			MemoryStream_t743994179 * L_33 = V_0;
			NullCheck(L_33);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_33);
			MemoryStream_t743994179 * L_34 = V_0;
			NullCheck(L_34);
			ByteU5BU5D_t3397334013* L_35 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_34);
			V_3 = L_35;
			IL2CPP_LEAVE(0x107, FINALLY_00fa);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00fa;
	}

FINALLY_00fa:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_36 = V_0;
			if (!L_36)
			{
				goto IL_0106;
			}
		}

IL_0100:
		{
			MemoryStream_t743994179 * L_37 = V_0;
			NullCheck(L_37);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_37);
		}

IL_0106:
		{
			IL2CPP_END_FINALLY(250)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(250)
	{
		IL2CPP_JUMP_TBL(0x107, IL_0107)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0107:
	{
		ByteU5BU5D_t3397334013* L_38 = V_3;
		return L_38;
	}
}
// System.String WebSocketSharp.WebSocketFrame::ToString()
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_ToString_m3964319027_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_ToString_m3964319027 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ToString_m3964319027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = WebSocketFrame_ToArray_m4086645856(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		String_t* L_1 = BitConverter_ToString_m927173850(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m3598500533 (WebSocketFrame_t764750278 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = WebSocketFrame_GetEnumerator_m2330151961(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey1__ctor_m284381467 (U3CdumpU3Ec__AnonStorey1_t3897361314 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::<>m__0()
extern Il2CppClass* U3CdumpU3Ec__AnonStorey2_t1475667190_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_4_t3853297115_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CdumpU3Ec__AnonStorey2_U3CU3Em__0_m2509098988_MethodInfo_var;
extern const MethodInfo* Action_4__ctor_m562450268_MethodInfo_var;
extern const uint32_t U3CdumpU3Ec__AnonStorey1_U3CU3Em__0_m3543383872_MetadataUsageId;
extern "C"  Action_4_t3853297115 * U3CdumpU3Ec__AnonStorey1_U3CU3Em__0_m3543383872 (U3CdumpU3Ec__AnonStorey1_t3897361314 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CdumpU3Ec__AnonStorey1_U3CU3Em__0_m3543383872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CdumpU3Ec__AnonStorey2_t1475667190 * V_0 = NULL;
	{
		U3CdumpU3Ec__AnonStorey2_t1475667190 * L_0 = (U3CdumpU3Ec__AnonStorey2_t1475667190 *)il2cpp_codegen_object_new(U3CdumpU3Ec__AnonStorey2_t1475667190_il2cpp_TypeInfo_var);
		U3CdumpU3Ec__AnonStorey2__ctor_m1935026999(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CdumpU3Ec__AnonStorey2_t1475667190 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU241_1(__this);
		U3CdumpU3Ec__AnonStorey2_t1475667190 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_lineCnt_0((((int64_t)((int64_t)0))));
		U3CdumpU3Ec__AnonStorey2_t1475667190 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CdumpU3Ec__AnonStorey2_U3CU3Em__0_m2509098988_MethodInfo_var);
		Action_4_t3853297115 * L_5 = (Action_4_t3853297115 *)il2cpp_codegen_object_new(Action_4_t3853297115_il2cpp_TypeInfo_var);
		Action_4__ctor_m562450268(L_5, L_3, L_4, /*hidden argument*/Action_4__ctor_m562450268_MethodInfo_var);
		return L_5;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey2__ctor_m1935026999 (U3CdumpU3Ec__AnonStorey2_t1475667190 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::<>m__0(System.String,System.String,System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t U3CdumpU3Ec__AnonStorey2_U3CU3Em__0_m2509098988_MetadataUsageId;
extern "C"  void U3CdumpU3Ec__AnonStorey2_U3CU3Em__0_m2509098988 (U3CdumpU3Ec__AnonStorey2_t1475667190 * __this, String_t* ___arg10, String_t* ___arg21, String_t* ___arg32, String_t* ___arg43, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CdumpU3Ec__AnonStorey2_U3CU3Em__0_m2509098988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_0 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_0);
		StringBuilder_t1221177846 * L_1 = L_0->get_output_0();
		U3CdumpU3Ec__AnonStorey1_t3897361314 * L_2 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_lineFmt_1();
		ObjectU5BU5D_t3614634134* L_4 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		int64_t L_5 = __this->get_lineCnt_0();
		int64_t L_6 = ((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)1)))));
		V_0 = L_6;
		__this->set_lineCnt_0(L_6);
		int64_t L_7 = V_0;
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_9);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_4;
		String_t* L_11 = ___arg10;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_10;
		String_t* L_13 = ___arg21;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		ObjectU5BU5D_t3614634134* L_14 = L_12;
		String_t* L_15 = ___arg32;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_14;
		String_t* L_17 = ___arg43;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_17);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1879616656(L_1, L_3, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3915779002 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m489817938 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0071;
		}
	}
	{
		goto IL_0099;
	}

IL_0021:
	{
		WebSocketFrame_t764750278 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = WebSocketFrame_ToArray_m4086645856(L_2, /*hidden argument*/NULL);
		__this->set_U24locvar0_0(L_3);
		__this->set_U24locvar1_1(0);
		goto IL_007f;
	}

IL_003e:
	{
		ByteU5BU5D_t3397334013* L_4 = __this->get_U24locvar0_0();
		int32_t L_5 = __this->get_U24locvar1_1();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_U3CbU3E__1_2(L_7);
		uint8_t L_8 = __this->get_U3CbU3E__1_2();
		__this->set_U24current_4(L_8);
		bool L_9 = __this->get_U24disposing_5();
		if (L_9)
		{
			goto IL_006c;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_006c:
	{
		goto IL_009b;
	}

IL_0071:
	{
		int32_t L_10 = __this->get_U24locvar1_1();
		__this->set_U24locvar1_1(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_007f:
	{
		int32_t L_11 = __this->get_U24locvar1_1();
		ByteU5BU5D_t3397334013* L_12 = __this->get_U24locvar0_0();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_0099:
	{
		return (bool)0;
	}

IL_009b:
	{
		return (bool)1;
	}
}
// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m155549798 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3117923212_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3117923212 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3117923212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0 = __this->get_U24current_4();
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1057994997 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2869931303_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2869931303 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2869931303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::.ctor()
extern "C"  void U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3__ctor_m3809299408 (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::<>m__0(System.Byte[])
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2724050918;
extern const uint32_t U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_U3CU3Em__0_m1260491982_MetadataUsageId;
extern "C"  void U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_U3CU3Em__0_m1260491982 (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_U3CU3Em__0_m1260491982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		NullCheck(L_0);
		int32_t L_1 = __this->get_len_0();
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		WebSocketException_t1348391352 * L_2 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_2, _stringLiteral2724050918, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		WebSocketFrame_t764750278 * L_3 = __this->get_frame_1();
		ByteU5BU5D_t3397334013* L_4 = ___bytes0;
		NullCheck(L_3);
		L_3->set__extPayloadLength_0(L_4);
		Action_1_t566549660 * L_5 = __this->get_completed_2();
		WebSocketFrame_t764750278 * L_6 = __this->get_frame_1();
		NullCheck(L_5);
		Action_1_Invoke_m2464278655(L_5, L_6, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::.ctor()
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7__ctor_m1437034700 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__0(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* Action_1_t566549660_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__1_m3454177245_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2625419694_MethodInfo_var;
extern const uint32_t U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__0_m1065408216_MetadataUsageId;
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__0_m1065408216 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__0_m1065408216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t3255436806 * L_0 = __this->get_stream_0();
		WebSocketFrame_t764750278 * L_1 = ___frame0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__1_m3454177245_MethodInfo_var);
		Action_1_t566549660 * L_3 = (Action_1_t566549660 *)il2cpp_codegen_object_new(Action_1_t566549660_il2cpp_TypeInfo_var);
		Action_1__ctor_m2625419694(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2625419694_MethodInfo_var);
		Action_1_t1729240069 * L_4 = __this->get_error_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_readExtendedPayloadLengthAsync_m1529395252(NULL /*static, unused*/, L_0, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__1(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* Action_1_t566549660_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__2_m1970510030_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2625419694_MethodInfo_var;
extern const uint32_t U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__1_m3454177245_MetadataUsageId;
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__1_m3454177245 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame10, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__1_m3454177245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t3255436806 * L_0 = __this->get_stream_0();
		WebSocketFrame_t764750278 * L_1 = ___frame10;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__2_m1970510030_MethodInfo_var);
		Action_1_t566549660 * L_3 = (Action_1_t566549660 *)il2cpp_codegen_object_new(Action_1_t566549660_il2cpp_TypeInfo_var);
		Action_1__ctor_m2625419694(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2625419694_MethodInfo_var);
		Action_1_t1729240069 * L_4 = __this->get_error_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_readMaskingKeyAsync_m1840909340(NULL /*static, unused*/, L_0, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__2(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* Action_1_t566549660_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__3_m3033981651_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2625419694_MethodInfo_var;
extern const uint32_t U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__2_m1970510030_MetadataUsageId;
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__2_m1970510030 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame20, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__2_m1970510030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t3255436806 * L_0 = __this->get_stream_0();
		WebSocketFrame_t764750278 * L_1 = ___frame20;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__3_m3033981651_MethodInfo_var);
		Action_1_t566549660 * L_3 = (Action_1_t566549660 *)il2cpp_codegen_object_new(Action_1_t566549660_il2cpp_TypeInfo_var);
		Action_1__ctor_m2625419694(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2625419694_MethodInfo_var);
		Action_1_t1729240069 * L_4 = __this->get_error_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_readPayloadDataAsync_m3818797513(NULL /*static, unused*/, L_0, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__3(WebSocketSharp.WebSocketFrame)
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern const uint32_t U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__3_m3033981651_MetadataUsageId;
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__3_m3033981651 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame30, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__3_m3033981651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_unmask_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		WebSocketFrame_t764750278 * L_1 = ___frame30;
		NullCheck(L_1);
		WebSocketFrame_Unmask_m217551411(L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Action_1_t566549660 * L_2 = __this->get_completed_3();
		WebSocketFrame_t764750278 * L_3 = ___frame30;
		NullCheck(L_2);
		Action_1_Invoke_m2464278655(L_2, L_3, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4::.ctor()
extern "C"  void U3CreadHeaderAsyncU3Ec__AnonStorey4__ctor_m1250933485 (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4::<>m__0(System.Byte[])
extern Il2CppClass* WebSocketFrame_t764750278_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern const uint32_t U3CreadHeaderAsyncU3Ec__AnonStorey4_U3CU3Em__0_m376984049_MetadataUsageId;
extern "C"  void U3CreadHeaderAsyncU3Ec__AnonStorey4_U3CU3Em__0_m376984049 (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHeaderAsyncU3Ec__AnonStorey4_U3CU3Em__0_m376984049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t566549660 * L_0 = __this->get_completed_0();
		ByteU5BU5D_t3397334013* L_1 = ___bytes0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t764750278_il2cpp_TypeInfo_var);
		WebSocketFrame_t764750278 * L_2 = WebSocketFrame_processHeader_m4003474107(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Action_1_Invoke_m2464278655(L_0, L_2, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::.ctor()
extern "C"  void U3CreadMaskingKeyAsyncU3Ec__AnonStorey5__ctor_m135055404 (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::<>m__0(System.Byte[])
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1719868532;
extern const uint32_t U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3284532258_MetadataUsageId;
extern "C"  void U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3284532258 (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3284532258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		NullCheck(L_0);
		int32_t L_1 = __this->get_len_0();
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		WebSocketException_t1348391352 * L_2 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_2, _stringLiteral1719868532, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		WebSocketFrame_t764750278 * L_3 = __this->get_frame_1();
		ByteU5BU5D_t3397334013* L_4 = ___bytes0;
		NullCheck(L_3);
		L_3->set__maskingKey_3(L_4);
		Action_1_t566549660 * L_5 = __this->get_completed_2();
		WebSocketFrame_t764750278 * L_6 = __this->get_frame_1();
		NullCheck(L_5);
		Action_1_Invoke_m2464278655(L_5, L_6, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::.ctor()
extern "C"  void U3CreadPayloadDataAsyncU3Ec__AnonStorey6__ctor_m4228040884 (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::<>m__0(System.Byte[])
extern Il2CppClass* WebSocketException_t1348391352_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t3839327312_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2464278655_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3570069019;
extern const uint32_t U3CreadPayloadDataAsyncU3Ec__AnonStorey6_U3CU3Em__0_m928418714_MetadataUsageId;
extern "C"  void U3CreadPayloadDataAsyncU3Ec__AnonStorey6_U3CU3Em__0_m928418714 (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_U3CU3Em__0_m928418714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m2538298538((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		int64_t L_2 = __this->get_llen_0();
		if ((((int64_t)L_1) == ((int64_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		WebSocketException_t1348391352 * L_3 = (WebSocketException_t1348391352 *)il2cpp_codegen_object_new(WebSocketException_t1348391352_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m2096770588(L_3, _stringLiteral3570069019, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		WebSocketFrame_t764750278 * L_4 = __this->get_frame_1();
		ByteU5BU5D_t3397334013* L_5 = ___bytes0;
		int64_t L_6 = __this->get_llen_0();
		PayloadData_t3839327312 * L_7 = (PayloadData_t3839327312 *)il2cpp_codegen_object_new(PayloadData_t3839327312_il2cpp_TypeInfo_var);
		PayloadData__ctor_m1269236835(L_7, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set__payloadData_5(L_7);
		Action_1_t566549660 * L_8 = __this->get_completed_2();
		WebSocketFrame_t764750278 * L_9 = __this->get_frame_1();
		NullCheck(L_8);
		Action_1_Invoke_m2464278655(L_8, L_9, /*hidden argument*/Action_1_Invoke_m2464278655_MethodInfo_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
