﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4
struct U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4::.ctor()
extern "C"  void U3CreadHeaderAsyncU3Ec__AnonStorey4__ctor_m1250933485 (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4::<>m__0(System.Byte[])
extern "C"  void U3CreadHeaderAsyncU3Ec__AnonStorey4_U3CU3Em__0_m376984049 (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
