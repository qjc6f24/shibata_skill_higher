﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.ClientSslConfiguration
struct ClientSslConfiguration_t1159130081;
// System.String
struct String_t;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t3696771181;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "System_System_Security_Authentication_SslProtocols894678499.h"

// System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String)
extern "C"  void ClientSslConfiguration__ctor_m596200420 (ClientSslConfiguration_t1159130081 * __this, String_t* ___targetHost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean)
extern "C"  void ClientSslConfiguration__ctor_m573646549 (ClientSslConfiguration_t1159130081 * __this, String_t* ___targetHost0, X509CertificateCollection_t1197680765 * ___clientCertificates1, int32_t ___enabledSslProtocols2, bool ___checkCertificateRevocation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificates()
extern "C"  X509CertificateCollection_t1197680765 * ClientSslConfiguration_get_ClientCertificates_m3726515733 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern "C"  LocalCertificateSelectionCallback_t3696771181 * ClientSslConfiguration_get_ClientCertificateSelectionCallback_m3153947016 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern "C"  RemoteCertificateValidationCallback_t2756269959 * ClientSslConfiguration_get_ServerCertificateValidationCallback_m3599237829 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.ClientSslConfiguration::get_TargetHost()
extern "C"  String_t* ClientSslConfiguration_get_TargetHost_m740170825 (ClientSslConfiguration_t1159130081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
