﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleController
struct BattleController_t3050984398;

#include "codegen/il2cpp-codegen.h"

// System.Void BattleController::.ctor()
extern "C"  void BattleController__ctor_m1953836025 (BattleController_t3050984398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleController::Start()
extern "C"  void BattleController_Start_m1127653241 (BattleController_t3050984398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleController::Update()
extern "C"  void BattleController_Update_m3719881328 (BattleController_t3050984398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BattleController::checkBattleStarted()
extern "C"  bool BattleController_checkBattleStarted_m639244660 (BattleController_t3050984398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BattleController::checkBattleEnded()
extern "C"  bool BattleController_checkBattleEnded_m846999951 (BattleController_t3050984398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
