﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ToString>c__AnonStorey7`1<System.Object>
struct U3CToStringU3Ec__AnonStorey7_1_t4020274930;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ToString>c__AnonStorey7`1<System.Object>::.ctor()
extern "C"  void U3CToStringU3Ec__AnonStorey7_1__ctor_m3149986787_gshared (U3CToStringU3Ec__AnonStorey7_1_t4020274930 * __this, const MethodInfo* method);
#define U3CToStringU3Ec__AnonStorey7_1__ctor_m3149986787(__this, method) ((  void (*) (U3CToStringU3Ec__AnonStorey7_1_t4020274930 *, const MethodInfo*))U3CToStringU3Ec__AnonStorey7_1__ctor_m3149986787_gshared)(__this, method)
// System.Void WebSocketSharp.Ext/<ToString>c__AnonStorey7`1<System.Object>::<>m__0(System.Int32)
extern "C"  void U3CToStringU3Ec__AnonStorey7_1_U3CU3Em__0_m166957321_gshared (U3CToStringU3Ec__AnonStorey7_1_t4020274930 * __this, int32_t ___i0, const MethodInfo* method);
#define U3CToStringU3Ec__AnonStorey7_1_U3CU3Em__0_m166957321(__this, ___i0, method) ((  void (*) (U3CToStringU3Ec__AnonStorey7_1_t4020274930 *, int32_t, const MethodInfo*))U3CToStringU3Ec__AnonStorey7_1_U3CU3Em__0_m166957321_gshared)(__this, ___i0, method)
