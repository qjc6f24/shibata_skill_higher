﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1146723439;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_AuthenticationS29593226.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "mscorlib_System_String2029220233.h"

// System.Void WebSocketSharp.Net.AuthenticationChallenge::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern "C"  void AuthenticationChallenge__ctor_m3414566521 (AuthenticationChallenge_t1146723439 * __this, int32_t ___scheme0, NameValueCollection_t3047564564 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.Net.AuthenticationChallenge::Parse(System.String)
extern "C"  AuthenticationChallenge_t1146723439 * AuthenticationChallenge_Parse_m2486528551 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationChallenge::ToBasicString()
extern "C"  String_t* AuthenticationChallenge_ToBasicString_m4168534939 (AuthenticationChallenge_t1146723439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationChallenge::ToDigestString()
extern "C"  String_t* AuthenticationChallenge_ToDigestString_m3565166461 (AuthenticationChallenge_t1146723439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
