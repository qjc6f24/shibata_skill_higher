﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket
struct WebSocket_t3268376029;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// WebSocketSharp.Net.ClientSslConfiguration
struct ClientSslConfiguration_t1159130081;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t3230782241;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t3388497467;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t1481358898;
// System.EventHandler
struct EventHandler_t277755526;
// WebSocketSharp.HttpResponse
struct HttpResponse_t2820540315;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t764750278;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;
// WebSocketSharp.HttpRequest
struct HttpRequest_t1845443631;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t2890051726;
// System.Exception
struct Exception_t1927440687;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t4248997468;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpResponse2820540315.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData3839327312.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726.h"
#include "mscorlib_System_Exception1927440687.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseStatusCode2945181741.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieCollect4248997468.h"
#include "websocketU2Dsharp_WebSocketSharp_Opcode2313788840.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "websocketU2Dsharp_WebSocketSharp_Fin2752139063.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpRequest1845443631.h"

// System.Void WebSocketSharp.WebSocket::.cctor()
extern "C"  void WebSocket__cctor_m1237934464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern "C"  void WebSocket__ctor_m666923799 (WebSocket_t3268376029 * __this, String_t* ___url0, StringU5BU5D_t1642385972* ___protocols1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::get_HasMessage()
extern "C"  bool WebSocket_get_HasMessage_m2690126053 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::get_SslConfiguration()
extern "C"  ClientSslConfiguration_t1159130081 * WebSocket_get_SslConfiguration_m3776182150 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern "C"  void WebSocket_add_OnClose_m963191942 (WebSocket_t3268376029 * __this, EventHandler_1_t3230782241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern "C"  void WebSocket_remove_OnClose_m1985956969 (WebSocket_t3268376029 * __this, EventHandler_1_t3230782241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern "C"  void WebSocket_add_OnError_m3284929358 (WebSocket_t3268376029 * __this, EventHandler_1_t3388497467 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern "C"  void WebSocket_remove_OnError_m2985090157 (WebSocket_t3268376029 * __this, EventHandler_1_t3388497467 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern "C"  void WebSocket_add_OnMessage_m2021047116 (WebSocket_t3268376029 * __this, EventHandler_1_t1481358898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern "C"  void WebSocket_remove_OnMessage_m2270677221 (WebSocket_t3268376029 * __this, EventHandler_1_t1481358898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnOpen(System.EventHandler)
extern "C"  void WebSocket_add_OnOpen_m924055045 (WebSocket_t3268376029 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnOpen(System.EventHandler)
extern "C"  void WebSocket_remove_OnOpen_m3980689398 (WebSocket_t3268376029 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::checkHandshakeResponse(WebSocketSharp.HttpResponse,System.String&)
extern "C"  bool WebSocket_checkHandshakeResponse_m3220347755 (WebSocket_t3268376029 * __this, HttpResponse_t2820540315 * ___response0, String_t** ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern "C"  bool WebSocket_checkIfAvailable_m4114200801 (WebSocket_t3268376029 * __this, bool ___connecting0, bool ___open1, bool ___closing2, bool ___closed3, String_t** ___message4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern "C"  bool WebSocket_checkIfAvailable_m2802667577 (WebSocket_t3268376029 * __this, bool ___client0, bool ___server1, bool ___connecting2, bool ___open3, bool ___closing4, bool ___closed5, String_t** ___message6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::checkReceivedFrame(WebSocketSharp.WebSocketFrame,System.String&)
extern "C"  bool WebSocket_checkReceivedFrame_m3485348324 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, String_t** ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::close(System.UInt16,System.String)
extern "C"  void WebSocket_close_m712187051 (WebSocket_t3268376029 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void WebSocket_close_m3666496073 (WebSocket_t3268376029 * __this, PayloadData_t3839327312 * ___payloadData0, bool ___send1, bool ___receive2, bool ___received3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::closeHandshake(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool WebSocket_closeHandshake_m3651859090 (WebSocket_t3268376029 * __this, PayloadData_t3839327312 * ___payloadData0, bool ___send1, bool ___receive2, bool ___received3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::connect()
extern "C"  bool WebSocket_connect_m3298158743 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::createExtensions()
extern "C"  String_t* WebSocket_createExtensions_m1782492306 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern "C"  HttpRequest_t1845443631 * WebSocket_createHandshakeRequest_m4128701898 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::doHandshake()
extern "C"  void WebSocket_doHandshake_m244091241 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern "C"  void WebSocket_enqueueToMessageEventQueue_m820858605 (WebSocket_t3268376029 * __this, MessageEventArgs_t2890051726 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::error(System.String,System.Exception)
extern "C"  void WebSocket_error_m3171452371 (WebSocket_t3268376029 * __this, String_t* ___message0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::fatal(System.String,System.Exception)
extern "C"  void WebSocket_fatal_m2341174381 (WebSocket_t3268376029 * __this, String_t* ___message0, Exception_t1927440687 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::fatal(System.String,System.UInt16)
extern "C"  void WebSocket_fatal_m2362838679 (WebSocket_t3268376029 * __this, String_t* ___message0, uint16_t ___code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::fatal(System.String,WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocket_fatal_m1525925709 (WebSocket_t3268376029 * __this, String_t* ___message0, uint16_t ___code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::init()
extern "C"  void WebSocket_init_m2964322191 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::message()
extern "C"  void WebSocket_message_m4162534566 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::messagec(WebSocketSharp.MessageEventArgs)
extern "C"  void WebSocket_messagec_m2913074030 (WebSocket_t3268376029 * __this, MessageEventArgs_t2890051726 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::open()
extern "C"  void WebSocket_open_m476730549 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processCloseFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processCloseFrame_m1007050172 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::processCookies(WebSocketSharp.Net.CookieCollection)
extern "C"  void WebSocket_processCookies_m4266111229 (WebSocket_t3268376029 * __this, CookieCollection_t4248997468 * ___cookies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processDataFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processDataFrame_m1864204174 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processFragmentFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processFragmentFrame_m2596041414 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processPingFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processPingFrame_m4267023506 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processPongFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processPongFrame_m3106904476 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processReceivedFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processReceivedFrame_m2771665755 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern "C"  void WebSocket_processSecWebSocketExtensionsServerHeader_m1362359134 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::processUnsupportedFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_processUnsupportedFrame_m1830558689 (WebSocket_t3268376029 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::releaseClientResources()
extern "C"  void WebSocket_releaseClientResources_m2146138518 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::releaseCommonResources()
extern "C"  void WebSocket_releaseCommonResources_m2256071308 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::releaseResources()
extern "C"  void WebSocket_releaseResources_m4163756605 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::releaseServerResources()
extern "C"  void WebSocket_releaseServerResources_m239262322 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern "C"  bool WebSocket_send_m161023691 (WebSocket_t3268376029 * __this, uint8_t ___opcode0, Stream_t3255436806 * ___stream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream,System.Boolean)
extern "C"  bool WebSocket_send_m3640785804 (WebSocket_t3268376029 * __this, uint8_t ___opcode0, Stream_t3255436806 * ___stream1, bool ___compressed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean)
extern "C"  bool WebSocket_send_m4234817766 (WebSocket_t3268376029 * __this, uint8_t ___fin0, uint8_t ___opcode1, ByteU5BU5D_t3397334013* ___data2, bool ___compressed3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::sendBytes(System.Byte[])
extern "C"  bool WebSocket_sendBytes_m1170524623 (WebSocket_t3268376029 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern "C"  HttpResponse_t2820540315 * WebSocket_sendHandshakeRequest_m1722434510 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHttpRequest(WebSocketSharp.HttpRequest,System.Int32)
extern "C"  HttpResponse_t2820540315 * WebSocket_sendHttpRequest_m4116722520 (WebSocket_t3268376029 * __this, HttpRequest_t1845443631 * ___request0, int32_t ___millisecondsTimeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::sendProxyConnectRequest()
extern "C"  void WebSocket_sendProxyConnectRequest_m2863768904 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::setClientStream()
extern "C"  void WebSocket_setClientStream_m3923254386 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::startReceiving()
extern "C"  void WebSocket_startReceiving_m872661131 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketAcceptHeader_m1256291114 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketExtensionsServerHeader_m1069285281 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketProtocolServerHeader_m2442699293 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketVersionServerHeader_m936281861 (WebSocket_t3268376029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern "C"  String_t* WebSocket_CreateBase64Key_m3247627260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern "C"  String_t* WebSocket_CreateResponseKey_m281547458 (Il2CppObject * __this /* static, unused */, String_t* ___base64Key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close()
extern "C"  void WebSocket_Close_m790379411 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Connect()
extern "C"  void WebSocket_Connect_m1908822439 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Send(System.String)
extern "C"  void WebSocket_Send_m4053134035 (WebSocket_t3268376029 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern "C"  void WebSocket_System_IDisposable_Dispose_m3961917556 (WebSocket_t3268376029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::<open>m__0(System.IAsyncResult)
extern "C"  void WebSocket_U3CopenU3Em__0_m4266960067 (WebSocket_t3268376029 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
