﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HttpBase/<Read>c__AnonStorey1`1<System.Object>
struct U3CReadU3Ec__AnonStorey1_1_t1859435416;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void WebSocketSharp.HttpBase/<Read>c__AnonStorey1`1<System.Object>::.ctor()
extern "C"  void U3CReadU3Ec__AnonStorey1_1__ctor_m1486188749_gshared (U3CReadU3Ec__AnonStorey1_1_t1859435416 * __this, const MethodInfo* method);
#define U3CReadU3Ec__AnonStorey1_1__ctor_m1486188749(__this, method) ((  void (*) (U3CReadU3Ec__AnonStorey1_1_t1859435416 *, const MethodInfo*))U3CReadU3Ec__AnonStorey1_1__ctor_m1486188749_gshared)(__this, method)
// System.Void WebSocketSharp.HttpBase/<Read>c__AnonStorey1`1<System.Object>::<>m__0(System.Object)
extern "C"  void U3CReadU3Ec__AnonStorey1_1_U3CU3Em__0_m3394472104_gshared (U3CReadU3Ec__AnonStorey1_1_t1859435416 * __this, Il2CppObject * ___state0, const MethodInfo* method);
#define U3CReadU3Ec__AnonStorey1_1_U3CU3Em__0_m3394472104(__this, ___state0, method) ((  void (*) (U3CReadU3Ec__AnonStorey1_1_t1859435416 *, Il2CppObject *, const MethodInfo*))U3CReadU3Ec__AnonStorey1_1_U3CU3Em__0_m3394472104_gshared)(__this, ___state0, method)
