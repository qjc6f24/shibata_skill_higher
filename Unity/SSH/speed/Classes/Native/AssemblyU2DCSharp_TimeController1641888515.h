﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TimeText
struct TimeText_t2509642322;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeController
struct  TimeController_t1641888515  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TimeController::_battleStartIntervalTimer
	float ____battleStartIntervalTimer_4;
	// System.Single TimeController::_battleTimer
	float ____battleTimer_5;
	// System.Boolean TimeController::_isBattleStart
	bool ____isBattleStart_6;
	// System.Boolean TimeController::_isBattleEnd
	bool ____isBattleEnd_7;
	// TimeText TimeController::_timeText
	TimeText_t2509642322 * ____timeText_8;

public:
	inline static int32_t get_offset_of__battleStartIntervalTimer_4() { return static_cast<int32_t>(offsetof(TimeController_t1641888515, ____battleStartIntervalTimer_4)); }
	inline float get__battleStartIntervalTimer_4() const { return ____battleStartIntervalTimer_4; }
	inline float* get_address_of__battleStartIntervalTimer_4() { return &____battleStartIntervalTimer_4; }
	inline void set__battleStartIntervalTimer_4(float value)
	{
		____battleStartIntervalTimer_4 = value;
	}

	inline static int32_t get_offset_of__battleTimer_5() { return static_cast<int32_t>(offsetof(TimeController_t1641888515, ____battleTimer_5)); }
	inline float get__battleTimer_5() const { return ____battleTimer_5; }
	inline float* get_address_of__battleTimer_5() { return &____battleTimer_5; }
	inline void set__battleTimer_5(float value)
	{
		____battleTimer_5 = value;
	}

	inline static int32_t get_offset_of__isBattleStart_6() { return static_cast<int32_t>(offsetof(TimeController_t1641888515, ____isBattleStart_6)); }
	inline bool get__isBattleStart_6() const { return ____isBattleStart_6; }
	inline bool* get_address_of__isBattleStart_6() { return &____isBattleStart_6; }
	inline void set__isBattleStart_6(bool value)
	{
		____isBattleStart_6 = value;
	}

	inline static int32_t get_offset_of__isBattleEnd_7() { return static_cast<int32_t>(offsetof(TimeController_t1641888515, ____isBattleEnd_7)); }
	inline bool get__isBattleEnd_7() const { return ____isBattleEnd_7; }
	inline bool* get_address_of__isBattleEnd_7() { return &____isBattleEnd_7; }
	inline void set__isBattleEnd_7(bool value)
	{
		____isBattleEnd_7 = value;
	}

	inline static int32_t get_offset_of__timeText_8() { return static_cast<int32_t>(offsetof(TimeController_t1641888515, ____timeText_8)); }
	inline TimeText_t2509642322 * get__timeText_8() const { return ____timeText_8; }
	inline TimeText_t2509642322 ** get_address_of__timeText_8() { return &____timeText_8; }
	inline void set__timeText_8(TimeText_t2509642322 * value)
	{
		____timeText_8 = value;
		Il2CppCodeGenWriteBarrier(&____timeText_8, value);
	}
};

struct TimeController_t1641888515_StaticFields
{
public:
	// System.Single TimeController::BATTLE_START_INTERVAL_TIME
	float ___BATTLE_START_INTERVAL_TIME_2;
	// System.Single TimeController::BATTLE_TIME
	float ___BATTLE_TIME_3;

public:
	inline static int32_t get_offset_of_BATTLE_START_INTERVAL_TIME_2() { return static_cast<int32_t>(offsetof(TimeController_t1641888515_StaticFields, ___BATTLE_START_INTERVAL_TIME_2)); }
	inline float get_BATTLE_START_INTERVAL_TIME_2() const { return ___BATTLE_START_INTERVAL_TIME_2; }
	inline float* get_address_of_BATTLE_START_INTERVAL_TIME_2() { return &___BATTLE_START_INTERVAL_TIME_2; }
	inline void set_BATTLE_START_INTERVAL_TIME_2(float value)
	{
		___BATTLE_START_INTERVAL_TIME_2 = value;
	}

	inline static int32_t get_offset_of_BATTLE_TIME_3() { return static_cast<int32_t>(offsetof(TimeController_t1641888515_StaticFields, ___BATTLE_TIME_3)); }
	inline float get_BATTLE_TIME_3() const { return ___BATTLE_TIME_3; }
	inline float* get_address_of_BATTLE_TIME_3() { return &___BATTLE_TIME_3; }
	inline void set_BATTLE_TIME_3(float value)
	{
		___BATTLE_TIME_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
