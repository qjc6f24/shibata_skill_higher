﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t344507773;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;

#include "codegen/il2cpp-codegen.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData3839327312.h"

// System.Void WebSocketSharp.CloseEventArgs::.ctor(WebSocketSharp.PayloadData)
extern "C"  void CloseEventArgs__ctor_m271874174 (CloseEventArgs_t344507773 * __this, PayloadData_t3839327312 * ___payloadData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.CloseEventArgs::set_WasClean(System.Boolean)
extern "C"  void CloseEventArgs_set_WasClean_m319124275 (CloseEventArgs_t344507773 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
