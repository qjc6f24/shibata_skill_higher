﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;

#include "websocketU2Dsharp_WebSocketSharp_Net_Authentication909684845.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationResponse
struct  AuthenticationResponse_t1212723231  : public AuthenticationBase_t909684845
{
public:
	// System.UInt32 WebSocketSharp.Net.AuthenticationResponse::_nonceCount
	uint32_t ____nonceCount_2;

public:
	inline static int32_t get_offset_of__nonceCount_2() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t1212723231, ____nonceCount_2)); }
	inline uint32_t get__nonceCount_2() const { return ____nonceCount_2; }
	inline uint32_t* get_address_of__nonceCount_2() { return &____nonceCount_2; }
	inline void set__nonceCount_2(uint32_t value)
	{
		____nonceCount_2 = value;
	}
};

struct AuthenticationResponse_t1212723231_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Net.AuthenticationResponse::<>f__am$cache0
	Func_2_t1989381442 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t1212723231_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t1989381442 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t1989381442 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t1989381442 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
