﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3
struct U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::.ctor()
extern "C"  void U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3__ctor_m3809299408 (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::<>m__0(System.Byte[])
extern "C"  void U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_U3CU3Em__0_m1260491982 (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
