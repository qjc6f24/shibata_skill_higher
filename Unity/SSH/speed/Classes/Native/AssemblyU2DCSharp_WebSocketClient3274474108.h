﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// WebSocketSharp.WebSocket
struct WebSocket_t3268376029;
// CardController
struct CardController_t2595996862;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t3388497467;

#include "AssemblyU2DCSharp_Pk2738205061.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketClient
struct  WebSocketClient_t3274474108  : public Pk_t2738205061
{
public:
	// System.Int32 WebSocketClient::_phase
	int32_t ____phase_18;
	// System.Int32 WebSocketClient::_userId
	int32_t ____userId_19;
	// System.Int32 WebSocketClient::_roomId
	int32_t ____roomId_20;
	// System.Int32 WebSocketClient::_playerNum
	int32_t ____playerNum_21;
	// System.Boolean WebSocketClient::_isConnecting
	bool ____isConnecting_22;
	// System.Boolean WebSocketClient::isChangedField
	bool ___isChangedField_23;
	// System.Int32[] WebSocketClient::_fieldCardNums
	Int32U5BU5D_t3030399641* ____fieldCardNums_24;
	// System.Int32[][] WebSocketClient::_playerCardNums
	Int32U5BU5DU5BU5D_t3750818532* ____playerCardNums_25;
	// System.Int32[] WebSocketClient::_playerUserIds
	Int32U5BU5D_t3030399641* ____playerUserIds_26;
	// CardController WebSocketClient::_cardController
	CardController_t2595996862 * ____cardController_28;

public:
	inline static int32_t get_offset_of__phase_18() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____phase_18)); }
	inline int32_t get__phase_18() const { return ____phase_18; }
	inline int32_t* get_address_of__phase_18() { return &____phase_18; }
	inline void set__phase_18(int32_t value)
	{
		____phase_18 = value;
	}

	inline static int32_t get_offset_of__userId_19() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____userId_19)); }
	inline int32_t get__userId_19() const { return ____userId_19; }
	inline int32_t* get_address_of__userId_19() { return &____userId_19; }
	inline void set__userId_19(int32_t value)
	{
		____userId_19 = value;
	}

	inline static int32_t get_offset_of__roomId_20() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____roomId_20)); }
	inline int32_t get__roomId_20() const { return ____roomId_20; }
	inline int32_t* get_address_of__roomId_20() { return &____roomId_20; }
	inline void set__roomId_20(int32_t value)
	{
		____roomId_20 = value;
	}

	inline static int32_t get_offset_of__playerNum_21() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____playerNum_21)); }
	inline int32_t get__playerNum_21() const { return ____playerNum_21; }
	inline int32_t* get_address_of__playerNum_21() { return &____playerNum_21; }
	inline void set__playerNum_21(int32_t value)
	{
		____playerNum_21 = value;
	}

	inline static int32_t get_offset_of__isConnecting_22() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____isConnecting_22)); }
	inline bool get__isConnecting_22() const { return ____isConnecting_22; }
	inline bool* get_address_of__isConnecting_22() { return &____isConnecting_22; }
	inline void set__isConnecting_22(bool value)
	{
		____isConnecting_22 = value;
	}

	inline static int32_t get_offset_of_isChangedField_23() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ___isChangedField_23)); }
	inline bool get_isChangedField_23() const { return ___isChangedField_23; }
	inline bool* get_address_of_isChangedField_23() { return &___isChangedField_23; }
	inline void set_isChangedField_23(bool value)
	{
		___isChangedField_23 = value;
	}

	inline static int32_t get_offset_of__fieldCardNums_24() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____fieldCardNums_24)); }
	inline Int32U5BU5D_t3030399641* get__fieldCardNums_24() const { return ____fieldCardNums_24; }
	inline Int32U5BU5D_t3030399641** get_address_of__fieldCardNums_24() { return &____fieldCardNums_24; }
	inline void set__fieldCardNums_24(Int32U5BU5D_t3030399641* value)
	{
		____fieldCardNums_24 = value;
		Il2CppCodeGenWriteBarrier(&____fieldCardNums_24, value);
	}

	inline static int32_t get_offset_of__playerCardNums_25() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____playerCardNums_25)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__playerCardNums_25() const { return ____playerCardNums_25; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__playerCardNums_25() { return &____playerCardNums_25; }
	inline void set__playerCardNums_25(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____playerCardNums_25 = value;
		Il2CppCodeGenWriteBarrier(&____playerCardNums_25, value);
	}

	inline static int32_t get_offset_of__playerUserIds_26() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____playerUserIds_26)); }
	inline Int32U5BU5D_t3030399641* get__playerUserIds_26() const { return ____playerUserIds_26; }
	inline Int32U5BU5D_t3030399641** get_address_of__playerUserIds_26() { return &____playerUserIds_26; }
	inline void set__playerUserIds_26(Int32U5BU5D_t3030399641* value)
	{
		____playerUserIds_26 = value;
		Il2CppCodeGenWriteBarrier(&____playerUserIds_26, value);
	}

	inline static int32_t get_offset_of__cardController_28() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108, ____cardController_28)); }
	inline CardController_t2595996862 * get__cardController_28() const { return ____cardController_28; }
	inline CardController_t2595996862 ** get_address_of__cardController_28() { return &____cardController_28; }
	inline void set__cardController_28(CardController_t2595996862 * value)
	{
		____cardController_28 = value;
		Il2CppCodeGenWriteBarrier(&____cardController_28, value);
	}
};

struct WebSocketClient_t3274474108_StaticFields
{
public:
	// System.String WebSocketClient::PORT_NUM
	String_t* ___PORT_NUM_7;
	// System.String WebSocketClient::IP_ADDRESS
	String_t* ___IP_ADDRESS_8;
	// System.Int32 WebSocketClient::PHASE_START
	int32_t ___PHASE_START_9;
	// System.Int32 WebSocketClient::PHASE_CREATE_USER
	int32_t ___PHASE_CREATE_USER_10;
	// System.Int32 WebSocketClient::PHASE_WAIT_CREATE_USER
	int32_t ___PHASE_WAIT_CREATE_USER_11;
	// System.Int32 WebSocketClient::PHASE_LOGIN
	int32_t ___PHASE_LOGIN_12;
	// System.Int32 WebSocketClient::PHASE_WAIT_ROOM_FILL
	int32_t ___PHASE_WAIT_ROOM_FILL_13;
	// System.Int32 WebSocketClient::PHASE_CHANGE_SCENE
	int32_t ___PHASE_CHANGE_SCENE_14;
	// System.Int32 WebSocketClient::PHASE_GAME_INIT
	int32_t ___PHASE_GAME_INIT_15;
	// System.Int32 WebSocketClient::PHASE_GAME_PLAYING
	int32_t ___PHASE_GAME_PLAYING_16;
	// System.Int32 WebSocketClient::PHASE_END
	int32_t ___PHASE_END_17;
	// WebSocketSharp.WebSocket WebSocketClient::_ws
	WebSocket_t3268376029 * ____ws_27;
	// System.EventHandler`1<WebSocketSharp.ErrorEventArgs> WebSocketClient::<>f__am$cache0
	EventHandler_1_t3388497467 * ___U3CU3Ef__amU24cache0_29;

public:
	inline static int32_t get_offset_of_PORT_NUM_7() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PORT_NUM_7)); }
	inline String_t* get_PORT_NUM_7() const { return ___PORT_NUM_7; }
	inline String_t** get_address_of_PORT_NUM_7() { return &___PORT_NUM_7; }
	inline void set_PORT_NUM_7(String_t* value)
	{
		___PORT_NUM_7 = value;
		Il2CppCodeGenWriteBarrier(&___PORT_NUM_7, value);
	}

	inline static int32_t get_offset_of_IP_ADDRESS_8() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___IP_ADDRESS_8)); }
	inline String_t* get_IP_ADDRESS_8() const { return ___IP_ADDRESS_8; }
	inline String_t** get_address_of_IP_ADDRESS_8() { return &___IP_ADDRESS_8; }
	inline void set_IP_ADDRESS_8(String_t* value)
	{
		___IP_ADDRESS_8 = value;
		Il2CppCodeGenWriteBarrier(&___IP_ADDRESS_8, value);
	}

	inline static int32_t get_offset_of_PHASE_START_9() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_START_9)); }
	inline int32_t get_PHASE_START_9() const { return ___PHASE_START_9; }
	inline int32_t* get_address_of_PHASE_START_9() { return &___PHASE_START_9; }
	inline void set_PHASE_START_9(int32_t value)
	{
		___PHASE_START_9 = value;
	}

	inline static int32_t get_offset_of_PHASE_CREATE_USER_10() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_CREATE_USER_10)); }
	inline int32_t get_PHASE_CREATE_USER_10() const { return ___PHASE_CREATE_USER_10; }
	inline int32_t* get_address_of_PHASE_CREATE_USER_10() { return &___PHASE_CREATE_USER_10; }
	inline void set_PHASE_CREATE_USER_10(int32_t value)
	{
		___PHASE_CREATE_USER_10 = value;
	}

	inline static int32_t get_offset_of_PHASE_WAIT_CREATE_USER_11() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_WAIT_CREATE_USER_11)); }
	inline int32_t get_PHASE_WAIT_CREATE_USER_11() const { return ___PHASE_WAIT_CREATE_USER_11; }
	inline int32_t* get_address_of_PHASE_WAIT_CREATE_USER_11() { return &___PHASE_WAIT_CREATE_USER_11; }
	inline void set_PHASE_WAIT_CREATE_USER_11(int32_t value)
	{
		___PHASE_WAIT_CREATE_USER_11 = value;
	}

	inline static int32_t get_offset_of_PHASE_LOGIN_12() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_LOGIN_12)); }
	inline int32_t get_PHASE_LOGIN_12() const { return ___PHASE_LOGIN_12; }
	inline int32_t* get_address_of_PHASE_LOGIN_12() { return &___PHASE_LOGIN_12; }
	inline void set_PHASE_LOGIN_12(int32_t value)
	{
		___PHASE_LOGIN_12 = value;
	}

	inline static int32_t get_offset_of_PHASE_WAIT_ROOM_FILL_13() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_WAIT_ROOM_FILL_13)); }
	inline int32_t get_PHASE_WAIT_ROOM_FILL_13() const { return ___PHASE_WAIT_ROOM_FILL_13; }
	inline int32_t* get_address_of_PHASE_WAIT_ROOM_FILL_13() { return &___PHASE_WAIT_ROOM_FILL_13; }
	inline void set_PHASE_WAIT_ROOM_FILL_13(int32_t value)
	{
		___PHASE_WAIT_ROOM_FILL_13 = value;
	}

	inline static int32_t get_offset_of_PHASE_CHANGE_SCENE_14() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_CHANGE_SCENE_14)); }
	inline int32_t get_PHASE_CHANGE_SCENE_14() const { return ___PHASE_CHANGE_SCENE_14; }
	inline int32_t* get_address_of_PHASE_CHANGE_SCENE_14() { return &___PHASE_CHANGE_SCENE_14; }
	inline void set_PHASE_CHANGE_SCENE_14(int32_t value)
	{
		___PHASE_CHANGE_SCENE_14 = value;
	}

	inline static int32_t get_offset_of_PHASE_GAME_INIT_15() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_GAME_INIT_15)); }
	inline int32_t get_PHASE_GAME_INIT_15() const { return ___PHASE_GAME_INIT_15; }
	inline int32_t* get_address_of_PHASE_GAME_INIT_15() { return &___PHASE_GAME_INIT_15; }
	inline void set_PHASE_GAME_INIT_15(int32_t value)
	{
		___PHASE_GAME_INIT_15 = value;
	}

	inline static int32_t get_offset_of_PHASE_GAME_PLAYING_16() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_GAME_PLAYING_16)); }
	inline int32_t get_PHASE_GAME_PLAYING_16() const { return ___PHASE_GAME_PLAYING_16; }
	inline int32_t* get_address_of_PHASE_GAME_PLAYING_16() { return &___PHASE_GAME_PLAYING_16; }
	inline void set_PHASE_GAME_PLAYING_16(int32_t value)
	{
		___PHASE_GAME_PLAYING_16 = value;
	}

	inline static int32_t get_offset_of_PHASE_END_17() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___PHASE_END_17)); }
	inline int32_t get_PHASE_END_17() const { return ___PHASE_END_17; }
	inline int32_t* get_address_of_PHASE_END_17() { return &___PHASE_END_17; }
	inline void set_PHASE_END_17(int32_t value)
	{
		___PHASE_END_17 = value;
	}

	inline static int32_t get_offset_of__ws_27() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ____ws_27)); }
	inline WebSocket_t3268376029 * get__ws_27() const { return ____ws_27; }
	inline WebSocket_t3268376029 ** get_address_of__ws_27() { return &____ws_27; }
	inline void set__ws_27(WebSocket_t3268376029 * value)
	{
		____ws_27 = value;
		Il2CppCodeGenWriteBarrier(&____ws_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_29() { return static_cast<int32_t>(offsetof(WebSocketClient_t3274474108_StaticFields, ___U3CU3Ef__amU24cache0_29)); }
	inline EventHandler_1_t3388497467 * get_U3CU3Ef__amU24cache0_29() const { return ___U3CU3Ef__amU24cache0_29; }
	inline EventHandler_1_t3388497467 ** get_address_of_U3CU3Ef__amU24cache0_29() { return &___U3CU3Ef__amU24cache0_29; }
	inline void set_U3CU3Ef__amU24cache0_29(EventHandler_1_t3388497467 * value)
	{
		___U3CU3Ef__amU24cache0_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
