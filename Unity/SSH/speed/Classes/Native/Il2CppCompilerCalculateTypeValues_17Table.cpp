﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "websocketU2Dsharp_U3CModuleU3E3783534214.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext870230697.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CContainsTw2127424492.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA1697116946.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA1697116943.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CReadBytesA4262448565.h"
#include "websocketU2Dsharp_WebSocketSharp_Ext_U3CSplitHeade2473694690.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseEventArgs344507773.h"
#include "websocketU2Dsharp_WebSocketSharp_ByteOrder469806806.h"
#include "websocketU2Dsharp_WebSocketSharp_ErrorEventArgs502222999.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket3268376029.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cstart506990866.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cvali2469772577.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocket_U3Cvali1655596450.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_AuthenticationS29593226.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Cookie1826188460.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieCollect4248997468.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_CookieExceptio780982235.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpUtility3363705102.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl1932982249.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl2338822889.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebHeaderColl1254512383.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpVersion4270509666.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseStatusCode2945181741.h"
#include "websocketU2Dsharp_WebSocketSharp_Fin2752139063.h"
#include "websocketU2Dsharp_WebSocketSharp_Mask1111889066.h"
#include "websocketU2Dsharp_WebSocketSharp_Opcode2313788840.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData3839327312.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData_U3CGe3664690781.h"
#include "websocketU2Dsharp_WebSocketSharp_Rsv1058189029.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_WebSockets_We3488732344.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpHeaderTyp1518115223.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_HttpHeaderInf2096319561.h"
#include "websocketU2Dsharp_WebSocketSharp_CompressionMethod4066553457.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketExceptio1348391352.h"
#include "websocketU2Dsharp_WebSocketSharp_LogData4095822710.h"
#include "websocketU2Dsharp_WebSocketSharp_LogLevel2748531832.h"
#include "websocketU2Dsharp_WebSocketSharp_Logger2598199114.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketState2935910988.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_NetworkCreden3911206805.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U33897361314.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31475667190.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31521887925.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U31882858074.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U33451788521.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3C983838767.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3CRe8184881.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame_U3C908706979.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1146723439.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1212723231.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authentication909684845.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpBase4283398485.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpBase_U3CreadH2579283176.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpRequest1845443631.h"
#include "websocketU2Dsharp_WebSocketSharp_HttpResponse2820540315.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_ClientSslConf1159130081.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_SslConfigurati760772650.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_Type1314578890.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_AddJSONCo3850664647.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_FieldNotFo865402053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_GetFieldR1259369279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CBakeAs1149809410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CPrintAs716304657.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CString4037879552.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObjectEnumer427597183.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1706[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1717[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1725[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (Ext_t870230697), -1, sizeof(Ext_t870230697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1728[3] = 
{
	Ext_t870230697_StaticFields::get_offset_of__last_0(),
	Ext_t870230697_StaticFields::get_offset_of__retry_1(),
	Ext_t870230697_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[3] = 
{
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492::get_offset_of_len_0(),
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492::get_offset_of_values_1(),
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492::get_offset_of_contains_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[8] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_stream_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_retry_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_buff_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_offset_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_length_4(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_callback_5(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_completed_6(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_error_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[8] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_bufferLength_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_stream_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_buff_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_dest_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_retry_4(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_read_5(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_completed_6(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_error_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[2] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565::get_offset_of_len_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[12] = 
{
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_value_0(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3ClenU3E__0_1(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_separators_2(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CsepsU3E__0_3(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CbuffU3E__0_4(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CescapedU3E__0_5(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CquotedU3E__0_6(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CiU3E__1_7(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CcU3E__2_8(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U24current_9(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U24disposing_10(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (MessageEventArgs_t2890051726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[4] = 
{
	MessageEventArgs_t2890051726::get_offset_of__data_1(),
	MessageEventArgs_t2890051726::get_offset_of__dataSet_2(),
	MessageEventArgs_t2890051726::get_offset_of__opcode_3(),
	MessageEventArgs_t2890051726::get_offset_of__rawData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (CloseEventArgs_t344507773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[2] = 
{
	CloseEventArgs_t344507773::get_offset_of__clean_1(),
	CloseEventArgs_t344507773::get_offset_of__payloadData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (ByteOrder_t469806806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1737[3] = 
{
	ByteOrder_t469806806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (ErrorEventArgs_t502222999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[2] = 
{
	ErrorEventArgs_t502222999::get_offset_of__exception_1(),
	ErrorEventArgs_t502222999::get_offset_of__message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (WebSocket_t3268376029), -1, sizeof(WebSocket_t3268376029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[54] = 
{
	WebSocket_t3268376029::get_offset_of__authChallenge_0(),
	WebSocket_t3268376029::get_offset_of__base64Key_1(),
	WebSocket_t3268376029::get_offset_of__client_2(),
	WebSocket_t3268376029::get_offset_of__closeContext_3(),
	WebSocket_t3268376029::get_offset_of__compression_4(),
	WebSocket_t3268376029::get_offset_of__context_5(),
	WebSocket_t3268376029::get_offset_of__cookies_6(),
	WebSocket_t3268376029::get_offset_of__credentials_7(),
	WebSocket_t3268376029::get_offset_of__emitOnPing_8(),
	WebSocket_t3268376029::get_offset_of__enableRedirection_9(),
	WebSocket_t3268376029::get_offset_of__extensions_10(),
	WebSocket_t3268376029::get_offset_of__extensionsRequested_11(),
	WebSocket_t3268376029::get_offset_of__forMessageEventQueue_12(),
	WebSocket_t3268376029::get_offset_of__forPing_13(),
	WebSocket_t3268376029::get_offset_of__forSend_14(),
	WebSocket_t3268376029::get_offset_of__forState_15(),
	WebSocket_t3268376029::get_offset_of__fragmentsBuffer_16(),
	WebSocket_t3268376029::get_offset_of__fragmentsCompressed_17(),
	WebSocket_t3268376029::get_offset_of__fragmentsOpcode_18(),
	0,
	WebSocket_t3268376029::get_offset_of__handshakeRequestChecker_20(),
	WebSocket_t3268376029::get_offset_of__ignoreExtensions_21(),
	WebSocket_t3268376029::get_offset_of__inContinuation_22(),
	WebSocket_t3268376029::get_offset_of__inMessage_23(),
	WebSocket_t3268376029::get_offset_of__logger_24(),
	WebSocket_t3268376029_StaticFields::get_offset_of__maxRetryCountForConnect_25(),
	WebSocket_t3268376029::get_offset_of__message_26(),
	WebSocket_t3268376029::get_offset_of__messageEventQueue_27(),
	WebSocket_t3268376029::get_offset_of__nonceCount_28(),
	WebSocket_t3268376029::get_offset_of__origin_29(),
	WebSocket_t3268376029::get_offset_of__pongReceived_30(),
	WebSocket_t3268376029::get_offset_of__preAuth_31(),
	WebSocket_t3268376029::get_offset_of__protocol_32(),
	WebSocket_t3268376029::get_offset_of__protocols_33(),
	WebSocket_t3268376029::get_offset_of__protocolsRequested_34(),
	WebSocket_t3268376029::get_offset_of__proxyCredentials_35(),
	WebSocket_t3268376029::get_offset_of__proxyUri_36(),
	WebSocket_t3268376029::get_offset_of__readyState_37(),
	WebSocket_t3268376029::get_offset_of__receivingExited_38(),
	WebSocket_t3268376029::get_offset_of__retryCountForConnect_39(),
	WebSocket_t3268376029::get_offset_of__secure_40(),
	WebSocket_t3268376029::get_offset_of__sslConfig_41(),
	WebSocket_t3268376029::get_offset_of__stream_42(),
	WebSocket_t3268376029::get_offset_of__tcpClient_43(),
	WebSocket_t3268376029::get_offset_of__uri_44(),
	0,
	WebSocket_t3268376029::get_offset_of__waitTime_46(),
	WebSocket_t3268376029_StaticFields::get_offset_of_EmptyBytes_47(),
	WebSocket_t3268376029_StaticFields::get_offset_of_FragmentLength_48(),
	WebSocket_t3268376029_StaticFields::get_offset_of_RandomNumber_49(),
	WebSocket_t3268376029::get_offset_of_OnClose_50(),
	WebSocket_t3268376029::get_offset_of_OnError_51(),
	WebSocket_t3268376029::get_offset_of_OnMessage_52(),
	WebSocket_t3268376029::get_offset_of_OnOpen_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (U3CstartReceivingU3Ec__AnonStorey4_t506990866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[2] = 
{
	U3CstartReceivingU3Ec__AnonStorey4_t506990866::get_offset_of_receive_0(),
	U3CstartReceivingU3Ec__AnonStorey4_t506990866::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[1] = 
{
	U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[1] = 
{
	U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (AuthenticationSchemes_t29593226)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[5] = 
{
	AuthenticationSchemes_t29593226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (Cookie_t1826188460), -1, sizeof(Cookie_t1826188460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1744[17] = 
{
	Cookie_t1826188460::get_offset_of__comment_0(),
	Cookie_t1826188460::get_offset_of__commentUri_1(),
	Cookie_t1826188460::get_offset_of__discard_2(),
	Cookie_t1826188460::get_offset_of__domain_3(),
	Cookie_t1826188460::get_offset_of__expires_4(),
	Cookie_t1826188460::get_offset_of__httpOnly_5(),
	Cookie_t1826188460::get_offset_of__name_6(),
	Cookie_t1826188460::get_offset_of__path_7(),
	Cookie_t1826188460::get_offset_of__port_8(),
	Cookie_t1826188460::get_offset_of__ports_9(),
	Cookie_t1826188460_StaticFields::get_offset_of__reservedCharsForName_10(),
	Cookie_t1826188460_StaticFields::get_offset_of__reservedCharsForValue_11(),
	Cookie_t1826188460::get_offset_of__secure_12(),
	Cookie_t1826188460::get_offset_of__timestamp_13(),
	Cookie_t1826188460::get_offset_of__value_14(),
	Cookie_t1826188460::get_offset_of__version_15(),
	Cookie_t1826188460::get_offset_of_U3CExactDomainU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (CookieCollection_t4248997468), -1, sizeof(CookieCollection_t4248997468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1745[4] = 
{
	CookieCollection_t4248997468::get_offset_of__list_0(),
	CookieCollection_t4248997468::get_offset_of__sync_1(),
	CookieCollection_t4248997468_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
	CookieCollection_t4248997468_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (CookieException_t780982235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (HttpUtility_t3363705102), -1, sizeof(HttpUtility_t3363705102_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1747[2] = 
{
	HttpUtility_t3363705102_StaticFields::get_offset_of__hexChars_0(),
	HttpUtility_t3363705102_StaticFields::get_offset_of__sync_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (WebHeaderCollection_t1932982249), -1, sizeof(WebHeaderCollection_t1932982249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1748[3] = 
{
	WebHeaderCollection_t1932982249_StaticFields::get_offset_of__headers_12(),
	WebHeaderCollection_t1932982249::get_offset_of__internallyUsed_13(),
	WebHeaderCollection_t1932982249::get_offset_of__state_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[3] = 
{
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889::get_offset_of_serializationInfo_0(),
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889::get_offset_of_cnt_1(),
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (U3CToStringU3Ec__AnonStorey2_t1254512383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[2] = 
{
	U3CToStringU3Ec__AnonStorey2_t1254512383::get_offset_of_buff_0(),
	U3CToStringU3Ec__AnonStorey2_t1254512383::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (HttpVersion_t4270509666), -1, sizeof(HttpVersion_t4270509666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1751[2] = 
{
	HttpVersion_t4270509666_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t4270509666_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (CloseStatusCode_t2945181741)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1752[14] = 
{
	CloseStatusCode_t2945181741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (Fin_t2752139063)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1753[3] = 
{
	Fin_t2752139063::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (Mask_t1111889066)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1754[3] = 
{
	Mask_t1111889066::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (Opcode_t2313788840)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1755[7] = 
{
	Opcode_t2313788840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (PayloadData_t3839327312), -1, sizeof(PayloadData_t3839327312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1756[9] = 
{
	PayloadData_t3839327312::get_offset_of__code_0(),
	PayloadData_t3839327312::get_offset_of__codeSet_1(),
	PayloadData_t3839327312::get_offset_of__data_2(),
	PayloadData_t3839327312::get_offset_of__extDataLength_3(),
	PayloadData_t3839327312::get_offset_of__length_4(),
	PayloadData_t3839327312::get_offset_of__reason_5(),
	PayloadData_t3839327312::get_offset_of__reasonSet_6(),
	PayloadData_t3839327312_StaticFields::get_offset_of_Empty_7(),
	PayloadData_t3839327312_StaticFields::get_offset_of_MaxLength_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t3664690781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24locvar1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U3CbU3E__1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (Rsv_t1058189029)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1758[3] = 
{
	Rsv_t1058189029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (WebSocketContext_t3488732344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (HttpHeaderType_t1518115223)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1760[8] = 
{
	HttpHeaderType_t1518115223::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (HttpHeaderInfo_t2096319561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[2] = 
{
	HttpHeaderInfo_t2096319561::get_offset_of__name_0(),
	HttpHeaderInfo_t2096319561::get_offset_of__type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (CompressionMethod_t4066553457)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1762[3] = 
{
	CompressionMethod_t4066553457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (WebSocketException_t1348391352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[1] = 
{
	WebSocketException_t1348391352::get_offset_of__code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (LogData_t4095822710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[4] = 
{
	LogData_t4095822710::get_offset_of__caller_0(),
	LogData_t4095822710::get_offset_of__date_1(),
	LogData_t4095822710::get_offset_of__level_2(),
	LogData_t4095822710::get_offset_of__message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (LogLevel_t2748531832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1765[7] = 
{
	LogLevel_t2748531832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (Logger_t2598199114), -1, sizeof(Logger_t2598199114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1766[6] = 
{
	Logger_t2598199114::get_offset_of__file_0(),
	Logger_t2598199114::get_offset_of__level_1(),
	Logger_t2598199114::get_offset_of__output_2(),
	Logger_t2598199114::get_offset_of__sync_3(),
	Logger_t2598199114_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	Logger_t2598199114_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (WebSocketState_t2935910988)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1767[5] = 
{
	WebSocketState_t2935910988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (NetworkCredential_t3911206805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[4] = 
{
	NetworkCredential_t3911206805::get_offset_of__domain_0(),
	NetworkCredential_t3911206805::get_offset_of__password_1(),
	NetworkCredential_t3911206805::get_offset_of__roles_2(),
	NetworkCredential_t3911206805::get_offset_of__userName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (WebSocketFrame_t764750278), -1, sizeof(WebSocketFrame_t764750278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[11] = 
{
	WebSocketFrame_t764750278::get_offset_of__extPayloadLength_0(),
	WebSocketFrame_t764750278::get_offset_of__fin_1(),
	WebSocketFrame_t764750278::get_offset_of__mask_2(),
	WebSocketFrame_t764750278::get_offset_of__maskingKey_3(),
	WebSocketFrame_t764750278::get_offset_of__opcode_4(),
	WebSocketFrame_t764750278::get_offset_of__payloadData_5(),
	WebSocketFrame_t764750278::get_offset_of__payloadLength_6(),
	WebSocketFrame_t764750278::get_offset_of__rsv1_7(),
	WebSocketFrame_t764750278::get_offset_of__rsv2_8(),
	WebSocketFrame_t764750278::get_offset_of__rsv3_9(),
	WebSocketFrame_t764750278_StaticFields::get_offset_of_EmptyPingBytes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (U3CdumpU3Ec__AnonStorey1_t3897361314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[2] = 
{
	U3CdumpU3Ec__AnonStorey1_t3897361314::get_offset_of_output_0(),
	U3CdumpU3Ec__AnonStorey1_t3897361314::get_offset_of_lineFmt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (U3CdumpU3Ec__AnonStorey2_t1475667190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[2] = 
{
	U3CdumpU3Ec__AnonStorey2_t1475667190::get_offset_of_lineCnt_0(),
	U3CdumpU3Ec__AnonStorey2_t1475667190::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[3] = 
{
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925::get_offset_of_len_0(),
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925::get_offset_of_frame_1(),
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[1] = 
{
	U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074::get_offset_of_completed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[3] = 
{
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521::get_offset_of_len_0(),
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521::get_offset_of_frame_1(),
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[3] = 
{
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767::get_offset_of_llen_0(),
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767::get_offset_of_frame_1(),
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[4] = 
{
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_stream_0(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_error_1(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_unmask_2(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_completed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t908706979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24locvar1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U3CbU3E__1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (AuthenticationChallenge_t1146723439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (AuthenticationResponse_t1212723231), -1, sizeof(AuthenticationResponse_t1212723231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1779[2] = 
{
	AuthenticationResponse_t1212723231::get_offset_of__nonceCount_2(),
	AuthenticationResponse_t1212723231_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (AuthenticationBase_t909684845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[2] = 
{
	AuthenticationBase_t909684845::get_offset_of__scheme_0(),
	AuthenticationBase_t909684845::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (HttpBase_t4283398485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[3] = 
{
	HttpBase_t4283398485::get_offset_of__headers_0(),
	HttpBase_t4283398485::get_offset_of__version_1(),
	HttpBase_t4283398485::get_offset_of_EntityBodyData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (U3CreadHeadersU3Ec__AnonStorey0_t2579283176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[2] = 
{
	U3CreadHeadersU3Ec__AnonStorey0_t2579283176::get_offset_of_buff_0(),
	U3CreadHeadersU3Ec__AnonStorey0_t2579283176::get_offset_of_cnt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (HttpRequest_t1845443631), -1, sizeof(HttpRequest_t1845443631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1784[3] = 
{
	HttpRequest_t1845443631::get_offset_of__method_3(),
	HttpRequest_t1845443631::get_offset_of__uri_4(),
	HttpRequest_t1845443631_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (HttpResponse_t2820540315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	HttpResponse_t2820540315::get_offset_of__code_3(),
	HttpResponse_t2820540315::get_offset_of__reason_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (ClientSslConfiguration_t1159130081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[2] = 
{
	ClientSslConfiguration_t1159130081::get_offset_of__certs_6(),
	ClientSslConfiguration_t1159130081::get_offset_of__host_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (SslConfiguration_t760772650), -1, sizeof(SslConfiguration_t760772650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[6] = 
{
	SslConfiguration_t760772650::get_offset_of__certSelectionCallback_0(),
	SslConfiguration_t760772650::get_offset_of__certValidationCallback_1(),
	SslConfiguration_t760772650::get_offset_of__checkCertRevocation_2(),
	SslConfiguration_t760772650::get_offset_of__enabledProtocols_3(),
	SslConfiguration_t760772650_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	SslConfiguration_t760772650_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1788[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (JSONObject_t1971882247), -1, sizeof(JSONObject_t1971882247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1791[15] = 
{
	0,
	0,
	0,
	0,
	JSONObject_t1971882247_StaticFields::get_offset_of_WHITESPACE_4(),
	JSONObject_t1971882247::get_offset_of_type_5(),
	JSONObject_t1971882247::get_offset_of_list_6(),
	JSONObject_t1971882247::get_offset_of_keys_7(),
	JSONObject_t1971882247::get_offset_of_str_8(),
	JSONObject_t1971882247::get_offset_of_n_9(),
	JSONObject_t1971882247::get_offset_of_useInt_10(),
	JSONObject_t1971882247::get_offset_of_i_11(),
	JSONObject_t1971882247::get_offset_of_b_12(),
	0,
	JSONObject_t1971882247_StaticFields::get_offset_of_printWatch_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (Type_t1314578890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1792[8] = 
{
	Type_t1314578890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (AddJSONContents_t3850664647), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (FieldNotFound_t865402053), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (GetFieldResponse_t1259369279), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (U3CBakeAsyncU3Ec__Iterator0_t1149809410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[6] = 
{
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24locvar0_0(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U3CsU3E__0_1(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24this_2(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24current_3(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24disposing_4(),
	U3CBakeAsyncU3Ec__Iterator0_t1149809410::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (U3CPrintAsyncU3Ec__Iterator1_t716304657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[9] = 
{
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U3CbuilderU3E__0_0(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_pretty_1(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24locvar0_2(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U3CeU3E__1_3(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24locvar1_4(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24this_5(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24current_6(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24disposing_7(),
	U3CPrintAsyncU3Ec__Iterator1_t716304657::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (U3CStringifyAsyncU3Ec__Iterator2_t4037879552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[19] = 
{
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_depth_0(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar0_1(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_builder_2(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_pretty_3(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CiU3E__0_4(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CkeyU3E__1_5(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CobjU3E__2_6(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar1_7(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CeU3E__3_8(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar2_9(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CiU3E__4_10(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar3_11(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CeU3E__5_12(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24locvar4_13(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24this_14(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24current_15(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24disposing_16(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U3CU24U3Edepth_17(),
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (JSONObjectEnumer_t427597183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[2] = 
{
	JSONObjectEnumer_t427597183::get_offset_of__jobj_0(),
	JSONObjectEnumer_t427597183::get_offset_of_position_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
