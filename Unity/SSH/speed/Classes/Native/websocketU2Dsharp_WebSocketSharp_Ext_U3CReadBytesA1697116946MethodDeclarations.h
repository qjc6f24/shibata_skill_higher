﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3
struct U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey3__ctor_m1339069249 (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::<>m__0(System.IAsyncResult)
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey3_U3CU3Em__0_m436706603 (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
