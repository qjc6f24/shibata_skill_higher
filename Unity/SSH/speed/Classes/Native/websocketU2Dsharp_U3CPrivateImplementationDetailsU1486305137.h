﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "websocketU2Dsharp_U3CPrivateImplementationDetailsU3894236545.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-EB8077FF0D4F4A927EB9224048538295DEF1227A
	U24ArrayTypeU3D16_t3894236545  ___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0)); }
	inline U24ArrayTypeU3D16_t3894236545  get_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0() const { return ___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0; }
	inline U24ArrayTypeU3D16_t3894236545 * get_address_of_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0() { return &___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0; }
	inline void set_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0(U24ArrayTypeU3D16_t3894236545  value)
	{
		___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
