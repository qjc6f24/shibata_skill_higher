﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7
struct U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t764750278;

#include "codegen/il2cpp-codegen.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278.h"

// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::.ctor()
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7__ctor_m1437034700 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__0(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__0_m1065408216 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__1(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__1_m3454177245 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__2(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__2_m1970510030 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::<>m__3(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CReadFrameAsyncU3Ec__AnonStorey7_U3CU3Em__3_m3033981651 (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881 * __this, WebSocketFrame_t764750278 * ___frame30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
