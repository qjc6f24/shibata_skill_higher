﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1
struct U3CContainsTwiceU3Ec__AnonStorey1_t2127424492;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::.ctor()
extern "C"  void U3CContainsTwiceU3Ec__AnonStorey1__ctor_m3941534061 (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::<>m__0(System.Int32)
extern "C"  bool U3CContainsTwiceU3Ec__AnonStorey1_U3CU3Em__0_m1497604835 (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492 * __this, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
