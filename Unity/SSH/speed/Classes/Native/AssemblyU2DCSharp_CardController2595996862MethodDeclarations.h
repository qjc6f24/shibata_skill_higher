﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardController
struct CardController_t2595996862;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void CardController::.ctor()
extern "C"  void CardController__ctor_m1278714583 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::Start()
extern "C"  void CardController_Start_m1936395595 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::instantiateCard(System.Int32,System.Int32,System.Int32,System.Single,System.Single,System.Single)
extern "C"  void CardController_instantiateCard_m1879922353 (CardController_t2595996862 * __this, int32_t ___target0, int32_t ___index1, int32_t ___num2, float ___x3, float ___y4, float ___rate5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::modifyScale(UnityEngine.GameObject,System.Single)
extern "C"  void CardController_modifyScale_m3816870080 (CardController_t2595996862 * __this, GameObject_t1756533147 * ___gameObject0, float ___rate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CardController::getCenterPos()
extern "C"  Vector3_t2243707580  CardController_getCenterPos_m1702691502 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::Update()
extern "C"  void CardController_Update_m3525294472 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::updateAllCards()
extern "C"  void CardController_updateAllCards_m567226642 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::updateFieldCards()
extern "C"  void CardController_updateFieldCards_m602347471 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::updateUsersCards()
extern "C"  void CardController_updateUsersCards_m2654522943 (CardController_t2595996862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::changeCard(System.Int32,System.Int32,System.Int32)
extern "C"  void CardController_changeCard_m1158561212 (CardController_t2595996862 * __this, int32_t ___target0, int32_t ___index1, int32_t ___num2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardController::checkChangeCard(System.Int32,System.Int32)
extern "C"  bool CardController_checkChangeCard_m2549392343 (Il2CppObject * __this /* static, unused */, int32_t ___selectingCard0, int32_t ___targetCard1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::checkChangeCards(System.Int32,System.Int32)
extern "C"  void CardController_checkChangeCards_m2291941626 (CardController_t2595996862 * __this, int32_t ___fieldIdx0, int32_t ___userSelectingIdx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardController::.cctor()
extern "C"  void CardController__cctor_m506378808 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
