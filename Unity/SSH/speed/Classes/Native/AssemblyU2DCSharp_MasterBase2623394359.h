﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MasterBase/Base
struct Base_t3850954593;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MasterBase
struct  MasterBase_t2623394359  : public MonoBehaviour_t1158329972
{
public:
	// System.String MasterBase::json
	String_t* ___json_2;
	// MasterBase/Base MasterBase::baseMaster
	Base_t3850954593 * ___baseMaster_3;

public:
	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(MasterBase_t2623394359, ___json_2)); }
	inline String_t* get_json_2() const { return ___json_2; }
	inline String_t** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(String_t* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier(&___json_2, value);
	}

	inline static int32_t get_offset_of_baseMaster_3() { return static_cast<int32_t>(offsetof(MasterBase_t2623394359, ___baseMaster_3)); }
	inline Base_t3850954593 * get_baseMaster_3() const { return ___baseMaster_3; }
	inline Base_t3850954593 ** get_address_of_baseMaster_3() { return &___baseMaster_3; }
	inline void set_baseMaster_3(Base_t3850954593 * value)
	{
		___baseMaster_3 = value;
		Il2CppCodeGenWriteBarrier(&___baseMaster_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
