﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Version
struct Version_t1755874712;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpBase
struct  HttpBase_t4283398485  : public Il2CppObject
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::_headers
	NameValueCollection_t3047564564 * ____headers_0;
	// System.Version WebSocketSharp.HttpBase::_version
	Version_t1755874712 * ____version_1;
	// System.Byte[] WebSocketSharp.HttpBase::EntityBodyData
	ByteU5BU5D_t3397334013* ___EntityBodyData_2;

public:
	inline static int32_t get_offset_of__headers_0() { return static_cast<int32_t>(offsetof(HttpBase_t4283398485, ____headers_0)); }
	inline NameValueCollection_t3047564564 * get__headers_0() const { return ____headers_0; }
	inline NameValueCollection_t3047564564 ** get_address_of__headers_0() { return &____headers_0; }
	inline void set__headers_0(NameValueCollection_t3047564564 * value)
	{
		____headers_0 = value;
		Il2CppCodeGenWriteBarrier(&____headers_0, value);
	}

	inline static int32_t get_offset_of__version_1() { return static_cast<int32_t>(offsetof(HttpBase_t4283398485, ____version_1)); }
	inline Version_t1755874712 * get__version_1() const { return ____version_1; }
	inline Version_t1755874712 ** get_address_of__version_1() { return &____version_1; }
	inline void set__version_1(Version_t1755874712 * value)
	{
		____version_1 = value;
		Il2CppCodeGenWriteBarrier(&____version_1, value);
	}

	inline static int32_t get_offset_of_EntityBodyData_2() { return static_cast<int32_t>(offsetof(HttpBase_t4283398485, ___EntityBodyData_2)); }
	inline ByteU5BU5D_t3397334013* get_EntityBodyData_2() const { return ___EntityBodyData_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_EntityBodyData_2() { return &___EntityBodyData_2; }
	inline void set_EntityBodyData_2(ByteU5BU5D_t3397334013* value)
	{
		___EntityBodyData_2 = value;
		Il2CppCodeGenWriteBarrier(&___EntityBodyData_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
