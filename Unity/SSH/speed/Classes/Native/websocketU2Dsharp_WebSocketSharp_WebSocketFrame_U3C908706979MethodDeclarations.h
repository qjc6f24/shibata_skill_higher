﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0
struct U3CGetEnumeratorU3Ec__Iterator0_t908706979;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3915779002 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m489817938 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m155549798 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3117923212 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1057994997 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2869931303 (U3CGetEnumeratorU3Ec__Iterator0_t908706979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
