﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Card
struct Card_t34057406;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"

// System.Void Card::.ctor()
extern "C"  void Card__ctor_m586349219 (Card_t34057406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::initialize()
extern "C"  void Card_initialize_m3158878599 (Card_t34057406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::Update()
extern "C"  void Card_Update_m4272690700 (Card_t34057406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::checkCursor()
extern "C"  void Card_checkCursor_m1602530015 (Card_t34057406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Card::getCursorObject()
extern "C"  GameObject_t1756533147 * Card_getCursorObject_m4179028025 (Card_t34057406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::destroyCursorObject()
extern "C"  void Card_destroyCursorObject_m2056992626 (Card_t34057406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
