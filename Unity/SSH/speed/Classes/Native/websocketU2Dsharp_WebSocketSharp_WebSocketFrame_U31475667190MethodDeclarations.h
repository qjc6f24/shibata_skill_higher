﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2
struct U3CdumpU3Ec__AnonStorey2_t1475667190;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey2__ctor_m1935026999 (U3CdumpU3Ec__AnonStorey2_t1475667190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::<>m__0(System.String,System.String,System.String,System.String)
extern "C"  void U3CdumpU3Ec__AnonStorey2_U3CU3Em__0_m2509098988 (U3CdumpU3Ec__AnonStorey2_t1475667190 * __this, String_t* ___arg10, String_t* ___arg21, String_t* ___arg32, String_t* ___arg43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
