﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelController
struct PanelController_t24792858;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelController::.ctor()
extern "C"  void PanelController__ctor_m1185452505 (PanelController_t24792858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelController::Start()
extern "C"  void PanelController_Start_m1747718305 (PanelController_t24792858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelController::Update()
extern "C"  void PanelController_Update_m2340368764 (PanelController_t24792858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
