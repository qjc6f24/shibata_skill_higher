﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestController
struct TestController_t674116116;

#include "codegen/il2cpp-codegen.h"

// System.Void TestController::.ctor()
extern "C"  void TestController__ctor_m2120569257 (TestController_t674116116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestController::Start()
extern "C"  void TestController_Start_m1731473377 (TestController_t674116116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestController::Update()
extern "C"  void TestController_Update_m4000770690 (TestController_t674116116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TestController::judge()
extern "C"  int32_t TestController_judge_m4085086222 (TestController_t674116116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
