﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t764750278;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t3255436806;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t566549660;
// System.Action`1<System.Exception>
struct Action_1_t1729240069;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "websocketU2Dsharp_WebSocketSharp_Fin2752139063.h"
#include "websocketU2Dsharp_WebSocketSharp_Opcode2313788840.h"
#include "websocketU2Dsharp_WebSocketSharp_PayloadData3839327312.h"
#include "websocketU2Dsharp_WebSocketSharp_Rsv1058189029.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern "C"  void WebSocketFrame__cctor_m2029978231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern "C"  void WebSocketFrame__ctor_m1988968598 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern "C"  void WebSocketFrame__ctor_m4039525610 (WebSocketFrame_t764750278 * __this, uint8_t ___fin0, uint8_t ___opcode1, ByteU5BU5D_t3397334013* ___data2, bool ___compressed3, bool ___mask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern "C"  void WebSocketFrame__ctor_m2307151800 (WebSocketFrame_t764750278 * __this, uint8_t ___fin0, uint8_t ___opcode1, PayloadData_t3839327312 * ___payloadData2, bool ___compressed3, bool ___mask4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLengthCount()
extern "C"  int32_t WebSocketFrame_get_ExtendedPayloadLengthCount_m522041249 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 WebSocketSharp.WebSocketFrame::get_FullPayloadLength()
extern "C"  uint64_t WebSocketFrame_get_FullPayloadLength_m3591012120 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern "C"  bool WebSocketFrame_get_IsClose_m4037370433 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern "C"  bool WebSocketFrame_get_IsCompressed_m2728001770 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern "C"  bool WebSocketFrame_get_IsContinuation_m4031195624 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern "C"  bool WebSocketFrame_get_IsData_m180465203 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern "C"  bool WebSocketFrame_get_IsFinal_m2923746739 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragment()
extern "C"  bool WebSocketFrame_get_IsFragment_m2286655259 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern "C"  bool WebSocketFrame_get_IsMasked_m373012606 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern "C"  bool WebSocketFrame_get_IsPing_m1702797379 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern "C"  bool WebSocketFrame_get_IsPong_m3078149733 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern "C"  bool WebSocketFrame_get_IsText_m3469174916 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern "C"  uint64_t WebSocketFrame_get_Length_m1540738281 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern "C"  uint8_t WebSocketFrame_get_Opcode_m1205139909 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern "C"  PayloadData_t3839327312 * WebSocketFrame_get_PayloadData_m4003687813 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern "C"  uint8_t WebSocketFrame_get_Rsv2_m3102243471 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern "C"  uint8_t WebSocketFrame_get_Rsv3_m353157140 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern "C"  ByteU5BU5D_t3397334013* WebSocketFrame_createMaskingKey_m4228633051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern "C"  String_t* WebSocketFrame_dump_m4054277966 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern "C"  String_t* WebSocketFrame_print_m1232680307 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::processHeader(System.Byte[])
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_processHeader_m4003474107 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_readExtendedPayloadLengthAsync_m1529395252 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, WebSocketFrame_t764750278 * ___frame1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_readHeaderAsync_m4057604783 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, Action_1_t566549660 * ___completed1, Action_1_t1729240069 * ___error2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_readMaskingKeyAsync_m1840909340 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, WebSocketFrame_t764750278 * ___frame1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_readPayloadDataAsync_m3818797513 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, WebSocketFrame_t764750278 * ___frame1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.PayloadData,System.Boolean)
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_CreateCloseFrame_m2686217177 (Il2CppObject * __this /* static, unused */, PayloadData_t3839327312 * ___payloadData0, bool ___mask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(System.Boolean)
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_CreatePingFrame_m1746908400 (Il2CppObject * __this /* static, unused */, bool ___mask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.PayloadData,System.Boolean)
extern "C"  WebSocketFrame_t764750278 * WebSocketFrame_CreatePongFrame_m3364323429 (Il2CppObject * __this /* static, unused */, PayloadData_t3839327312 * ___payloadData0, bool ___mask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_ReadFrameAsync_m2244208000 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, bool ___unmask1, Action_1_t566549660 * ___completed2, Action_1_t1729240069 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::Unmask()
extern "C"  void WebSocketFrame_Unmask_m217551411 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern "C"  Il2CppObject* WebSocketFrame_GetEnumerator_m2330151961 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern "C"  String_t* WebSocketFrame_PrintToString_m2233387245 (WebSocketFrame_t764750278 * __this, bool ___dumped0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketFrame::ToArray()
extern "C"  ByteU5BU5D_t3397334013* WebSocketFrame_ToArray_m4086645856 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::ToString()
extern "C"  String_t* WebSocketFrame_ToString_m3964319027 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m3598500533 (WebSocketFrame_t764750278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
