﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.IList
struct IList_t3321498491;
// System.Text.Encoding
struct Encoding_t663144255;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_Encoding663144255.h"

// System.Int32 WebSocketSharp.Net.HttpUtility::getChar(System.String,System.Int32,System.Int32)
extern "C"  int32_t HttpUtility_getChar_m2917879435 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.HttpUtility::getInt(System.Byte)
extern "C"  int32_t HttpUtility_getInt_m135708171 (Il2CppObject * __this /* static, unused */, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpUtility::writeCharBytes(System.Char,System.Collections.IList,System.Text.Encoding)
extern "C"  void HttpUtility_writeCharBytes_m3800210614 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, Il2CppObject * ___buffer1, Encoding_t663144255 * ___encoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding WebSocketSharp.Net.HttpUtility::GetEncoding(System.String)
extern "C"  Encoding_t663144255 * HttpUtility_GetEncoding_m792195308 (Il2CppObject * __this /* static, unused */, String_t* ___contentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String)
extern "C"  String_t* HttpUtility_UrlDecode_m2084203637 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern "C"  String_t* HttpUtility_UrlDecode_m834738660 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t663144255 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpUtility::.cctor()
extern "C"  void HttpUtility__cctor_m223165678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
