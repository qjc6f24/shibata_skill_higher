﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// JSONObject
struct JSONObject_t1971882247;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t3886661509;
// JSONObject/AddJSONContents
struct AddJSONContents_t3850664647;
// JSONObject[]
struct JSONObjectU5BU5D_t2270799614;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1328083999;
// JSONObject/FieldNotFound
struct FieldNotFound_t865402053;
// JSONObject/GetFieldResponse
struct GetFieldResponse_t1259369279;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// JSONObjectEnumer
struct JSONObjectEnumer_t427597183;
// JSONObject/<BakeAsync>c__Iterator0
struct U3CBakeAsyncU3Ec__Iterator0_t1149809410;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// JSONObject/<PrintAsync>c__Iterator1
struct U3CPrintAsyncU3Ec__Iterator1_t716304657;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// JSONObject/<StringifyAsync>c__Iterator2
struct U3CStringifyAsyncU3Ec__Iterator2_t4037879552;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1568637717.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1568637717MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_Type1314578890.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1341003379MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1341003379.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3886661509.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3886661509MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21644006731.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En911718915.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En911718915MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21644006731MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_AddJSONCo3850664647.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_AddJSONCo3850664647MethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_FieldNotFo865402053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_FieldNotFo865402053MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_GetFieldR1259369279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_GetFieldR1259369279MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CBakeAs1149809410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CBakeAs1149809410.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CPrintAs716304657MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CPrintAs716304657.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CString4037879552MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_U3CString4037879552.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObjectEnumer427597183.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObjectEnumer427597183MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "System_System_Diagnostics_Stopwatch1380178105MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "System_System_Diagnostics_Stopwatch1380178105.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_Type1314578890MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONTemplates3006274921.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONTemplates3006274921MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Char>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisChar_t3454481338_m1428431425_gshared (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* p0, Il2CppChar p1, const MethodInfo* method);
#define Array_IndexOf_TisChar_t3454481338_m1428431425(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CharU5BU5D_t1328083999*, Il2CppChar, const MethodInfo*))Array_IndexOf_TisChar_t3454481338_m1428431425_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JSONObject::.ctor(JSONObject/Type)
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m3572921510_MetadataUsageId;
extern "C"  void JSONObject__ctor_m3572921510 (JSONObject_t1971882247 * __this, int32_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3572921510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___t0;
		__this->set_type_5(L_0);
		int32_t L_1 = ___t0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___t0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		goto IL_004b;
	}

IL_0020:
	{
		List_1_t1341003379 * L_3 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_3, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_3);
		goto IL_004b;
	}

IL_0030:
	{
		List_1_t1341003379 * L_4 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_4, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_4);
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_7(L_5);
		goto IL_004b;
	}

IL_004b:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(System.Boolean)
extern "C"  void JSONObject__ctor_m2099455527 (JSONObject_t1971882247 * __this, bool ___b0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(5);
		bool L_0 = ___b0;
		__this->set_b_12(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(System.Single)
extern "C"  void JSONObject__ctor_m3204764989 (JSONObject_t1971882247 * __this, float ___f0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(2);
		float L_0 = ___f0;
		__this->set_n_9(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(System.Int32)
extern "C"  void JSONObject__ctor_m3012147621 (JSONObject_t1971882247 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(2);
		int32_t L_0 = ___i0;
		__this->set_i_11((((int64_t)((int64_t)L_0))));
		__this->set_useInt_10((bool)1);
		int32_t L_1 = ___i0;
		__this->set_n_9((((float)((float)L_1))));
		return;
	}
}
// System.Void JSONObject::.ctor(System.Int64)
extern "C"  void JSONObject__ctor_m3818716510 (JSONObject_t1971882247 * __this, int64_t ___l0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(2);
		int64_t L_0 = ___l0;
		__this->set_i_11(L_0);
		__this->set_useInt_10((bool)1);
		int64_t L_1 = ___l0;
		__this->set_n_9((((float)((float)L_1))));
		return;
	}
}
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4048655796_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2641560888_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2909389725_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3978419092_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m52089307_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m2422585723_MetadataUsageId;
extern "C"  void JSONObject__ctor_m2422585723 (JSONObject_t1971882247 * __this, Dictionary_2_t3943999495 * ___dic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m2422585723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1701344717  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t969056901  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(3);
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_7(L_0);
		List_1_t1341003379 * L_1 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_1, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_1);
		Dictionary_2_t3943999495 * L_2 = ___dic0;
		NullCheck(L_2);
		Enumerator_t969056901  L_3 = Dictionary_2_GetEnumerator_m2456297074(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var);
		V_1 = L_3;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_002f:
		{
			KeyValuePair_2_t1701344717  L_4 = Enumerator_get_Current_m4048655796((&V_1), /*hidden argument*/Enumerator_get_Current_m4048655796_MethodInfo_var);
			V_0 = L_4;
			List_1_t1398341365 * L_5 = __this->get_keys_7();
			String_t* L_6 = KeyValuePair_2_get_Key_m2641560888((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2641560888_MethodInfo_var);
			NullCheck(L_5);
			List_1_Add_m4061286785(L_5, L_6, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
			List_1_t1341003379 * L_7 = __this->get_list_6();
			String_t* L_8 = KeyValuePair_2_get_Value_m2909389725((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m2909389725_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
			JSONObject_t1971882247 * L_9 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			List_1_Add_m3978419092(L_7, L_9, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
		}

IL_0060:
		{
			bool L_10 = Enumerator_MoveNext_m52089307((&V_1), /*hidden argument*/Enumerator_MoveNext_m52089307_MethodInfo_var);
			if (L_10)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m882561911((&V_1), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007f:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,JSONObject>)
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1930451981_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1144143097_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m855618423_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m674158924_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3978419092_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m630265068_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m214092148_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m3385912102_MetadataUsageId;
extern "C"  void JSONObject__ctor_m3385912102 (JSONObject_t1971882247 * __this, Dictionary_2_t3886661509 * ___dic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3385912102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1644006731  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t911718915  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(3);
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_7(L_0);
		List_1_t1341003379 * L_1 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_1, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_1);
		Dictionary_2_t3886661509 * L_2 = ___dic0;
		NullCheck(L_2);
		Enumerator_t911718915  L_3 = Dictionary_2_GetEnumerator_m1930451981(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m1930451981_MethodInfo_var);
		V_1 = L_3;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_002f:
		{
			KeyValuePair_2_t1644006731  L_4 = Enumerator_get_Current_m1144143097((&V_1), /*hidden argument*/Enumerator_get_Current_m1144143097_MethodInfo_var);
			V_0 = L_4;
			List_1_t1398341365 * L_5 = __this->get_keys_7();
			String_t* L_6 = KeyValuePair_2_get_Key_m855618423((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m855618423_MethodInfo_var);
			NullCheck(L_5);
			List_1_Add_m4061286785(L_5, L_6, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
			List_1_t1341003379 * L_7 = __this->get_list_6();
			JSONObject_t1971882247 * L_8 = KeyValuePair_2_get_Value_m674158924((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m674158924_MethodInfo_var);
			NullCheck(L_7);
			List_1_Add_m3978419092(L_7, L_8, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
		}

IL_005b:
		{
			bool L_9 = Enumerator_MoveNext_m630265068((&V_1), /*hidden argument*/Enumerator_MoveNext_m630265068_MethodInfo_var);
			if (L_9)
			{
				goto IL_002f;
			}
		}

IL_0067:
		{
			IL2CPP_LEAVE(0x7A, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m214092148((&V_1), /*hidden argument*/Enumerator_Dispose_m214092148_MethodInfo_var);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007a:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject/AddJSONContents)
extern "C"  void JSONObject__ctor_m1425064717 (JSONObject_t1971882247 * __this, AddJSONContents_t3850664647 * ___content0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		AddJSONContents_t3850664647 * L_0 = ___content0;
		NullCheck(L_0);
		AddJSONContents_Invoke_m412080169(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject[])
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m587359300_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m3296569527_MetadataUsageId;
extern "C"  void JSONObject__ctor_m3296569527 (JSONObject_t1971882247 * __this, JSONObjectU5BU5D_t2270799614* ___objs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3296569527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_type_5(4);
		JSONObjectU5BU5D_t2270799614* L_0 = ___objs0;
		List_1_t1341003379 * L_1 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m587359300(L_1, (Il2CppObject*)(Il2CppObject*)L_0, /*hidden argument*/List_1__ctor_m587359300_MethodInfo_var);
		__this->set_list_6(L_1);
		return;
	}
}
// System.Void JSONObject::.ctor()
extern "C"  void JSONObject__ctor_m2218833806 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.ctor(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  void JSONObject__ctor_m2243138423 (JSONObject_t1971882247 * __this, String_t* ___str0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str0;
		int32_t L_1 = ___maxDepth1;
		bool L_2 = ___storeExcessLevels2;
		bool L_3 = ___strict3;
		JSONObject_Parse_m32158862(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject::get_isContainer()
extern "C"  bool JSONObject_get_isContainer_m2971179132 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_type_5();
		if ((((int32_t)L_0) == ((int32_t)4)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_type_5();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 JSONObject::get_Count()
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern const uint32_t JSONObject_get_Count_m3265098784_MetadataUsageId;
extern "C"  int32_t JSONObject_get_Count_m3265098784 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_Count_m3265098784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		List_1_t1341003379 * L_1 = __this->get_list_6();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m1879545138(L_1, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		return L_2;
	}
}
// System.Single JSONObject::get_f()
extern "C"  float JSONObject_get_f_m2513971409 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_n_9();
		return L_0;
	}
}
// JSONObject JSONObject::get_nullJO()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_nullJO_m928623965_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_nullJO_m928623965 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_nullJO_m928623965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m2839115232(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::get_obj()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_obj_m1428033972_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_obj_m1428033972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_obj_m1428033972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m2839115232(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::get_arr()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_arr_m3127667794_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_arr_m3127667794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_arr_m3127667794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m2839115232(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::StringObject(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_StringObject_m536719572_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_StringObject_m536719572 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_StringObject_m536719572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::Absorb(JSONObject)
extern const MethodInfo* List_1_AddRange_m276214970_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2513889489_MethodInfo_var;
extern const uint32_t JSONObject_Absorb_m2114544266_MetadataUsageId;
extern "C"  void JSONObject_Absorb_m2114544266 (JSONObject_t1971882247 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Absorb_m2114544266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_6();
		JSONObject_t1971882247 * L_1 = ___obj0;
		NullCheck(L_1);
		List_1_t1341003379 * L_2 = L_1->get_list_6();
		NullCheck(L_0);
		List_1_AddRange_m276214970(L_0, L_2, /*hidden argument*/List_1_AddRange_m276214970_MethodInfo_var);
		List_1_t1398341365 * L_3 = __this->get_keys_7();
		JSONObject_t1971882247 * L_4 = ___obj0;
		NullCheck(L_4);
		List_1_t1398341365 * L_5 = L_4->get_keys_7();
		NullCheck(L_3);
		List_1_AddRange_m2513889489(L_3, L_5, /*hidden argument*/List_1_AddRange_m2513889489_MethodInfo_var);
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_str_8();
		__this->set_str_8(L_7);
		JSONObject_t1971882247 * L_8 = ___obj0;
		NullCheck(L_8);
		float L_9 = L_8->get_n_9();
		__this->set_n_9(L_9);
		JSONObject_t1971882247 * L_10 = ___obj0;
		NullCheck(L_10);
		bool L_11 = L_10->get_useInt_10();
		__this->set_useInt_10(L_11);
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		int64_t L_13 = L_12->get_i_11();
		__this->set_i_11(L_13);
		JSONObject_t1971882247 * L_14 = ___obj0;
		NullCheck(L_14);
		bool L_15 = L_14->get_b_12();
		__this->set_b_12(L_15);
		JSONObject_t1971882247 * L_16 = ___obj0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_type_5();
		__this->set_type_5(L_17);
		return;
	}
}
// JSONObject JSONObject::Create()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m3929211220_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m3929211220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m3929211220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t1971882247 * L_0 = (JSONObject_t1971882247 *)il2cpp_codegen_object_new(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2218833806(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::Create(JSONObject/Type)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t JSONObject_Create_m2839115232_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m2839115232 (Il2CppObject * __this /* static, unused */, int32_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2839115232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		int32_t L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_type_5(L_2);
		int32_t L_3 = ___t0;
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_4 = ___t0;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		goto IL_004b;
	}

IL_0020:
	{
		JSONObject_t1971882247 * L_5 = V_0;
		List_1_t1341003379 * L_6 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_6, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		NullCheck(L_5);
		L_5->set_list_6(L_6);
		goto IL_004b;
	}

IL_0030:
	{
		JSONObject_t1971882247 * L_7 = V_0;
		List_1_t1341003379 * L_8 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_8, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_list_6(L_8);
		JSONObject_t1971882247 * L_9 = V_0;
		List_1_t1398341365 * L_10 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_10, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		NullCheck(L_9);
		L_9->set_keys_7(L_10);
		goto IL_004b;
	}

IL_004b:
	{
		JSONObject_t1971882247 * L_11 = V_0;
		return L_11;
	}
}
// JSONObject JSONObject::Create(System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m311647169_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m311647169 (Il2CppObject * __this /* static, unused */, bool ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m311647169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(5);
		JSONObject_t1971882247 * L_2 = V_0;
		bool L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_b_12(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m3209723715_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m3209723715 (Il2CppObject * __this /* static, unused */, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m3209723715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(2);
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_n_9(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m1100244703_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m1100244703 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m1100244703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(2);
		JSONObject_t1971882247 * L_2 = V_0;
		int32_t L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_n_9((((float)((float)L_3))));
		JSONObject_t1971882247 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_useInt_10((bool)1);
		JSONObject_t1971882247 * L_5 = V_0;
		int32_t L_6 = ___val0;
		NullCheck(L_5);
		L_5->set_i_11((((int64_t)((int64_t)L_6))));
		JSONObject_t1971882247 * L_7 = V_0;
		return L_7;
	}
}
// JSONObject JSONObject::Create(System.Int64)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m4232412420_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m4232412420 (Il2CppObject * __this /* static, unused */, int64_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m4232412420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(2);
		JSONObject_t1971882247 * L_2 = V_0;
		int64_t L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_n_9((((float)((float)L_3))));
		JSONObject_t1971882247 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_useInt_10((bool)1);
		JSONObject_t1971882247 * L_5 = V_0;
		int64_t L_6 = ___val0;
		NullCheck(L_5);
		L_5->set_i_11(L_6);
		JSONObject_t1971882247 * L_7 = V_0;
		return L_7;
	}
}
// JSONObject JSONObject::CreateStringObject(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_CreateStringObject_m1387837628_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_CreateStringObject_m1387837628 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_CreateStringObject_m1387837628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(1);
		JSONObject_t1971882247 * L_2 = V_0;
		String_t* L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_str_8(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::CreateBakedObject(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_CreateBakedObject_m3909534024_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_CreateBakedObject_m3909534024 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_CreateBakedObject_m3909534024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(6);
		JSONObject_t1971882247 * L_2 = V_0;
		String_t* L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_str_8(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.String,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m2628830417_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m2628830417 (Il2CppObject * __this /* static, unused */, String_t* ___val0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2628830417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		String_t* L_2 = ___val0;
		int32_t L_3 = ___maxDepth1;
		bool L_4 = ___storeExcessLevels2;
		bool L_5 = ___strict3;
		NullCheck(L_1);
		JSONObject_Parse_m32158862(L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_6 = V_0;
		return L_6;
	}
}
// JSONObject JSONObject::Create(JSONObject/AddJSONContents)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m950353587_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m950353587 (Il2CppObject * __this /* static, unused */, AddJSONContents_t3850664647 * ___content0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m950353587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		AddJSONContents_t3850664647 * L_1 = ___content0;
		JSONObject_t1971882247 * L_2 = V_0;
		NullCheck(L_1);
		AddJSONContents_Invoke_m412080169(L_1, L_2, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_3 = V_0;
		return L_3;
	}
}
// JSONObject JSONObject::Create(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4048655796_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2641560888_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2909389725_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3978419092_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m52089307_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern const uint32_t JSONObject_Create_m3190450357_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Create_m3190450357 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___dic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m3190450357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t969056901  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_5(3);
		JSONObject_t1971882247 * L_2 = V_0;
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		NullCheck(L_2);
		L_2->set_keys_7(L_3);
		JSONObject_t1971882247 * L_4 = V_0;
		List_1_t1341003379 * L_5 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_5, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_list_6(L_5);
		Dictionary_2_t3943999495 * L_6 = ___dic0;
		NullCheck(L_6);
		Enumerator_t969056901  L_7 = Dictionary_2_GetEnumerator_m2456297074(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var);
		V_2 = L_7;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_002f:
		{
			KeyValuePair_2_t1701344717  L_8 = Enumerator_get_Current_m4048655796((&V_2), /*hidden argument*/Enumerator_get_Current_m4048655796_MethodInfo_var);
			V_1 = L_8;
			JSONObject_t1971882247 * L_9 = V_0;
			NullCheck(L_9);
			List_1_t1398341365 * L_10 = L_9->get_keys_7();
			String_t* L_11 = KeyValuePair_2_get_Key_m2641560888((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2641560888_MethodInfo_var);
			NullCheck(L_10);
			List_1_Add_m4061286785(L_10, L_11, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
			JSONObject_t1971882247 * L_12 = V_0;
			NullCheck(L_12);
			List_1_t1341003379 * L_13 = L_12->get_list_6();
			String_t* L_14 = KeyValuePair_2_get_Value_m2909389725((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m2909389725_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
			JSONObject_t1971882247 * L_15 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			NullCheck(L_13);
			List_1_Add_m3978419092(L_13, L_15, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
		}

IL_0060:
		{
			bool L_16 = Enumerator_MoveNext_m52089307((&V_2), /*hidden argument*/Enumerator_MoveNext_m52089307_MethodInfo_var);
			if (L_16)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m882561911((&V_2), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007f:
	{
		JSONObject_t1971882247 * L_17 = V_0;
		return L_17;
	}
}
// System.Void JSONObject::Parse(System.String,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* Array_IndexOf_TisChar_t3454481338_m1428431425_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3978419092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral916993783;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral1751346954;
extern Il2CppCodeGenString* _stringLiteral376316188;
extern Il2CppCodeGenString* _stringLiteral452738781;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral150668183;
extern const uint32_t JSONObject_Parse_m32158862_MetadataUsageId;
extern "C"  void JSONObject_Parse_m32158862 (JSONObject_t1971882247 * __this, String_t* ___str0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Parse_m32158862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	String_t* V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B67_0 = NULL;
	List_1_t1341003379 * G_B67_1 = NULL;
	String_t* G_B66_0 = NULL;
	List_1_t1341003379 * G_B66_1 = NULL;
	int32_t G_B68_0 = 0;
	String_t* G_B68_1 = NULL;
	List_1_t1341003379 * G_B68_2 = NULL;
	{
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0400;
		}
	}
	{
		String_t* L_2 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_3 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_4();
		NullCheck(L_2);
		String_t* L_4 = String_Trim_m3982520224(L_2, L_3, /*hidden argument*/NULL);
		___str0 = L_4;
		bool L_5 = ___strict3;
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_6 = ___str0;
		NullCheck(L_6);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_6, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)91))))
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_8 = ___str0;
		NullCheck(L_8);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_8, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)123))))
		{
			goto IL_004d;
		}
	}
	{
		__this->set_type_5(0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral916993783, /*hidden argument*/NULL);
		return;
	}

IL_004d:
	{
		String_t* L_10 = ___str0;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_03f4;
		}
	}
	{
		String_t* L_12 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_13 = String_Compare_m2851607672(NULL /*static, unused*/, L_12, _stringLiteral3323263070, (bool)1, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_type_5(5);
		__this->set_b_12((bool)1);
		goto IL_03ef;
	}

IL_007d:
	{
		String_t* L_14 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_15 = String_Compare_m2851607672(NULL /*static, unused*/, L_14, _stringLiteral2609877245, (bool)1, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00a1;
		}
	}
	{
		__this->set_type_5(5);
		__this->set_b_12((bool)0);
		goto IL_03ef;
	}

IL_00a1:
	{
		String_t* L_16 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_17 = String_Compare_m2851607672(NULL /*static, unused*/, L_16, _stringLiteral1743624307, (bool)1, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_type_5(0);
		goto IL_03ef;
	}

IL_00be:
	{
		String_t* L_18 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral1751346954, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e5;
		}
	}
	{
		__this->set_type_5(2);
		__this->set_n_9((std::numeric_limits<float>::infinity()));
		goto IL_03ef;
	}

IL_00e5:
	{
		String_t* L_20 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, _stringLiteral376316188, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_010c;
		}
	}
	{
		__this->set_type_5(2);
		__this->set_n_9((-std::numeric_limits<float>::infinity()));
		goto IL_03ef;
	}

IL_010c:
	{
		String_t* L_22 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_22, _stringLiteral452738781, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0133;
		}
	}
	{
		__this->set_type_5(2);
		__this->set_n_9((std::numeric_limits<float>::quiet_NaN()));
		goto IL_03ef;
	}

IL_0133:
	{
		String_t* L_24 = ___str0;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m4230566705(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0162;
		}
	}
	{
		__this->set_type_5(1);
		String_t* L_26 = ___str0;
		String_t* L_27 = ___str0;
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m1606060069(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_29 = String_Substring_m12482732(L_26, 1, ((int32_t)((int32_t)L_28-(int32_t)2)), /*hidden argument*/NULL);
		__this->set_str_8(L_29);
		goto IL_03ef;
	}

IL_0162:
	{
		V_0 = 1;
		V_1 = 0;
		String_t* L_30 = ___str0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		Il2CppChar L_32 = String_get_Chars_m4230566705(L_30, L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		Il2CppChar L_33 = V_2;
		if ((((int32_t)L_33) == ((int32_t)((int32_t)123))))
		{
			goto IL_0183;
		}
	}
	{
		Il2CppChar L_34 = V_2;
		if ((((int32_t)L_34) == ((int32_t)((int32_t)91))))
		{
			goto IL_01a5;
		}
	}
	{
		goto IL_01bc;
	}

IL_0183:
	{
		__this->set_type_5(3);
		List_1_t1398341365 * L_35 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_35, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_7(L_35);
		List_1_t1341003379 * L_36 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_36, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_36);
		goto IL_0215;
	}

IL_01a5:
	{
		__this->set_type_5(4);
		List_1_t1341003379 * L_37 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_37, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_37);
		goto IL_0215;
	}

IL_01bc:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_38 = ___str0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
			float L_39 = Convert_ToSingle_m1977583125(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
			__this->set_n_9(L_39);
			String_t* L_40 = ___str0;
			NullCheck(L_40);
			bool L_41 = String_Contains_m4017059963(L_40, _stringLiteral372029316, /*hidden argument*/NULL);
			if (L_41)
			{
				goto IL_01eb;
			}
		}

IL_01d8:
		{
			String_t* L_42 = ___str0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
			int64_t L_43 = Convert_ToInt64_m3181519185(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
			__this->set_i_11(L_43);
			__this->set_useInt_10((bool)1);
		}

IL_01eb:
		{
			__this->set_type_5(2);
			goto IL_0214;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01f7;
		throw e;
	}

CATCH_01f7:
	{ // begin catch(System.FormatException)
		__this->set_type_5(0);
		String_t* L_44 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral150668183, L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		goto IL_0214;
	} // end catch (depth: 1)

IL_0214:
	{
		return;
	}

IL_0215:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_46;
		V_4 = (bool)0;
		V_5 = (bool)0;
		V_6 = 0;
		goto IL_03df;
	}

IL_0229:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_47 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_4();
		String_t* L_48 = ___str0;
		int32_t L_49 = V_1;
		NullCheck(L_48);
		Il2CppChar L_50 = String_get_Chars_m4230566705(L_48, L_49, /*hidden argument*/NULL);
		int32_t L_51 = Array_IndexOf_TisChar_t3454481338_m1428431425(NULL /*static, unused*/, L_47, L_50, /*hidden argument*/Array_IndexOf_TisChar_t3454481338_m1428431425_MethodInfo_var);
		if ((((int32_t)L_51) <= ((int32_t)(-1))))
		{
			goto IL_0245;
		}
	}
	{
		goto IL_03df;
	}

IL_0245:
	{
		String_t* L_52 = ___str0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		Il2CppChar L_54 = String_get_Chars_m4230566705(L_52, L_53, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_025c;
		}
	}
	{
		int32_t L_55 = V_1;
		V_1 = ((int32_t)((int32_t)L_55+(int32_t)1));
		goto IL_03df;
	}

IL_025c:
	{
		String_t* L_56 = ___str0;
		int32_t L_57 = V_1;
		NullCheck(L_56);
		Il2CppChar L_58 = String_get_Chars_m4230566705(L_56, L_57, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_02ba;
		}
	}
	{
		bool L_59 = V_4;
		if (!L_59)
		{
			goto IL_02a2;
		}
	}
	{
		bool L_60 = V_5;
		if (L_60)
		{
			goto IL_029a;
		}
	}
	{
		int32_t L_61 = V_6;
		if (L_61)
		{
			goto IL_029a;
		}
	}
	{
		int32_t L_62 = __this->get_type_5();
		if ((!(((uint32_t)L_62) == ((uint32_t)3))))
		{
			goto IL_029a;
		}
	}
	{
		String_t* L_63 = ___str0;
		int32_t L_64 = V_0;
		int32_t L_65 = V_1;
		int32_t L_66 = V_0;
		NullCheck(L_63);
		String_t* L_67 = String_Substring_m12482732(L_63, ((int32_t)((int32_t)L_64+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_65-(int32_t)L_66))-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_67;
	}

IL_029a:
	{
		V_4 = (bool)0;
		goto IL_02ba;
	}

IL_02a2:
	{
		int32_t L_68 = V_6;
		if (L_68)
		{
			goto IL_02b7;
		}
	}
	{
		int32_t L_69 = __this->get_type_5();
		if ((!(((uint32_t)L_69) == ((uint32_t)3))))
		{
			goto IL_02b7;
		}
	}
	{
		int32_t L_70 = V_1;
		V_0 = L_70;
	}

IL_02b7:
	{
		V_4 = (bool)1;
	}

IL_02ba:
	{
		bool L_71 = V_4;
		if (!L_71)
		{
			goto IL_02c6;
		}
	}
	{
		goto IL_03df;
	}

IL_02c6:
	{
		int32_t L_72 = __this->get_type_5();
		if ((!(((uint32_t)L_72) == ((uint32_t)3))))
		{
			goto IL_02ee;
		}
	}
	{
		int32_t L_73 = V_6;
		if (L_73)
		{
			goto IL_02ee;
		}
	}
	{
		String_t* L_74 = ___str0;
		int32_t L_75 = V_1;
		NullCheck(L_74);
		Il2CppChar L_76 = String_get_Chars_m4230566705(L_74, L_75, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_76) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_02ee;
		}
	}
	{
		int32_t L_77 = V_1;
		V_0 = ((int32_t)((int32_t)L_77+(int32_t)1));
		V_5 = (bool)1;
	}

IL_02ee:
	{
		String_t* L_78 = ___str0;
		int32_t L_79 = V_1;
		NullCheck(L_78);
		Il2CppChar L_80 = String_get_Chars_m4230566705(L_78, L_79, /*hidden argument*/NULL);
		if ((((int32_t)L_80) == ((int32_t)((int32_t)91))))
		{
			goto IL_030a;
		}
	}
	{
		String_t* L_81 = ___str0;
		int32_t L_82 = V_1;
		NullCheck(L_81);
		Il2CppChar L_83 = String_get_Chars_m4230566705(L_81, L_82, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_83) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_0315;
		}
	}

IL_030a:
	{
		int32_t L_84 = V_6;
		V_6 = ((int32_t)((int32_t)L_84+(int32_t)1));
		goto IL_0337;
	}

IL_0315:
	{
		String_t* L_85 = ___str0;
		int32_t L_86 = V_1;
		NullCheck(L_85);
		Il2CppChar L_87 = String_get_Chars_m4230566705(L_85, L_86, /*hidden argument*/NULL);
		if ((((int32_t)L_87) == ((int32_t)((int32_t)93))))
		{
			goto IL_0331;
		}
	}
	{
		String_t* L_88 = ___str0;
		int32_t L_89 = V_1;
		NullCheck(L_88);
		Il2CppChar L_90 = String_get_Chars_m4230566705(L_88, L_89, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_90) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0337;
		}
	}

IL_0331:
	{
		int32_t L_91 = V_6;
		V_6 = ((int32_t)((int32_t)L_91-(int32_t)1));
	}

IL_0337:
	{
		String_t* L_92 = ___str0;
		int32_t L_93 = V_1;
		NullCheck(L_92);
		Il2CppChar L_94 = String_get_Chars_m4230566705(L_92, L_93, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_94) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_034c;
		}
	}
	{
		int32_t L_95 = V_6;
		if (!L_95)
		{
			goto IL_0354;
		}
	}

IL_034c:
	{
		int32_t L_96 = V_6;
		if ((((int32_t)L_96) >= ((int32_t)0)))
		{
			goto IL_03df;
		}
	}

IL_0354:
	{
		V_5 = (bool)0;
		String_t* L_97 = ___str0;
		int32_t L_98 = V_0;
		int32_t L_99 = V_1;
		int32_t L_100 = V_0;
		NullCheck(L_97);
		String_t* L_101 = String_Substring_m12482732(L_97, L_98, ((int32_t)((int32_t)L_99-(int32_t)L_100)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_102 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_4();
		NullCheck(L_101);
		String_t* L_103 = String_Trim_m3982520224(L_101, L_102, /*hidden argument*/NULL);
		V_7 = L_103;
		String_t* L_104 = V_7;
		NullCheck(L_104);
		int32_t L_105 = String_get_Length_m1606060069(L_104, /*hidden argument*/NULL);
		if ((((int32_t)L_105) <= ((int32_t)0)))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_106 = __this->get_type_5();
		if ((!(((uint32_t)L_106) == ((uint32_t)3))))
		{
			goto IL_0392;
		}
	}
	{
		List_1_t1398341365 * L_107 = __this->get_keys_7();
		String_t* L_108 = V_3;
		NullCheck(L_107);
		List_1_Add_m4061286785(L_107, L_108, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
	}

IL_0392:
	{
		int32_t L_109 = ___maxDepth1;
		if ((((int32_t)L_109) == ((int32_t)(-1))))
		{
			goto IL_03c3;
		}
	}
	{
		List_1_t1341003379 * L_110 = __this->get_list_6();
		String_t* L_111 = V_7;
		int32_t L_112 = ___maxDepth1;
		G_B66_0 = L_111;
		G_B66_1 = L_110;
		if ((((int32_t)L_112) >= ((int32_t)(-1))))
		{
			G_B67_0 = L_111;
			G_B67_1 = L_110;
			goto IL_03af;
		}
	}
	{
		G_B68_0 = ((int32_t)-2);
		G_B68_1 = G_B66_0;
		G_B68_2 = G_B66_1;
		goto IL_03b2;
	}

IL_03af:
	{
		int32_t L_113 = ___maxDepth1;
		G_B68_0 = ((int32_t)((int32_t)L_113-(int32_t)1));
		G_B68_1 = G_B67_0;
		G_B68_2 = G_B67_1;
	}

IL_03b2:
	{
		bool L_114 = ___storeExcessLevels2;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_115 = JSONObject_Create_m2628830417(NULL /*static, unused*/, G_B68_1, G_B68_0, L_114, (bool)0, /*hidden argument*/NULL);
		NullCheck(G_B68_2);
		List_1_Add_m3978419092(G_B68_2, L_115, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
		goto IL_03db;
	}

IL_03c3:
	{
		bool L_116 = ___storeExcessLevels2;
		if (!L_116)
		{
			goto IL_03db;
		}
	}
	{
		List_1_t1341003379 * L_117 = __this->get_list_6();
		String_t* L_118 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_119 = JSONObject_CreateBakedObject_m3909534024(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
		NullCheck(L_117);
		List_1_Add_m3978419092(L_117, L_119, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
	}

IL_03db:
	{
		int32_t L_120 = V_1;
		V_0 = ((int32_t)((int32_t)L_120+(int32_t)1));
	}

IL_03df:
	{
		int32_t L_121 = V_1;
		int32_t L_122 = ((int32_t)((int32_t)L_121+(int32_t)1));
		V_1 = L_122;
		String_t* L_123 = ___str0;
		NullCheck(L_123);
		int32_t L_124 = String_get_Length_m1606060069(L_123, /*hidden argument*/NULL);
		if ((((int32_t)L_122) < ((int32_t)L_124)))
		{
			goto IL_0229;
		}
	}

IL_03ef:
	{
		goto IL_03fb;
	}

IL_03f4:
	{
		__this->set_type_5(0);
	}

IL_03fb:
	{
		goto IL_0407;
	}

IL_0400:
	{
		__this->set_type_5(0);
	}

IL_0407:
	{
		return;
	}
}
// System.Boolean JSONObject::get_IsNumber()
extern "C"  bool JSONObject_get_IsNumber_m632669178 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_5();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsNull()
extern "C"  bool JSONObject_get_IsNull_m2948283770 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_5();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsString()
extern "C"  bool JSONObject_get_IsString_m3878590566 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_5();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsBool()
extern "C"  bool JSONObject_get_IsBool_m1340073173 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_5();
		return (bool)((((int32_t)L_0) == ((int32_t)5))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsArray()
extern "C"  bool JSONObject_get_IsArray_m1146507746 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_5();
		return (bool)((((int32_t)L_0) == ((int32_t)4))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsObject()
extern "C"  bool JSONObject_get_IsObject_m1393183916 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_type_5();
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_type_5();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)6))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Void JSONObject::Add(System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m2030604252_MetadataUsageId;
extern "C"  void JSONObject_Add_m2030604252 (JSONObject_t1971882247 * __this, bool ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m2030604252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m311647169(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m2474874762(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m3290419010_MetadataUsageId;
extern "C"  void JSONObject_Add_m3290419010 (JSONObject_t1971882247 * __this, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m3290419010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m3209723715(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m2474874762(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m2036826576_MetadataUsageId;
extern "C"  void JSONObject_Add_m2036826576 (JSONObject_t1971882247 * __this, int32_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m2036826576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m1100244703(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m2474874762(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m492836235_MetadataUsageId;
extern "C"  void JSONObject_Add_m492836235 (JSONObject_t1971882247 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m492836235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m2474874762(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(JSONObject/AddJSONContents)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m255018418_MetadataUsageId;
extern "C"  void JSONObject_Add_m255018418 (JSONObject_t1971882247 * __this, AddJSONContents_t3850664647 * ___content0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m255018418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddJSONContents_t3850664647 * L_0 = ___content0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m950353587(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m2474874762(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3978419092_MethodInfo_var;
extern const uint32_t JSONObject_Add_m2474874762_MetadataUsageId;
extern "C"  void JSONObject_Add_m2474874762 (JSONObject_t1971882247 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m2474874762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_1 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_2 = __this->get_type_5();
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0034;
		}
	}
	{
		__this->set_type_5(4);
		List_1_t1341003379 * L_3 = __this->get_list_6();
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1341003379 * L_4 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_4, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_4);
	}

IL_0034:
	{
		List_1_t1341003379 * L_5 = __this->get_list_6();
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_5);
		List_1_Add_m3978419092(L_5, L_6, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
	}

IL_0040:
	{
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m1576819900_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1576819900 (JSONObject_t1971882247 * __this, String_t* ___name0, bool ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1576819900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m311647169(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1302524246(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2737597678_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2737597678 (JSONObject_t1971882247 * __this, String_t* ___name0, float ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2737597678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		float L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m3209723715(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1302524246(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m1847913364_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1847913364 (JSONObject_t1971882247 * __this, String_t* ___name0, int32_t ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1847913364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m1100244703(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1302524246(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Int64)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2829144319_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2829144319 (JSONObject_t1971882247 * __this, String_t* ___name0, int64_t ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2829144319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		int64_t L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m4232412420(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1302524246(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,JSONObject/AddJSONContents)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2074310270_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2074310270 (JSONObject_t1971882247 * __this, String_t* ___name0, AddJSONContents_t3850664647 * ___content1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2074310270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		AddJSONContents_t3850664647 * L_1 = ___content1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m950353587(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1302524246(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m1305136679_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1305136679 (JSONObject_t1971882247 * __this, String_t* ___name0, String_t* ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1305136679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m1302524246(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1341003379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1371584160_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3978419092_MethodInfo_var;
extern const uint32_t JSONObject_AddField_m1302524246_MetadataUsageId;
extern "C"  void JSONObject_AddField_m1302524246 (JSONObject_t1971882247 * __this, String_t* ___name0, JSONObject_t1971882247 * ___obj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m1302524246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		JSONObject_t1971882247 * L_0 = ___obj1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_1 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_2 = __this->get_type_5();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0092;
		}
	}
	{
		List_1_t1398341365 * L_3 = __this->get_keys_7();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1398341365 * L_4 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_4, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_keys_7(L_4);
	}

IL_002d:
	{
		int32_t L_5 = __this->get_type_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)4))))
		{
			goto IL_0075;
		}
	}
	{
		V_0 = 0;
		goto IL_005f;
	}

IL_0040:
	{
		List_1_t1398341365 * L_6 = __this->get_keys_7();
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_11 = String_Concat_m56707527(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_Add_m4061286785(L_6, L_11, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_13 = V_0;
		List_1_t1341003379 * L_14 = __this->get_list_6();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m1879545138(L_14, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_008b;
	}

IL_0075:
	{
		List_1_t1341003379 * L_16 = __this->get_list_6();
		if (L_16)
		{
			goto IL_008b;
		}
	}
	{
		List_1_t1341003379 * L_17 = (List_1_t1341003379 *)il2cpp_codegen_object_new(List_1_t1341003379_il2cpp_TypeInfo_var);
		List_1__ctor_m1371584160(L_17, /*hidden argument*/List_1__ctor_m1371584160_MethodInfo_var);
		__this->set_list_6(L_17);
	}

IL_008b:
	{
		__this->set_type_5(3);
	}

IL_0092:
	{
		List_1_t1398341365 * L_18 = __this->get_keys_7();
		String_t* L_19 = ___name0;
		NullCheck(L_18);
		List_1_Add_m4061286785(L_18, L_19, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1341003379 * L_20 = __this->get_list_6();
		JSONObject_t1971882247 * L_21 = ___obj1;
		NullCheck(L_20);
		List_1_Add_m3978419092(L_20, L_21, /*hidden argument*/List_1_Add_m3978419092_MethodInfo_var);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.String)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m586990890_MetadataUsageId;
extern "C"  void JSONObject_SetField_m586990890 (JSONObject_t1971882247 * __this, String_t* ___name0, String_t* ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m586990890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m2709314095(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Boolean)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m994904533_MetadataUsageId;
extern "C"  void JSONObject_SetField_m994904533 (JSONObject_t1971882247 * __this, String_t* ___name0, bool ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m994904533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m311647169(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m2709314095(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Single)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m776874027_MetadataUsageId;
extern "C"  void JSONObject_SetField_m776874027 (JSONObject_t1971882247 * __this, String_t* ___name0, float ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m776874027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		float L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m3209723715(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m2709314095(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Int32)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m1382612311_MetadataUsageId;
extern "C"  void JSONObject_SetField_m1382612311 (JSONObject_t1971882247 * __this, String_t* ___name0, int32_t ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m1382612311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_2 = JSONObject_Create_m1100244703(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m2709314095(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,JSONObject)
extern const MethodInfo* List_1_Remove_m3780843529_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m875018944_MethodInfo_var;
extern const uint32_t JSONObject_SetField_m2709314095_MetadataUsageId;
extern "C"  void JSONObject_SetField_m2709314095 (JSONObject_t1971882247 * __this, String_t* ___name0, JSONObject_t1971882247 * ___obj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m2709314095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = JSONObject_HasField_m4286787708(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t1341003379 * L_2 = __this->get_list_6();
		String_t* L_3 = ___name0;
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m223257276(__this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_Remove_m3780843529(L_2, L_4, /*hidden argument*/List_1_Remove_m3780843529_MethodInfo_var);
		List_1_t1398341365 * L_5 = __this->get_keys_7();
		String_t* L_6 = ___name0;
		NullCheck(L_5);
		List_1_Remove_m875018944(L_5, L_6, /*hidden argument*/List_1_Remove_m875018944_MethodInfo_var);
	}

IL_002c:
	{
		String_t* L_7 = ___name0;
		JSONObject_t1971882247 * L_8 = ___obj1;
		JSONObject_AddField_m1302524246(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::RemoveField(System.String)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m1879454498_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m875018944_MethodInfo_var;
extern const uint32_t JSONObject_RemoveField_m1226873592_MetadataUsageId;
extern "C"  void JSONObject_RemoveField_m1226873592 (JSONObject_t1971882247 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_RemoveField_m1226873592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = __this->get_keys_7();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m1277572545(L_0, L_1, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		if ((((int32_t)L_2) <= ((int32_t)(-1))))
		{
			goto IL_0036;
		}
	}
	{
		List_1_t1341003379 * L_3 = __this->get_list_6();
		List_1_t1398341365 * L_4 = __this->get_keys_7();
		String_t* L_5 = ___name0;
		NullCheck(L_4);
		int32_t L_6 = List_1_IndexOf_m1277572545(L_4, L_5, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		NullCheck(L_3);
		List_1_RemoveAt_m1879454498(L_3, L_6, /*hidden argument*/List_1_RemoveAt_m1879454498_MethodInfo_var);
		List_1_t1398341365 * L_7 = __this->get_keys_7();
		String_t* L_8 = ___name0;
		NullCheck(L_7);
		List_1_Remove_m875018944(L_7, L_8, /*hidden argument*/List_1_Remove_m875018944_MethodInfo_var);
	}

IL_0036:
	{
		return;
	}
}
// System.Boolean JSONObject::GetField(System.Boolean&,System.String,System.Boolean)
extern "C"  bool JSONObject_GetField_m1574006448 (JSONObject_t1971882247 * __this, bool* ___field0, String_t* ___name1, bool ___fallback2, const MethodInfo* method)
{
	{
		bool* L_0 = ___field0;
		bool L_1 = ___fallback2;
		*((int8_t*)(L_0)) = (int8_t)L_1;
		bool* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m3335073054(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Boolean&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m3335073054_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m3335073054 (JSONObject_t1971882247 * __this, bool* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m3335073054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m1277572545(L_1, L_2, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		bool* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_8);
		bool L_9 = L_8->get_b_12();
		*((int8_t*)(L_5)) = (int8_t)L_9;
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.Single&,System.String,System.Single)
extern "C"  bool JSONObject_GetField_m2841318950 (JSONObject_t1971882247 * __this, float* ___field0, String_t* ___name1, float ___fallback2, const MethodInfo* method)
{
	{
		float* L_0 = ___field0;
		float L_1 = ___fallback2;
		*((float*)(L_0)) = (float)L_1;
		float* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m109740502(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Single&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m109740502_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m109740502 (JSONObject_t1971882247 * __this, float* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m109740502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m1277572545(L_1, L_2, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		float* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_9();
		*((float*)(L_5)) = (float)L_9;
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.Int32&,System.String,System.Int32)
extern "C"  bool JSONObject_GetField_m2009009808 (JSONObject_t1971882247 * __this, int32_t* ___field0, String_t* ___name1, int32_t ___fallback2, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___field0;
		int32_t L_1 = ___fallback2;
		*((int32_t*)(L_0)) = (int32_t)L_1;
		int32_t* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m2835199374(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Int32&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m2835199374_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m2835199374 (JSONObject_t1971882247 * __this, int32_t* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2835199374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m1277572545(L_1, L_2, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_9();
		*((int32_t*)(L_5)) = (int32_t)(((int32_t)((int32_t)L_9)));
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.Int64&,System.String,System.Int64)
extern "C"  bool JSONObject_GetField_m2159922640 (JSONObject_t1971882247 * __this, int64_t* ___field0, String_t* ___name1, int64_t ___fallback2, const MethodInfo* method)
{
	{
		int64_t* L_0 = ___field0;
		int64_t L_1 = ___fallback2;
		*((int64_t*)(L_0)) = (int64_t)L_1;
		int64_t* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m1610446153(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.Int64&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m1610446153_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m1610446153 (JSONObject_t1971882247 * __this, int64_t* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m1610446153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m1277572545(L_1, L_2, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		int64_t* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_9();
		*((int64_t*)(L_5)) = (int64_t)(((int64_t)((int64_t)L_9)));
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.UInt32&,System.String,System.UInt32)
extern "C"  bool JSONObject_GetField_m525320576 (JSONObject_t1971882247 * __this, uint32_t* ___field0, String_t* ___name1, uint32_t ___fallback2, const MethodInfo* method)
{
	{
		uint32_t* L_0 = ___field0;
		uint32_t L_1 = ___fallback2;
		*((int32_t*)(L_0)) = (int32_t)L_1;
		uint32_t* L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m2130833495(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.UInt32&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m2130833495_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m2130833495 (JSONObject_t1971882247 * __this, uint32_t* ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2130833495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m1277572545(L_1, L_2, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		uint32_t* L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_n_9();
		*((int32_t*)(L_5)) = (int32_t)(((int32_t)((uint32_t)L_9)));
		return (bool)1;
	}

IL_0035:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean JSONObject::GetField(System.String&,System.String,System.String)
extern "C"  bool JSONObject_GetField_m757276436 (JSONObject_t1971882247 * __this, String_t** ___field0, String_t* ___name1, String_t* ___fallback2, const MethodInfo* method)
{
	{
		String_t** L_0 = ___field0;
		String_t* L_1 = ___fallback2;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)L_1;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)L_1);
		String_t** L_2 = ___field0;
		String_t* L_3 = ___name1;
		bool L_4 = JSONObject_GetField_m792552725(__this, L_2, L_3, (FieldNotFound_t865402053 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean JSONObject::GetField(System.String&,System.String,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m792552725_MetadataUsageId;
extern "C"  bool JSONObject_GetField_m792552725 (JSONObject_t1971882247 * __this, String_t** ___field0, String_t* ___name1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m792552725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		int32_t L_3 = List_1_IndexOf_m1277572545(L_1, L_2, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		String_t** L_5 = ___field0;
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = L_8->get_str_8();
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)L_9;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)L_9);
		return (bool)1;
	}

IL_0034:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name1;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Void JSONObject::GetField(System.String,JSONObject/GetFieldResponse,JSONObject/FieldNotFound)
extern const MethodInfo* List_1_IndexOf_m1277572545_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m881953590_MetadataUsageId;
extern "C"  void JSONObject_GetField_m881953590 (JSONObject_t1971882247 * __this, String_t* ___name0, GetFieldResponse_t1259369279 * ___response1, FieldNotFound_t865402053 * ___fail2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m881953590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GetFieldResponse_t1259369279 * L_0 = ___response1;
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		bool L_1 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		List_1_t1398341365 * L_2 = __this->get_keys_7();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		int32_t L_4 = List_1_IndexOf_m1277572545(L_2, L_3, /*hidden argument*/List_1_IndexOf_m1277572545_MethodInfo_var);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		GetFieldResponse_t1259369279 * L_6 = ___response1;
		List_1_t1341003379 * L_7 = __this->get_list_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		JSONObject_t1971882247 * L_9 = List_1_get_Item_m3990778905(L_7, L_8, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_6);
		GetFieldResponse_Invoke_m1070419673(L_6, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		FieldNotFound_t865402053 * L_10 = ___fail2;
		if (!L_10)
		{
			goto IL_0045;
		}
	}
	{
		FieldNotFound_t865402053 * L_11 = ___fail2;
		String_t* L_12 = ___name0;
		NullCheck(L_11);
		FieldNotFound_Invoke_m677615214(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// JSONObject JSONObject::GetField(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const uint32_t JSONObject_GetField_m2890738298_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_GetField_m2890738298 (JSONObject_t1971882247 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2890738298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003a;
	}

IL_0012:
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m1112119647(L_1, L_2, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		List_1_t1341003379 * L_6 = __this->get_list_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = List_1_get_Item_m3990778905(L_6, L_7, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		return L_8;
	}

IL_0036:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_10 = V_0;
		List_1_t1398341365 * L_11 = __this->get_keys_7();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m780127360(L_11, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}

IL_004b:
	{
		return (JSONObject_t1971882247 *)NULL;
	}
}
// System.Boolean JSONObject::HasFields(System.String[])
extern const MethodInfo* List_1_Contains_m3447151919_MethodInfo_var;
extern const uint32_t JSONObject_HasFields_m2566276779_MetadataUsageId;
extern "C"  bool JSONObject_HasFields_m2566276779 (JSONObject_t1971882247 * __this, StringU5BU5D_t1642385972* ___names0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_HasFields_m2566276779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0014:
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		StringU5BU5D_t1642385972* L_2 = ___names0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		bool L_6 = List_1_Contains_m3447151919(L_1, L_5, /*hidden argument*/List_1_Contains_m3447151919_MethodInfo_var);
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_0;
		StringU5BU5D_t1642385972* L_9 = ___names0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean JSONObject::HasField(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const uint32_t JSONObject_HasField_m4286787708_MetadataUsageId;
extern "C"  bool JSONObject_HasField_m4286787708 (JSONObject_t1971882247 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_HasField_m4286787708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = JSONObject_get_IsObject_m1393183916(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0014:
	{
		List_1_t1398341365 * L_1 = __this->get_keys_7();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m1112119647(L_1, L_2, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_7 = V_0;
		List_1_t1398341365 * L_8 = __this->get_keys_7();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m780127360(L_8, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void JSONObject::Clear()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m1668058715_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2045495756_MethodInfo_var;
extern const uint32_t JSONObject_Clear_m4074654805_MetadataUsageId;
extern "C"  void JSONObject_Clear_m4074654805 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Clear_m4074654805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_type_5(0);
		List_1_t1341003379 * L_0 = __this->get_list_6();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t1341003379 * L_1 = __this->get_list_6();
		NullCheck(L_1);
		List_1_Clear_m1668058715(L_1, /*hidden argument*/List_1_Clear_m1668058715_MethodInfo_var);
	}

IL_001d:
	{
		List_1_t1398341365 * L_2 = __this->get_keys_7();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		List_1_t1398341365 * L_3 = __this->get_keys_7();
		NullCheck(L_3);
		List_1_Clear_m2045495756(L_3, /*hidden argument*/List_1_Clear_m2045495756_MethodInfo_var);
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_8(L_4);
		__this->set_n_9((0.0f));
		__this->set_b_12((bool)0);
		return;
	}
}
// JSONObject JSONObject::Copy()
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Copy_m1092556847_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_Copy_m1092556847 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Copy_m1092556847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = JSONObject_Print_m439839387(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_1 = JSONObject_Create_m2628830417(NULL /*static, unused*/, L_0, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::Merge(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Merge_m1871178173_MetadataUsageId;
extern "C"  void JSONObject_Merge_m1871178173 (JSONObject_t1971882247 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Merge_m1871178173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m1967708379(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::MergeRecur(JSONObject,JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3451532966;
extern const uint32_t JSONObject_MergeRecur_m1967708379_MetadataUsageId;
extern "C"  void JSONObject_MergeRecur_m1967708379 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___left0, JSONObject_t1971882247 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_MergeRecur_m1967708379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		JSONObject_t1971882247 * L_0 = ___left0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_type_5();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = ___left0;
		JSONObject_t1971882247 * L_3 = ___right1;
		NullCheck(L_2);
		JSONObject_Absorb_m2114544266(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0176;
	}

IL_0017:
	{
		JSONObject_t1971882247 * L_4 = ___left0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_type_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_00d2;
		}
	}
	{
		JSONObject_t1971882247 * L_6 = ___right1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_type_5();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_00d2;
		}
	}
	{
		V_0 = 0;
		goto IL_00bc;
	}

IL_0036:
	{
		JSONObject_t1971882247 * L_8 = ___right1;
		NullCheck(L_8);
		List_1_t1398341365 * L_9 = L_8->get_keys_7();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m1112119647(L_9, L_10, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_1 = L_11;
		JSONObject_t1971882247 * L_12 = ___right1;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_14 = JSONObject_get_Item_m1007985851(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = JSONObject_get_isContainer_m2971179132(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008b;
		}
	}
	{
		JSONObject_t1971882247 * L_16 = ___left0;
		String_t* L_17 = V_1;
		NullCheck(L_16);
		bool L_18 = JSONObject_HasField_m4286787708(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0078;
		}
	}
	{
		JSONObject_t1971882247 * L_19 = ___left0;
		String_t* L_20 = V_1;
		NullCheck(L_19);
		JSONObject_t1971882247 * L_21 = JSONObject_get_Item_m223257276(L_19, L_20, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_22 = ___right1;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		JSONObject_t1971882247 * L_24 = JSONObject_get_Item_m1007985851(L_22, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m1967708379(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0078:
	{
		JSONObject_t1971882247 * L_25 = ___left0;
		String_t* L_26 = V_1;
		JSONObject_t1971882247 * L_27 = ___right1;
		int32_t L_28 = V_0;
		NullCheck(L_27);
		JSONObject_t1971882247 * L_29 = JSONObject_get_Item_m1007985851(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		JSONObject_AddField_m1302524246(L_25, L_26, L_29, /*hidden argument*/NULL);
	}

IL_0086:
	{
		goto IL_00b8;
	}

IL_008b:
	{
		JSONObject_t1971882247 * L_30 = ___left0;
		String_t* L_31 = V_1;
		NullCheck(L_30);
		bool L_32 = JSONObject_HasField_m4286787708(L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00aa;
		}
	}
	{
		JSONObject_t1971882247 * L_33 = ___left0;
		String_t* L_34 = V_1;
		JSONObject_t1971882247 * L_35 = ___right1;
		int32_t L_36 = V_0;
		NullCheck(L_35);
		JSONObject_t1971882247 * L_37 = JSONObject_get_Item_m1007985851(L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_33);
		JSONObject_SetField_m2709314095(L_33, L_34, L_37, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_00aa:
	{
		JSONObject_t1971882247 * L_38 = ___left0;
		String_t* L_39 = V_1;
		JSONObject_t1971882247 * L_40 = ___right1;
		int32_t L_41 = V_0;
		NullCheck(L_40);
		JSONObject_t1971882247 * L_42 = JSONObject_get_Item_m1007985851(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_38);
		JSONObject_AddField_m1302524246(L_38, L_39, L_42, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		int32_t L_43 = V_0;
		V_0 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00bc:
	{
		int32_t L_44 = V_0;
		JSONObject_t1971882247 * L_45 = ___right1;
		NullCheck(L_45);
		List_1_t1341003379 * L_46 = L_45->get_list_6();
		NullCheck(L_46);
		int32_t L_47 = List_1_get_Count_m1879545138(L_46, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_44) < ((int32_t)L_47)))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0176;
	}

IL_00d2:
	{
		JSONObject_t1971882247 * L_48 = ___left0;
		NullCheck(L_48);
		int32_t L_49 = L_48->get_type_5();
		if ((!(((uint32_t)L_49) == ((uint32_t)4))))
		{
			goto IL_0176;
		}
	}
	{
		JSONObject_t1971882247 * L_50 = ___right1;
		NullCheck(L_50);
		int32_t L_51 = L_50->get_type_5();
		if ((!(((uint32_t)L_51) == ((uint32_t)4))))
		{
			goto IL_0176;
		}
	}
	{
		JSONObject_t1971882247 * L_52 = ___right1;
		NullCheck(L_52);
		int32_t L_53 = JSONObject_get_Count_m3265098784(L_52, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_54 = ___left0;
		NullCheck(L_54);
		int32_t L_55 = JSONObject_get_Count_m3265098784(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_53) <= ((int32_t)L_55)))
		{
			goto IL_0106;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3451532966, /*hidden argument*/NULL);
		return;
	}

IL_0106:
	{
		V_2 = 0;
		goto IL_0165;
	}

IL_010d:
	{
		JSONObject_t1971882247 * L_56 = ___left0;
		int32_t L_57 = V_2;
		NullCheck(L_56);
		JSONObject_t1971882247 * L_58 = JSONObject_get_Item_m1007985851(L_56, L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		int32_t L_59 = L_58->get_type_5();
		JSONObject_t1971882247 * L_60 = ___right1;
		int32_t L_61 = V_2;
		NullCheck(L_60);
		JSONObject_t1971882247 * L_62 = JSONObject_get_Item_m1007985851(L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = L_62->get_type_5();
		if ((!(((uint32_t)L_59) == ((uint32_t)L_63))))
		{
			goto IL_0161;
		}
	}
	{
		JSONObject_t1971882247 * L_64 = ___left0;
		int32_t L_65 = V_2;
		NullCheck(L_64);
		JSONObject_t1971882247 * L_66 = JSONObject_get_Item_m1007985851(L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		bool L_67 = JSONObject_get_isContainer_m2971179132(L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0153;
		}
	}
	{
		JSONObject_t1971882247 * L_68 = ___left0;
		int32_t L_69 = V_2;
		NullCheck(L_68);
		JSONObject_t1971882247 * L_70 = JSONObject_get_Item_m1007985851(L_68, L_69, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_71 = ___right1;
		int32_t L_72 = V_2;
		NullCheck(L_71);
		JSONObject_t1971882247 * L_73 = JSONObject_get_Item_m1007985851(L_71, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m1967708379(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		goto IL_0161;
	}

IL_0153:
	{
		JSONObject_t1971882247 * L_74 = ___left0;
		int32_t L_75 = V_2;
		JSONObject_t1971882247 * L_76 = ___right1;
		int32_t L_77 = V_2;
		NullCheck(L_76);
		JSONObject_t1971882247 * L_78 = JSONObject_get_Item_m1007985851(L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_74);
		JSONObject_set_Item_m1069161616(L_74, L_75, L_78, /*hidden argument*/NULL);
	}

IL_0161:
	{
		int32_t L_79 = V_2;
		V_2 = ((int32_t)((int32_t)L_79+(int32_t)1));
	}

IL_0165:
	{
		int32_t L_80 = V_2;
		JSONObject_t1971882247 * L_81 = ___right1;
		NullCheck(L_81);
		List_1_t1341003379 * L_82 = L_81->get_list_6();
		NullCheck(L_82);
		int32_t L_83 = List_1_get_Count_m1879545138(L_82, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_80) < ((int32_t)L_83)))
		{
			goto IL_010d;
		}
	}

IL_0176:
	{
		return;
	}
}
// System.Void JSONObject::Bake()
extern "C"  void JSONObject_Bake_m3429471729 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_5();
		if ((((int32_t)L_0) == ((int32_t)6)))
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_1 = JSONObject_Print_m439839387(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_str_8(L_1);
		__this->set_type_5(6);
	}

IL_0020:
	{
		return;
	}
}
// System.Collections.IEnumerable JSONObject::BakeAsync()
extern Il2CppClass* U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_BakeAsync_m1054124159_MetadataUsageId;
extern "C"  Il2CppObject * JSONObject_BakeAsync_m1054124159 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_BakeAsync_m1054124159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CBakeAsyncU3Ec__Iterator0_t1149809410 * V_0 = NULL;
	{
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_0 = (U3CBakeAsyncU3Ec__Iterator0_t1149809410 *)il2cpp_codegen_object_new(U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var);
		U3CBakeAsyncU3Ec__Iterator0__ctor_m1827367839(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_2 = V_0;
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_5(((int32_t)-2));
		return L_3;
	}
}
// System.String JSONObject::Print(System.Boolean)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Print_m439839387_MetadataUsageId;
extern "C"  String_t* JSONObject_Print_m439839387 (JSONObject_t1971882247 * __this, bool ___pretty0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Print_m439839387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		bool L_2 = ___pretty0;
		JSONObject_Stringify_m394345745(__this, 0, L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<System.String> JSONObject::PrintAsync(System.Boolean)
extern Il2CppClass* U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_PrintAsync_m2187380794_MetadataUsageId;
extern "C"  Il2CppObject* JSONObject_PrintAsync_m2187380794 (JSONObject_t1971882247 * __this, bool ___pretty0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_PrintAsync_m2187380794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPrintAsyncU3Ec__Iterator1_t716304657 * V_0 = NULL;
	{
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_0 = (U3CPrintAsyncU3Ec__Iterator1_t716304657 *)il2cpp_codegen_object_new(U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var);
		U3CPrintAsyncU3Ec__Iterator1__ctor_m2008311324(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_1 = V_0;
		bool L_2 = ___pretty0;
		NullCheck(L_1);
		L_1->set_pretty_1(L_2);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_5(__this);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_4 = V_0;
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_5 = L_4;
		NullCheck(L_5);
		L_5->set_U24PC_8(((int32_t)-2));
		return L_5;
	}
}
// System.Collections.IEnumerable JSONObject::StringifyAsync(System.Int32,System.Text.StringBuilder,System.Boolean)
extern Il2CppClass* U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_StringifyAsync_m2756392147_MetadataUsageId;
extern "C"  Il2CppObject * JSONObject_StringifyAsync_m2756392147 (JSONObject_t1971882247 * __this, int32_t ___depth0, StringBuilder_t1221177846 * ___builder1, bool ___pretty2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_StringifyAsync_m2756392147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * V_0 = NULL;
	{
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_0 = (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 *)il2cpp_codegen_object_new(U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var);
		U3CStringifyAsyncU3Ec__Iterator2__ctor_m992930667(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_1 = V_0;
		int32_t L_2 = ___depth0;
		NullCheck(L_1);
		L_1->set_depth_0(L_2);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_3 = V_0;
		StringBuilder_t1221177846 * L_4 = ___builder1;
		NullCheck(L_3);
		L_3->set_builder_2(L_4);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_5 = V_0;
		bool L_6 = ___pretty2;
		NullCheck(L_5);
		L_5->set_pretty_3(L_6);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_7 = V_0;
		int32_t L_8 = ___depth0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Edepth_17(L_8);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U24this_14(__this);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_10 = V_0;
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_11 = L_10;
		NullCheck(L_11);
		L_11->set_U24PC_18(((int32_t)-2));
		return L_11;
	}
}
// System.Void JSONObject::Stringify(System.Int32,System.Text.StringBuilder,System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2041690406;
extern Il2CppCodeGenString* _stringLiteral1554931876;
extern Il2CppCodeGenString* _stringLiteral1751346954;
extern Il2CppCodeGenString* _stringLiteral376316188;
extern Il2CppCodeGenString* _stringLiteral452738781;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern Il2CppCodeGenString* _stringLiteral845265362;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t JSONObject_Stringify_m394345745_MetadataUsageId;
extern "C"  void JSONObject_Stringify_m394345745 (JSONObject_t1971882247 * __this, int32_t ___depth0, StringBuilder_t1221177846 * ___builder1, bool ___pretty2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Stringify_m394345745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	JSONObject_t1971882247 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		int32_t L_0 = ___depth0;
		int32_t L_1 = L_0;
		___depth0 = ((int32_t)((int32_t)L_1+(int32_t)1));
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)100))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2041690406, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_5();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_03cf;
		}
		if (L_3 == 1)
		{
			goto IL_0058;
		}
		if (L_3 == 2)
		{
			goto IL_006f;
		}
		if (L_3 == 3)
		{
			goto IL_0117;
		}
		if (L_3 == 4)
		{
			goto IL_0262;
		}
		if (L_3 == 5)
		{
			goto IL_03a2;
		}
		if (L_3 == 6)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_03e0;
	}

IL_0046:
	{
		StringBuilder_t1221177846 * L_4 = ___builder1;
		String_t* L_5 = __this->get_str_8();
		NullCheck(L_4);
		StringBuilder_Append_m3636508479(L_4, L_5, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_0058:
	{
		StringBuilder_t1221177846 * L_6 = ___builder1;
		String_t* L_7 = __this->get_str_8();
		NullCheck(L_6);
		StringBuilder_AppendFormat_m3265503696(L_6, _stringLiteral1554931876, L_7, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_006f:
	{
		bool L_8 = __this->get_useInt_10();
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		StringBuilder_t1221177846 * L_9 = ___builder1;
		int64_t* L_10 = __this->get_address_of_i_11();
		String_t* L_11 = Int64_ToString_m689375889(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		StringBuilder_Append_m3636508479(L_9, L_11, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_0097:
	{
		float L_12 = __this->get_n_9();
		bool L_13 = Single_IsInfinity_m3331110346(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00b8;
		}
	}
	{
		StringBuilder_t1221177846 * L_14 = ___builder1;
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, _stringLiteral1751346954, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00b8:
	{
		float L_15 = __this->get_n_9();
		bool L_16 = Single_IsNegativeInfinity_m2615809279(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00d9;
		}
	}
	{
		StringBuilder_t1221177846 * L_17 = ___builder1;
		NullCheck(L_17);
		StringBuilder_Append_m3636508479(L_17, _stringLiteral376316188, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00d9:
	{
		float L_18 = __this->get_n_9();
		bool L_19 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00fa;
		}
	}
	{
		StringBuilder_t1221177846 * L_20 = ___builder1;
		NullCheck(L_20);
		StringBuilder_Append_m3636508479(L_20, _stringLiteral452738781, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00fa:
	{
		StringBuilder_t1221177846 * L_21 = ___builder1;
		float* L_22 = __this->get_address_of_n_9();
		String_t* L_23 = Single_ToString_m1813392066(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m3636508479(L_21, L_23, /*hidden argument*/NULL);
	}

IL_0112:
	{
		goto IL_03e0;
	}

IL_0117:
	{
		StringBuilder_t1221177846 * L_24 = ___builder1;
		NullCheck(L_24);
		StringBuilder_Append_m3636508479(L_24, _stringLiteral372029399, /*hidden argument*/NULL);
		List_1_t1341003379 * L_25 = __this->get_list_6();
		NullCheck(L_25);
		int32_t L_26 = List_1_get_Count_m1879545138(L_25, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_020a;
		}
	}
	{
		bool L_27 = ___pretty2;
		if (!L_27)
		{
			goto IL_0146;
		}
	}
	{
		StringBuilder_t1221177846 * L_28 = ___builder1;
		NullCheck(L_28);
		StringBuilder_Append_m3636508479(L_28, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_0146:
	{
		V_1 = 0;
		goto IL_01d2;
	}

IL_014d:
	{
		List_1_t1398341365 * L_29 = __this->get_keys_7();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		String_t* L_31 = List_1_get_Item_m1112119647(L_29, L_30, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_2 = L_31;
		List_1_t1341003379 * L_32 = __this->get_list_6();
		int32_t L_33 = V_1;
		NullCheck(L_32);
		JSONObject_t1971882247 * L_34 = List_1_get_Item_m3990778905(L_32, L_33, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		V_3 = L_34;
		JSONObject_t1971882247 * L_35 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_36 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01ce;
		}
	}
	{
		bool L_37 = ___pretty2;
		if (!L_37)
		{
			goto IL_019a;
		}
	}
	{
		V_4 = 0;
		goto IL_0192;
	}

IL_0180:
	{
		StringBuilder_t1221177846 * L_38 = ___builder1;
		NullCheck(L_38);
		StringBuilder_Append_m3636508479(L_38, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_39 = V_4;
		V_4 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0192:
	{
		int32_t L_40 = V_4;
		int32_t L_41 = ___depth0;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0180;
		}
	}

IL_019a:
	{
		StringBuilder_t1221177846 * L_42 = ___builder1;
		String_t* L_43 = V_2;
		NullCheck(L_42);
		StringBuilder_AppendFormat_m3265503696(L_42, _stringLiteral845265362, L_43, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_44 = V_3;
		int32_t L_45 = ___depth0;
		StringBuilder_t1221177846 * L_46 = ___builder1;
		bool L_47 = ___pretty2;
		NullCheck(L_44);
		JSONObject_Stringify_m394345745(L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_48 = ___builder1;
		NullCheck(L_48);
		StringBuilder_Append_m3636508479(L_48, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_49 = ___pretty2;
		if (!L_49)
		{
			goto IL_01ce;
		}
	}
	{
		StringBuilder_t1221177846 * L_50 = ___builder1;
		NullCheck(L_50);
		StringBuilder_Append_m3636508479(L_50, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		int32_t L_51 = V_1;
		V_1 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_01d2:
	{
		int32_t L_52 = V_1;
		List_1_t1341003379 * L_53 = __this->get_list_6();
		NullCheck(L_53);
		int32_t L_54 = List_1_get_Count_m1879545138(L_53, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_014d;
		}
	}
	{
		bool L_55 = ___pretty2;
		if (!L_55)
		{
			goto IL_01fc;
		}
	}
	{
		StringBuilder_t1221177846 * L_56 = ___builder1;
		StringBuilder_t1221177846 * L_57 = L_56;
		NullCheck(L_57);
		int32_t L_58 = StringBuilder_get_Length_m1608241323(L_57, /*hidden argument*/NULL);
		NullCheck(L_57);
		StringBuilder_set_Length_m3039225444(L_57, ((int32_t)((int32_t)L_58-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_01fc:
	{
		StringBuilder_t1221177846 * L_59 = ___builder1;
		StringBuilder_t1221177846 * L_60 = L_59;
		NullCheck(L_60);
		int32_t L_61 = StringBuilder_get_Length_m1608241323(L_60, /*hidden argument*/NULL);
		NullCheck(L_60);
		StringBuilder_set_Length_m3039225444(L_60, ((int32_t)((int32_t)L_61-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_020a:
	{
		bool L_62 = ___pretty2;
		if (!L_62)
		{
			goto IL_0251;
		}
	}
	{
		List_1_t1341003379 * L_63 = __this->get_list_6();
		NullCheck(L_63);
		int32_t L_64 = List_1_get_Count_m1879545138(L_63, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_64) <= ((int32_t)0)))
		{
			goto IL_0251;
		}
	}
	{
		StringBuilder_t1221177846 * L_65 = ___builder1;
		NullCheck(L_65);
		StringBuilder_Append_m3636508479(L_65, _stringLiteral372029352, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0247;
	}

IL_0235:
	{
		StringBuilder_t1221177846 * L_66 = ___builder1;
		NullCheck(L_66);
		StringBuilder_Append_m3636508479(L_66, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_67 = V_5;
		V_5 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0247:
	{
		int32_t L_68 = V_5;
		int32_t L_69 = ___depth0;
		if ((((int32_t)L_68) < ((int32_t)((int32_t)((int32_t)L_69-(int32_t)1)))))
		{
			goto IL_0235;
		}
	}

IL_0251:
	{
		StringBuilder_t1221177846 * L_70 = ___builder1;
		NullCheck(L_70);
		StringBuilder_Append_m3636508479(L_70, _stringLiteral372029393, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_0262:
	{
		StringBuilder_t1221177846 * L_71 = ___builder1;
		NullCheck(L_71);
		StringBuilder_Append_m3636508479(L_71, _stringLiteral372029431, /*hidden argument*/NULL);
		List_1_t1341003379 * L_72 = __this->get_list_6();
		NullCheck(L_72);
		int32_t L_73 = List_1_get_Count_m1879545138(L_72, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_73) <= ((int32_t)0)))
		{
			goto IL_034a;
		}
	}
	{
		bool L_74 = ___pretty2;
		if (!L_74)
		{
			goto IL_0291;
		}
	}
	{
		StringBuilder_t1221177846 * L_75 = ___builder1;
		NullCheck(L_75);
		StringBuilder_Append_m3636508479(L_75, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_0291:
	{
		V_6 = 0;
		goto IL_0311;
	}

IL_0299:
	{
		List_1_t1341003379 * L_76 = __this->get_list_6();
		int32_t L_77 = V_6;
		NullCheck(L_76);
		JSONObject_t1971882247 * L_78 = List_1_get_Item_m3990778905(L_76, L_77, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_79 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_030b;
		}
	}
	{
		bool L_80 = ___pretty2;
		if (!L_80)
		{
			goto IL_02d8;
		}
	}
	{
		V_7 = 0;
		goto IL_02d0;
	}

IL_02be:
	{
		StringBuilder_t1221177846 * L_81 = ___builder1;
		NullCheck(L_81);
		StringBuilder_Append_m3636508479(L_81, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_82 = V_7;
		V_7 = ((int32_t)((int32_t)L_82+(int32_t)1));
	}

IL_02d0:
	{
		int32_t L_83 = V_7;
		int32_t L_84 = ___depth0;
		if ((((int32_t)L_83) < ((int32_t)L_84)))
		{
			goto IL_02be;
		}
	}

IL_02d8:
	{
		List_1_t1341003379 * L_85 = __this->get_list_6();
		int32_t L_86 = V_6;
		NullCheck(L_85);
		JSONObject_t1971882247 * L_87 = List_1_get_Item_m3990778905(L_85, L_86, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		int32_t L_88 = ___depth0;
		StringBuilder_t1221177846 * L_89 = ___builder1;
		bool L_90 = ___pretty2;
		NullCheck(L_87);
		JSONObject_Stringify_m394345745(L_87, L_88, L_89, L_90, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_91 = ___builder1;
		NullCheck(L_91);
		StringBuilder_Append_m3636508479(L_91, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_92 = ___pretty2;
		if (!L_92)
		{
			goto IL_030b;
		}
	}
	{
		StringBuilder_t1221177846 * L_93 = ___builder1;
		NullCheck(L_93);
		StringBuilder_Append_m3636508479(L_93, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_030b:
	{
		int32_t L_94 = V_6;
		V_6 = ((int32_t)((int32_t)L_94+(int32_t)1));
	}

IL_0311:
	{
		int32_t L_95 = V_6;
		List_1_t1341003379 * L_96 = __this->get_list_6();
		NullCheck(L_96);
		int32_t L_97 = List_1_get_Count_m1879545138(L_96, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_95) < ((int32_t)L_97)))
		{
			goto IL_0299;
		}
	}
	{
		bool L_98 = ___pretty2;
		if (!L_98)
		{
			goto IL_033c;
		}
	}
	{
		StringBuilder_t1221177846 * L_99 = ___builder1;
		StringBuilder_t1221177846 * L_100 = L_99;
		NullCheck(L_100);
		int32_t L_101 = StringBuilder_get_Length_m1608241323(L_100, /*hidden argument*/NULL);
		NullCheck(L_100);
		StringBuilder_set_Length_m3039225444(L_100, ((int32_t)((int32_t)L_101-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_034a;
	}

IL_033c:
	{
		StringBuilder_t1221177846 * L_102 = ___builder1;
		StringBuilder_t1221177846 * L_103 = L_102;
		NullCheck(L_103);
		int32_t L_104 = StringBuilder_get_Length_m1608241323(L_103, /*hidden argument*/NULL);
		NullCheck(L_103);
		StringBuilder_set_Length_m3039225444(L_103, ((int32_t)((int32_t)L_104-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_034a:
	{
		bool L_105 = ___pretty2;
		if (!L_105)
		{
			goto IL_0391;
		}
	}
	{
		List_1_t1341003379 * L_106 = __this->get_list_6();
		NullCheck(L_106);
		int32_t L_107 = List_1_get_Count_m1879545138(L_106, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_107) <= ((int32_t)0)))
		{
			goto IL_0391;
		}
	}
	{
		StringBuilder_t1221177846 * L_108 = ___builder1;
		NullCheck(L_108);
		StringBuilder_Append_m3636508479(L_108, _stringLiteral372029352, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_0387;
	}

IL_0375:
	{
		StringBuilder_t1221177846 * L_109 = ___builder1;
		NullCheck(L_109);
		StringBuilder_Append_m3636508479(L_109, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_110 = V_8;
		V_8 = ((int32_t)((int32_t)L_110+(int32_t)1));
	}

IL_0387:
	{
		int32_t L_111 = V_8;
		int32_t L_112 = ___depth0;
		if ((((int32_t)L_111) < ((int32_t)((int32_t)((int32_t)L_112-(int32_t)1)))))
		{
			goto IL_0375;
		}
	}

IL_0391:
	{
		StringBuilder_t1221177846 * L_113 = ___builder1;
		NullCheck(L_113);
		StringBuilder_Append_m3636508479(L_113, _stringLiteral372029425, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_03a2:
	{
		bool L_114 = __this->get_b_12();
		if (!L_114)
		{
			goto IL_03be;
		}
	}
	{
		StringBuilder_t1221177846 * L_115 = ___builder1;
		NullCheck(L_115);
		StringBuilder_Append_m3636508479(L_115, _stringLiteral3323263070, /*hidden argument*/NULL);
		goto IL_03ca;
	}

IL_03be:
	{
		StringBuilder_t1221177846 * L_116 = ___builder1;
		NullCheck(L_116);
		StringBuilder_Append_m3636508479(L_116, _stringLiteral2609877245, /*hidden argument*/NULL);
	}

IL_03ca:
	{
		goto IL_03e0;
	}

IL_03cf:
	{
		StringBuilder_t1221177846 * L_117 = ___builder1;
		NullCheck(L_117);
		StringBuilder_Append_m3636508479(L_117, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_03e0;
	}

IL_03e0:
	{
		return;
	}
}
// UnityEngine.WWWForm JSONObject::op_Implicit(JSONObject)
extern Il2CppClass* WWWForm_t3950226929_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern const uint32_t JSONObject_op_Implicit_m3631423691_MetadataUsageId;
extern "C"  WWWForm_t3950226929 * JSONObject_op_Implicit_m3631423691 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_op_Implicit_m3631423691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WWWForm_t3950226929 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		WWWForm_t3950226929 * L_0 = (WWWForm_t3950226929 *)il2cpp_codegen_object_new(WWWForm_t3950226929_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2129424870(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_007d;
	}

IL_000d:
	{
		int32_t L_1 = V_1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_type_5();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0037;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = ___obj0;
		NullCheck(L_8);
		List_1_t1398341365 * L_9 = L_8->get_keys_7();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m1112119647(L_9, L_10, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_2 = L_11;
	}

IL_0037:
	{
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		List_1_t1341003379 * L_13 = L_12->get_list_6();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1971882247 * L_15 = List_1_get_Item_m3990778905(L_13, L_14, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		JSONObject_t1971882247 * L_17 = ___obj0;
		NullCheck(L_17);
		List_1_t1341003379 * L_18 = L_17->get_list_6();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_20 = List_1_get_Item_m3990778905(L_18, L_19, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_type_5();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_22);
		String_t* L_24 = String_Replace_m1941156251(L_22, _stringLiteral372029312, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
	}

IL_0071:
	{
		WWWForm_t3950226929 * L_25 = V_0;
		String_t* L_26 = V_2;
		String_t* L_27 = V_3;
		NullCheck(L_25);
		WWWForm_AddField_m1334606983(L_25, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_29 = V_1;
		JSONObject_t1971882247 * L_30 = ___obj0;
		NullCheck(L_30);
		List_1_t1341003379 * L_31 = L_30->get_list_6();
		NullCheck(L_31);
		int32_t L_32 = List_1_get_Count_m1879545138(L_31, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_29) < ((int32_t)L_32)))
		{
			goto IL_000d;
		}
	}
	{
		WWWForm_t3950226929 * L_33 = V_0;
		return L_33;
	}
}
// JSONObject JSONObject::get_Item(System.Int32)
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const uint32_t JSONObject_get_Item_m1007985851_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObject_get_Item_m1007985851 (JSONObject_t1971882247 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_Item_m1007985851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_6();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1879545138(L_0, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		int32_t L_2 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1341003379 * L_3 = __this->get_list_6();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_5 = List_1_get_Item_m3990778905(L_3, L_4, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		return L_5;
	}

IL_001e:
	{
		return (JSONObject_t1971882247 *)NULL;
	}
}
// System.Void JSONObject::set_Item(System.Int32,JSONObject)
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m2969780842_MethodInfo_var;
extern const uint32_t JSONObject_set_Item_m1069161616_MetadataUsageId;
extern "C"  void JSONObject_set_Item_m1069161616 (JSONObject_t1971882247 * __this, int32_t ___index0, JSONObject_t1971882247 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_set_Item_m1069161616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1341003379 * L_0 = __this->get_list_6();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1879545138(L_0, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		int32_t L_2 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1341003379 * L_3 = __this->get_list_6();
		int32_t L_4 = ___index0;
		JSONObject_t1971882247 * L_5 = ___value1;
		NullCheck(L_3);
		List_1_set_Item_m2969780842(L_3, L_4, L_5, /*hidden argument*/List_1_set_Item_m2969780842_MethodInfo_var);
	}

IL_001e:
	{
		return;
	}
}
// JSONObject JSONObject::get_Item(System.String)
extern "C"  JSONObject_t1971882247 * JSONObject_get_Item_m223257276 (JSONObject_t1971882247 * __this, String_t* ___index0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___index0;
		JSONObject_t1971882247 * L_1 = JSONObject_GetField_m2890738298(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::set_Item(System.String,JSONObject)
extern "C"  void JSONObject_set_Item_m3241589931 (JSONObject_t1971882247 * __this, String_t* ___index0, JSONObject_t1971882247 * ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___index0;
		JSONObject_t1971882247 * L_1 = ___value1;
		JSONObject_SetField_m2709314095(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JSONObject::ToString()
extern "C"  String_t* JSONObject_ToString_m1473866835 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = JSONObject_Print_m439839387(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String JSONObject::ToString(System.Boolean)
extern "C"  String_t* JSONObject_ToString_m1025907180 (JSONObject_t1971882247 * __this, bool ___pretty0, const MethodInfo* method)
{
	{
		bool L_0 = ___pretty0;
		String_t* L_1 = JSONObject_Print_m439839387(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> JSONObject::ToDictionary()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2768501952_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2139965640_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral942984848;
extern Il2CppCodeGenString* _stringLiteral688080911;
extern Il2CppCodeGenString* _stringLiteral3109692691;
extern const uint32_t JSONObject_ToDictionary_m2142313491_MetadataUsageId;
extern "C"  Dictionary_2_t3943999495 * JSONObject_ToDictionary_m2142313491 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ToDictionary_m2142313491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	int32_t V_1 = 0;
	JSONObject_t1971882247 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_type_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_00ff;
		}
	}
	{
		Dictionary_2_t3943999495 * L_1 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2768501952(L_1, /*hidden argument*/Dictionary_2__ctor_m2768501952_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_00ec;
	}

IL_0019:
	{
		List_1_t1341003379 * L_2 = __this->get_list_6();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		JSONObject_t1971882247 * L_4 = List_1_get_Item_m3990778905(L_2, L_3, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		V_2 = L_4;
		JSONObject_t1971882247 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_type_5();
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
		{
			goto IL_004e;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 1)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 2)
		{
			goto IL_00c3;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 3)
		{
			goto IL_00c3;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 4)
		{
			goto IL_0097;
		}
	}
	{
		goto IL_00c3;
	}

IL_004e:
	{
		Dictionary_2_t3943999495 * L_8 = V_0;
		List_1_t1398341365 * L_9 = __this->get_keys_7();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m1112119647(L_9, L_10, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		JSONObject_t1971882247 * L_12 = V_2;
		NullCheck(L_12);
		String_t* L_13 = L_12->get_str_8();
		NullCheck(L_8);
		Dictionary_2_Add_m2139965640(L_8, L_11, L_13, /*hidden argument*/Dictionary_2_Add_m2139965640_MethodInfo_var);
		goto IL_00e8;
	}

IL_006b:
	{
		Dictionary_2_t3943999495 * L_14 = V_0;
		List_1_t1398341365 * L_15 = __this->get_keys_7();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		String_t* L_17 = List_1_get_Item_m1112119647(L_15, L_16, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		JSONObject_t1971882247 * L_18 = V_2;
		NullCheck(L_18);
		float L_19 = L_18->get_n_9();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_14);
		Dictionary_2_Add_m2139965640(L_14, L_17, L_23, /*hidden argument*/Dictionary_2_Add_m2139965640_MethodInfo_var);
		goto IL_00e8;
	}

IL_0097:
	{
		Dictionary_2_t3943999495 * L_24 = V_0;
		List_1_t1398341365 * L_25 = __this->get_keys_7();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		String_t* L_27 = List_1_get_Item_m1112119647(L_25, L_26, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		JSONObject_t1971882247 * L_28 = V_2;
		NullCheck(L_28);
		bool L_29 = L_28->get_b_12();
		bool L_30 = L_29;
		Il2CppObject * L_31 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_33 = String_Concat_m56707527(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_24);
		Dictionary_2_Add_m2139965640(L_24, L_27, L_33, /*hidden argument*/Dictionary_2_Add_m2139965640_MethodInfo_var);
		goto IL_00e8;
	}

IL_00c3:
	{
		List_1_t1398341365 * L_34 = __this->get_keys_7();
		int32_t L_35 = V_1;
		NullCheck(L_34);
		String_t* L_36 = List_1_get_Item_m1112119647(L_34, L_35, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral942984848, L_36, _stringLiteral688080911, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_00e8:
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00ec:
	{
		int32_t L_39 = V_1;
		List_1_t1341003379 * L_40 = __this->get_list_6();
		NullCheck(L_40);
		int32_t L_41 = List_1_get_Count_m1879545138(L_40, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_39) < ((int32_t)L_41)))
		{
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t3943999495 * L_42 = V_0;
		return L_42;
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3109692691, /*hidden argument*/NULL);
		return (Dictionary_2_t3943999495 *)NULL;
	}
}
// System.Boolean JSONObject::op_Implicit(JSONObject)
extern "C"  bool JSONObject_op_Implicit_m2930985192 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___o0, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = ___o0;
		return (bool)((((int32_t)((((Il2CppObject*)(JSONObject_t1971882247 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.IEnumerator JSONObject::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JSONObject_System_Collections_IEnumerable_GetEnumerator_m772759225 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	{
		JSONObjectEnumer_t427597183 * L_0 = JSONObject_GetEnumerator_m3479551862(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObjectEnumer JSONObject::GetEnumerator()
extern Il2CppClass* JSONObjectEnumer_t427597183_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_GetEnumerator_m3479551862_MetadataUsageId;
extern "C"  JSONObjectEnumer_t427597183 * JSONObject_GetEnumerator_m3479551862 (JSONObject_t1971882247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetEnumerator_m3479551862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObjectEnumer_t427597183 * L_0 = (JSONObjectEnumer_t427597183 *)il2cpp_codegen_object_new(JSONObjectEnumer_t427597183_il2cpp_TypeInfo_var);
		JSONObjectEnumer__ctor_m34994965(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void JSONObject::.cctor()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Stopwatch_t1380178105_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0_FieldInfo_var;
extern const uint32_t JSONObject__cctor_m1128223947_MetadataUsageId;
extern "C"  void JSONObject__cctor_m1128223947 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__cctor_m1128223947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0_FieldInfo_var), /*hidden argument*/NULL);
		((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->set_WHITESPACE_4(L_0);
		Stopwatch_t1380178105 * L_1 = (Stopwatch_t1380178105 *)il2cpp_codegen_object_new(Stopwatch_t1380178105_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m589309528(L_1, /*hidden argument*/NULL);
		((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->set_printWatch_14(L_1);
		return;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator0::.ctor()
extern "C"  void U3CBakeAsyncU3Ec__Iterator0__ctor_m1827367839 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject/<BakeAsync>c__Iterator0::MoveNext()
extern Il2CppClass* IEnumerable_1_t2321347278_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3799711356_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_MoveNext_m1623163241_MetadataUsageId;
extern "C"  bool U3CBakeAsyncU3Ec__Iterator0_MoveNext_m1623163241 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_MoveNext_m1623163241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_00f6;
	}

IL_0023:
	{
		JSONObject_t1971882247 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_type_5();
		if ((((int32_t)L_3) == ((int32_t)6)))
		{
			goto IL_00ef;
		}
	}
	{
		JSONObject_t1971882247 * L_4 = __this->get_U24this_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = JSONObject_PrintAsync_m2187380794(L_4, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t2321347278_il2cpp_TypeInfo_var, L_5);
		__this->set_U24locvar0_0(L_6);
		V_0 = ((int32_t)-3);
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_7 = V_0;
			if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
			{
				goto IL_009d;
			}
		}

IL_005a:
		{
			goto IL_00b3;
		}

IL_005f:
		{
			Il2CppObject* L_8 = __this->get_U24locvar0_0();
			NullCheck(L_8);
			String_t* L_9 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t3799711356_il2cpp_TypeInfo_var, L_8);
			__this->set_U3CsU3E__0_1(L_9);
			String_t* L_10 = __this->get_U3CsU3E__0_1();
			if (L_10)
			{
				goto IL_00a2;
			}
		}

IL_007b:
		{
			String_t* L_11 = __this->get_U3CsU3E__0_1();
			__this->set_U24current_3(L_11);
			bool L_12 = __this->get_U24disposing_4();
			if (L_12)
			{
				goto IL_0096;
			}
		}

IL_008f:
		{
			__this->set_U24PC_5(1);
		}

IL_0096:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xF8, FINALLY_00c8);
		}

IL_009d:
		{
			goto IL_00b3;
		}

IL_00a2:
		{
			JSONObject_t1971882247 * L_13 = __this->get_U24this_2();
			String_t* L_14 = __this->get_U3CsU3E__0_1();
			NullCheck(L_13);
			L_13->set_str_8(L_14);
		}

IL_00b3:
		{
			Il2CppObject* L_15 = __this->get_U24locvar0_0();
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_005f;
			}
		}

IL_00c3:
		{
			IL2CPP_LEAVE(0xE3, FINALLY_00c8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c8;
	}

FINALLY_00c8:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00cc;
			}
		}

IL_00cb:
		{
			IL2CPP_END_FINALLY(200)
		}

IL_00cc:
		{
			Il2CppObject* L_18 = __this->get_U24locvar0_0();
			if (!L_18)
			{
				goto IL_00e2;
			}
		}

IL_00d7:
		{
			Il2CppObject* L_19 = __this->get_U24locvar0_0();
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_00e2:
		{
			IL2CPP_END_FINALLY(200)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(200)
	{
		IL2CPP_JUMP_TBL(0xF8, IL_00f8)
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e3:
	{
		JSONObject_t1971882247 * L_20 = __this->get_U24this_2();
		NullCheck(L_20);
		L_20->set_type_5(6);
	}

IL_00ef:
	{
		__this->set_U24PC_5((-1));
	}

IL_00f6:
	{
		return (bool)0;
	}

IL_00f8:
	{
		return (bool)1;
	}
}
// System.Object JSONObject/<BakeAsync>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1365685033 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object JSONObject/<BakeAsync>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2945594673 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator0::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_Dispose_m1326644018_MetadataUsageId;
extern "C"  void U3CBakeAsyncU3Ec__Iterator0_Dispose_m1326644018 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_Dispose_m1326644018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0044;
		}
		if (L_1 == 1)
		{
			goto IL_0028;
		}
	}
	{
		goto IL_0044;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x44, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U24locvar0_0();
			if (!L_2)
			{
				goto IL_0043;
			}
		}

IL_0038:
		{
			Il2CppObject* L_3 = __this->get_U24locvar0_0();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_3);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(45)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_Reset_m2621420648_MetadataUsageId;
extern "C"  void U3CBakeAsyncU3Ec__Iterator0_Reset_m2621420648 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_Reset_m2621420648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1468939038 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3979890094(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3979890094_MetadataUsageId;
extern "C"  Il2CppObject* U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3979890094 (U3CBakeAsyncU3Ec__Iterator0_t1149809410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3979890094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CBakeAsyncU3Ec__Iterator0_t1149809410 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_2 = (U3CBakeAsyncU3Ec__Iterator0_t1149809410 *)il2cpp_codegen_object_new(U3CBakeAsyncU3Ec__Iterator0_t1149809410_il2cpp_TypeInfo_var);
		U3CBakeAsyncU3Ec__Iterator0__ctor_m1827367839(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_3 = V_0;
		JSONObject_t1971882247 * L_4 = __this->get_U24this_2();
		NullCheck(L_3);
		L_3->set_U24this_2(L_4);
		U3CBakeAsyncU3Ec__Iterator0_t1149809410 * L_5 = V_0;
		return L_5;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator1::.ctor()
extern "C"  void U3CPrintAsyncU3Ec__Iterator1__ctor_m2008311324 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject/<PrintAsync>c__Iterator1::MoveNext()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_MoveNext_m2267275540_MetadataUsageId;
extern "C"  bool U3CPrintAsyncU3Ec__Iterator1_MoveNext_m2267275540 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_MoveNext_m2267275540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_006c;
		}
		if (L_1 == 2)
		{
			goto IL_0113;
		}
	}
	{
		goto IL_011a;
	}

IL_0027:
	{
		StringBuilder_t1221177846 * L_2 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_2, /*hidden argument*/NULL);
		__this->set_U3CbuilderU3E__0_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_3 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_14();
		NullCheck(L_3);
		Stopwatch_Reset_m3196507227(L_3, /*hidden argument*/NULL);
		Stopwatch_t1380178105 * L_4 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_14();
		NullCheck(L_4);
		Stopwatch_Start_m2051791460(L_4, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_5 = __this->get_U24this_5();
		StringBuilder_t1221177846 * L_6 = __this->get_U3CbuilderU3E__0_0();
		bool L_7 = __this->get_pretty_1();
		NullCheck(L_5);
		Il2CppObject * L_8 = JSONObject_StringifyAsync_m2756392147(L_5, 0, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_8);
		__this->set_U24locvar0_2(L_9);
		V_0 = ((int32_t)-3);
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_10 = V_0;
			if (((int32_t)((int32_t)L_10-(int32_t)1)) == 0)
			{
				goto IL_00b0;
			}
		}

IL_0078:
		{
			goto IL_00b0;
		}

IL_007d:
		{
			Il2CppObject * L_11 = __this->get_U24locvar0_2();
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_11);
			__this->set_U3CeU3E__1_3(((Il2CppObject *)Castclass(L_12, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			__this->set_U24current_6((String_t*)NULL);
			bool L_13 = __this->get_U24disposing_7();
			if (L_13)
			{
				goto IL_00a9;
			}
		}

IL_00a2:
		{
			__this->set_U24PC_8(1);
		}

IL_00a9:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x11C, FINALLY_00c5);
		}

IL_00b0:
		{
			Il2CppObject * L_14 = __this->get_U24locvar0_2();
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_007d;
			}
		}

IL_00c0:
		{
			IL2CPP_LEAVE(0xEE, FINALLY_00c5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c5;
	}

FINALLY_00c5:
	{ // begin finally (depth: 1)
		{
			bool L_16 = V_1;
			if (!L_16)
			{
				goto IL_00c9;
			}
		}

IL_00c8:
		{
			IL2CPP_END_FINALLY(197)
		}

IL_00c9:
		{
			Il2CppObject * L_17 = __this->get_U24locvar0_2();
			Il2CppObject * L_18 = ((Il2CppObject *)IsInst(L_17, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_2 = L_18;
			__this->set_U24locvar1_4(L_18);
			Il2CppObject * L_19 = V_2;
			if (!L_19)
			{
				goto IL_00ed;
			}
		}

IL_00e2:
		{
			Il2CppObject * L_20 = __this->get_U24locvar1_4();
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_20);
		}

IL_00ed:
		{
			IL2CPP_END_FINALLY(197)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(197)
	{
		IL2CPP_JUMP_TBL(0x11C, IL_011c)
		IL2CPP_JUMP_TBL(0xEE, IL_00ee)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ee:
	{
		StringBuilder_t1221177846 * L_21 = __this->get_U3CbuilderU3E__0_0();
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		__this->set_U24current_6(L_22);
		bool L_23 = __this->get_U24disposing_7();
		if (L_23)
		{
			goto IL_010e;
		}
	}
	{
		__this->set_U24PC_8(2);
	}

IL_010e:
	{
		goto IL_011c;
	}

IL_0113:
	{
		__this->set_U24PC_8((-1));
	}

IL_011a:
	{
		return (bool)0;
	}

IL_011c:
	{
		return (bool)1;
	}
}
// System.String JSONObject/<PrintAsync>c__Iterator1::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m115244662 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object JSONObject/<PrintAsync>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1570059502 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator1::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_Dispose_m271739125_MetadataUsageId;
extern "C"  void U3CPrintAsyncU3Ec__Iterator1_Dispose_m271739125 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_Dispose_m271739125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0056;
		}
		if (L_1 == 1)
		{
			goto IL_002c;
		}
		if (L_1 == 2)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0056;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x56, FINALLY_0031);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_U24locvar0_2();
			Il2CppObject * L_3 = ((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_1 = L_3;
			__this->set_U24locvar1_4(L_3);
			Il2CppObject * L_4 = V_1;
			if (!L_4)
			{
				goto IL_0055;
			}
		}

IL_004a:
		{
			Il2CppObject * L_5 = __this->get_U24locvar1_4();
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_0055:
		{
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0056:
	{
		return;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_Reset_m4157287975_MetadataUsageId;
extern "C"  void U3CPrintAsyncU3Ec__Iterator1_Reset_m4157287975 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_Reset_m4157287975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m2623495403 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3669576121(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<PrintAsync>c__Iterator1::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern Il2CppClass* U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3669576121_MetadataUsageId;
extern "C"  Il2CppObject* U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3669576121 (U3CPrintAsyncU3Ec__Iterator1_t716304657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3669576121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPrintAsyncU3Ec__Iterator1_t716304657 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_8();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_2 = (U3CPrintAsyncU3Ec__Iterator1_t716304657 *)il2cpp_codegen_object_new(U3CPrintAsyncU3Ec__Iterator1_t716304657_il2cpp_TypeInfo_var);
		U3CPrintAsyncU3Ec__Iterator1__ctor_m2008311324(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_3 = V_0;
		JSONObject_t1971882247 * L_4 = __this->get_U24this_5();
		NullCheck(L_3);
		L_3->set_U24this_5(L_4);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_5 = V_0;
		bool L_6 = __this->get_pretty_1();
		NullCheck(L_5);
		L_5->set_pretty_1(L_6);
		U3CPrintAsyncU3Ec__Iterator1_t716304657 * L_7 = V_0;
		return L_7;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator2::.ctor()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator2__ctor_m992930667 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean JSONObject/<StringifyAsync>c__Iterator2::MoveNext()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3990778905_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2041690406;
extern Il2CppCodeGenString* _stringLiteral1554931876;
extern Il2CppCodeGenString* _stringLiteral1751346954;
extern Il2CppCodeGenString* _stringLiteral376316188;
extern Il2CppCodeGenString* _stringLiteral452738781;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern Il2CppCodeGenString* _stringLiteral845265362;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m1989013769_MetadataUsageId;
extern "C"  bool U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m1989013769 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_MoveNext_m1989013769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	TimeSpan_t3430258949  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_18();
		V_0 = L_0;
		__this->set_U24PC_18((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0097;
		}
		if (L_1 == 2)
		{
			goto IL_030a;
		}
		if (L_1 == 3)
		{
			goto IL_0576;
		}
	}
	{
		goto IL_075a;
	}

IL_002b:
	{
		int32_t L_2 = __this->get_depth_0();
		int32_t L_3 = L_2;
		V_2 = L_3;
		__this->set_depth_0(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)100))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2041690406, /*hidden argument*/NULL);
		goto IL_075a;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_5 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_14();
		NullCheck(L_5);
		TimeSpan_t3430258949  L_6 = Stopwatch_get_Elapsed_m3190561196(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		double L_7 = TimeSpan_get_TotalSeconds_m1295026915((&V_3), /*hidden argument*/NULL);
		if ((!(((double)L_7) > ((double)(0.00800000037997961)))))
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_8 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_14();
		NullCheck(L_8);
		Stopwatch_Reset_m3196507227(L_8, /*hidden argument*/NULL);
		__this->set_U24current_15(NULL);
		bool L_9 = __this->get_U24disposing_16();
		if (L_9)
		{
			goto IL_0092;
		}
	}
	{
		__this->set_U24PC_18(1);
	}

IL_0092:
	{
		goto IL_075c;
	}

IL_0097:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		Stopwatch_t1380178105 * L_10 = ((JSONObject_t1971882247_StaticFields*)JSONObject_t1971882247_il2cpp_TypeInfo_var->static_fields)->get_printWatch_14();
		NullCheck(L_10);
		Stopwatch_Start_m2051791460(L_10, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		JSONObject_t1971882247 * L_11 = __this->get_U24this_14();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_type_5();
		__this->set_U24locvar0_1(L_12);
		int32_t L_13 = __this->get_U24locvar0_1();
		if (L_13 == 0)
		{
			goto IL_073d;
		}
		if (L_13 == 1)
		{
			goto IL_00fa;
		}
		if (L_13 == 2)
		{
			goto IL_011b;
		}
		if (L_13 == 3)
		{
			goto IL_01fa;
		}
		if (L_13 == 4)
		{
			goto IL_0495;
		}
		if (L_13 == 5)
		{
			goto IL_0701;
		}
		if (L_13 == 6)
		{
			goto IL_00de;
		}
	}
	{
		goto IL_0753;
	}

IL_00de:
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_2();
		JSONObject_t1971882247 * L_15 = __this->get_U24this_14();
		NullCheck(L_15);
		String_t* L_16 = L_15->get_str_8();
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, L_16, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_00fa:
	{
		StringBuilder_t1221177846 * L_17 = __this->get_builder_2();
		JSONObject_t1971882247 * L_18 = __this->get_U24this_14();
		NullCheck(L_18);
		String_t* L_19 = L_18->get_str_8();
		NullCheck(L_17);
		StringBuilder_AppendFormat_m3265503696(L_17, _stringLiteral1554931876, L_19, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_011b:
	{
		JSONObject_t1971882247 * L_20 = __this->get_U24this_14();
		NullCheck(L_20);
		bool L_21 = L_20->get_useInt_10();
		if (!L_21)
		{
			goto IL_0152;
		}
	}
	{
		StringBuilder_t1221177846 * L_22 = __this->get_builder_2();
		JSONObject_t1971882247 * L_23 = __this->get_U24this_14();
		NullCheck(L_23);
		int64_t* L_24 = L_23->get_address_of_i_11();
		String_t* L_25 = Int64_ToString_m689375889(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		StringBuilder_Append_m3636508479(L_22, L_25, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_0152:
	{
		JSONObject_t1971882247 * L_26 = __this->get_U24this_14();
		NullCheck(L_26);
		float L_27 = L_26->get_n_9();
		bool L_28 = Single_IsInfinity_m3331110346(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_017d;
		}
	}
	{
		StringBuilder_t1221177846 * L_29 = __this->get_builder_2();
		NullCheck(L_29);
		StringBuilder_Append_m3636508479(L_29, _stringLiteral1751346954, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_017d:
	{
		JSONObject_t1971882247 * L_30 = __this->get_U24this_14();
		NullCheck(L_30);
		float L_31 = L_30->get_n_9();
		bool L_32 = Single_IsNegativeInfinity_m2615809279(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_01a8;
		}
	}
	{
		StringBuilder_t1221177846 * L_33 = __this->get_builder_2();
		NullCheck(L_33);
		StringBuilder_Append_m3636508479(L_33, _stringLiteral376316188, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_01a8:
	{
		JSONObject_t1971882247 * L_34 = __this->get_U24this_14();
		NullCheck(L_34);
		float L_35 = L_34->get_n_9();
		bool L_36 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01d3;
		}
	}
	{
		StringBuilder_t1221177846 * L_37 = __this->get_builder_2();
		NullCheck(L_37);
		StringBuilder_Append_m3636508479(L_37, _stringLiteral452738781, /*hidden argument*/NULL);
		goto IL_01f5;
	}

IL_01d3:
	{
		StringBuilder_t1221177846 * L_38 = __this->get_builder_2();
		JSONObject_t1971882247 * L_39 = __this->get_U24this_14();
		NullCheck(L_39);
		float* L_40 = L_39->get_address_of_n_9();
		String_t* L_41 = Single_ToString_m1813392066(L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		StringBuilder_Append_m3636508479(L_38, L_41, /*hidden argument*/NULL);
	}

IL_01f5:
	{
		goto IL_0753;
	}

IL_01fa:
	{
		StringBuilder_t1221177846 * L_42 = __this->get_builder_2();
		NullCheck(L_42);
		StringBuilder_Append_m3636508479(L_42, _stringLiteral372029399, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_43 = __this->get_U24this_14();
		NullCheck(L_43);
		List_1_t1341003379 * L_44 = L_43->get_list_6();
		NullCheck(L_44);
		int32_t L_45 = List_1_get_Count_m1879545138(L_44, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_45) <= ((int32_t)0)))
		{
			goto IL_041f;
		}
	}
	{
		bool L_46 = __this->get_pretty_3();
		if (!L_46)
		{
			goto IL_023d;
		}
	}
	{
		StringBuilder_t1221177846 * L_47 = __this->get_builder_2();
		NullCheck(L_47);
		StringBuilder_Append_m3636508479(L_47, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_023d:
	{
		__this->set_U3CiU3E__0_4(0);
		goto IL_03ce;
	}

IL_0249:
	{
		JSONObject_t1971882247 * L_48 = __this->get_U24this_14();
		NullCheck(L_48);
		List_1_t1398341365 * L_49 = L_48->get_keys_7();
		int32_t L_50 = __this->get_U3CiU3E__0_4();
		NullCheck(L_49);
		String_t* L_51 = List_1_get_Item_m1112119647(L_49, L_50, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_U3CkeyU3E__1_5(L_51);
		JSONObject_t1971882247 * L_52 = __this->get_U24this_14();
		NullCheck(L_52);
		List_1_t1341003379 * L_53 = L_52->get_list_6();
		int32_t L_54 = __this->get_U3CiU3E__0_4();
		NullCheck(L_53);
		JSONObject_t1971882247 * L_55 = List_1_get_Item_m3990778905(L_53, L_54, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		__this->set_U3CobjU3E__2_6(L_55);
		JSONObject_t1971882247 * L_56 = __this->get_U3CobjU3E__2_6();
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_57 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_03c0;
		}
	}
	{
		bool L_58 = __this->get_pretty_3();
		if (!L_58)
		{
			goto IL_02c8;
		}
	}
	{
		V_4 = 0;
		goto IL_02bb;
	}

IL_02a4:
	{
		StringBuilder_t1221177846 * L_59 = __this->get_builder_2();
		NullCheck(L_59);
		StringBuilder_Append_m3636508479(L_59, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_60 = V_4;
		V_4 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_02bb:
	{
		int32_t L_61 = V_4;
		int32_t L_62 = __this->get_depth_0();
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_02a4;
		}
	}

IL_02c8:
	{
		StringBuilder_t1221177846 * L_63 = __this->get_builder_2();
		String_t* L_64 = __this->get_U3CkeyU3E__1_5();
		NullCheck(L_63);
		StringBuilder_AppendFormat_m3265503696(L_63, _stringLiteral845265362, L_64, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_65 = __this->get_U3CobjU3E__2_6();
		int32_t L_66 = __this->get_depth_0();
		StringBuilder_t1221177846 * L_67 = __this->get_builder_2();
		bool L_68 = __this->get_pretty_3();
		NullCheck(L_65);
		Il2CppObject * L_69 = JSONObject_StringifyAsync_m2756392147(L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Il2CppObject * L_70 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_69);
		__this->set_U24locvar1_7(L_70);
		V_0 = ((int32_t)-3);
	}

IL_030a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_71 = V_0;
			if (((int32_t)((int32_t)L_71-(int32_t)2)) == 0)
			{
				goto IL_0353;
			}
		}

IL_0316:
		{
			goto IL_0353;
		}

IL_031b:
		{
			Il2CppObject * L_72 = __this->get_U24locvar1_7();
			NullCheck(L_72);
			Il2CppObject * L_73 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_72);
			__this->set_U3CeU3E__3_8(((Il2CppObject *)Castclass(L_73, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			Il2CppObject * L_74 = __this->get_U3CeU3E__3_8();
			__this->set_U24current_15(L_74);
			bool L_75 = __this->get_U24disposing_16();
			if (L_75)
			{
				goto IL_034c;
			}
		}

IL_0345:
		{
			__this->set_U24PC_18(2);
		}

IL_034c:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x75C, FINALLY_0368);
		}

IL_0353:
		{
			Il2CppObject * L_76 = __this->get_U24locvar1_7();
			NullCheck(L_76);
			bool L_77 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_76);
			if (L_77)
			{
				goto IL_031b;
			}
		}

IL_0363:
		{
			IL2CPP_LEAVE(0x393, FINALLY_0368);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0368;
	}

FINALLY_0368:
	{ // begin finally (depth: 1)
		{
			bool L_78 = V_1;
			if (!L_78)
			{
				goto IL_036c;
			}
		}

IL_036b:
		{
			IL2CPP_END_FINALLY(872)
		}

IL_036c:
		{
			Il2CppObject * L_79 = __this->get_U24locvar1_7();
			Il2CppObject * L_80 = ((Il2CppObject *)IsInst(L_79, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_5 = L_80;
			__this->set_U24locvar2_9(L_80);
			Il2CppObject * L_81 = V_5;
			if (!L_81)
			{
				goto IL_0392;
			}
		}

IL_0387:
		{
			Il2CppObject * L_82 = __this->get_U24locvar2_9();
			NullCheck(L_82);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_82);
		}

IL_0392:
		{
			IL2CPP_END_FINALLY(872)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(872)
	{
		IL2CPP_JUMP_TBL(0x75C, IL_075c)
		IL2CPP_JUMP_TBL(0x393, IL_0393)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0393:
	{
		StringBuilder_t1221177846 * L_83 = __this->get_builder_2();
		NullCheck(L_83);
		StringBuilder_Append_m3636508479(L_83, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_84 = __this->get_pretty_3();
		if (!L_84)
		{
			goto IL_03c0;
		}
	}
	{
		StringBuilder_t1221177846 * L_85 = __this->get_builder_2();
		NullCheck(L_85);
		StringBuilder_Append_m3636508479(L_85, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_03c0:
	{
		int32_t L_86 = __this->get_U3CiU3E__0_4();
		__this->set_U3CiU3E__0_4(((int32_t)((int32_t)L_86+(int32_t)1)));
	}

IL_03ce:
	{
		int32_t L_87 = __this->get_U3CiU3E__0_4();
		JSONObject_t1971882247 * L_88 = __this->get_U24this_14();
		NullCheck(L_88);
		List_1_t1341003379 * L_89 = L_88->get_list_6();
		NullCheck(L_89);
		int32_t L_90 = List_1_get_Count_m1879545138(L_89, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_87) < ((int32_t)L_90)))
		{
			goto IL_0249;
		}
	}
	{
		bool L_91 = __this->get_pretty_3();
		if (!L_91)
		{
			goto IL_040c;
		}
	}
	{
		StringBuilder_t1221177846 * L_92 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_93 = L_92;
		NullCheck(L_93);
		int32_t L_94 = StringBuilder_get_Length_m1608241323(L_93, /*hidden argument*/NULL);
		NullCheck(L_93);
		StringBuilder_set_Length_m3039225444(L_93, ((int32_t)((int32_t)L_94-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_041f;
	}

IL_040c:
	{
		StringBuilder_t1221177846 * L_95 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_96 = L_95;
		NullCheck(L_96);
		int32_t L_97 = StringBuilder_get_Length_m1608241323(L_96, /*hidden argument*/NULL);
		NullCheck(L_96);
		StringBuilder_set_Length_m3039225444(L_96, ((int32_t)((int32_t)L_97-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_041f:
	{
		bool L_98 = __this->get_pretty_3();
		if (!L_98)
		{
			goto IL_047f;
		}
	}
	{
		JSONObject_t1971882247 * L_99 = __this->get_U24this_14();
		NullCheck(L_99);
		List_1_t1341003379 * L_100 = L_99->get_list_6();
		NullCheck(L_100);
		int32_t L_101 = List_1_get_Count_m1879545138(L_100, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_101) <= ((int32_t)0)))
		{
			goto IL_047f;
		}
	}
	{
		StringBuilder_t1221177846 * L_102 = __this->get_builder_2();
		NullCheck(L_102);
		StringBuilder_Append_m3636508479(L_102, _stringLiteral372029352, /*hidden argument*/NULL);
		V_6 = 0;
		goto IL_0470;
	}

IL_0459:
	{
		StringBuilder_t1221177846 * L_103 = __this->get_builder_2();
		NullCheck(L_103);
		StringBuilder_Append_m3636508479(L_103, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_104 = V_6;
		V_6 = ((int32_t)((int32_t)L_104+(int32_t)1));
	}

IL_0470:
	{
		int32_t L_105 = V_6;
		int32_t L_106 = __this->get_depth_0();
		if ((((int32_t)L_105) < ((int32_t)((int32_t)((int32_t)L_106-(int32_t)1)))))
		{
			goto IL_0459;
		}
	}

IL_047f:
	{
		StringBuilder_t1221177846 * L_107 = __this->get_builder_2();
		NullCheck(L_107);
		StringBuilder_Append_m3636508479(L_107, _stringLiteral372029393, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_0495:
	{
		StringBuilder_t1221177846 * L_108 = __this->get_builder_2();
		NullCheck(L_108);
		StringBuilder_Append_m3636508479(L_108, _stringLiteral372029431, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_109 = __this->get_U24this_14();
		NullCheck(L_109);
		List_1_t1341003379 * L_110 = L_109->get_list_6();
		NullCheck(L_110);
		int32_t L_111 = List_1_get_Count_m1879545138(L_110, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_111) <= ((int32_t)0)))
		{
			goto IL_068b;
		}
	}
	{
		bool L_112 = __this->get_pretty_3();
		if (!L_112)
		{
			goto IL_04d8;
		}
	}
	{
		StringBuilder_t1221177846 * L_113 = __this->get_builder_2();
		NullCheck(L_113);
		StringBuilder_Append_m3636508479(L_113, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_04d8:
	{
		__this->set_U3CiU3E__4_10(0);
		goto IL_063a;
	}

IL_04e4:
	{
		JSONObject_t1971882247 * L_114 = __this->get_U24this_14();
		NullCheck(L_114);
		List_1_t1341003379 * L_115 = L_114->get_list_6();
		int32_t L_116 = __this->get_U3CiU3E__4_10();
		NullCheck(L_115);
		JSONObject_t1971882247 * L_117 = List_1_get_Item_m3990778905(L_115, L_116, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_118 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		if (!L_118)
		{
			goto IL_062c;
		}
	}
	{
		bool L_119 = __this->get_pretty_3();
		if (!L_119)
		{
			goto IL_053b;
		}
	}
	{
		V_7 = 0;
		goto IL_052e;
	}

IL_0517:
	{
		StringBuilder_t1221177846 * L_120 = __this->get_builder_2();
		NullCheck(L_120);
		StringBuilder_Append_m3636508479(L_120, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_121 = V_7;
		V_7 = ((int32_t)((int32_t)L_121+(int32_t)1));
	}

IL_052e:
	{
		int32_t L_122 = V_7;
		int32_t L_123 = __this->get_depth_0();
		if ((((int32_t)L_122) < ((int32_t)L_123)))
		{
			goto IL_0517;
		}
	}

IL_053b:
	{
		JSONObject_t1971882247 * L_124 = __this->get_U24this_14();
		NullCheck(L_124);
		List_1_t1341003379 * L_125 = L_124->get_list_6();
		int32_t L_126 = __this->get_U3CiU3E__4_10();
		NullCheck(L_125);
		JSONObject_t1971882247 * L_127 = List_1_get_Item_m3990778905(L_125, L_126, /*hidden argument*/List_1_get_Item_m3990778905_MethodInfo_var);
		int32_t L_128 = __this->get_depth_0();
		StringBuilder_t1221177846 * L_129 = __this->get_builder_2();
		bool L_130 = __this->get_pretty_3();
		NullCheck(L_127);
		Il2CppObject * L_131 = JSONObject_StringifyAsync_m2756392147(L_127, L_128, L_129, L_130, /*hidden argument*/NULL);
		NullCheck(L_131);
		Il2CppObject * L_132 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_131);
		__this->set_U24locvar3_11(L_132);
		V_0 = ((int32_t)-3);
	}

IL_0576:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_133 = V_0;
			if (((int32_t)((int32_t)L_133-(int32_t)3)) == 0)
			{
				goto IL_05bf;
			}
		}

IL_0582:
		{
			goto IL_05bf;
		}

IL_0587:
		{
			Il2CppObject * L_134 = __this->get_U24locvar3_11();
			NullCheck(L_134);
			Il2CppObject * L_135 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_134);
			__this->set_U3CeU3E__5_12(((Il2CppObject *)Castclass(L_135, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			Il2CppObject * L_136 = __this->get_U3CeU3E__5_12();
			__this->set_U24current_15(L_136);
			bool L_137 = __this->get_U24disposing_16();
			if (L_137)
			{
				goto IL_05b8;
			}
		}

IL_05b1:
		{
			__this->set_U24PC_18(3);
		}

IL_05b8:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x75C, FINALLY_05d4);
		}

IL_05bf:
		{
			Il2CppObject * L_138 = __this->get_U24locvar3_11();
			NullCheck(L_138);
			bool L_139 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_138);
			if (L_139)
			{
				goto IL_0587;
			}
		}

IL_05cf:
		{
			IL2CPP_LEAVE(0x5FF, FINALLY_05d4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_05d4;
	}

FINALLY_05d4:
	{ // begin finally (depth: 1)
		{
			bool L_140 = V_1;
			if (!L_140)
			{
				goto IL_05d8;
			}
		}

IL_05d7:
		{
			IL2CPP_END_FINALLY(1492)
		}

IL_05d8:
		{
			Il2CppObject * L_141 = __this->get_U24locvar3_11();
			Il2CppObject * L_142 = ((Il2CppObject *)IsInst(L_141, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_5 = L_142;
			__this->set_U24locvar4_13(L_142);
			Il2CppObject * L_143 = V_5;
			if (!L_143)
			{
				goto IL_05fe;
			}
		}

IL_05f3:
		{
			Il2CppObject * L_144 = __this->get_U24locvar4_13();
			NullCheck(L_144);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_144);
		}

IL_05fe:
		{
			IL2CPP_END_FINALLY(1492)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1492)
	{
		IL2CPP_JUMP_TBL(0x75C, IL_075c)
		IL2CPP_JUMP_TBL(0x5FF, IL_05ff)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_05ff:
	{
		StringBuilder_t1221177846 * L_145 = __this->get_builder_2();
		NullCheck(L_145);
		StringBuilder_Append_m3636508479(L_145, _stringLiteral372029314, /*hidden argument*/NULL);
		bool L_146 = __this->get_pretty_3();
		if (!L_146)
		{
			goto IL_062c;
		}
	}
	{
		StringBuilder_t1221177846 * L_147 = __this->get_builder_2();
		NullCheck(L_147);
		StringBuilder_Append_m3636508479(L_147, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_062c:
	{
		int32_t L_148 = __this->get_U3CiU3E__4_10();
		__this->set_U3CiU3E__4_10(((int32_t)((int32_t)L_148+(int32_t)1)));
	}

IL_063a:
	{
		int32_t L_149 = __this->get_U3CiU3E__4_10();
		JSONObject_t1971882247 * L_150 = __this->get_U24this_14();
		NullCheck(L_150);
		List_1_t1341003379 * L_151 = L_150->get_list_6();
		NullCheck(L_151);
		int32_t L_152 = List_1_get_Count_m1879545138(L_151, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_149) < ((int32_t)L_152)))
		{
			goto IL_04e4;
		}
	}
	{
		bool L_153 = __this->get_pretty_3();
		if (!L_153)
		{
			goto IL_0678;
		}
	}
	{
		StringBuilder_t1221177846 * L_154 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = StringBuilder_get_Length_m1608241323(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		StringBuilder_set_Length_m3039225444(L_155, ((int32_t)((int32_t)L_156-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_068b;
	}

IL_0678:
	{
		StringBuilder_t1221177846 * L_157 = __this->get_builder_2();
		StringBuilder_t1221177846 * L_158 = L_157;
		NullCheck(L_158);
		int32_t L_159 = StringBuilder_get_Length_m1608241323(L_158, /*hidden argument*/NULL);
		NullCheck(L_158);
		StringBuilder_set_Length_m3039225444(L_158, ((int32_t)((int32_t)L_159-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_068b:
	{
		bool L_160 = __this->get_pretty_3();
		if (!L_160)
		{
			goto IL_06eb;
		}
	}
	{
		JSONObject_t1971882247 * L_161 = __this->get_U24this_14();
		NullCheck(L_161);
		List_1_t1341003379 * L_162 = L_161->get_list_6();
		NullCheck(L_162);
		int32_t L_163 = List_1_get_Count_m1879545138(L_162, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_163) <= ((int32_t)0)))
		{
			goto IL_06eb;
		}
	}
	{
		StringBuilder_t1221177846 * L_164 = __this->get_builder_2();
		NullCheck(L_164);
		StringBuilder_Append_m3636508479(L_164, _stringLiteral372029352, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_06dc;
	}

IL_06c5:
	{
		StringBuilder_t1221177846 * L_165 = __this->get_builder_2();
		NullCheck(L_165);
		StringBuilder_Append_m3636508479(L_165, _stringLiteral372029349, /*hidden argument*/NULL);
		int32_t L_166 = V_8;
		V_8 = ((int32_t)((int32_t)L_166+(int32_t)1));
	}

IL_06dc:
	{
		int32_t L_167 = V_8;
		int32_t L_168 = __this->get_depth_0();
		if ((((int32_t)L_167) < ((int32_t)((int32_t)((int32_t)L_168-(int32_t)1)))))
		{
			goto IL_06c5;
		}
	}

IL_06eb:
	{
		StringBuilder_t1221177846 * L_169 = __this->get_builder_2();
		NullCheck(L_169);
		StringBuilder_Append_m3636508479(L_169, _stringLiteral372029425, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_0701:
	{
		JSONObject_t1971882247 * L_170 = __this->get_U24this_14();
		NullCheck(L_170);
		bool L_171 = L_170->get_b_12();
		if (!L_171)
		{
			goto IL_0727;
		}
	}
	{
		StringBuilder_t1221177846 * L_172 = __this->get_builder_2();
		NullCheck(L_172);
		StringBuilder_Append_m3636508479(L_172, _stringLiteral3323263070, /*hidden argument*/NULL);
		goto IL_0738;
	}

IL_0727:
	{
		StringBuilder_t1221177846 * L_173 = __this->get_builder_2();
		NullCheck(L_173);
		StringBuilder_Append_m3636508479(L_173, _stringLiteral2609877245, /*hidden argument*/NULL);
	}

IL_0738:
	{
		goto IL_0753;
	}

IL_073d:
	{
		StringBuilder_t1221177846 * L_174 = __this->get_builder_2();
		NullCheck(L_174);
		StringBuilder_Append_m3636508479(L_174, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_0753;
	}

IL_0753:
	{
		__this->set_U24PC_18((-1));
	}

IL_075a:
	{
		return (bool)0;
	}

IL_075c:
	{
		return (bool)1;
	}
}
// System.Object JSONObject/<StringifyAsync>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1261231077 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_15();
		return L_0;
	}
}
// System.Object JSONObject/<StringifyAsync>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3799945405 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_15();
		return L_0;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator2::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_Dispose_m3203954212_MetadataUsageId;
extern "C"  void U3CStringifyAsyncU3Ec__Iterator2_Dispose_m3203954212 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_Dispose_m3203954212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_18();
		V_0 = L_0;
		__this->set_U24disposing_16((bool)1);
		__this->set_U24PC_18((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0089;
		}
		if (L_1 == 1)
		{
			goto IL_0089;
		}
		if (L_1 == 2)
		{
			goto IL_0030;
		}
		if (L_1 == 3)
		{
			goto IL_005f;
		}
	}
	{
		goto IL_0089;
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x5A, FINALLY_0035);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_U24locvar1_7();
			Il2CppObject * L_3 = ((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_1 = L_3;
			__this->set_U24locvar2_9(L_3);
			Il2CppObject * L_4 = V_1;
			if (!L_4)
			{
				goto IL_0059;
			}
		}

IL_004e:
		{
			Il2CppObject * L_5 = __this->get_U24locvar2_9();
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_0059:
		{
			IL2CPP_END_FINALLY(53)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005a:
	{
		goto IL_0089;
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x89, FINALLY_0064);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_6 = __this->get_U24locvar3_11();
			Il2CppObject * L_7 = ((Il2CppObject *)IsInst(L_6, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_1 = L_7;
			__this->set_U24locvar4_13(L_7);
			Il2CppObject * L_8 = V_1;
			if (!L_8)
			{
				goto IL_0088;
			}
		}

IL_007d:
		{
			Il2CppObject * L_9 = __this->get_U24locvar4_13();
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0088:
		{
			IL2CPP_END_FINALLY(100)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0089:
	{
		return;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_Reset_m1426621878_MetadataUsageId;
extern "C"  void U3CStringifyAsyncU3Ec__Iterator2_Reset_m1426621878 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_Reset_m1426621878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m815430336 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1727038216(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator2::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1727038216_MetadataUsageId;
extern "C"  Il2CppObject* U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1727038216 (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1727038216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_18();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_2 = (U3CStringifyAsyncU3Ec__Iterator2_t4037879552 *)il2cpp_codegen_object_new(U3CStringifyAsyncU3Ec__Iterator2_t4037879552_il2cpp_TypeInfo_var);
		U3CStringifyAsyncU3Ec__Iterator2__ctor_m992930667(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_3 = V_0;
		JSONObject_t1971882247 * L_4 = __this->get_U24this_14();
		NullCheck(L_3);
		L_3->set_U24this_14(L_4);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Edepth_17();
		NullCheck(L_5);
		L_5->set_depth_0(L_6);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_7 = V_0;
		StringBuilder_t1221177846 * L_8 = __this->get_builder_2();
		NullCheck(L_7);
		L_7->set_builder_2(L_8);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_9 = V_0;
		bool L_10 = __this->get_pretty_3();
		NullCheck(L_9);
		L_9->set_pretty_3(L_10);
		U3CStringifyAsyncU3Ec__Iterator2_t4037879552 * L_11 = V_0;
		return L_11;
	}
}
// System.Void JSONObject/AddJSONContents::.ctor(System.Object,System.IntPtr)
extern "C"  void AddJSONContents__ctor_m485378606 (AddJSONContents_t3850664647 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JSONObject/AddJSONContents::Invoke(JSONObject)
extern "C"  void AddJSONContents_Invoke_m412080169 (AddJSONContents_t3850664647 * __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddJSONContents_Invoke_m412080169((AddJSONContents_t3850664647 *)__this->get_prev_9(),___self0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___self0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___self0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___self0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult JSONObject/AddJSONContents::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddJSONContents_BeginInvoke_m4152843006 (AddJSONContents_t3850664647 * __this, JSONObject_t1971882247 * ___self0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___self0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void JSONObject/AddJSONContents::EndInvoke(System.IAsyncResult)
extern "C"  void AddJSONContents_EndInvoke_m2540087684 (AddJSONContents_t3850664647 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void JSONObject/FieldNotFound::.ctor(System.Object,System.IntPtr)
extern "C"  void FieldNotFound__ctor_m1500725734 (FieldNotFound_t865402053 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JSONObject/FieldNotFound::Invoke(System.String)
extern "C"  void FieldNotFound_Invoke_m677615214 (FieldNotFound_t865402053 * __this, String_t* ___name0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FieldNotFound_Invoke_m677615214((FieldNotFound_t865402053 *)__this->get_prev_9(),___name0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___name0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___name0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___name0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___name0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___name0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FieldNotFound_t865402053 (FieldNotFound_t865402053 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	il2cppPInvokeFunc(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
// System.IAsyncResult JSONObject/FieldNotFound::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FieldNotFound_BeginInvoke_m3313113739 (FieldNotFound_t865402053 * __this, String_t* ___name0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___name0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void JSONObject/FieldNotFound::EndInvoke(System.IAsyncResult)
extern "C"  void FieldNotFound_EndInvoke_m804539840 (FieldNotFound_t865402053 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void JSONObject/GetFieldResponse::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFieldResponse__ctor_m484938706 (GetFieldResponse_t1259369279 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JSONObject/GetFieldResponse::Invoke(JSONObject)
extern "C"  void GetFieldResponse_Invoke_m1070419673 (GetFieldResponse_t1259369279 * __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetFieldResponse_Invoke_m1070419673((GetFieldResponse_t1259369279 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JSONObject_t1971882247 * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult JSONObject/GetFieldResponse::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFieldResponse_BeginInvoke_m978030778 (GetFieldResponse_t1259369279 * __this, JSONObject_t1971882247 * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void JSONObject/GetFieldResponse::EndInvoke(System.IAsyncResult)
extern "C"  void GetFieldResponse_EndInvoke_m3298171160 (GetFieldResponse_t1259369279 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void JSONObjectEnumer::.ctor(JSONObject)
extern "C"  void JSONObjectEnumer__ctor_m34994965 (JSONObjectEnumer_t427597183 * __this, JSONObject_t1971882247 * ___jsonObject0, const MethodInfo* method)
{
	{
		__this->set_position_1((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_0 = ___jsonObject0;
		__this->set__jobj_0(L_0);
		return;
	}
}
// System.Boolean JSONObjectEnumer::MoveNext()
extern "C"  bool JSONObjectEnumer_MoveNext_m3554948016 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_position_1();
		__this->set_position_1(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_position_1();
		JSONObject_t1971882247 * L_2 = __this->get__jobj_0();
		NullCheck(L_2);
		int32_t L_3 = JSONObject_get_Count_m3265098784(L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) < ((int32_t)L_3))? 1 : 0);
	}
}
// System.Void JSONObjectEnumer::Reset()
extern "C"  void JSONObjectEnumer_Reset_m2137692453 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method)
{
	{
		__this->set_position_1((-1));
		return;
	}
}
// System.Object JSONObjectEnumer::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * JSONObjectEnumer_System_Collections_IEnumerator_get_Current_m2823805388 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method)
{
	{
		JSONObject_t1971882247 * L_0 = JSONObjectEnumer_get_Current_m78793822(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObjectEnumer::get_Current()
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const uint32_t JSONObjectEnumer_get_Current_m78793822_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONObjectEnumer_get_Current_m78793822 (JSONObjectEnumer_t427597183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObjectEnumer_get_Current_m78793822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		JSONObject_t1971882247 * L_0 = __this->get__jobj_0();
		NullCheck(L_0);
		bool L_1 = JSONObject_get_IsArray_m1146507746(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = __this->get__jobj_0();
		int32_t L_3 = __this->get_position_1();
		NullCheck(L_2);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m1007985851(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0022:
	{
		JSONObject_t1971882247 * L_5 = __this->get__jobj_0();
		NullCheck(L_5);
		List_1_t1398341365 * L_6 = L_5->get_keys_7();
		int32_t L_7 = __this->get_position_1();
		NullCheck(L_6);
		String_t* L_8 = List_1_get_Item_m1112119647(L_6, L_7, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_0 = L_8;
		JSONObject_t1971882247 * L_9 = __this->get__jobj_0();
		String_t* L_10 = V_0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_11 = JSONObject_get_Item_m223257276(L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// JSONObject JSONTemplates::TOJSON(System.Object)
extern const Il2CppType* JSONTemplates_t3006274921_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Add_m2563198203_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2967033390;
extern Il2CppCodeGenString* _stringLiteral2122037725;
extern Il2CppCodeGenString* _stringLiteral1264830171;
extern Il2CppCodeGenString* _stringLiteral2539188669;
extern Il2CppCodeGenString* _stringLiteral1638831994;
extern const uint32_t JSONTemplates_TOJSON_m2387349653_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_TOJSON_m2387349653 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_TOJSON_m2387349653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	FieldInfoU5BU5D_t125053523* V_1 = NULL;
	FieldInfo_t * V_2 = NULL;
	FieldInfoU5BU5D_t125053523* V_3 = NULL;
	int32_t V_4 = 0;
	JSONObject_t1971882247 * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	ObjectU5BU5D_t3614634134* V_7 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_8 = NULL;
	PropertyInfo_t * V_9 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_10 = NULL;
	int32_t V_11 = 0;
	JSONObject_t1971882247 * V_12 = NULL;
	MethodInfo_t * V_13 = NULL;
	ObjectU5BU5D_t3614634134* V_14 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONTemplates_t3006274921_il2cpp_TypeInfo_var);
		HashSet_1_t1022910149 * L_0 = ((JSONTemplates_t3006274921_StaticFields*)JSONTemplates_t3006274921_il2cpp_TypeInfo_var->static_fields)->get_touched_0();
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		bool L_2 = HashSet_1_Add_m2563198203(L_0, L_1, /*hidden argument*/HashSet_1_Add_m2563198203_MethodInfo_var);
		if (!L_2)
		{
			goto IL_02d7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_3 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m191970594(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FieldInfoU5BU5D_t125053523* L_6 = Type_GetFields_m445058499(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		FieldInfoU5BU5D_t125053523* L_7 = V_1;
		V_3 = L_7;
		V_4 = 0;
		goto IL_0162;
	}

IL_002c:
	{
		FieldInfoU5BU5D_t125053523* L_8 = V_3;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		FieldInfo_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_2 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_12 = JSONObject_get_nullJO_m928623965(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_12;
		FieldInfo_t * L_13 = V_2;
		Il2CppObject * L_14 = ___obj0;
		NullCheck(L_13);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_13, L_14);
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_15, NULL);
		if (L_16)
		{
			goto IL_00e4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JSONTemplates_t3006274921_0_0_0_var), /*hidden argument*/NULL);
		FieldInfo_t * L_18 = V_2;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_18);
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2967033390, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		MethodInfo_t * L_22 = Type_GetMethod_m1197504218(L_17, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		MethodInfo_t * L_23 = V_6;
		if (!L_23)
		{
			goto IL_00a0;
		}
	}
	{
		V_7 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3614634134* L_24 = V_7;
		FieldInfo_t * L_25 = V_2;
		Il2CppObject * L_26 = ___obj0;
		NullCheck(L_25);
		Il2CppObject * L_27 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_25, L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_27);
		MethodInfo_t * L_28 = V_6;
		ObjectU5BU5D_t3614634134* L_29 = V_7;
		NullCheck(L_28);
		Il2CppObject * L_30 = MethodBase_Invoke_m1075809207(L_28, NULL, L_29, /*hidden argument*/NULL);
		V_5 = ((JSONObject_t1971882247 *)CastclassClass(L_30, JSONObject_t1971882247_il2cpp_TypeInfo_var));
		goto IL_00e4;
	}

IL_00a0:
	{
		FieldInfo_t * L_31 = V_2;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_31);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_32) == ((Il2CppObject*)(Type_t *)L_33))))
		{
			goto IL_00cd;
		}
	}
	{
		FieldInfo_t * L_34 = V_2;
		Il2CppObject * L_35 = ___obj0;
		NullCheck(L_34);
		Il2CppObject * L_36 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_34, L_35);
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_36);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_38 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		goto IL_00e4;
	}

IL_00cd:
	{
		FieldInfo_t * L_39 = V_2;
		Il2CppObject * L_40 = ___obj0;
		NullCheck(L_39);
		Il2CppObject * L_41 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_39, L_40);
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_41);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_43 = JSONObject_Create_m2628830417(NULL /*static, unused*/, L_42, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_5 = L_43;
	}

IL_00e4:
	{
		JSONObject_t1971882247 * L_44 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_45 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_015c;
		}
	}
	{
		JSONObject_t1971882247 * L_46 = V_5;
		NullCheck(L_46);
		int32_t L_47 = L_46->get_type_5();
		if (!L_47)
		{
			goto IL_010f;
		}
	}
	{
		JSONObject_t1971882247 * L_48 = V_0;
		FieldInfo_t * L_49 = V_2;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_49);
		JSONObject_t1971882247 * L_51 = V_5;
		NullCheck(L_48);
		JSONObject_AddField_m1302524246(L_48, L_50, L_51, /*hidden argument*/NULL);
		goto IL_015c;
	}

IL_010f:
	{
		StringU5BU5D_t1642385972* L_52 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, _stringLiteral2122037725);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2122037725);
		StringU5BU5D_t1642385972* L_53 = L_52;
		FieldInfo_t * L_54 = V_2;
		NullCheck(L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_54);
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, L_55);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_55);
		StringU5BU5D_t1642385972* L_56 = L_53;
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, _stringLiteral1264830171);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1264830171);
		StringU5BU5D_t1642385972* L_57 = L_56;
		Il2CppObject * L_58 = ___obj0;
		NullCheck(L_58);
		Type_t * L_59 = Object_GetType_m191970594(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_59);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_60);
		StringU5BU5D_t1642385972* L_61 = L_57;
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, _stringLiteral2539188669);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2539188669);
		StringU5BU5D_t1642385972* L_62 = L_61;
		FieldInfo_t * L_63 = V_2;
		NullCheck(L_63);
		Type_t * L_64 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_63);
		NullCheck(L_64);
		String_t* L_65 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_64);
		NullCheck(L_62);
		ArrayElementTypeCheck (L_62, L_65);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Concat_m626692867(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
	}

IL_015c:
	{
		int32_t L_67 = V_4;
		V_4 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0162:
	{
		int32_t L_68 = V_4;
		FieldInfoU5BU5D_t125053523* L_69 = V_3;
		NullCheck(L_69);
		if ((((int32_t)L_68) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_69)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_70 = ___obj0;
		NullCheck(L_70);
		Type_t * L_71 = Object_GetType_m191970594(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		PropertyInfoU5BU5D_t1736152084* L_72 = Type_GetProperties_m2803026104(L_71, /*hidden argument*/NULL);
		V_8 = L_72;
		PropertyInfoU5BU5D_t1736152084* L_73 = V_8;
		V_10 = L_73;
		V_11 = 0;
		goto IL_02ca;
	}

IL_0185:
	{
		PropertyInfoU5BU5D_t1736152084* L_74 = V_10;
		int32_t L_75 = V_11;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		PropertyInfo_t * L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		V_9 = L_77;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_78 = JSONObject_get_nullJO_m928623965(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_12 = L_78;
		PropertyInfo_t * L_79 = V_9;
		Il2CppObject * L_80 = ___obj0;
		NullCheck(L_79);
		Il2CppObject * L_81 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_79, L_80, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_81);
		bool L_82 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_81, NULL);
		if (L_82)
		{
			goto IL_0249;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_83 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JSONTemplates_t3006274921_0_0_0_var), /*hidden argument*/NULL);
		PropertyInfo_t * L_84 = V_9;
		NullCheck(L_84);
		Type_t * L_85 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_84);
		NullCheck(L_85);
		String_t* L_86 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_85);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2967033390, L_86, /*hidden argument*/NULL);
		NullCheck(L_83);
		MethodInfo_t * L_88 = Type_GetMethod_m1197504218(L_83, L_87, /*hidden argument*/NULL);
		V_13 = L_88;
		MethodInfo_t * L_89 = V_13;
		if (!L_89)
		{
			goto IL_0200;
		}
	}
	{
		V_14 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3614634134* L_90 = V_14;
		PropertyInfo_t * L_91 = V_9;
		Il2CppObject * L_92 = ___obj0;
		NullCheck(L_91);
		Il2CppObject * L_93 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_91, L_92, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_90);
		ArrayElementTypeCheck (L_90, L_93);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_93);
		MethodInfo_t * L_94 = V_13;
		ObjectU5BU5D_t3614634134* L_95 = V_14;
		NullCheck(L_94);
		Il2CppObject * L_96 = MethodBase_Invoke_m1075809207(L_94, NULL, L_95, /*hidden argument*/NULL);
		V_12 = ((JSONObject_t1971882247 *)CastclassClass(L_96, JSONObject_t1971882247_il2cpp_TypeInfo_var));
		goto IL_0249;
	}

IL_0200:
	{
		PropertyInfo_t * L_97 = V_9;
		NullCheck(L_97);
		Type_t * L_98 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_97);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_98) == ((Il2CppObject*)(Type_t *)L_99))))
		{
			goto IL_0230;
		}
	}
	{
		PropertyInfo_t * L_100 = V_9;
		Il2CppObject * L_101 = ___obj0;
		NullCheck(L_100);
		Il2CppObject * L_102 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_100, L_101, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_102);
		String_t* L_103 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_102);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_104 = JSONObject_CreateStringObject_m1387837628(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		V_12 = L_104;
		goto IL_0249;
	}

IL_0230:
	{
		PropertyInfo_t * L_105 = V_9;
		Il2CppObject * L_106 = ___obj0;
		NullCheck(L_105);
		Il2CppObject * L_107 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_105, L_106, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		NullCheck(L_107);
		String_t* L_108 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_107);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_109 = JSONObject_Create_m2628830417(NULL /*static, unused*/, L_108, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_12 = L_109;
	}

IL_0249:
	{
		JSONObject_t1971882247 * L_110 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_111 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		if (!L_111)
		{
			goto IL_02c4;
		}
	}
	{
		JSONObject_t1971882247 * L_112 = V_12;
		NullCheck(L_112);
		int32_t L_113 = L_112->get_type_5();
		if (!L_113)
		{
			goto IL_0275;
		}
	}
	{
		JSONObject_t1971882247 * L_114 = V_0;
		PropertyInfo_t * L_115 = V_9;
		NullCheck(L_115);
		String_t* L_116 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_115);
		JSONObject_t1971882247 * L_117 = V_12;
		NullCheck(L_114);
		JSONObject_AddField_m1302524246(L_114, L_116, L_117, /*hidden argument*/NULL);
		goto IL_02c4;
	}

IL_0275:
	{
		StringU5BU5D_t1642385972* L_118 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, _stringLiteral2122037725);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2122037725);
		StringU5BU5D_t1642385972* L_119 = L_118;
		PropertyInfo_t * L_120 = V_9;
		NullCheck(L_120);
		String_t* L_121 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_120);
		NullCheck(L_119);
		ArrayElementTypeCheck (L_119, L_121);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_121);
		StringU5BU5D_t1642385972* L_122 = L_119;
		NullCheck(L_122);
		ArrayElementTypeCheck (L_122, _stringLiteral1264830171);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1264830171);
		StringU5BU5D_t1642385972* L_123 = L_122;
		Il2CppObject * L_124 = ___obj0;
		NullCheck(L_124);
		Type_t * L_125 = Object_GetType_m191970594(L_124, /*hidden argument*/NULL);
		NullCheck(L_125);
		String_t* L_126 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_125);
		NullCheck(L_123);
		ArrayElementTypeCheck (L_123, L_126);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_126);
		StringU5BU5D_t1642385972* L_127 = L_123;
		NullCheck(L_127);
		ArrayElementTypeCheck (L_127, _stringLiteral2539188669);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2539188669);
		StringU5BU5D_t1642385972* L_128 = L_127;
		PropertyInfo_t * L_129 = V_9;
		NullCheck(L_129);
		Type_t * L_130 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_129);
		NullCheck(L_130);
		String_t* L_131 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_130);
		NullCheck(L_128);
		ArrayElementTypeCheck (L_128, L_131);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_131);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_132 = String_Concat_m626692867(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
	}

IL_02c4:
	{
		int32_t L_133 = V_11;
		V_11 = ((int32_t)((int32_t)L_133+(int32_t)1));
	}

IL_02ca:
	{
		int32_t L_134 = V_11;
		PropertyInfoU5BU5D_t1736152084* L_135 = V_10;
		NullCheck(L_135);
		if ((((int32_t)L_134) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_135)->max_length)))))))
		{
			goto IL_0185;
		}
	}
	{
		JSONObject_t1971882247 * L_136 = V_0;
		return L_136;
	}

IL_02d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1638831994, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_137 = JSONObject_get_nullJO_m928623965(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_137;
	}
}
// UnityEngine.Vector2 JSONTemplates::ToVector2(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern const uint32_t JSONTemplates_ToVector2_m1842675386_MetadataUsageId;
extern "C"  Vector2_t2243707579  JSONTemplates_ToVector2_m1842675386 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToVector2_m1842675386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m223257276(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m223257276(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m2513971409(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m223257276(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m223257276(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m2513971409(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		Vector2_t2243707579  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3067419446(&L_14, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// JSONObject JSONTemplates::FromVector2(UnityEngine.Vector2)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern const uint32_t JSONTemplates_FromVector2_m1095856393_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromVector2_m1095856393 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromVector2_m1095856393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___v0)->get_x_0();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___v0)->get_x_0();
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___v0)->get_y_1();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___v0)->get_y_1();
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		JSONObject_t1971882247 * L_7 = V_0;
		return L_7;
	}
}
// JSONObject JSONTemplates::FromVector3(UnityEngine.Vector3)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t JSONTemplates_FromVector3_m2598040713_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromVector3_m2598040713 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromVector3_m2598040713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___v0)->get_x_1();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___v0)->get_x_1();
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___v0)->get_y_2();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___v0)->get_y_2();
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___v0)->get_z_3();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___v0)->get_z_3();
		NullCheck(L_8);
		JSONObject_AddField_m2737597678(L_8, _stringLiteral372029400, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		JSONObject_t1971882247 * L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector3 JSONTemplates::ToVector3(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t JSONTemplates_ToVector3_m253295700_MetadataUsageId;
extern "C"  Vector3_t2243707580  JSONTemplates_ToVector3_m253295700 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToVector3_m253295700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m223257276(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m223257276(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m2513971409(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m223257276(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m223257276(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m2513971409(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m223257276(L_12, _stringLiteral372029400, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m223257276(L_15, _stringLiteral372029400, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m2513971409(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_008f;
	}

IL_008a:
	{
		G_B9_0 = (0.0f);
	}

IL_008f:
	{
		V_2 = G_B9_0;
		float L_18 = V_0;
		float L_19 = V_1;
		float L_20 = V_2;
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// JSONObject JSONTemplates::FromVector4(UnityEngine.Vector4)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t JSONTemplates_FromVector4_m482826505_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromVector4_m482826505 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromVector4_m482826505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___v0)->get_x_1();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___v0)->get_x_1();
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___v0)->get_y_2();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___v0)->get_y_2();
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___v0)->get_z_3();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___v0)->get_z_3();
		NullCheck(L_8);
		JSONObject_AddField_m2737597678(L_8, _stringLiteral372029400, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___v0)->get_w_4();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___v0)->get_w_4();
		NullCheck(L_11);
		JSONObject_AddField_m2737597678(L_11, _stringLiteral372029387, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Vector4 JSONTemplates::ToVector4(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t JSONTemplates_ToVector4_m102102150_MetadataUsageId;
extern "C"  Vector4_t2243707581  JSONTemplates_ToVector4_m102102150 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToVector4_m102102150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m223257276(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m223257276(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m2513971409(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m223257276(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m223257276(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m2513971409(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m223257276(L_12, _stringLiteral372029400, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m223257276(L_15, _stringLiteral372029400, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m2513971409(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_008f;
	}

IL_008a:
	{
		G_B9_0 = (0.0f);
	}

IL_008f:
	{
		V_2 = G_B9_0;
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m223257276(L_18, _stringLiteral372029387, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_20 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ba;
		}
	}
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m223257276(L_21, _stringLiteral372029387, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = JSONObject_get_f_m2513971409(L_22, /*hidden argument*/NULL);
		G_B12_0 = L_23;
		goto IL_00bf;
	}

IL_00ba:
	{
		G_B12_0 = (0.0f);
	}

IL_00bf:
	{
		V_3 = G_B12_0;
		float L_24 = V_0;
		float L_25 = V_1;
		float L_26 = V_2;
		float L_27 = V_3;
		Vector4_t2243707581  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector4__ctor_m1222289168(&L_28, L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// JSONObject JSONTemplates::FromMatrix4x4(UnityEngine.Matrix4x4)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104528321;
extern Il2CppCodeGenString* _stringLiteral104528322;
extern Il2CppCodeGenString* _stringLiteral104528323;
extern Il2CppCodeGenString* _stringLiteral104528324;
extern Il2CppCodeGenString* _stringLiteral2833411676;
extern Il2CppCodeGenString* _stringLiteral2833411677;
extern Il2CppCodeGenString* _stringLiteral2833411678;
extern Il2CppCodeGenString* _stringLiteral2833411679;
extern Il2CppCodeGenString* _stringLiteral3236696203;
extern Il2CppCodeGenString* _stringLiteral3236696204;
extern Il2CppCodeGenString* _stringLiteral3236696205;
extern Il2CppCodeGenString* _stringLiteral3236696206;
extern Il2CppCodeGenString* _stringLiteral1670612262;
extern Il2CppCodeGenString* _stringLiteral1670612263;
extern Il2CppCodeGenString* _stringLiteral1670612264;
extern Il2CppCodeGenString* _stringLiteral1670612265;
extern const uint32_t JSONTemplates_FromMatrix4x4_m2845632969_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromMatrix4x4_m2845632969 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromMatrix4x4_m2845632969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___m0)->get_m00_0();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___m0)->get_m00_0();
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral104528321, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___m0)->get_m01_4();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___m0)->get_m01_4();
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral104528322, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___m0)->get_m02_8();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___m0)->get_m02_8();
		NullCheck(L_8);
		JSONObject_AddField_m2737597678(L_8, _stringLiteral104528323, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___m0)->get_m03_12();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___m0)->get_m03_12();
		NullCheck(L_11);
		JSONObject_AddField_m2737597678(L_11, _stringLiteral104528324, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		float L_13 = (&___m0)->get_m10_1();
		if ((((float)L_13) == ((float)(0.0f))))
		{
			goto IL_00b5;
		}
	}
	{
		JSONObject_t1971882247 * L_14 = V_0;
		float L_15 = (&___m0)->get_m10_1();
		NullCheck(L_14);
		JSONObject_AddField_m2737597678(L_14, _stringLiteral2833411676, L_15, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		float L_16 = (&___m0)->get_m11_5();
		if ((((float)L_16) == ((float)(0.0f))))
		{
			goto IL_00d8;
		}
	}
	{
		JSONObject_t1971882247 * L_17 = V_0;
		float L_18 = (&___m0)->get_m11_5();
		NullCheck(L_17);
		JSONObject_AddField_m2737597678(L_17, _stringLiteral2833411677, L_18, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		float L_19 = (&___m0)->get_m12_9();
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00fb;
		}
	}
	{
		JSONObject_t1971882247 * L_20 = V_0;
		float L_21 = (&___m0)->get_m12_9();
		NullCheck(L_20);
		JSONObject_AddField_m2737597678(L_20, _stringLiteral2833411678, L_21, /*hidden argument*/NULL);
	}

IL_00fb:
	{
		float L_22 = (&___m0)->get_m13_13();
		if ((((float)L_22) == ((float)(0.0f))))
		{
			goto IL_011e;
		}
	}
	{
		JSONObject_t1971882247 * L_23 = V_0;
		float L_24 = (&___m0)->get_m13_13();
		NullCheck(L_23);
		JSONObject_AddField_m2737597678(L_23, _stringLiteral2833411679, L_24, /*hidden argument*/NULL);
	}

IL_011e:
	{
		float L_25 = (&___m0)->get_m20_2();
		if ((((float)L_25) == ((float)(0.0f))))
		{
			goto IL_0141;
		}
	}
	{
		JSONObject_t1971882247 * L_26 = V_0;
		float L_27 = (&___m0)->get_m20_2();
		NullCheck(L_26);
		JSONObject_AddField_m2737597678(L_26, _stringLiteral3236696203, L_27, /*hidden argument*/NULL);
	}

IL_0141:
	{
		float L_28 = (&___m0)->get_m21_6();
		if ((((float)L_28) == ((float)(0.0f))))
		{
			goto IL_0164;
		}
	}
	{
		JSONObject_t1971882247 * L_29 = V_0;
		float L_30 = (&___m0)->get_m21_6();
		NullCheck(L_29);
		JSONObject_AddField_m2737597678(L_29, _stringLiteral3236696204, L_30, /*hidden argument*/NULL);
	}

IL_0164:
	{
		float L_31 = (&___m0)->get_m22_10();
		if ((((float)L_31) == ((float)(0.0f))))
		{
			goto IL_0187;
		}
	}
	{
		JSONObject_t1971882247 * L_32 = V_0;
		float L_33 = (&___m0)->get_m22_10();
		NullCheck(L_32);
		JSONObject_AddField_m2737597678(L_32, _stringLiteral3236696205, L_33, /*hidden argument*/NULL);
	}

IL_0187:
	{
		float L_34 = (&___m0)->get_m23_14();
		if ((((float)L_34) == ((float)(0.0f))))
		{
			goto IL_01aa;
		}
	}
	{
		JSONObject_t1971882247 * L_35 = V_0;
		float L_36 = (&___m0)->get_m23_14();
		NullCheck(L_35);
		JSONObject_AddField_m2737597678(L_35, _stringLiteral3236696206, L_36, /*hidden argument*/NULL);
	}

IL_01aa:
	{
		float L_37 = (&___m0)->get_m30_3();
		if ((((float)L_37) == ((float)(0.0f))))
		{
			goto IL_01cd;
		}
	}
	{
		JSONObject_t1971882247 * L_38 = V_0;
		float L_39 = (&___m0)->get_m30_3();
		NullCheck(L_38);
		JSONObject_AddField_m2737597678(L_38, _stringLiteral1670612262, L_39, /*hidden argument*/NULL);
	}

IL_01cd:
	{
		float L_40 = (&___m0)->get_m31_7();
		if ((((float)L_40) == ((float)(0.0f))))
		{
			goto IL_01f0;
		}
	}
	{
		JSONObject_t1971882247 * L_41 = V_0;
		float L_42 = (&___m0)->get_m31_7();
		NullCheck(L_41);
		JSONObject_AddField_m2737597678(L_41, _stringLiteral1670612263, L_42, /*hidden argument*/NULL);
	}

IL_01f0:
	{
		float L_43 = (&___m0)->get_m32_11();
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_0213;
		}
	}
	{
		JSONObject_t1971882247 * L_44 = V_0;
		float L_45 = (&___m0)->get_m32_11();
		NullCheck(L_44);
		JSONObject_AddField_m2737597678(L_44, _stringLiteral1670612264, L_45, /*hidden argument*/NULL);
	}

IL_0213:
	{
		float L_46 = (&___m0)->get_m33_15();
		if ((((float)L_46) == ((float)(0.0f))))
		{
			goto IL_0236;
		}
	}
	{
		JSONObject_t1971882247 * L_47 = V_0;
		float L_48 = (&___m0)->get_m33_15();
		NullCheck(L_47);
		JSONObject_AddField_m2737597678(L_47, _stringLiteral1670612265, L_48, /*hidden argument*/NULL);
	}

IL_0236:
	{
		JSONObject_t1971882247 * L_49 = V_0;
		return L_49;
	}
}
// UnityEngine.Matrix4x4 JSONTemplates::ToMatrix4x4(JSONObject)
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104528321;
extern Il2CppCodeGenString* _stringLiteral104528322;
extern Il2CppCodeGenString* _stringLiteral104528323;
extern Il2CppCodeGenString* _stringLiteral104528324;
extern Il2CppCodeGenString* _stringLiteral2833411676;
extern Il2CppCodeGenString* _stringLiteral2833411677;
extern Il2CppCodeGenString* _stringLiteral2833411678;
extern Il2CppCodeGenString* _stringLiteral2833411679;
extern Il2CppCodeGenString* _stringLiteral3236696203;
extern Il2CppCodeGenString* _stringLiteral3236696204;
extern Il2CppCodeGenString* _stringLiteral3236696205;
extern Il2CppCodeGenString* _stringLiteral3236696206;
extern Il2CppCodeGenString* _stringLiteral1670612262;
extern Il2CppCodeGenString* _stringLiteral1670612263;
extern Il2CppCodeGenString* _stringLiteral1670612264;
extern Il2CppCodeGenString* _stringLiteral1670612265;
extern const uint32_t JSONTemplates_ToMatrix4x4_m2376919994_MetadataUsageId;
extern "C"  Matrix4x4_t2933234003  JSONTemplates_ToMatrix4x4_m2376919994 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToMatrix4x4_m2376919994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m223257276(L_0, _stringLiteral104528321, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m223257276(L_3, _stringLiteral104528321, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m2513971409(L_4, /*hidden argument*/NULL);
		(&V_0)->set_m00_0(L_5);
	}

IL_0034:
	{
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m223257276(L_6, _stringLiteral104528322, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m223257276(L_9, _stringLiteral104528322, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m2513971409(L_10, /*hidden argument*/NULL);
		(&V_0)->set_m01_4(L_11);
	}

IL_0060:
	{
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m223257276(L_12, _stringLiteral104528323, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008c;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m223257276(L_15, _stringLiteral104528323, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m2513971409(L_16, /*hidden argument*/NULL);
		(&V_0)->set_m02_8(L_17);
	}

IL_008c:
	{
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m223257276(L_18, _stringLiteral104528324, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_20 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m223257276(L_21, _stringLiteral104528324, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = JSONObject_get_f_m2513971409(L_22, /*hidden argument*/NULL);
		(&V_0)->set_m03_12(L_23);
	}

IL_00b8:
	{
		JSONObject_t1971882247 * L_24 = ___obj0;
		NullCheck(L_24);
		JSONObject_t1971882247 * L_25 = JSONObject_get_Item_m223257276(L_24, _stringLiteral2833411676, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_26 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e4;
		}
	}
	{
		JSONObject_t1971882247 * L_27 = ___obj0;
		NullCheck(L_27);
		JSONObject_t1971882247 * L_28 = JSONObject_get_Item_m223257276(L_27, _stringLiteral2833411676, /*hidden argument*/NULL);
		NullCheck(L_28);
		float L_29 = JSONObject_get_f_m2513971409(L_28, /*hidden argument*/NULL);
		(&V_0)->set_m10_1(L_29);
	}

IL_00e4:
	{
		JSONObject_t1971882247 * L_30 = ___obj0;
		NullCheck(L_30);
		JSONObject_t1971882247 * L_31 = JSONObject_get_Item_m223257276(L_30, _stringLiteral2833411677, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_32 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0110;
		}
	}
	{
		JSONObject_t1971882247 * L_33 = ___obj0;
		NullCheck(L_33);
		JSONObject_t1971882247 * L_34 = JSONObject_get_Item_m223257276(L_33, _stringLiteral2833411677, /*hidden argument*/NULL);
		NullCheck(L_34);
		float L_35 = JSONObject_get_f_m2513971409(L_34, /*hidden argument*/NULL);
		(&V_0)->set_m11_5(L_35);
	}

IL_0110:
	{
		JSONObject_t1971882247 * L_36 = ___obj0;
		NullCheck(L_36);
		JSONObject_t1971882247 * L_37 = JSONObject_get_Item_m223257276(L_36, _stringLiteral2833411678, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_38 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_013c;
		}
	}
	{
		JSONObject_t1971882247 * L_39 = ___obj0;
		NullCheck(L_39);
		JSONObject_t1971882247 * L_40 = JSONObject_get_Item_m223257276(L_39, _stringLiteral2833411678, /*hidden argument*/NULL);
		NullCheck(L_40);
		float L_41 = JSONObject_get_f_m2513971409(L_40, /*hidden argument*/NULL);
		(&V_0)->set_m12_9(L_41);
	}

IL_013c:
	{
		JSONObject_t1971882247 * L_42 = ___obj0;
		NullCheck(L_42);
		JSONObject_t1971882247 * L_43 = JSONObject_get_Item_m223257276(L_42, _stringLiteral2833411679, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_44 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0168;
		}
	}
	{
		JSONObject_t1971882247 * L_45 = ___obj0;
		NullCheck(L_45);
		JSONObject_t1971882247 * L_46 = JSONObject_get_Item_m223257276(L_45, _stringLiteral2833411679, /*hidden argument*/NULL);
		NullCheck(L_46);
		float L_47 = JSONObject_get_f_m2513971409(L_46, /*hidden argument*/NULL);
		(&V_0)->set_m13_13(L_47);
	}

IL_0168:
	{
		JSONObject_t1971882247 * L_48 = ___obj0;
		NullCheck(L_48);
		JSONObject_t1971882247 * L_49 = JSONObject_get_Item_m223257276(L_48, _stringLiteral3236696203, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_50 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0194;
		}
	}
	{
		JSONObject_t1971882247 * L_51 = ___obj0;
		NullCheck(L_51);
		JSONObject_t1971882247 * L_52 = JSONObject_get_Item_m223257276(L_51, _stringLiteral3236696203, /*hidden argument*/NULL);
		NullCheck(L_52);
		float L_53 = JSONObject_get_f_m2513971409(L_52, /*hidden argument*/NULL);
		(&V_0)->set_m20_2(L_53);
	}

IL_0194:
	{
		JSONObject_t1971882247 * L_54 = ___obj0;
		NullCheck(L_54);
		JSONObject_t1971882247 * L_55 = JSONObject_get_Item_m223257276(L_54, _stringLiteral3236696204, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_56 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01c0;
		}
	}
	{
		JSONObject_t1971882247 * L_57 = ___obj0;
		NullCheck(L_57);
		JSONObject_t1971882247 * L_58 = JSONObject_get_Item_m223257276(L_57, _stringLiteral3236696204, /*hidden argument*/NULL);
		NullCheck(L_58);
		float L_59 = JSONObject_get_f_m2513971409(L_58, /*hidden argument*/NULL);
		(&V_0)->set_m21_6(L_59);
	}

IL_01c0:
	{
		JSONObject_t1971882247 * L_60 = ___obj0;
		NullCheck(L_60);
		JSONObject_t1971882247 * L_61 = JSONObject_get_Item_m223257276(L_60, _stringLiteral3236696205, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_62 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01ec;
		}
	}
	{
		JSONObject_t1971882247 * L_63 = ___obj0;
		NullCheck(L_63);
		JSONObject_t1971882247 * L_64 = JSONObject_get_Item_m223257276(L_63, _stringLiteral3236696205, /*hidden argument*/NULL);
		NullCheck(L_64);
		float L_65 = JSONObject_get_f_m2513971409(L_64, /*hidden argument*/NULL);
		(&V_0)->set_m22_10(L_65);
	}

IL_01ec:
	{
		JSONObject_t1971882247 * L_66 = ___obj0;
		NullCheck(L_66);
		JSONObject_t1971882247 * L_67 = JSONObject_get_Item_m223257276(L_66, _stringLiteral3236696206, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_68 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0218;
		}
	}
	{
		JSONObject_t1971882247 * L_69 = ___obj0;
		NullCheck(L_69);
		JSONObject_t1971882247 * L_70 = JSONObject_get_Item_m223257276(L_69, _stringLiteral3236696206, /*hidden argument*/NULL);
		NullCheck(L_70);
		float L_71 = JSONObject_get_f_m2513971409(L_70, /*hidden argument*/NULL);
		(&V_0)->set_m23_14(L_71);
	}

IL_0218:
	{
		JSONObject_t1971882247 * L_72 = ___obj0;
		NullCheck(L_72);
		JSONObject_t1971882247 * L_73 = JSONObject_get_Item_m223257276(L_72, _stringLiteral1670612262, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_74 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0244;
		}
	}
	{
		JSONObject_t1971882247 * L_75 = ___obj0;
		NullCheck(L_75);
		JSONObject_t1971882247 * L_76 = JSONObject_get_Item_m223257276(L_75, _stringLiteral1670612262, /*hidden argument*/NULL);
		NullCheck(L_76);
		float L_77 = JSONObject_get_f_m2513971409(L_76, /*hidden argument*/NULL);
		(&V_0)->set_m30_3(L_77);
	}

IL_0244:
	{
		JSONObject_t1971882247 * L_78 = ___obj0;
		NullCheck(L_78);
		JSONObject_t1971882247 * L_79 = JSONObject_get_Item_m223257276(L_78, _stringLiteral1670612263, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_80 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_0270;
		}
	}
	{
		JSONObject_t1971882247 * L_81 = ___obj0;
		NullCheck(L_81);
		JSONObject_t1971882247 * L_82 = JSONObject_get_Item_m223257276(L_81, _stringLiteral1670612263, /*hidden argument*/NULL);
		NullCheck(L_82);
		float L_83 = JSONObject_get_f_m2513971409(L_82, /*hidden argument*/NULL);
		(&V_0)->set_m31_7(L_83);
	}

IL_0270:
	{
		JSONObject_t1971882247 * L_84 = ___obj0;
		NullCheck(L_84);
		JSONObject_t1971882247 * L_85 = JSONObject_get_Item_m223257276(L_84, _stringLiteral1670612264, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_86 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_029c;
		}
	}
	{
		JSONObject_t1971882247 * L_87 = ___obj0;
		NullCheck(L_87);
		JSONObject_t1971882247 * L_88 = JSONObject_get_Item_m223257276(L_87, _stringLiteral1670612264, /*hidden argument*/NULL);
		NullCheck(L_88);
		float L_89 = JSONObject_get_f_m2513971409(L_88, /*hidden argument*/NULL);
		(&V_0)->set_m32_11(L_89);
	}

IL_029c:
	{
		JSONObject_t1971882247 * L_90 = ___obj0;
		NullCheck(L_90);
		JSONObject_t1971882247 * L_91 = JSONObject_get_Item_m223257276(L_90, _stringLiteral1670612265, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_92 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_02c8;
		}
	}
	{
		JSONObject_t1971882247 * L_93 = ___obj0;
		NullCheck(L_93);
		JSONObject_t1971882247 * L_94 = JSONObject_get_Item_m223257276(L_93, _stringLiteral1670612265, /*hidden argument*/NULL);
		NullCheck(L_94);
		float L_95 = JSONObject_get_f_m2513971409(L_94, /*hidden argument*/NULL);
		(&V_0)->set_m33_15(L_95);
	}

IL_02c8:
	{
		Matrix4x4_t2933234003  L_96 = V_0;
		return L_96;
	}
}
// JSONObject JSONTemplates::FromQuaternion(UnityEngine.Quaternion)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t JSONTemplates_FromQuaternion_m4053724321_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromQuaternion_m4053724321 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromQuaternion_m4053724321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___q0)->get_w_3();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___q0)->get_w_3();
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral372029387, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___q0)->get_x_0();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___q0)->get_x_0();
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral372029398, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___q0)->get_y_1();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___q0)->get_y_1();
		NullCheck(L_8);
		JSONObject_AddField_m2737597678(L_8, _stringLiteral372029397, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___q0)->get_z_2();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___q0)->get_z_2();
		NullCheck(L_11);
		JSONObject_AddField_m2737597678(L_11, _stringLiteral372029400, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Quaternion JSONTemplates::ToQuaternion(JSONObject)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t JSONTemplates_ToQuaternion_m1175772296_MetadataUsageId;
extern "C"  Quaternion_t4030073918  JSONTemplates_ToQuaternion_m1175772296 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToQuaternion_m1175772296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m223257276(L_0, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_2 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_get_Item_m223257276(L_3, _stringLiteral372029398, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = JSONObject_get_f_m2513971409(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002f;
	}

IL_002a:
	{
		G_B3_0 = (0.0f);
	}

IL_002f:
	{
		V_0 = G_B3_0;
		JSONObject_t1971882247 * L_6 = ___obj0;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_7 = JSONObject_get_Item_m223257276(L_6, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_8 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_9 = ___obj0;
		NullCheck(L_9);
		JSONObject_t1971882247 * L_10 = JSONObject_get_Item_m223257276(L_9, _stringLiteral372029397, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = JSONObject_get_f_m2513971409(L_10, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		goto IL_005f;
	}

IL_005a:
	{
		G_B6_0 = (0.0f);
	}

IL_005f:
	{
		V_1 = G_B6_0;
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_get_Item_m223257276(L_12, _stringLiteral372029400, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_14 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_16 = JSONObject_get_Item_m223257276(L_15, _stringLiteral372029400, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = JSONObject_get_f_m2513971409(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_008f;
	}

IL_008a:
	{
		G_B9_0 = (0.0f);
	}

IL_008f:
	{
		V_2 = G_B9_0;
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m223257276(L_18, _stringLiteral372029387, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		bool L_20 = JSONObject_op_Implicit_m2930985192(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ba;
		}
	}
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m223257276(L_21, _stringLiteral372029387, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = JSONObject_get_f_m2513971409(L_22, /*hidden argument*/NULL);
		G_B12_0 = L_23;
		goto IL_00bf;
	}

IL_00ba:
	{
		G_B12_0 = (0.0f);
	}

IL_00bf:
	{
		V_3 = G_B12_0;
		float L_24 = V_0;
		float L_25 = V_1;
		float L_26 = V_2;
		float L_27 = V_3;
		Quaternion_t4030073918  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Quaternion__ctor_m3196903881(&L_28, L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// JSONObject JSONTemplates::FromColor(UnityEngine.Color)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern Il2CppCodeGenString* _stringLiteral372029376;
extern Il2CppCodeGenString* _stringLiteral372029373;
extern const uint32_t JSONTemplates_FromColor_m4006842377_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromColor_m4006842377 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromColor_m4006842377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&___c0)->get_r_0();
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = (&___c0)->get_r_0();
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral372029392, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = (&___c0)->get_g_1();
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = (&___c0)->get_g_1();
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral372029371, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = (&___c0)->get_b_2();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = (&___c0)->get_b_2();
		NullCheck(L_8);
		JSONObject_AddField_m2737597678(L_8, _stringLiteral372029376, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = (&___c0)->get_a_3();
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = (&___c0)->get_a_3();
		NullCheck(L_11);
		JSONObject_AddField_m2737597678(L_11, _stringLiteral372029373, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Color JSONTemplates::ToColor(JSONObject)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern Il2CppCodeGenString* _stringLiteral372029376;
extern Il2CppCodeGenString* _stringLiteral372029373;
extern const uint32_t JSONTemplates_ToColor_m2581707814_MetadataUsageId;
extern "C"  Color_t2020392075  JSONTemplates_ToColor_m2581707814 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToColor_m2581707814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		Initobj (Color_t2020392075_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_00cb;
	}

IL_000f:
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		List_1_t1398341365 * L_1 = L_0->get_keys_7();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m1112119647(L_1, L_2, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_2 = L_3;
		String_t* L_4 = V_2;
		if (!L_4)
		{
			goto IL_00c7;
		}
	}
	{
		String_t* L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral372029392, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral372029371, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral372029376, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral372029373, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00af;
		}
	}
	{
		goto IL_00c7;
	}

IL_0067:
	{
		JSONObject_t1971882247 * L_13 = ___obj0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1971882247 * L_15 = JSONObject_get_Item_m1007985851(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		float L_16 = JSONObject_get_f_m2513971409(L_15, /*hidden argument*/NULL);
		(&V_0)->set_r_0(L_16);
		goto IL_00c7;
	}

IL_007f:
	{
		JSONObject_t1971882247 * L_17 = ___obj0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m1007985851(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = JSONObject_get_f_m2513971409(L_19, /*hidden argument*/NULL);
		(&V_0)->set_g_1(L_20);
		goto IL_00c7;
	}

IL_0097:
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_23 = JSONObject_get_Item_m1007985851(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = JSONObject_get_f_m2513971409(L_23, /*hidden argument*/NULL);
		(&V_0)->set_b_2(L_24);
		goto IL_00c7;
	}

IL_00af:
	{
		JSONObject_t1971882247 * L_25 = ___obj0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JSONObject_t1971882247 * L_27 = JSONObject_get_Item_m1007985851(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		float L_28 = JSONObject_get_f_m2513971409(L_27, /*hidden argument*/NULL);
		(&V_0)->set_a_3(L_28);
		goto IL_00c7;
	}

IL_00c7:
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_30 = V_1;
		JSONObject_t1971882247 * L_31 = ___obj0;
		NullCheck(L_31);
		int32_t L_32 = JSONObject_get_Count_m3265098784(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_000f;
		}
	}
	{
		Color_t2020392075  L_33 = V_0;
		return L_33;
	}
}
// JSONObject JSONTemplates::FromLayerMask(UnityEngine.LayerMask)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t JSONTemplates_FromLayerMask_m3615216329_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromLayerMask_m3615216329 (Il2CppObject * __this /* static, unused */, LayerMask_t3188175821  ___l0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromLayerMask_m3615216329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		int32_t L_2 = LayerMask_get_value_m251765876((&___l0), /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_AddField_m1847913364(L_1, _stringLiteral1803325615, L_2, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.LayerMask JSONTemplates::ToLayerMask(JSONObject)
extern Il2CppClass* LayerMask_t3188175821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t JSONTemplates_ToLayerMask_m1695073362_MetadataUsageId;
extern "C"  LayerMask_t3188175821  JSONTemplates_ToLayerMask_m1695073362 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToLayerMask_m1695073362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LayerMask_t3188175821  V_0;
	memset(&V_0, 0, sizeof(V_0));
	LayerMask_t3188175821  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (LayerMask_t3188175821_il2cpp_TypeInfo_var, (&V_1));
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		JSONObject_t1971882247 * L_1 = JSONObject_get_Item_m223257276(L_0, _stringLiteral1803325615, /*hidden argument*/NULL);
		NullCheck(L_1);
		float L_2 = L_1->get_n_9();
		LayerMask_set_value_m3659587097((&V_1), (((int32_t)((int32_t)L_2))), /*hidden argument*/NULL);
		LayerMask_t3188175821  L_3 = V_1;
		V_0 = L_3;
		LayerMask_t3188175821  L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONTemplates::FromRect(UnityEngine.Rect)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral1113197259;
extern Il2CppCodeGenString* _stringLiteral1366542454;
extern const uint32_t JSONTemplates_FromRect_m3481700065_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromRect_m3481700065 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromRect_m3481700065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Rect_get_x_m1393582490((&___r0), /*hidden argument*/NULL);
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = Rect_get_x_m1393582490((&___r0), /*hidden argument*/NULL);
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = Rect_get_y_m1393582395((&___r0), /*hidden argument*/NULL);
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = Rect_get_y_m1393582395((&___r0), /*hidden argument*/NULL);
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral372029397, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		float L_7 = Rect_get_height_m3128694305((&___r0), /*hidden argument*/NULL);
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_006f;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		float L_9 = Rect_get_height_m3128694305((&___r0), /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONObject_AddField_m2737597678(L_8, _stringLiteral1113197259, L_9, /*hidden argument*/NULL);
	}

IL_006f:
	{
		float L_10 = Rect_get_width_m1138015702((&___r0), /*hidden argument*/NULL);
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0092;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = Rect_get_width_m1138015702((&___r0), /*hidden argument*/NULL);
		NullCheck(L_11);
		JSONObject_AddField_m2737597678(L_11, _stringLiteral1366542454, L_12, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONObject_t1971882247 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Rect JSONTemplates::ToRect(JSONObject)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral1113197259;
extern Il2CppCodeGenString* _stringLiteral1366542454;
extern const uint32_t JSONTemplates_ToRect_m1627578696_MetadataUsageId;
extern "C"  Rect_t3681755626  JSONTemplates_ToRect_m1627578696 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToRect_m1627578696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		Initobj (Rect_t3681755626_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_00cb;
	}

IL_000f:
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		List_1_t1398341365 * L_1 = L_0->get_keys_7();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = List_1_get_Item_m1112119647(L_1, L_2, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_2 = L_3;
		String_t* L_4 = V_2;
		if (!L_4)
		{
			goto IL_00c7;
		}
	}
	{
		String_t* L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral372029398, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral372029397, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral1113197259, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral1366542454, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00af;
		}
	}
	{
		goto IL_00c7;
	}

IL_0067:
	{
		JSONObject_t1971882247 * L_13 = ___obj0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1971882247 * L_15 = JSONObject_get_Item_m1007985851(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		float L_16 = JSONObject_get_f_m2513971409(L_15, /*hidden argument*/NULL);
		Rect_set_x_m3783700513((&V_0), L_16, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_007f:
	{
		JSONObject_t1971882247 * L_17 = ___obj0;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		JSONObject_t1971882247 * L_19 = JSONObject_get_Item_m1007985851(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = JSONObject_get_f_m2513971409(L_19, /*hidden argument*/NULL);
		Rect_set_y_m4294916608((&V_0), L_20, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_0097:
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		JSONObject_t1971882247 * L_23 = JSONObject_get_Item_m1007985851(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = JSONObject_get_f_m2513971409(L_23, /*hidden argument*/NULL);
		Rect_set_height_m2019122814((&V_0), L_24, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00af:
	{
		JSONObject_t1971882247 * L_25 = ___obj0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JSONObject_t1971882247 * L_27 = JSONObject_get_Item_m1007985851(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		float L_28 = JSONObject_get_f_m2513971409(L_27, /*hidden argument*/NULL);
		Rect_set_width_m1921257731((&V_0), L_28, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00c7:
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_30 = V_1;
		JSONObject_t1971882247 * L_31 = ___obj0;
		NullCheck(L_31);
		int32_t L_32 = JSONObject_get_Count_m3265098784(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_000f;
		}
	}
	{
		Rect_t3681755626  L_33 = V_0;
		return L_33;
	}
}
// JSONObject JSONTemplates::FromRectOffset(UnityEngine.RectOffset)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70907419;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral1502598707;
extern const uint32_t JSONTemplates_FromRectOffset_m4077180741_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromRectOffset_m4077180741 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromRectOffset_m4077180741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		RectOffset_t3387826427 * L_1 = ___r0;
		NullCheck(L_1);
		int32_t L_2 = RectOffset_get_bottom_m4112328858(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = V_0;
		RectOffset_t3387826427 * L_4 = ___r0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_bottom_m4112328858(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		JSONObject_AddField_m1847913364(L_3, _stringLiteral70907419, L_5, /*hidden argument*/NULL);
	}

IL_0022:
	{
		RectOffset_t3387826427 * L_6 = ___r0;
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m439065308(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		RectOffset_t3387826427 * L_9 = ___r0;
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m439065308(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONObject_AddField_m1847913364(L_8, _stringLiteral3423761043, L_10, /*hidden argument*/NULL);
	}

IL_003e:
	{
		RectOffset_t3387826427 * L_11 = ___r0;
		NullCheck(L_11);
		int32_t L_12 = RectOffset_get_right_m281378687(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005a;
		}
	}
	{
		JSONObject_t1971882247 * L_13 = V_0;
		RectOffset_t3387826427 * L_14 = ___r0;
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_right_m281378687(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		JSONObject_AddField_m1847913364(L_13, _stringLiteral109637592, L_15, /*hidden argument*/NULL);
	}

IL_005a:
	{
		RectOffset_t3387826427 * L_16 = ___r0;
		NullCheck(L_16);
		int32_t L_17 = RectOffset_get_top_m3629049358(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0076;
		}
	}
	{
		JSONObject_t1971882247 * L_18 = V_0;
		RectOffset_t3387826427 * L_19 = ___r0;
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m3629049358(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		JSONObject_AddField_m1847913364(L_18, _stringLiteral1502598707, L_20, /*hidden argument*/NULL);
	}

IL_0076:
	{
		JSONObject_t1971882247 * L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.RectOffset JSONTemplates::ToRectOffset(JSONObject)
extern Il2CppClass* RectOffset_t3387826427_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral70907419;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral1502598707;
extern const uint32_t JSONTemplates_ToRectOffset_m1320081928_MetadataUsageId;
extern "C"  RectOffset_t3387826427 * JSONTemplates_ToRectOffset_m1320081928 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToRectOffset_m1320081928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectOffset_t3387826427 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		RectOffset_t3387826427 * L_0 = (RectOffset_t3387826427 *)il2cpp_codegen_object_new(RectOffset_t3387826427_il2cpp_TypeInfo_var);
		RectOffset__ctor_m2227510254(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_00c9;
	}

IL_000d:
	{
		JSONObject_t1971882247 * L_1 = ___obj0;
		NullCheck(L_1);
		List_1_t1398341365 * L_2 = L_1->get_keys_7();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		String_t* L_4 = List_1_get_Item_m1112119647(L_2, L_3, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_2 = L_4;
		String_t* L_5 = V_2;
		if (!L_5)
		{
			goto IL_00c5;
		}
	}
	{
		String_t* L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral70907419, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3423761043, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007d;
		}
	}
	{
		String_t* L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral109637592, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral1502598707, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_00c5;
	}

IL_0065:
	{
		RectOffset_t3387826427 * L_14 = V_0;
		JSONObject_t1971882247 * L_15 = ___obj0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		JSONObject_t1971882247 * L_17 = JSONObject_get_Item_m1007985851(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		float L_18 = L_17->get_n_9();
		NullCheck(L_14);
		RectOffset_set_bottom_m4065521443(L_14, (((int32_t)((int32_t)L_18))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_007d:
	{
		RectOffset_t3387826427 * L_19 = V_0;
		JSONObject_t1971882247 * L_20 = ___obj0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		JSONObject_t1971882247 * L_22 = JSONObject_get_Item_m1007985851(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		float L_23 = L_22->get_n_9();
		NullCheck(L_19);
		RectOffset_set_left_m620681523(L_19, (((int32_t)((int32_t)L_23))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_0095:
	{
		RectOffset_t3387826427 * L_24 = V_0;
		JSONObject_t1971882247 * L_25 = ___obj0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		JSONObject_t1971882247 * L_27 = JSONObject_get_Item_m1007985851(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		float L_28 = L_27->get_n_9();
		NullCheck(L_24);
		RectOffset_set_right_m1671272302(L_24, (((int32_t)((int32_t)L_28))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00ad:
	{
		RectOffset_t3387826427 * L_29 = V_0;
		JSONObject_t1971882247 * L_30 = ___obj0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		JSONObject_t1971882247 * L_32 = JSONObject_get_Item_m1007985851(L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		float L_33 = L_32->get_n_9();
		NullCheck(L_29);
		RectOffset_set_top_m3579196427(L_29, (((int32_t)((int32_t)L_33))), /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00c5:
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c9:
	{
		int32_t L_35 = V_1;
		JSONObject_t1971882247 * L_36 = ___obj0;
		NullCheck(L_36);
		int32_t L_37 = JSONObject_get_Count_m3265098784(L_36, /*hidden argument*/NULL);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_000d;
		}
	}
	{
		RectOffset_t3387826427 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.AnimationCurve JSONTemplates::ToAnimationCurve(JSONObject)
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1879545138_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857677462;
extern Il2CppCodeGenString* _stringLiteral4185425568;
extern Il2CppCodeGenString* _stringLiteral10864839;
extern const uint32_t JSONTemplates_ToAnimationCurve_m2652802056_MetadataUsageId;
extern "C"  AnimationCurve_t3306541151 * JSONTemplates_ToAnimationCurve_m2652802056 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToAnimationCurve_m2652802056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimationCurve_t3306541151 * V_0 = NULL;
	JSONObject_t1971882247 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		AnimationCurve_t3306541151 * L_0 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m3707994114(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = ___obj0;
		NullCheck(L_1);
		bool L_2 = JSONObject_HasField_m4286787708(L_1, _stringLiteral1857677462, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		JSONObject_t1971882247 * L_3 = ___obj0;
		NullCheck(L_3);
		JSONObject_t1971882247 * L_4 = JSONObject_GetField_m2890738298(L_3, _stringLiteral1857677462, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0040;
	}

IL_0029:
	{
		AnimationCurve_t3306541151 * L_5 = V_0;
		JSONObject_t1971882247 * L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		JSONObject_t1971882247 * L_8 = JSONObject_get_Item_m1007985851(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONTemplates_t3006274921_il2cpp_TypeInfo_var);
		Keyframe_t1449471340  L_9 = JSONTemplates_ToKeyframe_m373665352(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationCurve_AddKey_m1034249733(L_5, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_2;
		JSONObject_t1971882247 * L_12 = V_1;
		NullCheck(L_12);
		List_1_t1341003379 * L_13 = L_12->get_list_6();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m1879545138(L_13, /*hidden argument*/List_1_get_Count_m1879545138_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0029;
		}
	}

IL_0051:
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		bool L_16 = JSONObject_HasField_m4286787708(L_15, _stringLiteral4185425568, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0078;
		}
	}
	{
		AnimationCurve_t3306541151 * L_17 = V_0;
		JSONObject_t1971882247 * L_18 = ___obj0;
		NullCheck(L_18);
		JSONObject_t1971882247 * L_19 = JSONObject_GetField_m2890738298(L_18, _stringLiteral4185425568, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = L_19->get_n_9();
		NullCheck(L_17);
		AnimationCurve_set_preWrapMode_m2999148321(L_17, (((int32_t)((int32_t)L_20))), /*hidden argument*/NULL);
	}

IL_0078:
	{
		JSONObject_t1971882247 * L_21 = ___obj0;
		NullCheck(L_21);
		bool L_22 = JSONObject_HasField_m4286787708(L_21, _stringLiteral10864839, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009f;
		}
	}
	{
		AnimationCurve_t3306541151 * L_23 = V_0;
		JSONObject_t1971882247 * L_24 = ___obj0;
		NullCheck(L_24);
		JSONObject_t1971882247 * L_25 = JSONObject_GetField_m2890738298(L_24, _stringLiteral10864839, /*hidden argument*/NULL);
		NullCheck(L_25);
		float L_26 = L_25->get_n_9();
		NullCheck(L_23);
		AnimationCurve_set_postWrapMode_m262982620(L_23, (((int32_t)((int32_t)L_26))), /*hidden argument*/NULL);
	}

IL_009f:
	{
		AnimationCurve_t3306541151 * L_27 = V_0;
		return L_27;
	}
}
// JSONObject JSONTemplates::FromAnimationCurve(UnityEngine.AnimationCurve)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapMode_t255797857_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4185425568;
extern Il2CppCodeGenString* _stringLiteral10864839;
extern Il2CppCodeGenString* _stringLiteral1857677462;
extern const uint32_t JSONTemplates_FromAnimationCurve_m132016741_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromAnimationCurve_m132016741 (Il2CppObject * __this /* static, unused */, AnimationCurve_t3306541151 * ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromAnimationCurve_m132016741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	JSONObject_t1971882247 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1971882247 * L_1 = V_0;
		AnimationCurve_t3306541151 * L_2 = ___a0;
		NullCheck(L_2);
		int32_t L_3 = AnimationCurve_get_preWrapMode_m1489197552(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppObject * L_4 = Box(WrapMode_t255797857_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_1);
		JSONObject_AddField_m1305136679(L_1, _stringLiteral4185425568, L_5, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_6 = V_0;
		AnimationCurve_t3306541151 * L_7 = ___a0;
		NullCheck(L_7);
		int32_t L_8 = AnimationCurve_get_postWrapMode_m1346218857(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Il2CppObject * L_9 = Box(WrapMode_t255797857_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		NullCheck(L_6);
		JSONObject_AddField_m1305136679(L_6, _stringLiteral10864839, L_10, /*hidden argument*/NULL);
		AnimationCurve_t3306541151 * L_11 = ___a0;
		NullCheck(L_11);
		KeyframeU5BU5D_t449065829* L_12 = AnimationCurve_get_keys_m162753017(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_13 = JSONObject_Create_m3929211220(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_13;
		V_4 = 0;
		goto IL_0083;
	}

IL_0060:
	{
		JSONObject_t1971882247 * L_14 = V_3;
		AnimationCurve_t3306541151 * L_15 = ___a0;
		NullCheck(L_15);
		KeyframeU5BU5D_t449065829* L_16 = AnimationCurve_get_keys_m162753017(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_4;
		NullCheck(L_16);
		IL2CPP_RUNTIME_CLASS_INIT(JSONTemplates_t3006274921_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_18 = JSONTemplates_FromKeyframe_m1091070949(NULL /*static, unused*/, (*(Keyframe_t1449471340 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))), /*hidden argument*/NULL);
		NullCheck(L_14);
		JSONObject_Add_m2474874762(L_14, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_4;
		V_4 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_4;
		AnimationCurve_t3306541151 * L_21 = ___a0;
		NullCheck(L_21);
		KeyframeU5BU5D_t449065829* L_22 = AnimationCurve_get_keys_m162753017(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		JSONObject_t1971882247 * L_23 = V_0;
		JSONObject_t1971882247 * L_24 = V_3;
		NullCheck(L_23);
		JSONObject_AddField_m1302524246(L_23, _stringLiteral1857677462, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		JSONObject_t1971882247 * L_25 = V_0;
		return L_25;
	}
}
// UnityEngine.Keyframe JSONTemplates::ToKeyframe(JSONObject)
extern Il2CppCodeGenString* _stringLiteral3457519049;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral2674127958;
extern Il2CppCodeGenString* _stringLiteral3634190493;
extern Il2CppCodeGenString* _stringLiteral602706040;
extern const uint32_t JSONTemplates_ToKeyframe_m373665352_MetadataUsageId;
extern "C"  Keyframe_t1449471340  JSONTemplates_ToKeyframe_m373665352 (Il2CppObject * __this /* static, unused */, JSONObject_t1971882247 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_ToKeyframe_m373665352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Keyframe_t1449471340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Keyframe_t1449471340 * G_B2_0 = NULL;
	Keyframe_t1449471340 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Keyframe_t1449471340 * G_B3_1 = NULL;
	float G_B5_0 = 0.0f;
	Keyframe_t1449471340 * G_B5_1 = NULL;
	float G_B4_0 = 0.0f;
	Keyframe_t1449471340 * G_B4_1 = NULL;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	Keyframe_t1449471340 * G_B6_2 = NULL;
	{
		JSONObject_t1971882247 * L_0 = ___obj0;
		NullCheck(L_0);
		bool L_1 = JSONObject_HasField_m4286787708(L_0, _stringLiteral3457519049, /*hidden argument*/NULL);
		G_B1_0 = (&V_0);
		if (!L_1)
		{
			G_B2_0 = (&V_0);
			goto IL_0027;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = ___obj0;
		NullCheck(L_2);
		JSONObject_t1971882247 * L_3 = JSONObject_GetField_m2890738298(L_2, _stringLiteral3457519049, /*hidden argument*/NULL);
		NullCheck(L_3);
		float L_4 = L_3->get_n_9();
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_0027:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		JSONObject_t1971882247 * L_5 = ___obj0;
		NullCheck(L_5);
		bool L_6 = JSONObject_HasField_m4286787708(L_5, _stringLiteral1803325615, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		if (!L_6)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			goto IL_0051;
		}
	}
	{
		JSONObject_t1971882247 * L_7 = ___obj0;
		NullCheck(L_7);
		JSONObject_t1971882247 * L_8 = JSONObject_GetField_m2890738298(L_7, _stringLiteral1803325615, /*hidden argument*/NULL);
		NullCheck(L_8);
		float L_9 = L_8->get_n_9();
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0056;
	}

IL_0051:
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0056:
	{
		Keyframe__ctor_m2042404667(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		JSONObject_t1971882247 * L_10 = ___obj0;
		NullCheck(L_10);
		bool L_11 = JSONObject_HasField_m4286787708(L_10, _stringLiteral2674127958, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0082;
		}
	}
	{
		JSONObject_t1971882247 * L_12 = ___obj0;
		NullCheck(L_12);
		JSONObject_t1971882247 * L_13 = JSONObject_GetField_m2890738298(L_12, _stringLiteral2674127958, /*hidden argument*/NULL);
		NullCheck(L_13);
		float L_14 = L_13->get_n_9();
		Keyframe_set_inTangent_m4280114775((&V_0), L_14, /*hidden argument*/NULL);
	}

IL_0082:
	{
		JSONObject_t1971882247 * L_15 = ___obj0;
		NullCheck(L_15);
		bool L_16 = JSONObject_HasField_m4286787708(L_15, _stringLiteral3634190493, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a9;
		}
	}
	{
		JSONObject_t1971882247 * L_17 = ___obj0;
		NullCheck(L_17);
		JSONObject_t1971882247 * L_18 = JSONObject_GetField_m2890738298(L_17, _stringLiteral3634190493, /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_19 = L_18->get_n_9();
		Keyframe_set_outTangent_m1054927214((&V_0), L_19, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		JSONObject_t1971882247 * L_20 = ___obj0;
		NullCheck(L_20);
		bool L_21 = JSONObject_HasField_m4286787708(L_20, _stringLiteral602706040, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d1;
		}
	}
	{
		JSONObject_t1971882247 * L_22 = ___obj0;
		NullCheck(L_22);
		JSONObject_t1971882247 * L_23 = JSONObject_GetField_m2890738298(L_22, _stringLiteral602706040, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = L_23->get_n_9();
		Keyframe_set_tangentMode_m1073266123((&V_0), (((int32_t)((int32_t)L_24))), /*hidden argument*/NULL);
	}

IL_00d1:
	{
		Keyframe_t1449471340  L_25 = V_0;
		return L_25;
	}
}
// JSONObject JSONTemplates::FromKeyframe(UnityEngine.Keyframe)
extern Il2CppClass* JSONObject_t1971882247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2674127958;
extern Il2CppCodeGenString* _stringLiteral3634190493;
extern Il2CppCodeGenString* _stringLiteral602706040;
extern Il2CppCodeGenString* _stringLiteral3457519049;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t JSONTemplates_FromKeyframe_m1091070949_MetadataUsageId;
extern "C"  JSONObject_t1971882247 * JSONTemplates_FromKeyframe_m1091070949 (Il2CppObject * __this /* static, unused */, Keyframe_t1449471340  ___k0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates_FromKeyframe_m1091070949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t1971882247 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1971882247_il2cpp_TypeInfo_var);
		JSONObject_t1971882247 * L_0 = JSONObject_get_obj_m1428033972(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Keyframe_get_inTangent_m3256944616((&___k0), /*hidden argument*/NULL);
		if ((((float)L_1) == ((float)(0.0f))))
		{
			goto IL_0029;
		}
	}
	{
		JSONObject_t1971882247 * L_2 = V_0;
		float L_3 = Keyframe_get_inTangent_m3256944616((&___k0), /*hidden argument*/NULL);
		NullCheck(L_2);
		JSONObject_AddField_m2737597678(L_2, _stringLiteral2674127958, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = Keyframe_get_outTangent_m1894374085((&___k0), /*hidden argument*/NULL);
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t1971882247 * L_5 = V_0;
		float L_6 = Keyframe_get_outTangent_m1894374085((&___k0), /*hidden argument*/NULL);
		NullCheck(L_5);
		JSONObject_AddField_m2737597678(L_5, _stringLiteral3634190493, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		int32_t L_7 = Keyframe_get_tangentMode_m1869200796((&___k0), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		JSONObject_t1971882247 * L_8 = V_0;
		int32_t L_9 = Keyframe_get_tangentMode_m1869200796((&___k0), /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONObject_AddField_m1847913364(L_8, _stringLiteral602706040, L_9, /*hidden argument*/NULL);
	}

IL_006a:
	{
		float L_10 = Keyframe_get_time_m2226372497((&___k0), /*hidden argument*/NULL);
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_008d;
		}
	}
	{
		JSONObject_t1971882247 * L_11 = V_0;
		float L_12 = Keyframe_get_time_m2226372497((&___k0), /*hidden argument*/NULL);
		NullCheck(L_11);
		JSONObject_AddField_m2737597678(L_11, _stringLiteral3457519049, L_12, /*hidden argument*/NULL);
	}

IL_008d:
	{
		float L_13 = Keyframe_get_value_m979894315((&___k0), /*hidden argument*/NULL);
		if ((((float)L_13) == ((float)(0.0f))))
		{
			goto IL_00b0;
		}
	}
	{
		JSONObject_t1971882247 * L_14 = V_0;
		float L_15 = Keyframe_get_value_m979894315((&___k0), /*hidden argument*/NULL);
		NullCheck(L_14);
		JSONObject_AddField_m2737597678(L_14, _stringLiteral1803325615, L_15, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		JSONObject_t1971882247 * L_16 = V_0;
		return L_16;
	}
}
// System.Void JSONTemplates::.cctor()
extern Il2CppClass* HashSet_1_t1022910149_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONTemplates_t3006274921_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m2041686063_MethodInfo_var;
extern const uint32_t JSONTemplates__cctor_m3765930825_MetadataUsageId;
extern "C"  void JSONTemplates__cctor_m3765930825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONTemplates__cctor_m3765930825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t1022910149 * L_0 = (HashSet_1_t1022910149 *)il2cpp_codegen_object_new(HashSet_1_t1022910149_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m2041686063(L_0, /*hidden argument*/HashSet_1__ctor_m2041686063_MethodInfo_var);
		((JSONTemplates_t3006274921_StaticFields*)JSONTemplates_t3006274921_il2cpp_TypeInfo_var->static_fields)->set_touched_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
