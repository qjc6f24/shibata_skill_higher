﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5
struct U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::.ctor()
extern "C"  void U3CreadMaskingKeyAsyncU3Ec__AnonStorey5__ctor_m135055404 (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::<>m__0(System.Byte[])
extern "C"  void U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3284532258 (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
