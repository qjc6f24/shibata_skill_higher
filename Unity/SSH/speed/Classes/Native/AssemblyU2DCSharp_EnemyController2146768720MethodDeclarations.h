﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyController
struct EnemyController_t2146768720;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyController::.ctor()
extern "C"  void EnemyController__ctor_m1153179309 (EnemyController_t2146768720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyController::Start()
extern "C"  void EnemyController_Start_m2470974037 (EnemyController_t2146768720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyController::moveTimer()
extern "C"  void EnemyController_moveTimer_m3204164943 (EnemyController_t2146768720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single EnemyController::getTimer()
extern "C"  float EnemyController_getTimer_m3600434872 (EnemyController_t2146768720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyController::resetTimer(System.Single)
extern "C"  void EnemyController_resetTimer_m3737201724 (EnemyController_t2146768720 * __this, float ___resetTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
