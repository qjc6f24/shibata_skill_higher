﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1
struct U3CGetObjectDataU3Ec__AnonStorey1_t2338822889;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::.ctor()
extern "C"  void U3CGetObjectDataU3Ec__AnonStorey1__ctor_m4120423482 (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::<>m__0(System.Int32)
extern "C"  void U3CGetObjectDataU3Ec__AnonStorey1_U3CU3Em__0_m882231296 (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
