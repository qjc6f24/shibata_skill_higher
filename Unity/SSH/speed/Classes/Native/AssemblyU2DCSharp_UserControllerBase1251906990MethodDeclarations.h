﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserControllerBase
struct UserControllerBase_t1251906990;

#include "codegen/il2cpp-codegen.h"

// System.Void UserControllerBase::.ctor()
extern "C"  void UserControllerBase__ctor_m345467335 (UserControllerBase_t1251906990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UserControllerBase::getSelectingCardIndex()
extern "C"  int32_t UserControllerBase_getSelectingCardIndex_m2205353237 (UserControllerBase_t1251906990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserControllerBase::setSelectingCardIndex(System.Int32)
extern "C"  void UserControllerBase_setSelectingCardIndex_m1941113010 (UserControllerBase_t1251906990 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UserControllerBase::getUserCard(System.Int32)
extern "C"  int32_t UserControllerBase_getUserCard_m352162031 (UserControllerBase_t1251906990 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserControllerBase::setUserCard(System.Int32,System.Int32)
extern "C"  void UserControllerBase_setUserCard_m1357951436 (UserControllerBase_t1251906990 * __this, int32_t ___index0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UserControllerBase::getUserDeckCount()
extern "C"  int32_t UserControllerBase_getUserDeckCount_m2706233554 (UserControllerBase_t1251906990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserControllerBase::decreaseUserDeckCount()
extern "C"  void UserControllerBase_decreaseUserDeckCount_m1860642640 (UserControllerBase_t1251906990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
