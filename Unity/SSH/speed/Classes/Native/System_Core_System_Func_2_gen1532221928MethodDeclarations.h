﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<System.String[],WebSocketSharp.HttpResponse>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m375740568(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1532221928 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.String[],WebSocketSharp.HttpResponse>::Invoke(T)
#define Func_2_Invoke_m1669657382(__this, ___arg10, method) ((  HttpResponse_t2820540315 * (*) (Func_2_t1532221928 *, StringU5BU5D_t1642385972*, const MethodInfo*))Func_2_Invoke_m3288232740_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.String[],WebSocketSharp.HttpResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1148339305(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1532221928 *, StringU5BU5D_t1642385972*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.String[],WebSocketSharp.HttpResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2208631536(__this, ___result0, method) ((  HttpResponse_t2820540315 * (*) (Func_2_t1532221928 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
