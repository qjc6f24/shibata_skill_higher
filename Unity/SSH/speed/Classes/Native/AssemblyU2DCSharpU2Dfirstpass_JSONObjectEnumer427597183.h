﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSONObject
struct JSONObject_t1971882247;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObjectEnumer
struct  JSONObjectEnumer_t427597183  : public Il2CppObject
{
public:
	// JSONObject JSONObjectEnumer::_jobj
	JSONObject_t1971882247 * ____jobj_0;
	// System.Int32 JSONObjectEnumer::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of__jobj_0() { return static_cast<int32_t>(offsetof(JSONObjectEnumer_t427597183, ____jobj_0)); }
	inline JSONObject_t1971882247 * get__jobj_0() const { return ____jobj_0; }
	inline JSONObject_t1971882247 ** get_address_of__jobj_0() { return &____jobj_0; }
	inline void set__jobj_0(JSONObject_t1971882247 * value)
	{
		____jobj_0 = value;
		Il2CppCodeGenWriteBarrier(&____jobj_0, value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(JSONObjectEnumer_t427597183, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
