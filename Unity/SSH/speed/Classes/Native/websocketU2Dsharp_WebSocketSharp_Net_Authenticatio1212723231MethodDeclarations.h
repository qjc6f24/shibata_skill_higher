﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.AuthenticationResponse
struct AuthenticationResponse_t1212723231;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t3911206805;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1146723439;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_NetworkCreden3911206805.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_Authenticatio1146723439.h"
#include "websocketU2Dsharp_WebSocketSharp_Net_AuthenticationS29593226.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "mscorlib_System_String2029220233.h"

// System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.NetworkCredential)
extern "C"  void AuthenticationResponse__ctor_m1022749245 (AuthenticationResponse_t1212723231 * __this, NetworkCredential_t3911206805 * ___credentials0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationChallenge,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern "C"  void AuthenticationResponse__ctor_m3805970484 (AuthenticationResponse_t1212723231 * __this, AuthenticationChallenge_t1146723439 * ___challenge0, NetworkCredential_t3911206805 * ___credentials1, uint32_t ___nonceCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern "C"  void AuthenticationResponse__ctor_m705882896 (AuthenticationResponse_t1212723231 * __this, int32_t ___scheme0, NameValueCollection_t3047564564 * ___parameters1, NetworkCredential_t3911206805 * ___credentials2, uint32_t ___nonceCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 WebSocketSharp.Net.AuthenticationResponse::get_NonceCount()
extern "C"  uint32_t AuthenticationResponse_get_NonceCount_m2749455448 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern "C"  String_t* AuthenticationResponse_createA1_m953246629 (Il2CppObject * __this /* static, unused */, String_t* ___username0, String_t* ___password1, String_t* ___realm2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern "C"  String_t* AuthenticationResponse_createA1_m811929829 (Il2CppObject * __this /* static, unused */, String_t* ___username0, String_t* ___password1, String_t* ___realm2, String_t* ___nonce3, String_t* ___cnonce4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String)
extern "C"  String_t* AuthenticationResponse_createA2_m3596400622 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___uri1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern "C"  String_t* AuthenticationResponse_createA2_m3539481730 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___uri1, String_t* ___entity2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::hash(System.String)
extern "C"  String_t* AuthenticationResponse_hash_m2875148591 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.AuthenticationResponse::initAsDigest()
extern "C"  void AuthenticationResponse_initAsDigest_m686452706 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern "C"  String_t* AuthenticationResponse_CreateRequestDigest_m1063439045 (Il2CppObject * __this /* static, unused */, NameValueCollection_t3047564564 * ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::ToBasicString()
extern "C"  String_t* AuthenticationResponse_ToBasicString_m679739463 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.AuthenticationResponse::ToDigestString()
extern "C"  String_t* AuthenticationResponse_ToDigestString_m2983562877 (AuthenticationResponse_t1212723231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.AuthenticationResponse::<initAsDigest>m__0(System.String)
extern "C"  bool AuthenticationResponse_U3CinitAsDigestU3Em__0_m1328643795 (Il2CppObject * __this /* static, unused */, String_t* ___qop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
