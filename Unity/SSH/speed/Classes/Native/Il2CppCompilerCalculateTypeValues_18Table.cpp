﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONTemplates3006274921.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_BattleController3050984398.h"
#include "AssemblyU2DCSharp_Card34057406.h"
#include "AssemblyU2DCSharp_CardController2595996862.h"
#include "AssemblyU2DCSharp_EnemyController2146768720.h"
#include "AssemblyU2DCSharp_Pk2738205061.h"
#include "AssemblyU2DCSharp_PlayerController4148409433.h"
#include "AssemblyU2DCSharp_TimeController1641888515.h"
#include "AssemblyU2DCSharp_TimeText2509642322.h"
#include "AssemblyU2DCSharp_UserControllerBase1251906990.h"
#include "AssemblyU2DCSharp_WebSocketClient3274474108.h"
#include "AssemblyU2DCSharp_ItemMaster335070819.h"
#include "AssemblyU2DCSharp_ItemMaster_Item1836005089.h"
#include "AssemblyU2DCSharp_MasterBase2623394359.h"
#include "AssemblyU2DCSharp_MasterBase_Base3850954593.h"
#include "AssemblyU2DCSharp_PanelController24792858.h"
#include "AssemblyU2DCSharp_TestController674116116.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (JSONTemplates_t3006274921), -1, sizeof(JSONTemplates_t3006274921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[1] = 
{
	JSONTemplates_t3006274921_StaticFields::get_offset_of_touched_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (BattleController_t3050984398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[3] = 
{
	BattleController_t3050984398::get_offset_of__enemyController_2(),
	BattleController_t3050984398::get_offset_of__cardController_3(),
	BattleController_t3050984398::get_offset_of__timeController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Card_t34057406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[8] = 
{
	Card_t34057406::get_offset_of_ownTargetNum_2(),
	Card_t34057406::get_offset_of_ownIndex_3(),
	Card_t34057406::get_offset_of_ownNum_4(),
	Card_t34057406::get_offset_of_cursor_5(),
	Card_t34057406::get_offset_of_cursorObject_6(),
	Card_t34057406::get_offset_of__playerController_7(),
	Card_t34057406::get_offset_of__cardController_8(),
	Card_t34057406::get_offset_of__battleController_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (CardController_t2595996862), -1, sizeof(CardController_t2595996862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1806[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	CardController_t2595996862::get_offset_of__topLeft_8(),
	CardController_t2595996862::get_offset_of__bottomRight_9(),
	CardController_t2595996862::get_offset_of__screenWidth_10(),
	CardController_t2595996862::get_offset_of__screenHeight_11(),
	CardController_t2595996862::get_offset_of__centerPos_12(),
	CardController_t2595996862::get_offset_of_mainCamera_13(),
	CardController_t2595996862::get_offset_of__playerController_14(),
	CardController_t2595996862::get_offset_of__webSocketClient_15(),
	CardController_t2595996862_StaticFields::get_offset_of_CARD_TYPE_COUNT_16(),
	CardController_t2595996862_StaticFields::get_offset_of_USERS_CARD_COUNT_17(),
	CardController_t2595996862_StaticFields::get_offset_of_FIELD_CARD_COUNT_18(),
	CardController_t2595996862::get_offset_of_cards_19(),
	CardController_t2595996862_StaticFields::get_offset_of__fieldObjects_20(),
	CardController_t2595996862_StaticFields::get_offset_of__playerObjects_21(),
	CardController_t2595996862_StaticFields::get_offset_of__enemyObjects1_22(),
	CardController_t2595996862_StaticFields::get_offset_of__enemyObjects2_23(),
	CardController_t2595996862_StaticFields::get_offset_of__enemyObjects3_24(),
	CardController_t2595996862::get_offset_of__objects_25(),
	CardController_t2595996862_StaticFields::get_offset_of_PHASE_INIT_26(),
	CardController_t2595996862_StaticFields::get_offset_of_PHASE_PLAYING_27(),
	CardController_t2595996862::get_offset_of__phase_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (EnemyController_t2146768720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	EnemyController_t2146768720::get_offset_of__timer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (Pk_t2738205061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (PlayerController_t4148409433), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (TimeController_t1641888515), -1, sizeof(TimeController_t1641888515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1810[7] = 
{
	TimeController_t1641888515_StaticFields::get_offset_of_BATTLE_START_INTERVAL_TIME_2(),
	TimeController_t1641888515_StaticFields::get_offset_of_BATTLE_TIME_3(),
	TimeController_t1641888515::get_offset_of__battleStartIntervalTimer_4(),
	TimeController_t1641888515::get_offset_of__battleTimer_5(),
	TimeController_t1641888515::get_offset_of__isBattleStart_6(),
	TimeController_t1641888515::get_offset_of__isBattleEnd_7(),
	TimeController_t1641888515::get_offset_of__timeText_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (TimeText_t2509642322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (UserControllerBase_t1251906990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[3] = 
{
	UserControllerBase_t1251906990::get_offset_of__selectingCardIndex_2(),
	UserControllerBase_t1251906990::get_offset_of__userCards_3(),
	UserControllerBase_t1251906990::get_offset_of__deckCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (WebSocketClient_t3274474108), -1, sizeof(WebSocketClient_t3274474108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[23] = 
{
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PORT_NUM_7(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_IP_ADDRESS_8(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_START_9(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_CREATE_USER_10(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_WAIT_CREATE_USER_11(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_LOGIN_12(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_WAIT_ROOM_FILL_13(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_CHANGE_SCENE_14(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_GAME_INIT_15(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_GAME_PLAYING_16(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_PHASE_END_17(),
	WebSocketClient_t3274474108::get_offset_of__phase_18(),
	WebSocketClient_t3274474108::get_offset_of__userId_19(),
	WebSocketClient_t3274474108::get_offset_of__roomId_20(),
	WebSocketClient_t3274474108::get_offset_of__playerNum_21(),
	WebSocketClient_t3274474108::get_offset_of__isConnecting_22(),
	WebSocketClient_t3274474108::get_offset_of_isChangedField_23(),
	WebSocketClient_t3274474108::get_offset_of__fieldCardNums_24(),
	WebSocketClient_t3274474108::get_offset_of__playerCardNums_25(),
	WebSocketClient_t3274474108::get_offset_of__playerUserIds_26(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of__ws_27(),
	WebSocketClient_t3274474108::get_offset_of__cardController_28(),
	WebSocketClient_t3274474108_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (ItemMaster_t335070819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	ItemMaster_t335070819::get_offset_of_itemMaster_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (Item_t1836005089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[3] = 
{
	Item_t1836005089::get_offset_of_id_0(),
	Item_t1836005089::get_offset_of_name_1(),
	Item_t1836005089::get_offset_of_description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (MasterBase_t2623394359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[2] = 
{
	MasterBase_t2623394359::get_offset_of_json_2(),
	MasterBase_t2623394359::get_offset_of_baseMaster_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (Base_t3850954593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[2] = 
{
	Base_t3850954593::get_offset_of_id_0(),
	Base_t3850954593::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (PanelController_t24792858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (TestController_t674116116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[7] = 
{
	0,
	0,
	0,
	0,
	TestController_t674116116::get_offset_of__timer_6(),
	TestController_t674116116::get_offset_of__playerHp_7(),
	TestController_t674116116::get_offset_of__bossHp_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
