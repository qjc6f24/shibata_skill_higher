﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t3664690781  : public Il2CppObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$locvar0
	ByteU5BU5D_t3397334013* ___U24locvar0_0;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::<b>__1
	uint8_t ___U3CbU3E__1_2;
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$this
	PayloadData_t3839327312 * ___U24this_3;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$current
	uint8_t ___U24current_4;
	// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24locvar0_0)); }
	inline ByteU5BU5D_t3397334013* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(ByteU5BU5D_t3397334013* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U3CbU3E__1_2)); }
	inline uint8_t get_U3CbU3E__1_2() const { return ___U3CbU3E__1_2; }
	inline uint8_t* get_address_of_U3CbU3E__1_2() { return &___U3CbU3E__1_2; }
	inline void set_U3CbU3E__1_2(uint8_t value)
	{
		___U3CbU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24this_3)); }
	inline PayloadData_t3839327312 * get_U24this_3() const { return ___U24this_3; }
	inline PayloadData_t3839327312 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PayloadData_t3839327312 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
