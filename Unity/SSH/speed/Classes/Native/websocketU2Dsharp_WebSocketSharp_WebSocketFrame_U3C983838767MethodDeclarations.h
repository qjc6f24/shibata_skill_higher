﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6
struct U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::.ctor()
extern "C"  void U3CreadPayloadDataAsyncU3Ec__AnonStorey6__ctor_m4228040884 (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::<>m__0(System.Byte[])
extern "C"  void U3CreadPayloadDataAsyncU3Ec__AnonStorey6_U3CU3Em__0_m928418714 (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
