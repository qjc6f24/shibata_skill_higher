﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pk
struct Pk_t2738205061;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t1971882247;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"

// System.Void Pk::.ctor()
extern "C"  void Pk__ctor_m1304747000 (Pk_t2738205061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pk::getInt(System.String,JSONObject)
extern "C"  int32_t Pk_getInt_m530972138 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pk::getStr(System.String,JSONObject)
extern "C"  String_t* Pk_getStr_m3051024009 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Pk::getLong(System.String,JSONObject)
extern "C"  int64_t Pk_getLong_m3967212028 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pk::getFloat(System.String,JSONObject)
extern "C"  float Pk_getFloat_m3540153931 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pk::getBool(System.String,JSONObject)
extern "C"  bool Pk_getBool_m3675005787 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Pk::getIntArr(System.String,JSONObject)
extern "C"  Int32U5BU5D_t3030399641* Pk_getIntArr_m37989831 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[][] Pk::getIntArrArr(System.String,JSONObject)
extern "C"  Int32U5BU5DU5BU5D_t3750818532* Pk_getIntArrArr_m3681112100 (Pk_t2738205061 * __this, String_t* ___key0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pk::debugLog(System.Int32[])
extern "C"  void Pk_debugLog_m1569006240 (Pk_t2738205061 * __this, Int32U5BU5D_t3030399641* ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pk::debugLogAA(System.Int32[][])
extern "C"  void Pk_debugLogAA_m2923645430 (Pk_t2738205061 * __this, Int32U5BU5DU5BU5D_t3750818532* ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
