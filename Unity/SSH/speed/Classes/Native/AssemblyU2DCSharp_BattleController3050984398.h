﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// EnemyController
struct EnemyController_t2146768720;
// CardController
struct CardController_t2595996862;
// TimeController
struct TimeController_t1641888515;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleController
struct  BattleController_t3050984398  : public MonoBehaviour_t1158329972
{
public:
	// EnemyController BattleController::_enemyController
	EnemyController_t2146768720 * ____enemyController_2;
	// CardController BattleController::_cardController
	CardController_t2595996862 * ____cardController_3;
	// TimeController BattleController::_timeController
	TimeController_t1641888515 * ____timeController_4;

public:
	inline static int32_t get_offset_of__enemyController_2() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ____enemyController_2)); }
	inline EnemyController_t2146768720 * get__enemyController_2() const { return ____enemyController_2; }
	inline EnemyController_t2146768720 ** get_address_of__enemyController_2() { return &____enemyController_2; }
	inline void set__enemyController_2(EnemyController_t2146768720 * value)
	{
		____enemyController_2 = value;
		Il2CppCodeGenWriteBarrier(&____enemyController_2, value);
	}

	inline static int32_t get_offset_of__cardController_3() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ____cardController_3)); }
	inline CardController_t2595996862 * get__cardController_3() const { return ____cardController_3; }
	inline CardController_t2595996862 ** get_address_of__cardController_3() { return &____cardController_3; }
	inline void set__cardController_3(CardController_t2595996862 * value)
	{
		____cardController_3 = value;
		Il2CppCodeGenWriteBarrier(&____cardController_3, value);
	}

	inline static int32_t get_offset_of__timeController_4() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ____timeController_4)); }
	inline TimeController_t1641888515 * get__timeController_4() const { return ____timeController_4; }
	inline TimeController_t1641888515 ** get_address_of__timeController_4() { return &____timeController_4; }
	inline void set__timeController_4(TimeController_t1641888515 * value)
	{
		____timeController_4 = value;
		Il2CppCodeGenWriteBarrier(&____timeController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
