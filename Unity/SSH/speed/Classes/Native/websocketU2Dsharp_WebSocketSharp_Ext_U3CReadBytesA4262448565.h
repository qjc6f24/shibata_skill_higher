﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5
struct  U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565  : public Il2CppObject
{
public:
	// System.Int64 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::len
	int64_t ___len_0;
	// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::<>f__ref$4
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565, ___len_0)); }
	inline int64_t get_len_0() const { return ___len_0; }
	inline int64_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int64_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565, ___U3CU3Ef__refU244_1)); }
	inline U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU244_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
