﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct U3CdumpU3Ec__AnonStorey1_t3897361314;
// System.Action`4<System.String,System.String,System.String,System.String>
struct Action_4_t3853297115;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey1__ctor_m284381467 (U3CdumpU3Ec__AnonStorey1_t3897361314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::<>m__0()
extern "C"  Action_4_t3853297115 * U3CdumpU3Ec__AnonStorey1_U3CU3Em__0_m3543383872 (U3CdumpU3Ec__AnonStorey1_t3897361314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
