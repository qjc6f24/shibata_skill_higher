﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/AddJSONContents
struct AddJSONContents_t3850664647;
// System.Object
struct Il2CppObject;
// JSONObject
struct JSONObject_t1971882247;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void JSONObject/AddJSONContents::.ctor(System.Object,System.IntPtr)
extern "C"  void AddJSONContents__ctor_m485378606 (AddJSONContents_t3850664647 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/AddJSONContents::Invoke(JSONObject)
extern "C"  void AddJSONContents_Invoke_m412080169 (AddJSONContents_t3850664647 * __this, JSONObject_t1971882247 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSONObject/AddJSONContents::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddJSONContents_BeginInvoke_m4152843006 (AddJSONContents_t3850664647 * __this, JSONObject_t1971882247 * ___self0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/AddJSONContents::EndInvoke(System.IAsyncResult)
extern "C"  void AddJSONContents_EndInvoke_m2540087684 (AddJSONContents_t3850664647 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
