﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4
struct U3CstartReceivingU3Ec__AnonStorey4_t506990866;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t764750278;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "websocketU2Dsharp_WebSocketSharp_WebSocketFrame764750278.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::.ctor()
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4__ctor_m3102331381 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::<>m__0()
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__0_m231510862 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::<>m__1(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__1_m2573079210 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, WebSocketFrame_t764750278 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::<>m__2(System.Exception)
extern "C"  void U3CstartReceivingU3Ec__AnonStorey4_U3CU3Em__2_m717826270 (U3CstartReceivingU3Ec__AnonStorey4_t506990866 * __this, Exception_t1927440687 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
