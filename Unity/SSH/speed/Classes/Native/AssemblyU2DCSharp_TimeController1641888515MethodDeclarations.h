﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeController
struct TimeController_t1641888515;

#include "codegen/il2cpp-codegen.h"

// System.Void TimeController::.ctor()
extern "C"  void TimeController__ctor_m1965318492 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::Start()
extern "C"  void TimeController_Start_m159638416 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::moveBattleStartIntervalTimer()
extern "C"  void TimeController_moveBattleStartIntervalTimer_m2480696497 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::checkBattleStartIntervalTimer()
extern "C"  void TimeController_checkBattleStartIntervalTimer_m2515658548 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::moveBattleTimer()
extern "C"  void TimeController_moveBattleTimer_m3304460796 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::checkBattleTimer()
extern "C"  void TimeController_checkBattleTimer_m3648717641 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::Update()
extern "C"  void TimeController_Update_m3259163491 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeController::isBattleStarted()
extern "C"  bool TimeController_isBattleStarted_m1090798863 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeController::isBattleEnded()
extern "C"  bool TimeController_isBattleEnded_m2521904612 (TimeController_t1641888515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeController::.cctor()
extern "C"  void TimeController__cctor_m2823837213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
