﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeText
struct TimeText_t2509642322;

#include "codegen/il2cpp-codegen.h"

// System.Void TimeText::.ctor()
extern "C"  void TimeText__ctor_m1704414703 (TimeText_t2509642322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeText::setTime(System.Single,System.Boolean,System.Single)
extern "C"  void TimeText_setTime_m3820777775 (TimeText_t2509642322 * __this, float ___time0, bool ___isStarted1, float ___battleTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
