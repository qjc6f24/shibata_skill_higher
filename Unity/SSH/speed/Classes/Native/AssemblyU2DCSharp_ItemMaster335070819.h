﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemMaster/Item
struct Item_t1836005089;

#include "AssemblyU2DCSharp_MasterBase2623394359.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemMaster
struct  ItemMaster_t335070819  : public MasterBase_t2623394359
{
public:
	// ItemMaster/Item ItemMaster::itemMaster
	Item_t1836005089 * ___itemMaster_4;

public:
	inline static int32_t get_offset_of_itemMaster_4() { return static_cast<int32_t>(offsetof(ItemMaster_t335070819, ___itemMaster_4)); }
	inline Item_t1836005089 * get_itemMaster_4() const { return ___itemMaster_4; }
	inline Item_t1836005089 ** get_address_of_itemMaster_4() { return &___itemMaster_4; }
	inline void set_itemMaster_4(Item_t1836005089 * value)
	{
		___itemMaster_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemMaster_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
