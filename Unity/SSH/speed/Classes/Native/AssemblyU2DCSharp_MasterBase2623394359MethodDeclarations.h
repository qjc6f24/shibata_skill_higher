﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MasterBase
struct MasterBase_t2623394359;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MasterBase::.ctor()
extern "C"  void MasterBase__ctor_m2050107174 (MasterBase_t2623394359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MasterBase::loadJson(System.String)
extern "C"  void MasterBase_loadJson_m1766485096 (MasterBase_t2623394359 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
