﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey4__ctor_m1345062016 (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::<>m__0(System.Int64)
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey4_U3CU3Em__0_m3429307487 (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * __this, int64_t ___len0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
