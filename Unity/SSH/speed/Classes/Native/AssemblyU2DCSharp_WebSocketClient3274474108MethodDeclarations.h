﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketClient
struct WebSocketClient_t3274474108;
// JSONObject
struct JSONObject_t1971882247;
// System.String
struct String_t;
// CardController
struct CardController_t2595996862;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t2890051726;
// WebSocketSharp.ErrorEventArgs
struct ErrorEventArgs_t502222999;
// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t344507773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject1971882247.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_CardController2595996862.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "websocketU2Dsharp_WebSocketSharp_MessageEventArgs2890051726.h"
#include "websocketU2Dsharp_WebSocketSharp_ErrorEventArgs502222999.h"
#include "websocketU2Dsharp_WebSocketSharp_CloseEventArgs344507773.h"

// System.Void WebSocketClient::.ctor()
extern "C"  void WebSocketClient__ctor_m3624786787 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::Start()
extern "C"  void WebSocketClient_Start_m173570775 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::Update()
extern "C"  void WebSocketClient_Update_m2959837662 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::OnDestroy()
extern "C"  void WebSocketClient_OnDestroy_m2402546446 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::ConnectWebSocket()
extern "C"  void WebSocketClient_ConnectWebSocket_m1699444666 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::sendMessage(System.Int32,JSONObject)
extern "C"  void WebSocketClient_sendMessage_m2434924180 (WebSocketClient_t3274474108 * __this, int32_t ___pkType0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::parseMessage(System.String)
extern "C"  void WebSocketClient_parseMessage_m1799235779 (WebSocketClient_t3274474108 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::changePhase(System.Int32)
extern "C"  void WebSocketClient_changePhase_m148092363 (WebSocketClient_t3274474108 * __this, int32_t ___phase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::saveUserId()
extern "C"  void WebSocketClient_saveUserId_m4285407080 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::pushStart()
extern "C"  void WebSocketClient_pushStart_m1598994261 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::pushNewPlayer()
extern "C"  void WebSocketClient_pushNewPlayer_m507443108 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::actionWithPk(System.Int32,JSONObject)
extern "C"  void WebSocketClient_actionWithPk_m1892780742 (WebSocketClient_t3274474108 * __this, int32_t ___pkType0, JSONObject_t1971882247 * ___jsonObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::setCardController(CardController)
extern "C"  void WebSocketClient_setCardController_m1671294365 (WebSocketClient_t3274474108 * __this, CardController_t2595996862 * ___cardController0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketClient::getPlayerNum()
extern "C"  int32_t WebSocketClient_getPlayerNum_m4203793634 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketClient::getNextPlayerNum(System.Int32)
extern "C"  int32_t WebSocketClient_getNextPlayerNum_m818655264 (WebSocketClient_t3274474108 * __this, int32_t ___basePlayerNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] WebSocketClient::getFieldCardNums()
extern "C"  Int32U5BU5D_t3030399641* WebSocketClient_getFieldCardNums_m2030196866 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketClient::getFieldCardNum(System.Int32)
extern "C"  int32_t WebSocketClient_getFieldCardNum_m3900970582 (WebSocketClient_t3274474108 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[][] WebSocketClient::getPlayerCardNums()
extern "C"  Int32U5BU5DU5BU5D_t3750818532* WebSocketClient_getPlayerCardNums_m1908209225 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] WebSocketClient::getPlayerCardNums(System.Int32)
extern "C"  Int32U5BU5D_t3030399641* WebSocketClient_getPlayerCardNums_m3514879090 (WebSocketClient_t3274474108 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketClient::getPlayerCardNum(System.Int32,System.Int32)
extern "C"  int32_t WebSocketClient_getPlayerCardNum_m2837115496 (WebSocketClient_t3274474108 * __this, int32_t ___userIndex0, int32_t ___cardIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] WebSocketClient::getPlayerUserIds()
extern "C"  Int32U5BU5D_t3030399641* WebSocketClient_getPlayerUserIds_m4162158257 (WebSocketClient_t3274474108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketClient::getPlayerUserId(System.Int32)
extern "C"  int32_t WebSocketClient_getPlayerUserId_m3572018765 (WebSocketClient_t3274474108 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::checkChangeCard(JSONObject)
extern "C"  void WebSocketClient_checkChangeCard_m1740291254 (WebSocketClient_t3274474108 * __this, JSONObject_t1971882247 * ___jsonObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::.cctor()
extern "C"  void WebSocketClient__cctor_m4262126418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::<ConnectWebSocket>m__0(System.Object,System.EventArgs)
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__0_m1860180633 (WebSocketClient_t3274474108 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::<ConnectWebSocket>m__1(System.Object,WebSocketSharp.MessageEventArgs)
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__1_m4218706127 (WebSocketClient_t3274474108 * __this, Il2CppObject * ___sender0, MessageEventArgs_t2890051726 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::<ConnectWebSocket>m__2(System.Object,WebSocketSharp.ErrorEventArgs)
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__2_m2020985979 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, ErrorEventArgs_t502222999 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketClient::<ConnectWebSocket>m__3(System.Object,WebSocketSharp.CloseEventArgs)
extern "C"  void WebSocketClient_U3CConnectWebSocketU3Em__3_m3695248688 (WebSocketClient_t3274474108 * __this, Il2CppObject * ___sender0, CloseEventArgs_t344507773 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
