﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestController
struct  TestController_t674116116  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TestController::_timer
	float ____timer_6;
	// System.Int32 TestController::_playerHp
	int32_t ____playerHp_7;
	// System.Int32 TestController::_bossHp
	int32_t ____bossHp_8;

public:
	inline static int32_t get_offset_of__timer_6() { return static_cast<int32_t>(offsetof(TestController_t674116116, ____timer_6)); }
	inline float get__timer_6() const { return ____timer_6; }
	inline float* get_address_of__timer_6() { return &____timer_6; }
	inline void set__timer_6(float value)
	{
		____timer_6 = value;
	}

	inline static int32_t get_offset_of__playerHp_7() { return static_cast<int32_t>(offsetof(TestController_t674116116, ____playerHp_7)); }
	inline int32_t get__playerHp_7() const { return ____playerHp_7; }
	inline int32_t* get_address_of__playerHp_7() { return &____playerHp_7; }
	inline void set__playerHp_7(int32_t value)
	{
		____playerHp_7 = value;
	}

	inline static int32_t get_offset_of__bossHp_8() { return static_cast<int32_t>(offsetof(TestController_t674116116, ____bossHp_8)); }
	inline int32_t get__bossHp_8() const { return ____bossHp_8; }
	inline int32_t* get_address_of__bossHp_8() { return &____bossHp_8; }
	inline void set__bossHp_8(int32_t value)
	{
		____bossHp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
