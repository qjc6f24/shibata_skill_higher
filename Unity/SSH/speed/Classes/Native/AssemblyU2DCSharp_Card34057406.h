﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayerController
struct PlayerController_t4148409433;
// CardController
struct CardController_t2595996862;
// BattleController
struct BattleController_t3050984398;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Card
struct  Card_t34057406  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Card::ownTargetNum
	int32_t ___ownTargetNum_2;
	// System.Int32 Card::ownIndex
	int32_t ___ownIndex_3;
	// System.Int32 Card::ownNum
	int32_t ___ownNum_4;
	// UnityEngine.GameObject Card::cursor
	GameObject_t1756533147 * ___cursor_5;
	// UnityEngine.GameObject Card::cursorObject
	GameObject_t1756533147 * ___cursorObject_6;
	// PlayerController Card::_playerController
	PlayerController_t4148409433 * ____playerController_7;
	// CardController Card::_cardController
	CardController_t2595996862 * ____cardController_8;
	// BattleController Card::_battleController
	BattleController_t3050984398 * ____battleController_9;

public:
	inline static int32_t get_offset_of_ownTargetNum_2() { return static_cast<int32_t>(offsetof(Card_t34057406, ___ownTargetNum_2)); }
	inline int32_t get_ownTargetNum_2() const { return ___ownTargetNum_2; }
	inline int32_t* get_address_of_ownTargetNum_2() { return &___ownTargetNum_2; }
	inline void set_ownTargetNum_2(int32_t value)
	{
		___ownTargetNum_2 = value;
	}

	inline static int32_t get_offset_of_ownIndex_3() { return static_cast<int32_t>(offsetof(Card_t34057406, ___ownIndex_3)); }
	inline int32_t get_ownIndex_3() const { return ___ownIndex_3; }
	inline int32_t* get_address_of_ownIndex_3() { return &___ownIndex_3; }
	inline void set_ownIndex_3(int32_t value)
	{
		___ownIndex_3 = value;
	}

	inline static int32_t get_offset_of_ownNum_4() { return static_cast<int32_t>(offsetof(Card_t34057406, ___ownNum_4)); }
	inline int32_t get_ownNum_4() const { return ___ownNum_4; }
	inline int32_t* get_address_of_ownNum_4() { return &___ownNum_4; }
	inline void set_ownNum_4(int32_t value)
	{
		___ownNum_4 = value;
	}

	inline static int32_t get_offset_of_cursor_5() { return static_cast<int32_t>(offsetof(Card_t34057406, ___cursor_5)); }
	inline GameObject_t1756533147 * get_cursor_5() const { return ___cursor_5; }
	inline GameObject_t1756533147 ** get_address_of_cursor_5() { return &___cursor_5; }
	inline void set_cursor_5(GameObject_t1756533147 * value)
	{
		___cursor_5 = value;
		Il2CppCodeGenWriteBarrier(&___cursor_5, value);
	}

	inline static int32_t get_offset_of_cursorObject_6() { return static_cast<int32_t>(offsetof(Card_t34057406, ___cursorObject_6)); }
	inline GameObject_t1756533147 * get_cursorObject_6() const { return ___cursorObject_6; }
	inline GameObject_t1756533147 ** get_address_of_cursorObject_6() { return &___cursorObject_6; }
	inline void set_cursorObject_6(GameObject_t1756533147 * value)
	{
		___cursorObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___cursorObject_6, value);
	}

	inline static int32_t get_offset_of__playerController_7() { return static_cast<int32_t>(offsetof(Card_t34057406, ____playerController_7)); }
	inline PlayerController_t4148409433 * get__playerController_7() const { return ____playerController_7; }
	inline PlayerController_t4148409433 ** get_address_of__playerController_7() { return &____playerController_7; }
	inline void set__playerController_7(PlayerController_t4148409433 * value)
	{
		____playerController_7 = value;
		Il2CppCodeGenWriteBarrier(&____playerController_7, value);
	}

	inline static int32_t get_offset_of__cardController_8() { return static_cast<int32_t>(offsetof(Card_t34057406, ____cardController_8)); }
	inline CardController_t2595996862 * get__cardController_8() const { return ____cardController_8; }
	inline CardController_t2595996862 ** get_address_of__cardController_8() { return &____cardController_8; }
	inline void set__cardController_8(CardController_t2595996862 * value)
	{
		____cardController_8 = value;
		Il2CppCodeGenWriteBarrier(&____cardController_8, value);
	}

	inline static int32_t get_offset_of__battleController_9() { return static_cast<int32_t>(offsetof(Card_t34057406, ____battleController_9)); }
	inline BattleController_t3050984398 * get__battleController_9() const { return ____battleController_9; }
	inline BattleController_t3050984398 ** get_address_of__battleController_9() { return &____battleController_9; }
	inline void set__battleController_9(BattleController_t3050984398 * value)
	{
		____battleController_9 = value;
		Il2CppCodeGenWriteBarrier(&____battleController_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
