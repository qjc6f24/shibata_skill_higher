﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2
struct U3CToStringU3Ec__AnonStorey2_t1254512383;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::.ctor()
extern "C"  void U3CToStringU3Ec__AnonStorey2__ctor_m606439006 (U3CToStringU3Ec__AnonStorey2_t1254512383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::<>m__0(System.Int32)
extern "C"  void U3CToStringU3Ec__AnonStorey2_U3CU3Em__0_m1532176860 (U3CToStringU3Ec__AnonStorey2_t1254512383 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
