﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5
struct U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey5__ctor_m3354575350 (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::<>m__0(System.IAsyncResult)
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey5_U3CU3Em__0_m3118594234 (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
