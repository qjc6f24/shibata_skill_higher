﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterBase : MonoBehaviour {

    public string json;

    [System.Serializable]
    public class Base
    {
        public int id;
        public string value;
    }

    public Base baseMaster;

    public void loadJson(string key)
    {
        json = (Resources.Load("Json/" + key) as TextAsset).text;
    }
}
