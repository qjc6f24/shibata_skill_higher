﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMaster : MasterBase {

    [System.Serializable]
    public class Item
    {
        public int id;
        public string name;
        public string description;
    }

    public Item itemMaster;
    
    void Start () {
        loadJson("item");
        baseMaster = JsonUtility.FromJson<Base>(json);
        itemMaster = JsonUtility.FromJson<Item>(baseMaster.value);
    }	
}
