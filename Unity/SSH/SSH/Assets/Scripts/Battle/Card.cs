﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {

    public int targetNum;
    public int ownIndex;
    private PlayerController _playerController;
    public GameObject _cursor;

	// Use this for initialization
	void Start () {
        _cursor = (GameObject)Instantiate(_cursor, transform.position, transform.rotation);
        _cursor.SetActive(false);
        _playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            // Save : 
            // ・クリックしたオブジェクトを取らないと
            // ・カーソルのサイズを調整→やっぱCardControllerにカーソル生成させる？
            _playerController.setSelectingCardIndex(ownIndex);
        }

        if (targetNum == CardController.TARGET_PLAYER)
        {
            checkCursor();
        }
	}

    private void checkCursor()
    {
        bool indexMatch = _playerController.getSelectingCardIndex() == ownIndex;
        if (indexMatch && !_cursor.active)
        {
            _cursor.SetActive(true);
        }
        else if (!indexMatch && _cursor.active)
        {
            _cursor.SetActive(false);
        }
    }
}
