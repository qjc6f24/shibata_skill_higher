﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private int _selectingCardIndex = 99;

    public int getSelectingCardIndex()
    {
        return _selectingCardIndex;
    }

    public void setSelectingCardIndex(int index)
    {
        _selectingCardIndex = index;
    }
}
