﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour {

    public const int TARGET_PLAYER = 0;
    public const int TARGET_ENEMY = 1;
    public const int TARGET_FIELD = 2;
    public const int TARGET_DECK = 3;

    private Vector3 _topLeft;
    private Vector3 _bottomRight;
    private float _screenWidth;
    private float _screenHeight;
    private Vector3 _centerPos;

    public Camera mainCamera;

    public GameObject[] cards = new GameObject[15];
    private static GameObject[] _fieldObjects = new GameObject[2];
    private static GameObject[] _playerObjects = new GameObject[4];
    private static GameObject[] _enemyObjects = new GameObject[4];
    private static GameObject[] _deckObjects = new GameObject[2];
    private List<GameObject[]> _objects = new List<GameObject[]>{_playerObjects, _enemyObjects, _fieldObjects, _deckObjects };

    void Start () {
        // カメラに映っている左上の座標取得
        _topLeft = mainCamera.ScreenToWorldPoint(Vector3.zero);
        // カメラに映っている右下の座標取得
        _bottomRight = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
        // カメラに映っている幅取得
        _screenWidth = _bottomRight.x - _topLeft.x;
        // カメラに映っている高さ取得
        _screenHeight = _topLeft.y - _bottomRight.y;

        // 中央位置取得
        _centerPos = new Vector3(transform.position.x, transform.position.y, 0.0f);

        // フィールドカード生成;
        instantiateCard(TARGET_FIELD, 0, 1, - _screenWidth / 5.0f, 0.0f);
        instantiateCard(TARGET_FIELD, 1, 2, _screenWidth / 5.0f, 0.0f);

        // 山札生成
        instantiateCard(TARGET_DECK, TARGET_PLAYER, 0, 0.0f, - _screenHeight / 5.0f, 0.1f);
        instantiateCard(TARGET_DECK, TARGET_ENEMY, 0, 0.0f, _screenHeight / 5.0f, 0.1f);

        // 手札生成
        instantiateCard(TARGET_PLAYER, 0, 1, _screenWidth / 8.0f * 3, - _screenHeight / 3.0f);
        instantiateCard(TARGET_PLAYER, 1, 2, _screenWidth / 8.0f, - _screenHeight / 3.0f);
        instantiateCard(TARGET_PLAYER, 2, 3, - _screenWidth / 8.0f, - _screenHeight / 3.0f);
        instantiateCard(TARGET_PLAYER, 3, 4, - _screenWidth / 8.0f * 3, - _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY, 0, 1, - _screenWidth / 8.0f * 3, _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY, 1, 2, - _screenWidth / 8.0f, _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY, 2, 3, _screenWidth / 8.0f, _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY, 3, 4, _screenWidth / 8.0f * 3, _screenHeight / 3.0f);
    }
	
    private void instantiateCard(int target, int index, int num, float x, float y, float rate = 0.2f)
    {
        GameObject gameObject = (GameObject)Instantiate(cards[num], _centerPos + new Vector3(x, y, 0.0f), transform.rotation);
        modifyScale(gameObject, rate);
        gameObject.GetComponent<Card>().targetNum = target;
        gameObject.GetComponent<Card>().ownIndex = index;
        _objects[target][index] = gameObject;
    }

    private void modifyScale(GameObject gameObject, float rate)
    {
        // 実際 * 理想 / 実際
        float objectWidth = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        float scale = _screenWidth * rate / objectWidth;
        gameObject.transform.localScale = new Vector3(scale, scale, 0);
    }

	void Update () {
		
	}

    // カード変更処理(player, enemyから呼び出される)
    public void changeCard(int target, int index, int num)
    {
        Transform tmpObjectTrans = _objects[target][index].transform;
        instantiateCard(target, index, num, tmpObjectTrans.position.x, tmpObjectTrans.transform.position.y);
    }
}