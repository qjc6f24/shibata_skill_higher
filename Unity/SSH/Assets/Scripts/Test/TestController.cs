﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestController : MonoBehaviour {

    // 勝利判定定数
    const int JUDGEMENT_PLAYING = 0;
    const int JUDGEMENT_PLAYER_WIN = 1;
    const int JUDGEMENT_PLAYER_LOSE = 2;
    const int JUDGEMENT_TIMEOVER = 3;

    

    // 実際にはtimeController,playerControllerとかが持つ変数を仮置き
    private float _timer;
    private int _playerHp;
    private int _bossHp;



    // Use this for initialization
    void Start () {
        _timer = 5.0f;
        _playerHp = 10;
        _bossHp = 10;
    }
	
	// Update is called once per frame
	void Update () {
        // タイマ稼働
        _timer -= Time.deltaTime;

        // 終了判定
        switch (judge())
        {
            case JUDGEMENT_PLAYING:
                break;
            case JUDGEMENT_PLAYER_WIN:
                break;
            case JUDGEMENT_PLAYER_LOSE:
                break;
            case JUDGEMENT_TIMEOVER:
                SceneManager.LoadScene("TimeOverScene");
                break;
            default:
                break;
        }

        // 
	}



    

    private int judge()
    {
        if (_bossHp <= 0)
        {
            return JUDGEMENT_PLAYER_WIN;
        }
        if (_playerHp <= 0)
        {
            return JUDGEMENT_PLAYER_LOSE;
        }
        if (_timer <= 0.0f)
        {
            return JUDGEMENT_TIMEOVER;
        }

        return JUDGEMENT_PLAYING;
    }
}

/* ターン制のゲームにする場合
    // start関数内
    _turn = TURN_PLAYER; 

    // ターン定数
    const int TURN_PLAYER = 0;
    const int TURN_ENEMY = 1;

    private int _turn;

    public void changeTurn(int turn)
    {
        switch (turn)
        {
            case TURN_PLAYER:
                _turn = TURN_PLAYER;
                break;
            case TURN_ENEMY:
                _turn = TURN_ENEMY;
                break;
            default:
                // ランダム決定にする？
                _turn = TURN_PLAYER;
                break;
        }
    }
    */
