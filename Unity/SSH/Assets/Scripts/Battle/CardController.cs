﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour {

    public const int TARGET_PLAYER = 0;
    public const int TARGET_ENEMY_1 = 1;
	public const int TARGET_ENEMY_2 = 2;
	public const int TARGET_ENEMY_3 = 3;
    public const int TARGET_FIELD = 4;
    public const int TARGET_DECK = 5;

    private Vector3 _topLeft;
    private Vector3 _bottomRight;
    private float _screenWidth;
    private float _screenHeight;
    private Vector3 _centerPos;

    public Camera mainCamera;
	private PlayerController _playerController;
	private BattleController _battleController;
	//private EnemyController _enemyController;
	private WebSocketClient _webSocketClient;

	public static int CARD_TYPE_COUNT = 6;
	public static int USERS_CARD_COUNT = 4;
	public static int FIELD_CARD_COUNT = 4;
	public static int USER_MAX_COUNT = 4;
	public static int MAX_CARD_COUNT = 99;
	public GameObject[] cards = new GameObject[CARD_TYPE_COUNT + 1];
	private static GameObject[] _fieldObjects = new GameObject[FIELD_CARD_COUNT];
	private static GameObject[] _playerObjects = new GameObject[USERS_CARD_COUNT];
	private static GameObject[] _enemyObjects1 = new GameObject[USERS_CARD_COUNT];
	private static GameObject[] _enemyObjects2 = new GameObject[USERS_CARD_COUNT];
	private static GameObject[] _enemyObjects3 = new GameObject[USERS_CARD_COUNT];
    //private static GameObject[] _deckObjects = new GameObject[2];
	private List<GameObject[]> _objects = new List<GameObject[]>{_playerObjects, _enemyObjects1, _enemyObjects2, _enemyObjects3, _fieldObjects};

	private static int PHASE_INIT = 0;
	private static int PHASE_PLAYING = 1;
	private static int PHASE_RESULT = 2;
	private int _phase = PHASE_INIT;

    void Start () {
        // カメラに映っている左上の座標取得
        _topLeft = mainCamera.ScreenToWorldPoint(Vector3.zero);
        // カメラに映っている右下の座標取得
        _bottomRight = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
        // カメラに映っている幅取得
        _screenWidth = _bottomRight.x - _topLeft.x;
        // カメラに映っている高さ取得
        _screenHeight = _topLeft.y - _bottomRight.y;

		// コントローラ取得
		_playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
		//_enemyController = GameObject.Find("EnemyController").GetComponent<EnemyController>();
		_webSocketClient = GameObject.Find ("GameController").GetComponent<WebSocketClient> ();
		_battleController = GameObject.Find ("BattleManager").GetComponent<BattleController> ();

		// 自身をWebSocketClientに渡す
		_webSocketClient.setCardController(this.GetComponent<CardController>());

        // 中央位置取得
        _centerPos = new Vector3(transform.position.x, transform.position.y, 0.0f);

        // フィールドカード生成;
		instantiateCard(TARGET_FIELD, 0, 1, - _screenWidth / 8.0f, - _screenHeight / 13.0f);
		instantiateCard(TARGET_FIELD, 1, 2, _screenWidth / 8.0f, - _screenHeight / 13.0f);
		instantiateCard(TARGET_FIELD, 2, 3, _screenWidth / 8.0f, _screenHeight / 13.0f);
		instantiateCard(TARGET_FIELD, 3, 4, - _screenWidth / 8.0f, _screenHeight / 13.0f);

        // 山札生成
        //instantiateCard(TARGET_DECK, TARGET_PLAYER, 0, 0.0f, - _screenHeight / 5.0f, 0.1f);
        //instantiateCard(TARGET_DECK, TARGET_ENEMY, 0, 0.0f, _screenHeight / 5.0f, 0.1f);

		// カード枚数設定
		initCounter();

        // 手札生成
        instantiateCard(TARGET_PLAYER, 0, 0, - _screenWidth / 8.0f * 3, + _screenHeight / 3.0f);
        instantiateCard(TARGET_PLAYER, 1, 0, - _screenWidth / 8.0f, + _screenHeight / 3.0f);
        instantiateCard(TARGET_PLAYER, 2, 0, _screenWidth / 8.0f, + _screenHeight / 3.0f);
        instantiateCard(TARGET_PLAYER, 3, 0, _screenWidth / 8.0f * 3, + _screenHeight / 3.0f);
		instantiateCard(TARGET_ENEMY_1, 0, 0, _screenWidth / 10.0f * 4, _screenHeight / 18.0f * 3);
		instantiateCard(TARGET_ENEMY_1, 1, 0, _screenWidth / 10.0f * 4, _screenHeight / 18.0f);
		instantiateCard(TARGET_ENEMY_1, 2, 0, _screenWidth / 10.0f * 4, - _screenHeight / 18.0f);
		instantiateCard(TARGET_ENEMY_1, 3, 0, _screenWidth / 10.0f * 4, - _screenHeight / 18.0f * 3);
        instantiateCard(TARGET_ENEMY_2, 0, 0, _screenWidth / 10.0f * 3, - _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY_2, 1, 0, _screenWidth / 10.0f, - _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY_2, 2, 0, - _screenWidth / 10.0f, - _screenHeight / 3.0f);
        instantiateCard(TARGET_ENEMY_2, 3, 0, - _screenWidth / 10.0f * 3, - _screenHeight / 3.0f);
		instantiateCard(TARGET_ENEMY_3, 0, 0, - _screenWidth / 10.0f * 4, - _screenHeight / 18.0f * 3);
		instantiateCard(TARGET_ENEMY_3, 1, 0, - _screenWidth / 10.0f * 4, - _screenHeight / 18.0f);
		instantiateCard(TARGET_ENEMY_3, 2, 0, - _screenWidth / 10.0f * 4, _screenHeight / 18.0f);
		instantiateCard(TARGET_ENEMY_3, 3, 0, - _screenWidth / 10.0f * 4, _screenHeight / 18.0f * 3);
		updateAllCards ();
		updateAllCounter ();
		_phase = PHASE_PLAYING;
    }
	
    private void instantiateCard(int target, int index, int num, float x, float y, float rate = 0.2f)
    {
		if (target != TARGET_PLAYER && target != TARGET_FIELD) {
			rate = 0.1f;
		}
        GameObject gameObject = (GameObject)Instantiate(cards[num], _centerPos + new Vector3(x, y, 0.0f), transform.rotation);
        modifyScale(gameObject, rate);
		gameObject.GetComponent<Card>().initialize ();
        gameObject.GetComponent<Card>().ownTargetNum = target;
        gameObject.GetComponent<Card>().ownIndex = index;
		gameObject.GetComponent<Card> ().ownNum = num;
		gameObject.GetComponent<Card>().getCursorObject().transform.localScale = gameObject.transform.localScale * 1.1f;
		if (_objects [target] [index] != null) {
			_objects [target] [index].GetComponent<Card>().destroyCursorObject();
			DestroyObject(_objects [target] [index]);
		}	
        _objects[target][index] = gameObject;

		if (target == TARGET_PLAYER) {
			_playerController.setUserCard (index, num);
		}
		/*
		if (target >= TARGET_ENEMY_1 && target <= TARGET_ENEMY_3) {
			_enemyController.setUserCard (index, num);
		}
		*/
    }

    private void modifyScale(GameObject gameObject, float rate)
    {
        // 実際 * 理想 / 実際
        float objectWidth = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        float scale = _screenWidth * rate / objectWidth;
        gameObject.transform.localScale = new Vector3(scale, scale, 0);
    }

	public Vector3 getCenterPos()
	{
		return _centerPos;
	}

	private void initCounter()
	{
		for (int num = 0; num < 4; num++) {
			GameObject counter = GameObject.Find ("CardCounter" + num.ToString());
			switch (num) {
			case TARGET_ENEMY_1:
				counter.transform.localPosition = new Vector2 (_screenWidth * 32.0f, 0.0f);
				break;
			case TARGET_ENEMY_2:
				counter.transform.localPosition = new Vector2 (0.0f, _screenWidth * 32.0f);
				break;
			case TARGET_ENEMY_3:
				counter.transform.localPosition = new Vector2 (- _screenWidth * 32.0f, 0.0f);
				break;
			case TARGET_PLAYER:
			default:
				counter.transform.localPosition = new Vector2 (0.0f, - _screenWidth * 32.0f);
				break;
			}
			counter.GetComponent<CardCounterText> ().setCount(MAX_CARD_COUNT);
		}
	}

	void Update () {
		if (_phase == PHASE_PLAYING && _battleController.checkBattleStarted() && _webSocketClient.isChangedField) {
			updateAllCards ();
			updateAllCounter ();
			_webSocketClient.isChangedField = false;
		}
	}

	void updateAllCards()
	{
		Debug.Log ("updateAllCards");
		updateFieldCards ();
		updateUsersCards ();
	}

	void updateFieldCards()
	{
		Debug.Log ("updateFieldCards");
		for (int i = 0; i < FIELD_CARD_COUNT; i++) {
			changeCard (TARGET_FIELD, i, _webSocketClient.getFieldCardNum(i));
		}
	}

	void updateUsersCards()
	{
		Debug.Log ("updateUsersCards");
		int playerNum = _webSocketClient.getPlayerNum();
		// 自分のカードを変更
		for (int i = 0; i < USERS_CARD_COUNT; i++) {
				changeCard (TARGET_PLAYER, i, _webSocketClient.getPlayerCardNum(playerNum, i));
		}
		// 敵のカードを変更
		int target_enemy_num = TARGET_ENEMY_1;
		for (int i = _webSocketClient.getNextPlayerNum(playerNum);
			i != playerNum; i = _webSocketClient.getNextPlayerNum(i)) {
			for (int j = 0; j < USERS_CARD_COUNT; j++) {
				changeCard (target_enemy_num, j, _webSocketClient.getPlayerCardNum(i, j));
			}
			target_enemy_num++;
		}
	}

	void updateAllCounter()
	{
		int playerNum = _webSocketClient.getPlayerNum ();
		// 自分のカウンターを更新
		GameObject counter = GameObject.Find ("CardCounter0");
		int cardCount = _webSocketClient.getPlayerCardCount (playerNum);
		counter.GetComponent<CardCounterText> ().setCount(cardCount);
		if (cardCount == 0) {
			_battleController.setWin (TARGET_PLAYER);
		}

		// 敵のカウンターを更新
		int enemy_counter_num = TARGET_ENEMY_1;
		for (int num = _webSocketClient.getNextPlayerNum(playerNum);
			num != playerNum; num = _webSocketClient.getNextPlayerNum(num)) {
			counter = GameObject.Find ("CardCounter" + enemy_counter_num);
			cardCount = _webSocketClient.getPlayerCardCount (num);
			counter.GetComponent<CardCounterText> ().setCount(cardCount);
			if (cardCount == 0) {
				_battleController.setWin (enemy_counter_num);
			}
			enemy_counter_num++;
		}
	}

	void setWin(int cardCount, int target)
	{
		if (cardCount == 0) {
			_battleController.setWin (target);
			_phase = PHASE_RESULT;
		}
	}

	/*
	public bool tryUseEnemyCard()
	{
		float randF = Random.Range(0, USERS_CARD_COUNT);
		int randomIndex = (int)randF;
		int enemyCardNum = _objects[TARGET_ENEMY][randomIndex].GetComponent<Card>().ownNum;
		int fieldLeftCardNum = _objects [TARGET_FIELD] [0].GetComponent<Card> ().ownNum;
		int fieldRightCardNum = _objects [TARGET_FIELD] [1].GetComponent<Card> ().ownNum;
		if (checkChangeCard(enemyCardNum, fieldLeftCardNum)) {
			changeCard (TARGET_FIELD, 0, enemyCardNum);
			changeCard (TARGET_ENEMY, randomIndex, Random.Range (1, CARD_TYPE_COUNT + 1));
			return true;
		}
		if (checkChangeCard(enemyCardNum, fieldRightCardNum)) {
			changeCard (TARGET_FIELD, 1, enemyCardNum);
			changeCard (TARGET_ENEMY, randomIndex, Random.Range (1, CARD_TYPE_COUNT + 1));
			return true;
		}
		return false;
	}
	*/

    // カード変更処理
    public void changeCard(int target, int index, int num)
    {
        Transform tmpObjectTrans = _objects[target][index].transform;
        instantiateCard(target, index, num, tmpObjectTrans.position.x, tmpObjectTrans.transform.position.y);
    }

	public static bool checkChangeCard(int selectingCard, int targetCard)
	{
		// 1とmax以外
		if (selectingCard > 1 && selectingCard < CARD_TYPE_COUNT)
		{
			if (targetCard == selectingCard + 1 || targetCard == selectingCard - 1)
			{
				return true;
			}
			return false;
		}
		
	    // 1の時
		if (selectingCard == 1)
		{
			if (targetCard == 2 || targetCard == CARD_TYPE_COUNT)
			{
				return true;
			}
			return false;
		}

		// maxの時
		if (targetCard == 1 || targetCard == CARD_TYPE_COUNT - 1)
		{
			return true;
		}
		return false;
	}

	public void checkChangeCards(int fieldIdx, int userSelectingIdx)
	{
		JSONObject jsonObject = new JSONObject ();
		jsonObject.AddField ("playerNum", _webSocketClient.getPlayerNum ());
		jsonObject.AddField ("fieldIdx", fieldIdx);
		jsonObject.AddField ("userSelectingIdx", userSelectingIdx);
		_webSocketClient.checkChangeCard (jsonObject);
	}

	public void setWaitStatusNewCard()
	{
		_battleController.setWaitStatusNewCard ();
	}

	public void setWaitStatusNotWait()
	{
		_battleController.setWaitStatusNotWait ();
	}

	public int checkCardCounts()
	{
		for (int i = 0; i < USER_MAX_COUNT; i++) {
			if (_webSocketClient.getPlayerCardCount (i) <= 0) {
				return i;
			}
		}
		return 99;
	}
}