﻿using UnityEngine;
using System;

public class Pk : MonoBehaviour
{
	protected const int PK_CREATE_USER_ID = 100;
	protected const int PK_LOGIN = 101;
	protected const int PK_START_GAME = 110;
	protected const int PK_UPDATE_CARDS = 200;
	protected const int PK_UPDATE_CARD_FORCE = 201;
	protected const int PK_CLOSE_WEBSOCKET = 9999;

	protected int getInt(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return 0;
		}
		return (int)jsonObject.GetField (key).i;
	}

	protected string getStr(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return "";
		}
		return jsonObject.GetField (key).str;
	}

	protected long getLong(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return 0;
		}
		return jsonObject.GetField (key).i;
	}

	protected float getFloat(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return 0.0f;
		}
		return jsonObject.GetField (key).f;
	}

	protected bool getBool(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return false;
		}
		return jsonObject.GetField (key).b;
	}

	protected int[] getIntArr(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return new int[1];
		}
		string arrStr = jsonObject.GetField (key).ToString();
		arrStr = arrStr.Replace ("[", "");
		arrStr = arrStr.Replace ("]", "");
		string[] splitNums = arrStr.Split (',');
		int size = splitNums.Length;
		int[] arr = new int[size];
		for (int i = 0; i < size; i++) {
			int r;
			if (Int32.TryParse (splitNums [i], out r)) {
				arr[i] = Int32.Parse(splitNums [i]);
			}
		}
		return arr;
	}

	protected int[][] getIntArrArr(string key, JSONObject jsonObject)
	{
		if (!jsonObject.HasField (key)) {
			return new int[1][];
		}

		string arrStr = jsonObject.GetField (key).ToString();    ;
		string[] splitStrs = arrStr.Split (']');
		int length = 0;
		for (int i = 0; i < splitStrs.Length - 1; i++) {
			if (splitStrs [i].Length <= 1) {
				continue;
			}
			length++;
		}
		int[][] arr = new int[length][];
		for (int i = 0; i < length; i++) {
			if (splitStrs [i].Length <= 1) {
				continue;
			}
			splitStrs[i] = splitStrs[i].Replace ("[", "");
			string[] splitNums = splitStrs[i].Split (',');
			int size = splitNums.Length;
			for (int j = 0; j < splitNums.Length; j++) {
				if (splitNums[j] == "") {
					size--;
					continue;
				}
			}
			int[] tmpArr = new int[size];
			int idx = 0;
			for (int j = 0; j < splitNums.Length; j++) {
				if (splitNums[j] == "") {
					continue;
				}
				int r;
				if (Int32.TryParse (splitNums [j], out r)) {
					tmpArr[idx++] = Int32.Parse (splitNums [j]);
				}
			}
			arr [i] = tmpArr;
		}
		return arr;
	}

	public void debugLog(int[] arr)
	{
		Debug.Log ("Debug int[] show.");
		for (int i = 0; i < arr.Length; i++) {
			Debug.Log ("int[" + i + "] = " + arr[i]);
		}
	}

	public void debugLogAA(int[][] arr)
	{
		Debug.Log ("Debug int[][] show.");
		for (int i = 0; i < arr.Length; i++) {
			for (int j = 0; j < arr[i].Length; j++) {
				Debug.Log ("int[" + i + "][" + j + "] = " + arr[i][j]);	
			}
		}
	}
}
