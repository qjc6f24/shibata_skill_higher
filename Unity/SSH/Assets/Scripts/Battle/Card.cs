﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {

    public int ownTargetNum;
    public int ownIndex;
	public int ownNum;
	public GameObject cursor;
	private GameObject cursorObject;
    private PlayerController _playerController;
	private CardController _cardController;
	private BattleController _battleController;

	// Use this for initialization
	public void initialize () {
        cursorObject = (GameObject)Instantiate(cursor, transform.position, transform.rotation);
		cursor.SetActive (false);
        cursorObject.SetActive(false);
        _playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
		_cardController = GameObject.Find ("CardController").GetComponent<CardController> ();
		_battleController = GameObject.Find ("BattleManager").GetComponent<BattleController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)
			&& _battleController.checkBattleStarted()
			&&_battleController.checkNotWait()
			&& !_battleController.checkBattleEnded())
        {
			float distance = 100.0f;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast (ray, out hit, distance)
				&& hit.collider.gameObject == this.gameObject) {
				if (ownTargetNum == CardController.TARGET_PLAYER) {
					_playerController.setSelectingCardIndex (ownIndex);
				} else if (ownTargetNum == CardController.TARGET_FIELD
					       && _playerController.getSelectingCardIndex() <= 3) {
					int playerSelectingCardNum = _playerController.getUserCard ();
					// TODO : サーバ上でチェック
					_cardController.checkChangeCards(ownIndex, _playerController.getSelectingCardIndex());
					// TODO : 上記できれば以下不要
					//if (CardController.checkChangeCard(ownNum, playerSelectingCardNum)) {
					//	_playerController.decreaseUserDeckCount ();
					//	Debug.Log (_playerController.getUserDeckCount());
					//	_cardController.changeCard (CardController.TARGET_PLAYER, _playerController.getSelectingCardIndex (), Random.Range (1, CardController.CARD_TYPE_COUNT + 1));
					//	_cardController.changeCard (ownTargetNum, ownIndex, playerSelectingCardNum);
					//}
				}
			}
        }

        if (ownTargetNum == CardController.TARGET_PLAYER)
        {
            checkCursor();
        }
	}

    private void checkCursor()
    {
        bool indexMatch = _playerController.getSelectingCardIndex() == ownIndex;
        if (indexMatch && !cursorObject.active)
        {
			cursorObject.SetActive(true);
        }
		else if (!indexMatch && cursorObject.active)
        {
			cursorObject.SetActive(false);
        }
    }

	public GameObject getCursorObject()
	{
		return cursorObject;
	}

	public void destroyCursorObject()
	{
		DestroyObject (cursorObject);
	}
}
