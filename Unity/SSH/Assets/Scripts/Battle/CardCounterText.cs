﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardCounterText : MonoBehaviour {

	private string _defaultCount = "";

	public void clearCount()
	{
		Text textComponent = GetComponent<Text> ();
		textComponent.text = "";
	}

	public void setCount(int count)
	{
		Text textComponent = GetComponent<Text> ();
		if (count < 99) {
			textComponent.text = count.ToString ();
		} else {
			textComponent.text = _defaultCount;
		}
	}
}
