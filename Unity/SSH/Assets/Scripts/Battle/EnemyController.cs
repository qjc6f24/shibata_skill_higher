﻿using UnityEngine;

public class EnemyController : UserControllerBase
{
	private float _timer;

	void Start()
	{
		_timer = 1.0f;
	}

	public void moveTimer()
	{
		_timer -= Time.deltaTime;
	}

	public float getTimer()
	{
		return _timer;
	}

	public void resetTimer(float resetTime)
	{
		_timer = resetTime;
	}
}
