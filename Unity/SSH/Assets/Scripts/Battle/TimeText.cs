﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeText : MonoBehaviour {

	public void clearTime()
	{
		Text textComponent = GetComponent<Text> ();
		textComponent.text = "";
	}

	public void setWaitingText()
	{
		Text textComponent = GetComponent<Text> ();
		textComponent.text = "waiting...";
	}

	public void setTime(float time, bool isStarted, float battleTime)
	{
		
		Text textComponent = GetComponent<Text> ();
		time += 1.0f;
		if (isStarted && battleTime < time) {
			textComponent.text = "start";
			return;
		}

		if (isStarted && time < 1.0f)
		{
			textComponent.text = "end";
			return;
		}

		textComponent.text = ((int)time).ToString ();
	}

	public void setEnd(int winPlayerNum)
	{
		Text textComponent = GetComponent<Text> ();
		if (winPlayerNum == 0) {
			textComponent.text = "WIN!";
		} else {
			textComponent.text = "LOSE (to " + winPlayerNum + ")";
		}
	}
}
