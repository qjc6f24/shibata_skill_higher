﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserControllerBase : MonoBehaviour {

    private int _selectingCardIndex = 99;
	private int[] _userCards = new int[4];
	private int _deckCount = 10;

    public int getSelectingCardIndex()
    {
        return _selectingCardIndex;
    }

    public void setSelectingCardIndex(int index)
    {
        _selectingCardIndex = index;
    }

	public int getUserCard(int index = -1)
	{
		if (index < 0) {
			return _userCards [this.getSelectingCardIndex ()];
		}
		return _userCards[index];
	}

	public void setUserCard(int index, int num)
	{
		_userCards [index] = num;
	}

	public int getUserDeckCount()
	{
		return _deckCount;
	}

	public void decreaseUserDeckCount()
	{
		_deckCount--;
	}
}
