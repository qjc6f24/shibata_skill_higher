﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour {

	private static float BATTLE_START_INTERVAL_TIME = 3.0f;
	//CountDownprivate static float BATTLE_TIME = 10.0f;
	private float _battleStartIntervalTimer;
	//CountDownprivate float _battleTimer;
	private bool _isBattleStart = false;
	private bool _isBattleEnd = false;
	private bool _isWaitingNewCard = false;

	private TimeText _timeText;
	private int _winPlayerNum;

	void Start()
	{
		_battleStartIntervalTimer = BATTLE_START_INTERVAL_TIME;
		//CountDown_battleTimer = BATTLE_TIME;
		_timeText = GameObject.Find ("Canvas").GetComponentInChildren<TimeText> ();
	}

	public void moveBattleStartIntervalTimer()
	{
		_battleStartIntervalTimer -= Time.deltaTime;
	}

	private void checkBattleStartIntervalTimer()
	{
		if (!_isBattleStart && _battleStartIntervalTimer <= 0.0f)
		{
			_isBattleStart = true;
		}
	}

	//CountDownpublic void moveBattleTimer()
	//CountDown{
	//CountDown	_battleTimer -= Time.deltaTime;
	//CountDown}

	//CountDownprivate void checkBattleTimer()
	//CountDown{
	//CountDown	if (_battleTimer <= 0.0f)
	//CountDown	{
	//CountDown		_isBattleEnd = true;
	//CountDown	}
	//CountDown}

	void Update()
	{
		_timeText.clearTime ();

		if (_isWaitingNewCard) {
			_timeText.setWaitingText ();
		}

		if (!_isBattleStart)
		{
			moveBattleStartIntervalTimer ();
			_timeText.setTime (_battleStartIntervalTimer, false, 0.0f);
			checkBattleStartIntervalTimer ();
			return;
		}

		if (_isBattleEnd)
		{
			_timeText.setEnd (_winPlayerNum);
			return;
		}
			
		//CountDown	moveBattleTimer ();
		//CountDown	_timeText.setTime (_battleTimer, true, BATTLE_TIME);
		//CountDown	checkBattleTimer ();
	}

	// 以下、外部参照

	public bool isBattleStarted(int waitStatus)
	{
		switch (waitStatus) {
		case BattleController.WAIT_STATUS_WAIT_NEW_CARD:
			_isWaitingNewCard = true;
			break;
		case BattleController.WAIT_STATUS_NOT_WAIT:
			_isWaitingNewCard = false;
			break;
		default:
			break;
		}
		return _isBattleStart;
	}

	public bool isBattleEnded()
	{
		return _isBattleEnd;
	}

	public void setEnd(int winPlayerNum)
	{
		_isBattleEnd = true;
		_winPlayerNum = winPlayerNum;
	}
}
