﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleController : MonoBehaviour {

	//private PlayerController _playerController;
	private EnemyController _enemyController;
	private CardController _cardController;
	private TimeController _timeController;

	public const int WAIT_STATUS_WAIT_READY = 0;
	public const int WAIT_STATUS_NOT_WAIT = 1;
	public const int WAIT_STATUS_WAIT_NEW_CARD = 2;
	public int waitStatus = WAIT_STATUS_WAIT_READY;

	private bool _isStarted = false;

	private int _winPlayerNum;

	void Start () {
		//_playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
		_enemyController = GameObject.Find("EnemyController").GetComponent<EnemyController>();
		_cardController = GameObject.Find("CardController").GetComponent<CardController>();
		_timeController = this.GetComponent<TimeController>();
	}

	void Update () {
		// バトル終了判定
		if (checkBattleEnded () && Input.GetMouseButtonDown(0))
		{
			// TODO: 遷移追加
			Destroy(GameObject.Find("GameController"));
			SceneManager.LoadScene ("TitleScene", LoadSceneMode.Single);
			return;
		}

		// バトル開始判定
		//if (!checkBattleStarted ())
		//{
			// 開始まで待機
		//	return;
		//}

		//enemyMove ();
	}

	public bool checkBattleStarted()
	{
		// timeController.isBattleStartedは毎回実行する
		if (_timeController.isBattleStarted (waitStatus) && !_isStarted) {
			_isStarted = true;
			waitStatus = WAIT_STATUS_NOT_WAIT;
		}
		// 開始条件が時間以外になる場合、ここに追記
		return _isStarted;
	}

	public bool checkBattleEnded()
	{
		// 終了条件が時間以外になる場合、ここに追記
		bool result = false;
		result = _timeController.isBattleEnded ();
		result = result && _cardController.checkCardCounts() != 99;
		return result;
	}

	public bool checkNotWait()
	{
		return waitStatus == WAIT_STATUS_NOT_WAIT;
	}

	/*
	private void enemyMove()
	{
		_enemyController.moveTimer ();
		if (_enemyController.getTimer () > 0.0f) {
			return;
		}

		if (_cardController.tryUseEnemyCard()) {
			// 成功時waitTime設定
			_enemyController.resetTimer (Random.Range (1.5f, 5.5f));
			return;
		}

		// 失敗時waitTime設定
		_enemyController.resetTimer (Random.Range (1.0f, 4.5f));
	}
	*/

	public void setWaitStatusNewCard()
	{
		waitStatus = WAIT_STATUS_WAIT_NEW_CARD;
	}

	public void setWaitStatusNotWait()
	{
		waitStatus = WAIT_STATUS_NOT_WAIT;
	}

	public void setWin(int winPlayerNum)
	{
		_winPlayerNum = winPlayerNum;
		_timeController.setEnd (winPlayerNum);
	}
}
