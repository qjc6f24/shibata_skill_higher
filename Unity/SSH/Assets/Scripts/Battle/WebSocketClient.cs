﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using WebSocketSharp;
using WebSocketSharp.Net;

public class WebSocketClient: Pk {

	private static string PORT_NUM = "1993";
	// server run
	private static string IP_ADDRESS = "153.126.163.82"; // SAKURA

	// local PC run
	//private static string IP_ADDRESS = "192.168.10.100"; // HOME
	//private static string IP_ADDRESS = "10.67.173.170";   // TURRYS
	//private static string IP_ADDRESS = "10.4.155.198";   // STARBACKS13
	//private static string IP_ADDRESS = "192.168.11.57";  // COMEDA
	//private static string IP_ADDRESS = "10.4.137.174"; // OTHER

	private static int PHASE_START = 0;
	private static int PHASE_CREATE_USER = 1;
	private static int PHASE_WAIT_CREATE_USER = 2;
	private static int PHASE_LOGIN = 3;
	private static int PHASE_WAIT_ROOM_FILL = 4;
	private static int PHASE_CHANGE_SCENE = 5;
	private static int PHASE_GAME_INIT = 6;
	private static int PHASE_GAME_PLAYING = 7;
	private static int PHASE_END = 8;

	private int _phase;

	private int _userId;
	private int _roomId;
	private int _playerNum;
	private bool _isConnecting = false;

	public bool isChangedField = false;
	private int[] _fieldCardNums;
	private int[][] _playerCardNums;
	private int[] _playerUserIds;
	private int[] _playerCardCounts;

	static WebSocket _ws;
	private CardController _cardController;

	void Start()
	{
		changePhase(PHASE_START);
		DontDestroyOnLoad (this);
		_userId = PlayerPrefs.GetInt ("userId", 0);
		ConnectWebSocket ();
	}

	void Update()
	{
		if (!_isConnecting && Input.GetKeyUp ("r")) {
			Debug.Log ("WebSocket connecting retry.");
			ConnectWebSocket ();
			return;
		}

		if (_phase == PHASE_START) {
			return;
		} else if (_phase == PHASE_CREATE_USER) {
			sendMessage (PK_CREATE_USER_ID, new JSONObject ());
			changePhase (PHASE_WAIT_CREATE_USER);
			return;
		}
		else if (_phase == PHASE_WAIT_CREATE_USER) {
			return;
		}
		else if (_phase == PHASE_LOGIN) {
			if (_userId == 0) {
				changePhase (PHASE_CREATE_USER);
				return;
			}

			saveUserId ();
			sendMessage (PK_LOGIN, new JSONObject ());
			changePhase (PHASE_WAIT_ROOM_FILL);
			return;
		}
		else if (_phase == PHASE_WAIT_ROOM_FILL) {
			return;
		}
		else if (_phase == PHASE_CHANGE_SCENE) {
			SceneManager.LoadScene ("GameScene", LoadSceneMode.Single);
			changePhase (PHASE_GAME_INIT);
			return;
		} 
		else if (_phase == PHASE_GAME_INIT) {
			
			changePhase (PHASE_GAME_PLAYING);
			return;
		}
		else if (_phase == PHASE_GAME_PLAYING) {
			// 終了まで、各種ControllerからのsendMessageコールと
			// サーバから受け取ってparseMessageからの各種PK処理で動作
			return;
		}
	}

	void OnDestroy()
	{
		sendMessage (PK_CLOSE_WEBSOCKET, null);
		_ws.Close();
		_ws = null;
	}

	void ConnectWebSocket()
	{
		_ws = new WebSocket ("ws://" + IP_ADDRESS +":" + PORT_NUM + "/");

		_ws.OnOpen += (sender, e) =>
		{
			_isConnecting = true;
			Debug.Log("WebSocket Open");
		};

		_ws.OnMessage += (sender, e) =>
		{
			// Message受け取った時の処理
			if (e.Data != null && e.Data != "")
			{
				parseMessage(e.Data);	
			}
		};

		_ws.OnError += (sender, e) =>
		{
			Debug.Log("WebSocket Error Message: " + e.Message);
		};

		_ws.OnClose += (sender, e) =>
		{
			_isConnecting = false;
			Debug.Log("WebSocket Close");
		};

		_ws.Connect();
	}

	public void sendMessage(int pkType, JSONObject jsonObject)
	{
		string message = "{\"userId\": " + _userId + ", \"type\": " + pkType + " ";
		if (jsonObject != null && jsonObject.Count > 0) {
			foreach (string key in jsonObject.keys)
			{
				message += ", \"" + key + "\": " + jsonObject.GetField(key) + " ";
			}
		}
		message += "}";
		Debug.Log (message);
		_ws.Send (message);
	}

	void parseMessage(string data)
	{
		Debug.Log("WebSocket Data: " + data);
		JSONObject jsonObject = new JSONObject(data);
		if (jsonObject.GetField ("type") != null) {
			int pkType = (int)jsonObject.GetField("type").i;
			actionWithPk (pkType, jsonObject);
		}
	}

	private void changePhase(int phase)
	{
		_phase = phase;
	}

	private void saveUserId()
	{
		PlayerPrefs.SetInt ("userId", _userId);
	}

	public void pushStart()
	{
		if (!_isConnecting) {
			Debug.Log ("WebSocket connecting retry.");
			ConnectWebSocket ();
			return;
		}
		if (_phase == PHASE_START) {
			changePhase (PHASE_LOGIN);
		}
	}

	public void pushNewPlayer()
	{
		if (_phase == PHASE_START) {
			_userId = 0;
			changePhase (PHASE_CREATE_USER);
		}
	}


	// パケットを受け取ってからの処理
	private void actionWithPk(int pkType, JSONObject jsonObject)
	{
		switch (pkType) {
		case PK_CREATE_USER_ID:
			_userId = getInt("userId", jsonObject);
			changePhase (PHASE_LOGIN);
			break;
		case PK_LOGIN:
			_roomId = getInt("roomId", jsonObject);
			_playerNum = getInt("playerNum", jsonObject);
			break;
		case PK_START_GAME:
			_fieldCardNums = null;
			_fieldCardNums = getIntArr ("fieldCardNums", jsonObject);
			_playerCardNums = null;
			_playerCardNums = getIntArrArr ("playerCardNums", jsonObject);
			_playerUserIds = null;
			_playerUserIds = getIntArr ("playerUserIds", jsonObject);
			_playerCardCounts = null;
			_playerCardCounts = getIntArr ("playerCardCounts", jsonObject);
			isChangedField = true;
			changePhase (PHASE_CHANGE_SCENE);
			break;
		case PK_UPDATE_CARDS:
			_fieldCardNums = null;
			_fieldCardNums = getIntArr ("fieldCardNums", jsonObject);
			_playerCardNums = null;
			_playerCardNums = getIntArrArr ("playerCardNums", jsonObject);
			_playerUserIds = null;
			_playerUserIds = getIntArr ("playerUserIds", jsonObject);
			isChangedField = true;
			if (!getBool ("existAvailableCard", jsonObject) && _cardController != null) {
				Debug.Log ("existAvailableCard is false.");
				_cardController.setWaitStatusNewCard ();
			}
			_playerCardCounts = null;
			_playerCardCounts = getIntArr ("playerCardCounts", jsonObject);
			break;
		case PK_UPDATE_CARD_FORCE:
			_fieldCardNums = null;
			_fieldCardNums = getIntArr ("fieldCardNums", jsonObject);
			_playerCardNums = null;
			_playerCardNums = getIntArrArr ("playerCardNums", jsonObject);
			_playerUserIds = null;
			_playerUserIds = getIntArr ("playerUserIds", jsonObject);
			isChangedField = true;
			if (getBool ("existAvailableCard", jsonObject) && _cardController != null) {
				Debug.Log ("existAvailableCard is true.");
				_cardController.setWaitStatusNotWait ();
			}
			break;
		default:
			break;
		}
	}

	public void setCardController(CardController cardController)
	{
		_cardController = cardController;
	}

	public int getPlayerNum()
	{
		return _playerNum;
	}

	public int getNextPlayerNum(int basePlayerNum)
	{
		if (basePlayerNum >= _playerUserIds.Length - 1) {
			return 0;
		}
		return basePlayerNum + 1;
	}

	public int[] getFieldCardNums()
	{
		return _fieldCardNums;
	}

	public int getFieldCardNum(int index)
	{
		return _fieldCardNums[index];
	}

	public int[][] getPlayerCardNums()
	{
		return _playerCardNums;
	}

	public int[] getPlayerCardNums(int index)
	{
		return _playerCardNums[index];
	}

	public int getPlayerCardNum(int userIndex, int cardIndex)
	{
		//Debug.Log ("userIdx: " + userIndex + ", cardIdx: " + cardIndex + ", card: " + _playerCardNums[userIndex][cardIndex]);
		return _playerCardNums[userIndex][cardIndex];
	}

	public int[] getPlayerUserIds()
	{
		return _playerUserIds;
	}

	public int getPlayerUserId(int index)
	{
		return _playerUserIds[index];
	}

	public void checkChangeCard(JSONObject jsonObject)
	{
		sendMessage (PK_UPDATE_CARDS, jsonObject);
	}

	public int getPlayerCardCount(int playerNum)
	{
		return _playerCardCounts [playerNum];
	}
}